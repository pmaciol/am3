
/*! \file matCalcClient.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareMatCalcMpiClient_matCalcClient_h__
#define am3softwareMatCalcMpiClient_matCalcClient_h__
/*	include *********************************************************/

#include "./softwareMatCalcMpiClient.h"
#include "../softwareMatCalcGeneric/matCalcScriptsBased.h"
#include "../modelTemplates/variableSource.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace smc = am3::software::matcalc;
namespace mvs = am3::model::varSource;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

class MatCalcInstance
  //: public smc::MatCalcScriptsBased<mvs::PushableSingleValueSource<mpr::Temperature> >
  : public smc::MatCalcScriptsBased<mvs::PushableTempPlusVectorValueSource>
{
public:
  MatCalcInstance(const smc::MatCalcScriptProvider& scripts, boost::shared_ptr < mvs::PushableTempPlusVectorValueSource> varSource)
    : smc::MatCalcScriptsBased<mvs::PushableTempPlusVectorValueSource>(scripts, varSource)
//   MatCalcInstance(const smc::MatCalcScriptProvider& scripts, boost::shared_ptr < mvs::PushableSingleValueSource<mpr::Temperature > > varSource)
//     : smc::MatCalcScriptsBased<mvs::PushableSingleValueSource<mpr::Temperature> >(scripts, varSource)
  {};

//   virtual void InitWithStartingValues(double time, double temperature);
//   virtual void RunNextStep(double time, double temperature);
//   virtual double GetVariableValue(std::string variableName, double time);
//   virtual double GetVariableValue(unsigned variableId, double time);
//   virtual void Finalize();
};
#endif // am3softwareMatCalcMpiClient_matCalcClient_h__
