
/*! \file mpiInterface.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareMatCalcMpiClient_mpiInterface_h__
#define am3softwareMatCalcMpiClient_mpiInterface_h__
/*	include *********************************************************/

#include <mpi.h>
#include <iostream>
#include "../models/point.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

void InitMpi(int argc, char* argv[]);
void FinalizeMpi();

double GetFromMpiInitTime();
double GetFromMpiInitTemperature();
double GetFromMpiEndTime();
double GetFromMpiEndTemperature();
void MpiSendResults(const double * resultsToSend);
bool MpiFinish();

template<typename MODEL, typename VARIABLE_SOURCE>
class Manager
{
public:
  Manager(boost::shared_ptr<MODEL> model, boost::shared_ptr<VARIABLE_SOURCE> varSource) : model_(model), varSource_(varSource){};
  void Listen();

private:
  boost::shared_ptr<MODEL> model_;
  boost::shared_ptr<VARIABLE_SOURCE> varSource_;

  mpt::Point<mpr::Coordinates2D>* GetMpiPoint();
  mpr::Temperature GetMpiTemperature();
  int GetMpiKeepGoingFlag();
  std::vector<double> GetMpiVariables();
  void SendVariables(std::vector<double> variables)
  {
    lo::debugLog("Entering SendVariable method");
    int howManyVariables = variables.size();
    lo::debugLog("Sending number of variables = " + std::to_string(howManyVariables));
    MPI_Send(&howManyVariables, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    for (auto& v : variables)
    {
      double val = v;
      lo::debugLog("Sending variable value = " + std::to_string(v));
      MPI_Send(&val, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
    }
    lo::debugLog("SendVariable method finished");
  }

  int flag;

  bool KeepGoing()
  {
    bool toRet;
    flag = GetMpiKeepGoingFlag();
    
    while (flag == 2)
    {
      int finished = 1;
      MPI_Send(&finished, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
      flag = GetMpiKeepGoingFlag();
    }

    if (flag == 0)
    {
      toRet = false;
    } 
    else
    {
      toRet = true;
    }
    return toRet;
  }
};

////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////


template<typename MODEL, typename VARIABLE_SOURCE>
std::vector<double> Manager<MODEL, VARIABLE_SOURCE>::GetMpiVariables()
{
  std::vector<double> toRet;
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Waiting for additional variables " + std::to_string(world_rank)));
  int howManyVariables;
  MPI_Recv(&howManyVariables, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Received " + std::to_string(howManyVariables)));
  for (int i = 0; i < howManyVariables;i++)
  {
    double tempVar = 0;
    MPI_Recv(&tempVar, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Received " + std::to_string(tempVar)));
    toRet.push_back(tempVar);
  }
//  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Received " + std::to_string(howManyVariables) + " variables"+ ", process = " + std::to_string(world_rank)));
  for (double v :toRet)
  {
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, (std::to_string(v)));
  }
  return toRet;
}


template<typename MODEL, typename VARIABLE_SOURCE>
int Manager<MODEL, VARIABLE_SOURCE>::GetMpiKeepGoingFlag()
{
  int toRet;
  MPI_Recv(&toRet, 1, MPI_INT, 0, 0, MPI_COMM_WORLD,MPI_STATUS_IGNORE);
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Received go flag in process " + std::to_string(world_rank) + " value = " + std::to_string(toRet)));
  return (toRet);
}

template<typename MODEL, typename VARIABLE_SOURCE>
mpr::Temperature Manager<MODEL, VARIABLE_SOURCE>::GetMpiTemperature()
{
  double temperature;
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Waiting for temperature " + std::to_string(world_rank)));
  MPI_Recv(&temperature, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Received temperature " + std::to_string(temperature) + ", process = " + std::to_string(world_rank)));
  mpr::Temperature toRet(temperature);
  return toRet;
}

template<typename MODEL, typename VARIABLE_SOURCE>
mpt::Point<mpr::Coordinates2D>*  Manager<MODEL, VARIABLE_SOURCE>::GetMpiPoint()
{
  double coordinates[2];
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Waiting for point coord and time " + std::to_string(world_rank)));
  MPI_Recv(coordinates, 2, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Received coords " + std::to_string(world_rank)));
  double time;
  MPI_Recv(&time, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Received time " + std::to_string(world_rank)));

  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Received point coord and time " + std::to_string(coordinates[0]) + " " + std::to_string(coordinates[1]) + " " + std::to_string(time) + ", process " + std::to_string(world_rank)));
  const mpr::Coordinates2D coord = mpr::Coordinates2D(coordinates[0], coordinates[1]);
  mpr::Time t = mpr::Time(time);
  mpt::Point<mpr::Coordinates2D> * toRet = new mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  std::cout << "Point created " << coordinates[0] << " " << coordinates[1] << " " << time << " world_rank = " << world_rank <<std::endl;
    return toRet;
}

template<typename MODEL, typename VARIABLE_SOURCE>
void Manager<MODEL, VARIABLE_SOURCE>::Listen()
{
  bool keepGoing = false;
  mpr::Temperature temperature;
  mpt::Point<mpr::Coordinates2D> * point;
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Start listening in process " + std::to_string(world_rank)));
  model_->SetProcessId(std::to_string(world_rank));
  do
  {
    keepGoing = KeepGoing();
    if (keepGoing)
    {
      if (flag == 1)
      {
        point = GetMpiPoint();
        temperature = GetMpiTemperature();
        varSource_->SetVariable(*point, temperature);
        lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Variable set in " + std::to_string(world_rank)));
        model_->InitIfNeeded(point);
        if (point->GetTime() > 0.0)
        {
          lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Calculations started in " + std::to_string(world_rank)));
          model_->CalculateStepIfNeeded(point);
          lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Calculations finished in " + std::to_string(world_rank)));
        }
      }
      else if (flag == 3)
      {
        SendVariables(model_->GetResults());
      }
      else if (flag == 4)
      {
        std::vector<double> additionalVariables = GetMpiVariables();
        varSource_->SetAdditionalVariables(*point, additionalVariables);
      }
     

    }
  } while (keepGoing);
  MpiFinish();
}

#endif // am3softwareMatCalcMpiClient_mpiInterface_h__
