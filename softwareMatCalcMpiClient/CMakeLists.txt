include_directories (../)
add_executable(am3softwareMatCalcMpiClient
softwareMatCalcMpiClient.h softwareMatCalcMpiClient.cpp
mpiInterface.h mpiInterface.cpp
matCalcClient.h 
#matCalcClient.cpp
)

target_link_libraries(am3softwareMatCalcMpiClient am3softwarematCalcGeneric am3softwareMatCalc
msmpi.lib
)

SET_TARGET_PROPERTIES(am3softwareMatCalcMpiClient PROPERTIES LINKER_LANGUAGE CXX)
SET(CMAKE_EXE_LINKER_FLAGS /NODEFAULTLIB:\"LIBCMTD.lib;libcpmtd.lib;LIBCMT.lib;libcpmt.lib;\")

