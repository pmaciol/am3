
/*! \file softwareMatCalcMpiClient.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "softwareMatCalcMpiClient.h"
#include "mpiInterface.h"
#include "matCalcClient.h"
#include "../modelTemplates/variableSource.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace mvs = am3::model::varSource;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

int main(int argc, char **argv)
{
   
    //boost::shared_ptr<mvs::PushableSingleValueSource<mpr::Temperature> > source(new mvs::PushableSingleValueSource<mpr::Temperature>);
  boost::shared_ptr<mvs::PushableTempPlusVectorValueSource > source(new mvs::PushableTempPlusVectorValueSource);
    smc::MatCalcScriptProvider scripts;
    
    
   boost::shared_ptr<MatCalcInstance> mCinstance(new MatCalcInstance(scripts, source));
   Manager<MatCalcInstance, mvs::PushableTempPlusVectorValueSource > manager(mCinstance, source);



   InitMpi(argc, argv);
   manager.Listen();
   FinalizeMpi();

   return true;
}
