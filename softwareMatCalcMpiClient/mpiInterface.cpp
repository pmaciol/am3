
/*! \file mpiInterface.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "mpiInterface.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/


void FinalizeMpi()
{
  MPI_Finalize();
}

void InitMpi(int argc, char* argv[])
{
  int par_rank = 0, par_size = 0;
  const int bufsize = 1000;
  char msgbuf[bufsize] = { 0 }, filename[bufsize] = { 0 };

  /* Init MPI */
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &par_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &par_size);

  char pname[MPI_MAX_PROCESSOR_NAME];
  int plen = 0;
  memset(pname, 0, sizeof(char)*MPI_MAX_PROCESSOR_NAME);
  MPI_Get_processor_name(pname, &plen);

  printf("%s: Rank %d is part of a %d processor job\n", pname, par_rank, par_size);
}

double GetFromMpiInitTime()
{
  return 0;
}

double GetFromMpiInitTemperature()
{
  return 0;
}

double GetFromMpiEndTime()
{
  return 0;
}

double GetFromMpiEndTemperature()
{
  return 0;
}

void MpiSendResults(const double * resultsToSend)
{

}

bool MpiFinish()
{
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Process finished " + std::to_string(world_rank)));
//  MPI_Finalize();
  return false;
}
