
/*! \file softwareMatCalcMpiClient.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareMatCalcMpiClient_softwareMatCalcMpiClient_h__
#define am3softwareMatCalcMpiClient_softwareMatCalcMpiClient_h__
/*	include *********************************************************/

#include <string>
//#include "../softwareMatCalcGeneric/matCalcGeneric.h"

/*	using ***********************************************************/

//namespace smc = am3::software::matcalc;

/*	extern **********************************************************/

#define RETURN_VARIABLES_NUMBER 10

/*	classes *********************************************************/

// 
// 
// void InitModel(smc::ModelInstance * mCinstance);
// void Listen(smc::ModelInstance * mCinstance);
// void Finalize(smc::ModelInstance * mCinstance);

#endif // am3softwareMatCalcMpiClient_softwareMatCalcMpiClient_h__
