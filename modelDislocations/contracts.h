
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef dislocationsContracts_h__
#define dislocationsContracts_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../models/contractMacros.h"
#include "../models/macrosSimplifiedModels.h"

/*	using ***********************************************************/

namespace moc=am3::model::contract;
namespace mpr=am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace problem
  {
    namespace dislocations
    {
      SIMPLE_PROP(DislocationsDensityWall,double);    // the dislocations composing the subgrain walls; [m-2]
      SIMPLE_PROP(DislocationsDensityRandom,double);  //all other dislocations, which do not compose the subgrain walls; [m-2]
      SIMPLE_PROP(ActivationVolume,double);
      SIMPLE_PROP(ActivationEnergy,double);
      SIMPLE_PROP(InterObstacleLimit,double);
      SIMPLE_PROP(FrequencyDominantObstacle,double);
      SIMPLE_PROP(DislocationDensityMobile,double);
      SIMPLE_PROP(BurgersVector,double);

      namespace contract
      {
        CONTRACT(		WallsDislocationsContract			        	,1,mpr::Coordinates2D, DislocationsDensityWall);
        CONTRACT(		RandomDislocationsContract			        ,1,mpr::Coordinates2D, DislocationsDensityRandom);
        CONTRACT(		DislocationsStorageContract			        ,2,mpr::Coordinates2D, DislocationsDensityWall, DislocationsDensityRandom);
        CONTRACT(		StrainRateTemperatureInput    	  ,2,mpr::Coordinates2D, mpr::EffectiveStrainRate,mpr::Temperature);
        CONTRACT(		StrainRateTemperatureStepInput    	  ,3,mpr::Coordinates2D, mpr::EffectiveStrainRate,mpr::Temperature,mpr::StepLength);
//         CONTRACT(		ShearRateInput		  		,1,mpr::Coordinates2D, mpr::ShearRate);
//         CONTRACT(		ShearRateTemperatureInput    	  		  	,2,mpr::Coordinates2D, mpr::ShearRate,mpr::Temperature);
      }
    }
  }
}
#endif // dislocationsContracts_h__
