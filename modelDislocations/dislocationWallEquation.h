
/*! \file dislocationWallEquation.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef dislocationWallEquation_h__
#define dislocationWallEquation_h__
/*	include *********************************************************/

#include "../models/inputTypelists.h"
#include "../models/models.h"
#include "../common/externalProgramOptions.h"
#include "./contracts.h"

/*	using ***********************************************************/

namespace mo  = am3::model;
namespace mpr = am3::model::properties;
namespace cfg = am3::common;
namespace pdi = am3::problem::dislocations;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace dislocations
		{

      template<typename STORAGE>
      struct  DislocationWallEquation_Param
      {
        DislocationWallEquation_Param(const double& w1a, const double& w1b, const double& w2a, const double& w2b, const double& w3a, const double& w3b, boost::shared_ptr<STORAGE> storage);

        boost::shared_ptr<STORAGE> previousStepData_;
        const double w1a_, w2a_, w3a_, w1b_, w2b_, w3b_;
      };




			template<typename INPUT, typename STORAGE>
      class DislocationWallEquation
        : public mo::InputTypelist<INPUT>
        , public pdi::contract::WallsDislocationsContract
        , public mo::ComputationsOnFirstCall < mpr::Coordinates2D >
      {
      public:
        typedef typename pdi::contract::StrainRateTemperatureStepInput Inputs;
        typedef typename pdi::contract::WallsDislocationsContract Contract;
        typedef STORAGE IvStorage;

        DislocationWallEquation(const DislocationWallEquation_Param<STORAGE>& param);
        DislocationWallEquation(const double& w1a, const double& w1b,const double& w2a, const double& w2b,const double& w3a, const double& w3b, boost::shared_ptr<STORAGE> storage);
        DislocationWallEquation(const double& w1a, const double& w1b,const double& w2a, const double& w2b,const double& w3a, const double& w3b);
        void SetIvStorage(boost::shared_ptr<STORAGE> storage);
        pdi::DislocationsDensityWall GetState(mpt::Point<mpr::Coordinates2D> * point, const pdi::DislocationsDensityWall *const);
        template<typename VAR> VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> pdi::DislocationsDensityWall Get<pdi::DislocationsDensityWall>(mpt::Point<mpr::Coordinates2D> * point) { pdi::DislocationsDensityWall* trait;  return GetState(point, trait); };
      protected:
        // UserGuide: Derived from BaseClass::ComputationsOnFirstCall. See implementation for guides
        virtual void GetData(pnt::Point<mpr::Coordinates2D> * point);
        // UserGuide: Derived from BaseClass::ComputationsOnFirstCall. See implementation for guides
        virtual void Calculate(pnt::Point<mpr::Coordinates2D> * point);

        // UserGuide: Used in GetData method. Stores data from previous step
        pdi::DislocationsDensityWall dislocationsDensityWallPrevious_;
        pdi::DislocationsDensityWall dislocationsDensityWallNew_;

        boost::shared_ptr<STORAGE> previousStepData_;
        const double w1a_, w2a_, w3a_, w1b_, w2b_, w3b_;
        mpr::Temperature temperature_;
        mpr::EffectiveStrainRate strainRate_;
        mpr::StepLength stepLength_;
      };

////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////
      template<typename STORAGE>
      DislocationWallEquation_Param<STORAGE>::DislocationWallEquation_Param(
        const double& w1a, const double& w1b,
        const double& w2a, const double& w2b,
        const double& w3a, const double& w3b,
        boost::shared_ptr<STORAGE> storage)
        : w1a_(w1a), w1b_(w1b), w2a_(w2a), w2b_(w2b), w3a_(w3a), w3b_(w3b), previousStepData_(storage)
      {};

      //******************************************************************************************************************************************************************

      template<typename INPUT, typename STORAGE>
      DislocationWallEquation<INPUT, STORAGE>::DislocationWallEquation(const DislocationWallEquation_Param<STORAGE>& param)
        : w1a_(param.w1a_), w1b_(param.w1b_), w2a_(param.w2a_), w2b_(param.w2b_), w3a_(param.w3a_), w3b_(param.w3b_), previousStepData_(param.previousStepData_)
      {};

      //******************************************************************************************************************************************************************
      
      template<typename INPUT, typename STORAGE>
      DislocationWallEquation<INPUT, STORAGE>::DislocationWallEquation(
        const double& w1a, const double& w1b,
        const double& w2a, const double& w2b,
        const double& w3a, const double& w3b,
        boost::shared_ptr<STORAGE> storage)
        : w1a_(w1a), w1b_(w1b), w2a_(w2a), w2b_(w2b), w3a_(w3a), w3b_(w3b), previousStepData_(storage)
      {};

      //******************************************************************************************************************************************************************

      template<typename INPUT, typename STORAGE>
      DislocationWallEquation<INPUT, STORAGE>::DislocationWallEquation(
        const double& w1a, const double& w1b,
        const double& w2a, const double& w2b,
        const double& w3a, const double& w3b)
        : w1a_(w1a), w1b_(w1b), w2a_(w2a), w2b_(w2b), w3a_(w3a), w3b_(w3b)
      {};

      //******************************************************************************************************************************************************************

      template<typename INPUT, typename STORAGE>
      void DislocationWallEquation<INPUT, STORAGE>::SetIvStorage(boost::shared_ptr<STORAGE> storage)
      {
        previousStepData_ = storage;
      }

      //******************************************************************************************************************************************************************

        template<typename INPUT, typename STORAGE>
        pdi::DislocationsDensityWall DislocationWallEquation<INPUT, STORAGE>::GetState(mpt::Point<mpr::Coordinates2D> * point, const pdi::DislocationsDensityWall *  trait)
        {
          CheckOrGetDataInPoint(point);
          return dislocationsDensityWallNew_;
        }

        //******************************************************************************************************************************************************************

      template<typename INPUT, typename STORAGE>
      void DislocationWallEquation<INPUT, STORAGE>::GetData(pnt::Point<mpr::Coordinates2D> * point)
      {
        try
        {
          dislocationsDensityWallPrevious_(previousStepData_->GetPreviousState<pdi::DislocationsDensityWall>());
          temperature_ = bus_->GetFromDatabus<mpr::Temperature>(point);
          strainRate_ = bus_->GetFromDatabus<mpr::EffectiveStrainRate>(point);
          stepLength_ = bus_->GetFromDatabus<mpr::StepLength>(point);
        }
        catch (...)
        {
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "PRobably previousStepData_ not initialized");
        }
      };

      //******************************************************************************************************************************************************************

      template<typename INPUT, typename STORAGE>
      void DislocationWallEquation<INPUT, STORAGE>::Calculate(pnt::Point<mpr::Coordinates2D> * point)
      {
        if (strainRate_() > 1e-5)
        {
          const double w3 = 0;
          const double w1 = 6.35e6;
          const double w2 = 4.89;

          double rhoWold = dislocationsDensityWallPrevious_();
          double tmp = w1 * sqrt(rhoWold);
          tmp += -w2 * rhoWold;
          tmp *= strainRate_();
          tmp -= w3 * rhoWold;
          tmp *= stepLength_();
          dislocationsDensityWallNew_(tmp + rhoWold);
        }
        else dislocationsDensityWallNew_ = dislocationsDensityWallPrevious_;
      }

      //******************************************************************************************************************************************************************

//       template<typename INPUT, typename STORAGE>
// //      template<> 
//       pdi::DislocationsDensityWall DislocationWallEquation<INPUT, STORAGE>::Get<pdi::DislocationsDensityWall>(mpt::Point<mpr::Coordinates2D> * point)
//       {
//         pdi::DislocationsDensityWall* trait;  
//         return GetState(point, trait); 
//       };
    
		} //dislocations
	} //problem
} //am3
#endif // dislocationWallEquation_h__
