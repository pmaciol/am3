
/*! \file dislocationWallEquation.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef dislocationRandomEquation_h__
#define dislocationRandomEquation_h__
/*	include *********************************************************/

#include "../models/models.h"
#include "../common/externalProgramOptions.h"
#include "./contracts.h"
#include "../models/inputTypelists.h"
#include "../databus/databusTypelists.h"

/*	using ***********************************************************/

namespace mo  = am3::model;
namespace mpr = am3::model::properties;
namespace pdi = am3::problem::dislocations;
namespace db  = am3::databus;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
    namespace dislocations
    {
      template<typename STORAGE>
      struct  DislocationRandomEquation_Param
      {
        DislocationRandomEquation_Param(
          const double& r1a, const double& r1b,
          const double& r2a, const double& r2b,
          const double& r3a, const double& r3b,
          boost::shared_ptr<STORAGE> storage);

        boost::shared_ptr<STORAGE> previousStepData_;
        const double r1a_, r2a_, r3a_, r1b_, r2b_, r3b_;
      };
      


      template<typename INPUT, typename STORAGE>
      class DislocationRandomEquation
        : public mo::InputTypelist<INPUT>
        , public pdi::contract::RandomDislocationsContract
        , public mo::ComputationsOnFirstCall<mpr::Coordinates2D>
      {
      public:
        typedef typename pdi::contract::StrainRateTemperatureStepInput Inputs;
        typedef typename  pdi::contract::RandomDislocationsContract Contract;
        typedef STORAGE IvStorage;

        typedef mpr::Coordinates2D BaseCoordinates;
        
        DislocationRandomEquation(const DislocationRandomEquation_Param<STORAGE>& param);
        DislocationRandomEquation(
          const double& r1a, const double& r1b,
          const double& r2a, const double& r2b,
          const double& r3a, const double& r3b,
          boost::shared_ptr<STORAGE> storage);
        DislocationRandomEquation(
          const double& r1a, const double& r1b,
          const double& r2a, const double& r2b,
          const double& r3a, const double& r3b);
        void SetIvStorage(boost::shared_ptr<STORAGE> storage);
        pdi::DislocationsDensityRandom GetState(mpt::Point<mpr::Coordinates2D> * point, const pdi::DislocationsDensityRandom  *const);
        template<typename VAR> VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> pdi::DislocationsDensityRandom Get<pdi::DislocationsDensityRandom>(mpt::Point<mpr::Coordinates2D> * point) { pdi::DislocationsDensityRandom trait ;  return GetState(point, &trait); };
      protected:
        // UserGuide: Derived from BaseClass::ComputationsOnFirstCall. See implementation for guidelines
        virtual void GetData(pnt::Point<mpr::Coordinates2D> * point);
        // UserGuide: Derived from BaseClass::ComputationsOnFirstCall. See implementation for guides
        virtual void Calculate(pnt::Point<mpr::Coordinates2D> * point);

        // UserGuide: Used in GetData method. Stores data from previous step
        pdi::DislocationsDensityRandom dislocationsDensityRandomPrevious_;
        pdi::DislocationsDensityRandom dislocationsDensityRandomNew_;

        boost::shared_ptr<STORAGE> previousStepData_;
        const double r1a_,r2a_,r3a_, r1b_,r2b_,r3b_;
        mpr::Temperature temperature_;
        mpr::EffectiveStrainRate strainRate_;
        mpr::StepLength stepLength_;
      };

////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////
      template<typename STORAGE>
      DislocationRandomEquation_Param<STORAGE>::DislocationRandomEquation_Param(
        const double& r1a, const double& r1b,
        const double& r2a, const double& r2b,
        const double& r3a, const double& r3b,
        boost::shared_ptr<STORAGE> storage)
        : r1a_(r1a), r1b_(r1b), r2a_(r2a), r2b_(r2b), r3a_(r3a), r3b_(r3b), previousStepData_(storage)
      {}; 

      //******************************************************************************************************************************************************************

      template<typename INPUT, typename STORAGE>
      DislocationRandomEquation<INPUT, STORAGE>::DislocationRandomEquation(const DislocationRandomEquation_Param<STORAGE>& param)
        : r1a_(param.r1a_), r1b_(param.r1b_), r2a_(param.r2a_), r2b_(param.r2b_), r3a_(param.r3a_), r3b_(param.r3b_), previousStepData_(param.previousStepData_)
      {};

      //******************************************************************************************************************************************************************
      

    template<typename INPUT, typename STORAGE>
    DislocationRandomEquation<INPUT, STORAGE>::DislocationRandomEquation(
      const double& r1a, const double& r1b,
      const double& r2a, const double& r2b,
      const double& r3a, const double& r3b,
      boost::shared_ptr<STORAGE> storage)
      : r1a_(r1a), r1b_(r1b), r2a_(r2a), r2b_(r2b), r3a_(r3a), r3b_(r3b), previousStepData_(storage)
    {};

    //******************************************************************************************************************************************************************

    template<typename INPUT, typename STORAGE>
    DislocationRandomEquation<INPUT, STORAGE>::DislocationRandomEquation(
      const double& r1a, const double& r1b,
      const double& r2a, const double& r2b,
      const double& r3a, const double& r3b)
      : r1a_(r1a), r1b_(r1b), r2a_(r2a), r2b_(r2b), r3a_(r3a), r3b_(r3b)
    {};

    //******************************************************************************************************************************************************************

    template<typename INPUT, typename STORAGE>
    void DislocationRandomEquation<INPUT, STORAGE>::SetIvStorage(boost::shared_ptr<STORAGE> storage)
    {
      previousStepData_ = storage;
    }

    //******************************************************************************************************************************************************************

    template<typename INPUT, typename STORAGE>
    pdi::DislocationsDensityRandom DislocationRandomEquation<INPUT, STORAGE>::GetState(mpt::Point<mpr::Coordinates2D> * point, const pdi::DislocationsDensityRandom *  trait)
    {
      CheckOrGetDataInPoint(point);
      return dislocationsDensityRandomNew_;
    }

    //******************************************************************************************************************************************************************

    template<typename INPUT, typename STORAGE>
    void DislocationRandomEquation<INPUT, STORAGE>::GetData(pnt::Point<mpr::Coordinates2D> * point)
    {
      try
      {
//        actualPoint = point->Copy();
        dislocationsDensityRandomPrevious_(previousStepData_->GetPreviousState<pdi::DislocationsDensityRandom>());
        temperature_ = bus_->GetFromDatabus<mpr::Temperature>(point);
        strainRate_ = bus_->GetFromDatabus<mpr::EffectiveStrainRate>(point);
        stepLength_ = bus_->GetFromDatabus<mpr::StepLength>(point);
      }
      catch (...)
      {
        lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Probably previousStepData_ not initialized");
      }
    };

    //******************************************************************************************************************************************************************

    template<typename INPUT, typename STORAGE>
    void DislocationRandomEquation<INPUT, STORAGE>::Calculate(pnt::Point<mpr::Coordinates2D> * point)
    {
      if (strainRate_() > 1e-5)
      {
        const double r1 = 4.85e8;
        const double r2 = 66.35;
        const double r3 = 0;

        double rhoRold = dislocationsDensityRandomPrevious_();
        double tmp = r1 * sqrt(rhoRold);
        tmp += -r2 * rhoRold;
        tmp *= strainRate_();
        tmp -= r3 * rhoRold;
        tmp *= stepLength_();
        dislocationsDensityRandomNew_(tmp + rhoRold);
      }
      else
      {
        dislocationsDensityRandomNew_ = dislocationsDensityRandomPrevious_;
      }
    }
        } //dislocations
	} //problem
} //am3
#endif // dislocationRandomEquation_h__
