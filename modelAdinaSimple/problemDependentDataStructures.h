
/*! \file problemDependentDataStructures.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details  Definitions of Properties and its Providers used in problem
*********************************************************************/
#ifndef problemDependentDataStructures_h__
#define problemDependentDataStructures_h__
/*	include *********************************************************/

#include "../models/contracts.h"

/*	using ***********************************************************/

namespace con=am3::model::contract;
namespace mod=am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace example1
    {
      //! Property defined for Example1
      struct AgglomerationFactor : public SimpleProperty<double,mod::HaveGetSetValue> {};
      // //! Property defined for Example1
      // struct SolidFraction : public SimpleProperty<double,mod::HaveGetSetValue> {};
      //! Provider for Property defined for Example1
      struct AgglomerationFactorProvider
        :public con::Provider<AgglomerationFactor> {virtual ~AgglomerationFactorProvider(){};};
    } //ionex
  } //model
} //am3

#endif // problemDependentDataStructures_h__
