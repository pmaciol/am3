
#include "problemModelAgglomeration.h"
#include "../logger/logger.h"

namespace com=am3::model::common;
namespace lo=am3::logger;

am3::model::example1::AgglomerationFactor am3::model::example1::modelAgglomeration1::GetState
  ( com::Point* point,const AgglomerationFactor* const )
{
  lo::Starting("modelAgglomeration1");
  am3::model::example1::AgglomerationFactor toReturn;
  // If you need any inputs, ask for them, like below (only variables from defined inputs are available)
  am3::model::VelocityWithDerivatives vel;
  am3::model::Temperature temp;
  vel=input_->GetState(point,&vel);
  temp=input_->GetState(point,&temp);
  // Here compute returned value - agglomeration in this case. Usually here is a call to external procedures
  toReturn(0.5);
  //And finally return computed value
  lo::Stopping("modelAgglomeration1");
  return toReturn;
}

int am3::model::example1::modelAgglomeration1::Init( InputsAgglomerationFactorContract* input )
{
  lo::Starting("modelAgglomeration1 initialization");
  input_=input;
  lo::Stopping("modelAgglomeration1 initialization");
  return Init();
}

am3::model::example1::modelAgglomeration1::~modelAgglomeration1()
{
  lo::Starting("modelAgglomeration1 destructor");
  if (input_!=nullptr) delete input_;
  input_=nullptr;
  lo::Stopping("modelAgglomeration1 destructor");
}
