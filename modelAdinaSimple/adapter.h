
/*! \file adapter.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief Definition of SimpleAdinaAdapter
 * \details   	
*********************************************************************/
#ifndef modelAdinaSimple_adapter_h__
#define modelAdinaSimple_adapter_h__
/*	include *********************************************************/

#include <vector>
#include "../adapter/adapter.h"
#include "../adinaModels/adinaDataInterface.h"
#include "../models/point.h"
#include "problemInterfaces.h"
#include "problemDependentDataStructures.h"
#include "problemModelProperties.h"

/*	using ***********************************************************/

using std::vector;
namespace ada=am3::adapter;
namespace def=am3::model::contract::defined;
namespace con=am3::model::contract;
namespace com=am3::model::common;
namespace exa=am3::model::example1;

/*	extern **********************************************************/
/*	classes *********************************************************/

/*! \page Example1 How to read example
  \tableofcontents
  Leading text.
  \section sec An example section
  This page contains the subsections \ref subsection1 and \ref subsection2.
  For more info see page \ref page2.
  \subsection subsection1 The first subsection
  Text.
  \subsection subsection2 The second subsection
  More text.
*/


namespace am3
{
  namespace adapter
  {
    namespace adinaSimple
    {
      //! Main class for problem (Example1 in this case)
      //!
      //! Must derive from:
      //!   - am3::adapter::Adapter
      //!   - Contracts for all models used by adapter. Contracts can overlap
      //!     - def::ViscContract - Viscosity, necessary for macroscopic model
      //!     - def::AdinaFContract - must be implemented to use ADINA-CFD model. Properties which are not really necessary should be set to "0.0"
      //!     - exa::ExampleMacroContract - Contract offered by macroscopic phenomena
      //!     - exa::InputsAgglomerationFactorContract - Contract for model calculating agglomeration factor
      //!     - exa::ThixotropyInternalsContract - Contract for model calulating viscosity (all data necessary for viscosity)
      class SimpleAdinaAdapter 
        : public ada::Adapter
        , public def::ViscContract
        , public def::AdinaFContract

        , public exa::ExampleMacroContract
        , public exa::ThixotropyInternalsContract
        , public exa::InputsAgglomerationFactorContract
      {
       protected:
         exa::ConstMaterialProperties material_properties_;
         //! container of viscosity models
         vector<def::ViscProvider*> viscosity_models_;
         //! container of agglomeration factor models
         vector<exa::AgglomerationFactorProvider*> agglomeration_models_;
         //! pointer to object providing ModelID of viscosity model (Proxy to KBS)
         kno::I4Adapter<pro::Viscosity>* viscosityModelSelector_;
         //! pointer to object providing ModelID of agglomeration model (Proxy to KBS)
         kno::I4Adapter<exa::AgglomerationFactor>* agglomerationModelSelector_;
         def::AdinaFProvider* adinaFStorage_;
      public:
        //! Constructor must prepare (create and initialize) fine models - fill containers: 
        //!   - viscosity_models_
        //!   - agglomeration_models_
        SimpleAdinaAdapter();
        virtual ~SimpleAdinaAdapter();

        // Must be used to omit ambiguous calls of GetState
        using con::ContractItemDispatcher<pro::VelocityWithDerivatives>::GetState ;
        using con::ContractItemDispatcher<pro::TemperatureWithDerivatives>::GetState;
        using con::ContractItemDispatcher<pro::Coordinates>::GetState;
        using con::ContractItemDispatcher<pro::Time>::GetState;
        using con::ContractItemDispatcher<pro::Viscosity>::GetState;
        using con::ContractItemDispatcher<exa::AgglomerationFactor>::GetState;
        using con::ContractItemDispatcher<pro::Cp>::GetState;
        using con::ContractItemDispatcher<pro::Cv>::GetState;
        using con::ContractItemDispatcher<pro::ThermalConductivity>::GetState;
        using con::ContractItemDispatcher<pro::Density>::GetState;

        //! \name "Dump" methods to provide full AdinaFContract
        //@{
        virtual pro::CoeffVolumeExpansion GetState(com::Point* point, const pro::CoeffVolumeExpansion* const p){return con::ZeroValue(p);}
        virtual pro::ReferenceTemperature GetState(com::Point* point, const pro::ReferenceTemperature* const p){return con::ZeroValue(p);}
        virtual pro::Gravitation GetState(com::Point* point, const pro::Gravitation* const p){return con::ZeroValue(p);}
        virtual pro::HeatPerVolume GetState(com::Point* point, const pro::HeatPerVolume* const p){return con::ZeroValue(p);}
        virtual pro::SurfaceTension GetState(com::Point* point, const pro::SurfaceTension* const p){return con::ZeroValue(p);}
        virtual pro::BulkModulus GetState(com::Point* point, const pro::BulkModulus* const p){return con::ZeroValue(p);}
        virtual pro::Cv GetState(com::Point* point, const pro::Cv* const p){return con::ZeroValue(p);}
        //@}
        //! \name Init methods, filing I4Adapter's pointers. Pointers must be initialized before running of model
        //@{        
        int Init( am3::knowledge::I4Adapter<pro::Viscosity>* a4Adapter );
        int Init( am3::knowledge::I4Adapter<exa::AgglomerationFactor>* a4Adapter );
        int Init( def::AdinaFProvider* adinaFStorage);
        //@}
        //! \name Implementation of non-trivial (non-dump) GetState methods derived from Contracts
        //@{
        virtual pro::Density GetState(am3::model::common::Point* point, const pro::Density* const p);
        virtual pro::Cp GetState(am3::model::common::Point* point, const pro::Cp* const p);
        virtual pro::ThermalConductivity GetState(am3::model::common::Point* point, const pro::ThermalConductivity* const p);
        virtual pro::Viscosity GetState(am3::model::common::Point* point, const pro::Viscosity* const p);
        virtual pro::VelocityWithDerivatives GetState(am3::model::common::Point* point, const pro::VelocityWithDerivatives* const p);
        virtual pro::Temperature GetState(am3::model::common::Point* point, const pro::Temperature* const p);
        virtual exa::AgglomerationFactor GetState(am3::model::common::Point* point, const exa::AgglomerationFactor* const p);
        //@}
        //! \name Realizes am3::model::Model interface
        //@{
        virtual int Init(){return -1;};
        virtual int Run(){return -1;};
        virtual int PostProcess(){return -1;};
        //@}
      };
    } //adinaSimple
  } //adapter
} //am3


#endif // modelAdinaSimple_adapter_h__
