
/*! \file problemInterfaces.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief Defines inputs, necessary for fine scale models      
 * \details   	
*********************************************************************/
#ifndef problemInterfaces_h__
#define problemInterfaces_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "problemDependentDataStructures.h"

/*	using ***********************************************************/
namespace pro=am3::model;
namespace con=am3::model::contract;
/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace example1
    {
      //! Finer models needs these variables from macro model
      //!
      //! Macroscopic model have to provide these values. Finer models can ask for them
      struct ExampleMacroContract
        : public con::ContractItemDispatcher<pro::VelocityWithDerivatives>
        , public con::ContractItemDispatcher<pro::TemperatureWithDerivatives> //Try with "Temperature"
        , public con::ContractItemDispatcher<pro::Coordinates>
        , public con::ContractItemDispatcher<pro::Time>
      {virtual ~ExampleMacroContract(){}};

      //! Thixotropy is important phenomena in Example1. Model(s) for its computing must be provided
      //! 
      //! If more variables are important in one phenomena, add additional lines in this structure.
      //! If there are more phenomena, add new Contract definitions
      struct ThixotropyInternalsContract
        : public con::ContractItemDispatcher<am3::model::example1::AgglomerationFactor>
      {virtual ~ThixotropyInternalsContract(){}};

      //! To compute AgglomerationFactor, Velocity and Temperature are necessary
      //!
      //! Adapter have to derive from this class or use other class deriving from this
      struct InputsAgglomerationFactorContract
        : public con::ContractItemDispatcher<pro::VelocityWithDerivatives>
        , public con::ContractItemDispatcher<pro::Temperature>
      {
        virtual ~InputsAgglomerationFactorContract(){};
        // Compilers are stupid. Lines below are necessary for all derived class
        using con::ContractItemDispatcher<pro::VelocityWithDerivatives>::GetState;
        using con::ContractItemDispatcher<pro::Temperature>::GetState;
      };

    } //example1
  } //model
} //am3

#endif // problemInterfaces_h__
