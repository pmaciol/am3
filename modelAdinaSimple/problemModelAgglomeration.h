
/*! \file problemModelAgglomeration.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief  Definitions of fine scale model for agglomeration     
 * \details   	
*********************************************************************/
#ifndef problemModelAgglomeration_h__
#define problemModelAgglomeration_h__
/*	include *********************************************************/

#include "../models/point.h"
#include "../models/models.h"
#include "problemDependentDataStructures.h"
#include "problemInterfaces.h"

/*	using ***********************************************************/

namespace com=am3::model::common;
namespace mod=am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace example1
    {
      //! One (presently only one) of models of agglomeration factor.
      //!
      //! Fine model class must implement am3::model::Model and chosen Provider interfaces
      class modelAgglomeration1
        : public mod::Model
        , public AgglomerationFactorProvider
      {
      public:
        virtual ~modelAgglomeration1();
        //! \name Realizes am3::model::contract::Provider (s) interface
        //@{
        virtual AgglomerationFactor GetState(com::Point* point,const AgglomerationFactor* const);
        //@}
        //! \name Realizes am3::model::Model interface
        //@{
        virtual int Init(){return -1;};
        virtual int Run(){return -1;};
        virtual int PostProcess(){return -1;};
        //@}
        //! Initialize adapter pointer
        virtual int Init(InputsAgglomerationFactorContract* input); 
      protected:
        //! Pointer to adpater, providing necessary inputs
        InputsAgglomerationFactorContract* input_;  
      };
    } //example1
  } //model
} //am3

#endif // problemModelAgglomeration_h__
