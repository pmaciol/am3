#include "adapter.h"
#include "problemModelAgglomeration.h"
#include "problemModelViscosity.h"
#include "../logger/logger.h"

namespace lo=am3::logger;

pro::Viscosity am3::adapter::adinaSimple::SimpleAdinaAdapter::GetState( am3::model::common::Point* point, const pro::Viscosity* const p )
{
  lo::Starting("Finding viscosity by SimpleAdinaAdapter");
  // Computed by fine model
  pro::Viscosity visc;
  am3::model::ModelID mId=this->viscosityModelSelector_->GetState(point,&visc);
  visc = viscosity_models_[mId()]->GetState(point,&visc);
  lo::Stopping("Finding viscosity by SimpleAdinaAdapter");
  return visc;
}

exa::AgglomerationFactor am3::adapter::adinaSimple::SimpleAdinaAdapter::GetState( am3::model::common::Point* point, const exa::AgglomerationFactor* const p )
{
  lo::Starting("Finding agglomeration by SimpleAdinaAdapter");
  // Computed by fine model
  exa::AgglomerationFactor aggF;
  am3::model::ModelID mId=this->agglomerationModelSelector_->GetState(point,&aggF);
  aggF = agglomeration_models_[mId()]->GetState(point,&aggF);
  lo::Stopping("Finding agglomeration by SimpleAdinaAdapter");
  return aggF;
}

pro::VelocityWithDerivatives am3::adapter::adinaSimple::SimpleAdinaAdapter::GetState( am3::model::common::Point* point, const pro::VelocityWithDerivatives* const p )
{
  lo::Starting("Finding velocity by SimpleAdinaAdapter");
  // Computed by macro model, get from storage
  pro::VelocityWithDerivatives vel;
  // Getting from storage
  vel=adinaFStorage_->GetState(point,&vel);
  lo::Stopping("Finding velocity by SimpleAdinaAdapter");
  return vel;
}

pro::Temperature am3::adapter::adinaSimple::SimpleAdinaAdapter::GetState( am3::model::common::Point* point, const pro::Temperature* const p )
{
  lo::Starting("Finding temperature by SimpleAdinaAdapter");
  // Computed by macro model, get from storage
  pro::TemperatureWithDerivatives tempwD;
  pro::Temperature temp;
  temp=adinaFStorage_->GetState(point,&tempwD);
  lo::Stopping("Finding temperature by SimpleAdinaAdapter");
  return temp;
}

pro::Cp am3::adapter::adinaSimple::SimpleAdinaAdapter::GetState( am3::model::common::Point* point, const pro::Cp* const p )
{
  lo::Starting("Finding cp by SimpleAdinaAdapter");
  // Constant
  lo::Stopping("Finding cp by SimpleAdinaAdapter");
  return this->material_properties_.GetState(point,p);
}

pro::ThermalConductivity am3::adapter::adinaSimple::SimpleAdinaAdapter::GetState( am3::model::common::Point* point, const pro::ThermalConductivity* const p )
{
  lo::Starting("Finding tc by SimpleAdinaAdapter");
  // Constant
  lo::Stopping("Finding tc by SimpleAdinaAdapter");
  return this->material_properties_.GetState(point,p);
}

pro::Density am3::adapter::adinaSimple::SimpleAdinaAdapter::GetState( am3::model::common::Point* point, const pro::Density* const p )
{
  lo::Starting("Finding density by SimpleAdinaAdapter");
  // Constant
  lo::Stopping("Finding density by SimpleAdinaAdapter");
  return this->material_properties_.GetState(point,p);
}

int am3::adapter::adinaSimple::SimpleAdinaAdapter::Init( am3::knowledge::I4Adapter<pro::Viscosity>* a4Adapter )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (viscosity adapter)");
  int isSuccess=false;
  if (viscosityModelSelector_!=nullptr)
  {
    viscosityModelSelector_=a4Adapter;
    isSuccess=1;
  }
  lo::Stopping("Initialization of SimpleAdinaAdapter (viscosity adapter)");
  return isSuccess;
}

int am3::adapter::adinaSimple::SimpleAdinaAdapter::Init( am3::knowledge::I4Adapter<exa::AgglomerationFactor>* a4Adapter )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (agglomeration adapter)");
  int isSuccess=false;
  if (agglomerationModelSelector_!=nullptr)
  {
    agglomerationModelSelector_=a4Adapter;
    isSuccess=1;
  }
  lo::Stopping("Initialization of SimpleAdinaAdapter (agglomeration adapter)");
  return isSuccess;
}

int am3::adapter::adinaSimple::SimpleAdinaAdapter::Init( def::AdinaFProvider* adinaFStorage )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (Adina-CFD storage)");
  int isSuccess=false;
  if (adinaFStorage!=nullptr)
  {
    adinaFStorage_=adinaFStorage;
    isSuccess=1;
  }
  lo::Stopping("Initialization of SimpleAdinaAdapter (Adina-CFD storage)");
  return isSuccess;
}

am3::adapter::adinaSimple::SimpleAdinaAdapter::SimpleAdinaAdapter()
{
  lo::Starting("SimpleAdinaAdapter constructor");
  //Create new models
  am3::model::example1::modelAgglomeration1* agg=new am3::model::example1::modelAgglomeration1;
  am3::model::example1::ThixotropyViscositySolidState* visc=new am3::model::example1::ThixotropyViscositySolidState;
  am3::model::example1::ThixotropyViscositySemiSolidState* visc2=new am3::model::example1::ThixotropyViscositySemiSolidState;
  am3::model::example1::ThixotropyViscosityLiquidState* visc3=new am3::model::example1::ThixotropyViscosityLiquidState;
  am3::model::example1::ThixotropyViscositySemiSolidStateSimple* visc4=new am3::model::example1::ThixotropyViscositySemiSolidStateSimple;
  //Initialize new models
  agg->Init(this);
  visc->Init(this,this);
  visc2->Init(this,this);
  visc3->Init(this,this);
  visc4->Init(this,this);
  //Add models to containers
  agglomeration_models_.push_back(agg);
  viscosity_models_.push_back(visc);
  viscosity_models_.push_back(visc2);
  viscosity_models_.push_back(visc3);
  viscosity_models_.push_back(visc4);


  //If more models, repeat
  lo::Stopping("SimpleAdinaAdapter constructor");
}

am3::adapter::adinaSimple::SimpleAdinaAdapter::~SimpleAdinaAdapter()
{
  lo::Starting("SimpleAdinaAdapter destructor");
  vector<def::ViscProvider*>::iterator itVisc;
  itVisc=viscosity_models_.begin();
  for (itVisc=viscosity_models_.begin();itVisc<viscosity_models_.end();itVisc++) delete (*itVisc);
  vector<exa::AgglomerationFactorProvider*>::iterator itAgg;
  itAgg=agglomeration_models_.begin();
  for (itAgg=agglomeration_models_.begin();itAgg<agglomeration_models_.end();itAgg++) delete (*itAgg);
  lo::Stopping("SimpleAdinaAdapter destructor");
}

