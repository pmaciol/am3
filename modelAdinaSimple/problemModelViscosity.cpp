#include "problemModelViscosity.h"
#include "../logger/logger.h"

namespace com=am3::model::common;
namespace lo=am3::logger;

pro::Viscosity am3::model::example1::ThixotropyViscositySolidState::GetState
  ( com::Point* point,const pro::Viscosity* const )
{
  lo::Starting("modelViscosity1");
  pro::Viscosity toReturn;
  // If you need any inputs, ask for them, like below (only variables from defined inputs are available)
  AgglomerationFactor aggF;
  //aggF=inputAgglomeration_->GetState(point,&aggF);
  inputAgglomeration_->GetState(point,&aggF);
  aggF(0.5);
  am3::model::Temperature temp;
  temp=inputTemperature_->GetState(point,&temp);
  // Here compute returned value - viscosity in this case. Usually here is a call to external procedures
  toReturn(1);
  //And finally return computed value
  lo::Stopping("modelViscosity1");
  return toReturn;
}


int am3::model::example1::ThixotropyViscositySolidState::Init(
  con::ContractItemDispatcher<am3::model::example1::AgglomerationFactor>* inputAgglomeration
  ,con::ContractItemDispatcher<pro::Temperature>* inputTemperature)
{
  lo::Starting("modelViscosity1 initialization");
  inputAgglomeration_=inputAgglomeration;
  inputTemperature_=inputTemperature;
  lo::Stopping("modelViscosity1 initialization");
  return 1;
}



am3::model::example1::ThixotropyViscositySolidState::~ThixotropyViscositySolidState()
{
  lo::Starting("modelViscosity1 destructor");
  if (inputAgglomeration_!=nullptr) delete inputAgglomeration_;
  inputAgglomeration_=nullptr;
  if (inputTemperature_!=nullptr) delete inputTemperature_;
  inputTemperature_=nullptr;
  lo::Stopping("modelViscosity1 destructor");
}

def::ViscProvider* am3::model::example1::ThixotropyViscositySolidState::clone() const
{
  am3::model::example1::ThixotropyViscositySolidState* toReturn = new am3::model::example1::ThixotropyViscositySolidState;
  toReturn->Init(inputAgglomeration_,inputTemperature_);
  toReturn->Init();
  return toReturn;
}

pro::Viscosity am3::model::example1::ThixotropyViscositySemiSolidState::GetState( com::Point* point,const pro::Viscosity* const )
{
  lo::Starting("modelViscosity2");
  pro::Viscosity toReturn;
  // If you need any inputs, ask for them, like below (only variables from defined inputs are available)
  AgglomerationFactor aggF;
  aggF=inputAgglomeration_->GetState(point,&aggF);
  am3::model::Temperature temp;
  temp=inputTemperature_->GetState(point,&temp);
  // Here compute returned value - viscosity in this case. Usually here is a call to external procedures
  toReturn(0.0001+(1100.0-temp())*0.003);
  //toReturn(0.05);
  //And finally return computed value
  lo::Stopping("modelViscosity2");
  return toReturn;
}

pro::Viscosity am3::model::example1::ThixotropyViscosityLiquidState::GetState( com::Point* point,const pro::Viscosity* const )
{
  lo::Starting("modelViscosity3");
  pro::Viscosity toReturn;
  // If you need any inputs, ask for them, like below (only variables from defined inputs are available)
  AgglomerationFactor aggF;
  aggF=inputAgglomeration_->GetState(point,&aggF);
  am3::model::Temperature temp;
  temp=inputTemperature_->GetState(point,&temp);
  // Here compute returned value - viscosity in this case. Usually here is a call to external procedures
  toReturn(0.0001);
  //And finally return computed value
  lo::Stopping("modelViscosity3");
  return toReturn;
}

pro::Viscosity am3::model::example1::ThixotropyViscositySemiSolidStateSimple::GetState( com::Point* point,const pro::Viscosity* const )
{
  lo::Starting("modelViscosity4");
  pro::Viscosity toReturn;
  toReturn(0.05);
  //And finally return computed value
  lo::Stopping("modelViscosity4");
  return toReturn;
}
