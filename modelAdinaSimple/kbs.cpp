#include <string>
#include <boost/lexical_cast.hpp>
#include "../common/literals.h"
#include "kbs.h"
#include "../logger/logger.h"

namespace lo=am3::logger;
using std::string;
using boost::lexical_cast;


void am3::knowledge::example1::Example1KBS::Init(kno::I4KBS<VariableDTO>* provider)
  //void am3::knowledge::rebit::MockRebitKBS::Init( am3::knowledge::I4KBS<am3::knowledge::StringPropertyDescriptor>* provider )
{
  provider_=provider;
}


bool am3::knowledge::example1::Example1KBS::Get(VariableDTO* mId, const VariableDTO& typeTrait)
{
  bool isSuccess=false;
  if ( mId->Id->compare(kModelId)==0)
  {
    if (typeTrait.Id->compare(kViscosity)==0)
    {
      mId->Value->push_back(lexical_cast<string>(GetModelIdViscosity()));
      isSuccess=true;
    } 
    else if (typeTrait.Id->compare(kAgglomerationFactor)==0)
    {
      mId->Value->push_back(lexical_cast<string>(GetModelIdAgglomeration()));
      isSuccess=true;
    } 
    else 
    {
      mId->Value->push_back("-1");
      isSuccess=false;
    }
    //Tu ma pyta� REBITA, !!!!!!
    lo::TestingMsg("I'AM A KBS!");
    
  } 
  return isSuccess;
}

// am3::knowledge::example1::Example1KBS::~Example1KBS()
// {
// 
// }

int am3::knowledge::example1::Example1KBS::GetModelIdViscosity()
{
  VariableDTO v1,v2;
  v1.Id->assign(kTemperature);
  provider_->Fetch(&v1);
  v2.Id->assign(kVelocityWithDerivatives);
  provider_->Fetch(&v2);
  int tempModelId=-1;
  double justForDebug=(lexical_cast<double>(v1.Value->at(0))<800.0);
   if (justForDebug)
//  if (lexical_cast<double>(v1.Value->at(0))<800.0)
  {
    tempModelId=kThixotropySolidState;
  } 
  else if (lexical_cast<double>(v1.Value->at(0))<1100.0)
  {
    tempModelId=kThixotropySemiSolidState;
    //tempModelId=3;
  }
  else tempModelId=kThixotropyLiquidState;
  return tempModelId;
}

int am3::knowledge::example1::Example1KBS::GetModelIdAgglomeration()
{
  return 0;
}

