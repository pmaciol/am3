/*! \file kbs.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief Exemplary, simple KBS definded
 * \details   	
*********************************************************************/
#ifndef modelAdinaSimple_kbs_h__
#define modelAdinaSimple_kbs_h__
/*	include *********************************************************/

#include <math.h>
#include "../knowledge/kbsInterfaces.h"
#include "../knowledgeRebit/rebitKnowledgeStructs.h"
#include "../knowledge/basicKbsInterfaces.h"
#include "../modelAdinaSimple/problemDependentDataStructures.h"
#include "../common/literals.h"


/*	using ***********************************************************/

namespace exa=am3::model::example1;
namespace kno=am3::knowledge;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace knowledge
  {
    namespace example1
    {
      //! COnverter dedicated for Example1
      template<typename ADAPTER>
      struct Example1GeneralRebit : public ConverterGeneralRebit<ADAPTER>
      {
        virtual ~Example1GeneralRebit(){};
        // Some methods derived from ConverterGeneralRebit
        using ConverterGeneralRebit<ADAPTER>::Convert;
        //! Converting method for Example1 specific types
        static VariableDTO Convert(const exa::AgglomerationFactor& aggF);
        //! Redefinition of Fetch(). All variables, which KBS can ask
        virtual bool Fetch(VariableDTO* property);
      };

      /** \addtogroup Example1*/
      /**@{*/
      //! Replace real Rebit interface. Used for tests and as an example
      struct Example1KBS
      {
        typedef VariableDTO Property;
        //void Init(kno::I4KBS<kno::StringPropertyDescriptor>* provider);
        void Init(kno::I4KBS<VariableDTO>* provider);
        //! Interface for asking about property value 
        bool Get(VariableDTO* mId, const VariableDTO& typeTrait);
        //virtual ~Example1KBS();
      protected:
        // 
        int GetModelIdViscosity();
        int GetModelIdAgglomeration();
        kno::I4KBS<VariableDTO>* provider_;
      };
      /**@}*/
    } //rebit  
  } //knowledge
} //am3


template<typename ADAPTER>
VariableDTO am3::knowledge::example1::Example1GeneralRebit<ADAPTER>::Convert( const exa::AgglomerationFactor& aggF )
{
  VariableDTO toReturn;
  toReturn.Id->assign(kAgglomerationFactor);
  toReturn.Value->push_back(lexical_cast<string>(aggF()));
  return toReturn;
}

template<typename ADAPTER>
bool am3::knowledge::example1::Example1GeneralRebit<ADAPTER>::Fetch( VariableDTO* property )
{
  bool isSuccess=false;
  isSuccess=ConverterGeneralRebit<ADAPTER>::Fetch(property);
  if (!isSuccess)
  {
    com::Point point;
    isSuccess=true;
    if (property->Id->compare(kVelocityWithDerivatives)==0)
    {
      am3::model::VelocityWithDerivatives vel;
      vel=adapter_->GetState(&point,&vel);
      property->Value->push_back(lexical_cast<string>
        (sqrt(pow(vel.x(),2)+pow(vel.y(),2)+pow(vel.z(),2))));

    }
    else if (property->Id->compare(kTemperature)==0)
    {
      am3::model::Temperature temp;
      temp=adapter_->GetState(&point,&temp);
      property->Value->push_back(lexical_cast<string>(temp()));
    }
    else if (property->Id->compare(kDensity)==0)
    {
      am3::model::Density temp;
      temp=adapter_->GetState(&point,&temp);
      property->Value->push_back(lexical_cast<string>(temp()));
    }
    else
    {
      isSuccess=false;
    }
  }

  return isSuccess; //Trzeba pyta� Proxy - brakuje interfejsu!!!!!!
}


#endif // modelAdinaSimple_kbs_h__
