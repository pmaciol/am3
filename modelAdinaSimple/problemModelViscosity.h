
/*! \file problemModelViscosity.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief  Definitions of fine scale model for viscosity         
 * \details   	
*********************************************************************/
#ifndef modelAdinaSimple_problemModelViscosity_h__
#define modelAdinaSimple_problemModelViscosity_h__
/*	include *********************************************************/

#include "../models/point.h"
#include "../models/models.h"
#include "../models/contracts.h"
#include "problemDependentDataStructures.h"

/*	using ***********************************************************/

namespace com=am3::model::common;
namespace mod=am3::model;
namespace def=am3::model::contract::defined;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace example1
    {
      class ThixotropyViscositySolidState
        : public mod::Model
        , public def::ViscProvider
      {
      public:
        virtual ~ThixotropyViscositySolidState();
        //! \name Realizes am3::model::contract::Provider (s) interface
        //@{
        virtual pro::Viscosity GetState(com::Point* point,const pro::Viscosity* const);
        //@}
        //! \name Realizes am3::model::Model interface
        //@{
        virtual int Init(){return -1;};
        virtual int Run(){return -1;};
        virtual int PostProcess(){return -1;};
        //@}
        //! Initialize adapter pointer
        //!
        //! It is not necessary to use predefined Providers set, like in agglomeration model. 
        //! In case of single input, direct ContractItemDispatcher can be used.
        virtual int Init(con::ContractItemDispatcher<AgglomerationFactor>* inputAgglomeration,
                         con::ContractItemDispatcher<pro::Temperature>* inputTemperature); 
        virtual ViscProvider* clone() const;
      protected:
        //! Pointer to adpapter, providing necessary input
        con::ContractItemDispatcher<AgglomerationFactor>* inputAgglomeration_;
        con::ContractItemDispatcher<pro::Temperature>* inputTemperature_;
      };
      //////////////////////////////////////////////////////////////////////////
      class ThixotropyViscositySemiSolidState
        : public ThixotropyViscositySolidState
      {
      public:
        virtual ~ThixotropyViscositySemiSolidState(){};
        virtual pro::Viscosity GetState(com::Point* point,const pro::Viscosity* const);
      };
      //////////////////////////////////////////////////////////////////////////
      class ThixotropyViscositySemiSolidStateSimple
        : public ThixotropyViscositySolidState
      {
      public:
        virtual ~ThixotropyViscositySemiSolidStateSimple(){};
        virtual pro::Viscosity GetState(com::Point* point,const pro::Viscosity* const);
      };
      //////////////////////////////////////////////////////////////////////////
      class ThixotropyViscosityLiquidState
        : public ThixotropyViscositySolidState
      {
      public:
        virtual ~ThixotropyViscosityLiquidState(){};
        virtual pro::Viscosity GetState(com::Point* point,const pro::Viscosity* const);
      };


} //example1
  } //model
} //am3

#endif // modelAdinaSimple_problemModelViscosity_h__
