
/*! \file problemModelProperties.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelAdinaSimple_problemModelProperties_h__
#define modelAdinaSimple_problemModelProperties_h__
/*	include *********************************************************/

#include <string>
#include "../models/point.h"
#include "../models/models.h"
#include "../models/contracts.h"

/*	using ***********************************************************/

namespace com=am3::model::common;
namespace mod=am3::model;
namespace con=am3::model::contract;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace example1
    {
      class ConstMaterialProperties
        : public mod::Model
        , public con::Provider<pro::Cp>
        , public con::Provider<pro::Density>
        , public con::Provider<pro::ThermalConductivity>
        , public con::Provider<pro::MaterialName>
      {
      public:
        ConstMaterialProperties()
        {
          cp_(420.0);
          thermalConductivity_(20.0);
          density_(7000.0);
          density_.dp(0.0);
          density_.dt(0.0);
          materialName_("steel");
        };
        virtual ~ConstMaterialProperties(){};
        //! \name Realizes am3::model::contract::Provider (s) interface
        //@{
        virtual pro::Cp GetState(com::Point* point,const pro::Cp* const){return cp_;};
        virtual pro::Density GetState(com::Point* point,const pro::Density* const){return density_;};
        virtual pro::ThermalConductivity GetState(com::Point* point,const pro::ThermalConductivity* const){return thermalConductivity_;};
        virtual pro::MaterialName GetState(com::Point* point,const pro::MaterialName* const){return materialName_;};
        //@}
        //! \name Realizes am3::model::Model interface
        //@{
        virtual int Init(){return -1;};
        virtual int Run(){return -1;};
        virtual int PostProcess(){return -1;};
        //@}
//         //! Initialize adapter pointer
//         //!
//         //! It is not necessary to use predefined Providers set, like in agglomeration model. 
//         //! In case of single input, direct ContractItemDispatcher can be used.
//         virtual int Init(con::ContractItemDispatcher<AgglomerationFactor>* inputAgglomeration,
//           con::ContractItemDispatcher<pro::Temperature>* inputTemperature); 
      protected:
        pro::MaterialName materialName_;
        pro::Cp cp_;
        pro::ThermalConductivity thermalConductivity_;
        pro::Density density_;
      };
    }
  }
}
#endif // modelAdinaSimple_problemModelProperties_h__
