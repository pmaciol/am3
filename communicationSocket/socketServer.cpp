/*! \file socketServer.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief Implementation of socket connection. General for all models      
 * \details   	
*********************************************************************/
/*	include *********************************************************/















// REMOVE THIS PROJECT AFTER DEFORM MOVING TO COMMUNICATION PROVIDER!!!
















#include "socketServer.h"

#if defined(_WIN32) || defined(_WIN64)
#pragma warning( default : 4996)
#endif

#include <winsock.h>
#include <string>
#include <vector>
#include <assert.h>

#include <boost/algorithm/string.hpp>

#include <boost/lexical_cast.hpp>

#include "../logger/logger.h"




/*	using ***********************************************************/

using std::string;
using std::vector;
namespace lo=am3::logger;
//using namespace std; - causing conflicts with winsock

/*	extern **********************************************************/

#if !defined(_WINSOCK2API_) 
// Winsock 2 header defines this, but Winsock 1.1 header doesn't.  In
// the interest of not requiring the Winsock 2 SDK which we don't really
// need, we'll just define this one constant ourselves.
#define SD_SEND 1
#endif

/*	classes *********************************************************/

const int am3::common::communication::SocketServer::kBufferSize=2048;

//////////////////////////////////////////////////////////////////////////

void am3::common::communication::SocketServer::Communicate()
{
  // Start Winsock up
  WSAData wsaData;
  int nCode;
  if ((nCode = WSAStartup(MAKEWORD(1, 1), &wsaData)) != 0) lo::log<lo::SEV_CRITICAL>(lo::LOG_COMMON,"WSAStartup() returned error code "+ nCode);
  else    DoWinsock(address_.c_str(), port_);
   // Shut Winsock back down and take off.
  WSACleanup();
}

void am3::common::communication::SocketServer::DoWinsock( const char* pcAddress, int nPort )
{
  lo::log<lo::SEV_NOTICE>(lo::LOG_COMMON,"Starting socket connection");
  SOCKET ListeningSocket = SetUpListener(pcAddress, htons(nPort));
  if (ListeningSocket == INVALID_SOCKET) 
  {
    lo::log<lo::SEV_CRITICAL>(lo::LOG_COMMON,"Socket connection failed");
  }
  else
  {
    while (1) //! \todo Stop condition should be added - possible only in multi-threaded version?
    {
      // Wait for a connection, and accepting it when one arrives.
      sockaddr_in sinRemote;
      SOCKET sd = AcceptConnection(ListeningSocket, sinRemote);
      if (sd != INVALID_SOCKET) 
      {
        lo::log<lo::SEV_DEBUG>(lo::LOG_COMMON,"Accepted connection");
        DoCallProcessing(sd);
        ShutdownConnection(sd);
      }
      else 
      {
        lo::log<lo::SEV_CRITICAL>(lo::LOG_COMMON,"Socket connection failed");          
      }
    }
  }
}

SOCKET am3::common::communication::SocketServer::SetUpListener( const char* pcAddress, int nPort )
{
  u_long nInterfaceAddr = inet_addr(pcAddress);
  if (nInterfaceAddr != INADDR_NONE) {
    SOCKET sd = socket(AF_INET, SOCK_STREAM, 0);
    if (sd != INVALID_SOCKET) {
      sockaddr_in sinInterface;
      sinInterface.sin_family = AF_INET;
      sinInterface.sin_addr.s_addr = nInterfaceAddr;
      sinInterface.sin_port = nPort;
      if (bind(sd, (sockaddr*)&sinInterface, sizeof(sockaddr_in)) != SOCKET_ERROR) {
        listen(sd, 1);
        return sd;
      }
    }
  }

  return INVALID_SOCKET;
}

SOCKET am3::common::communication::SocketServer::AcceptConnection( SOCKET ListeningSocket, sockaddr_in& sinRemote )
{
  int nAddrSize = sizeof(sinRemote);
  return accept(ListeningSocket, (sockaddr*)&sinRemote, &nAddrSize);
}

void am3::common::communication::SocketServer::ShutdownConnection( SOCKET sd )
{
  if (shutdown(sd, SD_SEND) == SOCKET_ERROR) lo::log<lo::SEV_CRITICAL>(lo::LOG_COMMON,"Socket shutdown error");
  char acReadBuffer[kBufferSize];
  while (1) {
    int nNewBytes = recv(sd, acReadBuffer, kBufferSize, 0);
    if (nNewBytes == SOCKET_ERROR) lo::log<lo::SEV_CRITICAL>(lo::LOG_COMMON,"Socket error");
    else if (nNewBytes != 0) lo::log<lo::SEV_CRITICAL>(lo::LOG_COMMON,(string("FYI, received ") + boost::lexical_cast<string>(nNewBytes) + string(" unexpected bytes during shutdown.")).c_str());
  }
  if (closesocket(sd) == SOCKET_ERROR)lo::log<lo::SEV_CRITICAL>(lo::LOG_COMMON,"Socket shutdown error");
}

bool am3::common::communication::SocketServer::isSocketInitialized()
{
  if (callProcessor_.use_count()==0) 
  {
    lo::log<lo::SEV_CRITICAL>(lo::LOG_COMMON,"Socket calls processor not initialized");
    return false;
  }
  else return true;
}

void am3::common::communication::SocketServer::DoCallProcessing( SOCKET sd )
{
  char acReadBuffer[kBufferSize];
  char sendBuffer[kBufferSize];
  std::string receivedString;
  std::vector<std::string> splittedStringsFromSocket;
  std::vector<std::string> splittedStringsFromModel;


  while(1)
  {
    memset(acReadBuffer,0,kBufferSize);
    int nTotalBytes = 0;
    do 
    { 
      int nNewBytes = recv(sd, acReadBuffer + nTotalBytes, kBufferSize - nTotalBytes, 0);
      if (nNewBytes == SOCKET_ERROR) lo::log<lo::SEV_CRITICAL>(lo::LOG_COMMON,"Socket receiving error");
      else if (nNewBytes == 0) lo::log<lo::SEV_NOTICE>(lo::LOG_COMMON,"Connection close by peer");
      nTotalBytes += nNewBytes;
    } 
    while (acReadBuffer[nTotalBytes-1] != 'T');

    receivedString = acReadBuffer;
    boost::split(splittedStringsFromSocket, receivedString, boost::is_any_of("DIT"));

    if (isSocketInitialized())
    {
      splittedStringsFromModel=callProcessor_->ProcessExternalCall(splittedStringsFromSocket);
    }
    memset(sendBuffer,0,kBufferSize);
//    strcpy(sendBuffer,splittedStringsFromModel[0].c_str());
        strcpy(sendBuffer,"D");
    vector<string>::iterator it=splittedStringsFromModel.begin();
   // ++it;
    for (it;it<splittedStringsFromModel.end();it++)
    {
      strcat(sendBuffer,"D");
      strcat(sendBuffer,it->c_str());
    }
    strcat(sendBuffer,"T");
    assert(sizeof(int)<=sizeof(size_t)); //casting size_t to int in next line
    if (send(sd, sendBuffer, (int)strlen(sendBuffer), 0) == SOCKET_ERROR) 
      lo::log<lo::SEV_CRITICAL>(lo::LOG_COMMON,"Socket sending error");
  }
}

po::options_description& am3::common::communication::SocketServer::DefineOptionsForModule()
{
  po::options_description& od=kProgramOptions.GetNewProgramOptions("Socket server configuration");
  od.add_options()
    ("SocketAdress", po::value< std::string >()->default_value("0.0.0.0") , "Socket server address")
    ("SocketPort", po::value<int>()->default_value(22564), "Socket server port")
    ; 
  kProgramOptions.AddToAllNewProgramOptions(od);
  return od;
}


void am3::common::communication::SocketServer::CopyReadOptionsToInternal()
{
  address_=((*module_variable_map)["SocketAdress"].as<string>());
  port_=((*module_variable_map)["SocketPort"].as<int>());
}

std::string am3::common::communication::SocketServer::GetModuleName()
{
  return ("socketServer");
}


