
/*! \file socketServer.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef communicationSocket_socketServer_h__
#define communicationSocket_socketServer_h__
/*	include *********************************************************/

#if defined(_WIN32) || defined(_WIN64)
#pragma warning( disable : 4996)
#endif

#include <string>
#include <vector>
#include <winsock.h>

#include <boost/shared_ptr.hpp>
#include <boost/program_options.hpp>
#include "../common/config.h"

#if defined(_WIN32) || defined(_WIN64)
#pragma warning( default : 4996)
#endif

#include "callsProcessor.h"

/*	using ***********************************************************/

namespace cfg=am3::common;
namespace po = boost::program_options;

/*	extern **********************************************************/








// REMOVE THIS PROJECT AFTER DEFORM MOVING TO COMMUNICATION PROVIDER!!!









/*	classes *********************************************************/

namespace am3
{
  namespace common
  {
    namespace communication
    {
        class CallProcessor
        {
        public:
          virtual std::vector<std::string> ProcessExternalCall(const std::vector<std::string>& cameFromSocket)=0; 
        };

        class SocketServer : public cfg::ConfigurableModule
        {
        public:
          SocketServer(boost::shared_ptr<CallProcessor> callProcessor): callProcessor_(callProcessor){};
          void Communicate();
        protected:
          string address_;
          int port_;
          static const int kBufferSize;
          boost::shared_ptr<CallProcessor> callProcessor_;

          void DoWinsock(const char* pcAddress, int nPort);
          SOCKET SetUpListener(const char* pcAddress, int nPort);
          SOCKET AcceptConnection(SOCKET ListeningSocket, sockaddr_in& sinRemote);
          void DoCallProcessing(SOCKET sd);
          void ShutdownConnection(SOCKET sd);

          bool isSocketInitialized();

          virtual po::options_description& DefineOptionsForModule();
          virtual void CopyReadOptionsToInternal();
          virtual std::string GetModuleName();
        };
    } //communication
  } //common
} //am3



#endif // communicationSocket_socketServer_h__
