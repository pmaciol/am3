
/*! \file modelStress.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseCreepModelStress_h__
#define caseCreepModelStress_h__
/*	include *********************************************************/

#include "caseCreep.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace creep
    {
      class StressModel
        : public DeformStressContract
        , public mo::Inputs<StressInputContract>
//        , mo::ComputationsOnFirstCall<mpr::Coordinates2D>
      {
      public:
        mpr::YieldStress GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::YieldStress *const ) 
        {
          mpr::PrecipitationsMeanRadius pmr = Get<mpr::PrecipitationsMeanRadius,BaseCoordinates>::InPoint(point);
          mpr::YieldStress toRet; toRet(0.1);return toRet;
        };
        mpr::StressDStrain GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressDStrain *const ) {mpr::StressDStrain toRet; toRet(0.0);return toRet;};
        mpr::StressDStrainRate GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressDStrainRate *const ) {mpr::StressDStrainRate toRet; toRet(0.0);return toRet;};
      };
    } //creep
  } //cases
} //am3
#endif // caseCreepModelStress_h__
