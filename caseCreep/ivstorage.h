
/*! \file ivstorage.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseCreepivstorage_h__
#define caseCreepivstorage_h__
/*	include *********************************************************/

#include "../adapter/ivstorage.h"
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "caseCreep.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace ccp = am3::cases::creep;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace adapter
	{
//     template <>
//     struct VariableID<ppm::contract::DislDensityMobile>
//     {
//       static unsigned int id = 0;
//     }
// 
//     template <>
//     struct VariableID<ppm::contract::SubgrainSize>
//     {
//       static unsigned int id = 0;
//     }
// 
//     template <>
//     struct VariableID<ppm::contract::StoredEnergy>
//     {
//       static unsigned int id = 0;
//     }
// 
//     template <>
//     struct VariableID<ppm::contract::GrainSize>
//     {
//       static unsigned int id = 0;
//     }
//     template <>
//     struct VariableID<ppm::contract::NumberOfRecrystalizedNuclei>
//     {
//       static unsigned int id = 0;
//     }
//     template <>
//     struct VariableID<ppm::contract::DislDensityMobileDeformed>
//     {
//       static unsigned int id = 0;
//     }
//     template <>
//     struct VariableID<ppm::contract::RecrystalizedGrainSize>
//     {
//       static unsigned int id = 0;
//     }
//     template <>
//     struct VariableID<ppm::contract::VolumeFractionOfRecrystallizedGrains>
//     {
//       static unsigned int id = 0;
//     }

    template <template<class, class> class HANDLER,typename IV_CONTRACT>
    std::vector<double> am3::adapter::IVStorage<HANDLER, IV_CONTRACT>::GetInternalVariables( mpt::Point<mpr::Coordinates2D> *point )
    {
      std::vector<double> toRet; 
      toRet.push_back(Get<mpr::PrecipitationsMeanRadius,mpr::Coordinates2D>::InPoint(point).Value());
      return toRet;
    }
	} //adapter
} //am3
#endif // caseCreepivstorage_h__
