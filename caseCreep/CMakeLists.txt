
include_directories (../)
add_library (am3caseCreep 
caseCreep.h caseCreep.cpp
#defs.h
ivstorage.h
macroModel.h
modelStress.h
)


#if (MATCALC)
  SET(MATCALCLIB am3softwareMatCalc)
  add_definitions(-DMATCALC)
#else()
#  SET(MATCALCLIB)
#endif()

target_link_libraries(am3caseCreep
${MATCALCLIB}
)


SET_TARGET_PROPERTIES (am3caseCreep PROPERTIES LINKER_LANGUAGE CXX)
SET(CMAKE_EXE_LINKER_FLAGS /NODEFAULTLIB:\"LIBCMTD.lib;libcpmtd.lib\")