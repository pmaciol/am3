#include "caseCreep.h"
#include "../softwareDeformBuiltin/deformHandlerOutputsProviders.h"
#include "../softwareMatCalcGeneric/matCalcGeneric.h"
#include "modelStress.h"
#include <fstream>

am3::cases::creep::Main mainInstance;
extern am3::model::InitializableMainModel *m = &mainInstance;
namespace smc = am3::software::matcalc;

namespace am3
{
	namespace cases
	{
		namespace creep
		{
		//////////////////////////////////////////////////////////////////////////

      namespace ccp = am3::cases::creep;
//      namespace smc = am3::software::matcalc;
      void Main::Init()
      {
        initialized = true;

        // 
        //         // Sets Deform representation as a IV source for databus
        mainDatabus_->SetPreviousStepsStorage(caseMacroImpl_);
        // 
        //         // Creates models (basic, defined in caseVectorTug project)
        //          smc::MatCalcScriptProvider scripts;
        //          boost::shared_ptr<smc::GenericMatcalcModel> gmm(new smc::GenericMatcalcModel(scripts));
        MODEL_INSTANCE(stressModel,StressModel);
        // //           MODEL_WITH_STORAGE_INSTANCE(stressModel,StressModelWithStorage,caseMacroImpl)
        // 
        //         // Internal knowledge based system, chooses proper model
        // //             KBS_INSTANCE(kbs,Kbs,mainDatabus);
        // // 
        // //           // Switcher for dislocation models
        // //           SWITCHER_INSTANCE(dislocationSwitcher,DislocationStateContract,kbs);
        // //           ADD_MODELS_TO_SWITCHER(dislocationSwitcher,dislocationModel,dislocationModel_2);
        // // 
        // 
        //          // Links models' outputs to databus



        smc::MatCalcScriptProvider scripts;
        boost::shared_ptr<smc::GenericMatcalcModel> gmm(new smc::GenericMatcalcModel(scripts));
         DATABUS_ADD_MODELS(mainDatabus_,stressModel, gmm, caseMacroImpl_)

         // Links databus with models' inputs
         MODEL_INPUT_FROM_DATABUS(stressModel,mainDatabus_);
         MODEL_INPUT_FROM_DATABUS(gmm,mainDatabus_);
         MODEL_INPUT_FROM_DATABUS(caseMacroImpl_,mainDatabus_);

      }

			Main::Main()
			{
        std::ofstream myfile;
        myfile.open ("example.txt");
        myfile << "Writing this to a file.\n";
        myfile.close();
        initialized = false;
         typedef ccp::CaseModel CaseModel;
// 
//         // Create Databus
         DATABUS_INSTANCE(mainDatabus,CaseCreepDatabus);
         mainDatabus_ = mainDatabus;

// 
//         // Create Deform representation
         DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, mainDatabus, mainDatabus,mainDatabus,mainDatabus);
         CASE_MODEL(CaseModel,caseMacroImpl);
         caseMacroImpl_ = caseMacroImpl;
// 
//         // Links created Deform representation with global access pointer
         deformHandlerProc = caseMacroImpl->GetHandler().get();
         deformHandlerProcBoost = caseMacroImpl->GetHandler();

			}

      bool Main::IsInitialized() const
      {
        return initialized;
      }

		}
	}
}