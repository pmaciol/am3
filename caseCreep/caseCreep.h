
/*! \file caseColdRolling.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseCreep_h__
#define caseCreep_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../databus/databus.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "../adapter/ivstorage.h"
//#include "../caseSimplified/macroModel.h"
//#include "defs.h"
#include "../models/macrosSimplifiedModels.h"
//#include "../caseColdRolling/caseColdRolling.h"
#include "../softwareMatCalc/contracts.h"


#include "../models/models.h"
#include "../modelPrecipitation/contracts.h"



namespace mpr = am3::model::properties;
namespace db = am3::databus;
namespace mo = am3::model;
namespace ad = am3::adapter;
namespace sdm = am3::software::deform;
namespace mpc = am3::model::precipitations;

extern am3::software::deform::DeformHandlerUserProc* deformHandlerProc;
boost::shared_ptr<am3::software::deform::DeformHandlerUserProc> deformHandlerProcBoost;

namespace am3
{
	namespace adapter
	{
    template <template<class, class> class HANDLER,typename IV_CONTRACT>
      std::vector<double> am3::adapter::IVStorage<HANDLER, IV_CONTRACT>::GetInternalVariables( mpt::Point<mpr::Coordinates2D> *point )
       {
      std::vector<double> toRet;
      return toRet;
    }
	} //adapter
} //am3
    

  

namespace am3
{
	namespace cases
	{
		namespace creep
		{
			using namespace mpr;

      typedef moc::Contract<TYPELIST_1(	mpr::PrecipitationsMeanRadius),mpr::Coordinates2D> DeformInput2D;
      typedef moc::Contract<TYPELIST_TUPLE_2( mpr::Temperature , mpr::Time),mpr::Coordinates2D> MacroInputsContract2D;
      
      CONTRACT(		DeformStressContract				,3,Coordinates2D, YieldStress, StressDStrain, StressDStrainRate);
      CONTRACT(		StressInputContract				  ,2,Coordinates2D, Temperature, PrecipitationsMeanRadius);
      CONTRACT(		MacroContract								,5,Coordinates2D, Temperature, EffectiveStrainRate, StepLength, Time, ElementNumber);
      //CONTRACT(		MacroContract								,4,Coordinates2D, Temperature, EffectiveStrainRate, StepLength, Time);


      typedef ad::IVStorage<sdm::DeformHandler,DeformInput2D> Storage;


      template<typename HANDLER> //, typename CONTRACT>
      class MacroModel : public am3::model::MacroModel<HANDLER, MacroContract>
      {
      public:
        typedef MacroContract CONTRACT;
        mpr::EffectiveStrainRate GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::EffectiveStrainRate *const )
        {
          return pointerToMacroHandler_->GetEffectiveStrainRateIn(*point);
        }
        mpr::Temperature GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::Temperature *const )
        {
          return pointerToMacroHandler_->GetTemperatureIn(*point);
        }
        mpr::StepLength GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::StepLength *const )
        {
          return pointerToMacroHandler_->GetStepLength();
        }
        mpr::Time GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::Time *const )
        {
          return pointerToMacroHandler_->GetTime();
        }
        mpr::ElementNumber GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::ElementNumber *const )
        {
          unsigned long id;
          return pointerToMacroHandler_->GetElementId(id);
        }
      };

      typedef MacroModel<sdm::DeformHandler<mpr::Coordinates2D, Storage> > MacroModelDeform;
      typedef mo::CaseMacroImplementation<MacroModelDeform,Storage> CaseModel;

      //typedef MacroModel<sdm::DeformHandler<mpr::Coordinates2D, am3::cases::simpified::Storage>,ppm::contract::MacroInputsContract2D> MacroModelDeform;
//       typedef MacroModel<sdm::DeformHandler<mpr::Coordinates2D, Storage> > MacroModelDeform;
//       typedef mo::CaseMacroImplementation<MacroModelDeform,Storage> CaseModel;
// 
// 
//       typedef am3::cases::creep::MacroModel<sdm::DeformHandler<mpr::Coordinates2D, am3::cases::creep::Storage>,MacroInputsContract2D> MacroModelDeform;
//       typedef mo::CaseMacroImplementation<MacroModelDeform,Storage> CaseModel;
// 
//      
// 			#define MacroContractLength 3
// 			#define StressContractLength 1
// 
 			
// 
       using mpc::contract::PrecipitationsMeanRadiusContract;
//       using smc::MatCalcGenericInputContract;
// 
// 
// 			// Databus
			DATABUS(CaseCreepDatabus,Storage,
        DeformStressContract,
				MacroContract,
//				StressContract,
				PrecipitationsMeanRadiusContract);
// 
// 			// Inputs (Input values for specific models)
// // 			
//  			CONTRACT(StressInput,2,Coordinates2D,Temperature,PrecipitationsMeanRadiusContract)
// 
// 			// Models
// 
// //			MODEL(StressModel,StressContract,StressContractLength,StressInput)
// 
// 
// 
// //			DATABUS_OUTPUTS_FOR_MODELS(CaseSimplifiedDatabus,StressBySherstnyev,ShearModulusBySherstnev,DiffusionBySherstnev,ZenerParamBySherstnev)
// 
// //      typedef ad::IVStorage<sdm::DeformHandler,ppm::DeformInput2D> Storage;
// //       typedef tests::caseColdRolling::MacroModel<sdm::DeformHandler<mpr::Coordinates2D, Storage>,ppm::contract::MacroInputsContract2D> MacroModel;
// //       typedef tests::caseColdRolling::CaseMacroImplementation<MacroModel,Storage> 
// 
// 			// This case is used as a library. Constructor of Main class works like a "main" procedure in single executable solution
			class Main: public am3::model::InitializableMainModel
			{
			public:
				Main();
        bool initialized;
        void Init();

        virtual bool IsInitialized() const;

        boost::shared_ptr<CaseCreepDatabus> mainDatabus_;
        boost::shared_ptr<CaseModel> caseMacroImpl_;
			};    
		} //creep
	} //cases  
} //am3



#endif // caseCreep_h__
