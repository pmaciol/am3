
/*! \file matCalc.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_matCalc_h__
#define am3caseTugMatCalc_matCalc_h__
/*	include *********************************************************/

// #include "../communicationMPI/controller.h"
// #include "../communicationMPI/instance.h"
// #include "../models/predefinedContracts.h"
// #include "../models/point.h"
// #include "../models/dataStructures.h"
// #include "../models/inputTypelists.h"
// #include "../modelTemplates/modelsTemplates.h"
// #include "../modelTemplates/macroState.h"
#include "caseTugMatCalc.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace tugmc
    {
//       typedef std::tuple<mpr::PrecipitationsMeanRadius, mpr::PrecipitationsVolumeFraction> PrecipitationOutput;
// 
// 
// 
//        typedef cco::MpiInstance<mda::Coord2dTimeTemp, PrecipitationOutput, mpt::ComputedPoint<mpr::Coordinates2D> > Instance;
//        typedef cco::Controller<mda::Coord2dTimeTemp, PrecipitationOutput, Instance, mpt::ComputedPoint<mpr::Coordinates2D>, mpr::Coordinates2D > Controller;

      template <typename INPUT>
      class SecondPhasePrecipitationsMatCalc
        : public PrecipitationsContract
        , public mo::InputTypelist < INPUT >
      {
      protected:
//        Controller controller_;
      public:
        typedef mot::MacroTemperatureContract Inputs;

        boost::shared_ptr<DeformCaseStepPrecomputations> precomp_;

//        void Init();

        mpr::PrecipitationsMeanRadius GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::PrecipitationsMeanRadius *const){ return precomp_->Get<mpr::PrecipitationsMeanRadius>(point); };
        mpr::PrecipitationsVolumeFraction GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::PrecipitationsVolumeFraction *const){ return precomp_->Get<mpr::PrecipitationsVolumeFraction>(point); };
// 
//         template<typename VAR> VAR Get(mpt::Point<mpr::Coordinates2D> * point);
//         template<> mpr::PrecipitationsMeanRadius Get<mpr::PrecipitationsMeanRadius>(mpt::Point<mpr::Coordinates2D> * point)
//         {
//           lo::debugLog("Get PrecipitationsVolumeFraction started for point at " + std::to_string(point->Coordinates().x()) + ", " + std::to_string(point->Coordinates().y()));
//           mpt::ComputedPoint<mpr::Coordinates2D> localPoint(point);
//           mpr::Temperature temperature = bus_->GetFromDatabus<mpr::Temperature>(point);
//           mda::Coord2dTimeTemp val(localPoint.Coordinates(), localPoint.GetTime(), temperature);
// 
// 
//           std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair(localPoint, val);
//           //          DebugBreak();
//           controller_.Run(pair);
//           controller_.WaitForAll();
//           Controller::PointWithOutputDataPair outputPair = controller_.Get(localPoint);
//           mpr::PrecipitationsMeanRadius toRet;
//           Controller::OutputData od = outputPair.second;
//           toRet(std::get<0>(od));
//           //toRet(std::get<mpr::PrecipitationsMeanRadius>(outputPair.second));
//           lo::debugLog("returning PrecipitationsMeanRadius" + std::to_string(toRet()));
//           return toRet;
//         };
// 
//         template<> mpr::PrecipitationsVolumeFraction Get<mpr::PrecipitationsVolumeFraction>(mpt::Point<mpr::Coordinates2D> * point)
//         {
//           lo::debugLog("Get PrecipitationsMeanRadius started for point at " + std::to_string(point->Coordinates().x()) + ", " + std::to_string(point->Coordinates().y()));
//           mpt::ComputedPoint<mpr::Coordinates2D> localPoint(point);
//           mpr::Temperature temperature = bus_->GetFromDatabus<mpr::Temperature>(point);
//           mda::Coord2dTimeTemp val(localPoint.Coordinates(), localPoint.GetTime(), temperature);
// 
// 
//           std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair(localPoint, val);
//           //          DebugBreak();
//           controller_.Run(pair);
//           controller_.WaitForAll();
//           Controller::PointWithOutputDataPair outputPair = controller_.Get(localPoint);
//           mpr::PrecipitationsVolumeFraction toRet;
//           Controller::OutputData od = outputPair.second;
//           toRet(std::get<1>(od));
//           //toRet(std::get<mpr::PrecipitationsMeanRadius>(outputPair.second));
//           lo::debugLog("returning PrecipitationsVolumeFraction " + std::to_string(toRet()));
//           return toRet;
//         };

        //        template<> mpr::PrecipitationsVolumeFraction Get<mpr::PrecipitationsVolumeFraction>(mpt::Point<mpr::Coordinates2D> * point) { mpr::PrecipitationsVolumeFraction* trait;  return GetState(point, trait); };


      };

//       template <typename INPUT>
//       void am3::cases::tugmc::SecondPhasePrecipitationsMatCalc<INPUT>::Init()
//       {
//         controller_.Init();
//       }

    } //tugmc
  } //cases
} //am3

// inline void VectorToOutput(std::vector<double>& input, am3::cases::tugmc::PrecipitationOutput& output)
// {
//   lo::debugLog("Converting received values to internal output, only precipitation on 2 and 3");
//   std::get<0>(output)(input[2]);
//   std::cout << input[2];
//   std::get<1>(output)(input[3]);
//   std::cout << input[3];
// }
#endif // am3caseTugMatCalc_matCalc_h__
