
/*! \file dllExport.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

//#ifdef BUILD_STANDALONE_CASE

#include "dllExport.h"
#include "../softwareDeform/usr_msh.h"
#include "../softwareDeform/usr_mtr.h"
#include "../softwareDeform/usr_upd.h"
#include "caseTugMatCalc.h"


/*	using ***********************************************************/

namespace sdm = am3::software::deform;
namespace ctm = am3::cases::tugmc;

/*	classes *********************************************************/

int DEFORM_TEMPERATURES_SET(int materialId, double time, double timeStep, double * temperatures, double * deltaTemperatures, int nodeStart, int nodeEnd)
{
  sdm::DeformHandlerFacadeMshTemperature* facade;
  facade = ctm::GetDeformHandlerFacadeMshTemperatureTime();
  facade->SetTemperaturesInNodes(materialId, time, timeStep, temperatures, deltaTemperatures, nodeStart, nodeEnd);
  return 0;
}

int DEFORM_USRE1_GET(const int * length, double * values, const int * meshId, const int * elementId)
{
  lo::debugLog("DEFORM_USRE1_GET, elementId" + std::to_string(*elementId));
  sdm::DeformHandlerFacadeUpd* facade;
  facade = ctm::GetDeformHandlerFacadeUpdate();
  facade->GetElementalVariables(*length, values, *meshId, *elementId);
  return 0;
}

int DEFORM_USRE1_SEND(const int * length, const double * values, const double * temperature, const double * time, const double * timeStep, const int * meshId, const int * elementId)
{
  lo::debugLog("DEFORM_USRE1_SEND, elementId" + std::to_string(*elementId) + "; time = " + std::to_string(*time));
  sdm::DeformHandlerFacadeUpd* facade;
  facade = ctm::GetDeformHandlerFacadeUpdate();
  facade->SetElementalVariables(*length, values, *meshId, *elementId);
  facade->SetTemperature(temperature, *meshId, *elementId);
  facade->SetTime(time, *meshId, *elementId);
  facade->SetStepLength(timeStep, *meshId, *elementId);
  return 0;
}

int DEFORM_MESH_SET(const int * meshId, const double * time, const int * maxElemement, const int * ConnectivityMatrix)
{
  lo::debugLog("Entering DEFORM_MESH_SET");
  sdm::DeformHandlerFacadeMshTemperature* facade;
  facade = ctm::GetDeformHandlerFacadeMshTemperatureTime();
  facade->SetConnectivityMatrix(*meshId, *time, *maxElemement, ConnectivityMatrix);
  return 0;
}

//#endif