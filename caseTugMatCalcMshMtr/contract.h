
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_contracts_h__
#define am3caseTugMatCalc_contracts_h__
/*	include *********************************************************/

#include "../models/macrosSimplifiedModels.h"
#include "../modelDislocations/dislocationRandomEquation.h"
#include "../modelDislocations/dislocationWallEquation.h"
#include "../models/dataStructures.h"
#include "../modelTemplates/macroState.h"

/*	using ***********************************************************/

namespace pdi = am3::problem::dislocations;
namespace mpr = am3::model::properties;
namespace mot = am3::model::templates;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
			typedef mot::MacroContractElNumber MacroContract;
      CONTRACT(StorageContract, 2, mpr::Coordinates2D, mpr::PrecipitationsMeanRadius, mpr::PrecipitationsVolumeFraction);
//			CONTRACT(StorageContract, 4, mpr::Coordinates2D, pdi::DislocationsDensityWall, pdi::DislocationsDensityRandom, mpr::PrecipitationsMeanRadius, mpr::PrecipitationsVolumeFraction);
			CONTRACT(PrecipitationsContract, 2, mpr::Coordinates2D,mpr::PrecipitationsMeanRadius, mpr::PrecipitationsVolumeFraction);
		} //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalc_contracts_h__
