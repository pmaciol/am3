
/*! \file ivMapping.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_ivMapping_h__
#define am3caseTugMatCalc_ivMapping_h__
/*	include *********************************************************/

#include "../modelDislocations/dislocationRandomEquation.h"
#include "../modelDislocations/dislocationWallEquation.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace pdi = am3::problem::dislocations;
namespace mpr = am3::model::properties;


/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
      // Structure used to code/decode values in IV vector
      template<typename T>
      struct VariableID;

      template <> struct VariableID < pdi::DislocationsDensityWall > { static const unsigned int id = 0; typedef pdi::DislocationsDensityWall Type; };
      template <> struct VariableID < pdi::DislocationsDensityRandom > { static const unsigned int id = 1; typedef pdi::DislocationsDensityRandom Type; };
			template <> struct VariableID < mpr::PrecipitationsMeanRadius > { static const unsigned int id = 2; typedef mpr::PrecipitationsMeanRadius Type; };
			template <> struct VariableID < mpr::PrecipitationsVolumeFraction > { static const unsigned int id = 3; typedef mpr::PrecipitationsVolumeFraction Type; };

		} //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalc_ivMapping_h__
