
/*! \file caseTugMatCalc.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_caseTugMatCalc_h__
#define am3caseTugMatCalc_caseTugMatCalc_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../softwareDeform/policyOneStepOneMeshTemperatureTime.h"
#include "../softwareDeform/policyOneStepOneElement.h"
#include "../communicationMPI/instance.h"
#include "../communicationMPI/controller.h"
#include "../models/modelsManager.h"


#include "../communicationMPI/controller.h"
#include "../communicationMPI/instance.h"
#include "../models/predefinedContracts.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../models/inputTypelists.h"
#include "../modelTemplates/modelsTemplates.h"
#include "../modelTemplates/macroState.h"



//#include "matcalc.h"
#include "databus.h"

/*	using ***********************************************************/

namespace sdm = am3::software::deform;
namespace cco = am3::common::communication;

namespace mom = am3::model::manager;


//namespace cco = am3::common::communication;
namespace mda = am3::model::datapacks;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;
namespace mo = am3::model;
namespace mot = am3::model::templates;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
      sdm::DeformHandlerFacadeMshTemperature* GetDeformHandlerFacadeMshTemperatureTime();
      sdm::DeformHandlerFacadeUpd* GetDeformHandlerFacadeUpdate();
      sdm::DeformHandlerFacadeMtr* GetDeformHandlerFacadeMaterial();

      typedef sdm::OneStepOneElementNoElementCheckUserValues DeformCaseIV;
      typedef std::tuple<mpr::PrecipitationsMeanRadius, mpr::PrecipitationsVolumeFraction> PrecipitationOutput;



      typedef cco::MpiInstance<mda::Coord2dTimeTemp, PrecipitationOutput, mpt::ComputedPoint<mpr::Coordinates2D> > Instance;
      typedef cco::Controller<mda::Coord2dTimeTemp, PrecipitationOutput, Instance, mpt::ComputedPoint<mpr::Coordinates2D>, mpr::Coordinates2D > Controller;

      struct MicroModelData
      {
        unsigned id;
        double time;
        double temperature;
        double variable;
      };



      class DeformCaseStepPrecomputations
//         : public sdm::OneStepOneMeshTemperatureTime
//         , public sdm::OneStepOneElementNoElementCheckUserValues
        : public sdm::OneStepOneMeshTemperatureTime
      {
      public:
//        DeformCaseStepPrecomputations(boost::shared_ptr<mom::ManagerAllTogether<MicroModelData> > runner);

        template<typename VAR> VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> mpr::PrecipitationsMeanRadius Get<mpr::PrecipitationsMeanRadius>(mpt::Point<mpr::Coordinates2D> * point)
        {
          mpt::ComputedPoint<mpr::Coordinates2D> localPoint(point);
          Controller::PointWithOutputDataPair outputPair = cont.Get(localPoint);
          mpr::PrecipitationsMeanRadius toRet;
          Controller::OutputData od = outputPair.second;
          toRet(std::get<0>(od));
          lo::debugLog("returning PrecipitationsMeanRadius" + std::to_string(toRet()));
          return toRet;
        }

        template<> mpr::PrecipitationsVolumeFraction Get<mpr::PrecipitationsVolumeFraction>(mpt::Point<mpr::Coordinates2D> * point)
        {
          mpt::ComputedPoint<mpr::Coordinates2D> localPoint(point);
          Controller::PointWithOutputDataPair outputPair = cont.Get(localPoint);
          mpr::PrecipitationsVolumeFraction toRet;
          Controller::OutputData od = outputPair.second;
          toRet(std::get<1>(od));
          lo::debugLog("returning PrecipitationsVolumeFraction" + std::to_string(toRet()));
          return toRet;
        }

        virtual void SetTemperaturesInNodes(const int materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const int nodeStart, const int nodeEnd) override;
        virtual void SetConnectivityMatrix(const int meshId, const double time, const int maxElemement, const int * ConnectivityMatrix) override;
        void Init(){ cont.Init(); };

      protected:
        cco::Controller<mda::Coord2dTimeTemp, PrecipitationOutput, Instance, mpt::ComputedPoint<mpr::Coordinates2D>, mpr::Coordinates2D > cont;
        //boost::shared_ptr<mom::ManagerAllTogether<mmk::MicroModelData>> runner_;
        bool isInitialized_;

        void PushData(const int i, const double temperature, const double time, std::vector<MicroModelData> & dataToRun);
        bool IsInitialization(const double time) const;
        void Initialized();

        typedef std::array<unsigned, 4> nodeStruct;
        std::vector<nodeStruct> connectivityMatrix_;

        

      };


		} //tugmc

	} //cases
} //am3

inline void VectorToOutput(std::vector<double>& input, am3::cases::tugmc::PrecipitationOutput& output)
{
  lo::debugLog("Converting received values to internal output, only precipitation on 2 and 3");
  std::get<0>(output)(input[2]);
  std::cout << input[2];
  std::get<1>(output)(input[3]);
  std::cout << input[3];
}
#endif // am3caseTugMatCalc_caseTugMatCalc_h__
