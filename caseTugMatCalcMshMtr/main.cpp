
/*! \file main.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       The 'business logic' of whole case.
 * \details   	In the Main::Init method, there is a definition how submodels comuunicate between themselves and to external software
 * 					  	Also use of internal variables is described
*********************************************************************/
/*	include *********************************************************/

#include "main.h"
#include "caseTugMatCalc.h"
#include "ivStorage.h"
#include "kbs.h"
#include "matCalc.h"
#include "../models/macrosSimplifiedModels.h"


/*	using ***********************************************************/

namespace ctm = am3::cases::tugmc;

/*	extern **********************************************************/

/*	classes *********************************************************/

am3::cases::tugmc::Main*  mainInstance;

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
			void Main::Init()
			{
				if (!IsInitialized())
				{

          std::cout << "Project am3caseTugMatCalc" << std::endl;
					// sub-models coefficients, should be read from file by submodels themselves
					const double burgers = 0.286e-9;
					const double interObstacle = 2.5e-6;
					const double sizeObstacle = 1.0e-6; //!!!!!!!! Verify!
					const double activationEnergy = 2.4e-19;
					const double freqObstacle = 8e13;
					const double dislDens = 1e11;


//           boost::shared_ptr<mmk::MicroScaleModel> microManager;
//           boost::shared_ptr<mom::ManagerAllTogether<mmk::MicroScaleModel::InputData>> runner;

//           microManager = boost::shared_ptr<mmk::MicroScaleModel>(new mmk::MicroScaleModel);
//           runner = boost::shared_ptr<mom::ManagerAllTogether<mmk::MicroScaleModel::InputData>>(new mom::BasicAllTogatherManager<mmk::MicroScaleModel>(microManager));
					precomp_ = boost::shared_ptr<DeformCaseStepPrecomputations>(new DeformCaseStepPrecomputations); //DeformCaseStepPrecomputations is used to run MatCalc precomputations; presently not connected to Databus //TODO: probably should be connected
          precomp_->Init();
          //DATABUS_INSTANCE(mainDatabus, CaseTugMatCalcDatabus); //Initialization of the main Databus
          DATABUS_INSTANCE(mainDatabus, CaseTugMatCalcToUSPDatabus); //Initialization of the main Databus
					mainDatabus_ = mainDatabus;

					//******************************************************************************************************************************************************************
					// Defining the models from the top to bottom
					//******************************************************************************************************************************************************************

          MODEL_INSTANCE_REDESIGN(IvManager, IvManagerInstance, CaseTugMatCalcToUSPDatabus, ivManager, mainDatabus);
          boost::shared_ptr<DeformCaseIV> ivProvider(new DeformCaseIV(ivManager));
          deformCaseIV_ = ivProvider;
          boost::shared_ptr<IVStorageAdaptedToPrecipitationsMeanRadius> precMeanRadiusStorage(new IVStorageAdaptedToPrecipitationsMeanRadius(ivProvider));
          boost::shared_ptr<IVStorageAdaptedToPrecipitationsVolumeFraction> PrecVolFractionStorage(new IVStorageAdaptedToPrecipitationsVolumeFraction(ivProvider));
          MODEL_INSTANCE_REDESIGN(PrecipitationsSwitcher, PrecipitationsModel, CaseTugMatCalcToUSPDatabus, precipitationsSwitcher, mainDatabus);
          precipitationsSwitcher->kbs_->SetModelInput(mainDatabus.get()); //This one should be done automatically, somewhere inside constructing process
//          MODEL_INSTANCE_IN_SWITCHER(SecondPhasePrecipitationsConst, SecondPhasePrecipitationsConstModel, precConst, mainDatabus, PrecipitationsModel, precipitationsSwitcher);
          MODEL_INSTANCE_IN_SWITCHER(SecondPhasePrecipitationsMatCalc, SecondPhasePrecipitationsMatCalcModel, precMC, mainDatabus, PrecipitationsModel, precipitationsSwitcher);
//          precMC->Init();
          precMC->precomp_ = precomp_;

          MODEL_INSTANCE_WITH_PARAM(MacroModelTemperature, MacroModelTemperaturesModel, CaseTugMatCalcToUSPDatabus, macroModelTemperatures, mainDatabus, ivProvider);


// 					// StressProxy; gets stress and stress's derivatives to macroscale software (FEM)
// 					// stressReturn is used by object being an interface for FEM calls
// 					MODEL_INSTANCE_REDESIGN(mst::StressProxy, StressProxyModel, CaseTugMatCalcDatabus, stressReturn, mainDatabus);
// 					
// 					// Mock stresses are needed because stress derivatives  are not available in stress model
// 					// Deform expects yield stress, but also derivatives with strain and strain rate
// 					MODEL_INSTANCE_REDESIGN(mot::MockStressDerivatives, MockStressesModel, CaseTugMatCalcDatabus, mockStressDerivatives, mainDatabus);
// 
// 					// Iv's dependent submodels with iv's interpretation plug-ins to Deform and Databus.
// 					// IvManager gets new IV values from Databus. 
// 					// It works on the upper part of Databus, but is passed to ivPRovider, which is necessary for each iv-dependent sub-model
// 					MODEL_INSTANCE_REDESIGN(IvManager, IvManagerInstance, CaseTugMatCalcDatabus, ivManager, mainDatabus);
// 					// ivProvider is a handler of Deform user variables procedure; it is responsible for both way communication
// 					// iVManager is used to get iv's from Databus
// 					boost::shared_ptr<DeformCaseIV> ivProvider(new DeformCaseIV(ivManager));
// 
// 					// Further submodels
// 					mst::StressAthermalDislocationsEq_Param strAth(0.3, 3.0, burgers);
// 					MODEL_INSTANCE_WITH_PARAM(mst::StressAthermalDislocationsEq, StressAthermalDislocationsEqModel, CaseTugMatCalcDatabus, stressAthermalDislocationsEq, mainDatabus, strAth);
// 					mst::StressThermalOrowanReduced_Params prms(burgers * interObstacle * sizeObstacle, activationEnergy, interObstacle, freqObstacle, dislDens, burgers);
// 					MODEL_INSTANCE_WITH_PARAM(mst::StressThermalOrowanReduced, StressThermalOrowanReducedModel, CaseTugMatCalcDatabus, stressThermalOrowanReduced, mainDatabus, prms);
// 					MODEL_INSTANCE_WITH_PARAM(mst::StressThermalAthermalTaylor, StressThermalAthermalTaylorModel, CaseTugMatCalcDatabus, stressThermalAthermalTaylor, mainDatabus, 3.06);
// 
// 					// IV source for sub-models; Gets iv's from ivProvider (iv's are represented just as doubles) and trasnforms them to am3 'properties'
// 					boost::shared_ptr<IVStorageAdaptedToDislRandEquat> dislRandEquatStorage(new IVStorageAdaptedToDislRandEquat(ivProvider));
// 					boost::shared_ptr<IVStorageAdaptedToDislWalEquat> dislWallEquatStorage(new IVStorageAdaptedToDislWalEquat(ivProvider));
// 
// 					// Dislocations submodels (uses iv's)
// 					mdi::DislocationWallEquation_Param<IVStorageAdaptedToDislWalEquat> dwParam(2.4e6, 6e7, 0.3, 7.0, 0.0, 0.0, dislWallEquatStorage);
// 					MODEL_INSTANCE_WITH_PARAM(DislocationWallEquation, DislocationWallEquationModel, CaseTugMatCalcDatabus, dislocationWallEquation, mainDatabus, dwParam);
// 					mdi::DislocationRandomEquation_Param<IVStorageAdaptedToDislRandEquat> drParam(22e6, 80e7, 2.2, 105.0, 0.0, 0.0, dislRandEquatStorage);
// 					MODEL_INSTANCE_WITH_PARAM(DislocationRandomEquation, DislocationRandomEquationModel, CaseTugMatCalcDatabus, dislocationRandomEquation, mainDatabus, drParam);
// 
// 
// 
// 										
// 					MODEL_INSTANCE_REDESIGN(PrecipitationsSwitcher, PrecipitationsModel, CaseTugMatCalcDatabus, precipitationsSwitcher, mainDatabus);
// 					precipitationsSwitcher->kbs_->SetModelInput(mainDatabus.get()); //This one should be done automatically, somewhere inside constructing process
// 
// 					//   MatCalcScriptProvider scripts;
// 					MODEL_INSTANCE_IN_SWITCHER(SecondPhasePrecipitationsConst, SecondPhasePrecipitationsConstModel, precConst, mainDatabus, PrecipitationsModel, precipitationsSwitcher);
// 					MODEL_INSTANCE_IN_SWITCHER(SecondPhasePrecipitationsMatCalc, SecondPhasePrecipitationsMatCalcModel, precMC, mainDatabus, PrecipitationsModel, precipitationsSwitcher);
// 
// 					//   precipitationsSwitcher->AddModel(gmm);
// 
// 					// Submodels with smallest set of inputs come first
// 					mpr::ShearModulus sm; sm(26e9);
// 					MODEL_INSTANCE_WITH_PARAM(mst::ShearModulusConst, ShearModulusConstModel, CaseTugMatCalcDatabus, shearModulusConst, mainDatabus, sm);
// 
// 					// State provider gets the state of the material from Deform (SetSomething procedures in user-supplied code)
// 					boost::shared_ptr<DeformCaseStress> stateProvider(new DeformCaseStress(stressReturn));
// 					// MacromodelState uses statePrivider to get the material state and feed Databus with them.
// 					//If not-default constructor needed, the macro MODEL_INSTANCE_WITH_PARAMS must be used
// 					MODEL_INSTANCE_WITH_PARAM(MacroModelState, MacroModelStateModel, CaseTugMatCalcDatabus, macroModelState, mainDatabus, stateProvider);

					initialized_ = true;
				}
			}


			Main::Main()
			{
				try
				{
					Init();
				}
				catch (std::exception& e)
				{
					lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, e.what());
				}
			}

			bool Main::IsInitialized() const
			{
				return initialized_;
			}

      sdm::DeformHandlerFacadeMshTemperature* Main::GetDeformCasePrecomp()
      {
        return precomp_.get();
      }

      sdm::DeformHandlerFacadeUpd* Main::GetDeformCaseIV()
      {
        return deformCaseIV_.get();
      }

		} //tugmc
	} //cases
} //am3