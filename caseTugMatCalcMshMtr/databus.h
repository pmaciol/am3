/*! \file databus.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_databus_h__
#define am3caseTugMatCalc_databus_h__
/*	include *********************************************************/

#include "../common/typelists.h"
#include "../modelTemplates/macroStress.h"
#include "../modelStress/stressSum.h"
#include "../modelStress/stressAthermalDislocationsEq.h"
#include "../modelStress/stressThermalOrowan.h"
#include "../modelStress/shearModulus.h"
#include "../modelStress/stressProxy.h"
#include "modelsAdapters.h"
#include "ivStorage.h"

/*	using ***********************************************************/

namespace mst = am3::model::stress;
namespace mot = am3::model::templates;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
      typedef TEMPLATELIST_3(
        IvManager,
        PrecipitationsSwitcher,
        MacroModelTemperature
        ) CaseTugMatCalcToUSPList;

      typedef db::DatabusTypelists<CaseTugMatCalcToUSPList> CaseTugMatCalcToUSPDatabus;


      // List of 'Databus' classes
      typedef TEMPLATELIST_11(
		        mst::StressProxy,
		        mot::MockStressDerivatives,
		        IvManager,
		        mst::StressThermalAthermalTaylor,
		        mst::StressThermalOrowanReduced,
		        mst::StressAthermalDislocationsEq,
		        DislocationWallEquation,
		        DislocationRandomEquation,
		        mst::ShearModulusConst,
		        PrecipitationsSwitcher, 
		        MacroModelState
		        ) CaseTugMatCalcList;
		
		      // Databus
		      typedef db::DatabusTypelists<CaseTugMatCalcList> CaseTugMatCalcDatabus;
				} //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalc_databus_h__
