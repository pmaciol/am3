
/*! \file caseTugMatCalc.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../softwareDeform/setDeformHandler.h"
#include "caseTugMatCalc.h"
#include "modelsAdapters.h"
#include "main.h"

/*	using ***********************************************************/

namespace sdm = am3::software::deform;
namespace ctm = am3::cases::tugmc;

/*	extern **********************************************************/

extern am3::cases::tugmc::Main*  mainInstance;

sdm::DeformHandlerFacadeMshTemperature* am3::cases::tugmc::GetDeformHandlerFacadeMshTemperatureTime()
{
  if (mainInstance == nullptr) mainInstance = new am3::cases::tugmc::Main;
  return mainInstance->GetDeformCasePrecomp();
}

/*	classes *********************************************************/


sdm::DeformHandlerFacadeUpd* am3::cases::tugmc::GetDeformHandlerFacadeUpdate()
{
  if (mainInstance == nullptr) mainInstance = new am3::cases::tugmc::Main;
  return mainInstance->GetDeformCaseIV();
}

void am3::cases::tugmc::DeformCaseStepPrecomputations::SetTemperaturesInNodes(const int materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const int nodeStart, const int nodeEnd)
{
  sdm::OneStepOneMeshTemperatureTime::SetTemperaturesInNodes(materialId, time, timeStep, temperatures, deltaTemperatures, nodeStart, nodeEnd);
  unsigned presentElement = 1;
  mpr::Time timeProperty; timeProperty(time);
  for (const auto element : connectivityMatrix_)
  {
    std::cout << "Element " << presentElement << std::endl;
    mpt::ComputedPoint<mpr::Coordinates2D> localPoint(&(cont.MakePoint(presentElement, timeProperty)));
    double elementMeanTemperature = 0;
    for (unsigned tempIndex = 0; tempIndex < 4; tempIndex++)
    {
      std::cout << "NodalTemperature " << connectivityMatrix_.at(presentElement - 1).at(tempIndex) << " " << temperatures[(connectivityMatrix_.at(presentElement - 1).at(tempIndex)) - 1]<<std::endl;
      elementMeanTemperature += temperatures[(connectivityMatrix_.at(presentElement - 1).at(tempIndex)) - 1];
    }
    elementMeanTemperature /= 4;
    
    mpr::Temperature temperature = elementMeanTemperature;
    mda::Coord2dTimeTemp val(localPoint.Coordinates(), localPoint.GetTime(), temperature);
    std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair(localPoint, val);
    cont.Run(pair);
    ++presentElement;
  }

// 
//   std::vector<MicroModelData> dataToRun;
//   cont.WaitForAll();
//   for (int i = nodeStart; i <= (nodeStart + 5); i++)
//   {
//     const mpr::Coordinates2D coord = mpr::Coordinates2D(materialId, i); //This is a fake!
//     mda::Coord2dTimeTemp val(coord, time, temperatures[i]);
// 
//     const mpr::Time t = mpr::Time(time);
//     mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);
// 
//     std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair(point, val);
//     cont.Run(pair);
//   }
}

void am3::cases::tugmc::DeformCaseStepPrecomputations::SetConnectivityMatrix(const int meshId, const double time, const int maxElemement, const int * connectivityMatrix)
{
  const int* conMatrixPointer = connectivityMatrix;
  connectivityMatrix_.clear();
  for (int i = 0; i < maxElemement; i++)
  {
    nodeStruct next = { { *conMatrixPointer, *(conMatrixPointer + 1), *(conMatrixPointer + 2), *(conMatrixPointer + 3) } };
    connectivityMatrix_.push_back(next);
    conMatrixPointer += 4;
  }
  std::cout << "ConMat" << std::endl;
  for (auto i = connectivityMatrix_.begin(); i != connectivityMatrix_.end(); ++i)
  {
    for (const auto s : *i)
      std::cout << s << ' ';
  }
  std::cout << std::endl;

}
