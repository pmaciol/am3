
/*! \file ivStorage.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_ivStorage_h__
#define am3caseTugMatCalc_ivStorage_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../softwareDeform/policyOneStepOneElement.h"
#include "../modelDislocations/dislocationRandomEquation.h"
#include "../modelDislocations/dislocationWallEquation.h"
#include "../adapter/ivStorageAdapted.h"
#include "ivMapping.h"
#include "contract.h"

/*	using ***********************************************************/

namespace sdm = am3::software::deform;
namespace pdi = am3::problem::dislocations;
namespace ad = am3::adapter;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
    namespace tugmc
    {
      // decodes double value identified with index into concrete type
      typedef ad::IVStorageAdaptedOneValue<sdm::OneStepOneElementNoElementCheckUserValues, VariableID<mpr::PrecipitationsMeanRadius>> IVStorageAdaptedToPrecipitationsMeanRadius;
      typedef ad::IVStorageAdaptedOneValue<sdm::OneStepOneElementNoElementCheckUserValues, VariableID<mpr::PrecipitationsVolumeFraction>> IVStorageAdaptedToPrecipitationsVolumeFraction;

      // Implements mot::OneStepOneElementIvNew interface for external IV requests; translates variables available in Databus into vectr of doubles
      template<typename INPUT>
      class IvManager
        : public mot::OneStepOneElementIvNew
        , public mo::InputTypelist < INPUT >
      {
      public:
        typedef StorageContract Inputs;
        typedef typename mot::EmptyContract2D Contract;

      protected:
        boost::shared_ptr<mot::OneStepOneElementIvPresent> ivStorage_;
        mpt::Point<mpr::Coordinates2D>* point_ = nullptr;
        bool CheckPoint(mpt::Point<mpr::Coordinates2D>* point);
        void SetPoint(mpt::Point<mpr::Coordinates2D>* point);

        virtual std::vector<double> GetInternalVariablesNew(mpt::Point<mpr::Coordinates2D> * point) override;
      };

      template<typename INPUT>
      void am3::cases::tugmc::IvManager<INPUT>::SetPoint(mpt::Point<mpr::Coordinates2D>* point)
      {
        point_ = point;
      }

      template<typename INPUT>
      bool am3::cases::tugmc::IvManager<INPUT>::CheckPoint(mpt::Point<mpr::Coordinates2D>* point)
      {
        return (point_ == nullptr) ? false : true;
      }




      // decodes double value identified with index into concrete type
      typedef ad::IVStorageAdaptedOneValue<sdm::OneStepOneElementNoElementCheckUserValues, VariableID<pdi::DislocationsDensityWall>> IVStorageAdaptedToDislWalEquat;
      typedef ad::IVStorageAdaptedOneValue<sdm::OneStepOneElementNoElementCheckUserValues, VariableID<pdi::DislocationsDensityRandom>> IVStorageAdaptedToDislRandEquat;
// 
//       // Implements mot::OneStepOneElementIvNew interface for external IV requests; translates variables available in Databus into vectr of doubles
//       template<typename INPUT>
//       class IvManager
//         : public mot::OneStepOneElementIvNew
//         , public mo::InputTypelist < INPUT >
//       {
//       public:
//         typedef StorageContract Inputs;
//         typedef typename mot::EmptyContract2D Contract;
// 
//       protected:
//         boost::shared_ptr<mot::OneStepOneElementIvPresent> ivStorage_;
//         mpt::Point<mpr::Coordinates2D>* point_;
// 
//         virtual std::vector<double> GetInternalVariablesNew() const;
//       };



      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////

      template<typename INPUT>
      std::vector<double> IvManager<INPUT>::GetInternalVariablesNew(mpt::Point<mpr::Coordinates2D> * point)
      {
        std::vector<double> toRet(4);
        toRet[VariableID<mpr::PrecipitationsMeanRadius>::id] = bus_->GetFromDatabus<mpr::PrecipitationsMeanRadius>(point).Value();
        toRet[VariableID<mpr::PrecipitationsVolumeFraction>::id] = bus_->GetFromDatabus<mpr::PrecipitationsVolumeFraction>(point).Value();
        return toRet;
      }
    } //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalc_ivStorage_h__
