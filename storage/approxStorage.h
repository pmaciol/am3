/*! \file approxStorage.h *******************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief
* \details
*********************************************************************/

#ifndef approxStorage_h__
#define approxStorage_h__

#ifndef   storage_h__
#include "storage.h"
#endif

#ifndef               mesh_h__
#include "../utilMesh/mesh.h"
#endif

#ifndef             dataStructures_h__
#include "../models/dataStructures.h"
#endif


namespace pnt = am3::model::point;

namespace am3
{
    namespace math //???
    {
        namespace storage
        {
            //==============================================================
            // Template to retrieve dimension from Coordinates
            //
            template <class T_COORDINATES>
            struct GetDim;

            template <>
            struct GetDim<am3::model::Coordinates3D>
            {
                enum { dim = 3 };
            };

            template <>
            struct GetDim<am3::model::Coordinates2D>
            {
                enum { dim = 2 };
            };

            //================================================================================================
            // ApproxStorageRAM
            //
            template<typename T_COORDINATE, typename T_DATA>
            class ApproxStorageRAM : public Storage<T_COORDINATE, T_DATA>
            {
            public:
                virtual bool IsDataAvailable(const pnt::NamedPoint<T_COORDINATE>& point);
                virtual void StoreData(const pnt::NamedPoint<T_COORDINATE>& point, const T_DATA* data);
                virtual bool GetState(const pnt::NamedPoint<T_COORDINATE>& point, T_DATA* data);

            protected:
                Mesh<GetDim<T_COORDINATE>::dim> mesh_;
            };


            template<typename T_COORDINATE, typename T_DATA>
            void ApproxStorageRAM<T_COORDINATE, T_DATA>::StoreData(const pnt::NamedPoint<T_COORDINATE>& point, const T_DATA* data)
            {
            }

            template<typename T_COORDINATE, typename T_DATA>
            bool ApproxStorageRAM<T_COORDINATE, T_DATA>::GetState(const pnt::NamedPoint<T_COORDINATE>& point, T_DATA* data)
            {



            }

            template<typename T_COORDINATE, typename T_DATA>
            bool ApproxStorageRAM<T_COORDINATE, T_DATA>::IsDataAvailable(const pnt::NamedPoint<T_COORDINATE>& point)
            {
                retun true;
            }

        }
    }
}

#endif //approxStorage_h__