/*! \file simpleTypes.h **************************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief	Templates to retrieve simple types from a complex data structure.
* \details
*********************************************************************/

#ifndef simpleTypes_h__
#define simpleTypes_h__

#ifndef              cons_h__
#include "../storage/cons.h"
#endif

#ifndef             dataStructures_h__
#include "../models/dataStructures.h"
#endif


namespace am3
{
	namespace math //???
	{
		namespace storage
		{
			//===========================================================
			// OneFieldSimpleTypes
			//

			template <class T_LIST, class T_SIMPLETYPES>
			struct OneFieldSimpleTypes
			{
				typedef typename OneFieldSimpleTypes<typename T_LIST::DerivedFrom, T_SIMPLETYPES>::simpleTypes simpleTypes;
			};


			template<typename INTERNAL, typename ADDITIONAL, class T_SIMPLETYPES>
			struct OneFieldSimpleTypes< am3::model::Dimensions3D<INTERNAL, ADDITIONAL>, T_SIMPLETYPES >
			{
				typedef
					typename OneFieldSimpleTypes
					<
						am3::model::ComplexProperty< INTERNAL, ADDITIONAL, am3::model::HaveGetSetValue >,

						typename OneFieldSimpleTypes
						<
							am3::model::ComplexProperty< INTERNAL, ADDITIONAL, am3::model::HaveGetSetValue >,

							typename OneFieldSimpleTypes
							<
								am3::model::ComplexProperty< INTERNAL, ADDITIONAL, am3::model::HaveGetSetValue >,
								T_SIMPLETYPES
							>::simpleTypes

						>::simpleTypes

					>::simpleTypes

					simpleTypes;
			};


			template<typename INTERNAL, typename ADDITIONAL, class T_SIMPLETYPES>
			struct OneFieldSimpleTypes<am3::model::SpatialDerivative3D<INTERNAL, ADDITIONAL>, T_SIMPLETYPES >
			{
				typedef
					typename OneFieldSimpleTypes
					<
						am3::model::ComplexProperty< INTERNAL, ADDITIONAL, am3::model::HaveGetSetValue >,

						typename OneFieldSimpleTypes
						<
							am3::model::ComplexProperty< INTERNAL, ADDITIONAL, am3::model::HaveGetSetValue >,

							typename OneFieldSimpleTypes
							<
								am3::model::ComplexProperty< INTERNAL, ADDITIONAL, am3::model::HaveGetSetValue >,
								T_SIMPLETYPES
							>::simpleTypes

						>::simpleTypes

					>::simpleTypes

					simpleTypes;
			};


			template<class T_SIMPLETYPES>
			struct OneFieldSimpleTypes<am3::model::util::Null, T_SIMPLETYPES>
			{
				typedef T_SIMPLETYPES simpleTypes;
			};


			template<class T_SIMPLETYPES>
			struct OneFieldSimpleTypes<EmptyType, T_SIMPLETYPES>
			{
				typedef T_SIMPLETYPES simpleTypes;
			};

			template<class T_SIMPLETYPES>
			struct OneFieldSimpleTypes<double, T_SIMPLETYPES>
			{
				typedef Typelist<double, T_SIMPLETYPES> simpleTypes;
			};

			template<class T_SIMPLETYPES>
			struct OneFieldSimpleTypes<float, T_SIMPLETYPES>
			{
				typedef Typelist<float, T_SIMPLETYPES> simpleTypes;
			};

			template<class T_SIMPLETYPES>
			struct OneFieldSimpleTypes<int, T_SIMPLETYPES>
			{
				typedef Typelist<int, T_SIMPLETYPES> simpleTypes;
			};


			template<typename T, typename T_HAVE_VALUE, typename T_SIMPLETYPES>
			struct OneFieldSimpleTypes< am3::model::SimpleProperty<T, T_HAVE_VALUE>, T_SIMPLETYPES >
			{
				typedef Typelist<T, T_SIMPLETYPES> simpleTypes;
			};


			template<typename T, typename T_ADDITIONAL_COMPONENETS, typename T_HAVE_VALUE, typename T_SIMPLETYPES>
			struct OneFieldSimpleTypes< am3::model::ComplexProperty<T, T_ADDITIONAL_COMPONENETS, T_HAVE_VALUE >, T_SIMPLETYPES >
			{
				typedef typename OneFieldSimpleTypes<T_ADDITIONAL_COMPONENETS, T_SIMPLETYPES>::simpleTypes simpleTypes;
			};

			template<typename T, typename T_ADDITIONAL_COMPONENETS, typename T_SIMPLETYPES>
			struct OneFieldSimpleTypes< am3::model::ComplexProperty<T, T_ADDITIONAL_COMPONENETS, am3::model::HaveGetValue >, T_SIMPLETYPES >
			{
				typedef Typelist<T, typename OneFieldSimpleTypes<T_ADDITIONAL_COMPONENETS, T_SIMPLETYPES>::simpleTypes > simpleTypes;
			};

			template<typename T, typename T_ADDITIONAL_COMPONENETS, typename T_SIMPLETYPES>
			struct OneFieldSimpleTypes< am3::model::ComplexProperty<T, T_ADDITIONAL_COMPONENETS, am3::model::HaveGetSetValue >, T_SIMPLETYPES >
			{
				typedef Typelist<T, typename OneFieldSimpleTypes<T_ADDITIONAL_COMPONENETS, T_SIMPLETYPES>::simpleTypes > simpleTypes;
			};


			//===========================================================
			// StructSimpleTypes
			//

			template <class T_LIST>
			struct StructSimpleTypes;

			template <>
			struct StructSimpleTypes<Typelist<EmptyType, EmptyType>>
			{
				typedef Typelist<EmptyType, EmptyType> simpleTypes;
			};

			template <typename T_HEAD, typename T_TAIL>
			struct StructSimpleTypes<Typelist<T_HEAD, T_TAIL>>
			{
				typedef typename OneFieldSimpleTypes< T_HEAD, typename StructSimpleTypes<T_TAIL>::simpleTypes >::simpleTypes simpleTypes;
			};

		}
	}
}

#endif //simpleTypes_h__