/*! \file Typelist.h **************************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief Typelist linearization based on article "Generic Programming: Typelists and Applications" by Andrei Alexandrecsu      
* \details   	
*********************************************************************/

#ifndef cons_h__
#define cons_h__

#ifndef Typelists_h__
#include "../common/Typelists.h"
#endif


template< typename T1, typename T2 = EmptyType, typename T3 = EmptyType, typename T4 = EmptyType, typename T5 = EmptyType, typename T6 = EmptyType, typename T7 = EmptyType, typename T8 = EmptyType, typename T9 = EmptyType, typename T10 = EmptyType, typename T11 = EmptyType, typename T12 = EmptyType >
struct cons
{
	typedef Typelist< T1,
		Typelist<T2,
		Typelist<T3,
		Typelist<T4,
		Typelist<T5,
		Typelist<T6,
		Typelist<T7,
		Typelist<T8,
		Typelist<T9,
		Typelist<T10,
		Typelist<T11,
		Typelist<T12,
		Typelist<EmptyType, EmptyType> > > > > > > > > > > > > type;
};


template< typename T1 >
struct cons<T1>
{
	typedef Typelist< T1, 
		Typelist<EmptyType, EmptyType> > type;
};

template< typename T1, typename T2 >
struct cons<T1, T2>
{
	typedef Typelist< T1, 
		Typelist<T2, 
		Typelist<EmptyType, EmptyType> > > type;
};

template< typename T1, typename T2, typename T3 >
struct cons<T1, T2, T3>
{
	typedef Typelist< T1, 
		Typelist<T2, 
		Typelist<T3,
		Typelist<EmptyType, EmptyType> > > > type;
};

template< typename T1, typename T2, typename T3, typename T4 >
struct cons<T1, T2, T3, T4>
{
	typedef Typelist< T1,
		Typelist<T2,	
		Typelist<T3,
		Typelist<T4,
		Typelist<EmptyType, EmptyType> > > > > type;
};

template< typename T1, typename T2, typename T3, typename T4, typename T5 >
struct cons<T1, T2, T3, T4, T5>
{
	typedef Typelist< T1,
		Typelist<T2,	
		Typelist<T3,
		Typelist<T4,
        Typelist<T5,
		Typelist<EmptyType, EmptyType> > > > > > type;
};

template< typename T1, typename T2, typename T3, typename T4, typename T5, typename T6 >
struct cons<T1, T2, T3, T4, T5, T6>
{
	typedef Typelist< T1,
		Typelist<T2,	
		Typelist<T3,
		Typelist<T4,
        Typelist<T5,
        Typelist<T6,
		Typelist<EmptyType, EmptyType> > > > > > > type;
};

template< typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7 >
struct cons<T1, T2, T3, T4, T5, T6, T7>
{
	typedef Typelist< T1,
		Typelist<T2,	
		Typelist<T3,
		Typelist<T4,
        Typelist<T5,
        Typelist<T6,
        Typelist<T7,
		Typelist<EmptyType, EmptyType> > > > > > > > type;
};

template< typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8 >
struct cons<T1, T2, T3, T4, T5, T6, T7, T8>
{
	typedef Typelist< T1,
		Typelist<T2,	
		Typelist<T3,
		Typelist<T4,
        Typelist<T5,
        Typelist<T6,
        Typelist<T7,
        Typelist<T8,
		Typelist<EmptyType, EmptyType> > > > > > > > > type;
};


template< typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9>
struct cons<T1, T2, T3, T4, T5, T6, T7, T8, T9>
{
	typedef Typelist< T1,
		Typelist<T2,
		Typelist<T3,
		Typelist<T4,
		Typelist<T5,
		Typelist<T6,
		Typelist<T7,
		Typelist<T8,
		Typelist<T9,
		Typelist<EmptyType, EmptyType> > > > > > > > > > type;
};


template< typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10>
struct cons<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
{
	typedef Typelist< T1,
		Typelist<T2,
		Typelist<T3,
		Typelist<T4,
		Typelist<T5,
		Typelist<T6,
		Typelist<T7,
		Typelist<T8,
		Typelist<T9,
		Typelist<T10,
		Typelist<EmptyType, EmptyType> > > > > > > > > > > type;
};


template< typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct cons<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
{
	typedef Typelist< T1,
		Typelist<T2,
		Typelist<T3,
		Typelist<T4,
		Typelist<T5,
		Typelist<T6,
		Typelist<T7,
		Typelist<T8,
		Typelist<T9,
		Typelist<T10,
		Typelist<T11,
		Typelist<EmptyType, EmptyType> > > > > > > > > > > > type;
};


#endif //cons_h__
