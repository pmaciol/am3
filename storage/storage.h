/*! \file storage.h **************************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief       
* \details   	
*********************************************************************/

#ifndef storage_h__
#define storage_h__

#include <vector>
#include <unordered_map>
#include "../models/namedPoint.h"

namespace pnt=am3::model::point;

namespace am3
{
    namespace math //???
    {
        namespace storage
        {
            template<typename T_COORDINATE, typename T_DATA>
            class Storage
            {
            public:
                virtual bool IsDataAvailable(const pnt::NamedPoint<T_COORDINATE>& point) = 0;
                virtual void StoreData(const pnt::NamedPoint<T_COORDINATE>& point, const T_DATA* data) = 0;
                virtual bool GetState(const pnt::NamedPoint<T_COORDINATE>& point, T_DATA* data) = 0;
            };

            //=============================================================================

            template<typename T_COORDINATE, typename T_DATA>
            class StorageRAM : public Storage<T_COORDINATE, T_DATA>
            {
            public:
                virtual bool IsDataAvailable(const pnt::NamedPoint<T_COORDINATE>& point);
                virtual void StoreData(const pnt::NamedPoint<T_COORDINATE>& point, const T_DATA* data);
                virtual bool GetState(const pnt::NamedPoint<T_COORDINATE>& point, T_DATA* data);

            protected:
                std::vector<T_DATA> store_;
                std::unordered_map<unsigned long, unsigned long>  dict_; //dictionary NamedPoint::id_ -> index_in_data
            };

            template<typename T_COORDINATE, typename T_DATA>
            void StorageRAM<T_COORDINATE, T_DATA>::StoreData(const pnt::NamedPoint<T_COORDINATE>& point, const T_DATA* data)
            {
                store_.push_back(*data);
                dict_.emplace(std::make_pair(point.GetId(), store_.size()-1));
            }

            template<typename T_COORDINATE, typename T_DATA>
            bool StorageRAM<T_COORDINATE, T_DATA>::GetState(const pnt::NamedPoint<T_COORDINATE>& point, T_DATA* data)
            {
                try
                {
                    unsigned long idx = dict_[point.GetId()];
                    *data = store_[idx];
                    return true;
                }
                catch(std::out_of_range ex)
                {
                    return false;
                }
            }

            template<typename T_COORDINATE, typename T_DATA>
            bool StorageRAM<T_COORDINATE, T_DATA>::IsDataAvailable(const pnt::NamedPoint<T_COORDINATE>& point)
            {
                try
                {
                    unsigned long idx = dict_[point.GetId()];
                    return true;
                }
                catch(std::out_of_range ex)
                {
                    return false;
                }
            }

        }
    }
}

#endif //storage_h__