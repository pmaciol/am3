/*! \file dataMaker.h **************************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief
* \details
*********************************************************************/

#ifndef makeData_h__
#define makeData_h__


#ifndef Typelists_h__
#include "../common/Typelists.h"
#endif
#include "boost/numeric/conversion/cast.hpp"

//--------------------------------------------------------------------
// TypeAt: Template to retrieve type at given index from Typelist.
//

template <class T_LIST, unsigned int index> struct TypeAt;

template <class HEAD, class TAIL>
struct TypeAt<Typelist<HEAD, TAIL>, 0>
{
    typedef HEAD Result;
};

template <class HEAD, class TAIL, unsigned int i>
struct TypeAt<Typelist<HEAD, TAIL>, i>
{
    typedef typename TypeAt<TAIL, i-1>::Result Result;
};


//--------------------------
// MakeData
//

template <class T_LIST>
struct MakeData;

template <typename T_HEAD, typename T_EMPTYTAIL>
struct MakeData< Typelist< T_HEAD, Typelist<EmptyType, T_EMPTYTAIL> > >
{
    typedef typename Typelist< T_HEAD, Typelist<EmptyType, T_EMPTYTAIL> > typelist;
    typedef T_HEAD Head;

    T_HEAD field_;
};

template <typename T_HEAD, typename T_TAIL>
struct MakeData< Typelist<T_HEAD, T_TAIL> > : public MakeData<T_TAIL>
{
    typedef typename Typelist<T_HEAD, T_TAIL> typelist;
    typedef T_HEAD Head;
    typedef typename MakeData<T_TAIL> Base;

    T_HEAD field_;
};

//--------------------------------------------------------------------
// Index based access to field of a data record created by DataMaker.
//

template <int i, class T_DATA>
struct DataField
{
    typedef typename TypeAt<typename T_DATA::typelist, i>::Result Result;

    static Result& get(T_DATA& data)
    {
        return DataField< i-1, typename T_DATA::Base >::get(data);
    }
};

template <class T_DATA>
struct DataField<0, T_DATA>
{
    typedef typename TypeAt<typename T_DATA::typelist, 0>::Result Result;

    static Result& get(T_DATA& data)
    {
        return data.field_;
    }
};

//
// shortcut
//
template <int i, class T_DATA>
typename TypeAt<typename T_DATA::typelist, i>::Result&
Field(T_DATA& data)
{
    return DataField<i, T_DATA>::get(data);
}


//====================================================================
// CopyVisitor: Visitor that makes a copy.
//
// data1 = data2
//

template< class T_DATA >
class CopyVisitor;

template< class T_HEAD, class T_TAIL >
class CopyVisitor< MakeData< Typelist<T_HEAD, T_TAIL> > >
{
    typedef typename MakeData< Typelist<T_HEAD, T_TAIL> > Data;

public:
    static void VisitList(Data* data1, Data* data2)
    {
        T_HEAD& value1 = data1->field_;
        T_HEAD& value2 = data2->field_;
        value1 = value2;

        CopyVisitor< MakeData< T_TAIL > >::VisitList(data1, data2);
    }
};

template<>
class CopyVisitor< MakeData< Typelist<EmptyType, EmptyType> > >
{
public:
    static void VisitList(void* data1, void* data2)
    {
    }
};


//====================================================================
// AddVisitor: Visitor that adds one value to the other.
//
// data1 += data2
//

template< class T_DATA >
class AddVisitor;

template< class T_HEAD, class T_TAIL >
class AddVisitor< MakeData< Typelist<T_HEAD, T_TAIL> > >
{
    typedef typename MakeData< Typelist<T_HEAD, T_TAIL> > Data;

public:
    static void VisitList(Data* data1, Data* data2)
    {
        T_HEAD& value1 = data1->field_;
        T_HEAD& value2 = data2->field_;
        value1 = value1 + value2;

        AddVisitor< MakeData< T_TAIL > >::VisitList(data1, data2);
    }
};

template<>
class AddVisitor< MakeData< Typelist<EmptyType, EmptyType> > >
{
public:
    static void VisitList(void* data1, void* data2)
    {
    }
};


//====================================================================
// MultVisitor: Visitor that multiplays by a double scalar value.
//
// data1 *= factor
//

template< class T_DATA >
class MultVisitor;

template< class T_HEAD, class T_TAIL >
class MultVisitor< MakeData< Typelist<T_HEAD, T_TAIL> > >
{
    typedef typename MakeData< Typelist<T_HEAD, T_TAIL> > Data;

public:
    static void VisitList(Data* data1, double factor)
    {
        T_HEAD& value1 = data1->field_;
				value1 = boost::numeric_cast<T_HEAD>(value1*factor);

        MultVisitor< MakeData< T_TAIL > >::VisitList(data1, factor);
    }
};

template<>
class MultVisitor< MakeData< Typelist<EmptyType, EmptyType> > >
{
public:
    static void VisitList(void* data1, double factor)
    {
    }
};



#endif //makeData_h__