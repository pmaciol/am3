
/*! \file usr_upd.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareDeform_usr_upd_h__
#define am3softwareDeform_usr_upd_h__
/*	include *********************************************************/



/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace deform
		{
// 			int DEFORM_USRE1_GET(const int * length, double * values, const int * meshId, const int * elementId);
// 			int DEFORM_USRE1_SEND(const int * length, const double * values, const int * meshId, const int * elementId);
		} //deform
	} //software
} //am3

#endif // am3softwareDeform_usr_upd_h__
