/*! \file usr_msh_lib.cpp **************************************************
 * \author		Piotr Maciol
 * \brief     
 * \details   	
*********************************************************************/

#include "deformHandlerFacadeMesh.h"
#include "usr_msh.h"
 
namespace sdm = am3::software::deform;

namespace am3
{
  namespace software
  {
    namespace deform
    {
      int DEFORM_TEMPERATURES_SET(int materialId, double  time, double  timeStep, double  * temperatures, double  * deltaTemperatures, int nodeStart, int nodeEnd)
      {
				if (sdm::GetDeformHandlerFacadeMshTemperaturePtr())
        {
          sdm::GetDeformHandlerFacadeMshTemperaturePtr()->SetTemperaturesInNodes(materialId, time, timeStep, temperatures, deltaTemperatures, nodeStart, nodeEnd);
					return 0;
        }
				else
				{
					return 1;
				}
       
      } //deform
    } //software
  } //am3
}
