#include "deformHandlerFacade.h"
#include "../logger/logger.h"
#include "boost/shared_ptr.hpp"

namespace lo = am3::logger;

namespace am3
{
  namespace software
  {
    namespace deform
    {
      boost::shared_ptr<DeformHandlerFacadeMtr> deformHandlerFacadeMtr;
      boost::shared_ptr<DeformHandlerFacadeTime> deformHandlerFacadeTime;
      boost::shared_ptr<DeformHandlerFacadeElm> deformHandlerFacadeElm;
      boost::shared_ptr<DeformHandlerFacadeUpd> deformHandlerFacadeUpd;
      boost::shared_ptr<DeformHandlerFacadeMsh> deformHandlerFacadeMsh;
      boost::shared_ptr<DeformHandlerFacadeMshTemperatureUsrElemVal> deformHandlerFacadeMshTemperatureUsrElemVal;

      template<typename FACADE>
      bool CheckFacade(FACADE ptr, char* message)
      {
        if (ptr)	return true;
        else
        {
          lo::log<lo::SEV_CRITICAL>(lo::LOG_MODEL, message);
          return false;
        };
      };

      DeformHandlerFacadeMtr* GetDeformHandlerFacadeMtrPtr()
      {
        if (CheckFacade(deformHandlerFacadeMtr, "Deform Mtr handler not set")) return deformHandlerFacadeMtr.get();
        else return nullptr;
      };

      DeformHandlerFacadeUpd* GetDeformHandlerFacadeUpdPtr()
{
        if (CheckFacade(deformHandlerFacadeUpd, "Deform Upd handler not set")) return deformHandlerFacadeUpd.get();
        else return nullptr;
      };

      DeformHandlerFacadeTime* GetDeformHandlerFacadeTimePtr()
{
        if (CheckFacade(deformHandlerFacadeTime, "Deform Time handler not set")) return deformHandlerFacadeTime.get();
        else return nullptr;
      };

      DeformHandlerFacadeElm* GetDeformHandlerFacadeElmPtr()
{
        if (CheckFacade(deformHandlerFacadeElm, "Deform Elm handler not set")) return deformHandlerFacadeElm.get();
        else return nullptr;
      };

      DeformHandlerFacadeMsh* GetDeformHandlerFacadeMshPtr()
{
        if (CheckFacade(deformHandlerFacadeMsh, "Deform Msh handler not set")) return deformHandlerFacadeMsh.get();
        else return nullptr;
      };

      DeformHandlerFacadeMshTemperatureUsrElemVal* GetDeformHandlerFacadeMshTempElemPtr()
      {
        if (CheckFacade(deformHandlerFacadeMshTemperatureUsrElemVal, "Deform MshTemperature handler not set")) return deformHandlerFacadeMshTemperatureUsrElemVal.get();
        else return nullptr;
      };

    } //deform
  } //software
} //am3


  

      

