
/*! \file deformHandlerFacadeMesh.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       Interfaces 
 * \details   	
*********************************************************************/
#ifndef am3softwareDeform_deformHandlerFacadeMesh_h__
#define am3softwareDeform_deformHandlerFacadeMesh_h__
/*	include *********************************************************/
/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


namespace am3
{
  namespace software
  {
    namespace deform
    {
      class DeformHandlerFacadeMshTemperature
      {
      public:
        virtual ~DeformHandlerFacadeMshTemperature(){};

        virtual void SetTemperaturesInNodes(const int materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const int nodeStart, const int nodeEnd) = 0;
        virtual void SetConnectivityMatrix(const int meshId, const double time, const int maxElemement, const int * ConnectivityMatrix) = 0;
        virtual void SetUsrVar(const int meshId, const double time, const int uvNumber, const int maxElemement, const double * userVariables) = 0;
      protected:
        DeformHandlerFacadeMshTemperature(){};
      };

      DeformHandlerFacadeMshTemperature*  GetDeformHandlerFacadeMshTemperaturePtr();

      //******************************************************************************************************************************************************************

      class DeformHandlerFacadeMshTime
      {
      public:
        virtual ~DeformHandlerFacadeMshTime(){};

        virtual int SetStepLength(const double timeStepLength) = 0;
        virtual int SetTime(const double time) = 0;
      protected:
        DeformHandlerFacadeMshTime(){};
      };

      DeformHandlerFacadeMshTime* GetDeformHandlerFacadeMshTimePtr();



      class DeformHandlerFacadeMshTemperatureTime
        //        : public DeformHandlerFacadeUpd
        : public DeformHandlerFacadeMshTime
        , public DeformHandlerFacadeMshTemperature
      {};
    } //deform
  } //software
} //am3


#endif // am3softwareDeform_deformHandlerFacadeMesh_h__
