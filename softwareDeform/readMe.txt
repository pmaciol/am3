To link this solution with Deform:
* build case project as dll, debug or release
* run VS command prompt and change to this folder
* set variables in runMeDebug.bat or runMeRelease.bat
* run runMeDebug.bat or runMeRelease.bat
* copy proper usr_mtr.f, usr_upd.f and build_def_sim_usr_Absoftv110.bat to Deform User Routine Folder
* run Absoft console and change to Deform User Routine Folder
* build_def_sim_usr_Absoftv110.bat 
* follow Deform script instructions

To test:
* build project as dll, debug
* run VS command prompt and change to this folder
* run runMeDebug.bat
* in VS, set am3softwareDeform properties / Debugging / Command as $(ProjectDir)$(Configuration)\test.exe
* start debugging