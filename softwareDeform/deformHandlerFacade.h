
/*! \file deformHandlerFacadeMtr.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       Facades defines interfaces used on the side if Deform software. 
 * \details   	Classes processing Deform variables inside AM3 framework implements these interfaces. 
 Each of user-supplied file in Deform uses at least one Facade
*********************************************************************/
#ifndef am3softwareDeform_deformHandlerFacadeMtr_h__
#define am3softwareDeform_deformHandlerFacadeMtr_h__
/*	include *********************************************************/

#include "deformHandlerFacadeMesh.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace deform
		{
      class DeformHandlerFacadeMtr;
      class DeformHandlerFacadeTime;
      class DeformHandlerFacadeElm;
      class DeformHandlerFacadeUpd;
      class DeformHandlerFacadeMsh;
      class DeformHandlerFacadeMshTemperatureUsrElemVal;

      DeformHandlerFacadeMtr* GetDeformHandlerFacadeMtrPtr();
      DeformHandlerFacadeTime* GetDeformHandlerFacadeTimePtr();
      DeformHandlerFacadeElm*  GetDeformHandlerFacadeElmPtr();
      DeformHandlerFacadeUpd* GetDeformHandlerFacadeUpdPtr();
      DeformHandlerFacadeMsh* GetDeformHandlerFacadeMshPtr();
      DeformHandlerFacadeMshTemperatureUsrElemVal* GetDeformHandlerFacadeMshTempElemPtr();


      class DeformHandlerFacadeMtr
      {
      public:
        virtual double GetYieldStress(const int meshId, const int elementId) { return 0.0; };
        virtual double GetYieldStressDStrain(const int meshId, const int elementId) = 0;
        virtual double GetYieldStressDStrainRate(const int meshId, const int elementId) = 0;
        virtual int SetTemperature(const double* temperature, const int meshId, const int elementId) = 0;
        virtual int SetEffectiveStrainRate(const double* effStrainRate, const int meshId, const int elementId) = 0;
        virtual int SetEffectiveStrain(const double* effStrain, const int meshId, const int elementId) = 0;
      };

      //******************************************************************************************************************************************************************

      class DeformHandlerFacadeTime
      {
      public:
        virtual int SetStepLength(const double* timeStepLength, const int meshId, const int elementId) = 0;
        virtual int SetTime(const double* time, const int meshId, const int elementId) = 0;
      };

      //******************************************************************************************************************************************************************

      class DeformHandlerFacadeElm
      {
      public:
        virtual int SetElementalNodes(const int length, const double* values, const double time, const int meshId, const int elementId) = 0;
      };

      //******************************************************************************************************************************************************************

      class DeformHandlerFacadeUpd
      {
      public:
        virtual int SetElementalVariables(const int length, const double* values, const int meshId, const int elementId) = 0;
        virtual int GetElementalVariables(const int length, double* values, const int meshId, const int elementId) = 0;
        virtual double GetElementalVariables(const unsigned int whichVariable) = 0;
        virtual int SetGlobalVariables(const int length, const double* values, const int meshId, const int elementId) = 0; 
        virtual int SetTemperature(const double* temperature, const int meshId, const int elementId) = 0;
        virtual int SetStepLength(const double* timeStepLength, const int meshId, const int elementId) = 0;
        virtual int SetTime(const double* time, const int meshId, const int elementId) = 0;
      };

      //******************************************************************************************************************************************************************




      //******************************************************************************************************************************************************************



		} //deform
	} //software
} //am3

#endif // am3softwareDeform_deformHandlerFacadeMtr_h__
