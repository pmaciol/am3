#include "deformHandlerFacadeAM3side.h"

namespace am3
{
  namespace software
  {
    namespace deform
    {
      extern boost::shared_ptr<DeformHandlerFacadeMtr> deformHandlerFacadeMtr;
      extern boost::shared_ptr<DeformHandlerFacadeTime> deformHandlerFacadeTime;
      extern boost::shared_ptr<DeformHandlerFacadeElm> deformHandlerFacadeElm;
      extern boost::shared_ptr<DeformHandlerFacadeUpd> deformHandlerFacadeUpd;
      extern boost::shared_ptr<DeformHandlerFacadeMsh> deformHandlerFacadeMsh;
      extern boost::shared_ptr<DeformHandlerFacadeMshTemperatureUsrElemVal> deformHandlerFacadeMshTemperatureUsrElemVal;

      void SetDeformHandlerFacadeMshTemperatureUsrElemVal(boost::shared_ptr<DeformHandlerFacadeMshTemperatureUsrElemVal> facadeMsh)
      {
        deformHandlerFacadeMshTemperatureUsrElemVal = facadeMsh;
      }

      void SetDeformHandlerFacadeMtr(boost::shared_ptr<DeformHandlerFacadeMtr> facadeMtr)
      {
        deformHandlerFacadeMtr = facadeMtr;
      }

      void SetDeformHandlerFacadeTime(boost::shared_ptr<DeformHandlerFacadeTime> facadeTime)
      {
        deformHandlerFacadeTime = facadeTime;
      }

      void SetDeformHandlerFacadeElm(boost::shared_ptr<DeformHandlerFacadeElm> facadeElm)
      {
        deformHandlerFacadeElm = facadeElm;
      }

      void SetDeformHandlerFacadeUpd(boost::shared_ptr<DeformHandlerFacadeUpd> facadeUpd)
      {
        deformHandlerFacadeUpd = facadeUpd;
      }

      void SetDeformHandlerFacadeMsh(boost::shared_ptr<DeformHandlerFacadeMsh> facadeMsh)
      {
        deformHandlerFacadeMsh = facadeMsh;
      }

      void UnsetDeformHandlerFacadeMshTemperatureUsrElemVal()
      {
        deformHandlerFacadeMshTemperatureUsrElemVal = nullptr;
      }
      void UnsetDeformHandlerFacadeMtr()
      {
        deformHandlerFacadeMtr = nullptr;
      }
      void UnsetDeformHandlerFacadeTime()
      {
        deformHandlerFacadeTime = nullptr;
      }
      void UnsetDeformHandlerFacadeElm()
      {
        deformHandlerFacadeElm = nullptr;
      }
      void UnsetDeformHandlerFacadeUpd()
      {
        deformHandlerFacadeUpd = nullptr;
      }
      void UnsetDeformHandlerFacadeMsh()
      {
        deformHandlerFacadeMsh = nullptr;
      }
    } //deform
  } //software
} //am3


