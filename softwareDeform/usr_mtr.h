
/*! \file usr_mtr.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareDeform_usr_mtr_h__
#define am3softwareDeform_usr_mtr_h__
/*	include *********************************************************/



/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/
namespace am3
{
	namespace software
	{
		namespace deform
		{
// 			int DEFORM_YIELD_STRESS_GET(double * yieldStress, const int * meshId, const int * elementId);
// 			int DEFORM_YIELD_STRESSDSTRAIN_GET(double * yieldStressDstrain, const int * meshId, const int * elementId);
// 			int DEFORM_YIELD_STRESSDSTRAINRATE_GET(double * yieldStressDstrainRate, const int * meshId, const int * elementId);
// 			int DEFORM_ALL_STRESS_GET(double * yieldStress, double * yieldStressDstrain, double * yieldStressDstrainRate, const int * meshId, const int * elementId);
// 			int DEFORM_TEMPERATURE(const double * temperature, const int * meshId, const int * elementId);
// 			int DEFORM_EFF_STR_RATE(const double * effStrainRate, const int * meshId, const int * elementId);
// 			int DEFORM_EFF_STR(const double * effStrain, const int * meshId, const int * elementId);
		} //deform
	} //software
} //am3
#endif // am3softwareDeform_usr_mtr_h__
