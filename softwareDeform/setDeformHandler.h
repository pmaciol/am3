
/*! \file setDeformHandler.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareDeform_setDeformHandler_h__
#define am3softwareDeform_setDeformHandler_h__
/*	include *********************************************************/

#include "deformHandlerFacadeMesh.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

//am3::software::deform::DeformHandlerFacadeMshTemperature*  SetDeformHandlerFacadeMshTemperaturePtr();

#endif // am3softwareDeform_setDeformHandler_h__
