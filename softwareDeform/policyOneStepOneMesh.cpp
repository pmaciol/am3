/*! \file policyOneStepOneMesh.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "policyOneStepOneMesh.h"
#include "../models/dataStructures.h"
#include "../models/description.h"
#include "../logger/logger.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace lo = am3::logger;
namespace mo = am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/
// 
// double am3::software::deform::OneStepOneMeshUserValues::GetInternalVariable(const unsigned elemId, std::size_t index) const
// {
//   if (elemId < userVariables_.size() && index < userVariables_.begin()->size())
//   {
//     return (userVariables_[elemId].at(index));
//   }
//   else
//   {
//     lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Element number outside range");
//     return std::numeric_limits<double>::signaling_NaN();
//   }
// }
// 
// int am3::software::deform::OneStepOneMeshUserValues::SetAllElementalVariables(double const * usrElemVar, const int * materialId, const int * numUsrElemVar, const int* numElements)
// {
//   userVariables_.clear();
//   for (int i = 0; i < *numElements; i++)
//   {
//     std::vector<double> tempVec;
//     for (int j = 0; j < *numUsrElemVar; j++)
//     {
//       tempVec.push_back(usrElemVar[j + i * (*numUsrElemVar)]); // Check i,j or j,i?
//     }
//     userVariables_.push_back(tempVec);
//   }
//   return true;
// }
// 
// unsigned am3::software::deform::OneStepOneMeshUserValues::NumberOfElements() const
// {
//   throw std::logic_error("The method or operation is not implemented.");
// }
// 
// unsigned am3::software::deform::OneStepOneMeshUserValues::NumberOfIvs() const
// {
//   throw std::logic_error("The method or operation is not implemented.");
// }
// 
// mpr::Temperature am3::software::deform::OneStepOneMeshTemperatureElem::GetOldTemperature(const unsigned elemId) const
// {
//   mpr::Temperature toRet;
//   if (elemId < oldTemperatures_.size())
//   {
//     toRet(oldTemperatures_[elemId]);
//   }
//   else
//   {
//     lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Element number outside range");
//     toRet = mo::WrongVariable< mpr::Temperature>();
//   }
//   return toRet;
// }
// 
// mpr::Temperature am3::software::deform::OneStepOneMeshTemperatureElem::GetNewTemperature(const unsigned elemId) const
// {
//   mpr::Temperature toRet;
//   if (elemId < newTemperatures_.size())
//   {
//     toRet(newTemperatures_[elemId]);
//   }
//   else
//   {
//     lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Element number outside range");
//     toRet = mo::WrongVariable< mpr::Temperature>();
//   }
//   return toRet;
// }


// int am3::software::deform::OneStepOneMeshTemperature::SetTemperaturesInNodes(double const * temperatures, double const * deltaTemperatures, const unsigned int materialId, const unsigned int numElements, const unsigned int nodesPerElement, const int* connectivityMatrix)
// {
//   oldTemperatures_.clear();
//   newTemperatures_.clear();
// 
//   for (unsigned i = 0; i < numElements;i++)
//   {
//     double oldTemperature = 0;
//     double newTemperature = 0;
//     double deltaTemperature = 0;
//     for (unsigned j = 0; j < nodesPerElement; j++)
//     {
//       unsigned node = connectivityMatrix[j + i * (nodesPerElement)]; // Check i,j or j,i?
// 
//       oldTemperature += temperatures[node];
//       deltaTemperature += deltaTemperatures[node];
//     }
//     oldTemperature /= nodesPerElement;
//     deltaTemperature /= nodesPerElement;
//     newTemperature = oldTemperature + deltaTemperature;
//     oldTemperatures_.push_back(oldTemperature);
//     newTemperatures_.push_back(newTemperature);
//   }
//   return true;
// }
// 
// void am3::software::deform::OneStepOneMeshTemperatureElem::SetTemperaturesInNodes(const int * materialId, double const * temperatures, double const * deltaTemperatures, const unsigned* numberOfNodes)
// {
//   newTemperatures_.clear();
//   oldTemperatures_.clear();
//   numberOfNodes_ = *numberOfNodes;
//   for (unsigned i = 0; i < numberOfNodes_; i++)
//   {
//     newTemperatures_.push_back(temperatures[i] + deltaTemperatures[i]);
//     oldTemperatures_.push_back(temperatures[i]);
//   }
// }
// 
// unsigned am3::software::deform::OneStepOneMeshTemperatureElem::GetNumberOfNodes() const
// {
//   return numberOfNodes_;
// }

int am3::software::deform::OneStepOneMeshTime::SetStepLength(const double timeStepLength)
{
  timeStep_ = timeStepLength;
  return true;
}

int am3::software::deform::OneStepOneMeshTime::SetTime(const double time)
{
  time_ = time;
  return true;
}

mpr::StepLength am3::software::deform::OneStepOneMeshTime::GetStepLength() const
{
  mpr::StepLength toRet;
  toRet(timeStep_);
  return toRet;
}

mpr::Time am3::software::deform::OneStepOneMeshTime::GetTime() const
{
  mpr::Time toRet;
  toRet(time_);
  return toRet;
}

mpr::Time am3::software::deform::OneStepOneMeshTime::GetNewTime() const
{
  mpr::Time toRet;
  toRet(time_ + timeStep_);
  return toRet;
}

// 
// int am3::software::deform::OneStepOneMeshTemperaturesUserValues::SetAllElementalVariables(double const * usrElemVar, const int * materialId, const int * numUsrElemVar, const int* numElements)
// {
//   return OneStepOneMeshUserValues::SetAllElementalVariables(usrElemVar, materialId, numUsrElemVar, numElements);
// }
// 
// int am3::software::deform::OneStepOneMeshTemperaturesUserValues::SetStepLength(const double* timeStepLength)
// {
//   return OneStepOneMeshTime::SetStepLength(timeStepLength);
// }
// 
// int am3::software::deform::OneStepOneMeshTemperaturesUserValues::SetTime(const double* time)
// {
//   return OneStepOneMeshTime::SetTime(time);
// }

// int am3::software::deform::OneStepOneMeshTemperaturesUserValues::SetTemperaturesInElements(double const * temperatures, double const * deltaTemperatures, const int * materialId, const int* numElements)
// {
//   return OneStepOneMeshTemperature::SetAllTemperatures(temperatures, deltaTemperatures, materialId, numElements);
// }

// int am3::software::deform::OneStepOneMeshTemperaturesUserValues::SetTemperaturesInNodes(double const * temperatures, double const * deltaTemperatures, const unsigned int materialId, const unsigned int numElements, const unsigned int nodesPerElement, const int* connectivityMatrix)
// {
//   return OneStepOneMeshTemperature::SetTemperaturesInNodes(temperatures, deltaTemperatures, materialId, numElements, nodesPerElement, connectivityMatrix);
// }
