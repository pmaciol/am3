
/*! \file oneStepOneMeshTemperature.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareDeform_oneStepOneMeshTemperature_h__
#define am3softwareDeform_oneStepOneMeshTemperature_h__
/*	include *********************************************************/

#include "../modelTemplates/innerSideInterfacesMacro.h"
#include "deformHandlerFacadeMesh.h"

/*	using ***********************************************************/

namespace mot = am3::model::templates;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace deform
    {
      class OneStepOneMeshTemperatureTime
        : public mot::OneStepOneMeshTemperatureInNodes
        , public mot::OneStepOneMeshTime
        , public DeformHandlerFacadeMshTemperature
//         , public DeformHandlerFacadeMshTemperature
      {
      public:
        ~OneStepOneMeshTemperatureTime(){};

        // from mot::OneStepOneMeshTemperatureInNodes
        virtual mpr::Temperature GetOldTemperature(const unsigned nodeId) const override;
        virtual mpr::Temperature GetNewTemperature(const unsigned nodeId) const override;
        virtual unsigned GetNodeStart() const override;
        virtual unsigned GetNodeLast() const override;
        virtual unsigned GetNodesNumber() const override;

        //from mot::OneStepOneMeshTime
        virtual mpr::StepLength GetStepLength() const override;
        virtual mpr::Time GetTime() const override;
        virtual mpr::Time GetNewTime() const override;

        //from DeformHandlerFacadeMshTemperature
        virtual void SetTemperaturesInNodes(const int materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const int nodeStart, const int nodeEnd) override;

      private:
        std::vector<double> oldTemperatures_;
        std::vector<double> newTemperatures_;
        unsigned nodeStart_, nodeEnd_;
        double time_, timeStep_;

        bool CheckNodeInRange(const unsigned nodeId) const;
        unsigned int CalculateNodeIndex(const unsigned nodeId) const;
      };
    }
  }
}


#endif // am3softwareDeform_oneStepOneMeshTemperature_h__
