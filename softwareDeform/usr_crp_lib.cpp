/*! \file usr_crp_lib.cpp **************************************************
 * \author		Piotr Maciol
 * \brief     Provides DEFORM_YS method for deform usr_mtr procedures
 * \details   	
*********************************************************************/
#include "deformHandlerFacade.h"
#include <fstream>

// extern am3::software::deform::DeformHandlerFacaderp* deformHandlerProcCrp;
// extern void CreateDeformHandler();

using namespace std;

// __declspec( dllexport ) int DEFORM_CREEP_RATE_GET(double * creepRate,
//   const double* effectiveStress,
//   const double* temperature,
//   const double* effectiveStrain,
// 	const int * meshId,
// 	const int * elementId)
// {
// 	if (SecureDeformHandlerExistance())
// 	{
//     *creepRate = deformHandlerProcCrp->GetCreepRate(* effectiveStress,* temperature,* effectiveStrain, *meshId, *elementId);
// 		return 0;
// 	}	
// 	else return -1;
// }
// 
// __declspec( dllexport ) int DEFORM_CREEP_RATE_DSTRAIN_GET(double * creepRate,
//   const double* effectiveStress,
//   const double* temperature,
//   const double* effectiveStrain,
//   const int * meshId,
//   const int * elementId)
// {
//   if (SecureDeformHandlerExistance())
//   {
//     *creepRate = deformHandlerProcCrp->GetCreepRateDStrain(* effectiveStress,* temperature,* effectiveStrain, *meshId, *elementId);
//     return 0;
//   }	
//   else return -1;
// }
