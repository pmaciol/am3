

#include "../modelTemplates/innerSideInterfacesMacro.h"
#include "deformHandlerFacadeMesh.h"
namespace mot = am3::model::templates;

namespace am3
{
  namespace software
  {
    namespace deform
    {
      class OneStepOneMeshTemperature
        : public mot::OneStepOneMeshTemperature
        , public DeformHandlerFacadeMshTemperature
      {
      public:
        ~OneStepOneMeshTemperature(){};

        virtual mpr::Temperature GetOldTemperature(const unsigned nodeId) const override;
        virtual mpr::Temperature GetNewTemperature(const unsigned nodeId) const override;
        virtual unsigned GetNodeStart() const override;
        virtual unsigned GetNodeLast() const override;
        virtual unsigned GetNodesNumber() const override;

        virtual void SetTemperaturesInNodes(const int materialId, double const * temperatures, double const * deltaTemperatures, const unsigned nodeStart, const unsigned nodeEnd) override;

      private:
        std::vector<double> oldTemperatures_;
        std::vector<double> newTemperatures_;
        unsigned nodeStart_, nodeEnd_;

        bool CheckNodeInRange(const unsigned nodeId) const;
        unsigned int CalculateNodeIndex(const unsigned nodeId) const;
      };

    }
  }
}