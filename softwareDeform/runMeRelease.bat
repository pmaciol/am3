set "case=am3caseTugMatCalc"
set "dir=caseTugMatCalc"
set "absoftdir=d:\pmaciol\Deform\absoft11\"
set deformdir="d:\Program Files\SFTC\DEFORM\v10.2.1\2D\"

del test.obj
del deformSide.obj
cd ..\..\bin_2013\softwareDeform\Release
copy ..\..\%dir%\Release\%case%.lib .\.
copy ..\..\%dir%\Release\%case%.dll %deformdir%%case%.dll
cl -c /Gd ..\..\..\src\softwareDeform\deformSide.cpp
lib /OUT:%case%.lib deformSide.obj %case%.lib
copy %case%.lib %absoftdir%lib\

cd ..\..\..\src\softwareDeform\