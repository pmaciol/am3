
/*! \file usr_msh.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareDeform_usr_msh_h__
#define am3softwareDeform_usr_msh_h__
/*	include *********************************************************/
/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
  	namespace deform
  	{
 //     int DEFORM_TEMPERATURES_SET(int materialId, double  time, double  timeStep, double  * temperatures, double  * deltaTemperatures, int nodeStart, int nodeEnd);
  	} //deform
  } //software
} //am3
#endif // am3softwareDeform_usr_msh_h__
