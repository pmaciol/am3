
/*! \file usr_mtr_lib.cpp **************************************************
 * \author		Piotr Maciol
 * \brief     Provides DEFORM_YS method for deform usr_mtr procedures
 * \details   	
*********************************************************************/
#include "deformHandlerFacade.h"
#include "usr_mtr.h"

namespace sdm = am3::software::deform;

using namespace std;

namespace am3
{
	namespace software
	{
		namespace deform
		{

			int DEFORM_YIELD_STRESS_GET(double * yieldStress,
				const int * meshId,
				const int * elementId)

			{
				if (sdm::GetDeformHandlerFacadeMtrPtr())
				{
					*yieldStress = sdm::GetDeformHandlerFacadeMtrPtr()->GetYieldStress(*meshId, *elementId);
					return 0;
				}
				else return -1;
			}


			int DEFORM_YIELD_STRESSDSTRAIN_GET(double * yieldStressDstrain,
				const int * meshId,
				const int * elementId)
			{
				if (sdm::GetDeformHandlerFacadeMtrPtr())
				{
					*yieldStressDstrain = sdm::GetDeformHandlerFacadeMtrPtr()->GetYieldStressDStrain(*meshId, *elementId);
					return 0;
				}
				else return -1;
			}

			int DEFORM_YIELD_STRESSDSTRAINRATE_GET(double * yieldStressDstrainRate,
				const int * meshId,
				const int * elementId)
			{
				if (sdm::GetDeformHandlerFacadeMtrPtr())
				{
					*yieldStressDstrainRate = sdm::GetDeformHandlerFacadeMtrPtr()->GetYieldStressDStrainRate(*meshId, *elementId);
					return 0;
				}
				else return -1;
			}

			int DEFORM_TEMPERATURE(const double * temperature,
				const int * meshId,
				const int * elementId)
			{
				if (sdm::GetDeformHandlerFacadeMtrPtr()) return sdm::GetDeformHandlerFacadeMtrPtr()->SetTemperature(temperature, *meshId, *elementId);
				else return -1;
			}

			int DEFORM_EFF_STR_RATE(const double * effStrainRate,
				const int * meshId,
				const int * elementId)
			{
				if (sdm::GetDeformHandlerFacadeMtrPtr()) return sdm::GetDeformHandlerFacadeMtrPtr()->SetEffectiveStrainRate(effStrainRate, *meshId, *elementId);
				else return -1;
			}

			int DEFORM_EFF_STR(const double * effStrain, const int * meshId, const int * elementId)
			{
				if (sdm::GetDeformHandlerFacadeMtrPtr()) return sdm::GetDeformHandlerFacadeMtrPtr()->SetEffectiveStrain(effStrain, *meshId, *elementId);
				else return -1;
			}

			int DEFORM_ALL_STRESS_GET(double * yieldStress, double * yieldStressDstrain, double * yieldStressDstrainRate, const int * meshId, const int * elementId)
			{
				if (sdm::GetDeformHandlerFacadeMtrPtr())
				{
					*yieldStress = sdm::GetDeformHandlerFacadeMtrPtr()->GetYieldStress(*meshId, *elementId);
					*yieldStressDstrain = sdm::GetDeformHandlerFacadeMtrPtr()->GetYieldStressDStrain(*meshId, *elementId);
					*yieldStressDstrainRate = sdm::GetDeformHandlerFacadeMtrPtr()->GetYieldStressDStrainRate(*meshId, *elementId);
					return 0;
				}
				else return -1;
			}

		} //deform
	} //software
} //am3
