
/*! \file usr_upd_lib.cpp **************************************************
 * \author		Piotr Maciol
 * \brief     Provides methods for deform usr_upd procedures
 * \details
 *********************************************************************/

#include "deformHandlerFacade.h"
#include "usr_upd.h"

namespace sdm = am3::software::deform;

namespace am3
{
	namespace software
	{
		namespace deform
		{
			int DEFORM_USRE1_SEND(const int * length, const double * values, const int * meshId, const int * elementId)
			{
				if (sdm::GetDeformHandlerFacadeUpdPtr())
				{
					sdm::GetDeformHandlerFacadeUpdPtr()->SetElementalVariables(*length, values, *meshId, *elementId);
					return 0;
				} 
				else
				{
					return -1;
				}
			}

			int DEFORM_USRE1_GET(const int * length, double * values, const int * meshId, const int * elementId)
			{
				if (sdm::GetDeformHandlerFacadeUpdPtr())
				{
					sdm::GetDeformHandlerFacadeUpdPtr()->GetElementalVariables(*length, values, *meshId, *elementId);
					return 0;
				} 
				else
				{
					return -1;
				}
			}

		} //deform
	} //software
} //am3

