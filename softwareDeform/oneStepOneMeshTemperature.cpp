
#include <stdexcept>
#include "oneStepOneMeshTemperature.h"

mpr::Temperature am3::software::deform::OneStepOneMeshTemperature::GetOldTemperature(const unsigned nodeId) const
{
  if ((nodeId < nodeStart_) || (nodeId > nodeEnd_)) throw std::out_of_range("nodeId outside range");
  mpr::Temperature toRet;
  toRet(oldTemperatures_[CalculateNodeIndex(nodeId)]);
  return toRet;
}

mpr::Temperature am3::software::deform::OneStepOneMeshTemperature::GetNewTemperature(const unsigned nodeId) const
{
  if ((nodeId < nodeStart_) || (nodeId > nodeEnd_)) throw std::out_of_range("nodeId outside range");
  mpr::Temperature toRet;
  toRet(newTemperatures_[CalculateNodeIndex(nodeId)]);
  return toRet;
}

unsigned am3::software::deform::OneStepOneMeshTemperature::GetNodeStart() const
{
  return nodeStart_;
}

unsigned am3::software::deform::OneStepOneMeshTemperature::GetNodeLast() const
{
  return nodeEnd_;
}

unsigned am3::software::deform::OneStepOneMeshTemperature::GetNodesNumber() const
{
  if (nodeEnd_ < nodeStart_) throw std::invalid_argument("End node number lower than start node number");
  return nodeEnd_ - nodeStart_;
}

void am3::software::deform::OneStepOneMeshTemperature::SetTemperaturesInNodes(const int materialId, double const * temperatures, double const * deltaTemperatures, const unsigned nodeStart, const unsigned nodeEnd)
{
  newTemperatures_.clear();
  oldTemperatures_.clear();
  nodeStart_ = nodeStart;
  nodeEnd_ = nodeEnd;
  if (nodeEnd_ < nodeStart_) throw std::invalid_argument("End node number lower than start node number");
  for (unsigned i = nodeStart_; i <= nodeEnd_; i++)
  {
    newTemperatures_.push_back(temperatures[i] + deltaTemperatures[i]);
    oldTemperatures_.push_back(temperatures[i]);
  }
}
