C************************************************************
C
      SUBROUTINE USRMSH(RZ,DRZ,URZ,TEMP,DTMP,
     +     FRZA,FRZB,EFSTS,EFEPS,TEPS,
     +     RDTY,STS,EPS,DCRP,TSRS,DAMG,USRVE,USRVN,ATOM,HEATND,EPRE,
     +     VOLT,WEAR,
     +     HDNS,VF,DVF,VFN,TICF,GRAIN,
     +     CURTIM,DTMAXC,
     +     NBCD,NBCDT,NOD,MATR,NBDRY,KOBJ,NUMEL,NUMNP,
     +     NDSTART,NDEND,NEDGE,NUSRVE,NUSRND,NTMATR,
     +     ISTATUS,NROUTINE,NTRELN,NGRNVAL,KSTEP,AVGSRT,SRTLMT,AXMT,
     &     IELMNOD,EFEPS_NP,TEPS_NP,DAMG_NP,STS_NP)
C
C************************************************************
C
C       All FIELD VARIABLES CAN BE CHANGED!
C
C       IMPROPER CHANGE MADE IN THIS ROUTINE WILL CAUSE PROBLEMS 
C       IN THE ANALYSIS.
C
C       PLEASE USE THIS ROUTINE WITH CAUTION!!
C
C************************************************************
C
C      This routine will be called at the beginning of the step and
C           at the end of the step
C
C      Object with FEM mesh will be passed to this Routine
C

C      REAL*8 array
C
C      RZ(2,NUMNP): Nodal Coordinateis
C      DRZ(2,NUMNP): Nodal displacemnts
C      URZ(2,NUMNP): Nodal Velocities
C      TEMP(NUMNP) : Nodal temperatures
C      DTMP(NUMNP) : Nodal temperature change for the step
C      FRZA(2,NUMNP): Nodal Point external forces
C      FRZB(2,NUMNP): Nodal point reaction forces
C      EFSTS(NUMEL): Effective stress
C      EFEPS(NUMEL): Effective strain rate
C      TEPS(NUMEL) : Total plastic strain
C      RDTY(NUMEL) : Element Densities
C      STS (4,NUMEL): Stress tensors
C      EPS (4,NUMEL): Strain rate tensors
C      DCRP(4,NUMEL): Creep rate tensors
C      TSRS(4,NUEML): Strain Components
C      DAMG(NUMEL)  : Damages 
C      USRVE(NUSRVE,NUMEL): User Defined Element Variables
C      NUSRVE: Number of User Defined Element Variables
C                (Must be declared in the Pre-Processor)
C      USRVN(NUSRND,NUMEL): User Defined Nodal Variables
C      NUSRND: Number of User defined Node variables 
C                (must be declared in the Pre-processor)
C
C      ATOM(NUMNP): Dominating Atom Contents
C      HEATND(NUMNP): Nodal Heat Source
C
C      EPRE(NEDGE): Pressure on the boundary edges 
C                   (based on the boundary node list:NBDRY)
C                   (Positive - tension, Negative-compressive)
C
C      VOLT(NUMNP): Voltage at Node (vailable Only for Heating Module)
C
C      WEAR(5,NUMNP): Die wear related Data
C         WEAR(1)      : Node Area 
C         WEAR(2)      : Contact Temperature
C         WEAR(3)      : Sliding Velocity 
C         WEAR(4)      : Shear Stress
C         WEAR(5)      : Contact Pressure
C
C      MICRO-STRUCTURE RELATED VARIABLE
C
C        Available ONLY for HT application
C
C        HDNS(2,*): Hardness
C        VF(NTMATR,*): Volume Fraction
C        VFN(NTMATR,*): Transformation Starting Volume Fraction
C        DVF(NTRELN,*): Transformation Volume Fraction Change Ammount
C        TICF(NTRELN,*): Incubation Time
C        GRAIN(NGRNVAL,*): Grain Size
C
C      REAL*8 Variables
C
C      CURTIM: Current Time
C      DTMAXC: Time Step
C      AVGSRT: Average strain rate
C      SRTLMT: Limiting Strain rate
C
C      Integer*4 Integer Array
C
C      NBCD(2,NUMNP): Nodal Boundary Condition
C                     0- Traction specified  => FRZA
C                     1- prescribed Velocity => URZ
C                     2- Pressure Specified  => EPRE
C      NBCDT(NUMNP): Temperature Boundary Condition
C                     0- Prescribed Nodal heat
C                     1- Prescribed Nodal Temperature
C
C      NOD(4,NUMEL): Element Connectivity (global node numbering)
C      MATR(*)     : Material Group Number:
C      NBDRY(NEDGE): Boundary Node list (Global node numbering)
C
C      Interger*4 Integer Variables
C
C      KOBJ : Current Object number
C
C      NUMEL: Total Number of Elements of KOBJ
C      NUMNP: Total Number of Nodes of KOBJ
C      NDSTART: Starting Node Number of KOBJ
C      NDEND  : Ending Node Number of KOBJ
C
C      NEDGE: Total Number of Boundary edges of KOBJ
C      NTMATR: Total Number of Materials
C      NTRELN: Total Number of Inter-materail relations
C      NGRNVAL: Number of Grain-related Variables
C      NROUTINE: User Controlled Routine Number
C
C      ISTATUS: 0 -> Called at the beginning of each step prior to the analysis
C               1 -> Called at the end of each step prior to writing to database
C
C     AXMT : material axis rotation
C
C     NODAL DEFINITION OPTIONS
C       IELMNOD(K)  = 0 : Element definition
C                   > 0 : Node+element definition
C         K = 1,2,3    -- REFERRING TO DAMAGE, EFF. STRAIN AND STRESS
C                         COMPONENTS (ELASTOPLASTIC OBJECT), RESPECTIVELY
C       EFEPS_NP(NUMNP) : Nodal eff. strain rate 
C       TEPS_NP(NUMNP)  : Nodal eff. strain
C       DAMG_NP(NUMNP)  : Nodal damage factor
C       STS_NP(NUMNP)   : Nodal stress components (elastoplastic object)              
C
C	For example for a given meshed object (either plastic or 
C	elasto-plastic)	once user turns on option to compute
C	the nodal strains (in the Pre Processor > Simulation controls > 
C	Advanced > Output control) the stored values of the nodal strain
C	strain rates can be extracted from the arrays TEPS_NP and
C	EFEPS_NP. For example the following statement assigns 
C	nodal effective strain for the node 123 to the variable RN123
C	RN123 = TEPS_NP(123)	
C
      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)

      COMMON /PLDSRK/ PDIE_SRK(2),PDIE_LD(2),PDIE_VEL(2)
C .. PDIE_SRK(1:3): x- and y- Strokes of P_DIE
C .. PDIE_LD(1:3): x- and y- Forces of P_DIE
C .. PDIE_VEL(1:3): x- and y- velocity of P_DIE
      COMMON /IDIMEN/ NUMSTN,NUMSTS
C .. NUMSTS : TOTAL NUMBER OF STRESS COMPONENTS PER ELEMENT
C      Torsional Element : NUMSTS=6, otherwise NUMSTS=4
C .. NUMSTN : TOTAL NUMBER OF STRAIN COMPONENTS PER ELEMENT
C     Ex) "Elastic","Plastic", and "Thermal" strains are selected for strain output
C       NUMSTN = NUMSTS + NUMSTS + 1 = 9    =>  TSRS(9,*)
C
       DIMENSION RZ(2,*),DRZ(2,*),URZ(2,*),TEMP(*),DTMP(*)
       DIMENSION FRZA(2,*),FRZB(2,*)
       DIMENSION EFSTS(*),EFEPS(*),TEPS(*)
       DIMENSION STS(4,*),EPS(4,*),DCRP(4,*),TSRS(NUMSTN,*)
       DIMENSION DAMG(*),ATOM(*),HDNS(2,*)
       DIMENSION NOD(4,*)
       DIMENSION RDTY(*),VOLT(*)
       DIMENSION USRVN(NUSRND,*)
       DIMENSION USRVE(NUSRVE,*)
       DIMENSION EPRE(*),HEATND(*)
       DIMENSION NBDRY(*),MATR(*)
       DIMENSION WEAR(5,*)
C
       DIMENSION NBCD(2,*),NBCDT(*)
C
       DIMENSION VF(NTMATR,*),VFN(NTMATR,*)
       DIMENSION DVF(NTRELN,*),TICF(NTRELN,*)
       DIMENSION AXMT(*),GRAIN(NGRNVAL,*)
       DIMENSION IELMNOD(*)
       DIMENSION EFEPS_NP(*),TEPS_NP(*),DAMG_NP(*),STS_NP(4,*)
       DIMENSION IN(4)
       DATA IN/1,3,4,2/
       integer*4 DEFORMMTRCRESULT
Const unsigned materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const unsigned nodeStart, const unsigned nodeEnd
c       IF (ISTATUS.EQ.0) RETURN
       IF (ISTATUS.EQ.0) THEN
      DEFORMMTRCRESULT=CALL_DEFORM_MESH_SET(MATR,CURTIM,NUMEL,NOD)
      DEFORMMTRCRESULT=CALL_DEFORM_TEMPERATURES_SET(
     +MATR,CURTIM,DTMAXC,TEMP,DTMP,NDSTART,NDEND) 
      ENDIF
c	  DEFORMMTRCRESULT=CALL_DEFORM_MESH_SET(
c     +RZ,DRZ,CURTIM,DTMACX,NUMEL,NUMNP,NOD)
c      ENDIF
CC       IF (NROUTINE.EQ.0) RETURN
C
       DO 100 NODE=NDSTART,NDEND
C
C       IF (ABS(WEAR(3,NODE)).GT.1.E-10) THEN
C
C        The node is contacting with other object
C
C        USRVN(1,NODE): TOTAL CONTACT TIME
C        USRVN(2,NODE): TOTAL SLIDING DISPLACEMENTS
C
C     USRVN(1,NODE)=USRVN(1,NODE)+DTMAXC
C     USRVN(2,NODE)=USRVN(2,NODE)+WEAR(3,NODE)*DTMAXC
C
C       ENDIF

100    CONTINUE
C
C  EXAMPLE FOR NODAL STRAIN CALCULATION, SAVING IT TO USRVN(1)
C
C%%       IF(IELMNOD2.GT.0) THEN
C%%          DO N  = NDSTART, NDEND
C%%             IF(EFEPS_NP(N).GE.SRTLMT) THEN
C%%                USRVN(1,N)=USRVN(1,N)+EFEPS_NP(N)*DTMAXC
C%%             ENDIF
C%%          ENDDO
C%%       ENDIF
       RETURN
       END
C======================================================================
