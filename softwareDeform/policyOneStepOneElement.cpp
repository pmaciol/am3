#include <limits>

#include "policyOneStepOneElement.h"

#if defined(_WIN32) || defined(_WIN64)
#pragma warning( disable : 4996)
#include <Windows.h>
#endif


namespace am3
{
  namespace software
  {
    namespace deform
    {
        

        mpr::Temperature OneStepOneElementNoElementCheckStresses::GetTemperature() const
        {
          return temperature_;
        }

        mpr::TotalEffectiveStrain OneStepOneElementNoElementCheckStresses::GetEffectiveStrain() const
        {
          return totEffStrainRate_;
        }

        mpr::EffectiveStrainRate OneStepOneElementNoElementCheckStresses::GetEffectiveStrainRate() const
        {
          return effStrainRate_;
        }

//         int OneStepOneElementNoElementCheck::SetElementalNodes(const int length, const double* values, const double time, const int meshId, const int elementId)
//         {
//           throw std::logic_error("The method or operation is not implemented.");
//         }

        double OneStepOneElementNoElementCheckStresses::GetYieldStress(const int meshId, const int elementId)
        {
          try
          {
            if (!stressesHandler_) throw "stressesHandler_ not set";
            return stressesHandler_->GetStress().Value();
          }
          catch (const char* msg)
          {
            lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, msg);
            return std::numeric_limits<double>::signaling_NaN();
          };
        }

        double OneStepOneElementNoElementCheckStresses::GetYieldStressDStrain(const int meshId, const int elementId)
        {
          try
          {
            if (!stressesHandler_) throw "stressesHandler_ not set";
            return stressesHandler_->GetStressDStrain().Value();
          }
          catch (const char* msg)
          {
            lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, msg);
            return std::numeric_limits<double>::signaling_NaN();
          };
        }

        double OneStepOneElementNoElementCheckStresses::GetYieldStressDStrainRate(const int meshId, const int elementId)
        {
          try
          {
            if (!stressesHandler_) throw "stressesHandler_ not set";
            return stressesHandler_->GetStressDStrainRate().Value();
          }
          catch (const char* msg)
          {
            lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, msg);
            return std::numeric_limits<double>::signaling_NaN();
          };
        }

        int OneStepOneElementNoElementCheckStresses::SetTemperature(const double* temperature, const int meshId, const int elementId)
        {
          temperature_(*temperature);
          return true;
        }

        int OneStepOneElementNoElementCheckStresses::SetEffectiveStrainRate(const double* effStrainRate, const int meshId, const int elementId)
        {
          effStrainRate_(*effStrainRate);
          return true;
        }

        int OneStepOneElementNoElementCheckStresses::SetEffectiveStrain(const double* effStrain, const int meshId, const int elementId)
        {
          totEffStrainRate_(*effStrain);
          return true;
        }

        mpr::Time OneStepOneElementNoElementCheckStresses::GetTime() const
        {
          return time_;
        }

        mpr::StepLength OneStepOneElementNoElementCheckStresses::GetStepLength() const
        {
          return stepLength_;
        }

        int OneStepOneElementNoElementCheckStresses::SetStepLength(const double* timeStepLength, const int meshId, const int elementId)
        {
          stepLength_(*timeStepLength);
          return true;
        }

        int OneStepOneElementNoElementCheckStresses::SetTime(const double* time, const int meshId, const int elementId)
        {
          time_(*time);
          return true;
        }

//         void OneStepOneElementNoElementCheckStresses::SetStressesHandler(boost::shared_ptr<mot::OneStepOneElementMacroStressWithDerivatives> val)
//         {
//           stressesHandler_ = val;
//         }

//         std::vector<mpt::Point<mpr::Coordinates2D>*> OneStepOneElementMacroStateProviderElement2D::GetNodes() const
//         {
//           throw std::logic_error("The method or operation is not implemented.");
//         }

        std::vector<double> OneStepOneElementNoElementCheckUserValues::GetInternalVariables() const
        {
          return ivValues_;
        }

        double OneStepOneElementNoElementCheckUserValues::GetInternalVariable(std::size_t index) const
        {
          double toRet = std::numeric_limits<double>::quiet_NaN();
          try
          {
            if ((index + 1) > ivValues_.size())
            {
              throw "IV from outside of range requested";
            }
            else toRet = ivValues_[index];
          }
          catch (const char* msg)
          {
            lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, msg);
          }

          return toRet;
          
        }

        int OneStepOneElementNoElementCheckUserValues::SetElementalVariables(const int length, const double* values, const int meshId, const int elementId)
        {
          ivValues_.clear();
          ivValues_.insert(ivValues_.end(), values, values + length);
          elemNumber_.operator()(elementId);
          return true;
        }

        int OneStepOneElementNoElementCheckUserValues::SetGlobalVariables(const int length, const double* values, const int meshId, const int elementId)
        {
          throw std::logic_error("The method or operation is not implemented.");
        }

        int OneStepOneElementNoElementCheckUserValues::GetElementalVariables(const int length, double* values, const int meshId, const int elementId)
        {
//          DebugBreak();
          int toRet = false;
          try
          {
            if (!newIvValues_)
            {
              throw "newIvValues_ not set";
            }
            else
            {
              SetPoint(elementId, time_);
							
              std::vector<double> tmpVec = newIvValues_->GetInternalVariablesNew(GetCentralElementPoint());
							lo::debugLog("tmpVec.size() " + std::to_string(tmpVec.size()));
              if (static_cast<unsigned int>(length) > tmpVec.size())
              {
                throw "IV out of range requested";
              }
              else
              {
                std::copy(tmpVec.begin(), tmpVec.end(), values);
                toRet = true;
              }
            }
          }
          catch (const char* msg)
          {
            lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, msg);
          };          
          return toRet;
        }

        double OneStepOneElementNoElementCheckUserValues::GetElementalVariables(const unsigned int whichVariable) 
        {
          double toRet = std::numeric_limits<double>::quiet_NaN();;
          try
          {
            toRet = ivValues_[whichVariable];
            throw "Array range not checked!";
          }
          catch (const char* msg)
          {
            lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, msg);
          };
          return toRet;
        }

        int OneStepOneElementNoElementCheckUserValues::SetTemperature(const double* temperature, const int meshId, const int elementId)
        {
          temperature_(*temperature);
          return true;
        }

        mpr::Temperature OneStepOneElementNoElementCheckUserValues::GetTemperature() const
        {
          return temperature_;
        }

        int OneStepOneElementNoElementCheckUserValues::SetStepLength(const double* timeStepLength, const int meshId, const int elementId)
        {
          stepLength_(*timeStepLength);
          return true;
        }

        int OneStepOneElementNoElementCheckUserValues::SetTime(const double* time, const int meshId, const int elementId)
        {
          time_(*time);
          return true;
        }

        mpr::Time OneStepOneElementNoElementCheckUserValues::GetTime() const
        {
          return time_;
        }

        mpr::StepLength OneStepOneElementNoElementCheckUserValues::GetStepLength() const
        {
          return stepLength_;
        }

        mpt::Point<mpr::Coordinates2D>* OneStepOneElementNoElementCheckUserValues::GetCentralElementPoint()
        {
          if (point_ != nullptr)
          {
            return point_;
          } 
          else
          {
            throw std::runtime_error("OneStepOneElementNoElementCheckUserValues::point_ == nullptr");
          }
        }

        void OneStepOneElementNoElementCheckUserValues::SetPoint(const unsigned int elemNumber, const mpr::Time& time)
        {
          lo::debugLog("Setting mock point for elemnNumber " + std::to_string(elemNumber));
          mpr::Coordinates2D coord(elemNumber, 0.0);
//          DebugBreak();
          point_ = new mpt::NamedPoint<mpt::NamedPointsContainer<mpt::NamedPointsStorageLastTwo<mpr::Coordinates2D> > >(elemNumber, coord, time);
					lo::debugLog("Point set for elemnNumber " + std::to_string(elemNumber));
        }

        mpr::ElementNumber OneStepOneElementNoElementCheckUserValues::GetElementNumber()
        {
          mpr::ElementNumber toRet;
          toRet(elemNumber_());
          return toRet;
        }

//         std::vector<double> OneStepOneElementNoElementCheckUserValues::GetInternalVariablesNew() const
//         {
//           try
//           {
//             if (!newIvValues_)
//             {
//               throw "newIvValues_ not set";
//               return std::vector<double>;
//             }
//             else return newIvValues_;
//           }
//           catch (const char* msg)
//           {
//             lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, msg);
//           };
//         }

//         void OneStepOneElementNoElementCheckUserValues::SetNewIvValuesProvider(boost::shared_ptr<mot::OneStepOneElementIvNew> newIvValues)
//         {
//           newIvValues_ = newIvValues;
//         }

    } //deform
  } //software
} //am3

