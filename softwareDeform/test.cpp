#include "test.h"
#include <iostream>
int main()
{
    double a = 7.4;
    double temperature = 1500;
    int m =1;
	int e = 2;
	//std::cin>>a;
	const int nodesNumberPerElem=4;
	//const double coordinates[8]={0,0,1,0,1,1,0,1};
	double time = 0;
  const double timeStep = 0.1;
	int elementId = 0;
	const int meshId = 0;
  const int elementsNumber = 2;
  const int nodesNumber = 6;
	std::cout<< "Calling Deform\n";

  const int connectivityMatrix[10]={0,0,1,2,3,1,1,4,5,2};
  const double coordinates[12]={0,0,1,0,1,1,0,1,2,0,2,1};
  const double displacements[12]={0,0,0,0,0,0,0,0,0,0,0,0};

  DEFORM_MESH_SET(coordinates, displacements, &time,&timeStep,&elementsNumber,&nodesNumber,connectivityMatrix);

	//DEFORM_NODES_SEND(&nodesNumberPerElem,coordinates,&time, &meshId,&elementId);
  DEFORM_ELEM_SET(&time,&meshId,&elementId);
	
	
	int userVariablesNumber=4;
	double userVariablesValues[4] = {100.0e10,5e-6,20e3,50e-6};	
 	DEFORM_USRE1_SEND(&userVariablesNumber,userVariablesValues,&meshId,&elementId);
 	
	DEFORM_TEMPERATURE(&temperature,&meshId,&elementId);
	DEFORM_EFF_STR_RATE(&a,&meshId,&elementId);
	DEFORM_TIME_STEP_LENGTH(&a,&meshId,&elementId);
	
	DEFORM_USRE1_GET(&userVariablesNumber,userVariablesValues,&meshId,&elementId);

  DEFORM_ELEM_SET(&time,&meshId,&elementId);
  //DEFORM_NODES_SEND(&nodesNumberPerElem,coordinates,&time, &meshId,&elementId);
	DEFORM_TEMPERATURE(&temperature,&meshId,&elementId);
	DEFORM_EFF_STR_RATE(&a,&meshId,&elementId);


	DEFORM_YIELD_STRESS_GET(&a, &meshId, &elementId);
	DEFORM_YIELD_STRESSDSTRAIN_GET(&a, &meshId, &elementId);
	DEFORM_YIELD_STRESSDSTRAINRATE_GET(&a, &meshId, &elementId);

// 	elementId = 1;
// 	//DEFORM_NODES_SEND(&nodesNumberPerElem,coordinates,&time, &meshId,&elementId);
//   DEFORM_ELEM_SET(&time,&meshId,&elementId);
// 	DEFORM_USRE1_SEND(&userVariablesNumber,userVariablesValues,&meshId,&elementId);
// 
// 	DEFORM_TEMPERATURE(&a,&meshId,&elementId);
// 	DEFORM_EFF_STR_RATE(&a,&meshId,&elementId);
// 	DEFORM_TIME_STEP_LENGTH(&a,&meshId,&elementId);
// 
// 	DEFORM_USRE1_GET(&userVariablesNumber,userVariablesValues,&meshId,&elementId);
// 
//   DEFORM_ELEM_SET(&time,&meshId,&elementId);
// 	//DEFORM_NODES_SEND(&nodesNumberPerElem,coordinates,&time, &meshId,&elementId);
// 	DEFORM_TEMPERATURE(&a,&meshId,&elementId);
// 	DEFORM_EFF_STR_RATE(&a,&meshId,&elementId);
// 
// 
// 	DEFORM_YIELD_STRESS_GET(&a, &meshId, &elementId);
// 	DEFORM_YIELD_STRESSDSTRAIN_GET(&a, &meshId, &elementId);
// 	DEFORM_YIELD_STRESSDSTRAINRATE_GET(&a, &meshId, &elementId);

  elementId = 0;
  time = 1000;
  temperature = 1400;
  //DEFORM_NODES_SEND(&nodesNumberPerElem,coordinates,&time, &meshId,&elementId);
  DEFORM_ELEM_SET(&time,&meshId,&elementId);
  DEFORM_USRE1_SEND(&userVariablesNumber,userVariablesValues,&meshId,&elementId);

  DEFORM_TEMPERATURE(&temperature,&meshId,&elementId);
  DEFORM_EFF_STR_RATE(&a,&meshId,&elementId);
  DEFORM_TIME_STEP_LENGTH(&a,&meshId,&elementId);

  DEFORM_USRE1_GET(&userVariablesNumber,userVariablesValues,&meshId,&elementId);

  DEFORM_ELEM_SET(&time,&meshId,&elementId);
  //DEFORM_NODES_SEND(&nodesNumberPerElem,coordinates,&time, &meshId,&elementId);
  DEFORM_TEMPERATURE(&temperature,&meshId,&elementId);
  DEFORM_EFF_STR_RATE(&a,&meshId,&elementId);


  DEFORM_YIELD_STRESS_GET(&a, &meshId, &elementId);
  DEFORM_YIELD_STRESSDSTRAIN_GET(&a, &meshId, &elementId);
  DEFORM_YIELD_STRESSDSTRAINRATE_GET(&a, &meshId, &elementId);

  elementId = 0;

  for (int i =0; i<10; i++)
  {
    time += 1000;
    temperature -= 100;
    DEFORM_ELEM_SET(&time,&meshId,&elementId);
    DEFORM_USRE1_SEND(&userVariablesNumber,userVariablesValues,&meshId,&elementId);

    DEFORM_TEMPERATURE(&temperature,&meshId,&elementId);
    DEFORM_EFF_STR_RATE(&a,&meshId,&elementId);
    DEFORM_TIME_STEP_LENGTH(&a,&meshId,&elementId);

    DEFORM_USRE1_GET(&userVariablesNumber,userVariablesValues,&meshId,&elementId);

    DEFORM_ELEM_SET(&time,&meshId,&elementId);
    //DEFORM_NODES_SEND(&nodesNumberPerElem,coordinates,&time, &meshId,&elementId);
    DEFORM_TEMPERATURE(&temperature,&meshId,&elementId);
    DEFORM_EFF_STR_RATE(&a,&meshId,&elementId);


    DEFORM_YIELD_STRESS_GET(&a, &meshId, &elementId);
    DEFORM_YIELD_STRESSDSTRAIN_GET(&a, &meshId, &elementId);
    DEFORM_YIELD_STRESSDSTRAINRATE_GET(&a, &meshId, &elementId);
  }
  return 0;
}