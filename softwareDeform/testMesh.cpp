#include "test.h"
#include <iostream>

int main()
{
    double a = 7.4;
    double temperature = 1500;
    int m =1;
	int e = 2;
	//std::cin>>a;
	const int nodesNumberPerElem=4;
	//const double coordinates[8]={0,0,1,0,1,1,0,1};
	double time = 0;
  const double timeStep = 0.1;
	int elementId = 0;
	const int meshId = 0;
  const int elementsNumber = 2;
  const int nodesNumber = 6;
	std::cout<< "Calling Deform\n";

  const int connectivityMatrix[10]={0,0,1,2,3,1,1,4,5,2};
  const double coordinates[12]={0,0,1,0,1,1,0,1,2,0,2,1};
  const double displacements[12]={0,0,0,0,0,0,0,0,0,0,0,0};
  const double temperatures[12]={700,700,700,700,700,700,700,700,700,700,700,700};
  const double dTemperatures[12]={70,70,70,70,70,70,70,70,70,70,70,70};
  

  DEFORM_TEMPERATURES_SET(m,time,timeStep,temperatures,dTemperatures,1,12);
  return 0;
}