
/*! \file deformHandlerFacadeAM3side.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareDeform_deformHandlerFacadeAM3side_h__
#define am3softwareDeform_deformHandlerFacadeAM3side_h__
/*	include *********************************************************/

#include "boost/shared_ptr.hpp"
#include "deformHandlerFacade.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace deform
    {
      void SetDeformHandlerFacadeMtr(boost::shared_ptr<DeformHandlerFacadeMtr> facadeMtr);
      void SetDeformHandlerFacadeTime(boost::shared_ptr<DeformHandlerFacadeTime> facadeTime);
      void SetDeformHandlerFacadeElm(boost::shared_ptr<DeformHandlerFacadeElm> facadeElm);
      void SetDeformHandlerFacadeUpd(boost::shared_ptr<DeformHandlerFacadeUpd> facadeUpd);
      void SetDeformHandlerFacadeMsh(boost::shared_ptr<DeformHandlerFacadeMsh> facadeMsh);
      void SetDeformHandlerFacadeMshTemperatureUsrElemVal(boost::shared_ptr<DeformHandlerFacadeMshTemperatureUsrElemVal> facadeMsh);

      void UnsetDeformHandlerFacadeMtr();
      void UnsetDeformHandlerFacadeTime();
      void UnsetDeformHandlerFacadeElm();
      void UnsetDeformHandlerFacadeUpd();
      void UnsetDeformHandlerFacadeMsh();
      void UnsetDeformHandlerFacadeMshTemperatureUsrElemVal();

    } //deform
  } //software
} //am3


#endif // am3softwareDeform_deformHandlerFacadeAM3side_h__
