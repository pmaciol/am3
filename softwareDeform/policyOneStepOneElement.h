
/*! \file policyOneStepOneElement.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareDeform_oneStepOneElement_h__
#define am3softwareDeform_oneStepOneElement_h__
/*	include *********************************************************/

#include "boost/shared_ptr.hpp"
#include "../modelTemplates/innerSideInterfacesMacro.h"
#include "deformHandlerFacadeAM3side.h"
#include "../models/namedPoint.h"

/*	using ***********************************************************/

namespace mot = am3::model::templates;
namespace mpt = am3::model::point;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace deform
		{
      class OneStepOneElementNoElementCheckStresses
        : public mot::OneStepOneElementMacroState
        , public DeformHandlerFacadeMtr
        , public DeformHandlerFacadeTime
      {
      public:
        OneStepOneElementNoElementCheckStresses(boost::shared_ptr<mot::OneStepOneElementMacroStressWithDerivatives> val) : stressesHandler_(val){};
        
        // OneStepOneElementMacroState
        virtual mpr::Time GetTime() const;
        virtual mpr::StepLength GetStepLength() const;
        virtual mpr::Temperature GetTemperature() const;
        virtual mpr::TotalEffectiveStrain GetEffectiveStrain() const;
        virtual mpr::EffectiveStrainRate GetEffectiveStrainRate() const;

        
        // From DeformHandlerFacadeMtr
        virtual double GetYieldStress(const int meshId, const int elementId);
        virtual double GetYieldStressDStrain(const int meshId, const int elementId);
        virtual double GetYieldStressDStrainRate(const int meshId, const int elementId);
        virtual int SetTemperature(const double* temperature, const int meshId, const int elementId);
        virtual int SetEffectiveStrainRate(const double* effStrainRate, const int meshId, const int elementId);
        virtual int SetEffectiveStrain(const double* effStrain, const int meshId, const int elementId);

        //From DeformHandlerFacadeTime
        virtual int SetStepLength(const double* timeStepLength, const int meshId, const int elementId);
        virtual int SetTime(const double* time, const int meshId, const int elementId);

      protected:
        boost::shared_ptr<mot::OneStepOneElementMacroStressWithDerivatives> stressesHandler_;
                
        mpr::TotalEffectiveStrain totEffStrainRate_;
        mpr::EffectiveStrainRate effStrainRate_;
        mpr::Temperature temperature_;
        mpr::Time time_;
        mpr::StepLength stepLength_;
      };


      //******************************************************************************************************************************************************************

      class OneStepOneElementNoElementCheckUserValues
        : public mot::OneStepOneElementIvPresent
        , public DeformHandlerFacadeUpd
        , public DeformHandlerFacadeTime
      {
      public:
        OneStepOneElementNoElementCheckUserValues(boost::shared_ptr<mot::OneStepOneElementIvNew> newIvValues) :newIvValues_(newIvValues){};
//				OneStepOneElementNoElementCheckUserValues(){}; //for test - to be removed!!!!! /TODO
        // mot::OneStepOneElementIvPresent
        virtual std::vector<double> GetInternalVariables() const;
        virtual double GetInternalVariable(std::size_t index) const;
        virtual mpr::Temperature GetTemperature() const;
        virtual mpr::Time GetTime() const;
        virtual mpr::StepLength GetStepLength() const;

        // DeformHandlerFacadeUpd
        virtual int SetElementalVariables(const int length, const double* values, const int meshId, const int elementId) override;
        virtual int SetGlobalVariables(const int length, const double* values, const int meshId, const int elementId) override;
        virtual int GetElementalVariables(const int length, double* values, const int meshId, const int elementId) override;
        virtual double GetElementalVariables(const unsigned int whichVariable);
        virtual int SetTemperature(const double* temperature, const int meshId, const int elementId) override;
        //From DeformHandlerFacadeTime
        virtual int SetStepLength(const double* timeStepLength, const int meshId, const int elementId);
        virtual int SetTime(const double* time, const int meshId, const int elementId);

        virtual mpt::Point<mpr::Coordinates2D>* GetCentralElementPoint() override;
        virtual mpr::ElementNumber GetElementNumber() override;

      protected:
        std::vector<double> ivValues_;
        boost::shared_ptr<mot::OneStepOneElementIvNew> newIvValues_;
        mpr::Temperature temperature_;
        mpr::Time time_;
        mpr::StepLength stepLength_;
        mpt::NamedPoint<mpt::NamedPointsContainer<mpt::NamedPointsStorageLastTwo<mpr::Coordinates2D> > >* point_;
        mpr::ElementNumberLocal elemNumber_;

      private:
        void SetPoint(const unsigned int elemNumber, const mpr::Time & time);

        

      };
		} //deform
	} //software
} //am3
#endif // am3softwareDeform_oneStepOneElement_h__
