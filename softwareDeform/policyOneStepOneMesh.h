
/*! \file oneStepOneMesh.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareDeform_oneStepOneMesh_h__
#define am3softwareDeform_oneStepOneMesh_h__
/*	include *********************************************************/

#include "../modelTemplates/innerSideInterfacesMacro.h"
#include "deformHandlerFacadeMesh.h"

/*	using ***********************************************************/

namespace mot = am3::model::templates;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace deform
    {
//       class OneStepOneMeshUserValues
//         : public mot::OneStepOneMeshIvPresent
//         , public DeformHandlerFacadeMshUsrElemVar
//       {
//       public:
//         ~OneStepOneMeshUserValues(){};
//         virtual double GetInternalVariable(const unsigned elemId, std::size_t index) const;
//         virtual unsigned NumberOfElements() const;
//         virtual unsigned NumberOfIvs() const;
// 
//         virtual int SetAllElementalVariables(double const * usrElemVar, const int * materialId, const int * numUsrElemVar, const int* numElements);
// 
//       private:
//         std::vector<std::vector<double> > userVariables_;
//       };
// 
//       //******************************************************************************************************************************************************************
// 
//       class OneStepOneMeshTemperatureElem
//         : public mot::OneStepOneMeshTemperature
//         , public DeformHandlerFacadeMshTemperature
//       {
//       public:
//         ~OneStepOneMeshTemperatureElem(){};
// 
//         virtual mpr::Temperature GetOldTemperature(const unsigned elementId) const;
//         virtual mpr::Temperature GetNewTemperature(const unsigned elementId) const;
// 
//         virtual void SetTemperaturesInNodes(const int * materialId, double const * temperatures, double const * deltaTemperatures, const unsigned* numberOfNodes);
// 
//       private:
//         std::vector<double> oldTemperatures_;
//         std::vector<double> newTemperatures_;
//       };

      //******************************************************************************************************************************************************************

      class OneStepOneMeshTime
        : public mot::OneStepOneMeshTime
        , public DeformHandlerFacadeMshTime
      {
      public:
        virtual int SetStepLength(const double timeStepLength) override;
        virtual int SetTime(const double time) override;

        virtual mpr::StepLength GetStepLength() const override;
        virtual mpr::Time GetTime() const override;
        virtual mpr::Time GetNewTime() const;

      private:
        double time_, timeStep_;
      };

//       class OneStepOneMeshTemperaturesUserValues
//         : public OneStepOneMeshUserValues
//         , public OneStepOneMeshTemperatureElem
//         , public OneStepOneMeshTime
//         , public DeformHandlerFacadeMshTemperatureUsrElemVal
//       {
//       public:
//         virtual int SetAllElementalVariables(double const * usrElemVar, const int * materialId, const int * numUsrElemVar, const int* numElements);
//         virtual int SetStepLength(const double* timeStepLength);
//         virtual int SetTime(const double* time);
//         virtual int SetTemperaturesInElements(double const * temperatures, double const * deltaTemperatures, const int * materialId, const int* numElements);
//         virtual int SetTemperaturesInNodes(double const * temperatures, double const * deltaTemperatures, const unsigned int materialId, const unsigned int  numElements, const unsigned int  nodesPerElement, const int* connectivityMatrix);
//         bool AllValuesSet(const double* time);
//       };

    } //deform
  } //software
} //am3
#endif // am3softwareDeform_oneStepOneMesh_h__
