
/*! \file oneStepOneMeshTemperature.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief
 * \details
 *********************************************************************/

/*	include *********************************************************/

#include <stdexcept>
#include "../models/properties.h"
#include "policyOneStepOneMeshTemperatureTime.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace deform
		{
      mpr::Temperature OneStepOneMeshTemperatureTime::GetOldTemperature(const unsigned nodeId) const
      {
        if (! CheckNodeInRange(nodeId)) throw std::out_of_range("nodeId outside range");
        mpr::Temperature toRet;
        toRet(oldTemperatures_[CalculateNodeIndex(nodeId)]);
        return toRet;
      }

      mpr::Temperature OneStepOneMeshTemperatureTime::GetNewTemperature(const unsigned nodeId) const
      {
        if (!CheckNodeInRange(nodeId)) throw std::out_of_range("nodeId outside range");
        mpr::Temperature toRet;
        toRet(newTemperatures_[CalculateNodeIndex(nodeId)]);
        return toRet;
      }

      unsigned OneStepOneMeshTemperatureTime::GetNodeStart() const
      {
        return nodeStart_;
      }

      unsigned OneStepOneMeshTemperatureTime::GetNodeLast() const
      {
        return nodeEnd_;
      }

      unsigned OneStepOneMeshTemperatureTime::GetNodesNumber() const
      {
        if (nodeEnd_ < nodeStart_) throw std::invalid_argument("End node number lower than start node number");
        return nodeEnd_ - nodeStart_ + 1;
      }

      void OneStepOneMeshTemperatureTime::SetTemperaturesInNodes(const int materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const int nodeStart, const int nodeEnd)
      {
//        DebugBreak();
        newTemperatures_.clear();
        oldTemperatures_.clear();
        nodeStart_ = nodeStart;
        nodeEnd_ = nodeEnd;
        if (nodeEnd_ < nodeStart_) throw std::invalid_argument("End node number lower than start node number");

        unsigned nodesNumber = GetNodesNumber();
        for (unsigned i = 0; i < nodesNumber ; i++)
        {
          newTemperatures_.push_back(temperatures[i] + deltaTemperatures[i]);
          oldTemperatures_.push_back(temperatures[i]);
        }
        time_ = time;
        timeStep_ = timeStep;
      }

      unsigned int OneStepOneMeshTemperatureTime::CalculateNodeIndex(const unsigned nodeId) const
      {
        return nodeId - nodeStart_;
      }

      bool OneStepOneMeshTemperatureTime::CheckNodeInRange(const unsigned nodeId) const
      {
        if (nodeId >= nodeStart_ && nodeId <= nodeEnd_)
        {
          return true;
        } 
        else
        {
          return false;
        }
      }

      mpr::StepLength OneStepOneMeshTemperatureTime::GetStepLength() const
      {
        mpr::StepLength toRet;
        toRet(timeStep_);
        return toRet;
      }

      mpr::Time OneStepOneMeshTemperatureTime::GetTime() const
      {
        mpr::Time toRet;
        toRet(time_);
        return toRet;
      }

      mpr::Time OneStepOneMeshTemperatureTime::GetNewTime() const
      {
        mpr::Time toRet;
        toRet(time_ + timeStep_);
        return toRet;
      }

		} //deform
	} //software
} //am3


