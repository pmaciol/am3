#ifndef AC_MANAGER
#define AC_MANAGER

#include <winsock.h>
#include "AC_DataPack.h"

class AC_Manager
{
    public:
        static AC_Manager& getManager()
        {
            static AC_Manager instance;
            return instance;
        }
		
		bool isOperable(){ return _isOperable;}
		bool isVerbose() {return _isVerbose;}
		char * lastErrorDescription() {return _errorMessage;}

		void adinaSend(AC_DataPackOUT &data);
		void adinaReceive(AC_DataPackIN &data);
		static AC_DataPackIN& getDataPackIN()
		{
			static AC_DataPackIN  data;
			return data;
		}
		
    private:
        AC_Manager();
        AC_Manager(AC_Manager const&);
        void operator=(AC_Manager const&);
		
		u_long LookupAddress(const char* pcHost);
		SOCKET EstablishConnection(u_long nRemoteAddr, u_short nPort);
		bool SendData(SOCKET sd, AC_DataPackOUT &data);
		int ReceiveData(SOCKET sd, char * readBuffer);
		//bool AC_Manager::ShutdownConnection(SOCKET sd);
		bool ReadConfigFile();
		
		SOCKET _sd;
		bool _isOperable;
		bool _isVerbose;
		char * _errorMessage;
		char _pcHost [100];
		int _nPort;

		static const char * _configFile;
};

#endif