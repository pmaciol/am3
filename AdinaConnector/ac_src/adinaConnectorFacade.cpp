#include "AC_DataPack.h"
#include "AC_Manager.h"
#include "AC_Logger.h"

extern "C" int ADINACSEND(
	    //R(IU)   = U    = X-VELOCITY
		double *x_velocity, 
		//R(IU+1) = DXU  = DERIVATIVE OF U WITH RESPECT TO X
        //R(IU+2) = DYU  = DERIVATIVE OF U WITH RESPECT TO Y
        //R(IU+3) = DZU  = DERIVATIVE OF U WITH RESPECT TO Z
		double *x_velocity_dx, double *x_velocity_dy, double *x_velocity_dz,
		//R(IV)   = V    = Y-VELOCITY
		double *y_velocity,
		//R(IV+1) = DXV  = DERIVATIVE OF V WITH RESPECT TO X
        //R(IV+2) = DYV  = DERIVATIVE OF V WITH RESPECT TO Y
        //R(IV+3) = DZV  = DERIVATIVE OF V WITH RESPECT TO Z
		double *y_velocity_dx, double *y_velocity_dy, double *y_velocity_dz,
		//R(IW)   = W    = Z-VELOCITY
		double *z_velocity,
		//R(IW+1) = DXW  = DERIVATIVE OF W WITH RESPECT TO X
        //R(IW+2) = DYW  = DERIVATIVE OF W WITH RESPECT TO Y
        //R(IW+3) = DZW  = DERIVATIVE OF W WITH RESPECT TO Z
		double *z_velocity_dx, double *z_velocity_dy, double *z_velocity_dz,

		//R(IP)   = P    = PRESSURE
		double *pressure,
		//R(IP+1) = DXP  = DERIVATIVE OF P WITH RESPECT TO X
        //R(IP+2) = DYP  = DERIVATIVE OF P WITH RESPECT TO Y
        //R(IP+3) = DZP  = DERIVATIVE OF P WITH RESPECT TO Z
		double *pressure_dx, double *pressure_dy, double *pressure_dz,

		//R(IT)   = T    = TEMPERATURE
		double *temperature,
		//R(IT+1) = DXT  = DERIVATIVE OF T WITH RESPECT TO X
        //R(IT+2) = DYT  = DERIVATIVE OF T WITH RESPECT TO Y
        //R(IT+3) = DZT  = DERIVATIVE OF T WITH RESPECT TO Z
		double *temperature_dx, double *temperature_dy, double *temperature_dz,

		//R(IK)   = TK   = TURBULENCE_K
		double *turbulence_k,
		//R(IK+1) = DXK  = DERIVATIVE OF TK WITH RESPECT TO X
        //R(IK+2) = DYK  = DERIVATIVE OF TK WITH RESPECT TO Y
        //R(IK+3) = DZK  = DERIVATIVE OF TK WITH RESPECT TO Z
		double *turbulence_k_dx, double *turbulence_k_dy, double *turbulence_k_dz,

		//R(IE)   = TE   = TURBULENCE_E OR TURBULENCE-W
		double *turbulence_e,
		//R(IE+1) = DXE  = DERIVATIVE OF TE WITH RESPECT TO X
        //R(IE+2) = DYE  = DERIVATIVE OF TE WITH RESPECT TO Y
        //R(IE+3) = DZE  = DERIVATIVE OF TE WITH RESPECT TO Z
		double *turbulence_e_dx, double *turbulence_e_dy, double *turbulence_e_dz,

		//R(IX)   = X    = X-COORDINATE
        //R(IY)   = Y    = Y-COORDINATE
		//R(IZ)   = Z    = Z-COORDINATE
		double *x_coord, double *y_coord, double *z_coord,

		//R(ITIM) = TIME = TIME
		double *time,
		
		//R(IM),R(IM+1),R(IM+2),R(IM+3) = 		MASS RATIO 1 AND ITS XYZ DERIVATIVES
		double *mr1, double *mr1dx, double *mr1dy, double *mr1dz,
		//R(IM+4),R(IM+5),R(IM+6),R(IM+7) = 	MASS RATIO 2 AND ITS XYZ DERIVATIVES
		double *mr2, double *mr2dx, double *mr2dy, double *mr2dz,
		//R(IM+8),R(IM+9),R(IM+10),R(IM+11) = 	MASS RATIO 3 AND ITS XYZ DERIVATIVES
		double *mr3, double *mr3dx, double *mr3dy, double *mr3dz,
		//R(IM+12),R(IM+13),R(IM+14),R(IM+15) = MASS RATIO 4 AND ITS XYZ DERIVATIVES
		double *mr4, double *mr4dx, double *mr4dy, double *mr4dz,		
       
		//L( 5) = IELG   = ELEMENT GROUP NUMBER
		int *element_group_number, 
		//L( 6) = IELM   = ELEMENT NUMBER
		int *element_number
	)
{
	static AC_DataPackOUT dataOUT;
	
	dataOUT.x_velocity = *x_velocity;
	dataOUT.x_velocity_dx = *x_velocity_dx;
	dataOUT.x_velocity_dy = *x_velocity_dy;
	dataOUT.x_velocity_dz = *x_velocity_dz;
	dataOUT.y_velocity = *y_velocity;
	dataOUT.y_velocity_dx = *y_velocity_dx;
	dataOUT.y_velocity_dy = *y_velocity_dy;
	dataOUT.y_velocity_dz = *y_velocity_dz;
	dataOUT.z_velocity = *z_velocity;
	dataOUT.z_velocity_dx = *z_velocity_dx;
	dataOUT.z_velocity_dy = *z_velocity_dy;
	dataOUT.z_velocity_dz = *z_velocity_dz;
	dataOUT.pressure = *pressure;
	dataOUT.pressure_dx = *pressure_dx;
	dataOUT.pressure_dy = *pressure_dy;
	dataOUT.pressure_dz = *pressure_dz;
	dataOUT.temperature = *temperature;
	dataOUT.temperature_dx = *temperature_dx;
	dataOUT.temperature_dy = *temperature_dy;
	dataOUT.temperature_dz = *temperature_dz;
	dataOUT.turbulence_k = *turbulence_k;
	dataOUT.turbulence_k_dx = *turbulence_k_dx;
	dataOUT.turbulence_k_dy = *turbulence_k_dy;
	dataOUT.turbulence_k_dz = *turbulence_k_dz;
	dataOUT.turbulence_e = *turbulence_e;
	dataOUT.turbulence_e_dx = *turbulence_e_dx;
	dataOUT.turbulence_e_dy = *turbulence_e_dy;
	dataOUT.turbulence_e_dz = *turbulence_e_dz;
	dataOUT.x_coord = *x_coord;
	dataOUT.y_coord = *y_coord;
	dataOUT.z_coord = *z_coord;
	dataOUT.time = *time;
	dataOUT.mr1 = *mr1;
	dataOUT.mr1dx = *mr1dx;
	dataOUT.mr1dy = *mr1dy;
	dataOUT.mr1dz = *mr1dz;
	dataOUT.mr2 = *mr2;
	dataOUT.mr2dx = *mr2dx;
	dataOUT.mr2dy = *mr2dy;
	dataOUT.mr2dz = *mr2dz;	
	dataOUT.mr3 = *mr3;
	dataOUT.mr3dx = *mr3dx;
	dataOUT.mr3dy = *mr3dy;
	dataOUT.mr3dz = *mr3dz;	
	dataOUT.mr4 = *mr4;
	dataOUT.mr4dx = *mr4dx;
	dataOUT.mr4dy = *mr4dy;
	dataOUT.mr4dz = *mr4dz;		
	dataOUT.element_group_number = *element_group_number; 
	dataOUT.element_number = *element_number;

	AC_Logger::initLogFile();

	AC_Manager &manager = AC_Manager::getManager();
	if(!manager.isOperable())
	{
		AC_Logger::reportOnce(manager.lastErrorDescription());
		return -1;
	}
		
	manager.adinaSend(dataOUT);
	
	if(!manager.isOperable())
	{
		AC_Logger::reportOnce(manager.lastErrorDescription());
		return -1;
	}
	
	manager.adinaReceive(manager.getDataPackIN());
	
	if(!manager.isOperable())
	{
		AC_Logger::reportOnce(manager.lastErrorDescription());
		return -1;
	}
	
	if(manager.isVerbose())
	{
		AC_Logger::logDataOUT(dataOUT);
		AC_Logger::logDataIN(manager.getDataPackIN());
	}
	
	return 0;
}

extern "C" void ADINACGET(
	
	double *viscosity,				//CMAT(1 ) = XMU   = VISCOSITY
	double *cp,						//CMAT(2 ) = CP    = SPECIFIC HEAT AT CONSTANT PRESSURE
	double *thermal_conductivity,	//CMAT(3 ) = XKCON = THERMAL CONDUCTIVITY
	double *coeff_volume_expansion,	//CMAT(4 ) = BETA  = COEFFICIENT OF VOLUME EXPANSION
	double *density,				//CMAT(5 ) = RO    = DENSITY
	double *referene_temperature,	//CMAT(6 ) = TC    = REFERENCE TEMPERATURE
	double *x_gravitation,			//CMAT(7 ) = GRAVX = X-GRAVITATION
	double *y_gravitation,			//CMAT(8 ) = GRAVY = Y-GRAVITATION
	double *z_gravitation,			//CMAT(9 ) = GRAVZ = Z-GRAVITATION
	double *heat_per_volume,		//CMAT(10) = QB    = RATE OF HEAT GENERATED PER VOLUME
	double *surface_tension,		//CMAT(11) = SIGMA = COEFFICIENT OF SURFACE TENSION
	double *bulk_modulus,			//CMAT(12) = XKAPA = BULK MODULUS
	double *cv,						//CMAT(13) = CV    = SPECIFIC HEAT AT CONSTANT VOLUME
	double *density_d_pressure,		//CMAT(14) = DR_DP = DERIVATIVE OF RO WITH RESPECT TO PRESSURE
	double *density_d_temperature	//CMAT(15) = DR_DT = DERIVATIVE OF RO WITH RESPECT TO TEMPERATURE
	)
{
	AC_DataPackIN& data = AC_Manager::getManager().getDataPackIN();
	
	*viscosity = data.viscosity ;							//CMAT(1 ) = XMU   = VISCOSITY
	*cp = data.cp ;											//CMAT(2 ) = CP    = SPECIFIC HEAT AT CONSTANT PRESSURE
	*thermal_conductivity = data.thermal_conductivity ;		//CMAT(3 ) = XKCON = THERMAL CONDUCTIVITY
	*coeff_volume_expansion = data.coeff_volume_expansion ;	//CMAT(4 ) = BETA  = COEFFICIENT OF VOLUME EXPANSION
	*density = data.density ;								//CMAT(5 ) = RO    = DENSITY
	*referene_temperature = data.referene_temperature ;		//CMAT(6 ) = TC    = REFERENCE TEMPERATURE
	*x_gravitation = data.x_gravitation ;					//CMAT(7 ) = GRAVX = X-GRAVITATION
	*y_gravitation = data.y_gravitation ;					//CMAT(8 ) = GRAVY = Y-GRAVITATION
	*z_gravitation = data.z_gravitation ;					//CMAT(9 ) = GRAVZ = Z-GRAVITATION
	*heat_per_volume = data.heat_per_volume ;				//CMAT(10) = QB    = RATE OF HEAT GENERATED PER VOLUME
	*surface_tension = data.surface_tension ;				//CMAT(11) = SIGMA = COEFFICIENT OF SURFACE TENSION
	*bulk_modulus = data.bulk_modulus ;						//CMAT(12) = XKAPA = BULK MODULUS
	*cv = data.cv ;											//CMAT(13) = CV    = SPECIFIC HEAT AT CONSTANT VOLUME
	*density_d_pressure = data.density_d_pressure ;			//CMAT(14) = DR_DP = DERIVATIVE OF RO WITH RESPECT TO PRESSURE
	*density_d_temperature = data.density_d_temperature;	//CMAT(15) = DR_DT = DERIVATIVE OF RO WITH RESPECT TO TEMPERATURE
}