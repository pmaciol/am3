#ifndef AC_LOGGER
#define AC_LOGGER

#include "AC_DataPack.h"

class AC_Logger
{
	public:
		static void initLogFile();
		static void reportOnce(const char * s);
		static void logDataIN(const AC_DataPackIN& data);
		static void logDataOUT(const AC_DataPackOUT& data);

	private:
		static const char * _file;
		static bool _fileInitialized;
};

#endif