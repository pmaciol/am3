#include "AC_Manager.h"
#include <winsock.h>
#include <stdio.h>
#include <string.h>

const char * AC_Manager::_configFile = "acconfig.txt";

void AC_Manager::adinaSend(AC_DataPackOUT &data)
{
    if (!SendData(_sd, data)) {
		_isOperable = false;
		_errorMessage = "ERROR: Send error.\n";
    }		
}

void AC_Manager::adinaReceive(AC_DataPackIN &data)
{
	char receiveBuffer[512];
	memset(receiveBuffer,0,512);

	int nBytes;
	if ((nBytes = ReceiveData(_sd, receiveBuffer)) > 0) {
		if (nBytes == 512) {
			_isOperable = false;
			_errorMessage = "ERROR: Receive: data overflow.\n";
		}
	}
	else if (nBytes == 0) {
		_isOperable = false;
		_errorMessage = "ERROR: Receive:  Server unexpectedly closed the connection.\n";
	}
	else {
		_isOperable = false;
		_errorMessage = "ERROR: Receive:  Socket error.\n";
	}

	
	data.viscosity = atof(strtok(receiveBuffer,"DIT"));		//CMAT(1 ) = XMU   = VISCOSITY
	data.cp = atof(strtok(NULL,"DIT"));						//CMAT(2 ) = CP    = SPECIFIC HEAT AT CONSTANT PRESSURE
	data.thermal_conductivity = atof(strtok(NULL,"DIT"));	//CMAT(3 ) = XKCON = THERMAL CONDUCTIVITY
	data.coeff_volume_expansion = atof(strtok(NULL,"DIT"));	//CMAT(4 ) = BETA  = COEFFICIENT OF VOLUME EXPANSION
	data.density = atof(strtok(NULL,"DIT"));				//CMAT(5 ) = RO    = DENSITY
	data.referene_temperature = atof(strtok(NULL,"DIT"));	//CMAT(6 ) = TC    = REFERENCE TEMPERATURE
	data.x_gravitation = atof(strtok(NULL,"DIT"));			//CMAT(7 ) = GRAVX = X-GRAVITATION
	data.y_gravitation = atof(strtok(NULL,"DIT"));			//CMAT(8 ) = GRAVY = Y-GRAVITATION
	data.z_gravitation = atof(strtok(NULL,"DIT"));			//CMAT(9 ) = GRAVZ = Z-GRAVITATION
	data.heat_per_volume = atof(strtok(NULL,"DIT"));		//CMAT(10) = QB    = RATE OF HEAT GENERATED PER VOLUME
	data.surface_tension = atof(strtok(NULL,"DIT"));		//CMAT(11) = SIGMA = COEFFICIENT OF SURFACE TENSION
	data.bulk_modulus = atof(strtok(NULL,"DIT"));			//CMAT(12) = XKAPA = BULK MODULUS
	data.cv = atof(strtok(NULL,"DIT"));						//CMAT(13) = CV    = SPECIFIC HEAT AT CONSTANT VOLUME
	data.density_d_pressure = atof(strtok(NULL,"DIT"));		//CMAT(14) = DR_DP = DERIVATIVE OF RO WITH RESPECT TO PRESSURE
	data.density_d_temperature = atof(strtok(NULL,"DIT"));	//CMAT(15) = DR_DT = DERIVATIVE OF RO WITH RESPECT TO TEMPERATURE
}

bool AC_Manager::ReadConfigFile()
{
	int verbose = 0;
	FILE * pFile;
	pFile = fopen (_configFile,"r");

	if (pFile == NULL) return false;

	if (fscanf (pFile, "%s\n", _pcHost) <= 0)
	{
		fclose (pFile);
		return false;
	}

	if (fscanf (pFile, "%d\n", &_nPort) <= 0)
	{
		fclose (pFile);
		return false;
	}
	

	if (fscanf (pFile, "%d\n", &verbose) <= 0)
	{
		fclose (pFile);
		return false;
	}

	_isVerbose = (verbose == 0) ? false : true;
	
	fclose (pFile);

	return true;
}

AC_Manager::AC_Manager():
_errorMessage("No error.\n"), _isOperable(false), _nPort(0), _isVerbose(true)
{
	if (!ReadConfigFile())
	{
		_errorMessage = "ERROR: Config file error.\n";
		return;
	}
	
	WSAData wsaData;
    int nCode = WSAStartup(MAKEWORD(1, 1), &wsaData);
	if(nCode != 0)
	{
		_errorMessage = "ERROR: Startup error.\n";
		return;
	}
	
	//---
	
	u_long nRemoteAddress = LookupAddress(_pcHost);
    if (nRemoteAddress == INADDR_NONE) {
		_errorMessage = "ERROR: LookupAddress error.\n";
		return;
    }
    in_addr Address;
    memcpy(&Address, &nRemoteAddress, sizeof(u_long)); 
	
	//---
	
	_sd = EstablishConnection(nRemoteAddress, htons(_nPort));
    if (_sd == INVALID_SOCKET) {
		_errorMessage = "ERROR: EstablishConnection error.\n";
		return;	
    }
	
	_isOperable = true;	
}

u_long AC_Manager::LookupAddress(const char* pcHost)
{
    u_long nRemoteAddr = inet_addr(pcHost);
    if (nRemoteAddr == INADDR_NONE) {
        hostent* pHE = gethostbyname(pcHost);
        if (pHE == 0) {
            return INADDR_NONE;
        }
        nRemoteAddr = *((u_long*)pHE->h_addr_list[0]);
    }

    return nRemoteAddr;
}

SOCKET AC_Manager::EstablishConnection(u_long nRemoteAddr, u_short nPort)
{
    SOCKET sd = socket(AF_INET, SOCK_STREAM, 0);
    if (sd != INVALID_SOCKET) {
        sockaddr_in sinRemote;
        sinRemote.sin_family = AF_INET;
        sinRemote.sin_addr.s_addr = nRemoteAddr;
        sinRemote.sin_port = nPort;
        if (connect(sd, (sockaddr*)&sinRemote, sizeof(sockaddr_in)) ==
                SOCKET_ERROR) {
            sd = INVALID_SOCKET;
        }
    }

    return sd;
}

bool AC_Manager::SendData(SOCKET sd, AC_DataPackOUT &data)
{
	char buffer[1024];
	memset(buffer,0,1024);

	int size;
	printf("Sending: D%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fI%dI%dT\n",
		data.x_velocity, 
		data.x_velocity_dx,data.x_velocity_dy,data.x_velocity_dz,
		data.y_velocity,
		data.y_velocity_dx,data.y_velocity_dy,data.y_velocity_dz,
		data.z_velocity,
		data.z_velocity_dx,data.z_velocity_dy,data.z_velocity_dz,
		data.pressure,
		data.pressure_dx, data.pressure_dy, data.pressure_dz,
		data.temperature,
		data.temperature_dx, data.temperature_dy, data.temperature_dz,
		data.turbulence_k,
		data.turbulence_k_dx, data.turbulence_k_dy, data.turbulence_k_dz,
		data.turbulence_e,
		data.turbulence_e_dx, data.turbulence_e_dy, data.turbulence_e_dz,
		data.x_coord, data.y_coord, data.z_coord,
		data.time,
		data.mr1, data.mr1dx, data.mr1dy, data.mr1dz,
		data.mr2, data.mr2dx, data.mr2dy, data.mr2dz,
		data.mr3, data.mr3dx, data.mr3dy, data.mr3dz,
		data.mr4, data.mr4dx, data.mr4dy, data.mr4dz,		
		data.element_group_number, 
		data.element_number);

	size = sprintf(buffer,"D%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fI%dI%dT",
		data.x_velocity, 
		data.x_velocity_dx,data.x_velocity_dy,data.x_velocity_dz,
		data.y_velocity,
		data.y_velocity_dx,data.y_velocity_dy,data.y_velocity_dz,
		data.z_velocity,
		data.z_velocity_dx,data.z_velocity_dy,data.z_velocity_dz,
		data.pressure,
		data.pressure_dx, data.pressure_dy, data.pressure_dz,
		data.temperature,
		data.temperature_dx, data.temperature_dy, data.temperature_dz,
		data.turbulence_k,
		data.turbulence_k_dx, data.turbulence_k_dy, data.turbulence_k_dz,
		data.turbulence_e,
		data.turbulence_e_dx, data.turbulence_e_dy, data.turbulence_e_dz,
		data.x_coord, data.y_coord, data.z_coord,
		data.time,
		data.mr1, data.mr1dx, data.mr1dy, data.mr1dz,
		data.mr2, data.mr2dx, data.mr2dy, data.mr2dz,
		data.mr3, data.mr3dx, data.mr3dy, data.mr3dz,
		data.mr4, data.mr4dx, data.mr4dy, data.mr4dz,		
		data.element_group_number, 
		data.element_number);
	
    if (send(sd, buffer, size, 0) != SOCKET_ERROR) //I should check if retval is equal to size+1 and send data that is left
		return true;
	else
        return false;
}

int AC_Manager::ReceiveData(SOCKET sd, char * readBuffer)
{
    int nTotalBytes = 0;
    do {
        int nNewBytes = recv(sd, readBuffer + nTotalBytes, 512 - nTotalBytes, 0);
        if (nNewBytes == SOCKET_ERROR) {
            return -1;
        }
        else if (nNewBytes == 0) {
            return 0;
        }

        nTotalBytes += nNewBytes;
    } while (readBuffer[nTotalBytes-1] != 'T');

    return nTotalBytes;
}

/*
bool AC_Manager::ShutdownConnection(SOCKET sd)
{
    if (shutdown(sd, 1) == SOCKET_ERROR) {
        return false;
    }

    char acReadBuffer[1024];
    while (1) {
        int nNewBytes = recv(sd, acReadBuffer, 1024, 0);
        if (nNewBytes == SOCKET_ERROR) {
            return false;
        }
        else if (nNewBytes != 0) {
            //fprintf(pFile, "FYI, received %d unexpected bytes during shutdown.\n", nNewBytes);
        }
        else {
            // Okay, we're done!
            break;
        }
    }

    if (closesocket(sd) == SOCKET_ERROR) {
        return false;
    }

    return true;
}
*/