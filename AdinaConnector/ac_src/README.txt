Adina connector - software layer for socket-based communication in Adina User Procedures.

I. Step 1 - Budilding C++ sources:
 1. In command line set enviroment variables of Microsoft Visual Studio C++ compiler (for example: C:\ProgramFiles\Microsoft Visual Studio 10\VC\bin\vcvars32.bat) (win 32bit) or Intel C++ x64 compiler (for example: C:\Program Files(x86)\Intel\Compiler\11.0\072\cpp\bin\iclvars.bat intel64) (win 64 bit)
 2. From command line run Make_32.bat or Make_64.bat - depending on architecture
 3. 3 .obj files will be produced - copy them to directory where Adina user procedures Fortran sources and makefiles are located.
 4. Proceed with Step 2
