#include "AC_Logger.h"
#include "AC_DataPack.h"
#include <stdio.h>
//#include <time.h>

const char * AC_Logger::_file = "aclog.txt";
bool AC_Logger::_fileInitialized = false;

void AC_Logger::initLogFile()
{
	if(!_fileInitialized){
		//time_t now = time(0);
		//struct tm  tstruct;
		//char       buf[80];
		//tstruct = *localtime(&now);
		//strftime(buf, sizeof(buf), "AdinaConnector started at %Y-%m-%d.%X", &tstruct);



		FILE * pFile;
		pFile = fopen (_file,"w");
		fprintf (pFile, "%s\n", "AdinaConnector started (unless you set verbosity to 1 in acconfig.txt only errors will be reported)");
		fclose (pFile);
		_fileInitialized = true;
	}
}

void AC_Logger::reportOnce(const char * s)
{
	static bool reported = false;
	if(!reported)
	{
		FILE * pFile;
		pFile = fopen (_file,"a");
		fprintf (pFile, "%s\n", s);
		fclose (pFile);
	}
	reported = true;
}

void AC_Logger::logDataOUT(const AC_DataPackOUT& data)
{
	FILE * pFile;
	pFile = fopen (_file,"a");

	fprintf (pFile, "OUT:\n");

	fprintf (pFile, "x_velocity  : %lf\n",data.x_velocity);
	fprintf (pFile, "x_velocity_dx  : %lf\n",data.x_velocity_dx);
	fprintf (pFile, "x_velocity_dy  : %lf\n",data.x_velocity_dy);
	fprintf (pFile, "x_velocity_dz  : %lf\n",data.x_velocity_dz);
	fprintf (pFile, "y_velocity  : %lf\n",data.y_velocity);
	fprintf (pFile, "y_velocity_dx  : %lf\n",data.y_velocity_dx);
	fprintf (pFile, "y_velocity_dy  : %lf\n",data.y_velocity_dy);
	fprintf (pFile, "y_velocity_dz  : %lf\n",data.y_velocity_dz);
	fprintf (pFile, "z_velocity  : %lf\n",data.z_velocity);
	fprintf (pFile, "z_velocity_dx  : %lf\n",data.z_velocity_dx);
	fprintf (pFile, "z_velocity_dy  : %lf\n",data.z_velocity_dy);
	fprintf (pFile, "z_velocity_dz  : %lf\n",data.z_velocity_dz);
	fprintf (pFile, "pressure  : %lf\n",data.pressure);
	fprintf (pFile, "pressure_dx  : %lf\n",data.pressure_dx);
	fprintf (pFile, "pressure_dy  : %lf\n",data.pressure_dy);
	fprintf (pFile, "pressure_dz  : %lf\n",data.pressure_dz);
	fprintf (pFile, "temperature  : %lf\n",data.temperature);
	fprintf (pFile, "temperature_dx  : %lf\n",data.temperature_dx);
	fprintf (pFile, "temperature_dy  : %lf\n",data.temperature_dy);
	fprintf (pFile, "temperature_dz  : %lf\n",data.temperature_dz);
	fprintf (pFile, "turbulence_k  : %lf\n",data.turbulence_k);
	fprintf (pFile, "turbulence_k_dx  : %lf\n",data.turbulence_k_dx);
	fprintf (pFile, "turbulence_k_dy  : %lf\n",data.turbulence_k_dy);
	fprintf (pFile, "turbulence_k_dz  : %lf\n",data.turbulence_k_dz);
	fprintf (pFile, "turbulence_e  : %lf\n",data.turbulence_e);
	fprintf (pFile, "turbulence_e_dx  : %lf\n",data.turbulence_e_dx);
	fprintf (pFile, "turbulence_e_dy  : %lf\n",data.turbulence_e_dy);
	fprintf (pFile, "turbulence_e_dz  : %lf\n",data.turbulence_e_dz);
	fprintf (pFile, "x_coord  : %lf\n",data.x_coord);
	fprintf (pFile, "y_coord  : %lf\n",data.y_coord);
	fprintf (pFile, "z_coord  : %lf\n",data.z_coord);
	fprintf (pFile, "time  : %lf\n",data.time);
	fprintf (pFile, "mr1  : %lf\n",data.mr1);
	fprintf (pFile, "mr1dx  : %lf\n",data.mr1dx);
	fprintf (pFile, "mr1dy  : %lf\n",data.mr1dy);
	fprintf (pFile, "mr1dz  : %lf\n",data.mr1dz);	
	fprintf (pFile, "mr2  : %lf\n",data.mr2);
	fprintf (pFile, "mr2dx  : %lf\n",data.mr2dx);
	fprintf (pFile, "mr2dy  : %lf\n",data.mr2dy);
	fprintf (pFile, "mr2dz  : %lf\n",data.mr2dz);		
	fprintf (pFile, "mr3  : %lf\n",data.mr3);
	fprintf (pFile, "mr3dx  : %lf\n",data.mr3dx);
	fprintf (pFile, "mr3dy  : %lf\n",data.mr3dy);
	fprintf (pFile, "mr3dz  : %lf\n",data.mr3dz);		
	fprintf (pFile, "mr4  : %lf\n",data.mr4);
	fprintf (pFile, "mr4dx  : %lf\n",data.mr4dx);
	fprintf (pFile, "mr4dy  : %lf\n",data.mr4dy);
	fprintf (pFile, "mr4dz  : %lf\n",data.mr4dz);		
	
	fprintf (pFile, "element_group_number : %d\n",data.element_group_number);
	fprintf (pFile, "element_number : %d\n",data.element_number);

	fclose (pFile);	
}

void AC_Logger::logDataIN(const AC_DataPackIN& data)
{
	FILE * pFile;
	pFile = fopen (_file,"a");

	fprintf (pFile, "IN:\n");

	fprintf (pFile, "viscosity  : %lf\n",data.viscosity);	
	//CMAT(2 ) = CP    = SPECIFIC HEAT AT CONSTANT PRESSURE
	fprintf (pFile, "cp  : %lf\n",data.cp);
	//CMAT(3 ) = XKCON = THERMAL CONDUCTIVITY
	fprintf (pFile, "thermal_conductivity  : %lf\n",data.thermal_conductivity);
	//CMAT(4 ) = BETA  = COEFFICIENT OF VOLUME EXPANSION
	fprintf (pFile, "coeff_volume_expansion  : %lf\n",data.coeff_volume_expansion);
	//CMAT(5 ) = RO    = DENSITY
	fprintf (pFile, "density  : %lf\n",data.density);
	//CMAT(6 ) = TC    = REFERENCE TEMPERATURE
	fprintf (pFile, "referene_temperature  : %lf\n",data.referene_temperature);
	//CMAT(7 ) = GRAVX = X-GRAVITATION
	fprintf (pFile, "x_gravitation  : %lf\n",data.x_gravitation);
	//CMAT(8 ) = GRAVY = Y-GRAVITATION
	fprintf (pFile, "y_gravitation  : %lf\n",data.y_gravitation);
	//CMAT(9 ) = GRAVZ = Z-GRAVITATION
	fprintf (pFile, "z_gravitation  : %lf\n",data.z_gravitation);
	//CMAT(10) = QB    = RATE OF HEAT GENERATED PER VOLUME
	fprintf (pFile, "heat_per_volume  : %lf\n",data.heat_per_volume);
	//CMAT(11) = SIGMA = COEFFICIENT OF SURFACE TENSION
	fprintf (pFile, "surface_tension  : %lf\n",data.surface_tension);
	//CMAT(12) = XKAPA = BULK MODULUS
	fprintf (pFile, "bulk_modulus  : %lf\n",data.bulk_modulus);
	//CMAT(13) = CV    = SPECIFIC HEAT AT CONSTANT VOLUME
	fprintf (pFile, "cv  : %lf\n",data.cv);
	//CMAT(14) = DR_DP = DERIVATIVE OF RO WITH RESPECT TO PRESSURE
	fprintf (pFile, "density_d_pressure  : %lf\n",data.density_d_pressure);
	//CMAT(15) = DR_DT = DERIVATIVE OF RO WITH RESPECT TO TEMPERATURE
	fprintf (pFile, "density_d_temperature  : %lf\n",data.density_d_temperature);	

	fclose (pFile);	
}