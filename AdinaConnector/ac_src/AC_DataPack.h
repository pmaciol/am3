#ifndef AC_DATAPACK
#define AC_DATAPACK

struct AC_DataPackOUT
{
      double 
		//R(IU)   = U    = X-VELOCITY
		x_velocity, 
		//R(IU+1) = DXU  = DERIVATIVE OF U WITH RESPECT TO X
        //R(IU+2) = DYU  = DERIVATIVE OF U WITH RESPECT TO Y
        //R(IU+3) = DZU  = DERIVATIVE OF U WITH RESPECT TO Z
		x_velocity_dx,x_velocity_dy,x_velocity_dz,
		//R(IV)   = V    = Y-VELOCITY
		y_velocity,
		//R(IV+1) = DXV  = DERIVATIVE OF V WITH RESPECT TO X
        //R(IV+2) = DYV  = DERIVATIVE OF V WITH RESPECT TO Y
        //R(IV+3) = DZV  = DERIVATIVE OF V WITH RESPECT TO Z
		y_velocity_dx,y_velocity_dy,y_velocity_dz,
		//R(IW)   = W    = Z-VELOCITY
		z_velocity,
		//R(IW+1) = DXW  = DERIVATIVE OF W WITH RESPECT TO X
        //R(IW+2) = DYW  = DERIVATIVE OF W WITH RESPECT TO Y
        //R(IW+3) = DZW  = DERIVATIVE OF W WITH RESPECT TO Z
		z_velocity_dx,z_velocity_dy,z_velocity_dz,

		//R(IP)   = P    = PRESSURE
		pressure,
		//R(IP+1) = DXP  = DERIVATIVE OF P WITH RESPECT TO X
        //R(IP+2) = DYP  = DERIVATIVE OF P WITH RESPECT TO Y
        //R(IP+3) = DZP  = DERIVATIVE OF P WITH RESPECT TO Z
		pressure_dx, pressure_dy, pressure_dz,

		//R(IT)   = T    = TEMPERATURE
		temperature,
		//R(IT+1) = DXT  = DERIVATIVE OF T WITH RESPECT TO X
        //R(IT+2) = DYT  = DERIVATIVE OF T WITH RESPECT TO Y
        //R(IT+3) = DZT  = DERIVATIVE OF T WITH RESPECT TO Z
		temperature_dx, temperature_dy, temperature_dz,

		//R(IK)   = TK   = TURBULENCE_K
		turbulence_k,
		//R(IK+1) = DXK  = DERIVATIVE OF TK WITH RESPECT TO X
        //R(IK+2) = DYK  = DERIVATIVE OF TK WITH RESPECT TO Y
        //R(IK+3) = DZK  = DERIVATIVE OF TK WITH RESPECT TO Z
		turbulence_k_dx, turbulence_k_dy, turbulence_k_dz,

		//R(IE)   = TE   = TURBULENCE_E OR TURBULENCE-W
		turbulence_e,
		//R(IE+1) = DXE  = DERIVATIVE OF TE WITH RESPECT TO X
        //R(IE+2) = DYE  = DERIVATIVE OF TE WITH RESPECT TO Y
        //R(IE+3) = DZE  = DERIVATIVE OF TE WITH RESPECT TO Z
		turbulence_e_dx, turbulence_e_dy, turbulence_e_dz,

		//R(IX)   = X    = X-COORDINATE
        //R(IY)   = Y    = Y-COORDINATE
		//R(IZ)   = Z    = Z-COORDINATE
		x_coord, y_coord, z_coord,

		//R(ITIM) = TIME = TIME
		time,
		
		//R(IM),R(IM+1),R(IM+2),R(IM+3) = 		MASS RATIO 1 AND ITS XYZ DERIVATIVES
		mr1,  mr1dx,  mr1dy,  mr1dz,
		//R(IM+4),R(IM+5),R(IM+6),R(IM+7) = 	MASS RATIO 2 AND ITS XYZ DERIVATIVES
		mr2,  mr2dx,  mr2dy,  mr2dz,
		//R(IM+8),R(IM+9),R(IM+10),R(IM+11) = 	MASS RATIO 3 AND ITS XYZ DERIVATIVES
		mr3,  mr3dx,  mr3dy,  mr3dz,
		//R(IM+12),R(IM+13),R(IM+14),R(IM+15) = MASS RATIO 4 AND ITS XYZ DERIVATIVES
		mr4,  mr4dx,  mr4dy,  mr4dz;		

      int 
		  //L( 5) = IELG   = ELEMENT GROUP NUMBER
		  element_group_number, 
		  //L( 6) = IELM   = ELEMENT NUMBER
		  element_number; 
};

struct AC_DataPackIN
{
	double 
	//CMAT(1 ) = XMU   = VISCOSITY
	viscosity,
	//CMAT(2 ) = CP    = SPECIFIC HEAT AT CONSTANT PRESSURE
	cp,
	//CMAT(3 ) = XKCON = THERMAL CONDUCTIVITY
	thermal_conductivity,
	//CMAT(4 ) = BETA  = COEFFICIENT OF VOLUME EXPANSION
	coeff_volume_expansion,
	//CMAT(5 ) = RO    = DENSITY
	density,
	//CMAT(6 ) = TC    = REFERENCE TEMPERATURE
	referene_temperature,
	//CMAT(7 ) = GRAVX = X-GRAVITATION
	x_gravitation,
	//CMAT(8 ) = GRAVY = Y-GRAVITATION
	y_gravitation,
	//CMAT(9 ) = GRAVZ = Z-GRAVITATION
	z_gravitation,
	//CMAT(10) = QB    = RATE OF HEAT GENERATED PER VOLUME
	heat_per_volume,
	//CMAT(11) = SIGMA = COEFFICIENT OF SURFACE TENSION
	surface_tension,
	//CMAT(12) = XKAPA = BULK MODULUS
	bulk_modulus,
	//CMAT(13) = CV    = SPECIFIC HEAT AT CONSTANT VOLUME
	cv,
	//CMAT(14) = DR_DP = DERIVATIVE OF RO WITH RESPECT TO PRESSURE
	density_d_pressure,
	//CMAT(15) = DR_DT = DERIVATIVE OF RO WITH RESPECT TO TEMPERATURE
	density_d_temperature;
};

#endif