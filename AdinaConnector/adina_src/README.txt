Adina connector - software layer for socket-based communication in Adina User Procedures.

I. Step 2 - buiding dlls
 1. First copy files from subfolder 32 or 64 (depending on architecture) into Adina user procedures directory - select to overwrite when asked (3 .obj files from previous step should also be copied)
 2. Open command line and change to the above directory
 3. Set Compaq Fortran (win 32bit) or Intel Fortran x64 environment variables (for example C:\Program Files (x86)\Intel\Compiler\11.0\075\fortran\Bin\ifortvars.bat intel64) (win 64bit)
 4. Run:
  - nmake /f Makefile.afusr
  - namke /f Makefile.adfusr
 5. Overwrite afusr.dll and adfusr.dll in dlls folder with builded ones.
