C***VERSION  ADINAF8.0 NT,PCNT,PCDOS,DPR,FILES*** DATE 11.02.15
CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC
C+--------------------------------------------------------------------+C
C|                                                                    |C
C|                                                                    |C
C|                                                                    |C
C|                     A   D   I   N   A  -  F                        |C
C|                                                                    |C
C|                         VERSION  8.5.0                             |C
C|                                                                    |C
C|            THIS PROGRAM IS IN ITS ENTIRTY PROPRIETARY              |C
C|              TO AND IS SUPPORTED AND MAINTAINED BY                 |C
C|                                                                    |C
C|                         ADINA R&D, INC.                            |C
C|                                                                    |C
C|                                                                    |C
C|   ADINA R&D, INC MAKES NO WARRANTY WHATSOEVER, EXPRESSED OR        |C
C|   IMPLIED, THAT THE PROGRAM AND ITS DOCUMENTATION INCLUDING ANY    |C
C|   MODIFICATIONS AND UPDATES ARE FREE FROM ERRORS AND DEFECTS. IN   |C
C|   NO EVENT SHALL ADINA R&D, INC BECOME LIABLE TO THE USER OR       |C
C|   ANY PARTY FOR ANY LOSS, INCLUDING BUT NOT LIMITED TO LOSS OF     |C
C|   TIME , MONEY OR GOODWILL, WHICH MAY ARISE FROM THE USE OF THE    |C
C|   PROGRAM AND ITS DOCUMENTATION INCLUDING ANY MODIFICATIONS AND    |C
C|   UPDATES.                                                         |C
C|                                                                    |C
C|                    ADINA R&D, INC.                                 |C
C|                    71 ELTON AVENUE                                 |C
C|                    WATERTOWN, MA 02472                             |C
C|                    TEL (617) 926-5199                              |C
C|                    TELEX 951-772 ADINA US WTWN                     |C
C|                    TELEFAX (617) 926-0238                          |C
C|                    http://www.adina.com                            |C
C|                                                                    |C
C|               COPYRIGHT ADINA R&D, INC., 1987-2006                 |C
C|                                                                    |C
C|   ALL RIGHTS RESERVED. NO PART OF THIS PROGRAM MAY BE REPRODUCED   |C
C|   BY ANY MEANS WITHOUT PERMISSION IN WRITING FROM ADINA R&D, INC.  |C
C|                                                                    |C
C|                                                                    |C
C|                                                                    |C
C|                      RESTRICTED RIGHTS LEGEND                      |C
C|                                                                    |C
C|   USE, DUPLICATION, OR DISCLOSURE OF THE ADINA SYSTEM BY THE U.S.  |C
C|   GOVERMENT IS SUBJECT TO RESTRICTIONS AS SET FORTH IN             |C
C|   SUBPARAGRAPH (C)(1)(II) OF THE RIGHTS IN TECHNICAL DATA AND      |C
C|   COMPUTER SOFTWARE CLAUSE AT DFARS 252.227-7013.                  |C
C|                                                                    |C
C|                          ADINA R&D, INC.                           |C
C|                                                                    |C
C+--------------------------------------------------------------------+C
CAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC
      SUBROUTINE CFVUSR [DLLEXPORT] (P,T,CMAT)
C======================================================================
C
C     USER-SUPPLIED MATERIAL DATA FOR COMPRESSIBLE FLUID FLOWS
C
C     INPUT : P,T
C     OUTPUT: CMAT
C
C     ------------------------------------------------------------------
C     WHERE
C        P       = PRESSURE AT THE LOCATION
C        T       = TEMPERATURE AT THE LOCATION
C        CMAT(1) = SPECIFIC HEAT AT CONSTANT PRESSURE           (CP )
C        CMAT(2) = DERIVATIVE OF CMAT(1) WITH RESPECT TO P      (CPP)
C        CMAT(3) = DERIVATIVE OF CMAT(1) WITH RESPECT TO T      (CPT)
C        CMAT(4) = SPECIFIC HEAT AT CONSTANT VOLUME             (CV )
C        CMAT(5) = DERIVATIVE OF CMAT(4) WITH RESPECT TO P      (CVP)
C        CMAT(6) = DERIVATIVE OF CMAT(4) WITH RESPECT TO T      (CVT)
C        CMAT(7) = VISCOSITY
C        CMAT(8) = COEFFICIENT OF THERMAL CONDUCTIVITY
C        CMAT(9) = RATE OF HEAT GENERATED PER UNIT VOLUME
C
C        BE NOTED THAT THE STATE EQUATIONS ARE DEFINED AS
C
C              P = (CP-CV)*D*T
C              T =  E/CV
C
C        WHERE D =  DENSITY
C              E =  INTERNAL ENERGY
C
C======================================================================
C*I+++INCLUDE "implit.com"                        !         6
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION  CMAT(*)
C----------------------------------------------------------------------
      P  =  P
      T  =  T
C----------------------------------------------------------------------
C
C     FOR A PERFECT GAS, USING T_SCALE = (U_SCALE)**2/(CP-CV), THE
C     MATERIAL DATA ARE REPRESENTED BY RAYNOLDS NUMBER "RA", PRANDTL
C     NUMBER "PR" AND THE RATIO OF SPECIFIC HEATS "GAMMA".
C
C       XMU   =  1/RA
C       XKCON =  XMU*CP/PR
C       CV    =  1/(GAMMA-1)
C       CP    =  1 + CV
C
C----------------------------------------------------------------------
      RA      =  1000.D0
      PR      =  0.71D0
      GAMMA   =  1.4D0
C
      XMU     =  1.D0/RA
      CV      =  1.D0/(GAMMA-1.D0)
      CP      =  1.D0 + CV
      CMAT(1) =  CP
      CMAT(2) =  0.D0
      CMAT(3) =  0.D0
      CMAT(4) =  CV
      CMAT(5) =  0.D0
      CMAT(6) =  0.D0
      CMAT(7) =  XMU
      CMAT(8) =  XMU*CP/PR
      CMAT(9) =  0.D0
C----------------------------------------------------------------------
C     FOR A REAL GAS, SUPPLY THE FUNCTIONS HERE AND COMMENT OUT THE
C     UPPER SECTION
C----------------------------------------------------------------------
C----------------------------------------------------------------------
 9999 CONTINUE
      RETURN
      END
      SUBROUTINE ADFUSR [DLLEXPORT] (CHEAD,L,NL,R,NR,CMAT,NCMAT)
C=======================================================================
C
C
C     USER-SUPPLIED MATERIAL DATA FOR INCOMPRESSIBLE FLOWS WITHOUT
C     TURBULENT-KE OR TURBULENT-KW MODELS
C
C     THIS ROUTINE WILL BE CALLED AT EACH INTEGRATION POINT OF EVERY
C     ELEMENT OF EVERY ELEMENT GROUP WHERE THE MATERIAL TYPE
C    "TURBULENT-MIXL" WAS SPECIFIED, AT EACH NONLINEAR EQUILIBRIUM
C     ITERATION AT EACH TIME STEP.
C
C     IN ORDER TO ACCESS THIS ROUTINE, USER MUST CHOOSE THE MATERIAL
C     TYPE "TURBULENT-MIXL".
C
C     USER MUST SPECIFY ALL MATERIAL DATA EXPLAINED HERE INTO THE ARRAY
C     CMAT(NCMAT) IN THIS ROUTINE. DEFINE ZERO VALUES IF THEY ARE
C     NOT USED.
C
C     WARNING: USER IS SUPPOSED TO BE AWARE OF THE AVAILABILITY OF THE
C              DATA STORED IN THE ARRAYS L(*) AND R(*) UPON THE
C              PROBLEM HE IS SOLVING FOR. IF NOT AVAILABLE, THEY COULD
C              BE RANDOM.
C
C     -----------------------------------------------------------------
C
C     PASSED IN:  CHEAD, L(NL),R(NR)
C
C     PASSED OUT: CMAT(NCMAT)
C
C     -----------------------------------------------------------------
C     WHERE
C
C        CHEAD = THE STRING OF THE PROBLEM HEADING
C
C        L( 5) = IELG   = ELEMENT GROUP NUMBER
C        L( 6) = IELM   = ELEMENT NUMBER
C        L( 7) = IU     = ENTRY OF THE X-VELOCITY      IN R(*)
C        L( 8) = IV     = ENTRY OF THE Y-VELOCITY      IN R(*)
C        L( 9) = IW     = ENTRY OF THE Z-VELOCITY      IN R(*)
C        L(10) = IP     = ENTRY OF THE PRESSURE        IN R(*)
C        L(11) = IT     = ENTRY OF THE TEMPERATURE     IN R(*)
C        L(12) = IK     = ENTRY OF THE TURBULENCE-K    IN R(*)
C        L(13) = IE     = ENTRY OF THE TURBULENCE-E/W  IN R(*)
C        L(14) = IX     = ENTRY OF THE X-COORDINATE    IN R(*)
C        L(15) = IY     = ENTRY OF THE Y-COORDINATE    IN R(*)
C        L(16) = IZ     = ENTRY OF THE Z-COORDINATE    IN R(*)
C        L(19) = ITIM   = ENTRY OF THE TIME            IN R(*)
C        L(20) = IM     = ENTRY OF THE MASS-RATIOS     IN R(*)
C
C        R(IU)   = U    = X-VELOCITY
C        R(IU+1) = DXU  = DERIVATIVE OF U WITH RESPECT TO X
C        R(IU+2) = DYU  = DERIVATIVE OF U WITH RESPECT TO Y
C        R(IU+3) = DZU  = DERIVATIVE OF U WITH RESPECT TO Z
C        R(IV)   = V    = Y-VELOCITY
C        R(IV+1) = DXV  = DERIVATIVE OF V WITH RESPECT TO X
C        R(IV+2) = DYV  = DERIVATIVE OF V WITH RESPECT TO Y
C        R(IV+3) = DZV  = DERIVATIVE OF V WITH RESPECT TO Z
C        R(IW)   = W    = Z-VELOCITY
C        R(IW+1) = DXW  = DERIVATIVE OF W WITH RESPECT TO X
C        R(IW+2) = DYW  = DERIVATIVE OF W WITH RESPECT TO Y
C        R(IW+3) = DZW  = DERIVATIVE OF W WITH RESPECT TO Z
C        R(IP)   = P    = PRESSURE
C        R(IP+1) = DXP  = DERIVATIVE OF P WITH RESPECT TO X
C        R(IP+2) = DYP  = DERIVATIVE OF P WITH RESPECT TO Y
C        R(IP+3) = DZP  = DERIVATIVE OF P WITH RESPECT TO Z
C        R(IT)   = T    = TEMPERATURE
C        R(IT+1) = DXT  = DERIVATIVE OF T WITH RESPECT TO X
C        R(IT+2) = DYT  = DERIVATIVE OF T WITH RESPECT TO Y
C        R(IT+3) = DZT  = DERIVATIVE OF T WITH RESPECT TO Z
C        R(IK)   = TK   = TURBULENCE_K
C        R(IK+1) = DXK  = DERIVATIVE OF TK WITH RESPECT TO X
C        R(IK+2) = DYK  = DERIVATIVE OF TK WITH RESPECT TO Y
C        R(IK+3) = DZK  = DERIVATIVE OF TK WITH RESPECT TO Z
C        R(IE)   = TE   = TURBULENCE_E OR TURBULENCE-W
C        R(IE+1) = DXE  = DERIVATIVE OF TE WITH RESPECT TO X
C        R(IE+2) = DYE  = DERIVATIVE OF TE WITH RESPECT TO Y
C        R(IE+3) = DZE  = DERIVATIVE OF TE WITH RESPECT TO Z
C        R(IX)   = X    = X-COORDINATE
C        R(IY)   = Y    = Y-COORDINATE
C        R(IZ)   = Z    = Z-COORDINATE
C        R(ITIM) = TIME = TIME
C        R(IM)   = S    = MASS-RATIO-1
C        R(IM+1) = DXS  = DERIVATIVE OF S WITH RESPECT TO X
C        R(IM+2) = DYS  = DERIVATIVE OF S WITH RESPECT TO Y
C        R(IM+3) = DZS  = DERIVATIVE OF S WITH RESPECT TO Z
C        ...... CONTINUE FOR MASS-RATIO-2,...,UNTIL THE MASS-RATIO-10
C
C     -----------------------------------------------------------------
C
C        CMAT(1 ) = XMU   = VISCOSITY
C        CMAT(2 ) = CP    = SPECIFIC HEAT AT CONSTANT PRESSURE
C        CMAT(3 ) = XKCON = THERMAL CONDUCTIVITY
C        CMAT(4 ) = BETA  = COEFFICIENT OF VOLUME EXPANSION
C        CMAT(5 ) = RO    = DENSITY
C        CMAT(6 ) = TC    = REFERENCE TEMPERATURE
C        CMAT(7 ) = GRAVX = X-GRAVITATION
C        CMAT(8 ) = GRAVY = Y-GRAVITATION
C        CMAT(9 ) = GRAVZ = Z-GRAVITATION
C        CMAT(10) = QB    = RATE OF HEAT GENERATED PER VOLUME
C        CMAT(11) = SIGMA = COEFFICIENT OF SURFACE TENSION
C        CMAT(12) = XKAPA = BULK MODULUS
C        CMAT(13) = CV    = SPECIFIC HEAT AT CONSTANT VOLUME
C        CMAT(14) = DR_DP = DERIVATIVE OF RO WITH RESPECT TO PRESSURE
C        CMAT(15) = DR_DT = DERIVATIVE OF RO WITH RESPECT TO TEMPERATURE
C
C======================================================================
C*I+++INCLUDE "implit.com"                        !         6
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER*80 CHEAD
      DIMENSION L(NL),R(NR),CMAT(NCMAT)
	  integer*4 ADINACRESULT
	  integer*4 ADINACSEND
C----------------------------------------------------------------------
C     WRITE YOUR OWN CODE HERE AND GO TO 9999
C----------------------------------------------------------------------
C	  DTEST=0.22
C	  ITEST=2
C	  ADINACRESULT = ADINACSEND(DTEST,ITEST)
	IELM=L(6)
	IU=L(7)
	IV=L(8)
	IW=L(9)
	IX=L(14)
	IY=L(15)
	IZ=L(16)
	IP=L(10)
	IT=L(11)
	IK=L(12)
	IE=L(13)
	ITIM=L(19)
	ETA_F=0.00067
	XK0=0.33
	XK8=0.78
	XGAMMAC=0.22
	XFI=0.33
	SRMIN= 0.00000001
	VMIN=0.00001
c	VMIN=0.05
	VMAX=10
	SR=SQRT((R(IU+2)+R(IV+1))**2+(R(IU+3)+R(IW+1))**2+(R(IV+3)+R(IW+2))**2)
	IF(SR.LT.SRMIN) THEN
		SR=SRMIN
	ENDIF
	XGAMMABAR=SR/XGAMMAC
	VPONE=(1.0-0.5*XFI*((XK0+XK8*XGAMMABAR**(0.5))/(1.0+XGAMMABAR**(0.5))))
	VISCOSITY=ETA_F*VPONE**(-2.0)
c	VISCOSITY=ETA_F*(1.0-XK)
c	WRITE(*,*)'SR=',SR,' VISC=',VISCOSITY
c	ADINACRESULT = ADINACSEND(R(IU),R(IU+1),R(IU+2),R(IU+3),
c	1R(IV),R(IV+1),R(IV+2),R(IV+3),
c	1R(IW),R(IW+1),R(IW+2),R(IW+3),
c	1R(IP),R(IP+1),R(IP+2),R(IP+3),
c	1R(IT),R(IT+1),R(IT+2),R(IT+3),
c	1R(IK),R(IK+1),R(IK+2),R(IK+3),
c	1R(IE),R(IE+1),R(IE+2),R(IE+3),
c	1R(IX),R(IY),R(IZ),
c	1R(ITIM),
c	1R(IM),R(IM+1),R(IM+2),R(IM+3),
c	1R(IM+4),R(IM+5),R(IM+6),R(IM+7),
c	1R(IM+8),R(IM+9),R(IM+10),R(IM+11),
c	1R(IM+12),R(IM+13),R(IM+14),R(IM+15),
c	1L(5),L(6))
c	call ADINACGET(CMAT(1),CMAT(2),CMAT(3),CMAT(4),CMAT(5),CMAT(6),
c	1CMAT(7),CMAT(8),CMAT(9),CMAT(10),CMAT(11),CMAT(12),
c	1CMAT(13),CMAT(14),CMAT(15))
c	open (unit = 1, file = "debugprints.txt", access = 'APPEND')	
c	write (1,*) "Result: ",ADINACRESULT
c	close (1)
c         CMAT(1 ) =  VISCOSITY
c         CMAT(2 ) =  1.0
c         CMAT(3 ) =  1.0
c         CMAT(4 ) =  0.D0
c         CMAT(5)  =  1000.0
c         CMAT(6 ) =  0.D0
c         CMAT(7 ) =  0.D0
c         CMAT(8 ) =  0.D0
c         CMAT(9 ) =  0.D0
c         CMAT(10) =  0.D0
c         CMAT(11) =  0.D0
c         CMAT(12) =  0.D0
C
         GO TO 9999
C----------------------------------------------------------------------	  
	  IF(CHEAD(1:6).EQ.'CATERP') THEN
C
C        GET THE CURRENT VALUE OF MASS-RATIO
C
         IM       =  L(20)
         S        =  R(IM)
C
C        SET UP THE AIR MATERIALS
C
         DENAIR     =  0.995D0
         VISAIR     =  2.082D-5
         CPVAIR     =  1009.D0
         XKCAIR     =  0.0304D0
C
C        SET UP THE OIL MATERIALS
C
         DENOIL     =  853.9D0
         VISOIL     =  3.56D-2
         CPVOIL     =  2118.D0
         XKCOIL     =  0.138D0
C
C        COMPUTE THE VISCOSITY AND DENSITY OF THE MIXTURE
C
         IF(S.GT.1.D0) THEN
            VIS      =  VISOIL
            DEN      =  DENOIL
            CPV      =  CPVOIL
            XKC      =  XKCOIL
         ELSE IF(S.LE.0.D0) THEN
            VIS      =  VISAIR
            DEN      =  DENAIR
            CPV      =  CPVAIR
            XKC      =  XKCAIR
         ELSE
            VIS      =  VISAIR + S*(VISOIL-VISAIR)
            DEN      =  DENAIR + S*(DENOIL-DENAIR)
            CPV      =  CPVAIR + S*(CPVOIL-CPVAIR)
            XKC      =  XKCAIR + S*(XKCOIL-XKCAIR)
         ENDIF
C
C        PASS OUT ALL MATERIAL DATA REQUIRED
C
         CMAT(1 ) =  VIS
         CMAT(2 ) =  CPV
         CMAT(3 ) =  XKC
         CMAT(4 ) =  0.D0
         CMAT(5)  =  DEN
         CMAT(6 ) =  0.D0
         CMAT(7 ) =  0.D0
         CMAT(8 ) =  0.D0
         CMAT(9 ) =  0.D0
         CMAT(10) =  0.D0
         CMAT(11) =  0.D0
         CMAT(12) =  0.D0
C
         GO TO 9999
      ENDIF
C
C----------------------------------------------------------------------
      IF(CHEAD(1:6).EQ.'F71001' .OR. CHEAD(1:5).EQ.'F7518') THEN
C       +---------------------------------------------------+
C       | THIS FOR THE INTERNAL VERIFICATION PROBLEM F71001 |
C       | WHICH HAS A TEMPERATURE DEPENDENT DENSITY         |
C       +---------------------------------------------------+
         CMAT(1 ) =  8.D-6
         CMAT(2 ) =  14030.D0
         CMAT(3 ) =  0.156D0
         CMAT(4 ) =  0.D0
         CMAT(5 ) =  0.086D0
         CMAT(6 ) =  0.D0
         CMAT(7 ) =  0.D0
         CMAT(8 ) =  0.D0
         CMAT(9 ) =  0.D0
         CMAT(10) =  0.D0
         CMAT(11) =  0.D0
         CMAT(12) =  0.D0
         CMAT(13) =  0.D0
C
         IT       =  L(11)
         T        =  R(IT)
         TP1      =  100.D0
         TP2      =  1000.D0
         DP1      =  0.086D0
         DP2      =  0.053D0
         IF(T .LT. TP1) THEN
            CMAT(5)  =  DP1
         ELSE IF(T .LT. TP2) THEN
            CMAT(5)  =  DP1 + (T-TP1)*(DP2-DP1)/(TP2-TP1)
         ELSE
            CMAT(5)  =  DP2
         ENDIF
C
         GO TO 9999
      ENDIF
C
C----------------------------------------------------------------------
      IF(CHEAD(1:6).EQ.'f76327') THEN
C       +---------------------------------------------------+
C       | THIS FOR THE INTERNAL VERIFICATION PROBLEM f76327 |
C       | WHICH HAS D = D(T) (LINEAR+OTHERS)                |
C       +---------------------------------------------------+
         CMAT(1 ) =  8.D-6
         CMAT(2 ) =  14030.D0
         CMAT(3 ) =  0.156D0
         CMAT(4 ) =  0.D0
         CMAT(5 ) =  0.086D0
         CMAT(6 ) =  0.D0
         CMAT(7 ) =  0.D0
         CMAT(8 ) =  0.D0
         CMAT(9 ) =  0.D0
         CMAT(10) =  0.D0
         CMAT(11) =  0.D0
         CMAT(12) =  1.D+31
         CMAT(13) =  0.D0
C
         IT       =  L(11)
         T        =  R(IT)
         TP1      =  100.D0
         TP2      =  1000.D0
         DP1      =  0.086D0
         DP2      =  0.075D0
         IF(T .LT. TP1) THEN
            CMAT(5)  =  DP1
         ELSE IF(T .LT. TP2) THEN
            CSI      = (T-TP1)/(TP2-TP1)
            CMAT(5)  =  DP1 + (DP2-DP1)*CSI*CSI*(3.D0-2.D0*CSI)
            CMAT(14) =  (DP2-DP1)*6.D0*CSI*(1.D0-CSI)/(TP2-TP1)
         ELSE
            CMAT(5)  =  DP2
         ENDIF
C
         GO TO 9999
      ENDIF
C
C----------------------------------------------------------------------
      IF(CHEAD(1:5).EQ.'F7100' .OR. CHEAD(1:5).EQ.'F7519') THEN
C       +---------------------------------------------------+
C       | THIS FOR THE INTERNAL VERIFICATION PROBLEM F7100X |
C       | WHICH HAS A Z-COODINATE DEPENDENT DENSITY (X=3-8) |
C       +---------------------------------------------------+
         CMAT(1 ) =  0.2D0
         CMAT(2 ) =  0.D0
         CMAT(3 ) =  0.D0
         CMAT(4 ) =  0.D0
         CMAT(5 ) =  0.D0
         CMAT(6 ) =  0.D0
         CMAT(7 ) =  0.D0
         CMAT(8 ) =  0.D0
         CMAT(9 ) =  0.D0
         CMAT(10) =  0.D0
         CMAT(11) =  0.D0
         CMAT(12) =  0.D0
         CMAT(13) =  0.D0
C
         IZ       =  L(16)
         Z        =  R(IZ)
         CMAT(5)  =  1.D0 + Z/10.D0  * 4.0D0
C
         GO TO 9999
      ENDIF
C
C----------------------------------------------------------------------
      IF(CHEAD(1:6).EQ.'F74007') THEN
C       +-----------------------------------------------------+
C       | THIS FOR THE INTERNAL VERIFICATION PROBLEM F74007   |
C       | WHICH HAS A MASS DEPENDENT DENSITY                  |
C       +-----------------------------------------------------+
         IM       =  L(20)
         S        =  R(IM)
         CMAT(1 ) =  0.01D0
         CMAT(2 ) =  0.D0
         CMAT(3 ) =  0.D0
         CMAT(4 ) =  0.D0
         CMAT(5 ) =  0.D0
         CMAT(6 ) =  0.D0
         CMAT(7 ) =  0.D0
         CMAT(8 ) =  0.D0
         CMAT(9 ) =  0.D0
         CMAT(10) =  0.D0
         CMAT(11) =  0.D0
         CMAT(12) =  0.D0
         CMAT(5)  =  1.D0
C
         IF(S.GT.0.9D0) THEN
            CMAT(1)  =  0.1D0
            CMAT(5)  =  10.D0
         ELSE IF(S.LT.0.1D0) THEN
            CMAT(1)  =  0.01D0
            CMAT(5)  =  1.D0
         ELSE
            RRR      = (S-0.1D0)/0.8D0
            CMAT(1)  =  0.01D0 + RRR*(0.1D0-0.01D0)
            CMAT(5)  =  1.D0   + RRR*(10.D0-1.D0  )
         ENDIF
C
         GO TO 9999
      ENDIF
C----------------------------------------------------------
      IF(CHEAD(1:6).EQ.'fn4186' .OR. CHEAD(1:6).EQ.'fn4187') THEN
C----------------------------------------------------------
C     IDEAL GAS EQUATION
C     AIR PROPERTIES AT STANDARD CONDITIONS
C----------------------------------------------------------
         EPS   =  1.D-8
C
         CP    =  1000.D0
         CV    =  713.D0
         XMU   =  1.D-5
         XKCON =  0.026D0
         BETA  =  0.D0
         TREF  =  0.D0
         SIGM  =  0.D0
C
         IP    =  L(10)
         IT    =  L(11)
C
         P     =  R(IP)
         T     =  R(IT)
         RG    =  CP - CV
C
         RT    =  RG*T
         RO    =  P/MAX(RT,EPS)
         DRP   =  RO/P
         DRT   = -RO/T
C
         CMAT(1 ) =  XMU
         CMAT(2 ) =  CP
         CMAT(3 ) =  XKCON
         CMAT(4 ) =  BETA
         CMAT(5 ) =  RO
         CMAT(6 ) =  0.D0
         CMAT(7 ) =  0.D0
         CMAT(8 ) =  0.D0
         CMAT(9 ) =  0.D0
         CMAT(10) =  0.D0
         CMAT(11) =  0.D0
         CMAT(12) =  0.D0
         CMAT(13) =  CV
         CMAT(14) =  DRP
         CMAT(15) =  DRT
C
         GO TO 9999
      ENDIF
C----------------------------------------------------------------------
      IF(CHEAD(1:6).EQ.'fn4188' .OR.
     .   CHEAD(1:6).EQ.'fn4189' .OR.
     .   CHEAD(1:6).EQ.'fn4190' .OR.
     .   CHEAD(1:6).EQ.'fn4191') THEN
C----------------------------------------------------------
C     VAN DER WAALS REAL GAS EQUATION
C     CONSTANTS A AND B ARE DEFINED AS FOLLOWS:
C     A=27*R^2*Tc^2/(64*Pc)
C     B=R*Tc/(8*Pc)
C----------------------------------------------------------
        EPS   =   1.D-8
        C01   =   1.D0
        PI    =   3.141593D0
C
C       AIR PROPERTIES AT STD CONDITIONS
C
        IF(CHEAD(1:6).EQ.'fn4190') GO TO 100
        CP    =   1000.D0
        CV    =   713.D0
        XMU   =   1.D-5
        XKCON =   0.026D0
        BETA  =   0.D0
        TREF  =   0.D0
        SIGM  =   0.D0
C
        WGTM  =   28.9D0  ! MOLECULAR WEIGTH KG/KMOL
        UGC   =   8314.D0 ! UNIVERSAL GAS CONSTANT  J/K*KMOL
C
        IP    =   L(10)
        IT    =   L(11)
C
        P     =   R(IP)
        T     =   R(IT)
        RG    =   UGC/WGTM
C
        A     =   162.59D0     ! J / M**3*KG
        B     =   0.0012568    ! M**3 / KG
        GO TO 1000
C
C       STEAM AT T=600 K AND P = 5 MPA
C
 100    CONTINUE
        CP    =   2834.7D0
        CV    =   2830.0D0
        XMU   =   20.D-6
        XKCON =   0.6D0
        BETA  =   0.D0
        TREF  =   0.D0
        SIGM  =   0.D0
C
        WGTM  =   18.D0    ! MOLECULAR WEIGTH KG/KMOL
        UGC   =   8314.D0  ! UNIVERSAL GAS CONSTANT  J/K*KMOL
C
        IP    =   L(10)
        IT    =   L(11)
C
        P     =   R(IP)
        T     =   R(IT)
        RG    =   UGC/WGTM
C
        A     =   1712.55D0   ! J / M**3*KG
        B     =   1.69D-3     ! M**3 / KG
C
 1000   CONTINUE
C
C       SOLVES CUBIC VDW EQUATIONN TO GET SPECIFIC VOLUME VV
C       V**3 + A1*V**2 + A2*V + A3 = 0
C
        A1    = - B - (RG*T/P)
        A2    =   A/P
        A3    = - A2*B
C
        QQ    =  ((3.D0*A2) - (A1*A1))/9.D0
        RR    =  ((9.D0*A1*A2) - (27.D0*A3) - (2.D0*A1*A1*A1))/54.D0
        QQ3   =  QQ*QQ*QQ
        RR2   =  RR*RR
        DD    =  QQ3 + RR2
C
        IF(DD.GT.-EPS) THEN
           SS  =  RR + SQRT(ABS(DD))
           SN  =  SIGN(C01,SS)
           SS  =  SN*(ABS(SS)**0.333D0)
           TT  =  RR - SQRT(ABS(DD))
           SN  =  SIGN(C01,TT)
           TT  =  SN*(ABS(TT)**0.333D0)
           VV  =  SS + TT - (A1/3.D0)
         ELSE
           IF(QQ.LT.0.D0) THEN
              SQQ =  SQRT(-QQ)
           ELSE
              SQQ =  SQRT(QQ)
           ENDIF
           TT  =  ACOS(-RR/SQRT(-QQ3))
           VV1 =  2.D0*SQQ*COS(TT/3.D0)
           VV2 =  2.D0*SQQ*COS((TT+2.D0*PI)/3.D0)
           VV3 =  2.D0*SQQ*COS((TT+4.D0*PI)/3.D0)
           VV  =  MAX(VV1,VV2)
           VV  =  MAX(VV,VV3)
         ENDIF
C
C        COMPUTE THE DENSITY AND ITS DERIVATIVES
C
         RO       =  1.D0/VV
         RO2      =  RO*RO
         DNN      =  A*VV - A*B - RG*T*VV*VV
         DDN      =  3.D0*VV*VV*P*P - 2.D0*VV*RG*T*P + A*P
         DRP      = -RO2*DNN/DDN
         DNN      = -RG*VV*VV
         DDN      =  3.D0*VV*VV*P - 2.D0*VV*RG*T + A
         DRT      =  RO2*DNN/DDN
C
         CMAT(1 ) =  XMU
         CMAT(2 ) =  CP
         CMAT(3 ) =  XKCON
         CMAT(4 ) =  BETA
         CMAT(5 ) =  RO
         CMAT(6 ) =  0.D0
         CMAT(7 ) =  0.D0
         CMAT(8 ) =  0.D0
         CMAT(9 ) =  0.D0
         CMAT(10) =  0.D0
         CMAT(11) =  0.D0
         CMAT(12) =  0.D0
         CMAT(13) =  CV
         CMAT(14) =  DRP
         CMAT(15) =  DRT
         GO TO 9999
      ENDIF
C----------------------------------------------------------------------
 9999 CONTINUE
      RETURN
      END
      SUBROUTINE ADFURM [DLLEXPORT] (MODE,CHEAD,TIME,IELG,IELM,IMAS,
     .                               NNOD,NM,H,DH,VESL,DEN,DIF,S0,SF,BM)
C=======================================================================
C
C     USER SUPPLIED MATERIAL DATA AND SOURCE TERMS FOR MASS TRANSFER
C
C-----------------------------------------------------------------------
C
C     INPUT:
C
C        MODE         = REFERRING PHASE (SEE EXPLANATIONS BELOW)
C        CHEAD        = PROBLEM HEADER
C        TIME         = CURRENT TIME
C        IELG         = ELEMENT GROUP NUMBER
C        IELM         = ELEMENT NUMBER
C        IMAS         = INDEX OF THE CURRENT SPECIES
C        NNOD         = NUMBER OF LOCAL NODES FOR NNOD-ELEMENT
C        H(*)         = INTERPOLATION FUNCTION
C        DH(3,*)      = DERIVATIVES OF H WITH RESPECT TO (X,Y,Z)
C        VESL( 1,  I) = X-VELOCITY     AT LOCAL NODE I
C        VESL( 2,  I) = Y-VELOCITY     AT LOCAL NODE I
C        VESL( 3,  I) = Z-VELOCITY     AT LOCAL NODE I
C        VESL( 4,  I) = PRESSURE       AT LOCAL NODE I
C        VESL( 5,  I) = TEMPERATURE    AT LOCAL NODE I
C        VESL( 6,  I) = X-DISPLACEMENT AT LOCAL NODE I
C        VESL( 7,  I) = Y-DISPLACEMENT AT LOCAL NODE I
C        VESL( 8,  I) = Z-DISPLACEMENT AT LOCAL NODE I
C        VESL( 9,  I) = TURBULENCE_K   AT LOCAL NODE I
C        VESL(10,  I) = TURBULENCE_E   AT LOCAL NODE I
C        VESL(11,  I) = X-COORDINATE   AT LOCAL NODE I
C        VESL(12,  I) = Y-COORDINATE   AT LOCAL NODE I
C        VESL(13,  I) = Z-COORDINATE   AT LOCAL NODE I
C        VESL(14+K,I) = MASS RATIO OF SPECIES-K (K=0,1,2,...,NMASS)
C        NM           = LENGTH OF THE FIRST ENTRY OF VESL
C
C-----------------------------------------------------------------------
C
C     OUTPUT MATERIAL DATA FOR MODE=1:
C
C        DEN      = BULK DENSITY OF MIXTURE
C                  (FOR INCOMP.FLUIDS, DEFAULT=SPECIFIED)
C                  (FOR COMP.FLUIDS, DEFAULT=COMPUTED USING STATE EQ.)
C        DIF(3,3) = DIFFUSIVITY TENSOR (MUST BE SPECIFIED HERE)
C
C-----------------------------------------------------------------------
C
C     OUTPUT MASS CREATION RATE FOR MODE=2:
C
C        DEFINITION:
C
C        MASS CREATION RATE = S0 - SF * (MASS RATIO OF SPECIES-IMAS)
C
C        S0 = THE TERM THAT WILL BE PUT ON THE RIGHT HAND SIDE
C        SF = COEF. THAT WILL BE PUT INTO THE STIFF MAXTRIX
C        FOR NUMERICAL STABILITY, ALWAYS MANAGE WELL SUCH THAT SF>=0
C
C-----------------------------------------------------------------------
C
C     OUTPUT BOUYANT FACTOR BM IN MOMENTUM EQUATION FOR MODE=3
C
C        DEFINITION:
C
C        BM = SUM{ BETMI*(SI-SI_REF) }
C
C        SI     = MASS RATIO OF SPECIES-I
C        SI_REF = REFERENCE MASS RATIO OF SPECIES-I
C        BETMI  = MASS VOLUME EXPANSION COEFFICIENT OF THE SPECIES-I
C
C======================================================================
C*I+++INCLUDE "implit.com"                        !         6
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER CHEAD*(*)
      DIMENSION H(NNOD),DH(3,NNOD),VESL(NM,NNOD)
      DIMENSION DIF(3,3)
	  
	  integer*4 SENDMR
	  integer*4 SENDCODE
C----------------------------------------------------------------------
      BM        =   0.D0
      DH(1,1)   =   DH(1,1)
      H(1)      =   H(1)
      IELG      =   IELG
      IELM      =   IELM
      TIME      =   TIME
      VESL(1,1) =   VESL(1,1)
C----------------------------------------------------------------------
c      IF(CHEAD(1:6).EQ.'am3mr4') THEN
	
	    IF(MODE.EQ.1) THEN
C
C		RATIO 1 - phase ratio
C		RATIO 2 - coefficient of boundary area
C		RATIO 3 - ions concentration in A
C		RATIO 4 - ions concentration in B
C
C       IMAS         = INDEX OF THE CURRENT SPECIES
		  IF(IMAS.EQ.1) THEN
			RATIO1=0.D0
			DO I=1,NNOD
			  RATIO1=RATIO1+VESL(15,I)*H(I)
			END DO
C		If phase ratio is almost 1, then diffusion is LARGE		
			IF(RATIO1.GT.0.95D0) THEN
			  DIFC=0.1D0+5.0**(RATIO1)
		  ELSE
			  DIFC=0.1D0
	      ENDIF
			DEN      = 1.D0
			DIF(1,1) = DIFC
			DIF(2,2) = DIFC
			DIF(3,3) = DIFC
			DIF(1,2) = 0.0D0
			DIF(1,3) = 0.0D0
			DIF(2,1) = 0.0D0
			DIF(2,3) = 0.0D0
			DIF(3,1) = 0.0D0
			DIF(3,2) = 0.0D0
		  ELSE IF(IMAS.EQ.2) THEN
			RATIO2=0.D0
			DO I=1,NNOD
			  RATIO2=RATIO2+VESL(15,I)*H(I)
			END DO
C		If boundary area coeff is 1, then diffusion is LARGE				
			IF(RATIO2.GT.1.D0) THEN
			  DIFC=0.1D0+(RATIO2-1.D0)*1.D0
			ELSE
			  DIFC=0.1D0
			ENDIF
			DEN      = 1.D0
			DIF(1,1) = DIFC
			DIF(2,2) = DIFC
			DIF(3,3) = DIFC
			DIF(1,2) = 0.0D0
			DIF(1,3) = 0.0D0
			DIF(2,1) = 0.0D0
			DIF(2,3) = 0.0D0
			DIF(3,1) = 0.0D0
			DIF(3,2) = 0.0D0
		  ELSE IF(IMAS.EQ.3) THEN
			RATIO3=0.D0
			DO I=1,NNOD
			  RATIO3=RATIO3+VESL(15,I)*H(I)
			END DO
			IF(RATIO3.GT.10.D0) THEN
			  DIFC=0.1D0+(RATIO3-10.D0)*1.D0
			ELSE
			 DIFC=0.1D0
			ENDIF
			DEN      = 1.D0
			DIF(1,1) = DIFC
			DIF(2,2) = DIFC
			DIF(3,3) = DIFC
			DIF(1,2) = 0.0D0
			DIF(1,3) = 0.0D0
			DIF(2,1) = 0.0D0
			DIF(2,3) = 0.0D0
			DIF(3,1) = 0.0D0
			DIF(3,2) = 0.0D0
		  ELSE IF(IMAS.EQ.4) THEN
			RATIO4=0.D0
			DO I=1,NNOD
			 RATIO4=RATIO4+VESL(15,I)*H(I)
			END DO
			IF(RATIO4.GT.20.D0) THEN
			  DIFC=0.1D0+(RATIO4-20.D0)*1.D0
			ELSE
			  DIFC=0.1D0
			ENDIF
			DEN      = 1.D0
			DIF(1,1) = DIFC
			DIF(2,2) = DIFC
			DIF(3,3) = DIFC
			DIF(1,2) = 0.0D0
			DIF(1,3) = 0.0D0
			DIF(2,1) = 0.0D0
			DIF(2,3) = 0.0D0
			DIF(3,1) = 0.0D0
			DIF(3,2) = 0.0D0
		  ENDIF
      ENDIF
      IF(MODE.EQ.2) THEN
C       +-------------------------------------------------+
C       |                                                 |
C       |    SPECIFY THE SOURCE IN TERMS OF S0 AND SF     |
C       |                                                 |
C       +-------------------------------------------------+	     
	  RATIO1=0.D0
	  RATIO2=0.D0
      RATIO4=0.D0		 
	  RATIO3=0.D0
	  PRESSURE=0.D0
      TEMPERATURE=0.D0
	  TUR_K=0.D0
	  TUR_E=0.D0
	  XCOORD=0.D0
	  YCOORD=0.D0
	  ZCOORD=0.D0
	  VX=0.D0
	  VY=0.D0
	  VZ=0.D0
	  DO I=1,NNOD
	    PRESSURE=PRESSURE+VESL(4,I)*H(I)
		TEMPERATURE=TEMPERATURE+VESL(5,I)*H(I)
		TUR_K=TUR_K+VESL(9,I)*H(I)
		TUR_E=TUR_E+VESL(10,I)*H(I)
		XCOORD=XCOORD+VESL(11,I)
		YCOORD=YCOORD+VESL(12,I)
		ZCOORD=ZCOORD+VESL(13,I)
		VX=VX+VESL(1,I)*H(I)
		VY=VY+VESL(2,I)*H(I)
		VZ=VZ+VESL(3,I)*H(I)
		RATIO4=RATIO4+VESL(18,I)*H(I)
		RATIO3=RATIO3+VESL(17,I)*H(I)
		RATIO1=RATIO1+VESL(15,I)*H(I)
		RATIO2=RATIO2+VESL(16,I)*H(I)
	  END DO
	  XCOORD=XCOORD/NNOD
	  YCOORD=YCOORD/NNOD
	  ZCOORD=ZCOORD/NNOD
      VESLSUM=0.D0
	  DO I=1,NNOD
		VESLSUM=VESLSUM+VESL(15,I)*H(I)
	  END DO 
		  IXMAX=1
		  IXMIN=1
		  IYMAX=1
		  IYMIN=1
		  IZMAX=1
		  IZMIN=1
C		Find a node with biggest and smallest coordinates (nodes of element)
		  DO I=2,NNOD
			IF(VESL(11,I).GT.VESL(11,IXMAX)) IXMAX=I
			IF(VESL(11,I).LT.VESL(11,IXMIN)) IXMIN=I
			IF(VESL(12,I).GT.VESL(12,IYMAX)) IYMAX=I
			IF(VESL(12,I).LT.VESL(12,IYMIN)) IYMIN=I
			IF(VESL(13,I).GT.VESL(13,IZMAX)) IZMAX=I
			IF(VESL(13,I).LT.VESL(13,IZMIN)) IZMIN=I
		  END DO
c		Find a dimensions of the element
		  XDX=(VESL(11,IXMAX)-VESL(11,IXMIN))
		  YDY=(VESL(12,IYMAX)-VESL(12,IYMIN))
		  ZDZ=(VESL(13,IZMAX)-VESL(13,IZMIN))
c		Find a velocity derivatives
		  VX2=((VESL(1,IXMAX)-VESL(1,IXMIN)))/XDX
		  VY2=((VESL(2,IXMAX)-VESL(2,IXMIN)))/XDX
		  VZ2=((VESL(3,IXMAX)-VESL(3,IXMIN)))/XDX
c  		
		  SRX=SQRT(VX2**2+VY2**2+VZ2**2)
c		 
		  VX2=((VESL(1,IYMAX)-VESL(1,IYMIN)))/YDY
		  VY2=((VESL(2,IYMAX)-VESL(2,IYMIN)))/YDY
		  VZ2=((VESL(3,IYMAX)-VESL(3,IYMIN)))/YDY
c  
		  SRY=SQRT(VX2**2+VY2**2+VZ2**2)
		  VX2=((VESL(1,IZMAX)-VESL(1,IZMIN)))/ZDZ
		  VY2=((VESL(2,IZMAX)-VESL(2,IZMIN)))/ZDZ
		  VZ2=((VESL(3,IZMAX)-VESL(3,IZMIN)))/ZDZ
c  
		  SRZ=SQRT(VX2**2+VY2**2+VZ2**2)
c		Find a shear rate (2D or 3D)		  
		  SR=SQRT(SRY**2+SRZ**2)
c		  SR=SQRT(SRX**2+SRY**2+SRZ**2)			 

	    
c      zaleznie od imas, policz dyspersje lub udzia� odpowiedniej fazy (A lub B)
	SENDCODE =  SENDMR(SR,VX,VY,VZ,TIME,IELG,IELM,IMAS,NNOD,PRESSURE,TEMPERATURE,
	1TUR_K,TUR_E,XCOORD,YCOORD,ZCOORD,RATIO1,RATIO2,RATIO3,RATIO4)

	call READMR(SO,SF)
c    SF=SF
c	S0=0.D0

	GO TO 9999
c	  ENDIF
c		If shear rate is large, boundary area increases, if small - decreases
c		  IF(SR.GT.60.0) THEN
c			SF=1.D0
c					S0=1.D0
c			S0=10.D0*SR*SF
c		  ELSE IF(SR.LT.5.0) THEN
c			SF=100.D0
c			S0=0.0*SF
c		  ENDIF
c		ELSE	 

c		ENDIF
c		Ions ration in phases change rate is a function of boundary area
c	  IF(IMAS.EQ.3) THEN
c	    SF=500.D0*RATIO1*(4.0*RATIO2)
c		S0=RATIO4*SF
c	  ELSE IF(IMAS.EQ.4) THEN
c	    SF=500.D0*(1.D0-RATIO1)*(4.0*RATIO2)
c			 *(0.3*RATIO2)
c		S0=RATIO3*SF
c	  ENDIF
c	  GO TO 9999
	  ENDIF
C
      IF(MODE.EQ.3) THEN
C       +-------------------------------------------------+
C       |                                                 |
C       |    SPECIFY BM = SUM{ BETM*(SI-SIREF) } HERE     |
C       |                                                 |
C       +-------------------------------------------------+
C      ONLY FIRST SPECIES HAVE INFLUENCE ON BOUYANCY!!!!
      NMASS   = 1
      BM      = 0.D0
      DO 31 I = 1, NMASS
      IM      = 14 + I
      SI      = 0.D0
      SIREF   = 0.D0
	  ETMI   = -50.00D0
c        IF(I.EQ.2) BETMI = 0.D0
      DO 32 J = 1, NNOD
      SI      = SI + H(J)*VESL(IM,J)
32      CONTINUE
        BM      = BM + BETMI*(SI-SIREF)
31      CONTINUE
        GO TO 9999
      ENDIF

C-----------------------------------------------------------------------
 9999 CONTINUE
      RETURN
      END
      SUBROUTINE ADFUBC [DLLEXPORT] (CHEAD,L,NL,R,NR,VAR,NVAR,A,B)
C======================================================================
C
C
C     USER-SUPPLIED BOUNDARY CONDITIONS
C
C     THIS ROUTINE WILL BE CALLED AT EACH INTEGRATION POINT OF EVERY
C     BOUNDARY ELEMENT OF BOUNDARY CONDITION LABEL "IBCL" AT EACH
C     NONLINEAR EQUILIBRIUM ITERATION AT EACH TIME STEP.
C
C     HERE WE EXPRESS THE BOUNDARY CONDITION AS A SOURCE S, IMPOSED
C     INTO THE COMPUTATIONAL DOMAIN.
C
C     -----------------------------------------------------------------
C
C     PASSED IN:  L(NL),R(NR),VAR(NVAR)
C
C     PASSED OUT: A,B(NVAR)
C
C     -----------------------------------------------------------------
C     WHERE
C
C        L( 1) = IBCL   = BOUNDARY CONDITION LABEL (NAME IN THE
C                         BOUNDARY-CONDITION COMMAND)
C        L( 2) = IVARID = ID OF THE VARIABLE (=1,2 AND 3 FOR FLUID,
C                         TURBULENCE AND MASS (VARIABLE IN THE
C                         BOUNDARY-CONDITION COMMAND)
C        L( 3) = ICOMID = ID OF THE CURRENT EQUATION:
C                       = (1,2,3,4,5) FOR (U,V,W,P,T  ), IF IVARID=1
C                       = (1,2      ) FOR (K,E/W)      , IF IVARID=2
C                       = (1,2,...  ) FOR (S_1,S_2,...), IF IVARID=3
C        L( 4) = METHOD = ID OF THE ITERATION METHOD:
C                       = 0, IF SUCCESIVE SUBSTITUTION METHOD
C                       = 1, IF NEWTON METHOD
C        L( 5) = IELG   = ELEMENT GROUP NUMBER
C        L( 6) = IELM   = ELEMENT NUMBER
C        L( 7) = IU     = ENTRY OF THE X-VELOCITY       IN VAR(*)
C        L( 8) = IV     = ENTRY OF THE Y-VELOCITY       IN VAR(*)
C        L( 9) = IW     = ENTRY OF THE Z-VELOCITY       IN VAR(*)
C        L(10) = IP     = ENTRY OF THE PRESSURE         IN VAR(*)
C        L(11) = IT     = ENTRY OF THE TEMPERATURE      IN VAR(*)
C        L(12) = IK     = ENTRY OF THE TURBULENCE-K     IN VAR(*)
C        L(13) = IE     = ENTRY OF THE TURBULENCE-E/W   IN VAR(*)
C        L(14) = IX     = ENTRY OF THE X-COORDINATE     IN VAR(*)
C        L(15) = IY     = ENTRY OF THE Y-COORDINATE     IN VAR(*)
C        L(16) = IZ     = ENTRY OF THE Z-COORDINATE     IN VAR(*)
C        L(17) = IM     = ENTRY OF THE MASS SPECIES     IN VAR(*)
C        L(18) = IQ     = ENTRY OF THE CURRENT VARIABLE IN VAR(*)
C                         NOTE THAT IQ DEPENDS ON IVARID AND ICOMID:
C                       IQ = IU         IF(IVARID=1 AND ICOMID=1)
C                          = IV         IF(IVARID=1 AND ICOMID=2)
C                          = IW         IF(IVARID=1 AND ICOMID=3)
C                          = IP         IF(IVARID=1 AND ICOMID=4)
C                          = IT         IF(IVARID=1 AND ICOMID=5)
C                          = IK         IF(IVARID=2 AND ICOMID=1)
C                          = IE         IF(IVARID=2 AND ICOMID=2)
C                          = IM+ICOMID, IF(IVARID=3)
C        L(20) = JX     = ENTRY OF THE X-OUT-NORMAL     IN   R(*)
C        L(21) = JY     = ENTRY OF THE Y-OUT-NORMAL     IN   R(*)
C        L(22) = JZ     = ENTRY OF THE Z-OUT-NORMAL     IN   R(*)
C        L(23) = JT     = ENTRY OF THE TIME             IN   R(*)
C
C        VAR(IU)   = U  = X-VELOCITY
C        VAR(IV)   = V  = Y-VELOCITY
C        VAR(IW)   = W  = Z-VELOCITY
C        VAR(IP)   = P  = PRESSURE
C        VAR(IT)   = T  = TEMPERATURE
C        VAR(IK)   = TK = TURBULENCE_K
C        VAR(IE)   = TE = TURBULENCE_E OR TURBULENCE-W
C        VAR(IX)   = X  = X-COORDINATE
C        VAR(IY)   = Y  = Y-COORDINATE
C        VAR(IZ)   = Z  = Z-COORDINATE
C        VAR(IM)   = S_1= MASS-RATIO_0 (FLUID ITSELF)
C        VAR(IM+1) = S_2= MASS-RATIO_1
C        ......
C        VAR(IQ)   = VALUE OF THE CURRENT VARIABLE
C
C        R(JX)     = SX = X-OUT-NORMAL OF THE BOUNDARY
C        R(JY)     = SY = Y-OUT-NORMAL OF THE BOUNDARY
C        R(JZ)     = SZ = Z-OUT-NORMAL OF THE BOUNDARY
C        R(JT)     = CURRENT TIME
C
C     -----------------------------------------------------------------
C     THE DEFINITION OF THE PASSED OUT VARIABLES A AND B(*) DEPENDS ON
C     THE FLAGS "IVARID", "METHOD" AND THE FLOW TYPE (INCOMPRESSIBLE
C     OR COMPRESSIBLE).
C
C       +------------------------------------------------------+
C       |                                                      |
C       |   CASE: FLUID OR TURBULENCE            (IVARID<=2)   |
C       |         SUCCESSIVE SUBSTITUTION METHOD (METHOD=0 )   |
C       |         INCOMPRESSIBLE FLOWS                         |
C       |                                                      |
C       +------------------------------------------------------+
C
C        THE SOURCE S IS SPLITTED INTO THE FORM
C
C           S  = A + B(IU)*U  + B(IV)*V  + B(IW)*W
C                  + B(IP)*P  + B(IT)*T
C                  + B(IK)*TK + B(IE)*TE
C
C        CAUTION: THERE ARE MANY WAYS TO SPLIT S. SOME OF
C                 THEM MAY CAUSE NUMERICAL INSTABILITY. IT
C                 IS RECOMMENDED TO ALWAYS KEEP B(IQ)<=0.
C
C       +------------------------------------------------------+
C       |                                                      |
C       |   CASE: FLUID OR TURBULENCE            (IVARID<=2)   |
C       |         NEWTON METHOD                  (METHOD=1 )   |
C       |         INCOMPRESSIBLE FLOWS                         |
C       |                                                      |
C       +------------------------------------------------------+
C
C        A     = THE SOURCE S
C        B(IU) = DERIVATIVE OF S WITH RESPECT TO U
C        B(IV) = DERIVATIVE OF S WITH RESPECT TO V
C        B(IW) = DERIVATIVE OF S WITH RESPECT TO W
C        B(IP) = DERIVATIVE OF S WITH RESPECT TO P
C        B(IT) = DERIVATIVE OF S WITH RESPECT TO T
C        B(IK) = DERIVATIVE OF S WITH RESPECT TO TK
C        B(IE) = DERIVATIVE OF S WITH RESPECT TO TE
C
C       +------------------------------------------------------+
C       |                                                      |
C       |   CASE: FLUID OR TURBULENCE            (IVARID<=2)   |
C       |         SUCCESSIVE SUBSTITUTION METHOD (METHOD=0 )   |
C       |         COMPRESSIBLE FLOWS                           |
C       |                                                      |
C       +------------------------------------------------------+
C
C        THE SOURCE S IS SPLITTED INTO THE FORM
C
C           S  = A + B(IU)*DU + B(IV)*DV + B(IW)*DW
C                  + B(IP)*D  + B(IT)*E
C                  + B(IK)*TK + B(IE)*TE
C
C        WHERE, D=DENSITY, E=TOTAL ENERGY, DU=D*U, DV=D*V
C               AND DW=D*W.
C
C        CAUTION: THERE ARE MANY WAYS TO SPLIT S. SOME OF
C                 THEM MAY CAUSE NUMERICAL INSTABILITY. IT
C                 IS RECOMMENDED TO ALWAYS KEEP B(IQ)<=0.
C
C       +------------------------------------------------------+
C       |                                                      |
C       |   CASE: FLUID OR TURBULENCE            (IVARID<=2)   |
C       |         NEWTON METHOD                  (METHOD=1 )   |
C       |         COMPRESSIBLE FLOWS                           |
C       |                                                      |
C       +------------------------------------------------------+
C
C        A     = THE SOURCE S
C        B(IU) = DERIVATIVE OF S WITH RESPECT TO DU
C        B(IV) = DERIVATIVE OF S WITH RESPECT TO DV
C        B(IW) = DERIVATIVE OF S WITH RESPECT TO DW
C        B(IP) = DERIVATIVE OF S WITH RESPECT TO D
C        B(IT) = DERIVATIVE OF S WITH RESPECT TO E
C        B(IK) = DERIVATIVE OF S WITH RESPECT TO TK
C        B(IE) = DERIVATIVE OF S WITH RESPECT TO TE
C
C        WHERE, D=DENSITY, E=TOTAL ENERGY, DU=D*U, DV=D*V
C               AND DW=D*W.
C
C       +------------------------------------------------------+
C       |                                                      |
C       |   CASE: MASS TRANSFERS                 (IVARID=3)    |
C       |         SUCCESSIVE SUBSTITUTION AND NEWTON METHODS   |
C       |         INCOMPRESSIBLE AND COMPRESSIBLE FLOWS        |
C       |                                                      |
C       +------------------------------------------------------+
C
C        THE SOURCE S IS SPLITTED INTO THE FORM
C
C           S  = A + B(IQ)*VAR(IQ)
C
C        CAUTION: THERE ARE MANY WAYS TO SPLIT S. SOME OF
C                 THEM MAY CAUSE NUMERICAL INSTABILITY. IT
C                 IS RECOMMENDED TO ALWAYS KEEP B(IQ)<=0.
C
C======================================================================
C*I+++INCLUDE "implit.com"                        !         6
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER  CHEAD*(*)
      DIMENSION  L(NL),R(NR),VAR(NVAR),B(NVAR)
C----------------------------------------------------------------------
C     START TO CODE HERE
C----------------------------------------------------------------------
      PARAMETER (NMASS=2)
      DIMENSION  XM(NMASS),X(NMASS)
C-----------------------------------------------------------------------
      R(1)   =   R(1)
C-----------------------------------------------------------------------
      IBCL   =   L( 1)
      IVARID =   L( 2)
      ICOMID =   L( 3)
C     METHOD =   L( 4)
      IM     =   L(17)
      IQ     =   L(18)
      UNDER  =   1.D-20
C
C======================================================================
C
      IF(IVARID.EQ.1) THEN
C
C     +-------------------------------------------------------------+
C     |                                                             |
C     |  FLUID BOUNDARY CONDITIONS ARE CODED HERE (U,V,W,P,T)       |
C     |                                                             |
C     +-------------------------------------------------------------+
C
         IF(CHEAD(1:6).EQ.'F76360' .AND. IBCL.EQ.2) THEN
            IU    =   L( 7)
            IV    =   L( 8)
            IW    =   L( 9)
            IP    =   L(10)
            IT    =   L(11)
            IK    =   L(12)
            IE    =   L(13)
            A     =   0.D0
            B(IU) =   0.D0
            B(IV) =   0.D0
            B(IW) =   0.D0
            B(IP) =   0.D0
            B(IT) =   0.D0
            B(IK) =   0.D0
            B(IE) =   0.D0
            IF(ICOMID.EQ.2) THEN
               A     =   1.D+20*(1.D0-VAR(IV))
               B(IQ) = - 1.D+20
               GO TO 9999
            ENDIF
         ENDIF
C
C        +----------------------------------------------------------+
C        |  END OF FLUID BOUNDARY CONDITIONS HERE                   |
C        +----------------------------------------------------------+
C
         GO TO 9999
      ENDIF
C
C======================================================================
C
      IF(IVARID.EQ.2) THEN
C
C     +-------------------------------------------------------------+
C     |                                                             |
C     |  TURBULENCE BOUNDARY CONDITIONS ARE CODED HERE (K,E/W)      |
C     |                                                             |
C     +-------------------------------------------------------------+
C
C
C        +----------------------------------------------------------+
C        |  END OF RTURBULENCE BOUNDARY CONDITIONS HERE             |
C        +----------------------------------------------------------+
C
         GO TO 9999
      ENDIF
C
C======================================================================
C
      IF(IVARID.EQ.3) THEN
C
C     +-------------------------------------------------------------+
C     |                                                             |
C     |  MASS TRANSFER BOUNDARY CONDITIONS ARE CODED HERE           |
C     |                                                             |
C     |  THE FOLLOWING EXAMPLE IS FOR 3 BOUNDARY CONDITIONS:        |
C     |                                                             |
C     |  IBCL = 1:  SPECIFY THE MASS RATIO 0.2                      |
C     |  IBCL = 2:  SPECIFY THE MASS RATIO 0.1                      |
C     |  IBCL = 4:  CVD CONDITION                                   |
C     |                                                             |
C     |  NMASS  =   NUMBER OF REACTING SPECIES (=2 HERE)            |
C     |  XM(*)  =   MOLECULAR WEIGHT                                |
C     |  X(*)   =   MOLE FRACTION                                   |
C     |  XNU    =   STOICHIOMETRIC COEFFICIENT                      |
C     |  XKA    =   KENETIC REACTION RATE CONSTANT                  |
C     |  NOTE: THE LARGE NUMBER 1.D+20 USED HERE IS TO FORCE THE    |
C     |        SPECIFIED VALUE TO BE RESPECTED IN THE FINAL EQUATION|
C     |                                                             |
C     +-------------------------------------------------------------+
C
         IF(CHEAD(1:6).EQ.'F76360' .AND. IBCL.EQ.3) THEN
            B(IQ) = - 1.D+20
            A     =   0.D0
            GO TO 9999
         ENDIF
C
         IF(IBCL.EQ.1) THEN
            IF(ICOMID.EQ.1) THEN
               SS =   0.2D0
            ELSE
               SS =   0.1D0
            ENDIF
            B(IQ) = - 1.D+20
            A     =   1.D+20*SS
            GO TO 9999
         ENDIF
C
         IF(IBCL.EQ.2) THEN
            IF(ICOMID.EQ.1) THEN
               SS =   0.1D0
            ELSE
               SS =   0.2D0
            ENDIF
            B(IQ) = - 1.D+20
            A     =   1.D+20*SS
            GO TO 9999
         ENDIF
C
         IF(IBCL.EQ.4) THEN
            XM(1) =   2.D0
            XM(2) =   1.D0
            XNU   =   1.0D0
            XKA   =   0.002D0
C
C           COMPUTE X(*)
C
            SUM      =   0.D0
            DO 1 I   =   1, NMASS
            X(I)     =   VAR(IM+I)/XM(I)
            SUM      =   SUM + X(I)
 1          CONTINUE
            SUM      =   MAX(SUM,UNDER)
            DO 2 I   =   1, NMASS
            X(I)     =   X(I)/SUM
 2          CONTINUE
C
C           COMPUTE A AND B
C
            XX       =   1.D0
            DO 3 I   =   1, NMASS
            IF(I.EQ.ICOMID) GO TO 3
            XX       =   XX*X(I)
 3          CONTINUE
            FAC      =   XNU*XKA
            B(IQ)    = - FAC/SUM
            A        = - FAC*XM(ICOMID)*XX
            GO TO 9999
         ENDIF
C
C        +----------------------------------------------------------+
C        |  END OF MASS BOUNDARY CONDITIONS HERE                    |
C        +----------------------------------------------------------+
C
      ENDIF
C----------------------------------------------------------------------
 9999 CONTINUE
      RETURN
      END
      SUBROUTINE ADFUCP [DLLEXPORT] (CHEAD,LINP,RINP,LOUT,ROUT)
C======================================================================
C
C
C     USER-CONTROLLED PARAMETERS
C
C     THIS ROUTINE WILL BE CALLED BEFORE EACH FSI ITERATION
C     IN ITERATIVE COUPLING OF FSI PROBLEMS
C
C     -----------------------------------------------------------------
C
C     PASSED IN:  CHEAD,LINP(*),RINP(*)
C     PASSED OUT:       LOUT(*),ROUT(*)
C
C     -----------------------------------------------------------------
C     WHERE
C
C        CHEAD   = NAME OF THE PROBLEM
C        LINP(1) = CURRENT TIME STEP
C        LINP(2) = CURRENT FSI ITERATION
C        LINP(3) = LATEST NUMBER OF ITERATIONS IN FLUID
C        LINP(4) = LATEST NUMBER OF ITERATIONS IN SOLID
C        RINP(1) = CURRENT TIME
C        RINP(2) = LAST STRESS RESIDUAL
C        RINP(3) = LAST DISPLACEMENT RESIDUAL
C
C        ROUT(1) = CURRENT (IN) AND UPDATED (OUT) RELAXATION FOR STRESS
C        ROUT(2) = CURRENT (IN) AND UPDATED (OUT) RELAXATION FOR DISPL.
C
C======================================================================
C*I+++INCLUDE "implit.com"                        !         6
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      CHARACTER  CHEAD*(*)
      DIMENSION  LINP(*),RINP(*)
      DIMENSION  LOUT(*),ROUT(*)
C
C     IF CONVERGENCE HISTORY IS REQUIRED, SAVE THEM IN LOCAL ARRAYS.
C     FOR EXAMPLE, HERE, RFOLD/RDOLD AND RFNEW/RDNEW ARE NEW AND OLD
C     STRESS/DISPLACEMENT RESIDUALS RESPECTIVELY (MAXIMUM THE LAST 5
C     OF THEM ARE SAVED)
C
      PARAMETER (MXITE=5)
      COMMON /CGU001/RFOLD(MXITE),RDOLD(MXITE)
      COMMON /CGU002/RFNEW(MXITE),RDNEW(MXITE)
      COMMON /CGU003/IOLD,INEW
C----------------------------------------------------------------------
C     ADD YOU OWN CODE HERE
C----------------------------------------------------------------------
C
C----------------------------------------------------------------------
C     THIS IS FOR A TEST PROBLEM (F076 CHANGED HEADER)
C----------------------------------------------------------------------
      IF(CHEAD(1:6).EQ.'TEST_F') THEN
C
         LOUT(1) =   0
         NT      =   LINP(1)
         IT      =   LINP(2)
         RF      =   RINP(2)
         RD      =   RINP(3)
C
         IF(IT.LE.1) THEN
C
C           INITIATE RESIDUAL HISTORY
C
            IF(NT.LE.1) THEN
               DO I     =   1, MXITE
               RFOLD(I) =   0.D0
               RDOLD(I) =   0.D0
               ENDDO
               IOLD     =   0
               INEW     =   0
            ELSE
               DO I     =   1, MXITE
               RFOLD(I) =   RFNEW(I)
               RDOLD(I) =   RDNEW(I)
               ENDDO
               IOLD     =   INEW
               INEW     =   0
            ENDIF
C
            ROUT(1)     =   0.2D0
            ROUT(2)     =   0.2D0
C
         ELSE
C
C           SAVE THE LAST RESIDUALS
C
            INEW        =   INEW + 1
            IF(INEW.GT.MXITE) INEW = 1
            RFNEW(INEW) =   RF
            RDNEW(INEW) =   RD
C
C           UPDATE RELAXTION FACTORS
C
            RLXF        =   ROUT(1)
            RLXD        =   ROUT(2)
C
            IF(RF.GT.0.5D0) THEN
               RLXF     =   0.2D0
               RLXD     =   0.2D0
            ELSE IF(RF.LT.0.05D0) THEN
               RLXF     =   0.9D0
               RLXD     =   0.9D0
            ELSE
               C1       = ( RF - 0.05D0 )/0.45D0
               C2       =   1.D0 - C1
               RLXF     =   C1*0.2D0 + C2*0.9D0
               RLXD     =   C1*0.2D0 + C2*0.9D0
            ENDIF
C
            ROUT(1)     =   RLXF
            ROUT(2)     =   RLXD
         ENDIF
      ENDIF
C----------------------------------------------------------------------
 9999 CONTINUE
      RETURN
      END
      SUBROUTINE ADFUCM [DLLEXPORT] (MODE,CHEAD,TIME,ICEL,IM1,IMAS,SSC,
     .                               XXC, NCELL,DEN, DIF, S0, SF,  BM)
C=======================================================================
C
C     USER SUPPLIED MATERIAL DATA AND SOURCE TERMS FOR MASS TRANSFER
C    (FOR FCBI-C ELEMENTS)
C
C-----------------------------------------------------------------------
C
C     INPUT:
C
C        MODE         = REFERRING PHASE (SEE EXPLANATIONS BELOW)
C        CHEAD        = PROBLEM HEADER
C        TIME         = CURRENT TIME
C        ICEL         = CELL NUMBER (OVERALL ELEMENT NUMBER)
C        IM1          = INDEX OF THE FIRST SPECIES
C        IMAS         = INDEX OF THE CURRENT SPECIES
C        SSC(NCELL,*) = REQUIRED SOLUTIONS
C        XXC(3,NCELL) = CELL-CENTER COORDINATES
C
C-----------------------------------------------------------------------
C
C     OUTPUT MATERIAL DATA FOR MODE=1:
C
C        DEN      = BULK DENSITY OF MIXTURE
C                  (FOR INCOMP.FLUIDS, DEFAULT=SPECIFIED)
C                  (FOR COMP.FLUIDS, DEFAULT=COMPUTED USING STATE EQ.)
C        DIF(3,3) = DIFFUSIVITY TENSOR (MUST BE SPECIFIED HERE)
C
C-----------------------------------------------------------------------
C
C     OUTPUT MASS CREATION RATE FOR MODE=2:
C
C        DEFINITION:
C
C        MASS CREATION RATE = S0 - SF * (MASS RATIO OF SPECIES-IMAS)
C
C        S0 = THE TERM THAT WILL BE PUT ON THE RIGHT HAND SIDE
C        SF = COEF. THAT WILL BE PUT INTO THE STIFF MAXTRIX
C        FOR NUMERICAL STABILITY, ALWAYS MANAGE WELL SUCH THAT SF>=0
C
C-----------------------------------------------------------------------
C
C     OUTPUT BOUYANT FACTOR BM IN MOMENTUM EQUATION FOR MODE=3
C
C        DEFINITION:
C
C        BM = SUM{ BETMI*(SI-SI_REF) }
C
C        SI     = MASS RATIO OF SPECIES-I
C        SI_REF = REFERENCE MASS RATIO OF SPECIES-I
C        BETMI  = MASS VOLUME EXPANSION COEFFICIENT OF THE SPECIES-I
C
C======================================================================
C*I+++INCLUDE "implit.com"                        !         6
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      CHARACTER CHEAD*(*)
      DIMENSION DIF(3,3)
      DIMENSION SSC(NCELL,*),XXC(3,*)
C----------------------------------------------------------------------
      IF(CHEAD(1:6).EQ.'FN1163') THEN
         IF(MODE.EQ.1) THEN
            DEN      = 1.D0
            DIF(1,1) = 1.D0
            DIF(2,2) = 1.D0
            DIF(3,3) = 1.D0
            DIF(1,2) = 0.0D0
            DIF(1,3) = 0.0D0
            DIF(2,1) = 0.0D0
            DIF(2,3) = 0.0D0
            DIF(3,1) = 0.0D0
            DIF(3,2) = 0.0D0
            go to 9999
         ENDIF
         IF(MODE.EQ.2) THEN
            Y     =   XXC(2,ICEL)
            F     =   SSC(ICEL,IM1)
            IF(TIME.LE.1.01D0) THEN
               S0 =   Y*6 + 2.D0
               SF =   0.D0
            ELSE IF(TIME.LE.2.01D0) THEN
               S0 =   0.D0
               SF =   9.D0
            ELSE
               S0 =   0.D0
               SF =   F
            ENDIF
            GO TO 9999
         ENDIF
      ENDIF
C----------------------------------------------------------------------
      IF(CHEAD(1:6).EQ.'FN1X77') THEN
         IF(MODE.EQ.1) THEN
C          +-------------------------------------------------+
C          |                                                 |
C          |    SPECIFY DEN AND DIF HERE FOR SPECIES-IMAS    |
C          |                                                 |
C          +-------------------------------------------------+
C
C           EXAMPLE FOR CONSTANT MATERIAL
C
            IF(IMAS.EQ.1) THEN
               DEN      = 1.D0
               DIF(1,1) = 10.D0
               DIF(2,2) = 0.2D0
               DIF(3,3) = 0.2D0
               DIF(1,2) = 0.0D0
               DIF(1,3) = 0.0D0
               DIF(2,1) = 0.0D0
               DIF(2,3) = 0.0D0
               DIF(3,1) = 0.0D0
               DIF(3,2) = 0.0D0
            ELSE IF(IMAS.EQ.2) THEN
               DEN      = 1.D0
               DIF(1,1) = 0.2D0
               DIF(2,2) = 1.2D0
               DIF(3,3) = 0.2D0
               DIF(1,2) = 0.0D0
               DIF(1,3) = 0.0D0
               DIF(2,1) = 0.0D0
               DIF(2,3) = 0.0D0
               DIF(3,1) = 0.0D0
               DIF(3,2) = 0.0D0
            ENDIF
            GO TO 9999
         ENDIF
C
         IF(MODE.EQ.2) THEN
C          +-------------------------------------------------+
C          |                                                 |
C          |    SPECIFY THE SOURCE IN TERMS OF S0 AND SF     |
C          |                                                 |
C          +-------------------------------------------------+
            S0     =   0.D0
            SF     =   0.D0
            IF(TIME.LT.1.D0) THEN
               S0  =   0.D0
               SF  =   0.001D0
            ENDIF
            GO TO 9999
         ENDIF
C
         IF(MODE.EQ.3) THEN
C          +-------------------------------------------------+
C          |                                                 |
C          |    SPECIFY BM = SUM{ BETM*(SI-SIREF) } HERE     |
C          |                                                 |
C          +-------------------------------------------------+
            BM      = 0.D0
            GO TO 9999
         ENDIF
      ENDIF
C-----------------------------------------------------------------------
      IF(CHEAD(1:6).EQ.'FN1X78') THEN
         IF(MODE.EQ.1) THEN
C          +-------------------------------------------------+
C          |                                                 |
C          |    SPECIFY DEN AND DIF HERE FOR SPECIES-IMAS    |
C          |                                                 |
C          +-------------------------------------------------+
C
C           EXAMPLE FOR V-DEPENDENT MATERIAL
C
            EPS      =  1.D-20
C
            DEN      =  118.6D0
            DIF0     =  0.01D0
            DIFVN    =  0.01D0
            DIFVT    =  0.003D0
C
            U        =  SSC(ICEL,1)
            V        =  SSC(ICEL,2)
            W        =  SSC(ICEL,3)
            F0FT     =  DIF0  + DIFVT
            FNFT     =  DIFVN - DIFVT
            U2       =  U*U + V*V + W*W
            U2       =  MAX(U2,EPS)
            UI2      =  FNFT/U2
            UU       =  U*UI2
            VV       =  V*UI2
            WW       =  W*UI2
            DIF(1,1) =  U*UU + F0FT
            DIF(2,2) =  V*VV + F0FT
            DIF(3,3) =  W*WW + F0FT
            DIF(1,2) =  U*VV
            DIF(1,3) =  U*WW
            DIF(2,3) =  V*WW
            DIF(3,1) =  DIF(1,3)
            DIF(3,2) =  DIF(2,3)
            DIF(2,1) =  DIF(1,2)
            GO TO 9999
         ENDIF
C
         IF(MODE.EQ.2) THEN
C          +-------------------------------------------------+
C          |                                                 |
C          |    SPECIFY THE SOURCE IN TERMS OF S0 AND SF     |
C          |                                                 |
C          +-------------------------------------------------+
            S0  = 0.D0
            SF  = 0.D0
            GO TO 9999
         ENDIF
C
         IF(MODE.EQ.3) THEN
C          +-------------------------------------------------+
C          |                                                 |
C          |    SPECIFY BM = SUM{ BETM*(SI-SIREF) } HERE     |
C          |                                                 |
C          +-------------------------------------------------+
            NMASS   = 2
            BM      = 0.D0
            DO 31 I = 1, NMASS
            J       = I + IM1 - 1
            SI      = 0.D0
            SIREF   = 0.D0
            BETMI   = 0.1D0
            IF(I.EQ.2) BETMI = 0.D0
            SI      = SSC(ICEL,J)
            BM      = BM + BETMI*(SI-SIREF)
 31         CONTINUE
            GO TO 9999
         ENDIF
      ENDIF
C-----------------------------------------------------------------------
 9999 CONTINUE
      RETURN
      END
      SUBROUTINE ESUSRS [DLLEXPORT] (CHEAD,MODE,IELG,IELM,XXX,
     .                               TIME,F,CJ)
C======================================================================
C
C     USER-SUPPLIED STATIC ELECTRIC SOURCE
C     PASSED IN:
C        CHEAD  =   THE STRING OF THE PROBLEM HEADING
C        MODE   =   2      STATIC ELECTRIC EQUATION
C                   1      STATIC CURRENT EQUATION
C        IELG   =   ELEMENT GROUP ID
C        IELM   =   ELEMENT ID
C        XXX(1) =   X-COODINATE
C        XXX(2) =   Y-COODINATE
C        XXX(3) =   Z-COODINATE
C        TIME   =   CURRENT TIME
C     PASSED OUT:
C        F      =   FREE CHARGE DENSITY (MODE=2)
C        CJ(1)  =   X-COMPONENT OF PRESCRIBED CURRENT DENSITY (MODE=1)
C        CJ(2)  =   Y-COMPONENT OF PRESCRIBED CURRENT DENSITY (MODE=1)
C        CJ(3)  =   Z-COMPONENT OF PRESCRIBED CURRENT DENSITY (MODE=1)
C
C======================================================================
C*I+++INCLUDE "implit.com"                        !         6
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      CHARACTER*80 CHEAD
      DIMENSION    XXX(*),CJ(*)
C----------------------------------------------------------------------
C
      IF(MODE.EQ.1) GO TO 2000
      IF(MODE.EQ.2) GO TO 1000
                    GO TO 9999
C
 1000 CONTINUE
C----------------------------------------------------------------------
C     WRITE YOUR OWN CODE HERE AND GO TO 9999 (CHARGE DENSITY)
C----------------------------------------------------------------------
C
      IF(CHEAD(1:3).EQ.'fn2'.AND.CHEAD(12:25).EQ.'CHARGE DENSITY') THEN
         F         =  -1.D0
         GO TO 9999
      ENDIF
      GO TO 9999
C
 2000 CONTINUE
C----------------------------------------------------------------------
C     WRITE YOUR OWN CODE HERE AND GO TO 9999 (J0)
C----------------------------------------------------------------------
C
      IF(CHEAD(1:4).EQ.'fn24'.AND.CHEAD(8:13).EQ.'CS, J0') THEN
         CJ(1)     =   XXX(1)*TIME
         CJ(2)     =   XXX(2)*TIME
         CJ(3)     =   XXX(3)*TIME
         GO TO 9999
      ENDIF
      GO TO 9999
C-----------------------------------------------------------------------
 9999 CONTINUE
      RETURN
      END
      SUBROUTINE CELUBC [DLLEXPORT] (CHEAD,NTRUN,TIM,  IBCL, NFB,
     .                  IRB,  IFC,  IFK,  IFN,  ITJ,
     .                  SSC,  XXC,  XXN,  IBF,  ABF,
     .                  BBF,  MCELL,KXFTN,MFACB)
C=======================================================================
C
C     USER-SUPPLIED BOUNDARY CONDITIONS FOR FCBI-C ELEMENTS
C
C     -----------------------------------------------------------------
C     INPUT:
C        CHEAD       =  PROBLEM HEADER (STRING)
C        NT          =  CURRENT SIMULATION TIME STEP
C        TIM         =  CURRENT SIMULATION TIME
C        IBCL        =  BOUNDARY CONDITION LABLE
C        NFB         =  NUMBER OF BOUNDARY FACES IN THIS CONDITION
C        IRB(*)      =  POINTER: BOUNDARY-FACE-INDEX TO FACE-INDEX
C        ITJ(*)      =  POINTER: VARIABLE-ID-->ACTIVE-VARIABLE-ID
C        IFK(*)      =  NUMBER OF NODES THAT FORM THE FACE
C        IFN(*,IFAC) =  NODAL INDICES ON FACE "IFAC"
C        IFC(1,IFAC) =  INDEX OF ELEMENT NEIGHBORING TO FACE "IFAC"
C        SSC(*,J)    =  CURRENT SOLUTION OF THE ACTIVE-VARIABLE "J"
C        XXC(3,*)    =  ELEMENT-CENTER COORDINATES
C        XXN(3,*)    =  NODAL COORDINATES
C        NOTE: VARIABLE ID=(1,2,3,...)<-->(U,V,W,P,T,X,Y,Z,K,E,M1,...)
C     -----------------------------------------------------------------
C     OUTPUT:
C        IBF(*,J)    =  CONDITION TYPE FOR VARIABLE "J" ON FACE
C        ABF(*,J)    =  A-VALUE        FOR VARIABLE "J" ON FACE
C        BBF(*,J)    =  B-VALUE        FOR VARIABLE "J" ON FACE
C           ( WHERE BOUNDARY-VALUE = A*(NEIGHBOR-INTERNAL-VALUE) + B )
C     ------------------------------------------------------------------
C
C=======================================================================
C*I+++INCLUDE "implit.com"                        !         6
      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
      CHARACTER   CHEAD*(*)
      DIMENSION   IRB(*),IFC(2,*),IFK(*),IFN(KXFTN,*),ITJ(*)
      DIMENSION   SSC(MCELL,*),XXC(3,*),XXN(3,*)
      DIMENSION   IBF(MFACB,*),ABF(MFACB,*),BBF(MFACB,*)
C
C-----------------------------------------------------------------------
      IF(NTRUN.LE.0   ) GO TO 9999
      IF(TIM  .LE.0.D0) GO TO 9999
      CL1    =   XXC(1,1)
C-----------------------------------------------------------------------
C
      IF(CHEAD(1:6).EQ.'FN1053') GO TO 1000
      IF(CHEAD(1:6).EQ.'FN1054') GO TO 2000
                                 GO TO 9999
C
C*****************************************************************
C*                                                               *
C*    PROBLEM FN1053: TEST THE SKEWED COORDINATE SYSTEM          *
C*                                                               *
C*****************************************************************
C
 1000 CONTINUE
C
C     CURRENT ACTIVE VARIBLES ARE (U,V,W,P)
C
      IU              =   ITJ(1)
      IV              =   ITJ(2)
      IW              =   ITJ(3)
      IP              =   ITJ(4)
C
      IF(IBCL.EQ.2) THEN
C
C        OUTLET CONDITION: ZERO PRESSURE
C
         DO 120 L     =   1, NFB
         IFAC         =   IRB(L)
         IBF(IFAC,IU) =   2
         ABF(IFAC,IU) =   1.D0
         BBF(IFAC,IU) =   0.D0
         IBF(IFAC,IV) =   2
         ABF(IFAC,IV) =   1.D0
         BBF(IFAC,IV) =   0.D0
         IBF(IFAC,IW) =   2
         ABF(IFAC,IW) =   1.D0
         BBF(IFAC,IW) =   0.D0
         IBF(IFAC,IP) =   4
         ABF(IFAC,IP) =   0.D0
         BBF(IFAC,IP) =   0.D0
 120     CONTINUE
         GO TO 9999
      ENDIF
C
      IF(IBCL.EQ.4) THEN
C
C        NOSLIP-WALL CONDITION: ZERO VELOCITY
C
         DO 140 L     =   1, NFB
         IFAC         =   IRB(L)
         ICL          =   IFC(1,IFAC)
         IBF(IFAC,IU) =   4
         ABF(IFAC,IU) =   0.D0
         BBF(IFAC,IU) =   0.D0
         IBF(IFAC,IV) =   4
         ABF(IFAC,IV) =   0.D0
         BBF(IFAC,IV) =   0.D0
         IBF(IFAC,IW) =   4
         ABF(IFAC,IW) =   0.D0
         BBF(IFAC,IW) =   0.D0
         IBF(IFAC,IP) =   2
         ABF(IFAC,IP) =   1.D0
         BBF(IFAC,IP) =   0.D0
 140     CONTINUE
         GO TO 9999
      ENDIF
C
C     DEFINITION OF THE NEW COORDINATE SYSTEM
C
      CL1             =   SQRT(3.D0)
      E1X             =   1.D0/CL1
      E1Y             =   1.D0/CL1
      E1Z             =   1.D0/CL1
C
      CL2             =   SQRT(6.D0)
      E2X             =   1.D0/CL2
      E2Y             = - 2.D0/CL2
      E2Z             =   1.D0/CL2
C
      E3X             =   E1Y*E2Z - E1Z*E2Y
      E3Y             =   E1Z*E2X - E1X*E2Z
      E3Z             =   E1X*E2Y - E1Y*E2X
C
      IF(IBCL.EQ.1) THEN
C
C        INLET CONDITION: PARABOLIC PROFILE
C
         DO 110 L     =   1, NFB
         IFAC         =   IRB(L)
         K            =   IFK(IFAC)
         XFAC         =   0.D0
         YFAC         =   0.D0
         ZFAC         =   0.D0
         DO I         =   1, K
            N         =   IFN(I,IFAC)
            XFAC      =   XFAC + XXN(1,N)
            YFAC      =   YFAC + XXN(2,N)
            ZFAC      =   ZFAC + XXN(3,N)
         ENDDO
         ZSKW         = ( E3X*XFAC + E3Y*YFAC + E3Z*ZFAC )/K
         VEL          =   ZSKW*(1.D0-ZSKW)*4
         U            =   E2X*VEL
         V            =   E2Y*VEL
         W            =   E2Z*VEL
         IBF(IFAC,IU) =   1
         ABF(IFAC,IU) =   0.D0
         BBF(IFAC,IU) =   U
         IBF(IFAC,IV) =   1
         ABF(IFAC,IV) =   0.D0
         BBF(IFAC,IV) =   V
         IBF(IFAC,IW) =   1
         ABF(IFAC,IW) =   0.D0
         BBF(IFAC,IW) =   W
         IBF(IFAC,IP) =   2
         ABF(IFAC,IP) =   1.D0
         BBF(IFAC,IP) =   0.D0
 110     CONTINUE
         GO TO 9999
      ENDIF
      IF(IBCL.EQ.3) THEN
C
C        SLIP-WALL CONDITION: ZERO NORMAL-VELOCITY
C
         DO 130 L     =   1, NFB
         IFAC         =   IRB(L)
         ICL          =   IFC(1,IFAC)
         UL           =   SSC(ICL,IU)
         VL           =   SSC(ICL,IV)
         WL           =   SSC(ICL,IW)
         V2L          =   E2X*UL + E2Y*VL + E2Z*WL
         V3L          =   E3X*UL + E3Y*VL + E3Z*WL
         U            =   E2X*V2L + E3X*V3L
         V            =   E2Y*V2L + E3Y*V3L
         W            =   E2Z*V2L + E3Z*V3L
         AU           =   E2X*E2X + E3X*E3X
         AV           =   E2Y*E2Y + E3Y*E3Y
         AW           =   E2Z*E2Z + E3Z*E3Z
         IBF(IFAC,IU) =   1
         ABF(IFAC,IU) =   AU
         BBF(IFAC,IU) =   U - AU*UL
         IBF(IFAC,IV) =   1
         ABF(IFAC,IV) =   AV
         BBF(IFAC,IV) =   V - AV*VL
         IBF(IFAC,IW) =   1
         ABF(IFAC,IW) =   AW
         BBF(IFAC,IW) =   W - AW*WL
         IBF(IFAC,IP) =   2
         ABF(IFAC,IP) =   1.D0
         BBF(IFAC,IP) =   0.D0
 130     CONTINUE
         GO TO 9999
      ENDIF
      GO TO 9999
C
C*****************************************************************
C*                                                               *
C*    PROBLEM FN1054: SAME AS FN1053 EXCELPT NO SKEW SYSTEM      *
C*                                                               *
C*****************************************************************
C
 2000 CONTINUE
C
C     CURRENT ACTIVE VARIBLES ARE (U,V,W,P)
C
      IU              =   ITJ(1)
      IV              =   ITJ(2)
      IW              =   ITJ(3)
      IP              =   ITJ(4)
C
      IF(IBCL.EQ.2) THEN
C
C        OUTLET CONDITION: ZERO PRESSURE
C
         DO 220 L     =   1, NFB
         IFAC         =   IRB(L)
         IBF(IFAC,IU) =   2
         ABF(IFAC,IU) =   1.D0
         BBF(IFAC,IU) =   0.D0
         IBF(IFAC,IV) =   2
         ABF(IFAC,IV) =   1.D0
         BBF(IFAC,IV) =   0.D0
         IBF(IFAC,IW) =   2
         ABF(IFAC,IW) =   1.D0
         BBF(IFAC,IW) =   0.D0
         IBF(IFAC,IP) =   4
         ABF(IFAC,IP) =   0.D0
         BBF(IFAC,IP) =   0.D0
 220     CONTINUE
         GO TO 9999
      ENDIF
C
      IF(IBCL.EQ.4) THEN
C
C        NOSLIP-WALL CONDITION: ZERO VELOCITY
C
         DO 240 L     =   1, NFB
         IFAC         =   IRB(L)
         ICL          =   IFC(1,IFAC)
         IBF(IFAC,IU) =   4
         ABF(IFAC,IU) =   0.D0
         BBF(IFAC,IU) =   0.D0
         IBF(IFAC,IV) =   4
         ABF(IFAC,IV) =   0.D0
         BBF(IFAC,IV) =   0.D0
         IBF(IFAC,IW) =   4
         ABF(IFAC,IW) =   0.D0
         BBF(IFAC,IW) =   0.D0
         IBF(IFAC,IP) =   2
         ABF(IFAC,IP) =   1.D0
         BBF(IFAC,IP) =   0.D0
 240     CONTINUE
         GO TO 9999
      ENDIF
C
      IF(IBCL.EQ.1) THEN
C
C        INLET CONDITION: PARABOLIC PROFILE
C
         DO 210 L     =   1, NFB
         IFAC         =   IRB(L)
         K            =   IFK(IFAC)
         ZFAC         =   0.D0
         DO I         =   1, K
            N         =   IFN(I,IFAC)
            ZFAC      =   ZFAC + XXN(3,N)
         ENDDO
         ZFAC         =   ZFAC/K
         V            =   ZFAC*(1.D0-ZFAC)*4
         IBF(IFAC,IU) =   1
         ABF(IFAC,IU) =   0.D0
         BBF(IFAC,IU) =   0.D0
         IBF(IFAC,IV) =   1
         ABF(IFAC,IV) =   0.D0
         BBF(IFAC,IV) =   V
         IBF(IFAC,IW) =   1
         ABF(IFAC,IW) =   0.D0
         BBF(IFAC,IW) =   0.D0
         IBF(IFAC,IP) =   2
         ABF(IFAC,IP) =   1.D0
         BBF(IFAC,IP) =   0.D0
 210     CONTINUE
         GO TO 9999
      ENDIF
      IF(IBCL.EQ.3) THEN
C
C        SLIP-WALL CONDITION: ZERO NORMAL-VELOCITY
C
         DO 230 L     =   1, NFB
         IFAC         =   IRB(L)
         IBF(IFAC,IU) =   1
         ABF(IFAC,IU) =   0.D0
         BBF(IFAC,IU) =   0.D0
         IBF(IFAC,IV) =   2
         ABF(IFAC,IV) =   1.D0
         BBF(IFAC,IV) =   0.D0
         IBF(IFAC,IW) =   2
         ABF(IFAC,IW) =   1.D0
         BBF(IFAC,IW) =   0.D0
         IBF(IFAC,IP) =   2
         ABF(IFAC,IP) =   1.D0
         BBF(IFAC,IP) =   0.D0
 230     CONTINUE
         GO TO 9999
      ENDIF
      GO TO 9999
C-----------------------------------------------------------------------
 9999 CONTINUE
      RETURN
C*FILE END
      END
