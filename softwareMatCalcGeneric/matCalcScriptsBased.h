
/*! \file matCalcScriptsBased.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef matCalcScriptsBased_h__
#define matCalcScriptsBased_h__
/*	include *********************************************************/

#include <string>
#include <boost/shared_ptr.hpp>
#include "../models/dataStructures.h"
#include "../models/modelsScheme.h"
#include "../common/common.h"
#include "../softwareMatCalc/matcalcCore.h"

/*	using ***********************************************************/

namespace mop = am3::model::patterns;
namespace co  = am3::common;
namespace mpr = am3::model::properties;
namespace mo = am3::model;

using std::string;
using boost::shared_ptr;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace matcalc
		{

      class MatCalcScriptProvider
      {
      public:
        std::string Workspace() const { return ".\\"; }
        std::string ProgramPath() const { return ".\\"; }
        std::string LicensePath() const { return ".\\license.dat"; }
        //         std::string ProgramPath() const { return "c:\\Program Files\\MatCalc5.52\\"; }
        //         std::string LicensePath() const { return "c:\\Program Files\\MatCalc5.52\\license.dat"; }
        std::string LicenseFile() const { return ".\\license.dat"; }
//         std::string InitScript() const { return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\esaform2015init.mcs"; }
//         std::string InitModelScript() const { return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\esaform2015initModel.mcs"; }
//         std::string FirstStepScript() const { return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\esaform2015first.mcs"; }
//         std::string NextStepScript() const { return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\esaform2015next.mcs"; }
//         std::string LastStepScript() const { return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\esaform2015next.mcs"; }

        std::string InitScript() const { return "init.mcs"; }
        std::string InitModelScript() const { return "initModel.mcs"; }
        std::string FirstStepScript() const { return "firstStep.mcs"; }
        std::string NextStepScript() const { return "nextStep.mcs"; }
        std::string LastStepScript() const { return "nextStep.mcs"; }

        std::string ProcessCodeName() const { return "DefaultProcess"; }
      };

      //******************************************************************************************************************************************************************

//       class TemperatureSource
//       {
//       public:
//         mpr::Temperature GetTemperature(mpt::Point<mpr::Coordinates2D> * point){ return temperature_; };
//         void SetTemperature(const mpr::Temperature& temperature){ temperature_(temperature); };
//       private:
//         mpr::Temperature temperature_;
//       };

      //******************************************************************************************************************************************************************
      template<typename VARIABLES_SOURCE>
      class MatCalcScriptsBased: public mo::InitializableModel 
      {
      public:
        MatCalcScriptsBased(const MatCalcScriptProvider& scripts, boost::shared_ptr<VARIABLES_SOURCE> varSource): scripts_(scripts), varSource_(varSource){};

        void SetProcessId(const std::string& processId);
        std::vector<double> GetResults();

      protected:
        
        void Init(mpt::Point<mpr::Coordinates2D> * point) override;
        void RunFirstStep(mpt::Point<mpr::Coordinates2D> * point) override;
        void RunNextStep(mpt::Point<mpr::Coordinates2D> * point) override;


        const MatCalcScriptProvider& scripts_;
        boost::shared_ptr<VARIABLES_SOURCE> varSource_;
        MatCalcCore mcCore_;
        std::string processId_;
        };

      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////

      template<typename VARIABLES_SOURCE>
      std::vector<double> am3::software::matcalc::MatCalcScriptsBased<VARIABLES_SOURCE>::GetResults()
      {
        return mcCore_.GetResults ();
      }


      template<typename VARIABLES_SOURCE>
      void am3::software::matcalc::MatCalcScriptsBased<VARIABLES_SOURCE>::SetProcessId(const std::string& processId)
      {
        processId_ = processId;
      }

      
      template < typename VARIABLES_SOURCE >
      void am3::software::matcalc::MatCalcScriptsBased<VARIABLES_SOURCE>::Init(mpt::Point<mpr::Coordinates2D> * point)
      {
        mpr::Time time = point->GetTime();
        mpr::Temperature temperature = varSource_->GetVariable(*point);
        mcCore_.Init(scripts_.ProgramPath(), scripts_.LicensePath(), scripts_.InitScript(), processId_);
        mcCore_.InitModel(scripts_.InitModelScript(), temperature, time);
      }


      template < typename VARIABLES_SOURCE >
      void am3::software::matcalc::MatCalcScriptsBased<VARIABLES_SOURCE>::RunFirstStep(mpt::Point<mpr::Coordinates2D> * point)
      {
        mpr::Time time = point->GetTime();
        mpr::Temperature temperature = varSource_->GetVariable(*point);
        if (varSource_->AnyAdditionalVariables())
        {
          lo::debugLog("Setting variables");
          mcCore_.SetVariables(varSource_->GetAdditionalVariables(*point));
        }
        mcCore_.RunStep(temperature, time, scripts_.FirstStepScript());
      }



      template < typename VARIABLES_SOURCE >
      void am3::software::matcalc::MatCalcScriptsBased<VARIABLES_SOURCE>::RunNextStep(mpt::Point<mpr::Coordinates2D> * point)
      {
        mpr::Time time = point->GetTime();
        mpr::Temperature temperature = varSource_->GetVariable(*point);
        lo::debugLog("Setting variables");
        mcCore_.SetVariables(varSource_->GetAdditionalVariables(*point));
        mcCore_.RunStep(temperature, time, scripts_.NextStepScript());
      }


		} //matcalc
	} //software
} //am3
#endif // matCalcScriptsBased_h__
