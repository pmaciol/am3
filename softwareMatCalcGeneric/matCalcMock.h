
/*! \file matCalcMock.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwarematCalcGeneric_matCalcMock_h__
#define am3softwarematCalcGeneric_matCalcMock_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace matcalc
		{
			class MatCalcMock
			{
			public:
        MatCalcMock() : initialized_(false), computedForTime(0.0){};
        
        void InitIfNeeded(double time, double temperature);
        void CalculateStepIfNeeded(double time, double temperature);
        void Finalize();
        double GetVariableMock(unsigned id, double time);
        double GetVariableMock(std::string name, double time);

      protected:
        bool initialized_;
        double computedForTime;
			};

		} //matcalc
	} //software
} //am3
#endif // am3softwarematCalcGeneric_matCalcMock_h__
