
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef softwareMatCalcGenericContracts_h__
#define softwareMatCalcGenericContracts_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/macrosSimplifiedModels.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace moc = am3::model::contract;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace matcalc
    {



    }
  }
}
#endif // softwareMatCalcGenericContracts_h__
