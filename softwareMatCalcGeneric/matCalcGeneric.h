
/*! \file matCalcGeneric.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef matCalcGeneric_h__
#define matCalcGeneric_h__
/*	include *********************************************************/

#include <limits>
//#include <boost/scoped_ptr.hpp>
#include "../models/dataStructures.h"
#include "./contracts.h"
#include "../models/point.h"
#include "../models/models.h"
#include "../softwareMatCalc/matcalcController.h"
#include "../softwareMatCalc/contracts.h"
#include "../modelPrecipitation/contracts.h"

#include <fstream>
/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace mpc = am3::model::precipitations;
namespace mo = am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace matcalc
    {
    class ModelInstance
    {
    public:
      virtual void InitWithStartingValues(double time, double temperature) = 0;
      virtual void RunNextStep(double time, double temperature) = 0;
      virtual double GetVariableValue(std::string variableName, double time) = 0;
      virtual double GetVariableValue(unsigned variableId, double time) = 0;
      virtual void Finalize() = 0;
    };

//       class MatCalcScriptProvider
//       {
//       public:
//         std::string Workspace() const {return "d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\";}
//         std::string LicensePath() const {return "d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\";}
//         std::string LicenseFile() const {return "license.dat";}
//         std::string MainScript() const {return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\test_on_heat_treatment_9Cr_steel_batch.mcs";}
//       };

//       class GenericMatcalcModel
//         : public mpc::contract::PrecipitationsMeanRadiusContract
//         , public mo::InputTypelist < INPUT >
//      //   , public mo::Inputs<MatCalcGenericInputContract>
//       {
// 
//       public:
//         GenericMatcalcModel(const MatCalcScriptProvider& scripts);
// 
//         mpr::PrecipitationsMeanRadius GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::PrecipitationsMeanRadius *const );
//         mpr::PrecipitationsAmount GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::PrecipitationsAmount *const );
//       protected:
//         mpr::PrecipitationsMeanRadius Init(mpr::Temperature initTemperature , mpr::Time initTime);
//         mpr::PrecipitationsMeanRadius RunFirstStep(mpr::Temperature initTemperature, mpr::Temperature afterStepTemperature, mpr::Time initTime, mpr::Time afterStepTime);
//         mpr::PrecipitationsMeanRadius RunNextStep(mpr::Temperature initTemperature, mpr::Temperature afterStepTemperature, mpr::Time initTime, mpr::Time afterStepTime);
//         mpr::Temperature GetPreviousTemperature() const {return lastTemperature_;};
//         void SetEndTemperature(const mpr::Temperature temp){lastTemperature_ = temp;};
// 
//         const MatCalcScriptProvider& scripts_;
//         boost::shared_ptr<MatcalcController> matcalc_;
//         bool firstStepRan_,initialized_;
//         mpr::Temperature lastTemperature_;
//         mpr::Time lastTime_;
//         mpr::Time GetLastTime() const { return lastTime_; }
//         void SetLastTime(mpr::Time val) { lastTime_ = val; }
// 
//       
//       };
// 
//       mpr::PrecipitationsAmount GenericMatcalcModel::GetState( mpt::Point<mpr::Coordinates2D> * point,const mpr::PrecipitationsAmount *const )
//       {
//         mpr::PrecipitationsAmount toRet;
//         toRet(std::numeric_limits<double>::signaling_NaN());
//         return toRet;
//       };
// 
//       mpr::PrecipitationsMeanRadius GenericMatcalcModel::GetState( mpt::Point<mpr::Coordinates2D> * point,const mpr::PrecipitationsMeanRadius *const )
//       {
//         mpr::PrecipitationsMeanRadius toRet;
//         mpr::Time time;
//         //time = Get<mpr::Time,mpr::Coordinates2D>::InPoint(point);
//         time = bus_->GetFromDatabus<mpr::Time>(point);
//         mpr::Temperature temperature;
//         temperature = Get<mpr::Temperature,mpr::Coordinates2D>::InPoint(point);
//         if (initialized_)
//         {
//           if (firstStepRan_)
//           {
//             if ((GetLastTime()>time))
//             {
//               toRet(RunNextStep(GetPreviousTemperature(),temperature,GetLastTime(),time));
//               SetEndTemperature(temperature);
//               SetLastTime(time);
//             }
//             else
//             {
//               lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"MatCalc should not be fired more than once for the same time!");
//             }
//           } 
//           else
//           {
//               toRet(RunFirstStep(GetPreviousTemperature(),temperature,0,time));
//               firstStepRan_ = true;
//           }
//         }
//         else
//         {
//           toRet(Init(temperature,time));
//           initialized_ = true;
//         }
//         return toRet;
//       }
// 
//       GenericMatcalcModel::GenericMatcalcModel( const MatCalcScriptProvider& scripts ) : scripts_(scripts), 
//         matcalc_(new MatcalcController(scripts.Workspace(),scripts.LicensePath(),scripts.MainScript())),
//         firstStepRan_(false), initialized_(false)
//       {
//         
//       }
// 
//       mpr::PrecipitationsMeanRadius GenericMatcalcModel::RunFirstStep( mpr::Temperature initTemperature, mpr::Temperature afterStepTemperature, mpr::Time initTime, mpr::Time afterStepTime )
//       {
//         mpr::PrecipitationsMeanRadius toRet;
//         boost::shared_ptr<PrecipitationStructure> returnedFromMatCalc;
//         mpr::Time stepLength;
//         stepLength(afterStepTime() - initTime());
//         returnedFromMatCalc = matcalc_->FirstStep(stepLength,initTemperature,afterStepTemperature);
//         toRet(returnedFromMatCalc->precipitationsMeanRadius);
//         return toRet;
//       }
// 
//       mpr::PrecipitationsMeanRadius GenericMatcalcModel::RunNextStep( mpr::Temperature startStepTemperature, mpr::Temperature afterStepTemperature, mpr::Time initTime,mpr::Time afterStepTime )
//       {
//         mpr::PrecipitationsMeanRadius toRet;
//         boost::shared_ptr<PrecipitationStructure> returnedFromMatCalc;
//         mpr::Time stepLength;
//         stepLength(afterStepTime() - initTime());
//         returnedFromMatCalc = matcalc_->NextStep(stepLength,startStepTemperature,afterStepTemperature);
//         toRet(returnedFromMatCalc->precipitationsMeanRadius);
//         return toRet;
//       }
// 
//       mpr::PrecipitationsMeanRadius GenericMatcalcModel::Init( mpr::Temperature initTemperature , mpr::Time initTime)
//       {
//         mpr::PrecipitationsMeanRadius toRet;
//         boost::shared_ptr<PrecipitationStructure> returnedFromMatCalc;
//         lastTemperature_ = initTemperature;
//         lastTime_ = initTime;
//         returnedFromMatCalc = matcalc_->Init(initTemperature);
//         toRet(returnedFromMatCalc->precipitationsMeanRadius);
//         return toRet;
//       }

    }
  }
}
#endif // matCalcGeneric_h__
