
/*! \file matCalcResults.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwarematCalcGeneric_matCalcResults_h__
#define am3softwarematCalcGeneric_matCalcResults_h__
/*	include *********************************************************/

#include <string>
#include "../softwareMatCalc/contracts.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace matcalc
    {
        template <typename VARIABLE_NAME>
        std::string GetName();

        template <> std::string GetName<Alcrfemnsi_a_p0_Radius>() { return "R_MEAN$ALCRFEMNSI_A_P0"; }
        template <> std::string GetName<Alcrfemnsi_a_p0_VolumeFraction>() { return "F_PREC$ALCRFEMNSI_A_P0"; }
        template <> std::string GetName<TestValue>() { return "testvalue"; }

        template <typename VARIABLE_NAME>
        VARIABLE_NAME GetVariable()
        {
          double val;
          //MCC_GetCalcVariable("R_MEAN$ALCRFEMNSI_A_P0", &val, MC_FALSE, MC_TRUE);
          std::string stringName = GetName<VARIABLE_NAME>();
          char varName[100];
          stringName.copy(varName, 98);
          varName[stringName.length()] = '\0';
          MCC_GetVariableValue(varName, val);
          VARIABLE_NAME toRet;
          toRet(val);
          return toRet;
      };
    }
  }
}
#endif // am3softwarematCalcGeneric_matCalcResults_h__
