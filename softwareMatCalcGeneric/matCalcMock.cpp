
/*! \file matCalcMock.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include <lexical_cast.hpp>
#include <limits>
#include "matCalcMock.h"
#include "../models/dataStructures.h"
#include "../logger/logger.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace lo = am3::logger;

/*	extern **********************************************************/



/*	classes *********************************************************/


void am3::software::matcalc::MatCalcMock::InitIfNeeded(double time, double temperature)
{
  if (! initialized_)
  {
    initialized_ = true;
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, "MatCalcMock initialized");
  }
  else
  {
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, "MatCalcMock was initialized before");
  }
}

void am3::software::matcalc::MatCalcMock::CalculateStepIfNeeded(double time, double temperature)
{
  if (computedForTime == time)
  {
    std::string str;
    str = "MatCalcMock was computed before, time " + boost::lexical_cast<std::string>(time);
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, str);
  } 
  else
  {
    std::string str;
    str = "MatCalcMock computed, time " + boost::lexical_cast<std::string>(time);
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, str);
  }
}

void am3::software::matcalc::MatCalcMock::Finalize()
{
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, "MatCalcMock finalized");
}

double am3::software::matcalc::MatCalcMock::GetVariableMock(unsigned id, double time)
{
  if (computedForTime == time)
  {
    std::string str; 
    str = "Variable " + boost::lexical_cast<std::string>(id)+" properly computed";
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, str);
    return id;
  } 
  else
  {
    std::string str;
    str = "Variable " + boost::lexical_cast<std::string>(id)+" was not computed";
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, str);
    return std::numeric_limits<double>::signaling_NaN();
  }
}

double am3::software::matcalc::MatCalcMock::GetVariableMock(std::string name, double time)
{
  if (computedForTime == time)
  {
    std::string str;
    str = "Variable " + name + " properly computed";
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, str);
    return name.length();
  }
  else
  {
    std::string str;
    str = "Variable " + name + " was not computed";
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, str);
    return std::numeric_limits<double>::signaling_NaN();
  }
}
