/*! \file dllExport.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

//#ifdef BUILD_STANDALONE_CASE

#include <exception>
#include <string>
#include "dllExport.h"
#include "../softwareAbaqus/usr_uhard.h"
#include "caseAbaqusTestDatabus.h"
#include "../logger/logger.h"


/*	using ***********************************************************/

namespace sab = am3::software::abaqus;
namespace lo = am3::logger;

namespace cta = am3::cases::taq;

/*	classes *********************************************************/
int abaqus_temperature(const double * temperature, const int * meshId, const int * elementId)
{
	lo::debugLog("ABAQUS_TEMPERATURE, elementId" + std::to_string(*elementId));
	sab::AbaqusHandlerFacadeMtr* facade;
	facade = cta::GetAbaqusHandlerFacadeMtr();
	facade->SetTemperature(temperature, *meshId, *elementId);
	return 0;
}

int abaqus_eff_str_rate(const double * effStrainRate, const int * meshId, const int * elementId)
{
	lo::debugLog("ABAQUS_EFF_STR_RATE, elementId" + std::to_string(*elementId));
	sab::AbaqusHandlerFacadeMtr* facade;
	facade = cta::GetAbaqusHandlerFacadeMtr();
	facade->SetEffectiveStrain(effStrainRate, *meshId, *elementId);
	return 0;
}
int abaqus_eff_str(const double * effStrain, const int * meshId, const int * elementId)
{
	lo::debugLog("ABAQUS_EFF_STR, elementId" + std::to_string(*elementId));
	sab::AbaqusHandlerFacadeMtr* facade;
	facade = cta::GetAbaqusHandlerFacadeMtr();
	facade->SetEffectiveStrain(effStrain, *meshId, *elementId);
	return 0;
}


int abaqus_all_stress_get(double * yieldStress, double * yieldStressDstrain, double * yieldStressDstrainRate, const int * meshId, const int * elementId)
{
  lo::debugLog("ABAQUS_ALL_STRESS_GET, elementId" + std::to_string(*elementId));
  sab::AbaqusHandlerFacadeMtr* facade;
	facade = cta::GetAbaqusHandlerFacadeMtr();
	*yieldStress = facade->GetYieldStress(*meshId, *elementId);
	*yieldStressDstrain = facade->GetYieldStressDStrain(*meshId, *elementId);
	*yieldStressDstrainRate = facade->GetYieldStressDStrainRate(*meshId, *elementId);
	return 0;
}

int abaqus_element(const int * meshId, const int * elementId)
{
	lo::debugLog("ABAQUS_ELEMENT, elementId" + std::to_string(*elementId));
	sab::AbaqusHandlerFacadeElm* facade;
	facade = cta::GetAbaqusHandlerFacadeElement();
	facade->SetElementNumber(*meshId, *elementId);
	std::cerr << "A tu byl blad" << std::endl;
	return 0;
}

int abaqus_time(const double * time, const double * timeStep, const int * meshId, const int * elementId)
{
	lo::debugLog("ABAQUS_TIME, elementId" + std::to_string(*elementId));
	sab::AbaqusHandlerFacadeTime* facade;
	facade = cta::GetAbaqusHandlerFacadeTime();
	facade->SetStepLength(timeStep, *meshId, *elementId);
	facade->SetTime(time, *meshId, *elementId);
	return 0;
}

//#endif