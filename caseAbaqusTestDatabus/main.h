
/*! \file main.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseAbaqusTestDatabus_main_h__
#define am3caseAbaqusTestDatabus_main_h__
/*	include *********************************************************/

#include "../models/models.h"
#include "../softwareAbaqus/usr_uhard.h"
//#include "caseAbaqusTestDatabus/"
#include "databus.h"

/*	using ***********************************************************/

namespace sab = am3::software::abaqus;

/*	extern **********************************************************/


/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace taq
		{
			// This case is used as a dll library. Init method of Main class works like a "main" procedure in single executable solution
			// Implementation in cpp file
			// private members are necessary to keep all models 'alive' during computations. If sub-models are not bind to databus, member shared_ptr must be kept for them
			class Main : public am3::model::InitializableMainModel
			{
			public:
				Main();
				bool initialized_ = false;
				void Init();

				virtual bool IsInitialized() const;
        
				boost::shared_ptr<sab::AbaqusHandlerFacadeMtr> GetAbaqusCaseMtr();
				boost::shared_ptr<sab::AbaqusHandlerFacadeElm> GetAbaqusCaseElement();
				boost::shared_ptr<sab::AbaqusHandlerFacadeTime> GetAbaqusCaseTime();
			private:
				boost::shared_ptr<CaseAbaqusTestDatabus> mainDatabus_;
				boost::shared_ptr<AbaqusCaseMtr> abaqusCaseMtr_;
			};
		} //taq
	} //cases
} //am3


#endif // am3caseAbaqusTest_main_h__
