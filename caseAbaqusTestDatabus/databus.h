/*! \file databus.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseAbaqusTest_databus_h__
#define am3caseAbaqusTest_databus_h__
/*	include *********************************************************/

#include "../common/typelists.h"
//#include "modelsAdapters.h"
#include "../databus/databusTypelists.h"
#include "../modelStress/stressConst.h"
#include "../modelStress/stressProxy.h"
#include "caseAbaqusTestDatabus.h"




/*	using ***********************************************************/

namespace db = am3::databus;
namespace mst = am3::model::stress;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace taq
		{



      // List of 'Databus' classes
			typedef TEMPLATELIST_3(
				mst::StressProxy,
				mst::StressConstantWithDerivatives,
		        MacroModelTemperature
		        ) caseAbaqusTestList;
		
		      // Databus
			typedef db::DatabusTypelists<caseAbaqusTestList> CaseAbaqusTestDatabus;
				} //taq
	} //cases
} //am3
#endif // am3caseAbaqusTest_databus_h__
