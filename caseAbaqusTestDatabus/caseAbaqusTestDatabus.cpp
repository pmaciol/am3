
/*! \file caseAbaqusTestDatabus.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "caseAbaqusTestDatabus.h"
#include "main.h"

/*	using ***********************************************************/

extern am3::cases::taq::Main*  mainInstance;

/*	extern **********************************************************/



/*	classes *********************************************************/


sab::AbaqusHandlerFacadeMtr* am3::cases::taq::GetAbaqusHandlerFacadeMtr()
{
	if (mainInstance == nullptr) mainInstance = new am3::cases::taq::Main;
	return mainInstance->GetAbaqusCaseMtr().get();
}

sab::AbaqusHandlerFacadeTime* am3::cases::taq::GetAbaqusHandlerFacadeTime()
{
	if (mainInstance == nullptr) mainInstance = new am3::cases::taq::Main;
	return mainInstance->GetAbaqusCaseTime().get();
}

sab::AbaqusHandlerFacadeElm* am3::cases::taq::GetAbaqusHandlerFacadeElement()
{
	if (mainInstance == nullptr) mainInstance = new am3::cases::taq::Main;
	return mainInstance->GetAbaqusCaseElement().get();
}
