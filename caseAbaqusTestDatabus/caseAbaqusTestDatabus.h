
/*! \file caseAbaqusTestDatabus.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseAbaqusTestDatabus_caseAbaqusTestDatabus_h__
#define am3caseAbaqusTestDatabus_caseAbaqusTestDatabus_h__
/*	include *********************************************************/

#include "../softwareAbaqus/usr_uhard.h"
#include "../modelTemplates/modelsTemplates.h"
#include "../modelTemplates/macroState.h"
#include "../softwareAbaqus/policyOneStepOneElement.h"

/*	using ***********************************************************/

namespace sab = am3::software::abaqus;
namespace mot = am3::model::templates;


/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace taq
		{
			sab::AbaqusHandlerFacadeMtr* GetAbaqusHandlerFacadeMtr();
			sab::AbaqusHandlerFacadeTime* GetAbaqusHandlerFacadeTime();
			sab::AbaqusHandlerFacadeElm* GetAbaqusHandlerFacadeElement();

			typedef sab::OneStepOneElementNoElementCheckStresses AbaqusCaseMtr;

			template<typename INPUT>
			class MacroModelTemperature
				: public mot::MacroModelTemperature < sab::OneStepOneElementNoElementCheckStresses, INPUT >
			{
			public:
				//inherited constructor - just when changed to VS2015
				MacroModelTemperature(boost::shared_ptr<sab::OneStepOneElementNoElementCheckStresses> macroStateHandler) : mot::MacroModelTemperature < sab::OneStepOneElementNoElementCheckStresses, INPUT >(macroStateHandler){};
			};

// 			template<typename INPUT>
// 			class IvManager
// 				: public mot::OneStepOneElementIvNew
// 				, public mo::InputTypelist < INPUT >
// 			{
// 			public:
// 				typedef StorageContract Inputs;
// 				typedef typename mot::EmptyContract2D Contract;
// 
// 			protected:
// 				boost::shared_ptr<mot::OneStepOneElementIvPresent> ivStorage_;
// 				mpt::Point<mpr::Coordinates2D>* point_ = nullptr;
// 				bool CheckPoint(mpt::Point<mpr::Coordinates2D>* point);
// 				void SetPoint(mpt::Point<mpr::Coordinates2D>* point);
// 
// 				virtual std::vector<double> GetInternalVariablesNew(mpt::Point<mpr::Coordinates2D> * point) override;
// 			};
			




		} //taq
	} //cases
} //am3
#endif // am3caseAbaqusTestDatabus_caseAbaqusTestDatabus_h__
