/*! \file dllExport.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseMatCalcMPI_dllExport_h__
#define am3caseMatCalcMPI_dllExport_h__
/*	include *********************************************************/
/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/

#ifdef BUILD_STANDALONE_CASE
  #define EXPORT __declspec(dllexport) 
#else
  #define EXPORT 
#endif

extern "C" {
	EXPORT int abaqus_element(const int * meshId, const int * elementId);
	EXPORT int abaqus_time(const double * time, const double * timeStep, const int * meshId, const int * elementId);
	EXPORT int abaqus_temperature(const double * temperature, const int * meshId, const int * elementId);
	EXPORT int abaqus_eff_str_rate(const double * effStrainRate, const int * meshId, const int * elementId);
	EXPORT int abaqus_eff_str(const double * effStrain, const int * meshId, const int * elementId);
	EXPORT int abaqus_all_stress_get(double * yieldStress, double * yieldStressDstrain, double * yieldStressDstrainRate, const int * meshId, const int * elementId);
}

extern "C" {
	__declspec(dllexport) int abaqusTest(const float* in, float* out);
}

extern "C" {
	__declspec(dllexport) int call_lambda(const float* E, const float* ANU, float* alambda);
}
#endif // am3caseMatCalcMPI_dllExport_h__
