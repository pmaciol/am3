
/*! \file main.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       The 'business logic' of whole case.
 * \details   	In the Main::Init method, there is a definition how submodels comuunicate between themselves and to external software
 * 					  	Also use of internal variables is described
*********************************************************************/
/*	include *********************************************************/

#include "main.h"
#include "caseAbaqusTestDatabus.h"

// #include "kbs.h"
// #include "matCalc.h"
#include "../models/macrosSimplifiedModels.h"


/*	using ***********************************************************/

namespace taq = am3::cases::taq;

/*	extern **********************************************************/

/*	classes *********************************************************/

taq::Main*  mainInstance = nullptr;

namespace am3
{
	namespace cases
	{
		namespace taq
		{
			void Main::Init()
			{
//        DebugBreak();
				if (!IsInitialized())
				{

					DATABUS_INSTANCE(mainDatabus, CaseAbaqusTestDatabus); //Initialization of the main Databus
					mainDatabus_ = mainDatabus;

					// StressProxy; gets stress and stress's derivatives to macroscale software (FEM)
					MODEL_INSTANCE_REDESIGN(mst::StressProxy, StressProxyModel, CaseAbaqusTestDatabus, stressReturn, mainDatabus);

					// Abaqus handler
					boost::shared_ptr<sab::OneStepOneElementNoElementCheckStresses> stateProvider(new sab::OneStepOneElementNoElementCheckStresses(stressReturn));

					boost::shared_ptr<AbaqusCaseMtr> abaqusCaseMtr(new AbaqusCaseMtr(stressReturn));
					abaqusCaseMtr_ = abaqusCaseMtr;


					MODEL_INSTANCE_WITH_PARAM(mst::StressConstantWithDerivatives, StressModel, CaseAbaqusTestDatabus, stress, mainDatabus, 1e5);
					MODEL_INSTANCE_WITH_PARAM(MacroModelTemperature, TemperatureModel, CaseAbaqusTestDatabus, temperature, mainDatabus, abaqusCaseMtr_);

// 					//******************************************************************************************************************************************************************
// 					// Defining the models from the top to bottom
// 					//******************************************************************************************************************************************************************
// 
// 					// Iv's dependent submodels with iv's interpretation plug-ins to Deform and Databus.
// 					// IvManager gets new IV values from Databus. 
// 					// It works on the upper part of Databus, but is passed to ivPRovider, which is necessary for each iv-dependent sub-model
// 					MODEL_INSTANCE_REDESIGN(IvManager, IvManagerInstance, caseAbaqusTestToUSPDatabus, ivManager, mainDatabus);
// 					// ivProvider is a handler of Deform user variables procedure; it is responsible for both way communication
// 					// iVManager is used to get iv's from Databus
// 					boost::shared_ptr<DeformCaseIV> ivProvider(new DeformCaseIV(ivManager));
//           deformCaseIV_ = ivProvider;
// 
// 					// IV source for sub-models; Gets iv's from ivProvider (iv's are represented just as doubles) and trasnforms them to am3 'properties'
// 					boost::shared_ptr<IVStorageAdaptedToPrecipitationsMeanRadius> precMeanRadiusStorage(new IVStorageAdaptedToPrecipitationsMeanRadius(ivProvider));
// 					boost::shared_ptr<IVStorageAdaptedToPrecipitationsVolumeFraction> PrecVolFractionStorage(new IVStorageAdaptedToPrecipitationsVolumeFraction(ivProvider));
// 
// 					// Dislocations submodels (uses iv's)
// // 					mdi::DislocationWallEquation_Param<IVStorageAdaptedToDislWalEquat> dwParam(2.4e6, 6e7, 0.3, 7.0, 0.0, 0.0, dislWallEquatStorage);
// // 					MODEL_INSTANCE_WITH_PARAM(DislocationWallEquation, DislocationWallEquationModel, caseAbaqusTestDatabus, dislocationWallEquation, mainDatabus, dwParam);
// // 					mdi::DislocationRandomEquation_Param<IVStorageAdaptedToDislRandEquat> drParam(22e6, 80e7, 2.2, 105.0, 0.0, 0.0, dislRandEquatStorage);
// // 					MODEL_INSTANCE_WITH_PARAM(DislocationRandomEquation, DislocationRandomEquationModel, caseAbaqusTestDatabus, dislocationRandomEquation, mainDatabus, drParam);
// 
// 
// 										
// 					MODEL_INSTANCE_REDESIGN(PrecipitationsSwitcher, PrecipitationsModel, caseAbaqusTestToUSPDatabus, precipitationsSwitcher, mainDatabus);
// 					precipitationsSwitcher->kbs_->SetModelInput(mainDatabus.get()); //This one should be done automatically, somewhere inside constructing process
// 
// 					//   MatCalcScriptProvider scripts;
// 					MODEL_INSTANCE_IN_SWITCHER(SecondPhasePrecipitationsConst, SecondPhasePrecipitationsConstModel, precConst, mainDatabus, PrecipitationsModel, precipitationsSwitcher);
// //					MODEL_INSTANCE_IN_SWITCHER(SecondPhasePrecipitationsMatCalc, SecondPhasePrecipitationsMatCalcModel, precMC, mainDatabus, PrecipitationsModel, precipitationsSwitcher);
// 
// //          lo::log<lo::SEV_DEBUG>(lo::LOG_MODEL, "Initializing MPI");
// //          precMC->Init();
// 
// 
// 					//   precipitationsSwitcher->AddModel(gmm);
// 
// 					// State provider gets the state of the material from Deform (SetSomething procedures in user-supplied code)
// 					//boost::shared_ptr<DeformCaseTemperatures> stateProvider(new DeformCaseTemperatures());
// 					// MacromodelState uses stateProvider to get the material state and feed Databus with them.
// 					//If not-default constructor needed, the macro MODEL_INSTANCE_WITH_PARAMS must be used
// //					MODEL_INSTANCE_WITH_PARAM(MacroModelState, MacroModelStateModel, caseAbaqusTestToUSPDatabus, macroModelState, mainDatabus, stateProvider);
// 					MODEL_INSTANCE_WITH_PARAM(MacroModelTemperature, MacroModelTemperaturesModel, caseAbaqusTestToUSPDatabus, macroModelTemperatures, mainDatabus, ivProvider);

					initialized_ = true;
				}
			}


			Main::Main()
			{
//        DebugBreak();
				try
				{
					Init();
				}
				catch (std::exception& e)
				{
					lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, e.what());
				}
			}

			bool Main::IsInitialized() const
			{
				return initialized_;
			}

			boost::shared_ptr<sab::AbaqusHandlerFacadeMtr> Main::GetAbaqusCaseMtr()
      {
				return abaqusCaseMtr_;
      }

			boost::shared_ptr<sab::AbaqusHandlerFacadeElm> Main::GetAbaqusCaseElement()
			{
				return abaqusCaseMtr_;
			}

			boost::shared_ptr<sab::AbaqusHandlerFacadeTime> Main::GetAbaqusCaseTime()
			{
				return abaqusCaseMtr_;
			}

		} //taq
	} //cases
} //am3