
/*! \file dataStructures.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief  Data structures specific for ADINA FEM software
 * \details Presently ADINA-CFD structures defined.
*********************************************************************/
#ifndef adinaDataInterface_h__
#define adinaDataInterface_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../models/point.h"
#include "../logger/logger.h"

/*	using ***********************************************************/
namespace pro=am3::model;
namespace mpr=am3::model;
namespace mpt=am3::model::common;
namespace lo=am3::logger;
/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {

//////////////////////////////////////////////////////////////////////////

    //! Values, which are provided by ADINA-F user supplied material
    //!
    //! Structure to store values, not an interface. Works fine, when combined with interface
    struct PropertiesAdinaFOutputs
    {
      PressureWithDerivatives pressure;
      VelocityWithDerivatives v;
      TemperatureWithDerivatives temperature;
      TurbulenceKWithDerivatives kappa;
      TurbulenceEWithDerivatives epsilon;
      Coordinates coord;
      Time time;
      ElementGroupNumber elGrNumber;
      ElementNumber elNumber;
    };

    //! Values, which are expected by ADINA-F user supplied material
    //!
    //! Structure to store values, not an interface. Works fine, when combined with interface
    struct PropertiesAdinaFInputs
    {
      Viscosity visc;
      Cp cp;
      Cv cv;
      ThermalConductivity thermalConductivity;
      CoeffVolumeExpansion coeffVolumeExpansion;
      Density density;
      ReferenceTemperature referenceTemperature;
      Gravitation gravitation;
      HeatPerVolume heatPerVolume;
      SurfaceTension surfaceTension;
      BulkModulus bulkModulus;
    };  

//////////////////////////////////////////////////////////////////////////

    namespace contract
    {
      namespace defined
      {
				inline mpt::ComputedPoint<mpr::Coordinates2D> ConvertAdina3DPointTo2DPoint(const mpt::Point<mpr::Coordinates3D>& point3d)
				{
					mpr::Coordinates2D coord2d(point3d.Coordinates().y(), point3d.Coordinates().z());
					if (point3d.Coordinates().x()!=0) lo::log<lo::SEV_WARNING>(lo::LOG_MODEL,"3D to 2D point conversion warning. x-coordinate not equal to 0");
					return mpt::ComputedPoint<mpr::Coordinates2D>(coord2d,point3d.GetTime());
				};
				inline mpt::ComputedPoint<mpr::Coordinates3D> Convert2DPointToAdina3DPoint(const mpt::Point<mpr::Coordinates2D>& point2d)
				{
					mpr::Coordinates3D coord3d(0.0, point2d.Coordinates().x(), point2d.Coordinates().y());
					return mpt::ComputedPoint<mpr::Coordinates3D>(coord3d,point2d.GetTime());
				};

        //! Interface providing values, which must be computed for ADINA-F user supplied material
        struct AdinaFContract
          : public ContractItemDispatcher<pro::Viscosity>
          , public ContractItemDispatcher<pro::Cp>
          , public ContractItemDispatcher<pro::Cv>
          , public ContractItemDispatcher<pro::ThermalConductivity>
          , public ContractItemDispatcher<pro::CoeffVolumeExpansion>
		  , public ContractItemDispatcher<pro::DensityWithDerivatives>
          , public ContractItemDispatcher<pro::ReferenceTemperature>
          , public ContractItemDispatcher<pro::Gravitation>
          , public ContractItemDispatcher<pro::HeatPerVolume>
          , public ContractItemDispatcher<pro::SurfaceTension>
          , public ContractItemDispatcher<pro::BulkModulus>
        {
          virtual ~AdinaFContract(){}
          using ContractItemDispatcher<pro::Viscosity>::GetState;
          using ContractItemDispatcher<pro::Cp>::GetState;
          using ContractItemDispatcher<pro::Cv>::GetState;
          using ContractItemDispatcher<pro::ThermalConductivity>::GetState;
          using ContractItemDispatcher<pro::CoeffVolumeExpansion>::GetState;
          using ContractItemDispatcher<pro::DensityWithDerivatives>::GetState;
          using ContractItemDispatcher<pro::ReferenceTemperature>::GetState;
          using ContractItemDispatcher<pro::Gravitation>::GetState;
          using ContractItemDispatcher<pro::HeatPerVolume>::GetState;
          using ContractItemDispatcher<pro::SurfaceTension>::GetState;
          using ContractItemDispatcher<pro::BulkModulus>::GetState;
        };

				struct AdinaFMassTransferContract
					: public ContractItemDispatcher<pro::SourceLH>
					, public ContractItemDispatcher<pro::SourceRH>
				{
					virtual ~AdinaFMassTransferContract(){}
					using ContractItemDispatcher<pro::SourceLH>::GetState;
					using ContractItemDispatcher<pro::SourceRH>::GetState;
				};

        //! Interface for storing Adina CFD results
        struct AdinaFStore
        {
          virtual void Set(const pro::PressureWithDerivatives& pressure,
            const pro::VelocityWithDerivatives& velocity, 
            const pro::TemperatureWithDerivatives& temperature, 
            const pro::TurbulenceKWithDerivatives& kappa, 
            const pro::TurbulenceEWithDerivatives& epsilon,
            const pro::Coordinates& coordinates, 
            const pro::Time& time, 
            const pro::ElementGroupNumber& elementGroupNumber, 
            const pro::ElementNumber& elementNumber)=0;
          virtual void Set(const pro::PressureWithDerivatives& pressure,
            const pro::VelocityWithDerivatives& velocity, 
            const pro::TemperatureWithDerivatives& temperature, 
            const pro::TurbulenceKWithDerivatives& kappa, 
            const pro::TurbulenceEWithDerivatives& epsilon,
            const pro::Coordinates& coordinates, 
            const pro::Time& time, 
            const pro::ElementGroupNumber& elementGroupNumber, 
            const pro::ElementNumber& elementNumber,
            const pro::RatiosSet& ratiosSet)=0;
        };

				struct AdinaFMassTransferStore
				{
					virtual void Set(const pro::ShearRate& shearRate,
						const pro::Velocity& velocity, 
						const pro::Pressure& pressure,
						const pro::Temperature& temperature, 
						const pro::TurbulenceK& kappa, 
						const pro::TurbulenceE& epsilon,
						const pro::Coordinates& coordinates, 
						const pro::Time& time, 
						const pro::ElementGroupNumber& elementGroupNumber, 
						const pro::ElementNumber& elementNumber,
						const pro::RatiosSet& ratiosSet,
						const pro::MaterialID& ratioCurrent)=0;
				};

        //! Interface providing values, which are computed and provided by ADINA-F user supplied material
        struct AdinaFProvider
          : public Provider<pro::VelocityWithDerivatives>
          , public Provider<pro::PressureWithDerivatives>
          , public Provider<pro::TemperatureWithDerivatives>
          , public Provider<pro::TurbulenceKWithDerivatives>
          , public Provider<pro::TurbulenceEWithDerivatives>
          , public Provider<pro::Coordinates>
          , public Provider<pro::Time>
          , public Provider<pro::ElementGroupNumber>
          , public Provider<pro::ElementNumber>
          , public Provider<pro::RatiosSet>
        {
          using Provider<pro::VelocityWithDerivatives>::GetState;
          using Provider<pro::TemperatureWithDerivatives>::GetState;
        };

				struct AdinaFMassTransferProvider
					: public Provider<pro::Velocity>
					, public Provider<pro::ShearRate>
					, public Provider<pro::Pressure>
					, public Provider<pro::Temperature>
					, public Provider<pro::TurbulenceK>
					, public Provider<pro::TurbulenceE>
					, public Provider<pro::Coordinates>
					, public Provider<pro::Time>
					, public Provider<pro::ElementGroupNumber>
					, public Provider<pro::ElementNumber>
					, public Provider<pro::RatiosSet>
					, public Provider<pro::MaterialID>
				{
					using Provider<pro::Velocity>::GetState;
					using Provider<pro::Temperature>::GetState;
					using Provider<pro::MaterialID>::GetState;
				};
			}
    }

    

//////////////////////////////////////////////////////////////////////////
 //! Adina-CFD input/output data structure
 typedef ModelDescription<
   am3::model::PropertiesAdinaFInputs,
   am3::model::PropertiesAdinaFOutputs,
   util::Null> 
   AdinaFModelDescription;

 }//model

 //saf::contract::defined::AdinaFContract

 namespace software
 {
   namespace adina
   {
     namespace adinaF
     {
       namespace contract
       {
         namespace defined
         {
           using am3::model::contract::defined::AdinaFContract;
					 using am3::model::contract::defined::AdinaFMassTransferContract;
					 using am3::model::contract::defined::AdinaFMassTransferProvider;
					 using am3::model::contract::defined::ConvertAdina3DPointTo2DPoint;
					 using am3::model::contract::defined::Convert2DPointToAdina3DPoint;
         } //defined
       } //contract
     } //adinaF
   } //adina
 } //software
} //am3

#endif // adinaDataInterface_h__
