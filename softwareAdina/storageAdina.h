
/*! \file storageAdina.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief Definitions of storage class. Storage can store all (or part of) data computed by models
 * \details   	
*********************************************************************/
#ifndef modelAdinaSimple_storageAdina_h__
#define modelAdinaSimple_storageAdina_h__
/*	include *********************************************************/

#include "../softwareAdina/adinaDataInterface.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace def=am3::model::contract::defined;
namespace pro=am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace storage
  {
    //! Very simple storage for ADINA-CFD
    //!
    //! Stores values only from last ADINA-CFD call (last step, last element)
    //! Must implements
    //!   - interface for storing data (am3::model::contract::defined::AdinaFStore)
    //!   - interface for providing data to other models (adapters) (am3::model::contract::defined::AdinaFProvider)
    class AdinaFStorage
      : public def::AdinaFStore
      , public def::AdinaFProvider
    {
    protected:
      //! \name Variables to store data from ADINA-CFD
      // @{
      pro::PressureWithDerivatives pressure_;
      pro::VelocityWithDerivatives velocity_;
      pro::TemperatureWithDerivatives temperature_;
      pro::TurbulenceKWithDerivatives kappa_;
      pro::TurbulenceEWithDerivatives epsilon_;
      pro::Coordinates coordinates_;
      pro::Time time_;
      pro::ElementGroupNumber elementGroupNumber_;
      pro::ElementNumber elementNumber_;
      pro::RatiosSet ratiosSet_;
      // @ }
    public:
      //! Implementation of am3::model::contract::defined::AdinaFStore interface
      virtual void Set(const pro::PressureWithDerivatives& pressure,
        const pro::VelocityWithDerivatives& velocity, 
        const pro::TemperatureWithDerivatives& temperature, 
        const pro::TurbulenceKWithDerivatives& kappa, 
        const pro::TurbulenceEWithDerivatives& epsilon,
        const pro::Coordinates& coordinates, 
        const pro::Time& time, 
        const pro::ElementGroupNumber& elementGroupNumber, 
        const pro::ElementNumber& elementNumber);
      virtual void Set(const pro::PressureWithDerivatives& pressure,
        const pro::VelocityWithDerivatives& velocity, 
        const pro::TemperatureWithDerivatives& temperature, 
        const pro::TurbulenceKWithDerivatives& kappa, 
        const pro::TurbulenceEWithDerivatives& epsilon,
        const pro::Coordinates& coordinates, 
        const pro::Time& time, 
        const pro::ElementGroupNumber& elementGroupNumber, 
        const pro::ElementNumber& elementNumber,
        const pro::RatiosSet& ratiosSet);
      //! \name Implementation of am3::model::contract::defined::AdinaFProvider interface
      // @{
      virtual pro::PressureWithDerivatives GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::PressureWithDerivatives* const typeTrait);
      virtual pro::VelocityWithDerivatives GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::VelocityWithDerivatives* const typeTrait);
      virtual pro::TemperatureWithDerivatives GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::TemperatureWithDerivatives* const typeTrait);
      virtual pro::TurbulenceKWithDerivatives GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::TurbulenceKWithDerivatives* const typeTrait);
      virtual pro::TurbulenceEWithDerivatives GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::TurbulenceEWithDerivatives* const typeTrait);
      virtual pro::Coordinates GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::Coordinates* const typeTrait);
      virtual pro::Time GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::Time* const typeTrait);
      virtual pro::ElementGroupNumber GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::ElementGroupNumber* const typeTrait);
      virtual pro::ElementNumber GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::ElementNumber* const typeTrait);
      virtual pro::RatiosSet GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::RatiosSet* const typeTrait);
      // @ }
    };
  } //storage
	namespace software
	{
		namespace adina
		{
			namespace storage
			{
				using namespace am3::storage;  

				class AdinaFMassTransferStorage
					: public def::AdinaFMassTransferStore
					, public def::AdinaFMassTransferProvider
				{
				protected:
					//! \name Variables to store data from ADINA-CFD
					// @{
					pro::Pressure pressure_;
					pro::ShearRate shearRate_;
					pro::Velocity velocity_;
					pro::Temperature temperature_;
					pro::TurbulenceK kappa_;
					pro::TurbulenceE epsilon_;
					pro::Coordinates coordinates_;
					pro::Time time_;
					pro::ElementGroupNumber elementGroupNumber_;
					pro::ElementNumber elementNumber_;
					pro::RatiosSet ratiosSet_;
					pro::MaterialID ratioCurrent_;
					// @ }
				public:
					//! Implementation of am3::model::contract::defined::AdinaFStore interface
					virtual void Set(const pro::ShearRate& shearRate,
						const pro::Velocity& velocity, 
						const pro::Pressure& pressure,
						const pro::Temperature& temperature, 
						const pro::TurbulenceK& kappa, 
						const pro::TurbulenceE& epsilon,
						const pro::Coordinates& coordinates, 
						const pro::Time& time, 
						const pro::ElementGroupNumber& elementGroupNumber, 
						const pro::ElementNumber& elementNumber,
						const pro::RatiosSet& ratiosSet,
						const pro::MaterialID& ratioCurrent);

					
					virtual pro::Pressure GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::Pressure* const typeTrait);
					virtual pro::ShearRate GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::ShearRate* const typeTrait);
					virtual pro::Velocity GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::Velocity* const typeTrait);
					virtual pro::Temperature GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::Temperature* const typeTrait);
					virtual pro::TurbulenceK GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::TurbulenceK* const typeTrait);
					virtual pro::TurbulenceE GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::TurbulenceE* const typeTrait);
					virtual pro::Coordinates GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::Coordinates* const typeTrait);
					virtual pro::Time GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::Time* const typeTrait);
					virtual pro::ElementGroupNumber GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::ElementGroupNumber* const typeTrait);
					virtual pro::ElementNumber GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::ElementNumber* const typeTrait);
					virtual pro::RatiosSet GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::RatiosSet* const typeTrait);
					virtual pro::MaterialID GetState(am3::model::common::Point<am3::model::Coordinates>* point,const pro::MaterialID* const typeTrait);
					
				};
			} //storage
			
		} //adina
	} //software
} //am3

#endif // modelAdinaSimple_storageAdina_h__
