#include "storageAdina.h"


void am3::storage::AdinaFStorage::Set( const pro::PressureWithDerivatives& pressure, const pro::VelocityWithDerivatives& velocity, const pro::TemperatureWithDerivatives& temperature, const pro::TurbulenceKWithDerivatives& kappa, const pro::TurbulenceEWithDerivatives& epsilon, const pro::Coordinates& coordinates, const pro::Time& time, const pro::ElementGroupNumber& elementGroupNumber, const pro::ElementNumber& elementNumber )
{
  pressure_=pressure;
  velocity_=velocity;
  temperature_=temperature;
  kappa_=kappa;
  epsilon_=epsilon;
  coordinates_=coordinates;
  time_=time;
  elementGroupNumber_=elementGroupNumber;
  elementNumber_=elementNumber;
}

void am3::storage::AdinaFStorage::Set( const pro::PressureWithDerivatives& pressure, const pro::VelocityWithDerivatives& velocity, const pro::TemperatureWithDerivatives& temperature, const pro::TurbulenceKWithDerivatives& kappa, const pro::TurbulenceEWithDerivatives& epsilon, const pro::Coordinates& coordinates, const pro::Time& time, const pro::ElementGroupNumber& elementGroupNumber, const pro::ElementNumber& elementNumber, const pro::RatiosSet& ratiosSet )
{
  this->Set(pressure,velocity,temperature,kappa,epsilon,coordinates,time,elementGroupNumber,elementNumber);
  ratiosSet_=ratiosSet;
  std::vector<double> rv;
  rv.push_back(0.5);
  rv.push_back(0.5);
  rv.push_back(0.5);
  rv.push_back(0.5);
  rv.push_back(0.5);
  ratiosSet_.Value(rv);
}

pro::PressureWithDerivatives am3::storage::AdinaFStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::PressureWithDerivatives* const typeTrait )
{
  return pressure_;
}

pro::VelocityWithDerivatives am3::storage::AdinaFStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::VelocityWithDerivatives* const typeTrait )
{
  return velocity_;
}

pro::TemperatureWithDerivatives am3::storage::AdinaFStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::TemperatureWithDerivatives* const typeTrait )
{
  return temperature_;
}

pro::TurbulenceKWithDerivatives am3::storage::AdinaFStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::TurbulenceKWithDerivatives* const typeTrait )
{
  return kappa_;
}

pro::TurbulenceEWithDerivatives am3::storage::AdinaFStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::TurbulenceEWithDerivatives* const typeTrait )
{
  return epsilon_;
}

pro::Coordinates am3::storage::AdinaFStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::Coordinates* const typeTrait )
{
  return coordinates_;
}

pro::Time am3::storage::AdinaFStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::Time* const typeTrait )
{
  return time_;
}

pro::ElementGroupNumber am3::storage::AdinaFStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::ElementGroupNumber* const typeTrait )
{
  return elementGroupNumber_;
}

pro::ElementNumber am3::storage::AdinaFStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::ElementNumber* const typeTrait )
{
  return elementNumber_;
}

pro::RatiosSet am3::storage::AdinaFStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::RatiosSet* const typeTrait )
{
  pro::RatiosSet rs;
  return rs;
}


void am3::software::adina::storage::AdinaFMassTransferStorage::Set( const pro::ShearRate& shearRate, const pro::Velocity& velocity, const pro::Pressure& pressure, const pro::Temperature& temperature, const pro::TurbulenceK& kappa, const pro::TurbulenceE& epsilon, const pro::Coordinates& coordinates, const pro::Time& time, const pro::ElementGroupNumber& elementGroupNumber, const pro::ElementNumber& elementNumber, const pro::RatiosSet& ratiosSet, const pro::MaterialID& ratioCurrent )
{
	shearRate_=shearRate;
	velocity_=velocity;
	temperature_=temperature;
	kappa_=kappa;
	epsilon_=epsilon;
	coordinates_=coordinates;
	time_=time;
	elementGroupNumber_=elementGroupNumber;
	elementNumber_=elementNumber;
	ratiosSet_=ratiosSet;
	ratioCurrent_=ratioCurrent;
}


pro::Pressure am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::Pressure* const typeTrait )
{
	return pressure_;
}

pro::Velocity am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::Velocity* const typeTrait )
{
	return velocity_;
}

pro::MaterialID am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::MaterialID* const typeTrait )
{
	return ratioCurrent_;
}

pro::ShearRate am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::ShearRate* const typeTrait )
{
	return shearRate_;
}

pro::Temperature am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::Temperature* const typeTrait )
{
	return temperature_;
}

pro::TurbulenceK am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::TurbulenceK* const typeTrait )
{
	return kappa_;
}

pro::TurbulenceE am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::TurbulenceE* const typeTrait )
{
	return epsilon_;
}

pro::Coordinates am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::Coordinates* const typeTrait )
{
	return coordinates_;
}

pro::Time am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::Time* const typeTrait )
{
	return time_;
}

pro::ElementGroupNumber am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::ElementGroupNumber* const typeTrait )
{
	return elementGroupNumber_;
}

pro::ElementNumber am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::ElementNumber* const typeTrait )
{
	return elementNumber_;
}

pro::RatiosSet am3::software::adina::storage::AdinaFMassTransferStorage::GetState( am3::model::common::Point<am3::model::Coordinates>* point,const pro::RatiosSet* const typeTrait )
{
	pro::RatiosSet rs;
	return rs;
}