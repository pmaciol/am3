#include "../models/point.h"
#include "adinaFprocessor.h"

namespace com=am3::model::common;

void am3::model::adina::AdinaFCallsProcessor::Init( def::AdinaFContract* otherModels,def::AdinaFStore* store  )
{
  adapterLevelStorage_=store;
  otherModels_=otherModels;
}

void am3::model::adina::AdinaFCallsProcessor::processAdinaData( AdinaFUserMaterial& in, AdinaFUserMaterialReturn& out )
{
  pro::PressureWithDerivatives p(in.pressure,in.pressure_dx,in.pressure_dy,in.pressure_dz);
  pro::Time t(in.time);
  pro::VelocityWithDerivatives v(
    in.x_velocity,in.x_velocity_dx,in.x_velocity_dy,in.x_velocity_dz,
    in.y_velocity,in.y_velocity_dx,in.y_velocity_dy,in.y_velocity_dz,
    in.z_velocity,in.z_velocity_dx,in.z_velocity_dy,in.z_velocity_dz);
  pro::TemperatureWithDerivatives temp(in.temperature,in.temperature_dx,in.temperature_dy,in.temperature_dz);
  pro::TurbulenceEWithDerivatives epsilon(in.turbulence_e,in.turbulence_e_dx,in.turbulence_e_dy,in.turbulence_e_dz);
  pro::TurbulenceKWithDerivatives kappa(in.turbulence_k,in.turbulence_k_dx,in.turbulence_k_dy,in.turbulence_k_dz);
  pro::Coordinates coord(in.x_coord,in.y_coord,in.z_coord);
  pro::ElementGroupNumber elGroupNum(in.element_group_number);
  pro::ElementNumber elNum(in.element_number);

  adapterLevelStorage_->Set(p,v,temp,kappa,epsilon,coord,t,elGroupNum,elNum);

  com::ComputedPoint<am3::model::Coordinates> point(coord,t);
  pro::Viscosity visc;
  out.viscosity=(otherModels_->GetState(&point,&visc)).Value();
  out.cp = (otherModels_->GetState(&point,&am3::model::kCpTrait)).Value(); //CMAT(2 ) = CP    = SPECIFIC HEAT AT CONSTANT PRESSURE
  out.thermal_conductivity=(otherModels_->GetState(&point,&am3::model::kThermalConductivityTrait)).Value(); // = 20.0;		//CMAT(3 ) = XKCON = THERMAL CONDUCTIVITY
  out.coeff_volume_expansion = (otherModels_->GetState(&point,&am3::model::kCoeffVolumeExpansionTrait)).Value();//0.000017;	//CMAT(4 ) = BETA  = COEFFICIENT OF VOLUME EXPANSION
  out.density = (otherModels_->GetState(&point,&am3::model::kDensityWithDerivativesTrait)).Value();//7000.0;					//CMAT(5 ) = RO    = DENSITY
  out.referene_temperature = (otherModels_->GetState(&point,&am3::model::kReferenceTemperatureTrait)).Value();//0.0;		//CMAT(6 ) = TC    = REFERENCE TEMPERATURE
  out.x_gravitation = (otherModels_->GetState(&point,&am3::model::kGravitationTrait)).x();//0.0;			//CMAT(7 ) = GRAVX = X-GRAVITATION
  out.y_gravitation = (otherModels_->GetState(&point,&am3::model::kGravitationTrait)).y();//0.0;			//CMAT(8 ) = GRAVY = Y-GRAVITATION
  out.z_gravitation = (otherModels_->GetState(&point,&am3::model::kGravitationTrait)).z();//0.0;			//CMAT(9 ) = GRAVZ = Z-GRAVITATION
  out.heat_per_volume = (otherModels_->GetState(&point,&am3::model::kHeatPerVolumeTrait)).Value();//0.0;			//CMAT(10) = QB    = RATE OF HEAT GENERATED PER VOLUME
  out.surface_tension = (otherModels_->GetState(&point,&am3::model::kSurfaceTensionTrait)).Value();//0.0;			//CMAT(11) = SIGMA = COEFFICIENT OF SURFACE TENSION
  out.bulk_modulus = (otherModels_->GetState(&point,&am3::model::kBulkModulusTrait)).Value();//1e20;				//CMAT(12) = XKAPA = BULK MODULUS
  out.cv = (otherModels_->GetState(&point,&am3::model::kCvTrait)).Value();//0.0;						//CMAT(13) = CV    = SPECIFIC HEAT AT CONSTANT VOLUME
  out.density_d_pressure = (otherModels_->GetState(&point,&am3::model::kDensityWithDerivativesTrait)).dp();//0.0;		//CMAT(14) = DR_DP = DERIVATIVE OF RO WITH RESPECT TO PRESSURE
  out.density_d_temperature = (otherModels_->GetState(&point,&am3::model::kDensityWithDerivativesTrait)).dt();//0.0;	//CMAT(15) = DR_DT = DERIVATIVE OF RO WITH RESPECT TO TEMPERATURE

  // The end of temporary
}

void am3::model::adina::AdinaFMassTransferCallsProcessor::processAdinaData( AdinaFUserMassTransfer& in, AdinaFUserMassTransferReturn& out )
{
	pro::ShearRate shearRate; shearRate(in.shearRate);
	pro::Velocity vel(in.vx,in.vy,in.vz);
	pro::Pressure pres; pres(in.pressure);
	pro::Temperature temperature; temperature(in.temperature);
	pro::TurbulenceE turbE; turbE(in.turbulence_e);
	pro::TurbulenceK turbK; turbK(in.turbulence_k);
	pro::Coordinates3D coord(in.coordX,in.coordY, in.coordZ);
	pro::Time time(in.time);
	pro::ElementGroupNumber elGN(in.elementGroup);
	pro::ElementNumber elN(in.elementNumber);
	pro::RatiosSet ratioSet; ratioSet.Value(in.massRatio);
	pro::MaterialID curRatio; curRatio(in.massRatioCurrent);

	adapterLevelStorage_->Set(shearRate,vel,pres, temperature,turbK,turbE,coord,time,elGN,elN,ratioSet,curRatio);

	com::ComputedPoint<am3::model::Coordinates> point(coord,time);
	pro::SourceRH srh;
	pro::SourceLH slh;
	out.so=(otherModels_->GetState(&point,&srh)).Value();
	out.sf=(otherModels_->GetState(&point,&slh)).Value();
}

void am3::model::adina::AdinaFMassTransferCallsProcessor::Init( def::AdinaFMassTransferContract* otherModels,def::AdinaFMassTransferStore* store )
{
	adapterLevelStorage_=store;
	otherModels_=otherModels;
}
