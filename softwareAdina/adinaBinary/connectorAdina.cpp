
#include "../../communicationProvider/predefinedStreams.h"
//#include <fstream>

/*	using ***********************************************************/

using namespace am3::common::communication;


extern "C"
{

bool isSocketOpen=false;

int socketID=0; 


// void TEST()
// {
// 	std::ofstream myfile;
// 	myfile.open ("g:/example.txt");
// 
// 	if (!isSocketOpen)
// 	{
// 		Winsock2Stream::initData socketData;
// 		socketData.bufferSize = 1000;
// 		socketData.hostname = "127.0.0.1";
// 		socketData.serverPort = "5345";
// 		socketData.operatingMode = socketData.CLIENT_MODE;
// 		int socketID = am3CreateWinsock2Stream(& socketData);
// 		if (socketID>0) isSocketOpen=true;
// 		myfile << " Client created a socket with ID: " << socketID << "\n";
// 	}
// 	else myfile<< " Socket was open!\n";
// 
// 	myfile.close();
// 
// }
void SENDMR(double* SR, double* VX, double* VY, double* VZ, double* TIME,int* IELG,int* IELM,int* IMAS,int* NNOD,double* PRESSURE,double* TEMPERATURE,double* TUR_K, double* TUR_E, double* X, double* Y, double* Z, double* RATIO1,double* RATIO2,double* RATIO3,double* RATIO4)
{
//	std::ofstream myfile;
//	myfile.open ("g:/example.txt",std::fstream::app);

	if (!isSocketOpen)
	{
		Winsock2Stream::initData socketData;
		socketData.bufferSize = 1000;
		socketData.hostname = "127.0.0.1";
		socketData.serverPort = "5345";
		socketData.operatingMode = socketData.CLIENT_MODE;
		socketID = am3CreateWinsock2Stream(& socketData);
		if (socketID>0) isSocketOpen=true;
//		myfile << " Client created a socket with ID: " << socketID << "\n";
	};

	am3Winsock2StreamWritedouble(& socketID, SR);
	am3Winsock2StreamWritedouble(& socketID, VX);
	am3Winsock2StreamWritedouble(& socketID, VY);
	am3Winsock2StreamWritedouble(& socketID, VZ);
	am3Winsock2StreamWritedouble(& socketID, TIME);
	am3Winsock2StreamWriteint(& socketID, IELG);
	am3Winsock2StreamWriteint(& socketID, IELM);
	am3Winsock2StreamWriteint(& socketID, IMAS);
	am3Winsock2StreamWriteint(& socketID, NNOD);
	am3Winsock2StreamWritedouble(& socketID, PRESSURE);
	am3Winsock2StreamWritedouble(& socketID, TEMPERATURE);
	am3Winsock2StreamWritedouble(& socketID, TUR_K);
	am3Winsock2StreamWritedouble(& socketID, TUR_E);
	am3Winsock2StreamWritedouble(& socketID, X);
	am3Winsock2StreamWritedouble(& socketID, Y);
	am3Winsock2StreamWritedouble(& socketID, Z);
	am3Winsock2StreamWritedouble(& socketID, RATIO1);
	am3Winsock2StreamWritedouble(& socketID, RATIO2);
	am3Winsock2StreamWritedouble(& socketID, RATIO3);
	am3Winsock2StreamWritedouble(& socketID, RATIO4);
	

}
void READMR(double* SF, double* SO)
{
	am3Winsock2StreamReaddouble(& socketID, SF);
	am3Winsock2StreamReaddouble(& socketID, SO);
}
}