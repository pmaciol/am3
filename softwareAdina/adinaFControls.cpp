/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/

/*	include *********************************************************/
#include <string>
#include <vector>
#include <boost/foreach.hpp>
#include "adinaFControls.h"
#include "logger/logger.h"
#include "../common/common.h"
#include "../softwareAdina/adinaFprocessor.h"
#include "../softwareAdina/adinaSocketServer.h"

/*	using ***********************************************************/

using std::string;
using std::vector;
using am3::logger::SEV_DEBUG;
using am3::logger::TestingMsg;
namespace pro=am3::model;
namespace adi=am3::model::adina;

/*	classes *********************************************************/ 

namespace am3
{
  namespace model
  {
    int ModelAdina::ValidateConf()
    {
	  using am3::common::FileWithExt;
	  int isSuccess=1;
	  am3::systemApi::SystemApi* sApi=sysApiH_.GetSystemApi();
	  isSuccess*=CheckFileExists(sApi,inProgramOptions_.Program_name(),inProgramOptions_.Program_path(),"ADINA IN binary");
	  isSuccess*=CheckFileExists(sApi,datProgramOptions_.Program_name(),datProgramOptions_.Program_path(),"ADINA DAT binary");
	  isSuccess*=CheckFileExists(sApi,runProgramOptions_.Program_name(),runProgramOptions_.Program_path(),"ADINA RUN binary");
	  isSuccess*=CheckFileExists(sApi,FileWithExt(fileDetails_.inFileName,fileDetails_.inFileExt),inProgramOptions_.Working_directory(),"ADINA IN file");
      return isSuccess;
    };

    int ModelAdina::ValidateDat()
    {
      using am3::common::FileWithExt;
      int isSuccess=1;
       am3::systemApi::SystemApi* sApi=sysApiH_.GetSystemApi();
       isSuccess*=CheckFileExists(sApi,FileWithExt(fileDetails_.datFileName,fileDetails_.datFileExt),datProgramOptions_.Working_directory(),"ADINA DAT file");
      return isSuccess;
    };
	int ModelAdina::ValidateRun()
	{
		using am3::common::FileWithExt;
		int isSuccess=1;
		am3::systemApi::SystemApi* sApi=sysApiH_.GetSystemApi();
		isSuccess*=CheckFileExists(sApi,FileWithExt(fileDetails_.runFileName,fileDetails_.runFileExt),runProgramOptions_.Working_directory(),"ADINA RUN file");
		return isSuccess;
	};

    int ModelAdina::Init( def::AdinaFContract* otherModels,def::AdinaFStore* store  )
    {
      am3::logger::Starting("MockModel initialization");
      TestingMsg("Setting owner");
      otherModels_=otherModels;
      store_=store;
      am3::logger::Stopping("MockModel initialization");

	  int isSuccess=1;
      isSuccess*=CheckIsModuleRegistered("adina");
      module_variable_map=&(ReadModuleConfigFileAndCmdLineOpts("adina"));
      isSuccess*=Set();
      PrepareInFile();
      PrepareDatFile();
	  return isSuccess;
    }

		int ModelAdina::Init( def::AdinaFMassTransferContract* otherModels,def::AdinaFMassTransferStore* store  )
		{
			am3::logger::Starting("MockModel initialization");
			TestingMsg("Setting owner");
			otherModelsMassTransfer_=otherModels;
			storeMassTransfer_=store;
			am3::logger::Stopping("MockModel initialization");

			int isSuccess=1;
			isSuccess*=CheckIsModuleRegistered("adina");
			module_variable_map=&(ReadModuleConfigFileAndCmdLineOpts("adina"));
			isSuccess*=Set();
			PrepareInFile();
			PrepareDatFile();
			return isSuccess;
		}

    int ModelAdina::Init()
    {
      am3::logger::Starting("MockModel initialization");
      TestingMsg("Setting owner");

      am3::logger::Stopping("MockModel initialization");

      int isSuccess=1;
      isSuccess*=CheckIsModuleRegistered("adina");
      module_variable_map=&(ReadModuleConfigFileAndCmdLineOpts("adina"));
      isSuccess*=Set();
      PrepareInFile();
      PrepareDatFile();
      return isSuccess;
    }

    int ModelAdina::Init( def::AdinaFContract* otherModels,boost::shared_ptr<def::AdinaFStore> store )
    {
      return this->Init(otherModels,store.get());
      
    }

    int am3::model::ModelAdina::Listen( am3::model::contract::defined::AdinaFContract* afc )
    {
      adi::AdinaFCallsProcessor processor;
      processor.Init(this->otherModels_,this->store_);
      //adinaSocketServer::Communicate("0.0.0.0", 22564, processor);
//			adinaSocketServer::Communicate("0.0.0.0", "5345", processor);
      return 1;
    }

		int am3::model::ModelAdina::Listen( am3::model::contract::defined::AdinaFMassTransferContract* afc )
		{
			adi::AdinaFMassTransferCallsProcessor processor;
			processor.Init(this->otherModelsMassTransfer_,this->storeMassTransfer_);
			//adinaSocketServer::Communicate("0.0.0.0", 22564, processor);
			adinaSocketServer::Communicate("0.0.0.0", "5345", processor);
			return 1;
		}

    void ModelAdina::PrepareInFile()
    {
      //Should call script or something like this, not adina

    }


    void ModelAdina::PrepareDatFile()
    {
      sysApiH_.GetSystemApi()->SynchronicExecute(
        inProgramOptions_.Program_path(),
        inProgramOptions_.Program_name(),
        (inProgramOptions_.OptionsToString()+fileDetails_.inFileName+"."+fileDetails_.inFileExt),
        inProgramOptions_.Working_directory());
    }

    int ModelAdina::Run()
    {
		int isSuccess=1;
		if (ValidateDat())
		{
			isSuccess=sysApiH_.GetSystemApi()->AsynchronicExecute(
				datProgramOptions_.Program_path(),
				datProgramOptions_.Program_name(),
				(datProgramOptions_.OptionsToString()+fileDetails_.datFileName+"."+fileDetails_.datFileExt),
				datProgramOptions_.Working_directory());
      this->Listen(otherModelsMassTransfer_);
 		} 
		else
		{
			isSuccess=0;
		}
		return isSuccess;
    }

    int ModelAdina::PostProcess()
    {
	  int isSuccess=1;
	  if (ValidateRun())
	  {
		isSuccess=sysApiH_.GetSystemApi()->AsynchronicExecute(
			runProgramOptions_.Program_path(),
			runProgramOptions_.Program_name(),
			(runProgramOptions_.OptionsToString()+fileDetails_.runFileName+"."+fileDetails_.runFileExt),
			runProgramOptions_.Working_directory());
	  }
	  else isSuccess=0;
	  return isSuccess;
    }

	int ModelAdina::CheckFileExists( const am3::systemApi::SystemApi* sApi, const string fileName, const string filePath,const string validationComment )
	{
		int isSuccess=0;
		using am3::logger::log;
		using am3::logger::SEV_NOTICE;
		using am3::logger::SEV_CRITICAL;
		using am3::logger::LOG_MODEL;

		if (sApi->FileExists(fileName,filePath))
		{
			log<SEV_NOTICE>(LOG_MODEL, (validationComment + " OK").c_str());
			isSuccess=1;
		} 
		else
		{log<SEV_CRITICAL>(LOG_MODEL,(validationComment + " not OK: "+fileName+" "+filePath).c_str());};
		return isSuccess;
	}


//////////////////////////////////////////////////////////////////////////
  }
}


po::options_description& am3::model::ModelAdina::DefineOptionsForModule()
{
  po::options_description& od=kProgramOptions.GetNewProgramOptions("Adina configuration");
  od.add_options()
    ("AdinaWorkDir,w", "Adina working directory")
    ("AdinaInName", po::value<string>()->default_value("test"),"Adina tests name")
    ("AdinaDatName", po::value<string>()->default_value("test"),"Adina tests name")
    ("AdinaPorName", po::value<string>()->default_value("test"),"Adina tests name")


    ("AdinaInSwitch", po::value<std::vector<string> >()->multitoken(),"")
    ("AdinaInSwitchFlag", po::value<std::vector<string> >()->multitoken(),"")

    ("AdinaDatOption", po::value<std::vector<string> >()->multitoken(),"")
    ("AdinaDatOptionFlag", po::value<std::vector<string> >()->multitoken(),"")
    ("AdinaDatOptionVal", po::value<std::vector<string> >()->multitoken(),"")

    ("AdinaBinPath,p", "Adina binaries path")
    ("InBin", po::value<string>()->default_value("aui.exe"),"Adina binaries path")
    ("DatBin", po::value<string>()->default_value("adinaf.exe"),"Adina binaries path")
    ("PorBin", po::value<string>()->default_value("aui.exe"),"Adina binaries path")
    ("DatExt", po::value<string>()->default_value("dat"),"Adina binaries path")
    ("InExt", po::value<string>()->default_value("in"),"Adina binaries path")
    ("PorExt", po::value<string>()->default_value("por"),"Adina binaries path")
    ; 
  kProgramOptions.AddToAllNewProgramOptions(od);
  return od;
}

int am3::model::ModelAdina::Set()
{
  int i=0;

  inProgramOptions_.Program_name((*module_variable_map)["InBin"].as<std::string>());
  inProgramOptions_.Program_path((*module_variable_map)["AdinaBinPath"].as<std::string>());
  inProgramOptions_.Working_directory((*module_variable_map)["AdinaWorkDir"].as<std::string>());

  const vector<string>& AdinaInSwitch = (*module_variable_map)["AdinaInSwitch"].as< vector<string> >();
  const vector<string>& AdinaInSwitchFlag = (*module_variable_map)["AdinaInSwitchFlag"].as< vector<string> >();i=0;
  BOOST_FOREACH(string str, AdinaInSwitch)
  {
    inProgramOptions_.AddOption(str,am3::common::Option(AdinaInSwitchFlag[i]));
  } 


  datProgramOptions_.Program_name((*module_variable_map)["DatBin"].as<std::string>());
  datProgramOptions_.Program_path((*module_variable_map)["AdinaBinPath"].as<std::string>());
  datProgramOptions_.Working_directory((*module_variable_map)["AdinaWorkDir"].as<std::string>());

  const vector<string>& AdinaDatOption = (*module_variable_map)["AdinaDatOption"].as< vector<string> >();
  const vector<string>& AdinaDatOptionFlag = (*module_variable_map)["AdinaDatOptionFlag"].as< vector<string> >();
  const vector<string>& AdinaDatOptionVal = (*module_variable_map)["AdinaDatOptionVal"].as< vector<string> >();

  BOOST_FOREACH(string str, AdinaDatOption)
  {
    datProgramOptions_.AddOption(str,am3::common::Option(AdinaDatOptionFlag[i],AdinaDatOptionVal[i]));
  }

  runProgramOptions_.Program_name((*module_variable_map)["PorBin"].as<std::string>());
  runProgramOptions_.Program_path((*module_variable_map)["AdinaBinPath"].as<std::string>());
  runProgramOptions_.Working_directory((*module_variable_map)["AdinaWorkDir"].as<std::string>());


  fileDetails_.datFileExt=(*module_variable_map)["DatExt"].as<std::string>();
  fileDetails_.inFileExt=(*module_variable_map)["InExt"].as<std::string>();
  fileDetails_.runFileExt=(*module_variable_map)["PorExt"].as<std::string>();
  fileDetails_.inFileName=(*module_variable_map)["AdinaInName"].as<std::string>();
  fileDetails_.datFileName=(*module_variable_map)["AdinaDatName"].as<std::string>();
  fileDetails_.runFileName=(*module_variable_map)["AdinaPorName"].as<std::string>();
  return ValidateConf();
}

void am3::model::ModelAdina::Configure()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::model::ModelAdina::PrepareForRun()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::model::ModelAdina::PrepareEnvironment()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::model::ModelAdina::PrepareInputFiles()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::model::ModelAdina::SetModuleOptionsTemplateMethod()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::model::ModelAdina::CopyReadOptionsToInternal()
{
	throw std::exception("The method or operation is not implemented.");
}

std::string am3::model::ModelAdina::GetModuleName()
{
	throw std::exception("The method or operation is not implemented.");
}

//po::options_description& am3::model::ModelAdina::GetConfiguredModuleOptions();