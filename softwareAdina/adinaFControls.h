
/*! \file adinaFControls.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef softwareAdina_adinaFControls_h__
#define softwareAdina_adinaFControls_h__
/*	include *********************************************************/

#include <string>
#include <boost/program_options.hpp>
#include "../common/systemApi.h"
#include "../common/config.h"
#include "../common/externalProgramOptions.h"
#include "../models/models.h"
#include "adinaDataInterface.h"

/*	using ***********************************************************/

namespace po=boost::program_options;
namespace def=am3::model::contract::defined;
using am3::common::kProgramOptions;
using std::string;

/*	extern **********************************************************/

 namespace am3
 {
   namespace common
  {
    extern Configuration kProgramOptions;
  }
   namespace adapter
   {
     class AdapterAdina;
   }
}


/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
//////////////////////////////////////////////////////////////////////////
/// Data structure for ADINA model files' names and extensions
    struct AdinaFilesDescription
    {
    public:
      string inFileName;
      string inFileExt;
      string datFileName;
      string datFileExt;
      string runFileName;
      string runFileExt;
    };
//////////////////////////////////////////////////////////////////////////
//High level ADINA model facade
    class ModelAdina: 
      public am3::model::Model, 
      public am3::common::ConfigurableModule
    {  
    public:
      ///\name IModel Implements Model interface
      //@{ 
      
      virtual int Run(); 
      virtual int PostProcess();
      //@}
      /// To be revised
      int Init( def::AdinaFContract* otherModels,def::AdinaFStore* store );
      int Init( def::AdinaFContract* otherModels,boost::shared_ptr<def::AdinaFStore> store );
			int Init( def::AdinaFMassTransferContract* otherModels,def::AdinaFMassTransferStore* store );

     /// What for?
      ModelAdina(){activeItem_=new AdinaFModelDescription;};
      ~ModelAdina(){if (activeItem_!=nullptr) delete activeItem_;};

    protected:
      def::AdinaFStore* store_;
			def::AdinaFMassTransferStore* storeMassTransfer_;
      virtual int Init();
      int Listen(am3::model::contract::defined::AdinaFContract* afc);
			int Listen(am3::model::contract::defined::AdinaFMassTransferContract* afc);
 //     GETTERS adinaFGetters_;

      ///What for?
      am3::model::contract::defined::AdinaFContract* otherModels_;
			am3::model::contract::defined::AdinaFMassTransferContract* otherModelsMassTransfer_;
      ///What for?
      AdinaFModelDescription* activeItem_;
      ///\name ProgramOptions Configuration of preprocessor/solver/postprocessor
      //@{ 
      am3::common::ExtProgramOpt inProgramOptions_;
      am3::common::ExtProgramOpt datProgramOptions_;
      am3::common::ExtProgramOpt runProgramOptions_;
      AdinaFilesDescription fileDetails_;
      //@}
      ///Handler to API operations
      am3::systemApi::SystemApiHandler sysApiH_;
      ///What for?
      int ValidateConf();
      ///What for?
	    int CheckFileExists(const am3::systemApi::SystemApi* sApi, const string fileName, const string filePath,const string validationComment);
      ///What for?
	    int ValidateDat();
      ///What for?
	    int ValidateRun();
      ///What for?
      void PrepareInFile();
      ///What for?
      void PrepareDatFile();
      ///What for?
      int Set();
      ///Implements ConfigurableModule interface
      po::options_description& DefineOptionsForModule();

			virtual void Configure();

			virtual void PrepareForRun();

			virtual void PrepareEnvironment();

			virtual void PrepareInputFiles();

			virtual void SetModuleOptionsTemplateMethod();

			virtual void CopyReadOptionsToInternal();

			virtual std::string GetModuleName();

		};

  }
	namespace software
	{
	  namespace adina
	  {
	    namespace adinaF
	    {
	      using namespace am3::model;
	    } //adinaF
	  } //adina
	} //software
}


#endif // softwareAdina_adinaFControls_h__
