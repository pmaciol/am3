
/*! \file kbsInterfaces.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief Abstractions of facts description. Used in communication with KBS
 * \details   	
*********************************************************************/
#ifndef knowledge_kbsInterfaces_h__
#define knowledge_kbsInterfaces_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp> 
#include "../models/dataStructures.h"
#include "../models/point.h"
//#include "../logger/logger.h"

/*	using ***********************************************************/

namespace com=am3::model::common;
namespace pro=am3::model;
namespace lo=am3::logger;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace knowledge
  {

		// Specialize templates below for your conversions between am3 types and KBS types
		template<typename PROPERTY_DESCRIPTION>
		bool ConvertToKbsTime(const double& from, PROPERTY_DESCRIPTION* to);

		template<typename PROPERTY_DESCRIPTION>
		bool ConvertToAm3Time(const PROPERTY_DESCRIPTION& from, double* to);

		template<typename PROPERTY_DESCRIPTION, typename COORDINATES>
		bool ConvertToKbsCoordinates(const COORDINATES& from, PROPERTY_DESCRIPTION* to);

		template<typename PROPERTY_DESCRIPTION, typename COORDINATES>
		bool ConvertToAm3Coordinates(const PROPERTY_DESCRIPTION& from, COORDINATES* to);

		template<typename PROPERTY_DESCRIPTION, typename TYPE_TRAIT>
		bool ConvertToKbsTypeTrait(const TYPE_TRAIT* const from, PROPERTY_DESCRIPTION* to);

		template<typename PROPERTY_DESCRIPTION, typename VARIABLE>
		bool ConvertToKbsValue(const VARIABLE* from, PROPERTY_DESCRIPTION* to);

		template<typename PROPERTY_DESCRIPTION>
		bool ConvertToModelIdProperty(const PROPERTY_DESCRIPTION& from, mpr::ModelID* to);

		template<typename PROPERTY_DESCRIPTION, typename DATABUS>
		struct QueryDatabus
		{
			template <typename POINT>
			bool static Query (PROPERTY_DESCRIPTION& toFill, boost::shared_ptr<DATABUS> fillFrom,  POINT* point);
		};
		// End of templates to specialize

//////////////////////////////////////////////////////////////////////////

    //! Abstraction of interface for KBS
    //!
    //! Only one method, Fetch(...). 
    //! Can be overloaded, if there are more properties descriptors in KBS, e.g. for different types of property
    template<typename PROPERTY_DESCRIPTOR/*, typename T_COORDINATES=pro::Coordinates*/>
    class I4KBS
    {
    public:
			//virtual bool Fetch(PROPERTY_DESCRIPTOR* property); //OBSOLETE, should be removed!!!
			//! If called without where and when properties, it means "in point, you ask for modelId"!
      virtual bool Fetch(boost::shared_ptr<PROPERTY_DESCRIPTOR> property) = 0;
//			virtual void Fetch(boost::shared_ptr<PROPERTY_DESCRIPTOR> property, const com::Point<T_COORDINATES> *const point) = 0;
      virtual bool Fetch(boost::shared_ptr<PROPERTY_DESCRIPTOR> property, const PROPERTY_DESCRIPTOR& where, const PROPERTY_DESCRIPTOR& when) = 0;
		protected:
// 			virtual bool IsValueAvailible(const com::Point<T_COORDINATES> *const){return false;} // !=0;
// 			virtual void GetValue(boost::shared_ptr<PROPERTY_DESCRIPTOR> property, const com::Point<T_COORDINATES> *const){}; // !=0;

    };

		template<typename DATABUS, typename KBS>
		class Kbs 
			: public I4KBS<typename KBS::PropertyDescription>
		{
		public:
			typedef typename DATABUS::CoordinatesType CoordinatesType;
			typedef typename KBS::PropertyDescription PropertyDescription;

			Kbs(boost::shared_ptr<DATABUS> databus, KBS& kbs);
			virtual bool Fetch(boost::shared_ptr<typename KBS::PropertyDescription> property);
			virtual bool Fetch(boost::shared_ptr<typename KBS::PropertyDescription> property, const typename KBS::PropertyDescription& where, const typename KBS::PropertyDescription& when);
			template <typename VARIABLE,typename COORDINATES> mpr::ModelID GetModelId(mpt::Point<COORDINATES> *point)
			{
				ConvertToKbsTypes<VARIABLE,COORDINATES>(point);
				mId = kbs_.GetModelId(coordinates, time, typeTrait);
				ConvertToModelIdProperty(mId,&mIdProperty);
				return mIdProperty;
			}

		protected:
			typename KBS::PropertyDescription time, coordinates, typeTrait, mId;
			mpr::ModelID mIdProperty;			
			
			CoordinatesType am3coordinates;
			double am3time;

			boost::shared_ptr<DATABUS> databus_;
			KBS& kbs_;

			template <typename VARIABLE,typename COORDINATES> void ConvertToKbsTypes( mpt::Point<COORDINATES> * point )
			{
				ConvertToKbsTime(point->GetTime(),&time);
				ConvertToKbsCoordinates(point->Coordinates(),&coordinates);
				const VARIABLE* const typeTraitProperty = nullptr;
				ConvertToKbsTypeTrait(typeTraitProperty,&typeTrait);
			}
		};

//////////////////////////////////////////////////////////////////////////

		template<typename DATABUS, typename KBS>
		Kbs<typename DATABUS, typename KBS>::Kbs(boost::shared_ptr<DATABUS> databus, KBS& kbs): databus_(databus), kbs_(kbs)
		{
			kbs.SetDataSource(this);
		};


		template<typename DATABUS, typename KBS>
		bool Kbs<typename DATABUS, typename KBS>::Fetch(boost::shared_ptr<typename KBS::PropertyDescription> property)
		{
			return false;
			// TODO
		}

		template<typename DATABUS, typename KBS>
		bool Kbs<typename DATABUS, typename KBS>::Fetch(boost::shared_ptr<typename KBS::PropertyDescription> property, const typename KBS::PropertyDescription& where, const typename KBS::PropertyDescription& when)
		{
			bool toReturn = true;

			ConvertToAm3Coordinates(where,&am3coordinates);
			ConvertToAm3Time(when,&am3time);

			mpt::ComputedPoint<mpr::Coordinates2D> point(am3coordinates,am3time);
			QueryDatabus<PropertyDescription,DATABUS>::Query(*property,databus_, &point);
			return false;
		}


		// Old, probably will be not suitable

		//     //! Abstraction of interface for adapter
		//     //!
		//     //! Only one method, GetState(Point, ModelID). 
		//     //! Can be overloaded, if there are more properties in KBS.
		//     template <typename T_COORDINATES=pro::Coordinates,typename PROPERTY_DESCRIPTOR=pro::ModelID>
		//     class I4Adapter
		//     {
		//     public:
		//       typedef T_COORDINATES COORDINATES;
		//       typedef PROPERTY_DESCRIPTOR MODEL_ID;
		//       virtual ~I4Adapter(){};
		// //      virtual am3::model::ModelID GetState(com::Point *,const pro::ModelID *const )=0;
		//       virtual MODEL_ID GetState(const com::Point<T_COORDINATES> *const,const PROPERTY_DESCRIPTOR *const )=0;
		//     };

  } //knowledge
} //am3


//////////////////////////////////////////////////////////////////////////

// template<typename PROPERTY_DESCRIPTOR, typename T_COORDINATES>
// bool am3::knowledge::I4KBS<PROPERTY_DESCRIPTOR, T_COORDINATES>::Fetch( PROPERTY_DESCRIPTOR* property )
// {
// 	return false; //OBSOLETE, should be removed!!!!
// }


// template<typename PROPERTY_DESCRIPTOR, typename T_COORDINATES>
// void am3::knowledge::I4KBS<PROPERTY_DESCRIPTOR, T_COORDINATES>::Fetch( boost::shared_ptr<PROPERTY_DESCRIPTOR> property, const com::Point<T_COORDINATES> *const point )
// {
// 	if (IsValueAvailible(point)) GetValue(property,point);
// 	else lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"No such value for this point");
// }
// 
// template<typename PROPERTY_DESCRIPTOR, typename T_COORDINATES>
// bool am3::knowledge::I4KBS<PROPERTY_DESCRIPTOR, T_COORDINATES>::Fetch( boost::shared_ptr<PROPERTY_DESCRIPTOR> property, const PROPERTY_DESCRIPTOR& where, const PROPERTY_DESCRIPTOR& when ) /*should be pure !!!! */
// {
// 	return false;//should be pure virtual!!!!
// }

#endif // knowledge_kbsInterfaces_h__
