
/*! \file basicKbsInterfaces.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details Basic implementations of properties descriptors
*********************************************************************/
#ifndef knowledge_basicKbsInterfaces_h__
#define knowledge_basicKbsInterfaces_h__
/*	include *********************************************************/

#include <boost/lexical_cast.hpp>
#include <string>
#include "../models/dataStructures.h"
#include "kbsInterfaces.h"
#include "../kbsRebit/reasoningData.h"
#include "../logger/logger.h"

/*	using ***********************************************************/

namespace pro=am3::model;
namespace kno=am3::knowledge;
namespace lo=am3::logger;
using std::string;
using boost::lexical_cast;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace knowledge
  {
    template<typename ADAPTER>
    struct ConverterGeneralRebit : public knowledge::I4KBS<VariableDTO>
    {
      ADAPTER* adapter_;
      virtual ~ConverterGeneralRebit(){};
      bool Init(ADAPTER* adapter);

      static VariableDTO Convert(const pro::Viscosity& visc);

      virtual bool Fetch(VariableDTO* property);
      typedef VariableDTO BasePropertyDescriptor;

    };

  } //knowledge
} //am3
#endif // knowledge_basicKbsInterfaces_h__
