
/*! \file kbsRepresentation.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief   Representation of external reasoning services
 * \details 
*********************************************************************/
#ifndef knowledge_kbsRepresentation_h__ 
#define knowledge_kbsRepresentation_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/point.h"
#include "kbsInterfaces.h"

/*	using ***********************************************************/

namespace mpr=am3::model;
namespace mpt=am3::model::common;


/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace knowledge
  {
		template <typename T_MODELS_CONTAINER, typename T_COORDINATES=mpr::Coordinates,typename PROPERTY_DESCRIPTOR=mpr::ModelID>
		class KbsRepresentation: public I4Adapter<T_COORDINATES, PROPERTY_DESCRIPTOR>
		{
		public:
			bool IsKnowledgeProperForModel(const T_MODELS_CONTAINER& containerToIterate);
			PROPERTY_DESCRIPTOR GetModelId(const mpt::Point<T_COORDINATES>& point)=0;
			virtual ~KbsRepresentation(){};
		protected:
			//! Checks is there a possibility to get from KBS model id of particular model; if no, warning should be logged, but do not break a program
			virtual bool LacksRuleForModel(typename T_MODELS_CONTAINER::iterator)=0;
			//! Checks is there a model for each possible model id from KBS; if no, error should be logged and program stopped
			virtual bool IsAnyKbsModelNotCovered()=0;
			//! Checks is KBS connected; if no, error should be logged and program stopped
			virtual bool IsKbsConnectionSet()=0;
			//! Checks is proper interface initialized; if no, error should be logged and program stopped
			virtual bool IsI4AdapterSet()=0;
		};
  } //knowledge
} //am3
#endif // knowledge_kbsRepresentation_h__