#include "basicKbsInterfaces.h"

template<typename ADAPTER>
bool am3::knowledge::ConverterGeneralRebit<ADAPTER>::Init( ADAPTER* adapter )
{
	bool isSuccess=false;
	if (adapter!=nullptr)
	{
		adapter_=adapter;
		isSuccess=true;
	}
	return isSuccess;
}

template<typename ADAPTER>
VariableDTO am3::knowledge::ConverterGeneralRebit<ADAPTER>::Convert( const pro::Viscosity& visc )
{
	VariableDTO toReturn;
	*(toReturn.Id)="viscosity";
	return toReturn;
}

template<typename ADAPTER>
bool am3::knowledge::ConverterGeneralRebit<ADAPTER>::Fetch( VariableDTO* property )
{
	return false;
}
