#include "kbsRepresentation.h"

#include <boost/foreach.hpp>

#include "../logger/logger.h"


namespace mo=am3::model;
namespace lo=am3::logger;

template <typename T_MODELS_CONTAINER,typename T_COORDINATES,typename PROPERTY_DESCRIPTOR>
bool am3::knowledge::KbsRepresentation<T_MODELS_CONTAINER, T_COORDINATES, PROPERTY_DESCRIPTOR>::IsKnowledgeProperForModel( const T_MODELS_CONTAINER& containerToIterate )
{
	bool areRulesForAllModels = true;
	bool areCriticalsOK = true;
	BOOST_FOREACH(typename T_MODELS_CONTAINER::iterator it, containerToIterate)
	{
		if (LacksRuleForModel(it)) areRulesForAllModels=false;
	};
	if (IsAnyKbsModelNotCovered() )
	{
		lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Not all models in KBS exists in models container");
		areCriticalsOK=false;
	};
	if (IsKbsConnectionSet() )
	{
		lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"KBS connection not set");
		areCriticalsOK=false;
	}
	if (IsI4AdapterSet() )
	{
		lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Interface for adapter not set");
		areCriticalsOK=false;
	}
	return areCriticalsOK;
}

