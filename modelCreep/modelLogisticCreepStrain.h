
/*! \file modelLogisticCreepStrain.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelLogisticCreepStrain_h__
#define modelLogisticCreepStrain_h__
/*	include *********************************************************/

#include "../models/models.h"
#include "contracts.h"

#include <math.h>
/*	using ***********************************************************/

namespace ppc = am3::problem::creep;
namespace mo = am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace creep
    {
      // Based on Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
      // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
      //

      struct FStressPow
      {
        double FStress(const double& stress) const {return pow(stress,0.75);}
      };

      struct FStressLog
      {
        double FStress(const double& stress) const {return log10(stress);}
      };


      template<typename FSTRESS>
      class TimeToRuptureMansonBrown4: public FSTRESS
      {
      public:
        virtual double TimeToRupture(const double & temperature, const double & stress) const
        {
          return pow(10,LogTimeToRupture(temperature,stress));
        }
        virtual double LogTimeToRupture(const double & temperature, const double & stress) const
        {
          double lttr;
          lttr = P(stress);
          lttr *= pow((temperature+timeShift_-b0_)/1000,bn_);
          lttr += b1_;
          return lttr;
        }
      protected:
        TimeToRuptureMansonBrown4(const double a0, const double a1, const double a2, const double a3, const double a4, const double p0, const double p1, const double pn, const double timeShift): a0_(a0), a1_(a1), a2_(a2), a3_(a3), a4_(a4), b0_(p0), b1_(p1), bn_(pn), timeShift_(timeShift) {};
        double P(const double & stressInPa) const
        {
          double stressInMPa = stressInPa / 1e6;
          double fStress = FStress(stressInMPa);
          return a0_ + a1_ * fStress + a2_*pow(fStress,2) + a3_*pow(fStress,3) + a4_*pow(fStress,4);
        }


        const double a0_, a1_, a2_, a3_, a4_, b0_, b1_, bn_, timeShift_;
//        double FStress(const double& stress) const {return pow(stress,0.75);}
      };

      struct TimeToRuptureMansonBrown4P91EN
        : public TimeToRuptureMansonBrown4<FStressPow>
      {
        TimeToRuptureMansonBrown4P91EN(): TimeToRuptureMansonBrown4(-36.293,-0.239,-0.073,1.367e-3,-1.262e-5,545.0,10.12,2.5,273) {};
      };

      struct TimeToRuptureMansonBrown4P22
        : public TimeToRuptureMansonBrown4<FStressLog>
      {
        TimeToRuptureMansonBrown4P22(): TimeToRuptureMansonBrown4(-725.3813,1730.2198,-1575.0618,632.4288,-96.5729,600.0,8.27,1.7,273) {};
      };

      struct ModelLogisticCreepStrainCoefficientsP91
      {
        static const double  x1_, x2_, x3_, p1_, p2_, p3_, c_,temperatureShift_; 
      };

      const double ModelLogisticCreepStrainCoefficientsP91::x1_ = -2.24;
      const double ModelLogisticCreepStrainCoefficientsP91::x2_ = 1.37;
      const double ModelLogisticCreepStrainCoefficientsP91::x3_ = -3170;
      const double ModelLogisticCreepStrainCoefficientsP91::p1_ = 4.21;
      const double ModelLogisticCreepStrainCoefficientsP91::p2_ = -7.27;
      const double ModelLogisticCreepStrainCoefficientsP91::p3_ = 14200;
      const double ModelLogisticCreepStrainCoefficientsP91::c_ = 3.5;
      const double ModelLogisticCreepStrainCoefficientsP91::temperatureShift_ = 273.0;

      struct ModelLogisticCreepStrainCoefficientsP22
      {
        static const double  x1_, x2_, x3_, p1_, p2_, p3_, c_,temperatureShift_; 
      };

      const double ModelLogisticCreepStrainCoefficientsP22::x1_ = -0.391;
      const double ModelLogisticCreepStrainCoefficientsP22::x2_ = 0.696;
      const double ModelLogisticCreepStrainCoefficientsP22::x3_ = -3392.5;
      const double ModelLogisticCreepStrainCoefficientsP22::p1_ = 4.363;
      const double ModelLogisticCreepStrainCoefficientsP22::p2_ = -2.271;
      const double ModelLogisticCreepStrainCoefficientsP22::p3_ = 3874.9;
      const double ModelLogisticCreepStrainCoefficientsP22::c_ = 3.49;
      const double ModelLogisticCreepStrainCoefficientsP22::temperatureShift_ = 273.0;

      //////////////////////////////////////////////////////////////////////////

      template<typename COEFFICIENTS, typename TIME_TO_RUPTURE, typename T_COORDINATE>
      class ModelLogisticCreepStrain 
        : public ppc::contract::CreepRateContract
        , public mo::Inputs<ppc::contract::CreepBasicInputs>
        , private COEFFICIENTS
        //, public TIME_TO_RUPTURE
      {
      public:
        ModelLogisticCreepStrain(boost::shared_ptr<TIME_TO_RUPTURE> ttrModel): ttrModel_(ttrModel){};
        ppc::CreepRate GetState(am3::model::common::Point<T_COORDINATE> *,const ppc::CreepRate *const )
        {
          ppc::CreepRate toRet;
          double stress = Get<mpr::Stress,BaseCoordinates>::InPoint(point).Value();
          double temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point).Value();
          double strain = Get<mpr::TotalEffectiveStrain,BaseCoordinates>::InPoint(point).Value();
          toRet(CreepRate(temperature,stress, strain));
          return toRet;
        }

public:

        double TimeToStrain(const double & temperature, const double & stressInPa, const double strain) const
        {
          return pow(10,LogTimeToStrain(temperature,stressInPa,strain));
        }

        double LogTimeToStrain(const double & temperature, const double & stressInPa, const double strain) const
        {
          double p = P(temperature,stressInPa);
          double x0 = X0(temperature,stressInPa);
          double r1,r2,r3,r4;
          r1 = ttrModel_->LogTimeToRupture(temperature,stressInPa) + c_;
          r2 = pow(log10(strain)/x0,p);
          r3 = 1 + r2;
          r4 = (r1/r3) - c_;
          return r4;
        }

        virtual double CreepRate(const double & temperature, const double & stress, const double& strain) const
        {
          double k1;
          double k2;
          double ltf, ltf1, ltf2;
          double ttr = ttrModel_->TimeToRupture(temperature,stress);
          double tts = TimeToStrain(temperature,stress,strain);

          ltf1 = log10(ttr/3600)+c_;
          ltf2 = log10(tts/3600)+c_;
          //ltf = (log10(ttr)+c_)/(log10(tts)+c_);
          ltf = ltf1 / ltf2;
          double p = P(temperature,stress);
          double x0 = X0(temperature,stress);
          k1 = pow((ltf - 1),1/p)/p;
          k2 = (log10(ttr/3600)+c_) /
            (
              pow((log10(tts/3600)+c_),2) *
              tts *
              (ltf - 1)
            );
          
          return -(strain * k1 * k2 *x0);
        }



      private:
        mpt::Point<mpr::Coordinates2D> * point;

      protected:
        double X0(const double & temperature, const double & stress) const
        {
          return x1_+x2_*log10(stress/1e6)+x3_/(temperature+temperatureShift_);
        };

        double P(const double & temperature, const double & stress) const
        {
          return p1_+p2_*log10(stress/1e6)+p3_/(temperature+temperatureShift_);
        }

        boost::shared_ptr<TIME_TO_RUPTURE> ttrModel_;

      };
    }
  }
}

#endif // modelLogisticCreepStrain_h__
