
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef creepContracts_h__
#define creepContracts_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../models/contractMacros.h"
#include "../models/macrosSimplifiedModels.h"

/*	using ***********************************************************/

namespace moc=am3::model::contract;
namespace mpr=am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace problem
  {
    namespace creep
    {
      SIMPLE_PROP(CreepRate,double);
      SIMPLE_PROP(CreepRateDStrain,double);

      namespace contract
      {
        CONTRACT(		CreepRateContract			        	,1,mpr::Coordinates2D, CreepRate);
        CONTRACT(		CreepRateStrainContract		  		,2,mpr::Coordinates2D, CreepRate,CreepRateDStrain);
        CONTRACT(		CreepBasicInputs    	  		  	,3,mpr::Coordinates2D, mpr::Time,mpr::Temperature,mpr::Stress, mpr::TotalEffectiveStrain);
        CONTRACT(		CreepPrecipitationAmountInputs	,4,mpr::Coordinates2D, mpr::Time,mpr::Temperature,mpr::Stress, mpr::TotalEffectiveStrain, mpr::PrecipitationsAmount);
      }
    }
  }
}
#endif // creepContracts_h__
