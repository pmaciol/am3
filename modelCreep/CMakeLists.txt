
include_directories (../)
add_library (am3modelCreep
contracts.h
modelLogisticCreepStrain.h
modelCreep.cpp
creepRatePrecipitationAbe.h
)




SET_TARGET_PROPERTIES(am3modelCreep PROPERTIES LINKER_LANGUAGE CXX)
SET(CMAKE_EXE_LINKER_FLAGS /NODEFAULTLIB:\"LIBCMTD.lib;libcpmtd.lib\")