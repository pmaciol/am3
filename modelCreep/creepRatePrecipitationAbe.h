
/*! \file CreepRatePrecipitationAbe.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef CreepRatePrecipitationAbe_h__
#define CreepRatePrecipitationAbe_h__
/*	include *********************************************************/



/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace creep
    {
      template <typename T_TIME_TO_RUPTURE_MODEL, typename COEFFICIENTS>
      class TimeToRupturePrecipitationInternalAbeEquation : public COEFFICIENTS
        // Based on Creep and creep rate curves of a 10cr-30mn austenitic steel during carbide precipitation, 
        // Fujio Abe, Metallurgical and Materials Transactions A (Impact Factor: 1.73). 01/1995; 26(9):2237-2246. DOI: 10.1007/BF02671239 
      {
      public:
        TimeToRupturePrecipitationInternalAbeEquation(boost::shared_ptr<T_TIME_TO_RUPTURE_MODEL> ttrModel): ttrModel_(ttrModel){};
        virtual double LogTimeToRupture(const double & temperature, const double & stress) const
        {
          double lttr,lttrc;
          lttrc = (ttr1_ * exp(carbonContent_ * ttr2_)); //carbonContent in %wg
          lttr = ttrModel_->LogTimeToRupture(temperature, stress);
          return lttr*lttrc;
        }
        virtual double TimeToRupture(const double & temperature, const double & stress) const
        {
          return pow(10,LogTimeToRupture(temperature,stress));
        }
      protected:
        boost::shared_ptr<T_TIME_TO_RUPTURE_MODEL> ttrModel_;
      };
    
      //////////////////////////////////////////////////////////////////////////
      template <typename T_CREEP_RATE_MODEL, typename COEFFICIENTS>
      class CreepRatePrecipitation: public COEFFICIENTS
      {
      public:
        CreepRatePrecipitation(boost::shared_ptr<T_CREEP_RATE_MODEL> crModel): crModel_(crModel) {};
   

        virtual double CreepRate(const double & temperature, const double & stress, const double& strain) const
        {
          double tts = TimeToStrain(temperature,stress,strain)*3600;
          double correction = pow(10,PrecipAtomsAmount(tts,temperature));
          double creepRate = crModel_->CreepRate(temperature,stress,strain) / correction;
          return creepRate;
        }

        virtual double TimeToStrain(const double & temperature, const double & stress, const double& strain) const
      {
      return crModel_->TimeToStrain(temperature,stress,strain);
      }
        

          protected:
          // Equation by Abe, DOI: 10.1007/BF02671239 
        double PrecipAtomsAmount(const double & timeInSec, const double & temperature) const
        {
          return y0_ * (1 - exp(-pow((k_ * timeInSec),n_)));
        }
        boost::shared_ptr<T_CREEP_RATE_MODEL> crModel_;
      };
      //////////////////////////////////////////////////////////////////////////

      struct CreepRatePrecipitationCoefficients
      {
        static const double y0_,k_,n_;
      };

      // Abe, 10Cr-30Mn, 0.26C
      const double CreepRatePrecipitationCoefficients::y0_ = 1.2;
      const double CreepRatePrecipitationCoefficients::k_ = 3e-6 /*1/s*/;
      const double CreepRatePrecipitationCoefficients::n_ = 2.0/3.0;
      
      struct TimeToRupturePrecipitationCoefficients
      {
        static const double ttr1_,ttr2_, carbonContent_;
      };

      const double TimeToRupturePrecipitationCoefficients::ttr1_ = 0.9205/*0.0953*/;
      const double TimeToRupturePrecipitationCoefficients::ttr2_ = 10.324/*0.5804*/;
      const double TimeToRupturePrecipitationCoefficients::carbonContent_ = 0.01;
      

    }

  }
}
#endif // CreepRatePrecipitationAbe_h__
