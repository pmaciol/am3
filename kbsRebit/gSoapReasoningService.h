/*! \file gSoapReasoningService.h **************************************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#ifndef gSoapReasoningService_H
#define gSoapReasoningService_H

#include "soapBasicHttpBinding_USCOREIRebitEngineServiceProxy.h"
#include "reasoner.h"

using namespace std;

namespace am3
{
	namespace gsoap
	{
		/* GSoap proxy to Rebit reasoning service */
		class GSoapReasoningService : public ReasoningServiceProxy
		{
			public:
				virtual bool Hello();

				virtual ResponseDTO *OpenSession(string& xml, string ruleSetId);
				virtual ResponseDTO *SetVariableValue(string *sessionId, vector<VariableDTO> *varVector);
				virtual ResponseDTO *SetFinalVariable(string *sessionId, string *variableId);
				virtual ResponseDTO *MixDeduction(string *sessionId);
				virtual ResponseDTO *RemoveSession(string *sessionId);

				// ctor
				GSoapReasoningService(string serviceUri);
	
			protected:
				string	ReasoningEndPoint;	// adres us�ugi
				BasicHttpBinding_USCOREIRebitEngineServiceProxy	*Svc;	// proxy us�ugi

				VariableDTO *MapToDTO(ns4__VariableDTO * varGSoap);
				vector<VariableDTO> *MapToDTOVector(std::vector<class ns4__VariableDTO *> &varArray);
				vector<string> *MapToStringVector(VariableDTO *varDTO);
		};
	} // gsoap
} // am3

#endif