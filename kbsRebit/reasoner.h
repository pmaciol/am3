/*! \file reasoner.h **************************************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#ifndef reasoner_H
#define reasoner_H

#include "reasoningData.h"

using namespace std;

namespace am3
{
	namespace gsoap
	{
		/* general logger interface */
		class Logger
		{
		public:
			virtual void LogResult(string result) = 0;
		};

		/* concrete logger class - prints results on the screen */
		class ScreenLogger : public Logger
		{
		public:
			virtual void LogResult(string result);
		};

		/* concrete logger class - do nothing */
		class NullLogger : public Logger
		{
		public:
			virtual void LogResult(string result);
		};

		/* general proxy to Rebit reasoning service */
		class ReasoningServiceProxy
		{
			public:
				virtual bool Hello() = 0;
				virtual ResponseDTO *OpenSession(string& xml, string ruleSetId) = 0;
				virtual ResponseDTO *SetFinalVariable(string *sessionId, string *variableId) = 0;
				virtual ResponseDTO *MixDeduction(string *sessionId) = 0;
				virtual ResponseDTO *SetVariableValue(string *sessionId, vector<VariableDTO> *varVector) = 0;
				virtual ResponseDTO *RemoveSession(string *sessionId) = 0;
		};

		/* general class responsible for inference process */
		class Reasoner
		{
			protected:
				vector<VariableDTO> *variableHolder;	// holds variable used in the inference process (these variables should be set before starting inference process)
				ReasoningServiceProxy *ReasoningProxy;		// gSoap (or other) proxy to Rebit reasoning service
				Logger *MessageLogger;

				// A callback methods used in inference process when the value of a new variable (fact) is required.
				// It calls TemplateGetVariableValue() to retrieve variable values (it works as in template method pattern).
				VariableDTO *GetVariableValue(string varId);

				// An internal callback methods that retrieves values of new variables (facts) from internal variable holder
				// Override this method if you want to provide your own procedure for retrieving (or calculating) values of required variables.
				virtual vector<string> *TemplateGetVariableValue(string varId);

			public:
				// ctor
				Reasoner(ReasoningServiceProxy *reasoningService, Logger *logger) 
					: ReasoningProxy(reasoningService), MessageLogger(logger), variableHolder(new vector<VariableDTO>)
				{ }

				// Stores single-valued variables (facts) in internal variable holder
				// The stored  variable is then used in inference process.
				// This method should be used in static cases, 
				// i.e. the value of potential variables used in inference process is known 
				// (or can be easely calulated) before starting inference process
				void SetVariable(string  *varId, string  *varValue);

				// Stores multiple-valued variables (facts) in internal variable holder
				// The stored  variable is then used in inference process.
				// This method should be used in static cases, 
				// i.e. the value of potential variables used in inference process is known 
				// (or can be easely calulated) before starting inference process
				void SetVariable(string *varId, vector<string> *varValues);

				// Starts inference process on REBIT
				ResponseDTO *Run(string & xml, string ruleSetId, string finalVariableId);
		};
	} // reasoner
} // am3

#endif