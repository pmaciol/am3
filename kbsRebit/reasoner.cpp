/*! \file reasoner.cpp *********************************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#include "reasoner.h"
#include "gSoapReasoningService.h"

/* static methods - reads knowledgebase from a file */
string am3::gsoap::GetXmlFromFile(string path)
{
	ifstream inFile;

	inFile.open(path);
	string line;
	string result;

	if (inFile.is_open())
	{
		while (!inFile.eof())
		{
			getline(inFile,line);
			result += line;
		}
	}
	inFile.close();

	return result;
}


////ctor
//am3::gsoap::Reasoner::Reasoner(ReasoningServiceProxy *reasoningService, Logger *logger)
//{
//	this->MessageLogger = logger;
//	this->ReasoningProxy = reasoningService;
//	this->variableHolder = new std::vector<VariableDTO>();
//}


/* starts and manages the inference process 
Args:
	ruleSetId	- ruleset identifier
	finalVariableId	- idefifier of final variable (the goal of the reasoning)

Results:
	1) resposneDTO = NULL								- error - session is not open
	2) resposneDTO != NULL i resposneDTO->Status != 0	- session is open, but error in inference process
	3) resposneDTO != NULL i resposneDTO->Status = 0	- session is open and inference process finished without errors

	In case 3: 
		resposneDTO->InteractionVariables holds the result of inference process, i.e. the value and id of final variable
*/
am3::gsoap::ResponseDTO *am3::gsoap::Reasoner::Run(string & xml, string ruleSetId, string finalVariableId)
{
	ResponseDTO *resp = NULL;

	// open session
	this->MessageLogger->LogResult("Operation: OpenSession");
	resp = this->ReasoningProxy->OpenSession(xml, ruleSetId);
	this->MessageLogger->LogResult(*resp->ToString());
	if (*resp->Status != 0)
		return resp;
	
	// get sessionId
	std::string sessionId(*resp->SessionId);

	// set final variable
	this->MessageLogger->LogResult("Operation: SetFinalVariable");
	resp = this->ReasoningProxy->SetFinalVariable(&sessionId, &finalVariableId);
	this->MessageLogger->LogResult(*resp->ToString());
	if (*resp->Status != 0)
		return resp;
		
	do
	{
		// KROK 3.1: execute one step of inference process
		this->MessageLogger->LogResult("Operation: MixDeduction");
		resp = this->ReasoningProxy->MixDeduction(&sessionId);
		this->MessageLogger->LogResult(*resp->ToString());
		if (*resp->Status != 0)	// error
			break;

		if ((*resp->SessionState != SessionStateDTO(Running)) || (resp->InteractionVariables->size() <= 0))
			break;

		// get id of required variable
		string varId = *(*resp->InteractionVariables)[0].Id;

		// Get the value of requierd variable (e.g. from callback)
		this->MessageLogger->LogResult("Operation: GetVariableValue");
		VariableDTO *varDTO = this->GetVariableValue(varId);
		this->MessageLogger->LogResult(*varDTO->ToString());
		if (varDTO->Value == NULL)
		{
			this->MessageLogger->LogResult("!!! Could not find value for the variable: " + varId + " !!!\n");
			break;
		}
		vector<VariableDTO> *varVector = new vector<VariableDTO>();
		varVector->push_back(*varDTO);

		// Send values of required variables to inference engine. 
		this->MessageLogger->LogResult("Operation: SetVariableValue");
		resp = this->ReasoningProxy->SetVariableValue(&sessionId, varVector);
		this->MessageLogger->LogResult(*resp->ToString());
		if (*resp->Status != 0)	// b��d
			break;

	} while (true);
	
	// remove session
	this->MessageLogger->LogResult("Operation: RemoveSession");
	this->ReasoningProxy->RemoveSession(&sessionId);
	this->MessageLogger->LogResult(*resp->ToString());
	return resp; 
}


// A callback methods used in inference process when the value of a new variable (fact) is required.
// It calls TemplateGetVariableValue() to retrieve variable values (it works as in template method pattern).
am3::gsoap::VariableDTO *am3::gsoap::Reasoner::GetVariableValue(string varId)
{
	VariableDTO *varDTO = new VariableDTO();
	varDTO->Id = new string(varId);

	vector<string> *result = this->TemplateGetVariableValue(varId);
	if (result != NULL)
		varDTO->Value = result;

	return varDTO;
}


// An internal callback methods that retrieves values of new variables (facts) from internal variable holder
// Override this method if you want to provide your own procedure for retrieving (or calculating) values of required variables.
vector<string> *am3::gsoap::Reasoner::TemplateGetVariableValue(string varId)
{
	VariableDTO *varDTO;

	int size = this->variableHolder->size();
	for(int i = 0; i < size; i++)
	{
		varDTO = &(*this->variableHolder)[i];
		if (*varDTO->Id == varId)
			return varDTO->Value;
	}

	return NULL;
}

// Stores a single-value variable in the internal variable holder.
// The stored  variable is then used in inference process.
// This method should be used in static cases, 
// i.e. the value of potential variables used in inference process is known 
// (or can be easely calulated) before starting inference process
void am3::gsoap::Reasoner::SetVariable(string * varId, string *varValue)
{
	VariableDTO *varDTO = new VariableDTO();
	varDTO->Id = varId;
	varDTO->Value = new vector<string>(1);
	(*varDTO->Value)[0] = *varValue;

	this->variableHolder->push_back(*varDTO);
}

// Stores a multiple-value variable in the internal variable holder.
// The stored  variable is then used in inference process.
// This method should be used in static cases, 
// i.e. the value of potential variables used in inference process is known 
// (or can be easely calulated) before starting inference process
void am3::gsoap::Reasoner::SetVariable(string *varId, vector<string> *varValues)
{
	VariableDTO *varDTO = new VariableDTO();
	varDTO->Id = varId;
	*varDTO->Value = *varValues;

	this->variableHolder->push_back(*varDTO);
}

