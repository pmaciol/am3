//Description:

// This project is a gSoap implementation of 'ReasoningServiceProxy'.
// It should be compiled as an ordinary lib. 
// It provides classes for a client of SOAP implementation of REBIT engine (ConsoleRebitSoapHost.exe). 
// The file kbsRebit_test.cpp in projects am3tests contains 3 tests based on thermostat_KB.xml: 
// connection test and static and dynamic data provider tests. 
 

// To build and test:

//* find the file 'EnginegSoapSettings.config' in REBIT directory, open it and set the address of REBIT service in 'Uri' property 
//		e.g.: Uri="http://localhost:8888/RebitEngineService"
//		(if you set different address change the the following steps in the 'file kbsRebit_test.cpp': 
//		'string serviceEndPoint = "http://localhost:8888/RebitEngineService"' to the proper address)
//* download the file 'thermostat_KB.xml' and save it on C:\ 
//		(or change the following steps in the the file 'kbsRebit_test.cpp': 
		'string thermostatPath = "c:/thermostat_KB.xml";' to the proper path)
//* build kbsRebit and tests projestcs
//* run ConsoleRebitPInvokeHost.exe as an administrator
//* run test.exe

