/*! \file gSoapReasoningService.cpp *********************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#include "gSoapReasoningService.h"

namespace am3
{
	namespace gsoap
	{
		/* ---------------------------------------------- */
		/* ----------- constructor section -------------- */
		/* ---------------------------------------------- */

		GSoapReasoningService::GSoapReasoningService(string serviceUri) 
		{
			this->ReasoningEndPoint = serviceUri;
			this->Svc = new BasicHttpBinding_USCOREIRebitEngineServiceProxy(this->ReasoningEndPoint.c_str());
		}


		/* ---------------------------------------------- */
		/* ------------- public section ----------------- */
		/* ---------------------------------------------- */

		/* Tests gSoap connection with REBIT */
		bool GSoapReasoningService::Hello()
		{
			bool result = false; 

			_ns1__Hello reqHello;
			_ns1__HelloResponse rspHello;
			
			string message("aqq");
			reqHello.message = &message;
			int hr = this->Svc->Hello( &reqHello, &rspHello );
			if (( hr == SOAP_OK ) && (strcmp(rspHello.HelloResult->c_str(), message.c_str()) == 0))
				result = true;
			
			return result;
		}

		/* Opens session in a given inference mode
		Args: 
			xml			- knowledge base in xml format
			ruleSetId	- ruleset identifier
			sessionType	- session type

		Results:
			responseDTO->Status			- error code:
											< 0 - Rebit service or gSoap error
											= 0 - no error
											> 0 - client side error
			responseDTO->SessionState	- session state
			responseDTO->SessionId		- session identifier. It is correct only if responseDTO->Status = 0 */
		ResponseDTO *GSoapReasoningService::OpenSession(string& xml, string ruleSetId)
		{
			ResponseDTO *rsp = new ResponseDTO(); 

			// knowledgebase
			ns6__KnowledgeBaseItemDTO kbDTO;
			string krDTOId("1");
			string kbDTOName("External Project Test - Name");
			string kbDTODescription("External Project Test - Description");
			string kbDTOAuthor("External Project Test - Author");
			string kbDTOVersion("External Project Test - Version");
			time_t kbDTOCreationDateTime = time(NULL);
			time_t kbDTOLastModifiedDateTime = time(NULL);

			kbDTO.Id = &krDTOId;
			kbDTO.Description = &kbDTODescription;
			kbDTO.Author = &kbDTOAuthor;
			kbDTO.Version = &kbDTOVersion;
			kbDTO.CreationDateTime = &kbDTOCreationDateTime;
			kbDTO.LastModifiedDateTime = &kbDTOLastModifiedDateTime; 

			// ruleset
			ns6__RuleSetItemDTO rsDTO;

			string rsDTODescription("External Project Test - Ruleset Desc");
			ns6__RuleSetCategoryDTO rsDTORuleSetCategory = ns6__RuleSetCategoryDTO__UserDefined;

			rsDTO.Id = &ruleSetId;
			rsDTO.Description = &rsDTODescription;
			rsDTO.RuleSetCategory = &rsDTORuleSetCategory;
	
			_ns1__OpenReasoningSession			reqOpenReasoningSession;
			_ns1__OpenReasoningSessionResponse	rspOpenReasoningSession;

			// forces mix mode
			ns4__SessionTypeDTO reqReasoningMode = ns4__SessionTypeDTO__FullReasoning;

			reqOpenReasoningSession.packageXML = &xml;
			reqOpenReasoningSession.ruleSet = &rsDTO;
			reqOpenReasoningSession.knowledgeBase = &kbDTO;
			reqOpenReasoningSession.reasoningMode = reinterpret_cast<ns4__SessionTypeDTO *>(&reqReasoningMode);
	
			// opens session in AllYouCanGetReasoning mode (mix???)
			*rsp->Status = this->Svc->OpenReasoningSession(&reqOpenReasoningSession, &rspOpenReasoningSession);
			if ( *rsp->Status != SOAP_OK )
			{
				return rsp;
			}
	
			// get session idenfifier
			rsp->SessionId = new string(*rspOpenReasoningSession.OpenReasoningSessionResult->Id);

			// get session state
			rsp->SessionState = reinterpret_cast<SessionStateDTO *>(rspOpenReasoningSession.OpenReasoningSessionResult->State);

			return rsp;
		}

		/* Sends values of new variable to Rebit
		Args:
			sessionId	- session identifier
			varVector	- vector of variable to be sent (Id and Value of all variables must be set. Other properties are not used)
							
		Results:
			responseDTO->Status			- error code:
											< 0 - Rebit service or gSoap error
											= 0 - operation OK, no error
											> 0 - client side error
			responseDTO->SessionState	- session state
			responseDTO->SessionId		- session identifier. It is correct only if responseDTO->Status = 0 */
		ResponseDTO *GSoapReasoningService::SetVariableValue(string *sessionId, vector<VariableDTO> *varVector)
		{
			ResponseDTO *resp = new ResponseDTO();

			VariableDTO	*varDTO;
			ns5__ArrayOfstring *varValue;
			_ns5__ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1_KeyValueOfstringArrayOfstringty7Ep6D1 *variable;

			int size = varVector->size();
			vector<_ns5__ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1_KeyValueOfstringArrayOfstringty7Ep6D1> variables(size);

			for(int i = 0; i < size; i++)
			{
				varDTO = &(*varVector)[i];
				varValue = new ns5__ArrayOfstring();

				varValue->string = *MapToStringVector(varDTO);

				variable = new _ns5__ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1_KeyValueOfstringArrayOfstringty7Ep6D1();

				variable->Key = varDTO->Id;
				variable->Value = varValue;

				variables[i] = *variable;
			}

			// collection of variables
			ns5__ArrayOfKeyValueOfstringArrayOfstringty7Ep6D1 variablesCollection;
			variablesCollection.KeyValueOfstringArrayOfstringty7Ep6D1 = &variables;

			_ns1__SetVariableValues reqSetVariableValues;
			_ns1__SetVariableValuesResponse rspSetVariableValuesResponse;

			reqSetVariableValues.sessionId = sessionId;
			reqSetVariableValues.variableValues = &variablesCollection;

			*resp->Status = this->Svc->SetVariableValues(&reqSetVariableValues, &rspSetVariableValuesResponse);
			if (*resp->Status != SOAP_OK )
			{
				return resp;
			}

			// sessionId
			resp->SessionId = new string(*sessionId);

			// gets session state
			resp->SessionState = reinterpret_cast<SessionStateDTO *>(rspSetVariableValuesResponse.SetVariableValuesResult->State);

			return resp;
		}



		/* 
		Sets variable passed by the user as a final variable (the goal of inference)
		Args:
			sessionId	- session identifier
			variableId	- final variable identifier

		Results:
			responseDTO->Status			- error code:
											< 0 - Rebit service or gSoap error
											= 0 - operation OK, no error
											> 0 - client side error
			responseDTO->SessionState	- session state
			responseDTO->SessionId		- session identifier. It is correct only if responseDTO->Status = 0 
		*/
		ResponseDTO *GSoapReasoningService::SetFinalVariable(string *sessionId, string *variableId)
		{
			ResponseDTO *resp = new ResponseDTO();

			_ns1__SetFinalVariable reqSetFinalVariable;
			_ns1__SetFinalVariableResponse respSetFinalVariableResponse;

			reqSetFinalVariable.sessionId = sessionId;
			reqSetFinalVariable.variableId = variableId;

			*resp->Status = this->Svc->SetFinalVariable(&reqSetFinalVariable, &respSetFinalVariableResponse);
			if ( *resp->Status != SOAP_OK )
			{
				return resp;
			}
	
			// sessionId
			resp->SessionId = new string(*sessionId);

			// get session state
			resp->SessionState = reinterpret_cast<SessionStateDTO *>(respSetFinalVariableResponse.SetFinalVariableResult->State);

			return resp;
		}


		/* 
		Executes one step of inference process
		Args:
			sessionId	- session id
	
		Results:
			responseDTO->Status			- error code:
											< 0 - Rebit service or gSoap error
											= 0 - operation OK, no error
											> 0 - client side error
			responseDTO->SessionState	- session state
			responseDTO->SessionId		- session identifier. It is correct only if responseDTO->Status = 0 
	
			if responseDTO-SessionState == Closed then responseDTO->InteractionVariables hold the result of inference, 
			i.e. the final variable value

			if ResponseDTO-SessionState == Running then responseDTO->InteractionVariables hold the variable required by inference process. 
			The value of this variable should be entered in next step
		*/
		ResponseDTO *GSoapReasoningService::MixDeduction(string *sessionId)
		{
			ResponseDTO *resp = new ResponseDTO();

			_ns1__MixDeduction reqMixDeduction; 
			_ns1__MixDeductionResponse rspMixDeduction;

			reqMixDeduction.sessionId = sessionId;
	
			*resp->Status = this->Svc->MixDeduction(&reqMixDeduction, &rspMixDeduction);
			if ( *resp->Status != SOAP_OK )
			{
				return resp;
			}

			// sessionId
			resp->SessionId = new std::string(*sessionId);

			// get session state and gSoap variables
			resp->SessionState = reinterpret_cast<SessionStateDTO*>(rspMixDeduction.MixDeductionResult->State);
			vector<class ns4__VariableDTO *> varArray;
	
			// test if session is running
			if (*resp->SessionState == SessionStateDTO(Running))
			{
				// ask for new variables
				varArray = rspMixDeduction.MixDeductionResult->DeductionVariables->VariableDTO;

				// map gSoap to DTO variables
				resp->InteractionVariables = MapToDTOVector(varArray);
			}
			else if (*resp->SessionState == SessionStateDTO(Closed))
			{
				// session is closed - show results
				varArray = rspMixDeduction.MixDeductionResult->ResultVariables->VariableDTO;

				// map resulting gSoap  to DTO
				resp->InteractionVariables = MapToDTOVector(varArray);
			}

			return resp;
		}

		/* Removes a session
		Argus:
			sessionId	- session identifier
	
		Wyniki:		
			Results:
			responseDTO->Status			- error code:
											< 0 - Rebit service or gSoap error
											= 0 - operation OK, no error
											> 0 - client side error
			responseDTO->SessionState	- session state
			responseDTO->SessionId		- session identifier. It is correct only if responseDTO->Status = 0 
		*/ 
		ResponseDTO *GSoapReasoningService::RemoveSession(string *sessionId)
		{
			ResponseDTO *resp = new ResponseDTO();

			_ns1__RemoveSession reqRemoveSession;
			_ns1__RemoveSessionResponse rspRemoveSession;

			reqRemoveSession.sessionId = sessionId;

			*resp->Status = this->Svc->RemoveSession(&reqRemoveSession, &rspRemoveSession);
			if ( *resp->Status != SOAP_OK )
			{
				return resp;
			}

			// sessionId
			resp->SessionId = new std::string(*sessionId);

			return resp;
		}


		/* ---------------------------------------------- */
		/* ----------- protected section ---------------- */
		/* ---------------------------------------------- */

		/* mapping gSoap variable to DTO variable */
		VariableDTO *GSoapReasoningService::MapToDTO(ns4__VariableDTO * varGSoap)
		{
			// create DTO variable
			VariableDTO *varDTO = new VariableDTO();

			varDTO->Id = varGSoap->Id;
			
			// single type variables
			if (*varGSoap->Multiplicity == ns4__VariableMultiplicity__Single)
			{
				ns4__SingleVariableDTO *singleVariable = (ns4__SingleVariableDTO *)varGSoap;

				// set value
				if (singleVariable->VarValue != NULL)
				{
					varDTO->Value = new std::vector<string>(1);
					string *v = new string(*singleVariable->VarValue);
					(*varDTO->Value)[0] = *v;
				}
			}
			// multiple variables!!!
			else
			{
				VariableDTO *tempVarDTO;
				ns4__MultipleVariableDTO *multipleVariable = (ns4__MultipleVariableDTO *)varGSoap;
				vector<class ns4__SingleVariableDTO * > q = multipleVariable->Questions->SingleVariableDTO;
				int size = q.size();
				varDTO->Value = new vector<string>(size);
				for(int i = 0; i < size; i++)
				{
					ns4__SingleVariableDTO *internalSingleVariable = (ns4__SingleVariableDTO *)q[i];
					if (internalSingleVariable->VarValue != NULL)
					{
						string *v = new string(*internalSingleVariable->VarValue);
						(*varDTO->Value)[i] = *v;
					}
				}
			}
		
			return varDTO;
		}

		/* mapping of vector of gSoap variables to vector of DTO variables*/
		vector<VariableDTO> *GSoapReasoningService::MapToDTOVector(vector<class ns4__VariableDTO *> &varArray)
		{
			vector<VariableDTO> *varVector = new vector<VariableDTO>();
			int size = varArray.size();
			for(int i = 0; i < size; i++)
			{
				// map gSoap toa DTO
				VariableDTO *varDTO = MapToDTO(varArray[i]);

				// add DTO to result
				varVector->push_back(*varDTO);
			}

			return varVector;
		}

		/* mapping DTO variable values to string vector */
		vector<std::string> *GSoapReasoningService::MapToStringVector(VariableDTO *varDTO)
		{
			// single type variable
			if ((varDTO->Value != NULL) && (varDTO->Value->size() == 1))
			{
				vector<string> *v = new vector<string>(1);
				(*v)[0] = (*varDTO->Value)[0];

				return v;
			}
			// multiple variables
			else if ((varDTO->Value != NULL) && (varDTO->Value->size() > 1))
			{
				//int size = varDTO->IntVars->size();
				vector<string> *v = varDTO->Value;

				return v;
			}

			return NULL;
		}
	} // gsoap
} // am3

