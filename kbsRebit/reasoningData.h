/*! \file reasoningData.h **************************************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#ifndef reasoningData_H
#define reasoningData_H

#include "stdafx.h"
#include <boost/lexical_cast.hpp>

using namespace std;

namespace am3
{
	namespace gsoap
	{
		// reads knowledgbase from xml file
		string GetXmlFromFile(std::string path);

		/* reasoning session state */
		enum SessionStateDTO 
		{
			Created = 0, 
			Open = 1, 
			Starting = 2, 
			Running = 3, 
			Closed = 4, 
			Failed = 5
		};


		/* represents variable (fact) in the inference process */
		struct VariableDTO
		{
			public:
            VariableDTO():Id(nullptr),Value(nullptr){};
				string *Id;					/* variable identifier */
				vector<string> *Value;	/* variable value -  */

				//ctor
// 				VariableDTO() : Id(NULL), Value(NULL)
// 				{ }
				
				// Builds a string descripion according to the following pattern:
				// Variable Id: thermostatSettings
				//		[0] = 2
				//		[1] = 1
				//		[2] = 9
				string *ToString()
				{
					string *result = new string();
					if (this->Id != NULL)
					{
						*result += "Variable Id: " + *Id + "\n";
						if (this->Value != NULL)
						{
							int size = this->Value->size();
							for (int i = 0; i < size; i++)
							{
								*result += "\t[" + boost::lexical_cast<string>(i) + string("] = ")  + (*this->Value)[i] +  "\n";
							}
						}
					}

					return result;
				}
		};


		/* represents result of operations on reasoning service */
		struct ResponseDTO
		{
			public:
				int *Status;								// return status - 0 - means OK, <>0 - means error
				string *SessionId;							// session identifier
				SessionStateDTO *SessionState;				// session state 
				vector<VariableDTO> *InteractionVariables;	// results variables

				//ctor
				ResponseDTO() : Status(new int(-1)), SessionState(new SessionStateDTO(Failed)), SessionId(NULL), InteractionVariables(NULL)
				{ }

				// Builds a string descripion according to the following pattern:
				// 
				// Operation Status: 0
				// Session Id: 2343456433232
				// Session State: Running
				// Session Result:
				// Variable Id:
				//		[0] = 2
				//		[1] = 1
				//		[2] = 9
				string *ToString()
				{
					string *result = new string();
					if (Status == NULL)
						return result;

					*result += "Operation Status: " + boost::lexical_cast<string>(*Status) + "\n";

					if (SessionId == NULL)
						return result;
					
					*result += "Session Id: " + *SessionId + "\n";

					if (SessionState == NULL)
						return result;

					*result += "Session State: " + boost::lexical_cast<string>(*SessionState) + "\n";

					if ((InteractionVariables == NULL))
						return result;

					*result += "Session Results: \n";

					int size = InteractionVariables->size();
					for (int i = 0; i < size; i++)
						*result += *((*InteractionVariables)[i].ToString());

					return result;
				}
		};
	} // gsoap
} // am3

#endif