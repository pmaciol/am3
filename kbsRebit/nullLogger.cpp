/*! \file nullLogger.cpp **************************************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#include "reasoner.h"

/* a concrete logger class */
void am3::gsoap::NullLogger::LogResult(string data)
{
	//does nothing
}