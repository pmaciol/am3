/*! \file screenLogger.cpp **************************************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#include "reasoner.h"

/* a concrete logger class - prints results on the screen */
void am3::gsoap::ScreenLogger::LogResult(string data)
{
	cout << data << "\n";
}