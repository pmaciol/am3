/*! \file main.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief  Example of am3 realization
 * \details All elements, necessary to design simple multiscale model are described here
*********************************************************************/

/*	include *********************************************************/
// 
// #include "../adapterIonExchange/adapterIonExchange.h"
// #include "../adapterIonExchange/proxyIonExchange.h"
// 
// // ModelAdina
// #include "../adinaModels/adinaFControls.h"
// 
// #include "../basicModels/rebitInterface.h"
// #include "kbs.h"
// 
// #include "../adinaServer/adinaSocketServer.h"
// #include "../adinaServer/adinaFprocessor.h"
// 
// #include "../adinaServer/storageAdina.h"
// #include "../modelAdinaSimple/adapter.h"
// #include "../modelAdinaSimple/proxy.h"
// #include "../modelAdinaSimple/problemDependentDataStructures.h"
// 
// 
// #include "../models/point.h"
// 
// /*	using ***********************************************************/
// 
// namespace adi=am3::model::adina;
// namespace sto=am3::storage;
// 
// /*	definitions *********************************************************/
// 
// //@{ \name globals_for_program_options Globals
// //! Used by boost::program_options implementation of program options
// extern int*  global_argc;
// extern char ***global_argv;
// //@}
// 
// /*	classes *********************************************************/
// 
// int main(int argc, char **argv)
// {
// 
//   //Passing argv to progam_options
//   global_argc=&argc;
//   global_argv=&argv;
// 
//   //Initialization od program_options, reading config files (check am3.conf in main binary directory)
//   kProgramOptions.Init();
// 
// //    Simulation simAdina;
// //    Manager manAdina;
// 
// 
//   am3::knowledge::ionex::IonExchangeKBS ioExKbs;
//   am3::adapter::ionex::IonExchangeAdapter ionExchangeAdapter;
// 
// 
//     // With example
//   am3::adapter::ionex::IonExchangeAdapterProxy<
//     kno::ionex::IonExchangeKBS, 
//     kno::ionex::IonExchangeGeneralRebit<
//       am3::adapter::ionex::IonExchangeAdapter
//     > 
//   > ionExchangeAdapterProxy(&ionExchangeAdapter,&ioExKbs);
// 
// 
//    am3::model::ModelAdina ma;
//    boost::shared_ptr<sto::AdinaFStorage> storage(new sto::AdinaFStorage);
// 
//    ionExchangeAdapter.Init(storage);
// 
// 
//    ma.Init(&ionExchangeAdapter,storage);
//    ma.Run();
// 
// }