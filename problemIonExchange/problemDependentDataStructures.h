
/*! \file problemDependentDataStructures.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details  Definitions of Properties and its Providers used in problem
*********************************************************************/
#ifndef adapterIonExchange_problemDependentDataStructures_h__
#define adapterIonExchange_problemDependentDataStructures_h__
/*	include *********************************************************/

#include "../models/contracts.h"

/*	using ***********************************************************/

namespace con=am3::model::contract;
namespace mod=am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace ionExchange
    {
      struct ZnIIRatioInWater : public SimpleProperty<double,mod::HaveGetSetValue> {};
      struct ZnIIRatioInWaterProvider
        :public con::Provider<ZnIIRatioInWater> {virtual ~ZnIIRatioInWaterProvider(){};};
      struct ZnIIRatioInOrganic : public SimpleProperty<double,mod::HaveGetSetValue> {};
      struct ZnIIRatioInOrganicProvider
        :public con::Provider<ZnIIRatioInOrganic> {virtual ~ZnIIRatioInOrganicProvider(){};};
    } //ionExchange
  } //model
} //am3

#endif // adapterIonExchange_problemDependentDataStructures_h__
