
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       Main header for ion exchange problem
 * \details   	
*********************************************************************/

#ifndef problemIonExchange_contracts_h__
#define problemIonExchange_contracts_h__

/*	include *********************************************************/

#include <vector>
#include "../models/contracts.h"
#include "../models/dataStructures.h"
#include "../models/description.h"

/*	using ***********************************************************/

namespace moc=am3::model::contract;
namespace mpr=am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace problem
	{
		namespace ionExchange
		{
			namespace contracts
			{


				/** \name Dispersed flows */

				struct SingleFluid;


				struct Component
				{
					SingleFluid* component;
					double ratio;
				};

				struct ContinousComponent : public mpr::SimpleProperty<Component,mpr::HaveGetSetValue > {};
				struct DisperesedComponent : public mpr::SimpleProperty<Component,mpr::HaveGetSetValue > {};
				struct SauterMeanDiameter : public mpr::SimpleProperty<double,mpr::HaveGetSetValue > {};
				struct TotalInterphacialArea : public mpr::SimpleProperty<double,mpr::HaveGetSetValue > {};

				/*@}*/

				struct RatioSourcesSet : public mpr::SimpleProperty<std::vector<mpr::RatioSource>,mpr::HaveGetSetValue > {};
				

				//////////////////////////////////////////////////////////////////////////
				//////////////////////////////////////////////////////////////////////////


				struct CFDLaminar
					: public moc::Provider<mpr::VelocityWithDerivatives,mpr::Coordinates2D>
					, public moc::Provider<mpr::Pressure,mpr::Coordinates2D>
					, public moc::Provider<mpr::Temperature,mpr::Coordinates2D>
					, public moc::Provider<mpr::MaterialName,mpr::Coordinates2D>
				{
					using moc::Provider<mpr::VelocityWithDerivatives,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::Pressure,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::Temperature,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::MaterialName,mpr::Coordinates2D>::GetState;
				};

				//////////////////////////////////////////////////////////////////////////

				struct CFDTurbulent
					: public moc::Provider<mpr::Velocity,mpr::Coordinates2D>
					, public moc::Provider<mpr::Pressure,mpr::Coordinates2D>
					, public moc::Provider<mpr::Temperature,mpr::Coordinates2D>
					, public moc::Provider<mpr::MaterialName,mpr::Coordinates2D>
					, public moc::Provider<mpr::TurbulenceK,mpr::Coordinates2D>
					, public moc::Provider<mpr::TurbulenceE,mpr::Coordinates2D>
				{
					using moc::Provider<mpr::Velocity,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::Pressure,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::Temperature,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::MaterialName,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::TurbulenceK,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::TurbulenceE,mpr::Coordinates2D>::GetState;
				};

				//////////////////////////////////////////////////////////////////////////

				struct TwoPhaseSystemTwoPhaseSystemTwoPhaseSystem
					: public moc::Provider<mpr::TwoPhaseComposition,mpr::Coordinates2D>
				{
					USING_GET_STATE(mpr::TwoPhaseComposition);
				};
				//////////////////////////////////////////////////////////////////////////

				struct SingleFluid
					: public moc::Provider<mpr::Viscosity,mpr::Coordinates2D>
					, public moc::Provider<mpr::Density,mpr::Coordinates2D>
					, public moc::Provider<mpr::Cp,mpr::Coordinates2D>
					, public moc::Provider<mpr::ThermalConductivity,mpr::Coordinates2D>
					, public moc::Provider<mpr::CoeffVolumeExpansion,mpr::Coordinates2D>
				{
					using moc::Provider<mpr::Viscosity,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::Density,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::Cp,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::ThermalConductivity,mpr::Coordinates2D>::GetState;
					using moc::Provider<mpr::CoeffVolumeExpansion,mpr::Coordinates2D>::GetState;
				};
			
				//////////////////////////////////////////////////////////////////////////

				struct TwoFluidSurface
					: public moc::Provider<mpr::SurfaceTension,mpr::Coordinates2D>
				{
					using moc::Provider<mpr::SurfaceTension,mpr::Coordinates2D>::GetState;
				};

				//////////////////////////////////////////////////////////////////////////

				struct Dispersion
					: public moc::Provider<ContinousComponent,mpr::Coordinates2D>
					, public moc::Provider<DisperesedComponent,mpr::Coordinates2D>
					, public moc::Provider<SauterMeanDiameter,mpr::Coordinates2D>
					, public moc::Provider<TotalInterphacialArea,mpr::Coordinates2D>
				{
					using moc::Provider<ContinousComponent,mpr::Coordinates2D>::GetState;
					using moc::Provider<DisperesedComponent,mpr::Coordinates2D>::GetState;
					using moc::Provider<SauterMeanDiameter,mpr::Coordinates2D>::GetState;
					using moc::Provider<TotalInterphacialArea,mpr::Coordinates2D>::GetState;
				};

				//////////////////////////////////////////////////////////////////////////

				//////////////////////////////////////////////////////////////////////////

				struct TwoPhaseSystem
					: public moc::Provider<mpr::TwoPhaseComposition,mpr::Coordinates2D>
				{
					using moc::Provider<mpr::TwoPhaseComposition,mpr::Coordinates2D>::GetState;
				};

				//////////////////////////////////////////////////////////////////////////

				struct ChemicalComponentSource
					: public moc::Provider<mpr::RatioSource,mpr::Coordinates2D>
				{
					using moc::Provider<mpr::RatioSource,mpr::Coordinates2D>::GetState;
				};

				//////////////////////////////////////////////////////////////////////////

				struct ChemicalComponentsSourcesSet
					: public moc::Provider<RatioSourcesSet,mpr::Coordinates2D>
				{
					using moc::Provider<RatioSourcesSet,mpr::Coordinates2D>::GetState;
				};

				//////////////////////////////////////////////////////////////////////////

				struct ChemicalComponentRatio
					: public moc::Provider<mpr::Ratio,mpr::Coordinates2D>
				{
					using moc::Provider<mpr::Ratio,mpr::Coordinates2D>::GetState;
				};

				//////////////////////////////////////////////////////////////////////////

				struct FreeSurface
					: public moc::Provider<mpr::MaterialPointVelocity2D,mpr::Coordinates2D>
				{
					using moc::Provider<mpr::MaterialPointVelocity2D,mpr::Coordinates2D>::GetState;
				};

				//////////////////////////////////////////////////////////////////////////

				struct ChemicalComposition
					//!< \todo What components?
				{

				};
			}

			//////////////////////////////////////////////////////////////////////////
			// INPUTS
			//////////////////////////////////////////////////////////////////////////

			namespace inputs
			{

				struct CFDLaminar
					: public moc::Provider<mpr::Viscosity,mpr::Coordinates2D>
					, public moc::Provider<mpr::Density,mpr::Coordinates2D>
					, public moc::Provider<mpr::Cp,mpr::Coordinates2D>
					, public moc::Provider<mpr::ThermalConductivity,mpr::Coordinates2D>
					, public moc::Provider<mpr::CoeffVolumeExpansion,mpr::Coordinates2D>
				{

				};

				//////////////////////////////////////////////////////////////////////////

				struct DispersedFluid
					: public moc::Provider<contracts::ContinousComponent,mpr::Coordinates2D>
					, public moc::Provider<contracts::DisperesedComponent,mpr::Coordinates2D>
					, public moc::Provider<contracts::SauterMeanDiameter,mpr::Coordinates2D>
					, public moc::Provider<contracts::TotalInterphacialArea,mpr::Coordinates2D>
				{

				};

				//////////////////////////////////////////////////////////////////////////

				struct CFDTurbulent
				{

				};

				//////////////////////////////////////////////////////////////////////////

				struct SingleFluid
				{

				};

				//////////////////////////////////////////////////////////////////////////

				struct TwoFluidSurface
				{

				};

				//////////////////////////////////////////////////////////////////////////

				struct Dispersion
				{

				};

				//////////////////////////////////////////////////////////////////////////

				struct ComponentSource
				{

				};

				//////////////////////////////////////////////////////////////////////////

				struct ComponentRatio
				{

				};

				//////////////////////////////////////////////////////////////////////////

// 				struct FreeSurface
// 					: public moc::Provider<mpr::MaterialPair,mpr::Coordinates2D>
// 				{
// 
// 				};

				//////////////////////////////////////////////////////////////////////////

				struct ChemicalComposition
					//!< \todo What components?
				{

				};
				
				//////////////////////////////////////////////////////////////////////////

				struct Temperature
					: public moc::Provider<mpr::Temperature,mpr::Coordinates2D>
				{

				};
			}

		}
	}
	namespace model
	{
		namespace util
		{
			template<>
			inline void Zero(am3::problem::ionExchange::contracts::Component & value)
			{
				value.component=nullptr;
				value.ratio=0;
			};
		}
	}
	
}

#endif // problemIonExchange_contracts_h__
