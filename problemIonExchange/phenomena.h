
/*! \file phenomena.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       Main header for ion exchange problem
 * \details   	
*********************************************************************/

#ifndef problemIonExchange_phenomena_h__
#define problemIonExchange_phenomena_h__

/*	include *********************************************************/

#include "contracts.h"
/*	using ***********************************************************/

/*	extern **********************************************************/
/*	classes *********************************************************/


// IT WAS WRONG IDEA?

/*
namespace am3
{
	namespace problem
	{
		namespace ionExchange
		{
			namespace phenomena
			{
				class CfdLaminar: public ionExchange::contracts::CFDLaminar
				{
					virtual AddInputs(inputs::SingleFluid*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class SingleFluid: public contracts::SingleFluid
				{
				};

				//////////////////////////////////////////////////////////////////////////

				class DispersedFlow: public contracts::SingleFluid
				{
					virtual AddInputs(inputs::DispersedFluid*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class PolimerFlow: public contracts::SingleFluid
				{
					 virtual AddInputs(inputs::ChemicalComposition*)=0;
					 virtual AddInputs(inputs::Temperature*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class FreeSurface: public contracts::FreeSurface
				{
					virtual AddInputs(inputs::TwoFluidSurface*)=0;
				};

				class LaminarFreeSurface: public FreeSurface
				{
					virtual AddInputs(inputs::CFDLaminar*)=0;
				};

				class TurbulentFreeSurface: public FreeSurface
				{
					virtual AddInputs(inputs::CFDTurbulent*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class Dispersion: public contracts::Dispersion
				{
					virtual AddInputs(inputs::ChemicalComposition*)=0;
					virtual AddInputs(inputs::Temperature*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class Coagulation: public contracts::Dispersion
				{
					virtual AddInputs(inputs::ChemicalComposition*)=0;
					virtual AddInputs(inputs::Temperature*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class DispertionCoagulation: public contracts::Dispersion
				{
					//virtual AddInputs(inputs::CFDTurbulent*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class PhasesDrift: public contracts::PhaseSource
				{
					//virtual AddInputs(inputs::CFDTurbulent*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class TurbulentFreeSurface: public FreeSurface
				{
					virtual AddInputs(inputs::CFDTurbulent*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class ConvectionDifusion: public contracts::ChemicalComposition
				{
					virtual AddInputs(inputs::CFDLaminar*)=0;
					virtual AddInputs(inputs::ComponentSource*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class ChemicalReaction
					: public contracts::ChemicalComposition
					, public contracts::HeatSource
				{
					virtual AddInputs(inputs::ChemicalComposition*)=0;
					virtual AddInputs(inputs::Temperature*)=0;
				};

				//////////////////////////////////////////////////////////////////////////

				class IonExchange: public contracts::ChemicalComposition
				{
					virtual AddInputs(inputs::ChemicalComposition*)=0; // x2, po obu stronach?
					virtual AddInputs(inputs::Temperature*)=0;
				};

			}

		}
	}
}
*/
#endif // problemIonExchange_phenomena_h__
