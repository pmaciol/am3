
/*! \file knowledgeBuiltIn.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelIonExchange_knowledgeBuiltIn_h__
#define modelIonExchange_knowledgeBuiltIn_h__
/*	include *********************************************************/

#include "../knowledge/kbsInterfaces.h"
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "modelIonExchange.h"

/*	using ***********************************************************/

namespace kn=am3::knowledge;
namespace mpr=am3::model;
namespace mpt=am3::model::point;
namespace pie=am3::problem::ionExchange;


/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace problem
	{
		namespace ionExchange
		{
			namespace knowledge
			{
				//! Class with basic KBS functionality. Can be used for tests instead of external KBS
				class IonExchangeBuiltInKBS : public kn::I4Adapter<pie::Coordinates,pie::ModelID>
				{
				public:
					typedef kn::I4Adapter<pie::Coordinates,pie::ModelID>::COORDINATES Coordinates;
					typedef kn::I4Adapter<pie::Coordinates,pie::ModelID>::MODEL_ID ModelId;
					virtual ModelId GetState(const pie::Point *const, const ModelId *const )
					{
						// just for tests!
						ModelId mId;
						mId(1);
						return mId;
					};
				};

			} //knowledge
		} //ionExchange
	} // problem
} // am3
#endif // modelIonExchange_knowledgeBuiltIn_h__
