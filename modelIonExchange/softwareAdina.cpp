#include <sstream>
#include "softwareAdina.h"

mpr::SourceLH am3::problem::ionExchange::software::AdinaFIonExchangeConverter::GetState( Point* point,const mpr::SourceLH* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates3D> point3d = saf::contract::defined::Convert2DPointToAdina3DPoint(*point);
	mpr::SourceLH tmp;
	return tmp;
}

mpr::SourceRH am3::problem::ionExchange::software::AdinaFIonExchangeConverter::GetState( Point* point,const mpr::SourceRH* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates3D> point3d = saf::contract::defined::Convert2DPointToAdina3DPoint(*point);
	mpr::SourceRH tmp;
	return tmp;
}

mpr::Velocity am3::problem::ionExchange::software::AdinaFIonExchangeConverter::GetState( Point* point,const mpr::Velocity* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates3D> point3d = saf::contract::defined::Convert2DPointToAdina3DPoint(*point);
	return storage_->GetState(&point3d,trait);
}

mpr::TurbulenceE am3::problem::ionExchange::software::AdinaFIonExchangeConverter::GetState( Point* point,const mpr::TurbulenceE* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates3D> point3d = saf::contract::defined::Convert2DPointToAdina3DPoint(*point);
	return storage_->GetState(&point3d,trait);
}

mpr::TurbulenceK am3::problem::ionExchange::software::AdinaFIonExchangeConverter::GetState( Point* point,const mpr::TurbulenceK* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates3D> point3d = saf::contract::defined::Convert2DPointToAdina3DPoint(*point);
	return storage_->GetState(&point3d,trait);
}

mpr::Pressure am3::problem::ionExchange::software::AdinaFIonExchangeConverter::GetState( Point* point,const mpr::Pressure* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates3D> point3d = saf::contract::defined::Convert2DPointToAdina3DPoint(*point);
	return storage_->GetState(&point3d,trait);
}

mpr::Temperature am3::problem::ionExchange::software::AdinaFIonExchangeConverter::GetState( Point* point,const mpr::Temperature* const trait)
{
	mpt::ComputedPoint<mpr::Coordinates3D> point3d = saf::contract::defined::Convert2DPointToAdina3DPoint(*point);
	return storage_->GetState(&point3d,trait);
}

mpr::TwoPhaseComposition am3::problem::ionExchange::software::AdinaFIonExchangeConverter::GetState( Point* point,const mpr::TwoPhaseComposition* const )
{
	mpr::RatiosSet ratioSet;
	mpt::ComputedPoint<mpr::Coordinates3D> point3d = saf::contract::defined::Convert2DPointToAdina3DPoint(*point);
	ratioSet=storage_->GetState(&point3d,&ratioSet);
	mpr::TwoPhaseComposition composition;
	composition.one(ratioSet.Value().at(0));
	composition.two(1-ratioSet.Value().at(0));
	return composition;
}

mpr::MaterialName am3::problem::ionExchange::software::AdinaFIonExchangeConverter::GetState( Point* point,const mpr::MaterialName* const )
{
	mpr::ElementGroupNumber grNum;
	mpt::ComputedPoint<mpr::Coordinates3D> point3d = saf::contract::defined::Convert2DPointToAdina3DPoint(*point);
	grNum=storage_->GetState(&point3d,&grNum);
	mpr::MaterialName matName;
	std::stringstream out;
	out << grNum();
	matName(out.str());
	return matName;
}

mpr::Viscosity am3::problem::ionExchange::software::AdinaFIonExchangeFromFineScale::GetState( mpt::Point<mpr::Coordinates> * point,const mpr::Viscosity* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates2D> point2d = saf::contract::defined::ConvertAdina3DPointTo2DPoint(*point);
	return source_->GetState(&point2d,trait);
}

mpr::Cp am3::problem::ionExchange::software::AdinaFIonExchangeFromFineScale::GetState( mpt::Point<mpr::Coordinates> * point,const mpr::Cp* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates2D> point2d = saf::contract::defined::ConvertAdina3DPointTo2DPoint(*point);
	return source_->GetState(&point2d,trait);
}

mpr::DensityWithDerivatives am3::problem::ionExchange::software::AdinaFIonExchangeFromFineScale::GetState( mpt::Point<mpr::Coordinates> * point,const mpr::DensityWithDerivatives* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates2D> point2d = saf::contract::defined::ConvertAdina3DPointTo2DPoint(*point);
	mpr::DensityWithDerivatives densWD;
	mpr::Density dens=source_->GetState(&point2d,&dens);
	densWD(dens());
	densWD.dp(0);
	densWD.dt(0);
	return densWD;
}

mpr::ThermalConductivity am3::problem::ionExchange::software::AdinaFIonExchangeFromFineScale::GetState( mpt::Point<mpr::Coordinates> * point,const mpr::ThermalConductivity* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates2D> point2d = saf::contract::defined::ConvertAdina3DPointTo2DPoint(*point);
	return source_->GetState(&point2d,trait);
}

mpr::CoeffVolumeExpansion am3::problem::ionExchange::software::AdinaFIonExchangeFromFineScale::GetState( mpt::Point<mpr::Coordinates> * point,const mpr::CoeffVolumeExpansion* const trait )
{
	mpt::ComputedPoint<mpr::Coordinates2D> point2d = saf::contract::defined::ConvertAdina3DPointTo2DPoint(*point);
	return source_->GetState(&point2d,trait);
}
