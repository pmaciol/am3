
/*! \file baseIonExchangeModels.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelIonExchange_baseIonExchangeModels_h__
#define modelIonExchange_baseIonExchangeModels_h__
/*	include *********************************************************/

#include <boost/weak_ptr.hpp>
#include "../models/models.h"
#include "../models/contracts.h"
#include "../models/dataStructures.h"
#include "problemDependentDataStructures.h"
#include "../knowledge/kbsInterfaces.h"

/*	using ***********************************************************/

namespace def=am3::model::contract::defined;
namespace mod=am3::model;
namespace com=am3::model::common;
namespace iom=am3::model::ionex;
namespace pro=am3::model;
namespace kno=am3::knowledge;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
//     namespace ionex
//     {
//       //////////////////////////////////////////////////////////////////////////
//       class MaterialProperties
//         : public mod::Model
//         , public con::Provider<pro::Density>
//         , public con::Provider<pro::CoeffVolumeExpansion>
//         , public con::Provider<pro::SurfaceTension>
//         , public con::Provider<pro::Cp>
//         , public con::Provider<pro::ThermalConductivity>
//       {
//       protected:
//         const pro::Density density_;
//         const pro::CoeffVolumeExpansion coeffVolumeExpansion_;
//         const pro::SurfaceTension surfaceTension_;
//         const pro::Cp cp_;
//         const pro::ThermalConductivity thermalConductivity_;
//         MaterialProperties(){};
// 
//       public:
// /*                MaterialProperties(){};*/
//         MaterialProperties(
//           const pro::Density density,
//           const pro::CoeffVolumeExpansion coeffVolumeExpansion,
//           const pro::SurfaceTension surfaceTension,
//           const pro::Cp cp,
//           const pro::ThermalConductivity thermalConductivity)
//           : density_(density),coeffVolumeExpansion_(coeffVolumeExpansion),surfaceTension_(surfaceTension),cp_(cp),thermalConductivity_(thermalConductivity){};
// 
//         virtual ~MaterialProperties(){};
//         virtual pro::Density GetState(com::Point* point,const pro::Density * const);
//         virtual pro::CoeffVolumeExpansion GetState(com::Point* point,const pro::CoeffVolumeExpansion * const);
//         virtual pro::SurfaceTension GetState(com::Point* point,const pro::SurfaceTension * const);
//         virtual pro::Cp GetState(com::Point* point,const pro::Cp * const);
//         virtual pro::ThermalConductivity GetState(com::Point* point,const pro::ThermalConductivity * const);
// 
//         virtual int Init(){return 0;};
//         virtual int Run(){return 0;};
//         virtual int PostProcess(){return 0;};
//       };
//       //////////////////////////////////////////////////////////////////////////
//       template <typename MATERIAL_1, typename RATIO_1_PROVIDER, typename MATERIAL_2, typename RATIO_2_PROVIDER>
//       class MeanMaterialProperties
//         : public mod::Model
//         , public con::Provider<pro::Density>
//         , public con::Provider<pro::CoeffVolumeExpansion>
//         , public con::Provider<pro::SurfaceTension>
//         , public con::Provider<pro::Cp>
//         , public con::Provider<pro::ThermalConductivity>
//       {
//       protected:
//         boost::weak_ptr<MATERIAL_1> material1_;
//         boost::weak_ptr<MATERIAL_2> material2_;
//         boost::weak_ptr<RATIO_1_PROVIDER> ratio1prov_;
//         boost::weak_ptr<RATIO_2_PROVIDER> ratio2prov_;
//       public:
//         MeanMaterialProperties(
//           const boost::weak_ptr<MATERIAL_1> material_1,
//           const boost::weak_ptr<typename RATIO_1_PROVIDER> RATIO_1_PROVIDER,
//           const boost::weak_ptr<MATERIAL_1> material_2,
//           const boost::weak_ptr<typename RATIO_2_PROVIDER> RATIO_2_PROVIDER
//           )
//           : material1_(material_1),material2_(material_2),ratio1prov_(ratio1prov_),ratio2prov_(RATIO_2_PROVIDER){};
//         virtual pro::Density GetState(com::Point* point,const pro::Density * const)
//         {
//           pro::Density dens;
//           double ratio1=ratio1prov_->GetState(point,p).Value();
//           double ratio2=ratio2prov_->GetState(point,p).Value();
//           dens(((material1_->GetState(point,p).Value()*ratio1)+(material2_->GetState(point,p).Value()*ratio2))/(ratio1+ratio2));
//           return dens;
//         }
//         virtual pro::CoeffVolumeExpansion GetState(com::Point* point,const pro::CoeffVolumeExpansion * const)
//           {
//             pro::CoeffVolumeExpansion p;
//             return p;
//         };
//         virtual pro::SurfaceTension GetState(com::Point* point,const pro::SurfaceTension * const);
//         virtual pro::Cp GetState(com::Point* point,const pro::Cp * const);
//         virtual pro::ThermalConductivity GetState(com::Point* point,const pro::ThermalConductivity * const);
//       };
//       //////////////////////////////////////////////////////////////////////////
// 
//       typedef MeanMaterialProperties<MaterialProperties,kno::I4Adapter<iom::WaterPhaseRatio>,MaterialProperties,kno::I4Adapter<iom::WaterPhaseRatio> >  MeanMaterialPropertiesProvider;
// 
//       //////////////////////////////////////////////////////////////////////////
//       template <typename PROVIDER, typename VARIABLE>
//       class OnlyConvection
//         : public mod::Model
//         , public PROVIDER
//       {
//       public:
//         virtual ~OnlyConvection(){};
//         virtual VARIABLE GetState(com::Point* point,const VARIABLE * const);
//         virtual int Init(){return 0;};
//         virtual int Run(){return 0;};
//         virtual int PostProcess(){return 0;};
//       };
// 
//       typedef OnlyConvection<iom::ZnIIRatioInWaterSourceProvider,iom::ZnIIRatioInWaterSource> ZnIIInWater_onlyConvection;
//       typedef OnlyConvection<iom::ZnIIRatioInOrganicSourceProvider,iom::ZnIIRatioInOrganicSource> ZnIIInOrganic_onlyConvection;
// // 
// //       class ZnIIInWater_onlyConvection
// //         : public mod::Model
// //         , public iom::ZnIIRatioInWaterSourceProvider
// //       {
// //       public:
// //         virtual ~ZnIIInWater_onlyConvection(){};
// //         virtual iom::ZnIIRatioInWaterSource GetState(com::Point* point,const iom::ZnIIRatioInWaterSource * const);
// //         virtual int Init(){return 0;};
// //         virtual int Run(){return 0;};
// //         virtual int PostProcess(){return 0;};
// //       };
// // 
// //      
// // 
// //       //////////////////////////////////////////////////////////////////////////
// //       class ZnIIInOrganic_onlyConvection
// //         : public mod::Model
// //         , public iom::ZnIIRatioInOrganicSourceProvider
// //       {
// //       public:
// //         virtual ~ZnIIInOrganic_onlyConvection(){};
// //         virtual iom::ZnIIRatioInOrganicSource GetState(com::Point* point,const iom::ZnIIRatioInOrganicSource* const);
// //         virtual int Init(){return 0;};
// //         virtual int Run(){return 0;};
// //         virtual int PostProcess(){return 0;};
// //       };
//       //////////////////////////////////////////////////////////////////////////
//     } //ionex
  } //model
} //am3

//   template <typename PROVIDER, typename VARIABLE>
//   VARIABLE iom::OnlyConvection<PROVIDER,VARIABLE>::GetState(com::Point* point,const VARIABLE* const)
//   {
//     lo::Starting("OnlyConvection");
//     VARIABLE toReturn;
//     toReturn(0);
//     lo::Stopping("OnlyConvection");
//     return toReturn;
//   }

//   template <typename MATERIAL_1, typename MATERIAL_1_PROVIDER, typename MATERIAL_2, typename MATERIAL_2_PROVIDER>
//   pro::Density am3::model::ionex::MeanMaterialProperties<MATERIAL_1, MATERIAL_1_PROVIDER, MATERIAL_2, MATERIAL_2_PROVIDER>::GetState( com::Point* point,const pro::Density p* const )
//   {
//     pro::Density dens;
//     double ratio1=material1prov_->GetState(point,p).Value();
//     double ratio2=material2prov_->GetState(point,p).Value();
//     dens(((material1_->GetState(point,p).Value()*ratio1)+(material2_->GetState(point,p).Value()*ratio2))/(ratio1+ratio2));
//     return dens;
//   }
#endif // modelIonExchange_baseIonExchangeModels_h__
