
/*! \file softwareAdina.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef softwareAdina_h__
#define softwareAdina_h__
/*	include *********************************************************/

#include <boost/weak_ptr.hpp>
#include "../softwareAdina/storageAdina.h"
#include "../softwareAdina/adinaFControls.h"
#include "../problemIonExchange/contracts.h"
#include "./modelIonExchange.h"
#include "./databus.h"

/*	using ***********************************************************/

namespace moc=am3::model::contract;
namespace mpr=am3::model;
namespace mpt=am3::model::common;
namespace sas=am3::software::adina::storage;
namespace saf=am3::software::adina::adinaF;
namespace pie=am3::problem::ionExchange;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace problem
	{
		namespace ionExchange
		{
			namespace software
			{
				namespace piec=am3::problem::ionExchange::contracts;

				//////////////////////////////////////////////////////////////////////////

				class MassSourceModelForPhase
					: public moc::Provider<mpr::SourceLH>
					, public moc::Provider<mpr::SourceRH>
				{
				public:
					MassSourceModelForPhase(pie::databus::DatabusTurbulentCase* databus):databus_(databus){};
					mpr::SourceLH GetState(mpt::Point<mpr::Coordinates>* point,const mpr::SourceLH* const trait)
					{
						return mpr::SourceLH();
					}
					mpr::SourceRH GetState(mpt::Point<mpr::Coordinates>* point,const mpr::SourceRH* const trait)
					{
						return mpr::SourceRH();
					}
				protected:
					pie::databus::DatabusTurbulentCase* databus_;
				};

				class MassSourceModelForDispertion
					: public moc::Provider<mpr::SourceLH>
					, public moc::Provider<mpr::SourceRH>
				{
				public:
					MassSourceModelForDispertion(pie::databus::DatabusTurbulentCase* databus):databus_(databus){};
					mpr::SourceLH GetState(mpt::Point<mpr::Coordinates>* point,const mpr::SourceLH* const trait)
					{
						mpr::SourceLH ret;
						ret(dispertionIncrease);
						return ret;
					}
					mpr::SourceRH GetState(mpt::Point<mpr::Coordinates>* point,const mpr::SourceRH* const trait)
					{
						mpr::SourceRH ret;
						ret(dispertionIncrease*10.0);
						return ret;
					}
				protected:
					void CalculateState(mpt::Point<mpr::Coordinates>* point)
					{
						Point* p;
						*p=saf::contract::defined::ConvertAdina3DPointTo2DPoint(*point);
						intArea_=databus_->GetState(p,&intArea_);
						mpr::ShearRate sr;
						sr=databus_->GetState(p,&sr);
						if (sr()>5.0) dispertionIncrease=5.0;
						else dispertionIncrease=0.0;
						//srh_=1.0*;
						//slh_=srh*10;
					};
					mpr::SourceRH srh_;
					mpr::SourceLH slh_;
					double dispertionIncrease;
					pie::contracts::TotalInterphacialArea intArea_;

					pie::databus::DatabusTurbulentCase* databus_;
				};

// 				class MassSourceModelForIonInPhaseOne
// 					: public moc::Provider<mpr::SourceLH>
// 					, public moc::Provider<mpr::SourceRH>
// 				{
// 
// 				};
// 
// 				class MassSourceModelForIonInPhaseTwo
// 					: public moc::Provider<mpr::SourceLH>
// 					, public moc::Provider<mpr::SourceRH>
// 				{
// 
// 				};
				
				class AdinaFIonExchangeMassRatios
					: public moc::Provider<mpr::SourceLH>
					, public moc::Provider<mpr::SourceRH>
				{
				public:
					AdinaFIonExchangeMassRatios(saf::contract::defined::AdinaFMassTransferProvider* provider,pie::databus::DatabusTurbulentCase* databus): provider_(provider),databus_(databus),msphase(databus){};
					mpr::SourceLH GetState(mpt::Point<mpr::Coordinates>* point,const mpr::SourceLH* const trait)
					{
						mpr::MaterialID MatId;
						MatId=provider_->GetState(point,&MatId);
						SetProviders(MatId);
						return this->sourceLH_->GetState(point,trait);
					}
					mpr::SourceRH GetState(mpt::Point<mpr::Coordinates>* point,const mpr::SourceRH* const trait)
					{
						mpr::MaterialID MatId;
						MatId=provider_->GetState(point,&MatId);
						SetProviders(MatId);
						return this->sourceRH_->GetState(point,trait);
					}
				protected:
					void SetProviders(const mpr::MaterialID& MatId)
					{
						int id=MatId();
						switch (id)
						{
						case 0:
							sourceLH_=&msphase;
							sourceRH_=&msphase;
							break;
						case 1:
							sourceLH_=&msphase;
							sourceRH_=&msphase;
							break;
						case 2:
							sourceLH_=&msphase;
							sourceRH_=&msphase;
							break;
						case 3:
							sourceLH_=&msphase;
							sourceRH_=&msphase;
							break;
						}
					}
					saf::contract::defined::AdinaFMassTransferProvider* provider_;
					pie::databus::DatabusTurbulentCase* databus_;
					MassSourceModelForPhase msphase;
					moc::Provider<mpr::SourceLH>* sourceLH_;
					moc::Provider<mpr::SourceRH>* sourceRH_;
				};

				class AdinaFIonExchangeFromFineScale
					: public moc::Provider<mpr::Viscosity>
					, public moc::Provider<mpr::Cp>
					, public moc::Provider<mpr::DensityWithDerivatives>
					, public moc::Provider<mpr::ThermalConductivity>
					, public moc::Provider<mpr::CoeffVolumeExpansion>
				{
				public:
					AdinaFIonExchangeFromFineScale(piec::SingleFluid* source): source_(source){};
					mpr::Viscosity GetState(mpt::Point<mpr::Coordinates> * point,const mpr::Viscosity* const trait);
					mpr::Cp GetState(mpt::Point<mpr::Coordinates> * point,const mpr::Cp* const trait);
					mpr::DensityWithDerivatives GetState(mpt::Point<mpr::Coordinates> * point,const mpr::DensityWithDerivatives* const trait);
					mpr::ThermalConductivity GetState(mpt::Point<mpr::Coordinates> * point,const mpr::ThermalConductivity* const trait);
					mpr::CoeffVolumeExpansion GetState(mpt::Point<mpr::Coordinates> * point,const mpr::CoeffVolumeExpansion* const trait);
				protected:
					piec::SingleFluid* source_;
				};


				class AdinaFIonExchangeComplement
					: public moc::Provider<mpr::BulkModulus>
					, public moc::Provider<mpr::Gravitation>
					, public moc::Provider<mpr::ReferenceTemperature>
					, public moc::Provider<mpr::Cv>
					, public moc::Provider<mpr::SurfaceTension> // ??
					, public moc::Provider<mpr::HeatPerVolume> //Could be moved to fine scale models
				{
				public:
					AdinaFIonExchangeComplement()
					{
						bulk_(1e20);
						g_.x(0); g_.y(0); g_.z(-9.81);
						rt_(0);
						cv_(0);
						st_(0);
						hpv_(0);
					}
					mpr::BulkModulus GetState(mpt::Point<mpr::Coordinates>* point,const mpr::BulkModulus* const trait) {return bulk_;};
					mpr::Gravitation GetState(mpt::Point<mpr::Coordinates>* point,const mpr::Gravitation* const trait) {return g_;};
					mpr::ReferenceTemperature GetState(mpt::Point<mpr::Coordinates>* point,const mpr::ReferenceTemperature* const trait) {return rt_;};
					mpr::Cv GetState(mpt::Point<mpr::Coordinates>* point,const mpr::Cv* const trait) {return cv_;};
					mpr::SurfaceTension GetState(mpt::Point<mpr::Coordinates>* point,const mpr::SurfaceTension* const trait) {return st_;};
					mpr::HeatPerVolume GetState(mpt::Point<mpr::Coordinates>* point,const mpr::HeatPerVolume* const trait) {return hpv_;};
					// etc
				protected:
					mpr::BulkModulus bulk_;
					mpr::Gravitation g_;
					mpr::ReferenceTemperature rt_;
					mpr::Cv cv_;
					mpr::SurfaceTension st_;
					mpr::HeatPerVolume hpv_;
				};


				class AdinaFIonExchangeConverter
					: public saf::contract::defined::AdinaFMassTransferContract
					, public piec::CFDTurbulent
					, public piec::TwoPhaseSystem
				{
				public:
					AdinaFIonExchangeConverter(sas::AdinaFMassTransferStorage* storage/*,pie::databus::DatabusLaminarCase* databus*/): storage_(storage)
					{
						fineScaleModel_=nullptr;
					};//, fineScaleModel_(databus)

					
					~AdinaFIonExchangeConverter()
					{
						if (fineScaleModel_!=nullptr)
						{
							delete fineScaleModel_;
						}				
					}
					void Init(pie::databus::DatabusTurbulentCase* databus)
					{
						fineScaleModel_=new AdinaFIonExchangeMassRatios(storage_,databus);
						//fineScaleModel_=new AdinaFIonExchangeFromFineScale(databus);

 						moc::ContractItemDispatcher<mpr::SourceLH>::SetCalculator(fineScaleModel_);
 						moc::ContractItemDispatcher<mpr::SourceRH>::SetCalculator(fineScaleModel_);
// 						moc::ContractItemDispatcher<mpr::Cp>::SetCalculator(fineScaleModel_);
// 						moc::ContractItemDispatcher<mpr::DensityWithDerivatives>::SetCalculator(fineScaleModel_);
// 						moc::ContractItemDispatcher<mpr::ThermalConductivity>::SetCalculator(fineScaleModel_);
// 						moc::ContractItemDispatcher<mpr::CoeffVolumeExpansion>::SetCalculator(fineScaleModel_);
// 						moc::ContractItemDispatcher<mpr::BulkModulus>::SetCalculator(&complementModel_);
// 						moc::ContractItemDispatcher<mpr::Gravitation>::SetCalculator(&complementModel_);
// 						moc::ContractItemDispatcher<mpr::ReferenceTemperature>::SetCalculator(&complementModel_);
// 						moc::ContractItemDispatcher<mpr::Cv>::SetCalculator(&complementModel_);
// 						moc::ContractItemDispatcher<mpr::SurfaceTension>::SetCalculator(&complementModel_);
// 						moc::ContractItemDispatcher<mpr::HeatPerVolume>::SetCalculator(&complementModel_);

					};
					mpr::SourceLH GetState(Point* point,const mpr::SourceLH* const trait);
					mpr::SourceRH GetState(Point* point,const mpr::SourceRH* const trait);
					mpr::Velocity GetState(Point* point,const mpr::Velocity* const trait);
					mpr::TurbulenceE GetState(Point* point,const mpr::TurbulenceE* const trait);
					mpr::TurbulenceK GetState(Point* point,const mpr::TurbulenceK* const trait);
					mpr::Pressure GetState(Point* point,const mpr::Pressure* const trait);
					mpr::Temperature GetState(Point* point,const mpr::Temperature* const trait);
					mpr::MaterialName GetState(Point* point,const mpr::MaterialName* const trait);
					mpr::TwoPhaseComposition GetState(Point* point,const mpr::TwoPhaseComposition* const trait);
				protected:
					sas::AdinaFMassTransferStorage* storage_;
					//boost::shared_ptr<AdinaFIonExchangeComplement> complementModel_;
//					AdinaFIonExchangeComplement complementModel_;
					AdinaFIonExchangeMassRatios* fineScaleModel_;

					//etc
				};

				//////////////////////////////////////////////////////////////////////////

				class AdinaF2DIonExchange 
					: public piec::CFDTurbulent
					, public piec::TwoPhaseSystem
				{
				public:
					AdinaF2DIonExchange():converter_(&storage_/*,databus_.get()*/){};
					mpr::Velocity GetState(Point* point,const mpr::Velocity* const trait){return converter_.GetState(point,trait);}
					mpr::TurbulenceK GetState(Point* point,const mpr::TurbulenceK* const trait){return converter_.GetState(point,trait);}
					mpr::TurbulenceE GetState(Point* point,const mpr::TurbulenceE* const trait){return converter_.GetState(point,trait);}
					mpr::Pressure GetState(Point* point,const mpr::Pressure* const trait){return converter_.GetState(point,trait);}
					mpr::Temperature GetState(Point* point,const mpr::Temperature* const trait){return converter_.GetState(point,trait);}
					mpr::MaterialName GetState(Point* point,const mpr::MaterialName* const trait){return converter_.GetState(point,trait);}
					mpr::TwoPhaseComposition GetState(Point* point,const mpr::TwoPhaseComposition* const trait){return converter_.GetState(point,trait);}

					//////////////////////////////////////////////////////////////////////////
					void SetDatabus(boost::shared_ptr<pie::databus::DatabusTurbulentCase> databus){ databus_=databus;};
					void Run()
					{
						converter_.Init(databus_.get());
						ma_.Init(&converter_,&storage_);
						ma_.Run();
					};

				protected:
					saf::ModelAdina ma_;
					sas::AdinaFMassTransferStorage storage_;
					boost::shared_ptr<pie::databus::DatabusTurbulentCase> databus_;
					AdinaFIonExchangeConverter converter_;

				};
			} //software
		} //ionExchange
	} // problem
} // am3
#endif // softwareAdina_h__
