
/*! \file databus.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelIonExchange_databus_h__
#define modelIonExchange_databus_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "modelIonExchange.h"
#include "../problemIonExchange/contracts.h"

/*	using ***********************************************************/

namespace pie=am3::problem::ionExchange;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace problem
	{
		namespace ionExchange
		{
			namespace databus
			{
				namespace piec=am3::problem::ionExchange::contracts;
				class DatabusTurbulentCase
					: public piec::CFDTurbulent
					, public piec::SingleFluid
					, public piec::Dispersion
					, public piec::TwoPhaseSystem
					//, etc
				{
				public:
					DatabusTurbulentCase(
						boost::shared_ptr<piec::CFDTurbulent> cfdTurbulent, // Storage, not adapter!
						boost::shared_ptr<piec::SingleFluid> singleFluid, // adapters ...
						boost::shared_ptr<piec::Dispersion> dispersion,
						boost::shared_ptr<piec::TwoPhaseSystem> twoPhaseSystem
//						boost::shared_ptr<piec::Dispersion> dispersion

						// etc
						):singleFluid_(singleFluid), cfdTurbulent_(cfdTurbulent),dispersion_(dispersion),twoPhaseSystem_(twoPhaseSystem){};
					mpr::Viscosity GetState(Point*,const mpr::Viscosity* const){return mpr::Viscosity();};
					mpr::Density GetState(Point*,const mpr::Density* const){return mpr::Density();};
					mpr::Cp GetState(Point*,const mpr::Cp* const){return mpr::Cp();};
					mpr::CoeffVolumeExpansion GetState(Point*,const mpr::CoeffVolumeExpansion* const){return mpr::CoeffVolumeExpansion();};
					mpr::ThermalConductivity GetState(Point*,const mpr::ThermalConductivity* const){return mpr::ThermalConductivity();};
					contracts::ContinousComponent GetState(Point*,const contracts::ContinousComponent* const){return contracts::ContinousComponent();}; 
					contracts::DisperesedComponent GetState(Point*,const contracts::DisperesedComponent* const){return contracts::DisperesedComponent();}; 
					contracts::SauterMeanDiameter GetState(Point*,const contracts::SauterMeanDiameter* const){return contracts::SauterMeanDiameter();}; 
					contracts::TotalInterphacialArea GetState(Point*,const contracts::TotalInterphacialArea* const){return contracts::TotalInterphacialArea();}; 
					mpr::Velocity GetState(Point*,const mpr::Velocity* const){return mpr::Velocity(0,0,0);};
					mpr::Pressure GetState(Point*,const mpr::Pressure* const){return mpr::Pressure();};
					mpr::ShearRate GetState(Point*,const mpr::ShearRate* const){return mpr::ShearRate();};
					mpr::TurbulenceE GetState(Point*,const mpr::TurbulenceE* const){return mpr::TurbulenceE();};
					mpr::TurbulenceK GetState(Point*,const mpr::TurbulenceK* const){return mpr::TurbulenceK();};
					mpr::Temperature GetState(Point*,const mpr::Temperature* const){return mpr::Temperature();};
					mpr::MaterialName GetState(Point*,const mpr::MaterialName* const){return mpr::MaterialName();};
					mpr::TwoPhaseComposition GetState(Point*,const mpr::TwoPhaseComposition* const){return mpr::TwoPhaseComposition();};
				protected:
					boost::shared_ptr<piec::SingleFluid> singleFluid_;
					boost::shared_ptr<piec::CFDTurbulent> cfdTurbulent_;
					boost::shared_ptr<piec::Dispersion> dispersion_;
					boost::shared_ptr<piec::TwoPhaseSystem> twoPhaseSystem_;
// 					// etc ...
				};

			} //databus
		} //ionExchange
	} // problem
} // am3

#endif // modelIonExchange_databus_h__
