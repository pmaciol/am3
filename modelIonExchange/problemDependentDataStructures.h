
/*! \file dataStructures.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelIonExchange_dataStructures_h__
#define modelIonExchange_dataStructures_h__
/*	include *********************************************************/

#include "../models/contracts.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace con=am3::model::contract;
namespace mod=am3::model;
namespace pro=am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace ionex
    { 

      //////////////////////////////////////////////////////////////////////////

      struct WaterPhaseRatio : public pro::Ratio {};
      struct WaterPhaseRatioProvider
        :public con::Provider<WaterPhaseRatio> {virtual ~WaterPhaseRatioProvider(){};};

      struct OrganicPhaseRatio : public pro::Ratio {};
      struct OrganicPhaseRatioProvider
        :public con::Provider<OrganicPhaseRatio> {virtual ~OrganicPhaseRatioProvider(){};};

      struct IonRatioInWater : public pro::Ratio {};
      struct IonRatioInWaterProvider
        :public con::Provider<IonRatioInWater> {virtual ~IonRatioInWaterProvider(){};};

      struct IonRatioInOrganic : public pro::Ratio {};
      struct IonRatioInOrganicProvider
        :public con::Provider<IonRatioInOrganic> {virtual ~IonRatioInOrganicProvider(){};};

      //////////////////////////////////////////////////////////////////////////

      struct WaterPhaseRatioSource : public pro::RatioSource {};
      struct WaterPhaseRatioSourceProvider
        :public con::Provider<WaterPhaseRatioSource> {virtual ~WaterPhaseRatioSourceProvider(){};};

      struct OrganicPhaseRatioSource : public pro::RatioSource {};
      struct OrganicPhaseRatioSourceProvider
        :public con::Provider<OrganicPhaseRatioSource> {virtual ~OrganicPhaseRatioSourceProvider(){};};

      struct IonRatioInWaterSource : public pro::RatioSource {};
      struct IonRatioInWaterSourceProvider
        :public con::Provider<IonRatioInWaterSource> {virtual ~IonRatioInWaterSourceProvider(){};};

      struct IonRatioInOrganicSource : public pro::RatioSource {};
      struct IonRatioInOrganicSourceProvider
        :public con::Provider<IonRatioInOrganicSource> {virtual ~IonRatioInOrganicSourceProvider(){};};

      //////////////////////////////////////////////////////////////////////////

      struct ZnIIRatioInOrganic : public IonRatioInWater {};
      struct ZnIIRatioInOrganicProvider
        :public con::Provider<ZnIIRatioInOrganic> {virtual ~ZnIIRatioInOrganicProvider(){};};

      struct ZnIIRatioInWater : public IonRatioInWater {};
      struct ZnIIRatioInWaterProvider
        :public con::Provider<ZnIIRatioInWater> {virtual ~ZnIIRatioInWaterProvider(){};};

      struct ZnIIRatioInWaterSource : public IonRatioInWaterSource {};
      struct ZnIIRatioInWaterSourceProvider
        :public con::Provider<ZnIIRatioInWaterSource> {virtual ~ZnIIRatioInWaterSourceProvider(){};};

      struct ZnIIRatioInOrganicSource : IonRatioInOrganicSource {};
      struct ZnIIRatioInOrganicSourceProvider
        :public con::Provider<ZnIIRatioInOrganicSource> {virtual ~ZnIIRatioInOrganicSourceProvider(){};};
      //////////////////////////////////////////////////////////////////////////

      struct BubbleCoefficient : public pro::Ratio {};
      struct BubbleCoefficientProvider
        :public con::Provider<BubbleCoefficient> {virtual ~BubbleCoefficientProvider(){};};
    } //ionex
  } //model
} //am3
#endif // modelIonExchange_dataStructures_h__
