
/*! \file modelIonExchange.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelIonExchange_modelIonExchange_h__
#define modelIonExchange_modelIonExchange_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/point.h"

#include "../models/models.h"
#include "../models/contracts.h"

/*	using ***********************************************************/

namespace mpr=am3::model;
namespace mpt=am3::model::common;


namespace def=am3::model::contract::defined;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace problem
	{
		namespace ionExchange
		{
			typedef mpr::ModelID ModelID;
			typedef mpr::Coordinates2D Coordinates;
			typedef mpt::Point<Coordinates> Point;
			
		}
	}
}

namespace am3
{
  namespace model
  {
//     namespace ionex
//     {
//       class ViscosityConst
//         : public Model
//         , public def::ViscProvider
//       {
//       public:
//         virtual ~ViscosityConst(){};
//         virtual pro::Viscosity GetState(am3::model::common::Point* point,const pro::Viscosity* const);
//         virtual int Init(){return 0;};
//         virtual int Run(){return 0;};
//         virtual int PostProcess(){return 0;};
//       };
// 
//     } //ionex
  } //model
} //am3

#endif // modelIonExchange_modelIonExchange_h__
