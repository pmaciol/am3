
/*! \file modelsTest.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelIonExchange_modelsTest_h__
#define modelIonExchange_modelsTest_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../problemIonExchange/contracts.h"
#include "modelIonExchange.h"

/*	using ***********************************************************/

namespace mpr=am3::model;
namespace moc=am3::model::contract;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace problem
	{
		namespace ionExchange
		{
			namespace model
			{
				namespace piec=am3::problem::ionExchange::contracts;
			  
				class ForSingleFluid
					:public piec::SingleFluid
				{
				public:
					mpr::Viscosity GetState(Point*,const mpr::Viscosity* const){return vis_;};
					mpr::Density GetState(Point*,const mpr::Density* const){return density_;};
					mpr::Cp GetState(Point*,const mpr::Cp* const){return cp_;};
					mpr::CoeffVolumeExpansion GetState(Point*,const mpr::CoeffVolumeExpansion* const){return volExp_;};
					mpr::ThermalConductivity GetState(Point*,const mpr::ThermalConductivity* const){return therCond_;};
				protected:
					mpr::Viscosity vis_;
					mpr::Density density_;
					mpr::Cp cp_;
					mpr::CoeffVolumeExpansion volExp_;
					mpr::ThermalConductivity therCond_;
				};

				class VelocityDependent : public ForSingleFluid
				{
				public:
					VelocityDependent(boost::shared_ptr<moc::Provider<mpr::Velocity,mpr::Coordinates2D> > dataSource):dataSource_(dataSource)
					{
						vis_(0.01);
						density_(1000);
						cp_(100);
						volExp_(1e-5);
						therCond_(1e-3);
					};
				protected:
					boost::shared_ptr<moc::Provider<mpr::Velocity,mpr::Coordinates2D> > dataSource_;
				};

				class TemperatureDependent : public ForSingleFluid
				{
				public:
					TemperatureDependent(boost::shared_ptr<moc::Provider<mpr::Temperature,mpr::Coordinates2D> > dataSource):dataSource_(dataSource)
					{
						vis_(0.02);
						density_(1000);
						cp_(100);
						volExp_(1e-5);
						therCond_(1e-3);
					};
				protected:
					boost::shared_ptr<moc::Provider<mpr::Temperature,mpr::Coordinates2D> > dataSource_;
				};

				//////////////////////////////////////////////////////////////////////////

				class ForDispersion
					:public piec::Dispersion
				{
				public:
					contracts::ContinousComponent GetState(Point*,const contracts::ContinousComponent* const){return contComp_;}; 
					contracts::DisperesedComponent GetState(Point*,const contracts::DisperesedComponent* const){return dispComp_;}; 
					contracts::SauterMeanDiameter GetState(Point*,const contracts::SauterMeanDiameter* const){return sautMD_;}; 
					contracts::TotalInterphacialArea GetState(Point*,const contracts::TotalInterphacialArea* const){return totalInter_;}; 
				protected:
					contracts::ContinousComponent contComp_;				
					contracts::DisperesedComponent dispComp_;		
					contracts::SauterMeanDiameter sautMD_;		
					contracts::TotalInterphacialArea totalInter_;		
				};

				class ForDispersionShareRate : public ForDispersion
				{
				public:
					ForDispersionShareRate(boost::shared_ptr<moc::Provider<mpr::Velocity,mpr::Coordinates2D> > dataSource):dataSource_(dataSource)
					{
						sautMD_(1e-3);
						totalInter_(10);
					};
				protected:
					boost::shared_ptr<moc::Provider<mpr::Velocity,mpr::Coordinates2D> > dataSource_;
				};

				class ForDispersionShareRateTemp : public ForDispersion
				{
				public:
					ForDispersionShareRateTemp(boost::shared_ptr<moc::Provider<mpr::Velocity,mpr::Coordinates2D> > dataSourceVel, boost::shared_ptr<moc::Provider<mpr::Temperature,mpr::Coordinates2D> > dataSourceTemp):dataSourceVel_(dataSourceVel),dataSourceTemp_(dataSourceTemp){};
				protected:
					boost::shared_ptr<moc::Provider<mpr::Velocity,mpr::Coordinates2D> > dataSourceVel_;
					boost::shared_ptr<moc::Provider<mpr::Temperature,mpr::Coordinates2D> > dataSourceTemp_;
				};

// 				class ForTwoPhase
// 					:public piec::TwoPhaseSystem
// 				{
// 				public:
// 					mpr::TwoPhaseComposition GetState(Point*,const mpr::TwoPhaseComposition* const){return tpComp_;}; 
// 				protected:
// 					mpr::TwoPhaseComposition tpComp_;
// 				};
			} //model
		}
	}
}
#endif // modelIonExchange_modelsTest_h__
