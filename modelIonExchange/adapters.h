
/*! \file adapters.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelIonExchange_adapters_h__
#define modelIonExchange_adapters_h__
/*	include *********************************************************/

#include "../adapter/adapter.h"
#include "../problemIonExchange/contracts.h"
#include "knowledge.h"
#include "modelIonExchange.h"
/*	using ***********************************************************/

namespace ad=am3::adapter;
namespace pie=am3::problem::ionExchange;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace problem
	{
		namespace ionExchange
		{
			namespace adapters
			{

#define GET_STATE(T) T GetState(Point*,const T* const){return T();}

				namespace piec=am3::problem::ionExchange::contracts;

				class SingleFluid 
					: public ad::Adapter<piec::SingleFluid, pie::knowledge::KbsRepresentationForAllModels, Point>
					, public piec::SingleFluid
				{
				public:
					SingleFluid(boost::shared_ptr<pie::knowledge::KbsRepresentationForAllModels> kbs)
					{
						ad::Adapter<piec::SingleFluid, pie::knowledge::KbsRepresentationForAllModels, Point>::SetKbs(kbs);
					}

					GET_STATE(mpr::Viscosity);
					GET_STATE(mpr::Density);
					GET_STATE(mpr::Cp);
					GET_STATE(mpr::CoeffVolumeExpansion);
					GET_STATE(mpr::ThermalConductivity);
				};

				class TwoFluidSurface
					: public ad::Adapter<piec::TwoFluidSurface, pie::knowledge::KbsRepresentationForAllModels, Point>
					, public pie::contracts::TwoFluidSurface
				{

				};

				class Dispersion
					: public ad::Adapter<piec::Dispersion, pie::knowledge::KbsRepresentationForAllModels, Point>
					, public pie::contracts::Dispersion
				{
				public:
					Dispersion(boost::shared_ptr<pie::knowledge::KbsRepresentationForAllModels> kbs)
					{
						ad::Adapter<piec::Dispersion, pie::knowledge::KbsRepresentationForAllModels, Point>::SetKbs(kbs);
					}
					contracts::ContinousComponent GetState(Point*,const contracts::ContinousComponent* const){return contracts::ContinousComponent();}; 
					contracts::DisperesedComponent GetState(Point*,const contracts::DisperesedComponent* const){return contracts::DisperesedComponent();}; 
					contracts::SauterMeanDiameter GetState(Point*,const contracts::SauterMeanDiameter* const){return contracts::SauterMeanDiameter();}; 
					contracts::TotalInterphacialArea GetState(Point*,const contracts::TotalInterphacialArea* const){return contracts::TotalInterphacialArea();}; 
				};

// 				class TwoPhaseComposition 
// 					: public ad::Adapter<piec::TwoPhaseSystem, pie::knowledge::KbsRepresentationForAllModels, Point>
// 					, public piec::TwoPhaseSystem
// 				{
// 				public:
// 					TwoPhaseComposition(boost::shared_ptr<pie::knowledge::KbsRepresentationForAllModels> kbs)
// 					{
// 						ad::Adapter<piec::TwoPhaseSystem, pie::knowledge::KbsRepresentationForAllModels, Point>::SetKbs(kbs);
// 					}
// 
// 					GET_STATE(mpr::TwoPhaseComposition);
// 				};
				// ... etc

			} //adapters
		} //ionExchange
	} // problem
} // am3
#endif // modelIonExchange_adapters_h__
