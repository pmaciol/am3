
/*! \file knowledge.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelIonExchange_knowledge_h__
#define modelIonExchange_knowledge_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>

#include "../knowledge/kbsRepresentation.h"
//#include "../problemIonExchange/contracts.h"
//#include "adapters.h"
#include "modelIonExchange.h"

/*	using ***********************************************************/

namespace kn=am3::knowledge;
namespace pie=am3::problem::ionExchange;



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace problem
	{
		namespace ionExchange
		{
			namespace knowledge
			{
//				namespace piec=am3::problem::ionExchange::contracts;


				class KbsRepresentationForAllModels 
//  					: public kn::KbsRepresentation<pie::adapters::SingleFluid, Coordinates, ModelID>
//  					, public kn::KbsRepresentation<pie::adapters::TwoFluidSurface, Coordinates, ModelID>
					// etc ...
				{
				public:
					KbsRepresentationForAllModels(boost::shared_ptr<kn::I4Adapter<pie::Coordinates,pie::ModelID> > knowledgeSource)//: knowledgeSource_(boost::shared_ptr<kn::I4Adapter<pie::Coordinates,pie::ModelID> >(&knowledgeSource)){};
					{
						knowledgeSource_=knowledgeSource;
					}
				protected:
					boost::shared_ptr<kn::I4Adapter<pie::Coordinates,pie::ModelID> > knowledgeSource_;
				};

// 				class KbsRepSingleFluid
// 					: public kn::KbsRepresentation<piec::SingleFluid, Coordinates, ModelID>
// 				{
// 
// 				};
// 
// 				class KbsRepTwoFluidSurface
// 					: public kn::KbsRepresentation<piec::TwoFluidSurface, Coordinates, ModelID>
// 				{
// 
// 				};


			} //knowledge
		} //ionExchange
	} // problem
} // am3

#endif // modelIonExchange_knowledge_h__
