
/*! \file am3.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief AdinaFCallsProcessor class; responds to Adina user supplied material query 
 * \details   	
*********************************************************************/
#ifndef adinaServer_am3_h__
#define adinaServer_am3_h__

/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../softwareAdina/adinaDataInterface.h"

/*	using ***********************************************************/

namespace pro=am3::model;
namespace def=am3::model::contract::defined;

/*	extern **********************************************************/
/*	classes *********************************************************/


namespace am3
{
  namespace model
  {
    namespace adina
    {
			struct AdinaFUserMassTransfer
			{
				double shearRate,time,pressure, temperature,turbulence_k, turbulence_e, vx,vy,vz;
				int elementGroup, elementNumber,massRatioCurrent, nodesNumber;
				std::vector<double> massRatio;
				double  coordX, coordY,coordZ;
			};

			struct AdinaFUserMassTransferReturn
			{
				double so,sf;
			};
      
//! Data structure of Adina-CFD call parameters  
struct AdinaFUserMaterial
{
  double 
    x_velocity, x_velocity_dx,x_velocity_dy,x_velocity_dz,
    y_velocity, y_velocity_dx,y_velocity_dy,y_velocity_dz,
    z_velocity, z_velocity_dx,z_velocity_dy,z_velocity_dz,
    pressure, pressure_dx, pressure_dy, pressure_dz,
    temperature, temperature_dx, temperature_dy, temperature_dz,
    turbulence_k, turbulence_k_dx, turbulence_k_dy, turbulence_k_dz,
    turbulence_e, turbulence_e_dx, turbulence_e_dy, turbulence_e_dz,
    x_coord, y_coord, z_coord,
    time,
    mr1,  mr1dx,  mr1dy,  mr1dz,
    mr2,  mr2dx,  mr2dy,  mr2dz,
    mr3,  mr3dx,  mr3dy,  mr3dz,
    mr4,  mr4dx,  mr4dy,  mr4dz;	
  int 
    element_group_number, 
    element_number; 
};

//! Data structure of Adina-CFD call returned variables  
struct AdinaFUserMaterialReturn
{
  double
    viscosity,
    cp,
    thermal_conductivity,
    coeff_volume_expansion,
    density,
    referene_temperature,
    x_gravitation, y_gravitation, z_gravitation,
    heat_per_volume,
    surface_tension,
    bulk_modulus,
    cv,
    //CMAT(14) = DR_DP = DERIVATIVE OF RO WITH RESPECT TO PRESSURE
    density_d_pressure,
    //CMAT(15) = DR_DT = DERIVATIVE OF RO WITH RESPECT TO TEMPERATURE
    density_d_temperature;
};

//! Copies data from Adina-CFD call to data storage; gets returned data from adapter
class AdinaFCallsProcessor
{
protected:
  def::AdinaFStore* adapterLevelStorage_;
  def::AdinaFContract* otherModels_;
public:
  void Init( def::AdinaFContract* otherModels,def::AdinaFStore* store  );
  void processAdinaData(AdinaFUserMaterial& in, AdinaFUserMaterialReturn& out);
};

class AdinaFMassTransferCallsProcessor
{
protected:
	def::AdinaFMassTransferStore* adapterLevelStorage_;
	def::AdinaFMassTransferContract* otherModels_;
public:
	void Init( def::AdinaFMassTransferContract* otherModels,def::AdinaFMassTransferStore* store  );
	void processAdinaData(AdinaFUserMassTransfer& in, AdinaFUserMassTransferReturn& out);
};

    } //adina
  } //model
} //am3
#endif // adinaServer_am3_h__

