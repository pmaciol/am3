
include_directories (../)

add_library(am3adinaServer adinaSocketServer.h adinaSocketServer.cpp adinaFprocessor.h adinaFprocessor.cpp storageAdina.h storageAdina.cpp)

target_link_libraries(am3adinaServer wsock32.lib)


