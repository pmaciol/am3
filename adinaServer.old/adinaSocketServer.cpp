#include <iostream>
#include <string>

#include "../communicationProvider/predefinedStreams.h"
#include "adinaSocketServer.h"


namespace adi=am3::model::adina;

void adinaSocketServer::Communicate( std::string address, std::string port, adi::AdinaFMassTransferCallsProcessor& am3 )
{
	am3::common::communication::Winsock2Stream::initData serverSocketData;
	serverSocketData.bufferSize = 1000;
	serverSocketData.hostname = new char[address.length() + 1];
	strcpy(serverSocketData.hostname,address.c_str());
	serverSocketData.serverPort = new char[port.length() + 1];
	strcpy(serverSocketData.serverPort,port.c_str());
	serverSocketData.operatingMode = serverSocketData.SERVER_MODE;
	Winsock2Stream serverSocketStream(serverSocketData);
	std::cout << "  Tried to open socket\n" ;
	if (serverSocketStream.good()) std::cout<<"socket OK\n";
	else std::cout<<"socket not OK\n";

	adi::AdinaFUserMassTransfer adinaIn;

	while (1)
	{
		serverSocketStream >> adinaIn.shearRate;
		serverSocketStream >> adinaIn.vx;
		serverSocketStream >> adinaIn.vy;
		serverSocketStream >> adinaIn.vz;
		serverSocketStream >> adinaIn.time;
		serverSocketStream >> adinaIn.elementGroup;
		serverSocketStream >> adinaIn.elementNumber;
		serverSocketStream >> adinaIn.massRatioCurrent;
		serverSocketStream >> adinaIn.nodesNumber;
		serverSocketStream >> adinaIn.pressure;
		serverSocketStream >> adinaIn.temperature;
		serverSocketStream >> adinaIn.turbulence_k;
		serverSocketStream >> adinaIn.turbulence_e;
		serverSocketStream >> adinaIn.coordX;
		serverSocketStream >> adinaIn.coordY;
		serverSocketStream >> adinaIn.coordZ;
		adinaIn.massRatio.clear();
		double tmp;
		for (int i=0;i<4;i++)
		{
			serverSocketStream >> tmp;
			adinaIn.massRatio.push_back(tmp);
		}

		adi::AdinaFUserMassTransferReturn adinaOut;
		am3.processAdinaData(adinaIn,adinaOut);

		serverSocketStream << adinaOut.so;
		serverSocketStream << adinaOut.sf;
	}
	
}