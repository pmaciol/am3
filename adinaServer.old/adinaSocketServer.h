#ifndef adinaSocketServer_h
#define adinaSocketServer_h

#include <string>
#include <winsock.h>

#include "adinaFprocessor.h"

namespace adi=am3::model::adina;

class adinaSocketServer
{
public:
//	static void Communicate(std::string address, std::string port, adi::AdinaFCallsProcessor& am3);		
	static void Communicate(std::string address, std::string port, adi::AdinaFMassTransferCallsProcessor& am3);		
};

#endif