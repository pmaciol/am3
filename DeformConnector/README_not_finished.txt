Deform connector - software layer for socket-based communication in Deform User Defined Routines.

I. Building from source:

 Deform connector consists of C++ sources that need to be build together with Fortran User Procedures. One needs to compile C++ sources into .obj files (Step 1) and then use modified Deform User Procedures makefiles in order to produce final dlls (Step 2). 
  - Detailed instructions for Step 1 can be found in dc_src folder in README.txt
  - Detailed instructions for Step 2 can be found in deform_src folder in README.txt

II. Running Deform with modified User Procedures:

 During the execution of UFLOW1 user routine (file usr_mtr.f) Deform will communicate with server in order to get required data. Server information (host and port) has to be written into dcconfig.txt in Deform working directory (Problem folder in Deform Manager). Sample dcconfig.txt contents are provided below:
dcconfig.txt:
 localhost
 22564
 1
 Configuration file consists of three lines. First is the server address, second port. In the third line 0 means no verbose log will be produced as opposed to 1. Important: Setting verbose mode on (1) will significantly slow down computations - use this option only for debugging purposes. No matter which option is set, error log entries will always be written - so whenever something goes wrong during the execution look inside dclog.txt file in Deform working directory first.

