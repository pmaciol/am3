#include "DeformConnector_Logger.h"
#include "DeformConnector_DataPack.h"
#include <stdio.h>

const char * DC_Logger::_file = "dclog.txt";
bool DC_Logger::_fileInitialized = false;

void DC_Logger::initLogFile()
{
	if(!_fileInitialized){
    FILE * pFile;
		pFile = fopen (_file,"w");
		fprintf (pFile, "%s\n", "DeformConnector started (unless you set verbosity to 1 in dcconfig.txt only errors will be reported)");
		fclose (pFile);
		_fileInitialized = true;
	}
}

void DC_Logger::reportOnce(const char * s)
{
	static bool reported = false;
	if(!reported)
	{
		FILE * pFile;
		pFile = fopen (_file,"a");
		fprintf (pFile, "%s\n", s);
		fclose (pFile);
	}
	reported = true;
}

void DC_Logger::logDataOUT(const DC_DataPackOUT& data)
{
	FILE * pFile;
	pFile = fopen (_file,"a");

	fprintf (pFile, "Not implemeted!:\n");

//	fprintf (pFile, "mr4dz  : %lf\n",data.mr4dz);		
	
	fclose (pFile);	
}

void DC_Logger::logDataIN(const DC_DataPackIN& data)
{
	FILE * pFile;
	pFile = fopen (_file,"a");

	fprintf (pFile, "Not implemeted!:\n");

	//fprintf (pFile, "viscosity  : %lf\n",data.viscosity);	
	
	fclose (pFile);	
}