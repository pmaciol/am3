Deform connector - software layer for socket-based communication in Deform User Procedures.

I. Step 1 - Budilding C++ sources:
 1. Prepare command line environment for C++ compiler. Tested with Intel C++ Compiler 11.1.070. MSVC2010 does not work due to problems with naming conventions
 2. From command line run Make_32.bat or Make_64.bat (not yet :( - depending on architecture
 3. 3 .obj files will be produced - copy them to directory where Deform user procedures Fortran sources and makefiles are located.
 4. Proceed with Step 2
