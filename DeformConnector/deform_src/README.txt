Deform connector - software layer for socket-based communication in Deform User Procedures.

I. Step 2 - buiding Deform executable
 1. First copy obj files from dc_src folder and WSock32.lib into Defrom user procedures directory
 2. Open command line and change to the above directory
 3. Set environment for Deform User procedures Fortran compiler.
 4. Run:
  build_def_sim_usr_absoft80.bat or similar, based on example in this directory.