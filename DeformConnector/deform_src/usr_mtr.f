C********************************************************************


      SUBROUTINE USRMTR(NPTRTN,YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C     THIS SUBROUTINE CALCULATES THE FLOW STRESS OF PLASTIC MATERIAL
C
C     INPUT :
C
C      NPTRTN    =   FLOW STRESS NUMBER
C      TEPS      =   EFFECTIVE STRAIN
C      EFEPS     =   EFFECTIVE STRAIN RATE
C      TEMP      =   TEMPERATURE
C
C     OUTPUT :
C
C      YS        =   FLOW STRESS
C      YPS       =   DERIVATIVE OF FLOW STRESS W.R.T TEPS
C      FIP       =   DERIVATIVE OF FLOW STRESS W.R.T. EFEPS
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C  USER SUPPLIED SUBROUTINES
C
C
C ****    USER DEFINED VARIABLES ****
C
      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C     
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     Version 5.1
C
C     COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,

C    +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),USRE1(2),USRE2(2),
C    +                NODEE(4),KELE
C
C     Version 6.0
C
C     COMMON /ELMCOM/
C
C         RZE     : Four corner coordinates
C         URZE    : Velocity
C         STSE    : Stress
C         EPSE    : Strain rate
C         EFEPSE  : effective strain rate
C         EFSTSE  : Effective stress
C         TEPSE   : Total effective strain
C         RDTYE   : Density
C         TEMPE   : Temperature
C         DTMPE   : Temperature rate
C         DAMAGE  : Damage value
C         USRE1   : Element user state variable 1
C         USRE2   : Element user state variable 2
C         USRNE   : Nodal user state variables 1,2 at 4 nodes
C         NODEE   : Connectivity
C         KELE    : Global element number
C         KELEL   : Local element number
C         KGROUP  : Material group number
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP

C
C     COMMON /NODCOM/
C
C        RZN      : Nodal point coordinates
C        URZN     : Nodal point velocities
C        DRZN     : Nodal point displacement
C        TEMPN    : Nodal point temperature
C        USRN1    : User defined state variables (Input : At the beginning of Step N)
C        USRN2    : User defined state variables (Output: At the end of Step N)
C        KNODE    : Node number
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE

C
C     CURTIM: CURRENT TIME           
C      
      COMMON /CLOCK/ CURTIM
C
C     DTMAXC: CURRENT TIME STEP SIZE
C
      COMMON /SSTU/ DTMAXC
C
C
C     COMMON /USRCTL/
C
C        KOBJ      : Object number
C        KSTEP     : Step number (N)
C        ISTATUS   : 0 - the beginning of the step
C                    1 - the end of the step
C        KSSTEP    : negative step indication -1 for negative step 1 for else
C
C     WHEN (ISTATUS.EQ. 1)  --> USRE2/USRN2 should be updated here
C     KELE  > 0             --> Element data is active
C     INODE > 0             --> Node Data is active
C
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
C
C     Branching to proper flow stress routine based on the
C     number specified in the pre-processor
C     
C
      GO TO (510,520,530,540,550,560,570,580,590,600),NPTRTN 
C     
  510 CALL UFLOW1(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  520 CALL UFLOW2(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  530 CALL UFLOW3(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  540 CALL UFLOW4(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  550 CALL UFLOW5(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  560 CALL UFLOW6(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  570 CALL UFLOW7(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  580 CALL UFLOW8(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  590 CALL UFLOW9(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  600 CALL UFLOW10(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
C  TO BE CONTINUED BY USER
C
      END
C********************************************************************

      SUBROUTINE UFLOW1(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C
C********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N) 
C
C ****    USER DEFINED VARIABLES ****
C
      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE	  
      COMMON /CLOCK/ CURTIM
      COMMON /SSTU/ DTMAXC
C     
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     DESCRIPTION
C
C       THIS ROUTINE IS USED TO DEMONSTRATE THE IMPLEMENTATION OF
C       MATERIAL ROUTINE.  ALL THE REAL VARIABLES SHOULD BE DOUBLE 
C       PRECISION. THE DEFINITION OF ARGUMENTS ARE DESCRIBED AS FOLLOWS:
C
C    INPUT :
C
C      TEPS     =   EFFECTIVE STRAIN
C      EFEPS    =   EFFECTIVE STRAIN RATE
C      TEMP     =   TEMPERATURE
C
C    OUTPUT :
C
C      YS       =   FLOW STRESS
C      YPS      =   DERIVATIVE OF FLOW STRESS W.R.T. TEPS
C      FIP      =   DERIVATIVE OF FLOW STRESS W.R.T. EFEPS
C
C
c      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
c     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
c     +                USRE1(1500),USRE2(1500),
c     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
c	
c     CURTIM: CURRENT TIME           
C      
c      COMMON /CLOCK/ CURTIM
C
c     DTMAXC: CURRENT TIME STEP SIZE
C
c      COMMON /SSTU/ DTMAXC
	  
	  integer*4 DEFORMMTRCRESULT
	  integer*4 DEFORMMTRCSEND
C
c      WRITE(*,*)'RZE(1,1)= ',RZE(1,1)
c      WRITE(*,8) NODEE(1),NODEE(2),NODEE(3),NODEE(4)
c	  WRITE(*,9) KELE, KELEL, KGROUP
8	  FORMAT('N1= ', I4, ' N2= ', I4,' N3= ', I4,' N4= ', I4)
9     FORMAT('KELE= ', I4,' KELEL= ', I4,' KGROUP= ', I4)
c      WRITE(*,10) RZE(1,1),RZE(2,1)
10    FORMAT('RZE(1,1)= ', F6.2, ' RZE(2,1)= ', F6.2)
c      WRITE(*,11) RZE(1,2),RZE(2,2)
11    FORMAT('RZE(1,2) ', F6.2, ' RZE(2,2)= ', F6.2)
c      WRITE(*,12) RZE(1,3),RZE(2,3)
12    FORMAT('RZE(1,3)= ', F6.2, ' RZE(2,3)= ', F6.2)
c      WRITE(*,13) RZE(1,4),RZE(2,4)
13    FORMAT('RZE(1,4)= ', F6.2, ' RZE(2,4)= ', F6.2)
c      WRITE(*,*)USRE1(1),DTMAXC
c      WRITE(*,*)DTMAXC
c      WRITE(*,*)RZE(1,1)
c	  WRITE(*,*)RZE(2,1)
c      WRITE(*,*)RZE(1,2)
c      WRITE(*,*)RZE(2,2)
c      WRITE(*,*)RZE(1,3)
c      WRITE(*,*)RZE(2,3)
c      WRITE(*,*)RZE(1,4)
c      WRITE(*,*)RZE(2,4)
c      WRITE(*,*)URZE(1,1)
c      WRITE(*,*)URZE(2,1)
c      WRITE(*,*)URZE(1,2)
c      WRITE(*,*)URZE(2,2)
c      WRITE(*,*)URZE(1,3)
c      WRITE(*,*)URZE(2,3)
c      WRITE(*,*)URZE(1,4)
c      WRITE(*,*)URZE(2,4)
c      WRITE(*,*)URZE(1,3)
c      WRITE(*,*)URZE(2,3)
c      WRITE(*,*)URZE(1,4)
c      WRITE(*,*)URZE(2,4)
c      WRITE(*,*)STSE(1)
c      WRITE(*,*)STSE(2)
c      WRITE(*,*)STSE(3)
c      WRITE(*,*)STSE(4)
c      WRITE(*,*)EPSE(1)
c      WRITE(*,*)EPSE(2)
c      WRITE(*,*)EPSE(3)
c      WRITE(*,*)EPSE(4)
c      WRITE(*,*)EFEPSE
c      WRITE(*,*)EFSTSE
c      WRITE(*,*)TEPSE
c      WRITE(*,*)RDTYE
c	  WRITE(*,*)TEMPE(1)
c      WRITE(*,*)TEMPE(2)
c      WRITE(*,*)TEMPE(3)
c      WRITE(*,*)TEMPE(4)
c      WRITE(*,*)DTMPE(1)
c      WRITE(*,*)DTMPE(2)
c      WRITE(*,*)DTMPE(3)
c      WRITE(*,*)DTMPE(4)
c      WRITE(*,*)DAMAGE
c      WRITE(*,*)NODEE(1)
c      WRITE(*,*)NODEE(2)
c      WRITE(*,*)NODEE(3)
c      WRITE(*,*)NODEE(4)
c      WRITE(*,*)KELE
c      WRITE(*,*)KELEL
c      WRITE(*,*)KGROUP
	 
      DEFORMMTRCRESULT = DEFORMMTRCSEND( USRE1(1), DTMAXC, RZE(1,1),
     + RZE(2,1),RZE(1,2),RZE(2,2),RZE(1,3),RZE(2,3),RZE(1,4),RZE(2,4),
     + URZE(1,1),URZE(2,1),URZE(1,2),URZE(2,2),
     + URZE(1,3),URZE(2,3),URZE(1,4),URZE(2,4),
     + STSE(1),STSE(2),STSE(3),STSE(4),
     + EPSE(1),EPSE(2),EPSE(3),EPSE(4),
     + EFEPSE, EFSTSE, TEPSE, RDTYE, 
     + TEMPE(1),TEMPE(2),TEMPE(3),TEMPE(4),
     + DTMPE(1),DTMPE(2),DTMPE(3),DTMPE(4),
     + DAMAGE,
     + NODEE(1),NODEE(2),NODEE(3),NODEE(4),KELE, KELEL, KGROUP)
c	DAMAGE,
c	USRE1(1),
c	USRE1(2),
c	USRE1(3),
c	...
c	USRE2(1),
c	USRE2(2),
c	USRE2(3),
c	...
c	USRNE(1,1),USRNE(1,2),USRNE(1,3),USRNE(1,4),
c	USRNE(2,1),USRNE(2,2),USRNE(2,3),USRNE(2,4),
c	USRNE(3,1),USRNE(3,2),USRNE(3,3),USRNE(3,4),
c	...
	YS=10000.D0
	FIP=1.D0
	YPS=1.D0
	call DEFORMMTRCGET(YS,FIP,YPS)
C    EXAMPLE :
C    
C       PEM = STRAIN_RATE SENSITIVITY INDEX
C       YS = MATERIAL_CONSTANT * (STRAIN)**PEN* (STRAIN_RATE)**PEM
C       PEN = STRAIN SENSITIVITY INDEX
C       FIP = MATERIAL_CONSTANT * PEM * (STRAIN_RATE)**(PEM-1.)
C       YPS = 0.
C
C   STRAIN CAN COME FROM ONE OF THE THREE SOURCES
C
C     (1) FROM THE INPUT ARGUMENT "TEPS"
C     (2) FROM THE ELEMENT COMMON BLOCK "TEPSE"
C     (3) FROM THE USER DEFINED STATE VARIABLES (SEE THE ROUTINE "USRST1")
C
C     THE FOLLOWING EXAMPLE IS WRITTEN BASED ON THE USED DEFINED STATE 
C     VARIABLE
C
c     STRAIN = USRE1(1)
c      IF (STRAIN.LE.0.) STRAIN = 1.E-5
C      IF (EFEPS.LE.0.0) EFEPS=1.E-5
c      EFEPS=EFEPSE
C
c      PEN = 0.15
c      PEM = 0.1
c      YS0= 1.0

c
c      YS = 20. * STRAIN**PEN* (EFEPS)**PEM+YS0
c      FIP = 10. * STRAIN**PEN* PEM * (EFEPS)**(PEM-1.)
c      YPS = 10. * PEN * STRAIN**(PEN-1.) * (EFEPS)**PEM 
c      YPS = 100. * PEN * STRAIN**(PEN-1.) * (EFEPS)**PEM 
c      IF(STRAIN.EQ.1.E-5) YPS=0.0
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW2(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION 
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW3(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW4(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW5(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW6(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW7(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW8(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW9(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW10(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************
