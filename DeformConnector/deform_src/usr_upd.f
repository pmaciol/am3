C********************************************************************

      SUBROUTINE USRUPD(NPTRTN)

C********************************************************************
C
C     User routine to update user defined nodal and elemental
C     variables
C
C     NPTRTN is the flow stress routine number and matches the
C            number passed to the UFLOW routines. If you are not
C            using user defined flow stress routines ignore this
C            value.
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)

C
C     COMMON /USRCTL/
C
C        KOBJ      : Object number
C        KSTEP     : Step number (N)
C        ISTATUS   : 0 - the beginning of the step
C                    1 - the end of the step
C
C     WHEN (ISTATUS.EQ. 1)  --> USRE2/USRN2 should be updated here
C     KELE  > 0             --> Element data is active
C     INODE > 0             --> Node Data is active
C
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP

C
C     CURTIM: CURRENT TIME           
C      
      COMMON /CLOK/ CURTIM
C
C     DTMAXC: CURRENT TIME STEP SIZE
C
      COMMON /SSTU/ DTMAXC

C
C     COMMON /ELMCOM/
C
C         RZE     : Four corner coordinates
C         URZE    : Velocity
C         STSE    : Stress
C         EPSE    : Strain rate
C         EFEPSE  : effective strain rate
C         EFSTSE  : Effective stress
C         TEPSE   : Total effective strain
C         RDTYE   : Density
C         TEMPE   : Temperature
C         DAMAGE  : Damage value
C         DTMPE   : Temperature rate
C         USRE1   : Element user state variable 1
C         USRE2   : Element user state variable 2
C         USRNE   : Nodal user state variables 1,2 at 4 nodes
C         NODEE   : Connectivity
C         KELE    : Global element number
C         KELEL   : Local element number
C         KGROUP  : Material group number
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
C         STRNE     : Strain Components
C         INTNALE =0:  Edge exposed to outside world
C                  1:  Internal 
C         NBCDE     : Boundary Condition of four corners
C
C
      COMMON /ELMCOM2/ STRNE(4),NBCDE(2,4),INTNALE(4)
C
C         TEPS_NE  : Nodal eff. strain 
C         EFEPS_NE : Nodal eff. strain rate
C         DAMG_NE  : Nodal damage factor
C         STS_NE   : Nodal stress components (elastoplastic object)
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4),DAMG_NE(4),STS_NE(4,4)
C
C	Please note that the common blocks ELMCOM, ELMCOM2, ELMCOM3
C	are one set of element data for the element KELE of the object 
C	KOBJ. For this element KELE, having the nodal connectivity 
C       indicated in the array NODEE, nodal strain and strain rate
C	values of each node (for the element KELE) are available 
C	in the array TEPS_NE and EFEPS_NE provided nodal option 
C	of these variables is turned on in the Pre Processor > 
C	Simulation Controls > Advanced > Output control.
C
C     COMMON /NODCOM/
C
C        RZN      : Nodal point coordinates
C        URZN     : Nodal point velocities
C        DRZN     : Nodal point displacement
C        TEMPN    : Nodal point temperature
C        USRN1    : User defined state variables (Input : At the beginning of Step N)
C        USRN2    : User defined state variables (Output: At the end of Step N)
C        KNODE    : Node number
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
C        AREAN    : Nodal area
C        TMPNEB   : The corresponding temperature at contacting surface
C        SLDVEL   : Sliding velocity
C        PRESR(2) : Traction in tangential (friction) and normal (pressure)
C        INTNAL   : 0 - External surface node, 1 - Internal node
C
      COMMON /NODCOM2/ AREAN, TMPNEB, SLDVEL, PRESR(2), INTNAL
C
C        EFEPS_NN       : Nodal effective strain rate
C        TEPS_NN        : Nodal effective strain 
C        DAMG_NN        : Nodal damage factor
C        STS_NN         : Nodal stress components (elastoplastic object)
C        IELMNOD(K)  = 0: Element definition
C                    > 0: Node+element definition
C
C          K = 1,2,3   -- REFERRING TO DAMAGE, EFF. STRAIN AND STRESS
C                         COMPONENTS, RESPECTIVELY
C 
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,DAMG_NN,STS_NN(4),IELMNOD(3)
C
C	Please note that the common blocks NODCOM, NODCOM2, NODCOM3
C	are one set of nodal data for the node KNODE of the object 
C	KOBJ. For this node KNODE, nodal strain and strain rate
C	values of (for the node KNODE) are available 
C	in the variable TEPS_NN and EFEPS_NN provided nodal option 
C	of these variables is turned on in the Pre Processor > 
C	Simulation Controls > Advanced > Output control.
C    
      COMMON /DEFGRA/ DFDX(3,3,2) 

C       Last digit if 1 is for dx/dX at x=n, and X=0
C       Last digit if 2 is for dx/dX at x=n+1, and X=0
C       Gradient computed is with respect to the original
C       configuration (X=0)
C       This True for every meshed 2D object, computed at the end of the step. 

C     See USRSV1 for an example
C  
      USRE1(1)=CURTIM
C	  
      GO TO (510,520,530,540,550,560,570,580,590,600),NPTRTN
C
  510 CALL USRSV1
      RETURN
C
  520 CALL USRSV2
      RETURN
C
  530 CALL USRSV3 
      RETURN
C
  540 CALL USRSV4
      RETURN
C
  550 CALL USRSV5
      RETURN
C
  560 CALL USRSV6
      RETURN
C
  570 CALL USRSV7
      RETURN
C
  580 CALL USRSV8
      RETURN
C
  590 CALL USRSV9
      RETURN
C
  600 CALL USRSV10
      RETURN
C
C  TO BE CONTINUED BY USER
C
      END
C********************************************************************

      SUBROUTINE USRSV2

C********************************************************************
C
C     User defined state variable calculations
C
C
C     DTMAXC : CURRENT TIME STEP
C     CURTIM: CURRENT TIME
C
C     COMMON /USRCTL/
C     KOBJ   : OBJECT NUMBER
C     ISTATUS: 0 - the begain of the step
C              1 - the end of the step
C     KSTEP  : Step Number (N)
C
C     COMMON /ELMCOM/
C     RZE    : NODAL POINT COORDINATES (four corner nodes)
C     URZE   : NODAL POINT VELOCITY    (four corner nodes)
C     STSE   : STRESS TENSOR
C     EPSE   : STRAIN RATE TENSOR
C     EFEPSE : EFFECTIVE STRAIN RATE
C     EFSTSE : EFFECTIVE STRESS
C     TEPSE  : TOTAL EFFECTIVE STRAIN
C     TEMPE  : FOUR NODAL TEMPERATURE
C     DTMPE  : TEMPERATURE CHANGE
C     DAMAGE : DAMAGE FACTOR

C     USRD2  : USER DEFINED STATE VARIABLES (OUTPUT: At the End of the STEP N)
C     NODEE  : CONNECTIVITY OF THE ELEMENT
C     KELE   : ELEMENT NUMBER
C     KELEL  : LOCAL ELEMENT NUMBER (CURRENT OBJECT)
C     KGROUP : ELEMENT MATERIAL GROUP NUMBER

C     COMMON /NODCOM/
C     RZN    : Nodal Point Coordinates
C     URZN   : Nodal Point Velocities
C     DRZN   : Nodal Point Displacement
C     TEMPN  : Nodal Point Temperature
C     USRN1  : User Defined State Variables (Input: At the beginning of Step N)
C     USRN2  : User Defined State Variables (Output: At the end of Step N)
C     KNODE  : Nonde Number
C
C     WHEN (ISTATUS.EQ. 1) --> USRE2/USRN2 should be updated here
C     KELE  > 0             --> Element data is active
C     INODE > 0             --> Node Data is active
C
C
C     Example
C
C     Examples :
C
C     USRE(1)   Strain is stored which is used in the flow stress routine
C     USRE(2)   the maximum principal stress is stored
C
C     USRN?(1) : unused in thie subroutine
C     USRN?(2) : unused in thie subroutine
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)

      COMMON /SSTU/ DTMAXC
      COMMON /CLOK/ CURTIM 
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4)
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,IELMNOD2
C
      IF (ISTATUS.EQ.1.AND.KELE.GT.0) THEN
C
C     Strain = time increment * strain rate
C
         USRE2(1)=USRE1(1) + DTMAXC * EFEPSE
C
C     Calculate max principal stress and if greater than current value
C     store in the user element value
C
         USRE2(2)=USRE1(2)
         CALL USR_MAXPRN(STSE,PRNSTS)
         IF (USRE2(2).LT.PRNSTS) USRE2(2) = PRNSTS
C
C     additional variable test
C
         USRE2(3)= EFEPSE
         USRE2(4)= EFSTSE
         DO I=5, 1500
            USRE2(I)=USRE1(I)
         ENDDO

         RETURN
      ENDIF
C
      IF (ISTATUS.EQ.1.AND.KNODE.GT.0) THEN
         DO I=1, 1500
            USRN2(I)=USRN1(I)
         ENDDO
         RETURN
      ENDIF
C
      RETURN
      END
C********************************************************************

      SUBROUTINE USRSV1

C********************************************************************
C
C     User defined state variable calculations
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)
C
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4)
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,IELMNOD2
C
      IF (ISTATUS.EQ.1.AND.KELE.GT.0) THEN
         DO I=1, 1500
            USRE2(I)=USRE1(I)
         ENDDO
         RETURN
      ENDIF
C
      IF (ISTATUS.EQ.1.AND.KNODE.GT.0) THEN
         DO I=1, 1500
            USRN2(I)=USRN1(I)
         ENDDO
         RETURN
      ENDIF

      RETURN
      END
C********************************************************************

      SUBROUTINE USRSV3

C********************************************************************
C
C     User defined state variable calculations
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4)
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,IELMNOD2
C
      IF (ISTATUS.EQ.1.AND.KELE.GT.0) THEN
         DO I=1, 1500
            USRE2(I)=USRE1(I)
         ENDDO
         RETURN
      ENDIF
C
      IF (ISTATUS.EQ.1.AND.KNODE.GT.0) THEN
         DO I=1, 1500
            USRN2(I)=USRN1(I)
         ENDDO
         RETURN
      ENDIF
C
      RETURN
      END
C********************************************************************

      SUBROUTINE USRSV4

C********************************************************************
C
C     User defined state variable calculations
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4)
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,IELMNOD2
C
      IF (ISTATUS.EQ.1.AND.KELE.GT.0) THEN
         DO I=1, 1500
            USRE2(I)=USRE1(I)
         ENDDO
C
         RETURN
      ENDIF
C
      IF (ISTATUS.EQ.1.AND.KNODE.GT.0) THEN
         DO I=1, 1500
            USRN2(I)=USRN1(I)
         ENDDO
         RETURN
      ENDIF
      RETURN
      END
C********************************************************************

      SUBROUTINE USRSV5

C********************************************************************
C
C     User defined state variable calculations
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4)
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,IELMNOD2
C
      IF (ISTATUS.EQ.1.AND.KELE.GT.0) THEN
         DO I=1, 1500
            USRE2(I)=USRE1(I)
         ENDDO
         RETURN
      ENDIF
C
      IF (ISTATUS.EQ.1.AND.KNODE.GT.0) THEN
         DO I=1, 1500
            USRN2(I)=USRN1(I)
         ENDDO
         RETURN
      ENDIF

      RETURN
      END
C********************************************************************

      SUBROUTINE USRSV6

C********************************************************************
C
C     User defined state variable calculations
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4)
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,IELMNOD2
C
      IF (ISTATUS.EQ.1.AND.KELE.GT.0) THEN
         DO I=1, 1500
            USRE2(I)=USRE1(I)
         ENDDO
         RETURN
      ENDIF
C
      IF (ISTATUS.EQ.1.AND.KNODE.GT.0) THEN
         DO I=1, 1500
            USRN2(I)=USRN1(I)
         ENDDO
         RETURN
      ENDIF
      RETURN
      END
C********************************************************************

      SUBROUTINE USRSV7

C********************************************************************
C
C     User defined state variable calculations
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4)
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,IELMNOD2
C
      IF (ISTATUS.EQ.1.AND.KELE.GT.0) THEN
         DO I=1, 1500
            USRE2(I)=USRE1(I)
         ENDDO
         RETURN
      ENDIF
C
      IF (ISTATUS.EQ.1.AND.KNODE.GT.0) THEN
         DO I=1, 1500
            USRN2(I)=USRN1(I)
         ENDDO
         RETURN
      ENDIF

      RETURN
      END
C********************************************************************

      SUBROUTINE USRSV8

C********************************************************************
C
C     User defined state variable calculations
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4)
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,IELMNOD2
C
      IF (ISTATUS.EQ.1.AND.KELE.GT.0) THEN
         DO I=1, 1500
            USRE2(I)=USRE1(I)
         ENDDO
         RETURN
      ENDIF
C
      IF (ISTATUS.EQ.1.AND.KNODE.GT.0) THEN
         DO I=1, 1500
            USRN2(I)=USRN1(I)
         ENDDO
         RETURN
      ENDIF

      RETURN
      END
C********************************************************************

      SUBROUTINE USRSV9

C********************************************************************
C
C     User defined state variable calculations
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4)
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,IELMNOD2
C
      IF (ISTATUS.EQ.1.AND.KELE.GT.0) THEN
         DO I=1, 1500
            USRE2(I)=USRE1(I)
         ENDDO
         RETURN
      ENDIF
C
      IF (ISTATUS.EQ.1.AND.KNODE.GT.0) THEN
         DO I=1, 1500
            USRN2(I)=USRN1(I)
         ENDDO
         RETURN
      ENDIF

      RETURN
      END
C********************************************************************

      SUBROUTINE USRSV10

C********************************************************************
C
C     User defined state variable calculations
C
C********************************************************************

      IMPLICIT INTEGER*4 (I,J,K,L,M,N), REAL*8 (A-H,O-Z)
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
      COMMON /ELMCOM3/ TEPS_NE(4),EFEPS_NE(4)
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
C
      COMMON /NODCOM3/ EFEPS_NN,TEPS_NN,IELMNOD2
C
      IF (ISTATUS.EQ.1.AND.KELE.GT.0) THEN
         DO I=1, 1500
            USRE2(I)=USRE1(I)
         ENDDO
         RETURN
      ENDIF
C
      IF (ISTATUS.EQ.1.AND.KNODE.GT.0) THEN
         DO I=1, 1500
            USRN2(I)=USRN1(I)
         ENDDO
         RETURN
      ENDIF
      RETURN
      END

C********************************************************************

      SUBROUTINE USR_MAXPRN(STS,PRNSTS)
      
C********************************************************************
C
C     THIS SUBROUTINE CALCULATES THE MAXIMUM PRINCIPAL STRESS FOR
C     TWO DIMENSIONAL DEFORMATION
C
C********************************************************************
      
      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
      DIMENSION STS(6)
      
      S3  =  STS(3)

      DNT  =  DSQRT((STS(1)-STS(2))**2 + 4. * STS(4)**2)
      S1  =  ((STS(1) + STS(2)) + DNT) * 0.5
      S2  =  ((STS(1) + STS(2)) - DNT) * 0.5

      PRNSTS  =  S1
      IF(PRNSTS .LT. S2) PRNSTS  =  S2
      IF(PRNSTS .LT. S3) PRNSTS  =  S3

      RETURN
      END


C********************************************************************

      SUBROUTINE USR_MINPRN(STS,PRNSTS)

C********************************************************************
C     
C     THIS SUBROUTINE CALCULATES THE MINIMUM PRINCIPAL STRESS FOR
C     TWO DIMENSIONAL DEFORMATION
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
      DIMENSION STS(6)

      S3  =  STS(3)

      DNT  =  DSQRT((STS(1)-STS(2))**2 + 4. * STS(4)**2)
      S1  =  ((STS(1) + STS(2)) + DNT) * 0.5
      S2  =  ((STS(1) + STS(2)) - DNT) * 0.5

      PRNSTS  =  S1
      IF(PRNSTS .GT. S2) PRNSTS  =  S2
      IF(PRNSTS .GT. S3) PRNSTS  =  S3

      RETURN
      END

C********************************************************************
