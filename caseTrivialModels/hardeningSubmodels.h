
/*! \file hardeningSubmodels.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef hardeningSubmodels_h__
#define hardeningSubmodels_h__
/*	include *********************************************************/
/*	include *********************************************************/

#include "submodels.h"
#include "../modelTemplates/modelsTemplates.h"
#include "../models/inputTypelists.h"
#include "../databus/databusTypelists.h"
#include "../adapter/adapter.h"
#include "../modelTemplates/consoleInputModel.h"
#include "../kbs/kbs.h"

/*	using ***********************************************************/

namespace kbs = am3::kbs;
namespace mot = am3::model::templates;
namespace mor = am3::model::trivial;
namespace db = am3::databus;
namespace ada = am3::adapter;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace trivialModels
    {
      template<typename INPUT>
      struct ConsoleInputModelForHardeningContract2D
        : public mor::ConsoleInputModel < typename HardeningFactorContract2D::VariablesTypes >
        , public mo::InputTypelist < INPUT >
        , public HardeningFactorContract2D
      {
        typedef typename TemperatureInput Inputs;
        typedef typename HardeningFactorContract2D Contract;

        HardeningFactor GetState(mpt::Point<mpr::Coordinates2D> * point, const HardeningFactor *const);
      };

      struct ConstantHardeningModel
        : public mot::ConstantValueModel < HardeningFactor, HardeningFactorContract2D >
      {
        ConstantHardeningModel(const HardeningFactor& hardeningFactor) : mot::ConstantValueModel < HardeningFactor, HardeningFactorContract2D >(hardeningFactor){};
        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> HardeningFactor Get<HardeningFactor>(mpt::Point<mpr::Coordinates2D> * point);
      };

      template<typename INPUT>
      class TempDependentHardeningModel
        : public HardeningFactorContract2D
        , public mo::InputTypelist < INPUT >
      {
      public:
        typedef typename TemperatureInput Inputs;
        typedef typename HardeningFactorContract2D Contract;

        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> HardeningFactor Get<HardeningFactor>(mpt::Point<mpr::Coordinates2D> * point)
        {
          mpr::Temperature temperature;
          temperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          HardeningFactor toRet;
          toRet(temperature() * 2.0);
          return toRet;
        }
        virtual HardeningFactor GetState(pnt::Point<mpr::Coordinates2D> *point, const HardeningFactor *const)
        {
          return Get<HardeningFactor>(point);
        }
      };

      template<typename INPUT>
      class StrainTempDependentHardeningModel
        : public HardeningFactorContract2D
        , public mo::InputTypelist < INPUT >
      {
      public:
        typedef typename HardeningFactorTemperatureInput Inputs;
        typedef typename HardeningFactorContract2D Contract;

        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> HardeningFactor Get<HardeningFactor>(mpt::Point<mpr::Coordinates2D> * point)
        {
          mpr::Temperature temperature;
          mpr::EffectiveStrainRate effStrainRate;
          temperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          effStrainRate = bus_->GetFromDatabus<mpr::EffectiveStrainRate>(point);
          HardeningFactor toRet;
          toRet(effStrainRate() * temperature() * 2.0);
          return toRet;
        }
        virtual HardeningFactor GetState(pnt::Point<mpr::Coordinates2D> *point, const HardeningFactor *const)
        {
          return Get<HardeningFactor>(point);
        }
      };



      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////


      template<typename INPUT>
      HardeningFactor ConsoleInputModelForHardeningContract2D<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point, const HardeningFactor *const)
      {
        return ConsoleInputModel < typename HardeningFactorContract2D::VariablesTypes>::Get<HardeningFactor>(point);
      }

    } //trivialModels
  } //cases
} //am3
#endif // hardeningSubmodels_h__
