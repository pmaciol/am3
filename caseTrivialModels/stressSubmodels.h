
/*! \file stressSubmodels.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef stressSubmodels_h__
#define stressSubmodels_h__
/*	include *********************************************************/

#include "../modelTemplates/modelsTemplates.h"
#include "../models/inputTypelists.h"
#include "../databus/databusTypelists.h"
#include "../adapter/adapter.h"
#include "../modelTemplates/consoleInputModel.h"
#include "../kbs/kbs.h"
#include "submodels.h"

/*	using ***********************************************************/

namespace kbs = am3::kbs;
namespace mot = am3::model::templates;
namespace mor = am3::model::trivial;
namespace db = am3::databus;
namespace ada = am3::adapter;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace trivialModels
    {
      template<typename INPUT>
      struct ConsoleInputModelForYieldStressContract2D
        : public mor::ConsoleInputModel < typename YieldStressContract2D::VariablesTypes >
        , public mo::InputTypelist < INPUT >
        , public YieldStressContract2D
      {
        typedef typename TemperatureInput Inputs;
        typedef typename YieldStressContract2D Contract;

        mpr::YieldStress GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::YieldStress *const);
      };

      struct ConstantStressModel
        : public mot::ConstantValueModel < mpr::YieldStress, YieldStressContract2D >
      {
        ConstantStressModel(const mpr::YieldStress& ystress) : mot::ConstantValueModel < mpr::YieldStress, YieldStressContract2D >(ystress){};
        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> mpr::YieldStress Get<mpr::YieldStress>(mpt::Point<mpr::Coordinates2D> * point);
      };


      template<typename INPUT>
      class TempDependentStressModel
        : public YieldStressContract2D
        , public mo::InputTypelist < INPUT >
      {
      public:
        typedef typename TemperatureInput Inputs;
        typedef typename YieldStressContract2D Contract;

        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> mpr::YieldStress Get<mpr::YieldStress>(mpt::Point<mpr::Coordinates2D> * point)
        {
          mpr::Temperature temperature;
          temperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          mpr::YieldStress toRet;
          toRet(temperature() * 5.0);
          return toRet;
        }
        virtual mpr::YieldStress GetState(pnt::Point<mpr::Coordinates2D> *point, const mpr::YieldStress *const)
        {
          return Get<mpr::YieldStress>(point);
        }
      };

      template<typename INPUT>
      class HardeningTempDependentStressModel
        : public YieldStressContract2D
        , public mo::InputTypelist < INPUT >
      {
      public:
        typedef typename StrainTemperatureInput Inputs;
        typedef typename YieldStressContract2D Contract;

        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> mpr::YieldStress Get<mpr::YieldStress>(mpt::Point<mpr::Coordinates2D> * point)
        {
          mpr::Temperature temperature;
          HardeningFactor hardFactor;
          temperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          hardFactor = bus_->GetFromDatabus<HardeningFactor>(point);
          mpr::YieldStress toRet;
          toRet(hardFactor() * temperature() * 5.0);
          return toRet;
        }
        virtual mpr::YieldStress GetState(pnt::Point<mpr::Coordinates2D> *point, const mpr::YieldStress *const)
        {
          return Get<mpr::YieldStress>(point);
        }
      };


      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////




      template<typename INPUT>
      mpr::YieldStress ConsoleInputModelForYieldStressContract2D<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::YieldStress *const)
      {
        return am3::model::trivial::ConsoleInputModel < typename YieldStressContract2D::VariablesTypes>::Get<mpr::YieldStress>(point);
      }

      template<typename VAR>
      VAR ConstantStressModel::Get(mpt::Point<mpr::Coordinates2D> * point)
      {
        static_assert(false, "Getting here means ther is no expected variable in the typelist");
        return mot::ConstantValueModel < mpr::YieldStress, YieldStressContract2D >::variable_;
      }

      template<>
      mpr::YieldStress ConstantStressModel::Get<mpr::YieldStress>(mpt::Point<mpr::Coordinates2D> * point)
      {
        return mot::ConstantValueModel < mpr::YieldStress, YieldStressContract2D >::variable_;
      };

    } //trivialModels
  } //cases
} //am3
#endif // stressSubmodels_h__
