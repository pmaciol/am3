#include "caseTrivialModels.h"
#include "../models/dataStructures.h"
#include "../models/point.h"

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;



#ifdef CASETRIVIAL
int main(int argc, char **argv)
{
  am3::cases::trivialModels::Main main;
}
#endif



namespace am3
{
	namespace cases
	{
		namespace trivialModels
		{

      using namespace am3::databus;
		//////////////////////////////////////////////////////////////////////////

			Main::Main()
			{
        char stop[1] = { 'n' };
        while ('y' != *stop)
        {
          const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
          const mpr::Time t = mpr::Time(0.0);
          mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);


          boost::shared_ptr<DatabusHardening> db(new DatabusHardening);
          boost::shared_ptr<kbs::DatabusRequester<DatabusHardening> >  dbr(new kbs::DatabusRequester<DatabusHardening>);
          boost::shared_ptr<Kbs> kbs(new Kbs(dbr));
          dbr->SetDatabus(db);


          typedef ModelInstanceType<ConsoleInputModelForMacroInputsContract2D, DatabusHardening>::Type MacroModel;
          boost::shared_ptr<MacroModel> macroModel(new MacroModel);
          SetModelInstance<DatabusHardening, ConsoleInputModelForMacroInputsContract2D>(db.get(), macroModel);

          //   typedef ModelInstanceType<ConsoleInputModelForMacroInputsContract2D, DatabusHardening>::Type MacroModel;
          //   boost::shared_ptr<MacroModel> macroModel(new MacroModel);
          //   SetModelInstance<DatabusHardening, ConsoleInputModelForMacroInputsContract2D>(db.get(), macroModel);

          typedef ModelInstanceType<SwitcherYStress, DatabusHardening>::Type YieldStressSwitcher;
          boost::shared_ptr<YieldStressSwitcher> yieldStressSwitcher(new YieldStressSwitcher(kbs));
          SetModelInstance<DatabusHardening, SwitcherYStress>(db.get(), yieldStressSwitcher);

          typedef ModelInstanceType<SwitcherHardening, DatabusHardening>::Type HardeningSwitcher;
          boost::shared_ptr<HardeningSwitcher> hardeningSwitcher(new HardeningSwitcher(kbs));
          SetModelInstance<DatabusHardening, SwitcherHardening>(db.get(), hardeningSwitcher);


          boost::shared_ptr<MacroModelHand> macroModelHand(new MacroModelHand);
          boost::shared_ptr<YieldModelHand> yModelHand(new YieldModelHand);
          boost::shared_ptr<HardeningModelHand> hardeningModelHand(new HardeningModelHand);

          boost::shared_ptr<ConstantMacroModel> macroModelConst(new ConstantMacroModel(Property<mpr::Temperature>(0.1), Property<mpr::EffectiveStrainRate>(10), Property<mpr::StepLength>(100)));
          boost::shared_ptr<ConstantStressModel> yModelConst(new ConstantStressModel(Property<mpr::YieldStress>(1e6)));
          boost::shared_ptr<ConstantHardeningModel> hardeningModelConst(new ConstantHardeningModel(Property<HardeningFactor>(2.0)));

          boost::shared_ptr<TempDependentHardeningModel<DatabusHardening> > hardeningModelTemp(new TempDependentHardeningModel<DatabusHardening>);
          boost::shared_ptr<StrainTempDependentHardeningModel<DatabusHardening> > hardeningModelStrainTemp(new StrainTempDependentHardeningModel<DatabusHardening>);

          boost::shared_ptr<TempDependentStressModel<DatabusHardening> > stressModelTemp(new TempDependentStressModel<DatabusHardening>);
          boost::shared_ptr<HardeningTempDependentStressModel<DatabusHardening> > stressModelHardeningTemp(new HardeningTempDependentStressModel<DatabusHardening>);

          //   yModelHand->SetModelInput(db.get());
          //   macroModelHand->SetModelInput(db.get());
          //  hardeningModelHand->SetModelInput(db.get());
          hardeningModelTemp->SetModelInput(db.get());
          hardeningModelStrainTemp->SetModelInput(db.get());
          stressModelTemp->SetModelInput(db.get());
          stressModelHardeningTemp->SetModelInput(db.get());

          yieldStressSwitcher->AddModel(yModelHand);
          yieldStressSwitcher->AddModel(yModelConst);
          yieldStressSwitcher->AddModel(stressModelTemp);
          yieldStressSwitcher->AddModel(stressModelHardeningTemp);

          hardeningSwitcher->AddModel(hardeningModelHand);
          hardeningSwitcher->AddModel(hardeningModelConst);
          hardeningSwitcher->AddModel(hardeningModelTemp);
          hardeningSwitcher->AddModel(hardeningModelStrainTemp);


          mpr::YieldStress ys;
          //   ys = yModelHand->Get<mpr::YieldStress>(&point);
          //   ys = yModelConst->Get<mpr::YieldStress>(&point);
          ys = db->GetFromDatabus<mpr::YieldStress>(&point);

          //   HardeningFactor hard;
          //   hard = db->GetFromDatabus<HardeningFactor>(&point);
          std::cout << "Finished? (y/n): ";
          std::cin >> stop;

        }

			}

		} //namespace trivialModels
	}
}