
/*! \file caseColdRolling.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseTrivialModels_h__
#define caseTrivialModels_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../databus/databus.h"
#include "../caseColdRolling/databus.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "../adapter/ivstorage.h"
#include "../caseSimplified/macroModel.h"
//#include "defs.h"
#include "../caseColdRolling/caseColdRolling.h"
#include "submodels.h"
#include "hardeningSubmodels.h"
#include "macroSubmodels.h"
#include "stressSubmodels.h"



#include "../models/models.h"


namespace am3
{
  namespace cases
  {
    namespace trivialModels
    {

      typedef TEMPLATELIST_3(SwitcherYStress, SwitcherHardening, ConsoleInputModelForMacroInputsContract2D) StressHardeningMacroList;

      typedef db::DatabusTypelists<StressHardeningMacroList> DatabusHardening;

      typedef db::ModelInstanceType<ConsoleInputModelForYieldStressContract2D, DatabusHardening>::Type YieldModelHand;
      typedef db::ModelInstanceType<ConsoleInputModelForMacroInputsContract2D, DatabusHardening>::Type MacroModelHand;
      typedef db::ModelInstanceType<ConsoleInputModelForHardeningContract2D, DatabusHardening>::Type HardeningModelHand;

      //////////////////////////////////////////////////////////////////////////

      struct Main
      {
        Main();
      };
        

    } //namespace trivialModels
  }
}


#endif // caseTrivialModels_h__
