
/*! \file macroSubmodels.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef macroSubmodels_h__
#define macroSubmodels_h__
/*	include *********************************************************/

#include "../modelTemplates/modelsTemplates.h"
#include "../models/inputTypelists.h"
#include "../databus/databusTypelists.h"
#include "../adapter/adapter.h"
#include "../modelTemplates/consoleInputModel.h"
#include "../kbs/kbs.h"
#include "submodels.h"

/*	using ***********************************************************/

namespace kbs = am3::kbs;
namespace mot = am3::model::templates;
namespace mor = am3::model::trivial;
namespace db = am3::databus;
namespace ada = am3::adapter;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace trivialModels
    {
      
      template<typename INPUT>
      struct ConsoleInputModelForMacroInputsContract2D
        : public mor::ConsoleInputModel < typename MacroInputsContract2D::VariablesTypes>
        , public mo::InputTypelist < INPUT >
        , public MacroInputsContract2D
      {
        typedef typename EmptyContract2D Inputs;
        typedef typename MacroInputsContract2D Contract;

        mpr::Temperature  GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::Temperature *const);
        mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::EffectiveStrainRate *const);
        mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StepLength *const);
      };

      class ConstantMacroModel
        : public mot::ConstantValueModel < mpr::Temperature, MacroInputsContract2D >
        , public mot::ConstantValueModel < mpr::EffectiveStrainRate, MacroInputsContract2D >
        , public mot::ConstantValueModel < mpr::StepLength, MacroInputsContract2D >
      {
      public:
        typedef EmptyContract2D Inputs;
        typedef mot::ConstantValueModel < mpr::Temperature, MacroInputsContract2D >::Contract Contract;
        //        typedef boost::shared_ptr<mot::ConstantValueModel < mpr::Temperature, MacroInputsContract2D > > CastToContract;

        ConstantMacroModel(const mpr::Temperature& temperature, const mpr::EffectiveStrainRate& effStrain, const mpr::StepLength& stepLength);
        mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::EffectiveStrainRate *const);
        mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StepLength *const);
        mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::Temperature *const);
        template<typename VAR> VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> mpr::Temperature Get<mpr::Temperature>(mpt::Point<mpr::Coordinates2D> * point);
        template<> mpr::EffectiveStrainRate Get<mpr::EffectiveStrainRate>(mpt::Point<mpr::Coordinates2D> * point);
        template<> mpr::StepLength Get<mpr::StepLength>(mpt::Point<mpr::Coordinates2D> * point);
      };




      
      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////



      template<typename INPUT>
      mpr::StepLength am3::cases::trivialModels::ConsoleInputModelForMacroInputsContract2D<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StepLength *const)
      {
        return Get<mpr::StepLength>(point);
      }

      template<typename INPUT>
      mpr::EffectiveStrainRate am3::cases::trivialModels::ConsoleInputModelForMacroInputsContract2D<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::EffectiveStrainRate *const)
      {
        return Get<mpr::EffectiveStrainRate>(point);
      }

      template<typename INPUT>
      mpr::Temperature am3::cases::trivialModels::ConsoleInputModelForMacroInputsContract2D<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::Temperature *const)
      {
        return Get<mpr::Temperature>(point);
      }

      ConstantMacroModel::ConstantMacroModel(const mpr::Temperature& temperature, const mpr::EffectiveStrainRate& effStrain, const mpr::StepLength& stepLength)
        : mot::ConstantValueModel < mpr::Temperature, MacroInputsContract2D >(temperature)
        , mot::ConstantValueModel < mpr::EffectiveStrainRate, MacroInputsContract2D >(effStrain)
        , mot::ConstantValueModel < mpr::StepLength, MacroInputsContract2D >(stepLength)
      {};

      mpr::EffectiveStrainRate ConstantMacroModel::GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::EffectiveStrainRate *const)
      {
        return Get<mpr::EffectiveStrainRate>(point);
      }

      mpr::StepLength ConstantMacroModel::GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StepLength *const)
      {
        return Get<mpr::StepLength>(point);
      }

      mpr::Temperature ConstantMacroModel::GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::Temperature *const)
      {
        return Get<mpr::Temperature>(point);
      }

      template<typename VAR>
      VAR ConstantMacroModel::Get(mpt::Point<mpr::Coordinates2D> * point)
      {
        static_assert(false, "Getting here means ther is no expected variable in the typelist");
        VAR toRet;
        return toRet;
      }

      template<>
      mpr::Temperature ConstantMacroModel::Get<mpr::Temperature>(mpt::Point<mpr::Coordinates2D> * point)
      {
        return mot::ConstantValueModel < mpr::Temperature, MacroInputsContract2D >::variable_;
      };
      template<>
      mpr::EffectiveStrainRate ConstantMacroModel::Get<mpr::EffectiveStrainRate>(mpt::Point<mpr::Coordinates2D> * point) { return mot::ConstantValueModel < mpr::EffectiveStrainRate, MacroInputsContract2D >::variable_; };
      template<>
      mpr::StepLength ConstantMacroModel::Get<mpr::StepLength>(mpt::Point<mpr::Coordinates2D> * point) { return mot::ConstantValueModel < mpr::StepLength, MacroInputsContract2D >::variable_; };

    } //trivialModels
  } //cases
} //am3
#endif // macroSubmodels_h__
