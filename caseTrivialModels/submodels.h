
/*! \file submodels.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef submodels_h__
#define submodels_h__
/*	include *********************************************************/

#include "../modelTemplates/modelsTemplates.h"
#include "../models/inputTypelists.h"
#include "../databus/databusTypelists.h"
#include "../adapter/adapter.h"
#include "../modelTemplates/consoleInputModel.h"
#include "../kbs/kbs.h"

/*	using ***********************************************************/

namespace kbs = am3::kbs;
namespace mot = am3::model::templates;
namespace mor = am3::model::trivial;
namespace db  = am3::databus;
namespace ada = am3::adapter;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace trivialModels
		{
      // Defining local contracts; mainly for testing purposes
      SIMPLE_PROP(HardeningFactor, double);

      typedef moc::Contract<TYPELIST_TUPLE_3(mpr::Temperature, mpr::EffectiveStrainRate, mpr::StepLength), mpr::Coordinates2D> MacroInputsContract2D;
      typedef moc::Contract<TYPELIST_TUPLE_1(mpr::YieldStress), mpr::Coordinates2D> YieldStressContract2D;
      typedef moc::Contract<TYPELIST_TUPLE_1(EmptyType), mpr::Coordinates2D> EmptyContract2D;
      typedef moc::Contract<TYPELIST_TUPLE_1(HardeningFactor), mpr::Coordinates2D> HardeningFactorContract2D;
      typedef moc::Contract<TYPELIST_TUPLE_2(mpr::EffectiveStrainRate, mpr::Temperature), mpr::Coordinates2D> StrainTemperatureInput;
      typedef moc::Contract<TYPELIST_TUPLE_2(HardeningFactor, mpr::Temperature), mpr::Coordinates2D> HardeningFactorTemperatureInput;
      typedef moc::Contract<TYPELIST_TUPLE_1(mpr::Temperature), mpr::Coordinates2D> TemperatureInput;

 
      // Kbs system
      typedef kbs::KbsStringByHand<kbs::KbsStringInterface> Kbs;

      // Switchers

      template <typename INPUT>
      struct SwitcherYStress
        : public ada::Switcher<Kbs, YieldStressContract2D>
        , public mo::InputTypelist < INPUT >
      {
        SwitcherYStress(boost::shared_ptr<Kbs> kbsystem) : ada::Switcher<Kbs, YieldStressContract2D>(kbsystem){};
        typedef EmptyContract2D Inputs;
        typedef typename YieldStressContract2D Contract;
      };

      template <typename INPUT>
      struct SwitcherHardening
        : public ada::Switcher<Kbs, HardeningFactorContract2D>
        , public mo::InputTypelist < INPUT >
      {
        SwitcherHardening(boost::shared_ptr<Kbs> kbsystem) : ada::Switcher<Kbs, HardeningFactorContract2D>(kbsystem){};
        typedef EmptyContract2D Inputs;
        typedef typename HardeningFactorContract2D Contract;
      };

    } //trivialModels
	} //cases
} //am3
#endif // submodels_h__
