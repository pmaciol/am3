/*!
 * \file communicationStream.h
 *
 * \author Kazimierz Michalik
 * \date 08.05.2013
 *
 *\brief 
 *
 */
#ifndef COMMUNICATION_STREAM_H_
#define COMMUNICATION_STREAM_H_

#include <cassert>
#include <istream>
#include <ostream>

#include "communicationStreamBase.h"

////////////////////////////////////////////////////////
/////////////////// MACROS /////////////////////////////
////////////////////////////////////////////////////////
/// The macros below are for the purpose 
/// of interfacing with pure "C" or Fortran.
////////////////////////////////////////////////////////
#define MAKE_C_FUNCTIONS_FOR_TYPE_DECLARATIONS(STREAM_CLASS_NAME_T,TYPE) \
	int am3##STREAM_CLASS_NAME_T##Write##TYPE(const int * streamID, const TYPE * val); \
	int am3##STREAM_CLASS_NAME_T##Read##TYPE(const int * streamID, TYPE * val); 

#define MAKE_C_FUNCTIONS_FOR_STREAM_DECLARATIONS(STREAM_CLASS_NAME,STREAM_DATA_STRUCT_NAME) \
	int am3Create##STREAM_CLASS_NAME(STREAM_DATA_STRUCT_NAME * data) ;   \
	void am3Destroy##STREAM_CLASS_NAME(const int * streamID) ; \
	int am3Good##STREAM_CLASS_NAME(const int * streamID); \
	int am3##STREAM_CLASS_NAME##WriteCString(const int * streamID, const char * str, int str_len); \
	int am3##STREAM_CLASS_NAME##ReadCString(const int * streamID, char * str, int str_len) ; \
	MAKE_C_FUNCTIONS_FOR_TYPE_DECLARATIONS(STREAM_CLASS_NAME,int)   \
	MAKE_C_FUNCTIONS_FOR_TYPE_DECLARATIONS(STREAM_CLASS_NAME,double) \
	MAKE_C_FUNCTIONS_FOR_TYPE_DECLARATIONS(STREAM_CLASS_NAME,float) \
	MAKE_C_FUNCTIONS_FOR_TYPE_DECLARATIONS(STREAM_CLASS_NAME,short) \
	MAKE_C_FUNCTIONS_FOR_TYPE_DECLARATIONS(STREAM_CLASS_NAME,long) 

///////////////// DEFINITIONS ////////////////////////

#define MAKE_C_FUNCTIONS_FOR_TYPE(STREAM_CLASS_NAME_T,TYPE) \
	int am3##STREAM_CLASS_NAME_T##Write##TYPE(const int * streamID, const TYPE * val) { return STREAM_CLASS_NAME_T##Ptr[*streamID]->write(*val); } \
	int am3##STREAM_CLASS_NAME_T##Read##TYPE(const int * streamID, TYPE * val) { return STREAM_CLASS_NAME_T##Ptr[*streamID]->read(*val); } 

#define MAKE_C_FUNCTIONS_FOR_STREAM(STREAM_CLASS_NAME,STREAM_DATA_STRUCT_NAME) \
	std::vector<STREAM_CLASS_NAME *> STREAM_CLASS_NAME##Ptr(1); \
	int am3Create##STREAM_CLASS_NAME(STREAM_DATA_STRUCT_NAME * data) {  \
		STREAM_CLASS_NAME##Ptr.push_back(new STREAM_CLASS_NAME(*data)); return STREAM_CLASS_NAME##Ptr.size()-1; } ;   \
	void am3Destroy##STREAM_CLASS_NAME(const int * streamID) { if(STREAM_CLASS_NAME##Ptr[*streamID] != NULL) delete STREAM_CLASS_NAME##Ptr[*streamID];  \
		STREAM_CLASS_NAME##Ptr.erase(STREAM_CLASS_NAME##Ptr.begin() + *streamID); } ; \
	int am3Good##STREAM_CLASS_NAME(const int * streamID) { return STREAM_CLASS_NAME##Ptr[*streamID]->good(); } \
	int am3##STREAM_CLASS_NAME##WriteCString(const int * streamID, const char * str, int str_len) { return STREAM_CLASS_NAME##Ptr[*streamID]->write(str); } \
	int am3##STREAM_CLASS_NAME##ReadCString(const int * streamID, char * str, int str_len) { return STREAM_CLASS_NAME##Ptr[*streamID]->read(str); } \
	MAKE_C_FUNCTIONS_FOR_TYPE(STREAM_CLASS_NAME,int)   \
	MAKE_C_FUNCTIONS_FOR_TYPE(STREAM_CLASS_NAME,double) \
	MAKE_C_FUNCTIONS_FOR_TYPE(STREAM_CLASS_NAME,float) \
	MAKE_C_FUNCTIONS_FOR_TYPE(STREAM_CLASS_NAME,short) \
	MAKE_C_FUNCTIONS_FOR_TYPE(STREAM_CLASS_NAME,long) 

////////////////////////////////////////////////////////
//////////////// END OF MACROS /////////////////////////
////////////////////////////////////////////////////////

using am3::common::communication::callbackFunPtr;

namespace am3{ namespace common { namespace communication {

template<class TMiddlewarePolicy>
class communicationStream //: public CommunicationStreamBase
{
public:
	typedef typename TMiddlewarePolicy::initData initData;

	
	communicationStream(const initData & lowerLayerInitData)
						: lowerLayer(lowerLayerInitData)
					
	{
	}
	
	virtual ~communicationStream()
	{
		close();
	}
	
	// This operator makes stream callable - can be run in other thread 
	void operator()()
	{
		lowerLayer.run();
	}

	template<typename T> bool write(const T & value, callbackFunPtr f = NULL)
	{
		bool result=false;
		if(f!=NULL){
			if(TMiddlewarePolicy::asyncWriteSupported) {
				result = lowerLayer.writeAsync(value,f);
			}
		}
		else{
			assert(TMiddlewarePolicy::syncWriteSupported);
			result = lowerLayer.write(value);
		}
		return result;
	}
	template<typename T> bool read(T & value, callbackFunPtr f = NULL)
	{
		bool result=false;
		if(f!=NULL && TMiddlewarePolicy::asyncReadSupported) {
			result = lowerLayer.readAsync(value,f);
		}
		else{
			assert(TMiddlewarePolicy::syncReadSupported);
			result = lowerLayer.read(value);
		}
		return result;
	}
	
	bool good() const 
	{
		return lowerLayer.good();
	}

	void close()
	{
		lowerLayer.destroy();
	}

protected:
		TMiddlewarePolicy lowerLayer;
};

template<class TMiddlewarePolicy, typename T>
communicationStream<TMiddlewarePolicy>& operator<<(communicationStream<TMiddlewarePolicy> & stream, const T & value) 
{
	if(!stream.write(value))
	{
		throw communicationException();
	}
	return stream;
}

template<class TMiddlewarePolicy, typename T>
communicationStream<TMiddlewarePolicy>& operator>>(communicationStream<TMiddlewarePolicy> & stream, T & value)
{
	if(!stream.read(value))
	{
		throw communicationException();
	}
	return stream;
}

}}} //! namespace am3.common.communication

#endif //!COMMUNICATION_STREAM_H_