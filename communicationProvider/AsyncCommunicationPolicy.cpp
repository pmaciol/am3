#ifndef ASYNC_COMMUNICATION_POLICY_
#define ASYNC_COMMUNICATION_POLICY_

#include "communicationProvider.h"

class asyncCommunicationPolicy
{
public:
	static const bool asyncSupported = true;
	static const bool syncSupported = true;
	
	template<class TLowerLayer>
	bool register(communicationProvider::callbackFunPtr f);
					
};

#endif //!SYNC_COMMUNICATION_POLICY_