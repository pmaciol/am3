/*!
 * \file predefinedStreams.h
 *
 * \author Kazimierz Michalik
 * \date 08.05.2013
 *
 *\brief 
 *
 */
#ifndef PREDEFINED_STREAMS_H_
#define PREDEFINED_STREAMS_H_

#include "communicationStream.h"

#include "cin_cout_Translator.h"
#include "fstreamTranslator.h"
#include "winsocketTranslator.h"
#include <vector>

namespace am3 { namespace common { namespace communication {

	typedef communicationStream<cin_cout_Translator> KeyboardAndScreenStream;
	typedef communicationStream<fstreamTranslator> FileStream;
	typedef communicationStream<winsocketTranslator> Winsock2Stream;

}}} // namespaces

using namespace am3::common::communication;

extern "C" {

	MAKE_C_FUNCTIONS_FOR_STREAM_DECLARATIONS(Winsock2Stream,winsocketTranslator_data);

}

#endif //! PREDEFINED_STREAMS_H_