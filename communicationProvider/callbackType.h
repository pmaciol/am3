/*!
 * \file callbackType.h
 *
 * \author Kazimierz Michalik
 * \date 08.05.2013
 *
 *\brief 
 *
 */
#ifndef CALLBACK_TYPE_
#define CALLBACK_TYPE_

namespace am3{ namespace common { namespace communication {
	typedef void (*callbackFunPtr)(communicationException& e);
}}} // !namespace

#endif //! CALLBACK_TYPE_