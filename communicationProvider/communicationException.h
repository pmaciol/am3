/*!
 * \file communicationException.h
 *
 * \author Kazimierz Michalik
 * \date 08.05.2013
 *
 *\brief 
 *
 */
#ifndef COMMUNICATION_EXCEPTION_H_
#define COMMUNICATION_EXCEPTION_H_

#include <exception>

class communicationException : public std::exception
{

};

#endif //!COMMUNICATION_EXCEPTION_H_