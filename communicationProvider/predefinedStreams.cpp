/*!
 * \file communicationStream.h
 *
 * \author Kazimierz Michalik
 * \date 08.05.2013
 *
 *\brief 
 *
 */
#include "predefinedStreams.h"

using namespace am3::common::communication;

extern "C" {

	MAKE_C_FUNCTIONS_FOR_STREAM(Winsock2Stream,winsocketTranslator_data);

}