/*!
 * \file winsocketTranslator.h
 *
 * \author Kazimierz Michalik
 * \date 08.05.2013
 *
 *\brief 
 *
 */
#ifndef WINSOCKET_TRANSLATOR_H_
#define WINSOCKET_TRANSLATOR_H_

#include "communicationException.h"
#include "callbackType.h"

#include <string>
#include <sstream>
#include <assert.h>

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

extern "C" {
	struct winsocketTranslator_data
	{
		static const int SERVER_MODE=1, 
						CLIENT_MODE=0,
						INVALID_MODE=-1;
		int operatingMode;
		char * serverPort;
		char * hostname;
		int bufferSize;
	};

}

namespace am3{ namespace common { namespace communication { 

class winsocketTranslator
{
public:
	typedef winsocketTranslator_data initData;
	static const bool asyncWriteSupported = true;
	static const bool syncWriteSupported = true;
	static const bool asyncReadSupported = true;
	static const bool syncReadSupported = true;

	winsocketTranslator(const initData & data) : 
	data_(data),listenSocket(INVALID_SOCKET), clientSocket(INVALID_SOCKET), underDestruction(false)
	{

		if(data.operatingMode == initData::SERVER_MODE) {
			initSocketServer();
		}
		else if(data.operatingMode == initData::CLIENT_MODE) {
			initSocketClient();
		}
	} //! end of constructor

	~winsocketTranslator(){
		destroy();
	}
	
	void run()
	{
		waitForClient();
	}

	bool write(const char buf[]) {
		assert(buf != NULL);
		waitForClient();
		std::ostringstream oss;
		oss << "(" << (int)strlen(buf) << ")" << buf;
		// Send an initial buffer
		int iResult = send( clientSocket, oss.str().c_str(), oss.str().length(), 0 );
		if (iResult == SOCKET_ERROR) {
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(clientSocket);
			WSACleanup();
		}
		//printf("Bytes Sent: %ld with content: %s\n", iResult,oss.str().c_str() );
		return good();
	}

	template<typename T>
	bool write(const T& val) {
		std::ostringstream oss;
		oss << val;
		return write(oss.str().c_str());
	}
	
	bool read( char* buff ) {
		assert(buff != NULL);
		assert(good());
		waitForClient();
		//printf("read: recieving data.\n");
		int iResult(0),headerSize(0);
		char * next_byte(buff);
		do{
			assert(next_byte != NULL);
			iResult = recv(clientSocket, next_byte, 1, 0);
			++headerSize;
		}while( *(next_byte++) != ')');

		//printf("Bytes received (header): %d with content: %s\n", headerSize,buff);

		std::istringstream iss(buff);
		iss.ignore();
		int length(0); // "("
		iss >> length;  // sizeof communicate
		iss.ignore();  // ")"

		memset(buff,0,headerSize);

		iResult = recv(clientSocket, buff, length, 0);
		if(iResult > 0) {
			//printf("Bytes received: %d with content: %s\n", iResult,buff);
			memmove(buff,buff+(iResult-length),length+1); // +1 to move also 0(null) which ends string
		}
		return good();
	}

	template<typename T>
	bool read(T& val) {
		assert(good());
		char * buf = new char[data_.bufferSize];
		memset(buf,0,data_.bufferSize);
		bool result = read(buf) ; 
		if(result) {
			std::istringstream iss(buf);
			iss >>val;
		}
		delete [] buf;
		return result;
	}

	template<typename T>
	bool readAsync(T&, callbackFunPtr) {
		waitForClient();
		//TODO:
		return good();
	}

	template<typename T>
	bool writeAsync(const T&,callbackFunPtr) {
		waitForClient();
				//TODO:
		return good();
	}

	void destroy()
	{
		if(!underDestruction) {
			underDestruction = true;
			char buf[1000]={0};
			int iResult(0);

			if(data_.operatingMode == initData::SERVER_MODE 
				&& listenSocket != INVALID_SOCKET
				&& clientSocket != INVALID_SOCKET) {
				printf("Server shutdown begins...\n");
				// No longer need server socket
				closesocket(listenSocket);

				// Receive until the peer shuts down the connection
				do {

					iResult = recv(clientSocket, buf, data_.bufferSize, 0);
					if (iResult > 0) {
						printf("Server shutdown: Bytes received: %d\n", iResult);

						// Echo the buffer back to the sender
						int iSendResult = send( clientSocket, buf, iResult, 0 );
						if (iSendResult == SOCKET_ERROR) {
							printf("Server shutdown: send failed with error: %d\n", WSAGetLastError());
							closesocket(clientSocket);
							WSACleanup();
						}
						printf("Server shutdown: Bytes sent: %d\n", iSendResult);
					}
					else if (iResult == 0)
						printf("Server shutdown: Connection closing...\n");
					else  {
						printf("Server shutdown: recv failed with error: %d\n", WSAGetLastError());
						closesocket(clientSocket);
						WSACleanup();
					}

				} while (iResult > 0);

				// shutdown the connection since we're done
				iResult = shutdown(clientSocket, SD_SEND);
				if (iResult == SOCKET_ERROR) {
					printf("Server shutdown: shutdown failed with error: %d\n", WSAGetLastError());
					closesocket(clientSocket);
					WSACleanup();
				}

				// cleanup
				closesocket(clientSocket);
				WSACleanup();
				printf("...server shutdown ends.\n");
			}
			else if(data_.operatingMode == initData::CLIENT_MODE 
				&& clientSocket != INVALID_SOCKET ) {
				printf("Client shutdown begins...\n");
				// shutdown the connection since no more data will be sent
				iResult = shutdown(clientSocket, SD_SEND);
				if (iResult == SOCKET_ERROR) {
					printf("Client shutdown: shutdown failed with error: %d\n", WSAGetLastError());
					closesocket(clientSocket);
					WSACleanup();
				}

				// Receive until the peer closes the connection
				do {
					iResult = recv(clientSocket, buf, data_.bufferSize, 0);
					if ( iResult > 0 )
						printf("Client shutdown: Bytes received: %d\n", iResult);
					else if ( iResult == 0 )
						printf("Client shutdown: Connection closed\n");
					else
						printf("Client shutdown: recv failed with error: %d\n", WSAGetLastError());

				} while( iResult > 0 );

				// cleanup
				closesocket(clientSocket);
				WSACleanup();
				printf("...client shutdown ends.\n");
			}
			clientSocket = INVALID_SOCKET;
			listenSocket = INVALID_SOCKET;
			underDestruction = false;
			
		}// !underDestruction?
		else {
			printf("destroy: already under destruction! \n");
		}
	}

	bool good() const 
	{
		bool result=false;
		if(data_.operatingMode == initData::SERVER_MODE) {
			result = (listenSocket != INVALID_SOCKET);
		}
		else if(data_.operatingMode == initData::CLIENT_MODE) {
			result = (clientSocket != INVALID_SOCKET);
		}
		return result;
	}

protected:
	const initData data_;
	SOCKET listenSocket,clientSocket;
	volatile bool underDestruction;

	// blocking copy-constructor
	winsocketTranslator(const winsocketTranslator & other);

	void initSocketServer()
	{
		// Initialize Winsock
		WSADATA wsaData;
		int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
		if (iResult != 0) {
			printf("WSAStartup failed with error: %d \n", iResult);
		}

		struct addrinfo *result = NULL, hints;
		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_flags = AI_PASSIVE;

		// Resolve the server address and port
		iResult = getaddrinfo(NULL, data_.serverPort, &hints, &result);
		if ( iResult != 0 ) {
			printf("getaddrinfo failed with error: %d\n", iResult);
			WSACleanup();
		}

		// Create a SOCKET for connecting to server
		listenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
		if (listenSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			freeaddrinfo(result);
			WSACleanup();
		}

		// Setup the TCP listening socket
		iResult = bind( listenSocket, result->ai_addr, (int)result->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			printf("bind failed with error: %d\n", WSAGetLastError());
			freeaddrinfo(result);
			closesocket(listenSocket);
			WSACleanup();
		}

		freeaddrinfo(result);

		iResult = listen(listenSocket, SOMAXCONN);
		if (iResult == SOCKET_ERROR) {
			printf("listen failed with error: %d\n", WSAGetLastError());
			closesocket(listenSocket);
			WSACleanup();
		}
	}

	void initSocketClient()
	{
		WSADATA wsaData;
		struct addrinfo *result = NULL,
						*ptr = NULL,
						hints;

		// Initialize Winsock
		int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
		if (iResult != 0) {
			printf("WSAStartup failed with error: %d\n", iResult);
		}

		ZeroMemory( &hints, sizeof(hints) );
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;

		// Resolve the server address and port
		iResult = getaddrinfo(data_.hostname, data_.serverPort, &hints, &result);
		if ( iResult != 0 ) {
			printf("getaddrinfo failed with error: %d\n", iResult);
			WSACleanup();
		}

		// Attempt to connect to an address until one succeeds
		for(ptr=result; ptr != NULL ;ptr=ptr->ai_next) {

			// Create a SOCKET for connecting to server
			clientSocket = socket(ptr->ai_family, ptr->ai_socktype, 
				ptr->ai_protocol);
			if (clientSocket == INVALID_SOCKET) {
				printf("socket failed with error: %ld\n", WSAGetLastError());
				WSACleanup();
			}

			// Connect to server.
			iResult = connect( clientSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR) {
				closesocket(clientSocket);
				clientSocket = INVALID_SOCKET;
				continue;
			}
			break;
		}

		freeaddrinfo(result);

		if (clientSocket == INVALID_SOCKET) {
			printf("Unable to connect to server!\n");
			WSACleanup();
		}
	}

	void waitForClient() {
		if((data_.operatingMode == initData::SERVER_MODE) && (clientSocket == INVALID_SOCKET)) {
			// Accept a client socket...
			printf("waitForClient: Accepting a client socket.\n");
			clientSocket = accept(listenSocket, NULL, NULL);
			if (clientSocket == INVALID_SOCKET) {
				printf("accept failed with error: %d\n", WSAGetLastError());
				closesocket(listenSocket);
				WSACleanup();
			}
		}
	}

};



}}} // namespace

#endif //! WINSOCKET_TRANSLATOR_H_