/*!
 * \file cin_cout_Translator.h
 *
 * \author Kazimierz Michalik
 * \date 08.05.2013
 *
 *\brief 
 *
 */
#ifndef CIN_COUT_TRANSLATOR_
#define CIN_COUT_TRANSLATOR_

#include "communicationException.h"
#include "callbackType.h"
#include <iostream>

extern "C" {
	struct cin_cout_Translator_data
	{
		int alwaysAddNewline;
	};
}

namespace am3{ namespace common { namespace communication { 

class cin_cout_Translator
{
public:
	typedef cin_cout_Translator_data initData;
	static const bool asyncWriteSupported = false;
	static const bool syncWriteSupported = true;
	static const bool asyncReadSupported = false;
	static const bool syncReadSupported = true;

	cin_cout_Translator(const initData & data) : data_(data) {};
	~cin_cout_Translator(){};
	
	
	template<typename T>
	bool write(const T& val) {
		std::cout << val;
		if(data_.alwaysAddNewline == 1) {
			std::cout << "\n";
		}
		return true;
	}
	
	template<typename T>
	bool readAsync(T& val, callbackFunPtr function){
		bool result = read(val);
		communicationException e;
		function(e);
		return result;
	}

	template<typename T>
	bool writeAsync(const T& val,callbackFunPtr function){
		bool result = write(val);
		communicationException e;
		function(e);
		return result;
	}
	
	template<typename T>
	bool read(T& val) {
		std::cin >> val;
		return true;
	}

	void destroy(){}

	bool good() const 
	{
		return std::cout.good() && std::cin.good();
	}

protected:
	const initData data_;
};

}}} // namespace

#endif //!CIN_COUT_TRANSLATOR_