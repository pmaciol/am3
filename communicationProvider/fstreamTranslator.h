/*!
 * \file fstreamTranslator.h
 *
 * \author Kazimierz Michalik
 * \date 08.05.2013
 *
 *\brief 
 *
 */
#ifndef FSTREAM_TRANSLATOR_H_
#define FSTREAM_TRANSLATOR_H_

#include <fstream>

extern "C" {
	struct fstreamTranslator_data
	{
		std::string filename;
		std::ios::open_mode openmode;

	};
}

namespace am3{ namespace common { namespace communication { 


class fstreamTranslator
{
public:
	typedef fstreamTranslator_data initData;
	static const bool asyncWriteSupported = false;
	static const bool syncWriteSupported = true;
	static const bool asyncReadSupported = false;
	static const bool syncReadSupported = true;

	fstreamTranslator(const initData & data) : data_(data), file(data.filename,data.openmode)
	{
		file.clear();
		assert(file.is_open());
		assert(file.good());
	};

	~fstreamTranslator(){
		destroy();
	};
	
	
	template<typename T>
	bool write(const T& val) {
		file << val;
		return file.good();
	}
	
	template<typename T>
	bool readAsync(T& val, callbackFunPtr function) {
		bool result = read(val);
		communicationException e;
		function(e);
		return result;
	}

	template<typename T>
	bool writeAsync(const T& val,callbackFunPtr function) {
		bool result = write(val);
		communicationException e;
		function(e);
		return result;
	}

	template<typename T>
	bool read(T& val) {
		file >>val;
		return file.good();
	}

	void destroy(){
		if(file.is_open()) {
			file.clear();
			file.close();
		}
		assert(!file.is_open());
	}

	bool good() const 
	{
		return file.is_open() && file.good();
	}

protected:
	std::fstream file;
	const initData data_;
};

}}} // namespace
#endif // !FSTREAM_TRANSLATOR_H_