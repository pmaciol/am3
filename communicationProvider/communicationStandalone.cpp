/*!
 * \file communicationStandalone.cpp
 *
 * \author Kazimierz Michalik
 * \date 08.05.2013
 *
 *\brief 
 *
 */
#include <iostream>
#include <cassert>
#include <string>
#include <algorithm>
#include <map>

#include "predefinedStreams.h"
using std::cout;
using std::cin;
using std::string;
using am3::common::communication::Winsock2Stream;

typedef int (*fptr)(Winsock2Stream* &, Winsock2Stream::initData&);


int handleStart(Winsock2Stream* &stream, Winsock2Stream::initData& data)
{
	if(stream != NULL) {
		cout << "Already started.\n To restart with new parameters, first use \"quit\" and then \"start\" \n";
	}
	else {
		assert(stream == NULL);
		cout << "Opening connection...";
		stream = new Winsock2Stream(data);

		if (stream->good())
		{
			cout << "Connection estabilished.\n"
				<< "Use \"send [type]\" and \"recv[type]\" for transfering data.\n";
		}
		else
		{
			cout << "Connection error.\n";
			if(stream != NULL) {
				delete stream;
			}
		}
	}
	return 0;
}

int handleQuit(Winsock2Stream* &stream, Winsock2Stream::initData&)
{
	cout << "Confirm quit: Yes/No: ";
	string new_value;
	cin >> new_value;
	std::transform(new_value.begin(), new_value.end(), new_value.begin(), ::tolower);


	if(new_value.compare("yes")==0
		|| new_value.compare("y")==0){

			if(stream != NULL) {
				cout << "Closing connection...";
				delete stream;
				cout << "Connection closed.\n";
			}
			cout << "Exiting...\n";
	}
	return 0;
}

int handleBuffersize(Winsock2Stream* &stream, Winsock2Stream::initData& data)
{
	//cout << "Enter new buffersize: ";
	int new_value;
	cin >> new_value;
	data.bufferSize = new_value;
	cout << "New buffersize: " << data.bufferSize << "\n";
	return 0;
}

int handlePort(Winsock2Stream* &stream, Winsock2Stream::initData& data)
{
	//cout << "Enter new buffersize: ";
	string new_value;
	cin >> new_value;
	delete [] data.serverPort;
	data.serverPort = new char[new_value.length()+2];
	strcpy_s(data.serverPort,new_value.length()+1,new_value.c_str());
	cout << "New port: " << data.serverPort << "\n";
	return 0;
}

int handleHostname(Winsock2Stream* &stream, Winsock2Stream::initData& data)
{
	//cout << "Enter new buffersize: ";
	string new_value;
	cin >> new_value;
	delete [] data.hostname;
	data.hostname = new char[new_value.length()+2];
	strcpy_s(data.hostname,new_value.length()+1,new_value.c_str());
	cout << "New hostname: " << data.hostname << "\n";
	return 0;
}

int handleMode(Winsock2Stream* &stream, Winsock2Stream::initData& data)
{
	//cout << "Enter new buffersize: ";
	string new_value;
	cin >> new_value;
	
	std::transform(new_value.begin(), new_value.end(), new_value.begin(), ::tolower);

	data.operatingMode = Winsock2Stream::initData::INVALID_MODE;
	if(new_value.compare("client")==0){
			data.operatingMode = Winsock2Stream::initData::CLIENT_MODE;
			cout << "New mode: CLIENT" << "\n";

	}
	else if(new_value.compare("server")==0) {
			data.operatingMode = Winsock2Stream::initData::SERVER_MODE;
			cout << "New mode: SERVER" << "\n";
	}
	else {
		cout << "New mode: INVALID." << " Expected values are CLIENT or SERVER.\n";
	}
	
	return 0;
}

template <typename T>
 int handleSend (Winsock2Stream* &stream, Winsock2Stream::initData&) 
 {
 T new_value;
 cin >> new_value;
 *stream << new_value;
 return 0;	 
}

 template <typename T>
 int handleRecv (Winsock2Stream* &stream, Winsock2Stream::initData&) 
 {
 T new_value;
 *stream >> new_value;
 cout << "Recieved value: " << new_value << "\n";
 return 0;
	 
}

 int handleHelp(Winsock2Stream* &stream, Winsock2Stream::initData&)
{
	cout << "Available commands:\n"
		<< " help - to display this information\n\n"
		<< " set buffersize [new_buffer_size]\n"
		<< "     hostname [new_hostname_value]\n"
		<< "     port [new_port_value]\n"
		<< "     mode [CLIENT|SERVER] \n\n"
		<< " start - in SERVER mode in creates server and begin to listen, in CLIENT mode it starts client and waits for \"send|recv\" commands.\n\n"
		<< " send [int|short|long|float|double|string|char] value\n\n"
		<< " recv [int|short|long|float|double|string|char] value\n\n"
		<< " quit\n\n";
	return 0;
}


int main (int argc, char** argv)
{
	
	 std::map<string,fptr> mymap;

	 mymap["start"]=handleStart;
	 mymap["quit"]=handleQuit;
	 mymap["q"]=handleQuit;
	 mymap["set buffersize"]=handleBuffersize;
	 mymap["set buffer"]=handleBuffersize;
	 mymap["set hostname"]=handleHostname;
	 mymap["set host"]=handleHostname;
	 mymap["set port"]=handlePort;
	 mymap["set mode"]=handleMode;
	 mymap["send int"]=handleSend<int>;
	 mymap["send long"]=handleSend<long>;
	 mymap["send double"]=handleSend<double>;
	 mymap["send float"]=handleSend<float>;
	 mymap["send string"]=handleSend<string>;
	 mymap["send char"]=handleSend<char>;
	 mymap["recv int"]=handleRecv<int>;
	 mymap["recv long"]=handleRecv<long>;
	 mymap["recv double"]=handleRecv<double>;
	 mymap["recv float"]=handleRecv<float>;
	 mymap["recv string"]=handleRecv<string>;
	 mymap["recv char"]=handleRecv<char>;
	 mymap["help"]=handleHelp;

	cout << "This is the communicationStandalone program which tests socket connections.\n";

	string defaultHostname("127.0.0.1"),defaultPort("5345");

	Winsock2Stream::initData serverSocketData;
	serverSocketData.bufferSize = 1000;
	serverSocketData.hostname = new char[defaultHostname.length()+2];
	strcpy_s(serverSocketData.hostname,defaultHostname.length()+1, defaultHostname.c_str());
	serverSocketData.serverPort = new char[defaultPort.length()+2];
	strcpy_s(serverSocketData.serverPort,defaultPort.length()+1, defaultPort.c_str());
	serverSocketData.operatingMode = serverSocketData.CLIENT_MODE;

	Winsock2Stream * socketStream(NULL);

	cout << "Default socket parameters:\n";
	cout << "Buffer size: " << serverSocketData.bufferSize << "\n";
	cout << "Hostname: " << serverSocketData.hostname << "\n";
	cout << "Port: " << serverSocketData.serverPort << "\n";
	cout << "Operating mode: client mode \n";

	cout << "In order to change the parameters write \"set [buffersize|hostname|port|mode]\" \n";

	cout << "To open connection write \"start\" \n";

	string new_command, new_command2;
	do
	{
		cout << ">";
		cin >> new_command;
		std::transform(new_command.begin(), new_command.end(), new_command.begin(), ::tolower);

		if (new_command.compare("set")==0 
			|| new_command.compare("send")==0
			|| new_command.compare("recv")==0)
		{
			cin >> new_command2;
			std::transform(new_command2.begin(), new_command2.end(), new_command2.begin(), ::tolower);

			new_command.append(" ");
			new_command.append(new_command2);

		}
		
		

		if (mymap[new_command]!=NULL)
		{
			mymap[new_command](socketStream, serverSocketData);

		}
		else
		{
			cout << "Unknown command.\n";
			cout << "To list available commands write \"help\" \n ";
		}

	}
	while (mymap[new_command] != handleQuit);

	delete [] serverSocketData.hostname;
	delete [] serverSocketData.serverPort;

	return 0;

}


