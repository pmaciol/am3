#ifndef COMMUNICATION_PROVIDER_H_
#define COMMUNICATION_PROVIDER_H_

#include <cassert>

#include "communicationException.h"

template<class TMiddlewarePolicy>
class communicationStream
{
public:
	typedef void (*callbackFunPtr)(communicationException& e);

	communicationStream(const TMiddlewarePolicy::data & lowerLayerInitData)
						: lowerLayer(lowerLayerInitData),
					
	{
	}
	
	virtual ~communicationStream()
	{
		lowerLayer.destroy();
	}
	
	template<typename T> bool write(const T value, callbackFunPtr f = NULL)
	{
		bool result=false;
		if(f!=NULL) {
			assert(TMiddlewarePolicy::asyncWriteSupported);
			assert(lowerLayer.asyncWriteEnabled());
			result = lowerLayer.writeAsync(T,f);
		}
		else{
			assert(TMiddlewarePolicy::syncWriteSupported);
			assert(lowerLayer.syncWriteEnabled());
			result = lowerLayer.write(T);
		}
		return result;
	}
	template<typename T> bool read(T & value, callbackFunPtr f = NULL)
	{
		bool result=false;
		if(f!=NULL) {
			assert(TMiddlewarePolicy::asyncReadSupported);
			assert(lowerLayer.asyncReadEnabled());
			result = lowerLayer.readAsync(T,f);
		}
		else{
			assert(TMiddlewarePolicy::syncReadSupported);
			assert(lowerLayer.syncReadEnabled());
			result = lowerLayer.read(T);
		}
		return result;
	}
	
protected:
		TMiddlewarePolicy lowerLayer;
};

template<typename T>
communicationStream& operator<<(communicationStream & stream, const T value) 
{
	if(!stream.write(T))
	{
		throw communicationException();
	}
	return stream;
}

template<typename T>
communicationStream& operator>>(communicationStream & stream, const T value)
{
	if(!stream.read(T))
	{
		throw communicationException();
	}
	return stream;
}



#endif //!COMMUNICATION_PROVIDER_H_