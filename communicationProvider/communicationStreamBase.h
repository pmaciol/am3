/*!
 * \file communicationStreamBase.h
 *
 * \author Kazimierz Michalik
 * \date 08.05.2013
 *
 *\brief 
 *
 */
#ifndef COMMUNICATION_STREAM_BASE_H_
#define COMMUNICATION_STREAM_BASE_H_

#include "communicationException.h"
#include "callbackType.h"

using am3::common::communication::callbackFunPtr;

namespace am3{ namespace common { namespace communication {

	class CommunicationStreamBase {
	public:

		virtual ~CommunicationStreamBase(){};

		template<typename T> bool write(const T & value, callbackFunPtr f = NULL);
	
		template<typename T> bool read(T & value, callbackFunPtr f = NULL);
	
		virtual bool good() const = 0; 
	
		virtual void close() = 0;
		
	};


template< typename T>
CommunicationStreamBase& operator<<(CommunicationStreamBase & stream, const T & value) 
{
	if(!stream.write(value))
	{
		throw communicationException();
	}
	return stream;
}

template<typename T>
CommunicationStreamBase& operator>>(CommunicationStreamBase & stream, T & value)
{
	if(!stream.read(value))
	{
		throw communicationException();
	}
	return stream;
}


}}} // !namespace

#endif // !COMMUNICATION_STREAM_BASE_H_