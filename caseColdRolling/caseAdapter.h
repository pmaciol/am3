
/*! \file caseAdapter.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseAdapter_h__
#define caseAdapter_h__
/*	include *********************************************************/

#include <vector>
#include "../models/models.h"
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../models/inputTypelists.h"

/*	using ***********************************************************/

namespace mo = am3::model;
namespace ppm = am3::problem::polycrystal;
namespace sdm = am3::software::deform;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace coldRolling
    {
      typedef moc::Contract<TYPELIST_8(	
        ppm::contract::DislDensityMobile,
        ppm::contract::SubgrainSize,
        ppm::contract::StoredEnergy,
        ppm::contract::GrainSize,
        ppm::contract::NumberOfRecrystalizedNuclei,
        ppm::contract::DislDensityMobileDeformed,
        ppm::contract::RecrystalizedGrainSize,
        ppm::contract::VolumeFractionOfRecrystallizedGrains
        ),mpr::Coordinates2D> DeformInput2D;

      class DeformOutputAdapter
        : public ppm::contract::MacroInputsContract2D
      {
      public:
        DeformOutputAdapter(boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > dhPointer): dhPointer_(dhPointer){};
        virtual ~DeformOutputAdapter(){};
        mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::Temperature *const )
        {
          mpr::Temperature toRet;
          return toRet;
        }
        mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::EffectiveStrainRate *const )
        {
          mpr::EffectiveStrainRate toRet;
          return toRet;
        }


        mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StepLength *const )
        {
          mpr::StepLength toRet;
          return toRet;
        }

      protected:
        boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > dhPointer_;
      }; 

      template <typename INPUT, typename HANDLER>
      class IVStorage
        : public mo::InputTypelist<INPUT>//s<DeformInput2D>
      {
      public:
//        using mo::Inputs<DeformInput2D>::Set;
        template<typename T>T GetPreviousState();
        // 
        template<> ppm::contract::GrainSize GetPreviousState() //GRains
        {
          ppm::contract::GrainSize  toRet;
          return toRet;
        }


        template<> ppm::contract::DislDensityMobile GetPreviousState() //GRains
        {
          ppm::contract::DislDensityMobile  toRet;
          return toRet;
        }

        template<> ppm::contract::SubgrainSize GetPreviousState() //GRains
        {
          ppm::contract::SubgrainSize  toRet;
          return toRet;
        }

        template<> ppm::contract::StoredEnergy GetPreviousState() //GRains
        {
          ppm::contract::StoredEnergy  toRet;
          return toRet;
        }

        template<> ppm::contract::NumberOfRecrystalizedNuclei GetPreviousState() //GRains
        {
          ppm::contract::NumberOfRecrystalizedNuclei  toRet;
          return toRet;
        }

        template<> ppm::contract::DislDensityMobileDeformed GetPreviousState() //GRains
        {
          ppm::contract::DislDensityMobileDeformed  toRet;
          return toRet;
        }

        template<> ppm::contract::RecrystalizedGrainSize GetPreviousState() //GRains
        {
          ppm::contract::RecrystalizedGrainSize  toRet;
          return toRet;
        }

        template<> ppm::contract::VolumeFractionOfRecrystallizedGrains GetPreviousState() //GRains
        {
          ppm::contract::VolumeFractionOfRecrystallizedGrains  toRet;
          return toRet;
        }
      protected:
        HANDLER* pointerToHandler_;
      };

      class CaseMacroImplementation
        : public DeformOutputAdapter

		//TODO Must be rewriten!

//        , public IVStorage<sdm::DeformHandler<mpr::Coordinates2D> >
      {
      public:
        CaseMacroImplementation(boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > handler): handler_(handler),DeformOutputAdapter(handler){};
        boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > GetHandler(){return handler_;};
      protected:
        boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > handler_;

      }; 
    }
  }
}
#endif // caseAdapter_h__
