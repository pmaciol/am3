
/*! \file kbs.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef kbs_h__
#define kbs_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../models/dataStructures.h"
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "databus.h"

/*	using ***********************************************************/

namespace ccr = am3::cases::coldRolling;
namespace mpr = am3::model::properties;
namespace ppm = am3::problem::polycrystal;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace coldRolling
    {
			// UserGuide: This is ugly and simple "Knowledge Based System" for caseColdRolling.
			class Kbs
			{
			public:
				template <typename VARIABLE> mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point)
				{
					const VARIABLE* trait=nullptr;
					return GetModelId(point,trait);
				}
				Kbs(boost::shared_ptr<ccr::DatabusForGottsteinDeform> databus): databus_(databus){};
			protected:
				boost::shared_ptr<ccr::DatabusForGottsteinDeform> databus_;
				// UserGuide:  Here is "knowledge"
				mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::RecrystalizedGrainSize* const)
				{
					// UserGuide: Getting actual values of variables from databus
					boost::shared_ptr<moc::Provider<ppm::contract::ZenerParam,mpr::Coordinates2D> > zenerDragProvider;
					boost::shared_ptr<moc::Provider<mpr::EffectiveStrainRate,mpr::Coordinates2D> > efepseProvider;
					efepseProvider = databus_;
					zenerDragProvider = databus_;
					mpr::EffectiveStrainRate efepse = efepseProvider->GetState(point,&efepse);
					ppm::contract::ZenerParam zenerDrag = zenerDragProvider->GetState(point,&zenerDrag);
					// UserGuide: Getting values of variables from previous step
					ppm::contract::StoredEnergy storedEnergy = databus_->PreviousData()->GetPreviousState<ppm::contract::StoredEnergy>();

					// UserGuide: Choosing model number basing on actual and previous state of material. Models IDs depends on the order of adding them to the Switcher
					mpr::ModelID mId;
					if (efepse() > 0.001)
					{
						mId(2);
					} 
					else
					{
						if (storedEnergy() < zenerDrag())
						{
							mId(0);
						} 
						else
						{
							mId(1);
						}
					}
					return mId;
				}
				// UserGuide: GetModelId must be provided for each variable in Contract of the Switcher, which use this KBS. If more than one Switcher is used, GetModelId must be provided for all variables from all Contracts
				mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::GrainSize* const)
				{
					mpr::ModelID mId;
					mId(0);
					return mId;
				}
				mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::NumberOfRecrystalizedNuclei* const)
				{
					mpr::ModelID mId;
					mId(0);
					return mId;
				}
				mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::VolumeFractionOfRecrystallizedGrains* const)
				{
					mpr::ModelID mId;
					mId(0);
					return mId;
				}
				mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::StoredEnergy* const)
				{
					mpr::ModelID mId;
					mId(0);
					return mId;
				}
			};
    } //coldRolling
  } //cases
} //am3
#endif // kbs_h__
