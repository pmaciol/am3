/*! \file caseColdRolling.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "../modelPolycrystalMicrostructure/gottstein.h" //Gottstein model by P. Sherstnev
#include "../adapter/adapter.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "caseColdRolling.h"
#include "databus.h"
#include "kbs.h"

/*	using ***********************************************************/

namespace mpm = am3::model::polycrystal; //Concretized models 
namespace ada = am3::adapter;
namespace sdm=am3::software::deform;

/*	global **********************************************************/

// Must be created to initialize model
am3::cases::coldRolling::Main m;

// Global access for external (Deform) calls
ActualDeformHandler* deformHandler;

/*	classes *********************************************************/
am3::cases::coldRolling::Main::Main()
{
	// UserGuide: Dump model for yield stress derivatives
	boost::shared_ptr<sdm::DeformStressContract2D > zeroStressModel (new sdm::ConstantYieldStress(0.0,0.0,0.0));
	
	boost::shared_ptr<mpm::YieldStressBySherstnev> yieldStressModel (new mpm::YieldStressBySherstnev);
	boost::shared_ptr<DatabusForGottsteinDeform> mainDatabus (new DatabusForGottsteinDeform);
	

	// UserGuide: MainDatabus provides yield stress, derivatives are always = 0
	boost::shared_ptr<ActualDeformHandler> deformHandlerPtr(new ActualDeformHandler(mainDatabus,zeroStressModel,zeroStressModel));
	// Setting global access
	deformHandler = deformHandlerPtr.get();

	mainDatabus->SetPreviousStepsStorage(deformHandlerPtr);

	// UserGuide: Objects of P. Sherstnev models. If you need other models, create them here
	boost::shared_ptr<mpm::ShearModulusBySherstnev> shearModulusModel (new mpm::ShearModulusBySherstnev);
	boost::shared_ptr<mpm::DislocationsGottsteinDeformation<ActualDeformHandler> > dislocationModel (new mpm::DislocationsGottsteinDeformation<ActualDeformHandler>(deformHandlerPtr));
	boost::shared_ptr<mpm::DiffusionBySherstnev> diffusionModel (new mpm::DiffusionBySherstnev);
	boost::shared_ptr<mpm::ZenerParamBySherstnev> zenerModel (new mpm::ZenerParamBySherstnev);
	// boost::shared_ptr<mpm::GrainsBySherstnevDeformation<ActualDeformHandler> > grainsModel (new mpm::GrainsBySherstnevDeformation<ActualDeformHandler>(deformHandlerPtr));

	boost::shared_ptr<mpm::GrainsBySherstnevStaticRecovery<ActualDeformHandler> > grainsModelStaticRecovery (new mpm::GrainsBySherstnevStaticRecovery<ActualDeformHandler>(deformHandlerPtr));
	boost::shared_ptr<mpm::GrainsBySherstnevNoDeformation<ActualDeformHandler> > grainsModelNoDeformation (new mpm::GrainsBySherstnevNoDeformation<ActualDeformHandler>(deformHandlerPtr));
	boost::shared_ptr<mpm::GrainsBySherstnevDeformation<ActualDeformHandler> > grainsModelDeformation (new mpm::GrainsBySherstnevDeformation<ActualDeformHandler>(deformHandlerPtr));


	// UserGuide: Models provides its outputs to databus
	ada::BindModelToBus(mainDatabus,yieldStressModel);
	ada::BindModelToBus(mainDatabus,shearModulusModel);
	ada::BindModelToBus(mainDatabus,shearModulusModel);
	ada::BindModelToBus(mainDatabus,zenerModel);
	ada::BindModelToBus(mainDatabus,diffusionModel);
	ada::BindModelToBus(mainDatabus,dislocationModel);

	// UserGuide: Setting databus as inputs provider for models
	ada::InputsProvider<mpm::YieldStressBySherstnev,DatabusForGottsteinDeform> inputsProviderForYieldStress;
	inputsProviderForYieldStress.Set(mainDatabus,yieldStressModel);
	ada::InputsProvider<mpm::ShearModulusBySherstnev, DatabusForGottsteinDeform> inputsProviderForShearModulus;
	inputsProviderForShearModulus.Set(mainDatabus,shearModulusModel );
	ada::InputsProvider<mpm::DiffusionBySherstnev, DatabusForGottsteinDeform> inputsProviderForDiffusion;
	inputsProviderForDiffusion.Set(mainDatabus,diffusionModel);
	ada::InputsProvider<mpm::GrainsBySherstnevDeformation<ActualDeformHandler>, DatabusForGottsteinDeform> inputsProviderForGrainsDeformation;
	inputsProviderForGrainsDeformation.Set(mainDatabus,grainsModelDeformation );
	
	// UserGuide: Some models (grains and dislocations here) needs a storage for last step data. DeformHnadler is used here
	ada::InputsProvider<mpm::DislocationsGottsteinDeformation<ActualDeformHandler>, DatabusForGottsteinDeform> inputsProviderForDislocations;
	inputsProviderForDislocations.Set(mainDatabus,dislocationModel );
	
	// UserGuide: If more than one model can be used for one phenomena (grains here), all models must get databus as an input
	ada::InputsProvider<mpm::GrainsBySherstnevStaticRecovery<ActualDeformHandler>, DatabusForGottsteinDeform> inputsProviderForGrainsStaticRecovery;
	inputsProviderForGrainsStaticRecovery.Set(mainDatabus,grainsModelStaticRecovery );
	ada::InputsProvider<mpm::GrainsBySherstnevNoDeformation<ActualDeformHandler>, DatabusForGottsteinDeform> inputsProviderForGrainsNoDeformation;
	inputsProviderForGrainsNoDeformation.Set(mainDatabus,grainsModelNoDeformation );
	
	// UserGuide: Deform needs feedback data (yield stress). It must be connected to databus
	ada::InputsProvider<ActualDeformHandler , DatabusForGottsteinDeform> inputsProviderForDeform;
	inputsProviderForDeform.Set(mainDatabus,deformHandlerPtr);

	// UserGuide: If more than one model could be used for one phenomena, Knowledge Based System is needed. KBS in kbs.h is trivial, if-the-else based solution
	boost::shared_ptr<Kbs> kbs (new Kbs(mainDatabus));

	// UserGuide: Definition of the Switcher for grains
	boost::shared_ptr<ada::Switcher<Kbs, ppm::contract::GrainsContract2D> > grainsSwitcher(new ada::Switcher<Kbs, ppm::contract::GrainsContract2D>(kbs));
	
	// UserGuide: Adding models to Switcher
	grainsSwitcher->AddModel(grainsModelStaticRecovery);
	grainsSwitcher->AddModel(grainsModelNoDeformation);
	grainsSwitcher->AddModel(grainsModelDeformation);
	
	// UserGuide: Switcher can be used as a single model, here is added to databus to provide grains Contract
	ada::BindModelToBus(mainDatabus,grainsSwitcher);
}
