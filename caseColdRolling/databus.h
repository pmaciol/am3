
/*! \file databus.h **************************************************
* \author		Piotr Maciol
* \copyright 	GNU Public License.
* \brief       
* \details   	
*********************************************************************/
#ifndef caseColdRolling_databus_h__
#define caseColdRolling_databus_h__
/*	include *********************************************************/

#include "../databus/databus.h"
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "caseAdapter.h"

/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace dtb = am3::databus;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace coldRolling
		{
			// UserGuide: Handlers must be defined for all Contracts (output values from phenomena)
			typedef dtb::Handler<ppm::contract::MacroInputsContract2D> MacroInputs2DHandlers;
			typedef dtb::Handler<ppm::contract::DiffusionContract2D> DiffusionContract2DHandlers;
			typedef dtb::Handler<ppm::contract::GrainsContract2D> GrainsContract2DHandlers;
			typedef dtb::Handler<ppm::contract::ShearModulusContract2D> ShearModulus2DHandlers;
			typedef dtb::Handler<ppm::contract::YieldStressContract2D> YieldStressContract2DHandlers;
			typedef dtb::Handler<ppm::contract::ZenerContract2D> ZenerContract2DHandlers;
			typedef dtb::Handler<ppm::contract::DislocationsContract2D> DislocationModel2DHandlers;

			// UserGuide: Declaration of main databus for a case
			template <typename PREVIOUS_DATA_STORAGE>
			class DatabusForGottsteinModel
			// UserGuide: PreviousStepStorage must be derived if variables from previous steps are needed, but not provided by models
				: public dtb::PreviousStepStorage<PREVIOUS_DATA_STORAGE>
				// UserGuide: Databus must derive from Handlers for all available Contracts
				, public MacroInputs2DHandlers
				, public GrainsContract2DHandlers
				, public ShearModulus2DHandlers
				, public DislocationModel2DHandlers
				, public DiffusionContract2DHandlers
				, public YieldStressContract2DHandlers
				, public ZenerContract2DHandlers
			{
			public:
				// UserGuide: this must be done due to C++ limitations
				using ShearModulus2DHandlers::Set;
				using MacroInputs2DHandlers::Set;
				using GrainsContract2DHandlers::Set;
				using DislocationModel2DHandlers::Set;
				using DiffusionContract2DHandlers::Set;
				using YieldStressContract2DHandlers::Set;
				using ZenerContract2DHandlers::Set;
			};


		}
	}
}

#endif // caseColdRolling_databus_h__
