
/*! \file caseColdRolling.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseColdRolling_h__
#define caseColdRolling_h__
/*	include *********************************************************/

#include "../softwareDeformBuiltin/deformHandler.h"
#include "../adapter/ivstorage.h"
#include "databus.h"

namespace sdm = am3::software::deform;
namespace ad = am3::adapter;

namespace am3
{
  namespace adapter
  {
    template <>
    struct VariableID<ppm::contract::DislDensityMobile>
    {
      static const unsigned int id = 0;
    };

    template <>
    struct VariableID<ppm::contract::SubgrainSize>
    {
      static const unsigned int id = 1;
    };

    template <>
    struct VariableID<ppm::contract::StoredEnergy>
    {
      static const unsigned int id = 2;
    };

    template <>
    struct VariableID<ppm::contract::GrainSize>
    {
      static const unsigned int id = 3;
    };
    template <>
    struct VariableID<ppm::contract::NumberOfRecrystalizedNuclei>
    {
      static const unsigned int id = 4;
    };
    template <>
    struct VariableID<ppm::contract::DislDensityMobileDeformed>
    {
      static const unsigned int id = 5;
    };
    template <>
    struct VariableID<ppm::contract::RecrystalizedGrainSize>
    {
      static const unsigned int id = 6;
    };
    template <>
    struct VariableID<ppm::contract::VolumeFractionOfRecrystallizedGrains>
    {
      static const unsigned int id = 7;
    };
  };
	namespace cases
	{
		namespace coldRolling
		{
      // UserGuide: Declaration of simplified name for database in the case

      typedef ad::IVStorage<sdm::DeformHandler,ppm::DeformInput2D> Storage;
      typedef DatabusForGottsteinModel<Storage>  DatabusForGottsteinDeform;
      template<typename HANDLER, typename CONTRACT>
      class MacroModelCold : public am3::model::MacroModel<HANDLER, CONTRACT>
      {
      public:
        virtual ~MacroModelCold(){};
        mpr::EffectiveStrainRate GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::EffectiveStrainRate *const )
        {
          return pointerToMacroHandler_->GetEffectiveStrainRateIn(*point);
        }
        mpr::Temperature GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::Temperature *const )
        {
          return pointerToMacroHandler_->GetTemperatureIn(*point);
        }
        mpr::StepLength GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::StepLength *const )
        {
          return pointerToMacroHandler_->GetStepLength();
        }
      };

      typedef MacroModelCold<sdm::DeformHandler<mpr::Coordinates2D, Storage>,ppm::contract::MacroInputsContract2D> MacroModel;
      typedef mo::CaseMacroImplementation<MacroModel,Storage> CaseModel;



			// This case is used as a library. Constructor of Main class works like a "main" procedure in single executable solution
			class Main
			{
			public:
				Main();
			};    
		} //coldRolling
	} //cases  
} //am3



#endif // caseColdRolling_h__
