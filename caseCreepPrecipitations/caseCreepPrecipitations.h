
/*! \file caseCreepPrecipitations.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseCreepPrecipitations_h__
#define caseCreepPrecipitations_h__
/*	include *********************************************************/

#include "../models/macrosSimplifiedModels.h"
#include "../modelPrecipitation/contracts.h"

/*	using ***********************************************************/

namespace mpc = am3::model::precipitations;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace creepPrecipiation
    {
      DATABUS_NO_STORAGE(CaseCreepPrecipiationDatabus, mpc::contract::PrecipitationsBasicInputs, mpc::contract::PrecipitationsOnlyAmountContract);
    }
  }
}

#endif // caseCreepPrecipitations_h__
