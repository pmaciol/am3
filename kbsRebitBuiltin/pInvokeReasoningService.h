/*! \file pInvokeReasoningService.h *********************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#ifndef _pInvokeReasoningService_h
#define _pInvokeReasoningService_h

#include <string>
#include "..\kbsRebit\reasoner.h"

namespace am3r = am3::gsoap;
using namespace std;

namespace am3
{
	namespace pinvoke
	{
		/* PInvoke proxy to Rebit reasoning service */
		class PInvokeReasoningService : public am3r::ReasoningServiceProxy
		{
			public:
				virtual bool Hello();

				virtual am3r::ResponseDTO *OpenSession(string& xml, string ruleSetId);
				virtual am3r::ResponseDTO *SetVariableValue(string *sessionId, vector<am3r::VariableDTO> *varVector);
				virtual am3r::ResponseDTO *SetFinalVariable(string *sessionId, string *variableId);
				virtual am3r::ResponseDTO *MixDeduction(string *sessionId);
				virtual am3r::ResponseDTO *RemoveSession(string *sessionId);

			protected:
				std::vector<string> split(const string &s, char delim);
				std::vector<string> & split(const string &s, char delim, vector<string> &elems);
				am3r::ResponseDTO *ParseResult(const string &s, char delim);
		};
	}
}


#endif