/*! \file tester.cpp ***********************************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#include <string>
#include "..\kbsRebit\reasoner.h"
#include "pInvokeReasoningService.h"
#include "thermostatPInvokeReasoner.h"
#include "connector.h"

namespace am3r = am3::gsoap;
using namespace std;

namespace am3
{
	namespace pinvoke
	{
		void PInvokeTestConnection()
		{
			am3r::ReasoningServiceProxy *proxy = new PInvokeReasoningService();
			bool result = proxy->Hello();

			if (!result)
			{
				cout << "Rebit connection test failed!\n\n";
				return;
			}

			cout << "Rebit connection test passed\n\n";
		}

		// Tests PInvoke Reasoner with static data provider;
		void PInvokeTestStaticDataProvider()
		{
			string thermostatPath = "c:/thermostat_KB.xml";	// full path for thermostat KB
			string ruleSetId = "ThermostatSettingRules";		// identifier of ruleset in KB
			string finalVariableId = "thermostatSetting";		// id of final variable 

			// reads thermostat knowledgebase
			string kb = am3r::GetXmlFromFile(thermostatPath);
	
			// creates PInvokeReasoner and set static facts (method GetVariableValue() will use these facts during inference process)
			am3r::Reasoner *mixReasoner = new am3r::Reasoner(new PInvokeReasoningService(), new am3r::NullLogger());
			mixReasoner->SetVariable(new string("day"), new string("Monday"));
			mixReasoner->SetVariable(new string("time"), new string("11"));
			mixReasoner->SetVariable(new string("month"), new string("January"));

			// start inference process
			am3r::ResponseDTO *resp = mixReasoner->Run(kb, ruleSetId, finalVariableId);

			// tests if inference process finished without errors
			if ((resp == NULL) || (*resp->Status != 0))
			{
				cout << "Static provider test on \"thermostat\" knowledgebase failed!\n";
				return;
			}

			// reads the value of final variable and map it to string
			if ((resp->InteractionVariables == NULL) || (resp->InteractionVariables->size() <= 0))
			{
				am3r::VariableDTO *varDTO = &(*resp->InteractionVariables)[0];
				if ((varDTO->Value == NULL) || (varDTO->Value->size() <= 0))
				{
					cout << "Static provider test on \"thermostat\" knowledgebase failed\n";
				}

				string result = (*varDTO->Value)[0];
				string expected = "24";

				// tests if the value of final variable is correct
				if (result != expected) 
					cout << "The value of variable \"thermostatSetting\" is not as expected!\n";
			}

			cout << "Static provider test on \"thermostat\" knowledgebase passed\n";
		}

		// Tests PInvoke Reasoner with dynamic data provider;
		void PInvokeTestDynamicDataProvider()
		{
			string thermostatPath = "c:/thermostat_KB.xml";	// full path for thermostat KB
			string ruleSetId = "ThermostatSettingRules";		// identifier of ruleset in KB
			string finalVariableId = "thermostatSetting";		// id of final variable 

			// reads thermostat knowledgebase
			string kb = am3r::GetXmlFromFile(thermostatPath);
	
			// creates ThermostatPInvokeReasoner, i.e. Reasoner with overriden GetVariableValue() method for data retrieving during inference process)
			am3r::Reasoner *mixReasoner = new ThermostatPInvokeReasoner(new PInvokeReasoningService(), new am3r::ScreenLogger());
			
			// start inference process
			am3r::ResponseDTO *resp = mixReasoner->Run(kb, ruleSetId, finalVariableId);

			// tests if inference process finished without errors
			if ((resp == NULL) || (*resp->Status != 0))
			{
				cout << "Dynamic provider test on \"thermostat\" knowledgebase failed!\n";
				return;
			}

			// reads the value of final variable and map it to string
			if ((resp->InteractionVariables == NULL) || (resp->InteractionVariables->size() <= 0))
			{
				am3r::VariableDTO *varDTO = &(*resp->InteractionVariables)[0];
				if ((varDTO->Value == NULL) || (varDTO->Value->size() <= 0))
				{
					cout << "Dynamic provider test on \"thermostat\" knowledgebase failed\n";
				}

				string result = (*varDTO->Value)[0];
				string expected = "24";

				// tests if the value of final variable is correct
				if (result != expected) 
					cout << "The value of variable \"thermostatSetting\" is not as expected!\n";
			}

			cout << "Dynamic provider test on \"thermostat\" knowledgebase passed\n";
		}
	} // reasoner
} // am3



