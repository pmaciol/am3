/*! \file pInvokeReasoningService.cpp *********************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#include "connector.h"
#include "pInvokeReasoningService.h"
#include <iostream>
#include <vector>
#include <sstream>
#include<iostream>
#include<boost/tokenizer.hpp>

namespace am3r = am3::gsoap;
using namespace std;
using namespace boost;

namespace am3
{
	namespace pinvoke
	{
		///* -------------- ctor ----------------------- */
		//PInvokeReasoningService::PInvokeReasoningService() 
		//{
		//	this->Logger = logger;
		//}

		/* ---------------------------------------------- */
		/* ------------- public section ----------------- */
		/* ---------------------------------------------- */

		bool PInvokeReasoningService::Hello()
		{
			bool result = false;
			char *resp = helloCallback("aqq");
			string *r = new string(resp);
			if (strcmp(r->c_str(), "aqq") == 0)
				result = true;

			return result;
		}

		am3r::ResponseDTO *PInvokeReasoningService::OpenSession(string& xml, string ruleSetId)
		{
			char *resp = openSessionCallback(xml.c_str(), ruleSetId.c_str());

			// it is assumed that output of OpenSession and all other methods in Rebit Interface has the following form:
			// "a:b:c:v:v0:v1:...:vn", where
			// a - session status
			// b - session id
			// c - session state
			// v - id of result variable
			// v0 - the value of variable at position 0
			// v1 - the value of variable at position 1
			// vn - the value of variable at position n

			// map char * result to ResponseDTO 
			am3r::ResponseDTO *respDTO = ParseResult(resp, ':');

			return respDTO;
		}

		am3r::ResponseDTO *PInvokeReasoningService::SetFinalVariable(string *sessionId, string *variableId)
		{
			char *resp = setFinalVariableCallback(sessionId->c_str(), variableId->c_str());
			am3r::ResponseDTO *respDTO = ParseResult(resp, ':');

			return respDTO;
		}

		am3r::ResponseDTO *PInvokeReasoningService::MixDeduction(string *sessionId)
		{
			char *resp = mixDeductionCallback(sessionId->c_str());
			am3r::ResponseDTO *respDTO = ParseResult(resp, ':');
			
			return respDTO;
		}

		am3r::ResponseDTO *PInvokeReasoningService::SetVariableValue(string *sessionId, vector<am3r::VariableDTO> *varVector)
		{
			am3r::ResponseDTO *respDTO = new am3r::ResponseDTO();

			if ((varVector == NULL) || (varVector->size() <= 0))
				return respDTO;

			// size of first variable value
			int length = (*varVector)[0].Value->size();
			if (length <= 0)
				return respDTO;

			string *variable = new string(*((*varVector)[0].Id));
			for (int i = 0; i < length; i++)
			{
				vector<string> *valueVector = (*varVector)[0].Value;
				if ((valueVector == NULL) || (valueVector->size() <= 0))
					return respDTO;

				string *post = &(*valueVector)[i];
				*variable += ":" + *post;
			}

			char *resp = setVariableValueCallback(sessionId->c_str(), variable->c_str());
			respDTO = ParseResult(resp, ':');

			return respDTO;
		}


		am3r::ResponseDTO *PInvokeReasoningService::RemoveSession(std::string *sessionId)
		{
			char *resp = removeSessionCallback(sessionId->c_str());
			am3r::ResponseDTO *respDTO = ParseResult(resp, ':');

			return respDTO;
		}

		/* ---------------------------------------------- */
		/* ----------- protected section ---------------- */
		/* ---------------------------------------------- */

		vector<string> PInvokeReasoningService::split(const string &s, char delim) 
		{
			vector<string> elems;
			split(s, delim, elems);
			return elems;
		}

				// http://stackoverflow.com/questions/236129/how-to-split-a-string-in-c
		// usage: std::vector<std::string> x = split("one:two::three", ':');
		std::vector<string> & PInvokeReasoningService::split(const string &s, char delim, vector<string> &elems) 
		{
			stringstream ss(s);
			string item;
			while (getline(ss, item, delim)) {
				elems.push_back(item);
			}
			return elems;
		}

		// It is assumed that result
		am3r::ResponseDTO *PInvokeReasoningService::ParseResult(const string &s, char delim)
		{
			am3r::ResponseDTO *respDTO = new am3r::ResponseDTO();

			vector<string> *tokens = new vector<string>();

			string separator1("\\");	
			string separator2(":");
			string separator3("\"");
			escaped_list_separator<char> els(separator1,separator2,separator3);
			tokenizer<escaped_list_separator<char>> tok(s, els);
			for(tokenizer<escaped_list_separator<char>>::iterator beg=tok.begin(); beg!=tok.end();++beg)
			{
				tokens->push_back(*beg);
			}

			if (tokens->size() < 3)
				return respDTO;

			// status
			respDTO->Status = new int(atoi((*tokens)[0].c_str()));
			
			// sessionId
			respDTO->SessionId = new std::string((*tokens)[1]);

			// session state
			int *state = new int(atoi((*tokens)[2].c_str()));
			respDTO->SessionState = reinterpret_cast<am3r::SessionStateDTO *>(state);

			// variable
			if (tokens->size() > 3)
			{
				// variableId
				am3r::VariableDTO *varDTO = new am3r::VariableDTO();
				string *variableId = new string((*tokens)[3]);
				varDTO->Id = variableId;
				
				varDTO->Value = new vector<string>();
				for (int i = 4; i < tokens->size(); i++)
				{
					string *varValue = new string((*tokens)[i]);
					varDTO->Value->push_back(*varValue);
				}
				
				respDTO->InteractionVariables = new vector<am3r::VariableDTO>();
				respDTO->InteractionVariables->push_back(*varDTO);
			}

			return respDTO;
		}
	} // pinvoke
} // am3