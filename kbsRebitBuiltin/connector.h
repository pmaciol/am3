/*! \file connector.h *********************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#ifndef _Connector_H_
#define _Connector_H_ 


#define DLLIMPORT __declspec (dllimport)

namespace am3
{
	namespace pinvoke
	{
		typedef char *(__stdcall  *HelloCallback)(const char *req);
		typedef char *(__stdcall  *OpenSessionCallback)(const char *xml, const char *rulesetId);
		typedef char *(__stdcall  *SetFinalVariableCallback)(const char *sessionId, const char *variableId);
		typedef char *(__stdcall  *MixDeductionCallback)(const char *sessionId);
		typedef char *(__stdcall  *SetVariableValueCallback)(const char *sessionId, const char *variable);
		typedef char *(__stdcall  *RemoveSessionCallback)(const char *sessionId);

		extern "C" DLLIMPORT void Inject_Hello(void * iArg);
		extern "C" DLLIMPORT void Inject_OpenSession(void * iArg);
		extern "C" DLLIMPORT void Inject_SetFinalVariable(void * iArg);
		extern "C" DLLIMPORT void Inject_MixDeduction(void * iArg);
		extern "C" DLLIMPORT void Inject_SetVariableValue(void * iArg);
		extern "C" DLLIMPORT void Inject_RemoveSession(void * iArg);
		extern "C" DLLIMPORT void StartAm3Client(void);

		// global variable declarations
		extern HelloCallback helloCallback;
		extern OpenSessionCallback openSessionCallback;
		extern SetFinalVariableCallback setFinalVariableCallback;
		extern MixDeductionCallback mixDeductionCallback;
		extern SetVariableValueCallback setVariableValueCallback;
		extern RemoveSessionCallback removeSessionCallback;

		// global function declaration
		extern void PInvokeTestConnection();
		extern void PInvokeTestStaticDataProvider();
		extern void PInvokeTestDynamicDataProvider();
	}
}

#endif