//Description:

// This project is a PInvoke implementation of 'ReasoningServiceProxy'.
// It should be compiled as windows dll library. 
// It works as a client of dedicated implementation of REBIT engine (ConsoleRebitPInvokeHost.exe). 
// The first action during execution of 'ConsoleRebitPInvokeHost.exe' 
// is injecting all methods of REBIT interface into this C++ dll library. 
// After that REBIT engine calls 'StartAm3Client()' from 'connector.cpp'. 
// This method is a good place where you can put testing code. 
// The file tester.cpp contains 3 tests based on thermostat_KB.xml: 
// connection test and static and dynamic data provider tests. 
 

// To build and test:

//* the file 'EnginePInvokeSettings.config' in REBIT directory, 
//  open it and set the name of this library as a Dll property, e.g.: Dll = "am3kbsRebitBuiltin.dll"
//* download the file 'thermostat_KB.xml' and save it on C:\ 
//		(or change the following steps in the file 'tester.cpp':
//		'string thermostatPath = "c:/thermostat_KB.xml";' to the proper path)
//* build project as dll
//* copy manually 'am3kbsRebitBuiltin.dll' to REBIT directory or add a CMake copy command to CMakeList.txt, e.g.:
//	add_custom_command(TARGET am3kbsRebitBuiltin POST_BUILD    
//    	COMMAND ${CMAKE_COMMAND} -E copy_if_different  
//        "${CMAKE_BINARY_DIR}/kbsRebitBuiltin/Debug/am3kbsRebitBuiltin.dll"      
//        "D:/GrantRegulyBiznesowe/RebitSolution/Host/ConsoleRebitPInvokeHost/bin/Debug/am3kbsRebitBuiltin.dll")
//* run ConsoleRebitPInvokeHost.exe

