/*! \file thermostatPInvokeReasoner.h *********************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#ifndef _thermostatPInvokeReasoner_h
#define _thermostatPInvokeReasoner_h

#include <string>
#include "..\kbsRebit\reasoner.h"

namespace am3r = am3::gsoap;

namespace am3
{
	namespace pinvoke
	{
		// user defined class for reasoning on "thermostat" knowledge base
		class ThermostatPInvokeReasoner : public am3r::Reasoner
		{
			public:
				// callback methods for retrieving (or dynamically calculating) 
				// values of new variables needed in inference process
				virtual std::vector<std::string> *TemplateGetVariableValue(std::string varId);

				// ctor
				ThermostatPInvokeReasoner(am3r::ReasoningServiceProxy *proxy, am3r::Logger *logger) : am3r::Reasoner(proxy, logger) 
				{ };
		};
	}
}

#endif
