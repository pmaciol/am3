/*! \file connector.cpp  ********************************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#include "connector.h"
#include "pInvokeReasoningService.h"

namespace am3
{
	namespace pinvoke
	{
		/* -------------------------------------------------- */
		/* -------------- global variables defs ------------- */
		/* -------------------------------------------------- */

		HelloCallback helloCallback;
		OpenSessionCallback openSessionCallback;
		SetFinalVariableCallback setFinalVariableCallback;
		MixDeductionCallback mixDeductionCallback;
		SetVariableValueCallback setVariableValueCallback;
		RemoveSessionCallback removeSessionCallback;



		/* -------------------------------------------------- */
		/* ---------------- Injects c# methods -------------- */
		/* -------------------------------------------------- */

		void Inject_Hello(void * iArg)  
		{ 
			HelloCallback arg = (HelloCallback) 0 ; 
			arg = (HelloCallback)iArg;  
			helloCallback = arg;  
		} 

		/* Injects C# function to open session */
		void Inject_OpenSession(void * iArg)  
		{ 
			OpenSessionCallback arg = (OpenSessionCallback) 0 ; 
			arg = (OpenSessionCallback)iArg;  
			openSessionCallback = arg;  
		} 

		/* Injects C# function to set final variable */
		void Inject_SetFinalVariable(void * iArg)  
		{ 
			SetFinalVariableCallback arg = (SetFinalVariableCallback) 0 ; 
			arg = (SetFinalVariableCallback)iArg;  
			setFinalVariableCallback = arg;  
		} 

		/* Injects C# function to deduce */
		void Inject_MixDeduction(void * iArg)  
		{ 
			MixDeductionCallback arg = (MixDeductionCallback) 0 ; 
			arg = (MixDeductionCallback)iArg;  
			mixDeductionCallback = arg;  
		} 

		/* Injects C# function to send variable value to engine*/
		void Inject_SetVariableValue(void * iArg)  
		{ 
			SetVariableValueCallback arg = (SetVariableValueCallback) 0 ; 
			arg = (SetVariableValueCallback)iArg;  
			setVariableValueCallback = arg;  
		} 

		/* Injects C# function to remove session */
		void Inject_RemoveSession(void * iArg)  
		{ 
			RemoveSessionCallback arg = (RemoveSessionCallback) 0 ; 
			arg = (RemoveSessionCallback)iArg;  
			removeSessionCallback = arg;  
		} 

		/* -------------------------------------------------- */
		/* -------------- Method called from C# ------------- */
		/* -------------------------------------------------- */

		// This method is called from REBIT Engine after 
		// injecting REBIT interface methods.
		// It should be used as entry point for reasoning
		void StartAm3Client()
		{
			PInvokeTestConnection();
			PInvokeTestDynamicDataProvider();
			PInvokeTestStaticDataProvider();
		}
	} // reasoner
} // am3



