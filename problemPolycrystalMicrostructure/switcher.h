
/*! \file switcher.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef problemPolycrystalMicrostructure_switcher_h__
#define problemPolycrystalMicrostructure_switcher_h__
/*	include *********************************************************/

#include "contracts.h"

/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace problem
	{
		namespace polycrystal
		{
			// UserGuide: For each phenomena, which could be modeled with more than one model, the Switcher must be defined
			class Switcher
				:public contract::GrainsContract2D
			{
			public:
				// UserGuide: Methods below must be defined for each variable in Contract. Just copy-and-paste, change only the types
				virtual ppm::contract::GrainElongation GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::GrainElongation *const ){ppm::contract::GrainElongation toRet;return toRet;};
				virtual ppm::contract::RecrystalizedGrainSize GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::RecrystalizedGrainSize *const ){ppm::contract::RecrystalizedGrainSize toRet;return toRet;};
				virtual ppm::contract::SubgrainSize GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::SubgrainSize *const ){ppm::contract::SubgrainSize toRet;return toRet;};
				virtual ppm::contract::NumberOfRecrystalizedNuclei GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::NumberOfRecrystalizedNuclei *const ){ppm::contract::NumberOfRecrystalizedNuclei toRet;return toRet;};;
				virtual ppm::contract::VolumeFractionOfRecrystallizedGrains GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::VolumeFractionOfRecrystallizedGrains *const ){ppm::contract::VolumeFractionOfRecrystallizedGrains toRet;return toRet;};;
			};
		}
	}
}
#endif // problemPolycrystalMicrostructure_switcher_h__
