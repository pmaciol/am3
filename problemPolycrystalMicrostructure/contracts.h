
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef problemPolycrystalMicrostructure_contracts_h__
#define problemPolycrystalMicrostructure_contracts_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../models/contractMacros.h"
#include "../common/typelists.h"
#include "../softwareDeformBuiltin/deformHandler.h"



/*	using ***********************************************************/

namespace moc=am3::model::contract;
namespace mpr=am3::model::properties;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace problem
	{
		namespace polycrystal
		{
			namespace contract
			{
				// UserGuide: Definition of variables, which are specific only for this problem. Others (more general) can be found in ../models/dataStructures.h
				SIMPLE_PROP(SubgrainSize,double);
				SIMPLE_PROP(GrainSize,double);
				SIMPLE_PROP(RecrystalizedGrainSize,double);
				SIMPLE_PROP(VolumeFractionOfRecrystallizedGrains,double);
				SIMPLE_PROP(NumberOfRecrystalizedNuclei,double);
				SIMPLE_PROP(DislDensitySum,double);
				SIMPLE_PROP(DislDensityMobile,double);
				SIMPLE_PROP(DislDensityImmobileInteriors,double);
				SIMPLE_PROP(DislDensityImmobileWalls,double);
				SIMPLE_PROP(DislDensityMobileDeformed,double);
				SIMPLE_PROP(DislDensityImmobileInteriorsDeformed,double);
				SIMPLE_PROP(DislDensityImmobileWallsDeformed,double);
				SIMPLE_PROP(SelfDiffusion,double);
				SIMPLE_PROP(GrainBoundaryDiffusion,double);
				SIMPLE_PROP(ZenerParam,double);
				SIMPLE_PROP(StoredEnergy,double);
				SIMPLE_PROP(PrecipitateSize,double);

				//////////////////////////////////////////////////////////////////////////
				// Contracts
				//////////////////////////////////////////////////////////////////////////

				// UserGuide: Definition of all contracts (sets of output variables) for available phenomena
				typedef moc::Contract<TYPELIST_TUPLE_5(	GrainSize, RecrystalizedGrainSize, NumberOfRecrystalizedNuclei, VolumeFractionOfRecrystallizedGrains, StoredEnergy ),mpr::Coordinates2D> GrainsContract2D;
				typedef moc::Contract<TYPELIST_TUPLE_7( DislDensityMobile, DislDensityImmobileInteriors, DislDensityImmobileWalls, DislDensityMobileDeformed, DislDensityImmobileInteriorsDeformed, DislDensityImmobileWallsDeformed, SubgrainSize ),mpr::Coordinates2D> DislocationsContract2D;
				typedef moc::Contract<TYPELIST_TUPLE_2( SelfDiffusion , GrainBoundaryDiffusion ),mpr::Coordinates2D> DiffusionContract2D;
				typedef moc::Contract<TYPELIST_TUPLE_3( mpr::Temperature , mpr::EffectiveStrainRate , mpr::StepLength),mpr::Coordinates2D> MacroInputsContract2D;
				typedef moc::Contract<TYPELIST_TUPLE_1( mpr::YieldStress ),mpr::Coordinates2D> YieldStressContract2D;
				typedef moc::Contract<TYPELIST_TUPLE_1( mpr::ShearModulus ),mpr::Coordinates2D> ShearModulusContract2D;
				typedef moc::Contract<TYPELIST_TUPLE_1( ZenerParam ),mpr::Coordinates2D> ZenerContract2D;
				typedef moc::Contract<TYPELIST_TUPLE_1( mpr::ShearModulus ),mpr::Coordinates2D> EmptyContract;

        typedef moc::Contract<TYPELIST_TUPLE_1( PrecipitateSize ),mpr::Coordinates2D> PrecipContract2D;


			} //contract			
      typedef moc::Contract<TYPELIST_8(	
        contract::DislDensityMobile,
        contract::SubgrainSize,
        contract::StoredEnergy,
        contract::GrainSize,
        contract::NumberOfRecrystalizedNuclei,
        contract::DislDensityMobileDeformed,
        contract::RecrystalizedGrainSize,
        contract::VolumeFractionOfRecrystallizedGrains
        ),mpr::Coordinates2D> DeformInput2D;

      typedef am3::software::deform::DeformHandler<mpr::Coordinates2D> CaseDeformHandler;
      typedef am3::software::deform::DeformHandler<mpr::Coordinates2D> ActualDeformHandler;
		}
	}
}

#endif // problemPolycrystalMicrostructure_contracts_h__
