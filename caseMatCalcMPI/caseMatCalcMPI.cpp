/*! \file caseMatCalcMPI.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include <vector>
#include <boost/shared_ptr.hpp>
#include "../softwareDeform/deformHandlerFacadeMesh.h"
#include "../mockModels/mockMicroscale.h"
#include "../models/modelsManager.h"
#include "../common/common.h"
#include "caseMatCalcMPI.h"

/*	using ***********************************************************/

namespace sdm = am3::software::deform;
namespace mmk = am3::model::mock;
namespace mom = am3::model::manager;

/*	extern **********************************************************/

/*	classes *********************************************************/

boost::shared_ptr<mmk::MicroScaleModel> microManager;
boost::shared_ptr<mom::ManagerAllTogether<mmk::MicroScaleModel::InputData>> runner;
boost::shared_ptr<DeformMeshReceiver>  deformServer;


// This is an implementation of the method declared in softwareDeform package. It is called by Deform handler to get the pointer to the object responisble for computing material data
sdm::DeformHandlerFacadeMshTemperature* SetDeformHandlerFacadeMshTemperaturePtr()
{
	if (microManager == nullptr)
	{
		microManager = boost::shared_ptr<mmk::MicroScaleModel> (new mmk::MicroScaleModel);
		runner = boost::shared_ptr<mom::ManagerAllTogether<mmk::MicroScaleModel::InputData>> (new mom::BasicAllTogatherManager<mmk::MicroScaleModel>(microManager));
		deformServer = boost::shared_ptr<DeformMeshReceiver>(new DeformMeshReceiver(runner));
	}
  return deformServer.get();
}

//******************************************************************************************************************************************************************

void DeformMeshReceiver::SetTemperaturesInNodes(const int materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const int nodeStart, const int nodeEnd)
{
	sdm::OneStepOneMeshTemperatureTime::SetTemperaturesInNodes(materialId, time, timeStep, temperatures, deltaTemperatures, nodeStart, nodeEnd);
	std::vector<mmk::MicroModelData> dataToRun;
	cont.WaitForAll();
	for (int i = nodeStart; i <= (nodeStart + 5); i++)
	{
		const mpr::Coordinates2D coord = mpr::Coordinates2D(materialId, i); //This is a fake!
		mda::Coord2dTimeTemp val(coord, time, temperatures[i]);

		const mpr::Time t = mpr::Time(time);
		mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);

		std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair(point, val);
		cont.Run(pair);
	}
}

//******************************************************************************************************************************************************************

DeformMeshReceiver::~DeformMeshReceiver() 
{
  runner_->FinalizeAll();
}

void DeformMeshReceiver::PushData(const int i, const double temperature, const double time, std::vector<mmk::MicroModelData> & dataToRun)
{
  mmk::MicroModelData data;
  data.id = i;
  data.temperature = temperature;
  data.time = time;
  data.variable = std::numeric_limits<double>::quiet_NaN();
  dataToRun.push_back(data);
}

bool DeformMeshReceiver::IsInitialization(const double time) const
{
  bool toRet = false;
  if (isInitialized_ == false)
  {
    toRet = true;
  } 
  else if (AlmostZero(time))
  {
    toRet = true;
  }
  return toRet;
}

void DeformMeshReceiver::Initialized()
{
  isInitialized_ = true;
}

DeformMeshReceiver::DeformMeshReceiver(boost::shared_ptr<mom::ManagerAllTogether<mmk::MicroModelData> > runner) : runner_(runner), isInitialized_(false)
{
	cont.Init(0, nullptr);
}

