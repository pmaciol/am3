
/*! \file caseMatCalcMPI.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseMatCalcMPI_caseMatCalcMPI_h__
#define am3caseMatCalcMPI_caseMatCalcMPI_h__

/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../softwareDeform/policyOneStepOneMeshTemperatureTime.h"
#include "../models/modelsManager.h"
#include "../mockModels/mockMicroscale.h"
#include "../softwareDeform/setDeformHandler.h"
#include "../communicationMPI/instance.h"
#include "../communicationMPI/controller.h"
#include "../models/point.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace cco = am3::common::communication;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;
namespace mom = am3::model::manager;
namespace mmk = am3::model::mock;
namespace sdm = am3::software::deform;

/*	extern **********************************************************/
/*	classes *********************************************************/

typedef cco::MpiInstance<mda::Coord2dTimeTemp, int, mpt::ComputedPoint<mpr::Coordinates2D> > Instance;


class DeformMeshReceiver
  : public sdm::OneStepOneMeshTemperatureTime
{
public:
  DeformMeshReceiver(boost::shared_ptr<mom::ManagerAllTogether<mmk::MicroModelData> > runner);
  virtual ~DeformMeshReceiver() override;

  virtual void SetTemperaturesInNodes(const int materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const int nodeStart, const int nodeEnd) override;

protected:
  cco::Controller<mda::Coord2dTimeTemp, int, Instance, mpt::ComputedPoint<mpr::Coordinates2D>, mpr::Coordinates2D > cont;
  boost::shared_ptr<mom::ManagerAllTogether<mmk::MicroModelData>> runner_;
  bool isInitialized_;

  void PushData(const int i, const double temperature, const double time, std::vector<mmk::MicroModelData> & dataToRun);
  bool IsInitialization(const double time) const;
  void Initialized();
};

#endif // am3caseMatCalcMPI_caseMatCalcMPI_h__
