
/*! \file dllExport.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseMatCalcMPI_dllExport_h__
#define am3caseMatCalcMPI_dllExport_h__
/*	include *********************************************************/
/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/


__declspec(dllexport) int DEFORM_TEMPERATURES_SET(int materialId, double  time, double  timeStep, double  * temperatures, double  * deltaTemperatures, int nodeStart, int nodeEnd);

#endif // am3caseMatCalcMPI_dllExport_h__
