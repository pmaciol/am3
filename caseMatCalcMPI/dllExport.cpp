
/*! \file dllExport.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#ifdef BUILD_STANDALONE_CASE

#include "dllExport.h"
#include "../softwareDeform/usr_msh.h"

/*	using ***********************************************************/

namespace sdm = am3::software::deform;

/*	classes *********************************************************/

int DEFORM_TEMPERATURES_SET(int materialId, double time, double timeStep, double * temperatures, double * deltaTemperatures, int nodeStart, int nodeEnd)
{
  return sdm::DEFORM_TEMPERATURES_SET(materialId, time, timeStep, temperatures, deltaTemperatures, nodeStart, nodeEnd);
}



#endif