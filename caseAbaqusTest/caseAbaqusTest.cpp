
/*! \file caseAbaqusTest.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../softwareDeform/setDeformHandler.h"
#include "modelsAdapters.h"
#include "main.h"

/*	using ***********************************************************/

namespace sdm = am3::software::deform;
namespace ctm = am3::cases::tugmc;

/*	extern **********************************************************/

extern am3::cases::tugmc::Main*  mainInstance;

/*	classes *********************************************************/


sdm::DeformHandlerFacadeUpd* am3::cases::tugmc::GetDeformHandlerFacadeUpdate()
{
  if (mainInstance == nullptr) mainInstance = new am3::cases::tugmc::Main;
  return mainInstance->GetDeformCaseIV();
}

