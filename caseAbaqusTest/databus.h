/*! \file databus.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseAbaqusTest_databus_h__
#define am3caseAbaqusTest_databus_h__
/*	include *********************************************************/

#include "../common/typelists.h"
#include "modelsAdapters.h"
#include "ivStorage.h"
#include "../databus/databusTypelists.h"

/*	using ***********************************************************/

namespace db = am3::databus;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
      // List of 'Databus' classes
      typedef TEMPLATELIST_3(
		        IvManager,
		        PrecipitationsSwitcher, 
		        MacroModelTemperature
		        ) caseAbaqusTestToUSPList;
		
		      // Databus
	  typedef db::DatabusTypelists<caseAbaqusTestToUSPList> caseAbaqusTestToUSPDatabus;
				} //tugmc
	} //cases
} //am3
#endif // am3caseAbaqusTest_databus_h__
