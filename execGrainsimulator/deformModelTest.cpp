
#include "communicationSocket/socketServer.h"

#include <boost/smart_ptr/shared_ptr.hpp>

#include "../communicationSocket/socketServer.h"
#include "../softwareDeform/deformControl.h"
#include "../softwareDeform/storage.h"
#include "../problemPlasticAndHeat/fineModelSSCurves.h"
#include "../problemPlasticAndHeat/fineTugCa.h"
#include "../problemPlasticAndHeat/kbsConverter.h"
#include "../problemPlasticAndHeat/fineCoarseDumbAdapter.h"

#include "../knowledgeRebit/mockRebitClient.h"
//#include "../knowledgeRebitClient/ThermostatClient.h"
//#include "../basicModels/rebitInterface.h"

namespace soc=am3::common::communication;
namespace dfc=am3::common::communication::deform;
namespace sdfrm=am3::software::deform;
namespace pah=am3::problem::plasticAndHeat;
namespace kph=am3::knowledge::plasticAndHeat;
namespace kre=am3::knowledge::rebit;


int main(int argc, char **argv)
{

  extern am3::common::Configuration kProgramOptions;

  //That is needed for command line options passing to Configure structures
  global_argc=&argc;
  global_argv=&argv;

  //Initialization of Configuration. After that, command line options and main config file options are stored in first variable map in kProgramOptions
  //For options details check configProgOptConfiguration.cpp file
  kProgramOptions.Init();

  am3::model::deform::DeformModel deformModel;
  
  
  //dfc::DeformMtrCallProcessor callProc;


  boost::shared_ptr<sdfrm::storage::OnePointOneElementStorage> storage(new sdfrm::storage::OnePointOneElementStorage);
  boost::shared_ptr<dfc::DeformConfiguration> configuration;

  typedef pah::FineModelsSwitcher<
    pah::PlasticAndHeat2DCoarseCaller<sdfrm::storage::OnePointOneElementStorage>, 
    kno::I4Adapter<prop::Coordinates2D>
  > FMS;
  boost::shared_ptr<FMS> selector(new FMS);
  selector->SetCalledModel(storage);
  boost::shared_ptr<pah::FineModelSSCurves> fmSSC(new pah::FineModelSSCurves);
  boost::shared_ptr<pah::FineModelTugCA> fmTUGCA(new pah::FineModelTugCA);
  fmSSC->SetCoarseModel(selector);
  fmTUGCA->SetCoarseModel(selector);
  selector->AddFineInterface(fmSSC);
  selector->AddFineInterface(fmTUGCA);

  // KBS object
  boost::shared_ptr<kre::mockRebitClient> mockRebit(new kre::mockRebitClient);
//  boost::shared_ptr<ThermostatClient> mockRebit(new ThermostatClient("",""));
//    boost::shared_ptr<kre::MockRebitKBS> mockRebit(new kre::MockRebitKBS);
  boost::shared_ptr<kph::PlasticAndHeat2RebitConverter> converter
    (new kph::PlasticAndHeat2RebitConverter(mockRebit, selector, selector, selector));
  mockRebit->Init(converter);

  // KBS reference setting
  selector->SetI4Adapter(converter);

  pah::FineCoarseDumbAdapter* adapter = new pah::FineCoarseDumbAdapter(selector);

  deformModel.Init(adapter,storage.get());
  //deformModel.GetCallProcessor().Init(adapter,storage.get());

  soc::SocketServer deformSocketServer(deformModel.GetCallProcessor());
  deformSocketServer.SetModuleOptionsTemplateMethod();
  deformModel.Run();
  deformSocketServer.Communicate();


  delete adapter;

}