
include_directories (../)
add_executable (  am3execGrainsimulator
deformModelTest.cpp
 )

 target_link_libraries(am3execGrainsimulator am3logger am3softwareDeform am3models am3common am3communicationSocket am3problemPlasticAndHeat am3knowledgeRebit am3utilMath)

SET(CMAKE_EXE_LINKER_FLAGS_DEBUG /NODEFAULTLIB:\"LIBCMTD.lib;libcpmtd.lib\")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE /NODEFAULTLIB:\"libcpmt.lib\")