#ifndef DC_MANAGER
#define DC_MANAGER

#include <winsock.h>
#include <windows.h> 
#include "DeformConnector_DataPack.h"

class DC_Manager
{
  public:
    static DC_Manager& getManager();
		bool isOperable(){ return _isOperable;}
		bool isVerbose() {return _isVerbose;}
		char * lastErrorDescription() {return _errorMessage;}

		void deformSend(DC_DataPackOUT &data);
		void deformReceive(DC_DataPackIN &data);
		static DC_DataPackIN& getDataPackIN();
		
  protected:
    DC_Manager();
    DC_Manager(DC_Manager const&);
    void operator=(DC_Manager const&);

		u_long LookupAddress(const char* pcHost);
		SOCKET EstablishConnection(u_long nRemoteAddr, u_short nPort);
		bool SendData(SOCKET sd, DC_DataPackOUT &data);
		int ReceiveData(SOCKET sd, char * readBuffer);
		//bool AC_Manager::ShutdownConnection(SOCKET sd);
		bool ReadConfigFile();
		
		SOCKET _sd;
		bool _isOperable;
		bool _isVerbose;
		char * _errorMessage;
		char _pcHost [100];
		int _nPort;

		static const char * _configFile;
};

#endif