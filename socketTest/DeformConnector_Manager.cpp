#include "DeformConnector_Manager.h"
#include <winsock.h>
#include <stdio.h>
#include <string.h>

const char * DC_Manager::_configFile = "dcconfig.txt";

void DC_Manager::deformSend(DC_DataPackOUT &data)
{
  _errorMessage = "deformsend 1.\n";
    if (!SendData(_sd, data)) {
		_isOperable = false;
		_errorMessage = "ERROR: Send error.\n";
    }		
    _errorMessage = "deformsend 2.\n";
}

void DC_Manager::deformReceive(DC_DataPackIN &data)
{
	char receiveBuffer[512];
	memset(receiveBuffer,0,512);

	int nBytes;
  _errorMessage = "deformreceive 1.\n";
	if ((nBytes = ReceiveData(_sd, receiveBuffer)) > 0) {
		if (nBytes == 512) {
			_isOperable = false;
			_errorMessage = "ERROR: Receive: data overflow.\n";
		}
	}
	else if (nBytes == 0) {
		_isOperable = false;
		_errorMessage = "ERROR: Receive:  Server unexpectedly closed the connection.\n";
	}
	else {
		_isOperable = false;
		_errorMessage = "ERROR: Receive:  Socket error.\n";
	}

	
	data.yield_stress = atof(strtok(receiveBuffer,"DIT"));	
	data.d_flow_stress_strain = atof(strtok(NULL,"DIT"));		
	data.d_flow_stress_strain_rate= atof(strtok(NULL,"DIT"));	
  _errorMessage = "deformreceive 2.\n";
}

bool DC_Manager::ReadConfigFile()
{
	int verbose = 0;
	FILE * pFile;
	pFile = fopen (_configFile,"r");

	if (pFile == NULL) return false;

	if (fscanf (pFile, "%s\n", _pcHost) <= 0)
	{
		fclose (pFile);
		return false;
	}

	if (fscanf (pFile, "%d\n", &_nPort) <= 0)
	{
		fclose (pFile);
		return false;
	}
	

	if (fscanf (pFile, "%d\n", &verbose) <= 0)
	{
		fclose (pFile);
		return false;
	}

	_isVerbose = (verbose == 0) ? false : true;
	
	fclose (pFile);

	return true;
}

DC_Manager::DC_Manager():
_errorMessage("No error.\n"), _isOperable(false), _nPort(0), _isVerbose(true)
{
	if (!ReadConfigFile())
	{
		_errorMessage = "ERROR: Config file error.\n";
		return;
	}
	
	WSAData wsaData;
    int nCode = WSAStartup(MAKEWORD(1, 1), &wsaData);
	if(nCode != 0)
	{
		_errorMessage = "ERROR: Startup error.\n";
		return;
	}
	
	//---
	
	u_long nRemoteAddress = LookupAddress(_pcHost);
    if (nRemoteAddress == INADDR_NONE) {
		_errorMessage = "ERROR: LookupAddress error.\n";
		return;
    }
    in_addr Address;
    memcpy(&Address, &nRemoteAddress, sizeof(u_long)); 
	
	//---
	
	_sd = EstablishConnection(nRemoteAddress, htons(_nPort));
    if (_sd == INVALID_SOCKET) {
		_errorMessage = "ERROR: EstablishConnection error.\n";
		return;	
    }
	
	_isOperable = true;	
}

u_long DC_Manager::LookupAddress(const char* pcHost)
{
    u_long nRemoteAddr = inet_addr(pcHost);
    if (nRemoteAddr == INADDR_NONE) {
        hostent* pHE = gethostbyname(pcHost);
        if (pHE == 0) {
            return INADDR_NONE;
        }
        nRemoteAddr = *((u_long*)pHE->h_addr_list[0]);
    }

    return nRemoteAddr;
}

SOCKET DC_Manager::EstablishConnection(u_long nRemoteAddr, u_short nPort)
{
    SOCKET sd = socket(AF_INET, SOCK_STREAM, 0);
    if (sd != INVALID_SOCKET) {
        sockaddr_in sinRemote;
        sinRemote.sin_family = AF_INET;
        sinRemote.sin_addr.s_addr = nRemoteAddr;
        sinRemote.sin_port = nPort;
        if (connect(sd, (sockaddr*)&sinRemote, sizeof(sockaddr_in)) ==
                SOCKET_ERROR) {
            sd = INVALID_SOCKET;
        }
    }

    return sd;
}

bool DC_Manager::SendData(SOCKET sd, DC_DataPackOUT &dataOUT)
{
	char buffer[1024];
	memset(buffer,0,1024);

	int size;
	size = sprintf(buffer,"D%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%fD%dD%dD%dT",
    dataOUT.coord_x_node1,
  dataOUT.coord_x_node2,
  dataOUT.coord_x_node3,
  dataOUT.coord_x_node4,
  dataOUT.coord_y_node1,
  dataOUT.coord_y_node2,
  dataOUT.coord_y_node3,
  dataOUT.coord_y_node4,

  dataOUT.velocity_x_node1,
  dataOUT.velocity_x_node2,
  dataOUT.velocity_x_node3,
  dataOUT.velocity_x_node4,
  dataOUT.velocity_y_node1,
  dataOUT.velocity_y_node2,
  dataOUT.velocity_y_node3,
  dataOUT.velocity_y_node4,

  dataOUT.stress_node1,
  dataOUT.stress_node2,
  dataOUT.stress_node3,
  dataOUT.stress_node4,

  dataOUT.strain_rate_node1,
  dataOUT.strain_rate_node2,
  dataOUT.strain_rate_node3,
  dataOUT.strain_rate_node4,



  dataOUT.effectiveStress,
  dataOUT.effectiveStrainRate,
  dataOUT.totalEffectiveStrain,
  dataOUT.density,


  dataOUT.temperature_node1,
  dataOUT.temperature_node2,
  dataOUT.temperature_node3,
  dataOUT.temperature_node4,

  dataOUT.temperature_rate_node1,
  dataOUT.temperature_rate_node2,
  dataOUT.temperature_rate_node3,
  dataOUT.temperature_rate_node4,

  dataOUT.damage,

  dataOUT.connectivity_node1,
  dataOUT.connectivity_node2,
  dataOUT.connectivity_node3,
  dataOUT.connectivity_node4,

  dataOUT.globalElementNumber,
  dataOUT.localElementNumber,
  dataOUT.materialGroupNumber);
	
    if (send(sd, buffer, size, 0) != SOCKET_ERROR) //I should check if retval is equal to size+1 and send data that is left
		return true;
	else
        return false;
}

int DC_Manager::ReceiveData(SOCKET sd, char * readBuffer)
{
    int nTotalBytes = 0;
    do {
        int nNewBytes = recv(sd, readBuffer + nTotalBytes, 512 - nTotalBytes, 0);
        if (nNewBytes == SOCKET_ERROR) {
            return -1;
        }
        else if (nNewBytes == 0) {
            return 0;
        }

        nTotalBytes += nNewBytes;
    } while (readBuffer[nTotalBytes-1] != 'T');

    return nTotalBytes;
}

DC_Manager& DC_Manager::getManager()
{
  static DC_Manager instance;
  return instance;
}

DC_DataPackIN& DC_Manager::getDataPackIN()
{
  static DC_DataPackIN  data;
  return data;
}

