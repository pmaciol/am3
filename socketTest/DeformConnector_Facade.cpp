#include "DeformConnector_DataPack.h"
#include "DeformConnector_Manager.h"
#include "DeformConnector_Logger.h"

extern "C" int DEFORMMTRCSEND(
  double* RZE_1_1, double* RZE_2_1, double* RZE_1_2, double* RZE_2_2, double* RZE_1_3, double* RZE_2_3, double* RZE_1_4, double* RZE_2_4, 
   double* URZE_1_1, double* URZE_2_1, double* URZE_1_2, double* URZE_2_2, double* URZE_1_3, double* URZE_2_3, double* URZE_1_4, double* URZE_2_4, 
   double* STSE_1, double* STSE_2, double* STSE_3, double* STSE_4, 
   double* EPSE_1, double* EPSE_2, double* EPSE_3, double* EPSE_4, 
   double* EFEPSE, double*  EFSTSE, double*  TEPSE, double*  RDTYE, 
   double* TEMPE_1, double* TEMPE_2, double* TEMPE_3, double* TEMPE_4, 
  double* DTMPE_1, double* DTMPE_2, double* DTMPE_3, double* DTMPE_4, 
   double* DAMAGE, 
   double* NODEE_1, double* NODEE_2, double* NODEE_3, double* NODEE_4, 
   int* KELE, int*  KELEL, int*  KGROUP
  )
{
	static DC_DataPackOUT dataOUT;



	
  dataOUT.coord_x_node1=*RZE_1_1;
  dataOUT.coord_x_node2=*RZE_1_2;
 dataOUT.coord_x_node3=*RZE_1_3;
  dataOUT.coord_x_node4=*RZE_1_4;
   dataOUT.coord_y_node1=*RZE_2_1;
  dataOUT.coord_y_node2=*RZE_2_2;
  dataOUT.coord_y_node3=*RZE_2_3;
  dataOUT.coord_y_node4=*RZE_2_4;

  dataOUT.velocity_x_node1=*URZE_1_1;
  dataOUT.velocity_x_node2=*URZE_1_2;
  dataOUT.velocity_x_node3=*URZE_1_3;
  dataOUT.velocity_x_node4=*URZE_1_4;
  dataOUT.velocity_y_node1=*URZE_2_1;
  dataOUT.velocity_y_node2=*URZE_2_2;
  dataOUT.velocity_y_node3=*URZE_2_3;
  dataOUT.velocity_y_node4=*URZE_2_4;
  
  dataOUT.stress_node1=*STSE_1;
  dataOUT.stress_node2=*STSE_2;
  dataOUT.stress_node3=*STSE_3;
  dataOUT.stress_node4=*STSE_4;

  dataOUT.strain_rate_node1=*EPSE_1;
  dataOUT.strain_rate_node2=*EPSE_2;
  dataOUT.strain_rate_node3=*EPSE_3;
  dataOUT.strain_rate_node4=*EPSE_4;

    
  
  dataOUT.effectiveStress=*EFEPSE;
  dataOUT.effectiveStrainRate=*EFSTSE;
  dataOUT.totalEffectiveStrain=*TEPSE;
  dataOUT.density=*RDTYE;

  
  dataOUT.temperature_node1=*TEMPE_1;
  dataOUT.temperature_node2=*TEMPE_2;
  dataOUT.temperature_node3=*TEMPE_3;
  dataOUT.temperature_node4=*TEMPE_4;

  dataOUT.temperature_rate_node1=*DTMPE_1;
  dataOUT.temperature_rate_node2=*DTMPE_2;
  dataOUT.temperature_rate_node3=*DTMPE_3;
  dataOUT.temperature_rate_node4=*DTMPE_4;

  dataOUT.damage=*DAMAGE;

  dataOUT.connectivity_node1=*NODEE_1;
  dataOUT.connectivity_node2=*NODEE_2;
  dataOUT.connectivity_node3=*NODEE_3;
  dataOUT.connectivity_node4=*NODEE_4;

  dataOUT.globalElementNumber=*KELE;
  dataOUT.localElementNumber=*KELEL;
  dataOUT.materialGroupNumber=*KGROUP;

	DC_Logger::initLogFile();

	DC_Manager &manager = DC_Manager::getManager();
	if(!manager.isOperable())
	{
		DC_Logger::reportOnce(manager.lastErrorDescription());
		return -1;
	}
		
	manager.deformSend(dataOUT);
	
	if(!manager.isOperable())
	{
		DC_Logger::reportOnce(manager.lastErrorDescription());
		return -1;
	}
	
	manager.deformReceive(manager.getDataPackIN());
	
	if(!manager.isOperable())
	{
		DC_Logger::reportOnce(manager.lastErrorDescription());
		return -1;
	}
	
	if(manager.isVerbose())
	{
		DC_Logger::logDataOUT(dataOUT);
		DC_Logger::logDataIN(manager.getDataPackIN());
	}
	
	return 0;
}

extern "C" void DEFORMMTRCGET(
	
	double *yield_stress,
	double *d_flow_stress_strain,
	double *d_flow_stress_strain_rate
  )
{
	DC_DataPackIN& data = DC_Manager::getManager().getDataPackIN();
	
	*yield_stress = data.yield_stress;							//CMAT(1 ) = XMU   = VISCOSITY
	*d_flow_stress_strain = data.d_flow_stress_strain ;											//CMAT(2 ) = CP    = SPECIFIC HEAT AT CONSTANT PRESSURE
	*d_flow_stress_strain_rate = data.d_flow_stress_strain_rate ;		//CMAT(3 ) = XKCON = THERMAL CONDUCTIVITY
}