#ifndef DC_LOGGER
#define DC_LOGGER

#include "DeformConnector_DataPack.h"

class DC_Logger
{
	public:
		static void initLogFile();
		static void reportOnce(const char * s);
		static void logDataIN(const DC_DataPackIN& data);
		static void logDataOUT(const DC_DataPackOUT& data);

	private:
		static const char * _file;
		static bool _fileInitialized;
};

#endif