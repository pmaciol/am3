#ifndef DC_DATAPACK
#define DC_DATAPACK

struct DC_DataPackOUT
{
  double coord_x_node1;
  double coord_x_node2;
  double coord_x_node3;
  double coord_x_node4;
  double coord_y_node1;
  double coord_y_node2;
  double coord_y_node3;
  double coord_y_node4;

  double velocity_x_node1;
  double velocity_x_node2;
  double velocity_x_node3;
  double velocity_x_node4;
  double velocity_y_node1;
  double velocity_y_node2;
  double velocity_y_node3;
  double velocity_y_node4;

  double stress_node1;
  double stress_node2;
  double stress_node3;
  double stress_node4;

  double strain_rate_node1;
  double strain_rate_node2;
  double strain_rate_node3;
  double strain_rate_node4;

  double temperature_node1;
  double temperature_node2;
  double temperature_node3;
  double temperature_node4;

  double temperature_rate_node1;
  double temperature_rate_node2;
  double temperature_rate_node3;
  double temperature_rate_node4;

  double connectivity_node1;
  double connectivity_node2;
  double connectivity_node3;
  double connectivity_node4;

  double effectiveStress, effectiveStrainRate, totalEffectiveStrain, density, damage;
  int globalElementNumber, localElementNumber, materialGroupNumber;
};

struct DC_DataPackIN
{
  double yield_stress;
  double d_flow_stress_strain;
  double d_flow_stress_strain_rate;
};

#endif