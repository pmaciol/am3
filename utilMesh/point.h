/*! \file Point.h **************************************************
* \author	Grzegorz Wiaderek
* \copyright 	GNU Public License.
* \brief       
* \details   	
*********************************************************************/
#ifndef mathutilpoint_h__
#define mathutilpoint_h__

#include <math.h>
#include "../models/point.h"
#include "../models/description.h"

namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

namespace am3
{
    namespace math
    {
        namespace mesh
        {
            template< int DIM > 
            struct Point
            {
                double v[DIM];
            };

            template <>
            struct Point<3>
            {
                union
                {
                    double v[3];
                    struct
                    {
                        double x, y, z;
                    };
                };

                Point() {}

                Point( double xx, double yy, double zz )
                {
                    x = xx;
                    y = yy;
                    z = zz;
                }
            };

            template <>
            struct Point<2>
            {
                union
                {
                    double v[2];
                    struct
                    {
                        double x, y;
                    };
                };

                Point() {}

				Point(const  mpt::Point<mpr::Coordinates2D>& that)
				{
					const mpr::Coordinates2D coord = that.Coordinates();
					x = coord.x();
					y = coord.y();
				}


                Point( double xx, double yy )
                {
                    x = xx;
                    y = yy;
                }
            };

            template< int DIM >
            inline Point<DIM> operator+( const Point<DIM>& p1, const Point<DIM>& p2 )
            {
                Point<DIM> p;
                for( int i = 0; i < DIM; i++ )
                    p.v[i] = p1.v[i]+p2.v[i];
                return p;
            }


            template< int DIM >
            inline Point<DIM> operator-( const Point<DIM>& p1, const Point<DIM>& p2 )
            {
                Point<DIM> p;
                for( int i = 0; i < DIM; i++ )
                    p.v[i] = p1.v[i]-p2.v[i];
                return p;
            }


            template< int DIM >
            inline Point<DIM> operator*( const Point<DIM>& p1, double d )
            {
                Point<DIM> p;
                for( int i = 0; i < DIM; i++ )
                    p.v[i] = p1.v[i]*d;
                return p;
            }


            inline Point<3> cross_product( const Point<3>& p1, const Point<3>& p2 )
            {
                return Point<3>( p1.y*p2.z - p2.y*p1.z,
                    p2.x*p1.z - p1.x*p2.z,
                    p1.x*p2.y - p2.x*p1.y );
            }


            template< int DIM >
            inline double dot_product( const Point<DIM>& p1, const Point<DIM>& p2 )
            {
                double d = 0;
                for( int i = 0; i < DIM; i++ )
                    d += p1.v[i]*p2.v[i];
                return d;
            }

            template< int DIM >
            inline double length( const Point<DIM>& v )
            {
                double s = 0;
                for( int i = 0; i < DIM; i++ )
                    s += v.v[i]*v.v[i];
                return sqrt(s);
            }

        } //mesh
    } //math
} //am3


#endif //mathutilpoint_h__
