/*! \file MeshApproximation.h ***************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief
* \details
*********************************************************************/

#ifndef meshApprox_h__
#define meshApprox_h__

#include <vector>
#include "mesh.h"
#include "../models/dataStructures.h"

namespace mpr = am3::model::properties;

namespace am3
{
    namespace math
    {
        namespace mesh
        {
            template< class T_DATA >
            class NodeDataProvider
            {
            public:
                virtual T_DATA* GetValue(int node_id) = 0;
            };

            //======================================================================================================================

            //
            // Abstract Mesh Approximation.
            //
            template< class T_DATA, int DIM >
            class MeshApprox
            {
            public:
                MeshApprox(NodeDataProvider<T_DATA>* provider)
                {
                    provider_ = provider;
                }

                bool Approximate(Mesh<DIM>& mesh, const Point<DIM>& pt, T_DATA& result);

            protected:
                virtual bool ApproximateCore(Mesh<DIM>& mesh, int elemIdx, const Point<DIM>& pt, T_DATA& result) = 0;

            protected:
                NodeDataProvider<T_DATA>* provider_;
            };


            //======================================================================================================================    


            template< class T_DATA, int DIM >
            bool MeshApprox<T_DATA, DIM>::Approximate(Mesh<DIM>& mesh, const Point<DIM>& pt, T_DATA& result)
            {                
                mpr::ElementNumber elemId;
                if(!mesh.FindElementContaining(pt, elemId))
                {
                    //not found
                    return false;
                }
                int elemIdx = mesh.ElemIdxFromId(elemId);
                return ApproximateCore(mesh, elemIdx, pt, result);
            }

        } //mesh
    } //math
} //am3

#endif //meshApprox_h__
