/*! \file meshList.h **************************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief
* \details
*********************************************************************/

#ifndef meshList_h__
#define meshList_h__

#include <vector>
#include <string>
#include "point.h"
#include "meshApprox.h"

namespace lo = am3::logger;
namespace mpr = am3::model::properties;


namespace am3
{
    namespace math
    {
        namespace mesh
        {
            //=============================================================================
            // MeshList: Collection of meshes for time & space approximation.
            //
            template< class T_DATA, int DIM >
            class MeshList : public std::vector< Mesh<DIM>* >
            {
            public:
                //Time & space approximation
                bool Approximate(MeshApprox<T_DATA, DIM>& approx, const Point<DIM>& pt, double time, T_DATA& result);

            protected:
                int bisection(double time, int bottom, int top, int* before, int* after);
            };

            //==============================================================================

            template< class T_DATA, int DIM >
            bool MeshList<T_DATA, DIM>::Approximate(MeshApprox<T_DATA, DIM>& approx, const Point<DIM>& pt, double time, T_DATA& result)
            {
                double t0 = at(0)->get_time();
                double t1 = at(size()-1)->get_time();
                if(time < t0 || time > t1)
                {
                    //time out of range
                    return false;
                }

                int before, after;
                int b;

                if(time == t0)
                {
                    before = 0;
                    b = 1;
                }
                else if(time == t1)
                {
                    before = size()-1;
                    b = 1;
                }
                else
                {
                    b = bisection(time, 0, size()-1, &before, &after);
                }

                if(b == 1)
                {
                    return approx.Approximate(*at(before), pt, result);
                }
                else // b == 2
                {
                    T_DATA result1;
                    T_DATA result2;

                    if(!approx.Approximate(*at(before), pt, result1))
                        return false;

                    if(!approx.Approximate(*at(after), pt, result2))
                        return false;

                    double l2 = (time-t0)/(t1-t0);
                    double l1 = 1.0 - l2;

                    CopyVisitor<T_DATA>::VisitList(&result, &result1);  //result = result1
                    MultVisitor<T_DATA>::VisitList(&result, l1);        //result *= l1

                    T_DATA a;
                    CopyVisitor<T_DATA>::VisitList(&a, &result2);   // a = result2
                    MultVisitor<T_DATA>::VisitList(&a, l2);         // a *= l2
                    AddVisitor<T_DATA>::VisitList(&result, &a);     //result += a

                    return true;
                }
            }

            template< class T_DATA, int DIM >
            int MeshList<T_DATA, DIM>::bisection(double time, int bottom, int top, int* before, int* after)
            {
                if(top == bottom+1)
                {
                    *before = bottom;
                    *after = top;
                    return 2;
                }

                int h = (bottom+top)/2;
                double t = at(h)->get_time();

                if(time == t)
                {
                    *before = *after = h;
                    return 1;
                }
                else if(time < t)
                {
                    return bisection(time, bottom, h, before, after);
                }
                else // time > t
                {
                    return bisection(time, h, top, before, after);
                }
            }

        } //mesh
    } //math
} //am3

#endif //meshList_h__
