/*! \file Mesh.cpp **************************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief
* \details
*********************************************************************/

#include "mesh.h"

namespace am3
{
    namespace math
    {
        namespace mesh
        {
            template<>
            bool Mesh<3>::SetupFaceNormal(MeshElementFace<3>& f, int other_vertex_index)
            {
                Point<3>& A = vertices_[f.vert_indices_[0]];
                Point<3>& B = vertices_[f.vert_indices_[1]];
                Point<3>& C = vertices_[f.vert_indices_[2]];
                f.normal_ = cross_product(B-A, C-A);

                Point<3>& O = vertices_[other_vertex_index];
                double dp = dot_product(f.normal_, O-A);
                if(dp == 0)
                {
                    //error!
                    lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Invalid element face");
                    return false;
                }
                if(dp < 0)
                {
                    f.normal_ = f.normal_*(-1);
                }
                return true;
            }

            template<>
            bool Mesh<2>::SetupFaceNormal(MeshElementFace<2>& f, int other_vertex_index)
            {
                Point<2>& A = vertices_[f.vert_indices_[0]];
                Point<2>& B = vertices_[f.vert_indices_[1]];

                Point<2> AB = B-A;
                f.normal_.x = AB.y;
                f.normal_.y = -AB.x;

                Point<2>& O = vertices_[other_vertex_index];
                double dp = dot_product(f.normal_, O-A);
                if(dp == 0)
                {
                    //error!
                    return false;
                }
                if(dp < 0)
                {
                    f.normal_ = f.normal_*(-1);
                }
                return true;
            }


            //shortcut for adding tetrahedral element
            template<>
            int Mesh<3>::AddTetrahedron(VertexId vertexId0, VertexId vertexId1, VertexId vertexId2, VertexId vertexId3, mpr::ElementNumber elemId)
            {
                int elemIdx = AddElement(4, elemId);

                //Face 0
                SetElementFace(elemId, 0, 3);
                SetElementFaceVertex(elemId, 0, 0, vertexId0);
                SetElementFaceVertex(elemId, 0, 1, vertexId1);
                SetElementFaceVertex(elemId, 0, 2, vertexId2);

                //Face 1
                SetElementFace(elemId, 1, 3);
                SetElementFaceVertex(elemId, 1, 0, vertexId3);
                SetElementFaceVertex(elemId, 1, 1, vertexId0);
                SetElementFaceVertex(elemId, 1, 2, vertexId2);

                //Face 2
                SetElementFace(elemId, 2, 3);
                SetElementFaceVertex(elemId, 2, 0, vertexId0);
                SetElementFaceVertex(elemId, 2, 1, vertexId3);
                SetElementFaceVertex(elemId, 2, 2, vertexId1);

                //Face 3
                SetElementFace(elemId, 3, 3);
                SetElementFaceVertex(elemId, 3, 0, vertexId1);
                SetElementFaceVertex(elemId, 3, 1, vertexId3);
                SetElementFaceVertex(elemId, 3, 2, vertexId2);

                return elemIdx;
            }

            //shortcut for adding triangular element
            template<>
            int Mesh<2>::AddTriangle(VertexId vertexId0, VertexId vertexId1, VertexId vertexId2, mpr::ElementNumber elemId)
            {
                int elemIdx = AddElement(3, elemId);

                //Face 0
                SetElementFace(elemId, 0, 2);
                SetElementFaceVertex(elemId, 0, 0, vertexId1);
                SetElementFaceVertex(elemId, 0, 1, vertexId2);

                //Face 1
                SetElementFace(elemId, 1, 2);
                SetElementFaceVertex(elemId, 1, 0, vertexId0);
                SetElementFaceVertex(elemId, 1, 1, vertexId2);

                //Face 2
                SetElementFace(elemId, 2, 2);
                SetElementFaceVertex(elemId, 2, 0, vertexId0);
                SetElementFaceVertex(elemId, 2, 1, vertexId1);

                return elemIdx;
            }

            //shortcut for adding triangular element (vertices must be listed in circular order!)
            template<>
            int Mesh<2>::AddTetragon(VertexId vertexId0, VertexId vertexId1, VertexId vertexId2, VertexId vertexId3, mpr::ElementNumber elemId)
            {
                int elemIdx = AddElement(4, elemId);

                //Face 0
                SetElementFace(elemId, 0, 2);
                SetElementFaceVertex(elemId, 0, 0, vertexId0);
                SetElementFaceVertex(elemId, 0, 1, vertexId1);

                //Face 1
                SetElementFace(elemId, 1, 2);
                SetElementFaceVertex(elemId, 1, 0, vertexId1);
                SetElementFaceVertex(elemId, 1, 1, vertexId2);

                //Face 2
                SetElementFace(elemId, 2, 2);
                SetElementFaceVertex(elemId, 2, 0, vertexId2);
                SetElementFaceVertex(elemId, 2, 1, vertexId3);

                //Face 3
                SetElementFace(elemId, 3, 2);
                SetElementFaceVertex(elemId, 3, 0, vertexId3);
                SetElementFaceVertex(elemId, 3, 1, vertexId0);

                return elemIdx;
            }

        } //mesh
    } //math
} //am3
