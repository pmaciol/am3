/*! \file mesh.h **************************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief
* \details
*********************************************************************/

#ifndef mesh_h__
#define mesh_h__

#include <vector>
#include <unordered_map>
#include <string>
#include <boost/lexical_cast.hpp>
#include "point.h"
#include "../logger/logger.h"
#include "../models/dataStructures.h"
#include "../models/namedPoint.h"

namespace lo = am3::logger;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;


namespace am3
{
    namespace math
    {
        namespace mesh
        {
            //-            typedef int ElementNumber;
            //-            typedef int MeshId;
            typedef unsigned long VertexId; //???

            template< int DIM >
            struct MeshElementFace
            {
                std::vector<int> vert_indices_; //indices in Mesh::vertices_ of this face's vertices
                Point<DIM> normal_; //normal vector of the plane containing face

                MeshElementFace(int numVertices) : vert_indices_(numVertices) {}
                MeshElementFace() {}
            };

            //==============================================================================

            template< int DIM >
            struct MeshElement
            {
                std::vector< MeshElementFace<DIM> > faces_;
                std::vector<int> vert_indices_; //union of all faces' vert_indices_
                mpr::ElementNumber id_;

                MeshElement(int numFaces, mpr::ElementNumber id) : faces_(numFaces) { id_ = id; }
            };

            template< int DIM >
            class Mesh;

            //=============================================================================
            // Abstract Mesh Searcher
            //
            template <int DIM>
            class MeshSearcher
            {
            public:
                MeshSearcher(Mesh<DIM>* m)
                {
                    mesh_ = m;
                }

                virtual ~MeshSearcher() {}

                Mesh<DIM>* mesh() { return mesh_; }

                virtual bool Organize() = 0;
                virtual int FindElementContaining(const Point<DIM>& p) = 0;

            protected:
                Mesh<DIM>* mesh_;
            };

            //=============================================================================
            // Very simple MeshSearcher implementation.
            // For testing purposes only!
            //
            template< int DIM >
            class DummyMeshSearcher : public MeshSearcher<DIM>
            {
            public:
                DummyMeshSearcher(Mesh<DIM>* m) : MeshSearcher<DIM>(m) {}

                //Overrides
                virtual bool Organize() { return true; }
                virtual int FindElementContaining(const Point<DIM>& p);
            };


            //=============================================================================
            // Mesh.
            //
            template< int DIM >
            class Mesh
            {
            public:
                Mesh(mpr::MeshId id);
                virtual ~Mesh();

                MeshSearcher<DIM>* get_searcher()             { return searcher_; }
                void set_searcher(MeshSearcher<DIM>* ms)      { searcher_ = ms; }

                mpr::MeshId get_id()              { return id_; }
                void set_id(mpr::MeshId id)       { id_ = id; }

                double get_time()                 { return time_; }
                void set_time(double time)        { time_ = time; }

                //-------------------------------------------------------------------------
                //Mesh Creation Methods
                //

                int AddVertex(const Point<DIM>& vrtx, VertexId id)
                {
                    vertices_.push_back(vrtx);
                    vertIds_.push_back(id);
                    int idx = vertices_.size()-1;
                    vertId_to_idx_.emplace(std::make_pair(id, idx));
                    return idx;
                }

                // Warning! This function does not check NamedPoint dimension!
                template<typename T>
                int AddVertex(const mpt::NamedPoint<T> point)
                {
                    Point<DIM> p(point);
                    vertices_.push_back(p);
                    vertIds_.push_back(point.GetId());
                    int idx = vertices_.size()-1;
                    vertId_to_idx_.emplace(std::make_pair(point.GetId(), idx));
                    return idx;
                }

                bool VertexExists(VertexId id)
                {
                    return (vertId_to_idx_.count(id) > 0);
                }

                int AddElement(int numFaces, mpr::ElementNumber id)
                {
                    elements_.push_back(MeshElement<DIM>(numFaces, id));
                    int idx = elements_.size()-1;
                    elemId_to_idx_.emplace(std::make_pair(id(), idx));
                    return idx;
                }

                void SetElementFace(mpr::ElementNumber elementId, int faceIdx, int numVertices)
                {
                    int elementIdx = elemId_to_idx_[elementId()];
                    MeshElement<DIM>& element = elements_[elementIdx];
                    element.faces_[faceIdx] = MeshElementFace<DIM>(numVertices);
                }

                void SetElementFaceVertex(mpr::ElementNumber elementId, int faceIdx, int faceVertexIdx, VertexId vertexId)
                {
                    int elementIdx = elemId_to_idx_[elementId()];
                    int vertexIdx = vertId_to_idx_[vertexId];
                    elements_[elementIdx].faces_[faceIdx].vert_indices_[faceVertexIdx] = vertexIdx;
                }

                //shortcut for adding tetrahedral element
                int AddTetrahedron(VertexId vertexId1, VertexId vertexId2, VertexId vertexId3, VertexId vertexId4, mpr::ElementNumber id);

                //2D--------------------------------------------

                //shortcut for adding triangular element
                int AddTriangle(VertexId vertexId1, VertexId vertexId2, VertexId vertexId3, mpr::ElementNumber id);

                //shortcut for adding tetragonal element (vertices must be listed in circular order!)
                int AddTetragon(VertexId vertexId1, VertexId vertexId2, VertexId vertexId3, VertexId vertexId4, mpr::ElementNumber id);

                //-----------------------------------------------------------------------
                // Mesh Organizing
                //

                //should be called AFTER mesh creation, BEFORE mesh searching 
                bool Organize();

                //-----------------------------------------------------------------------
                // Mesh Searching
                //

                bool FindElementContaining(const Point<DIM>& p, mpr::ElementNumber& elemId)
                {
                    int elemIdx = searcher_->FindElementContaining(p);
                    if(elemIdx == -1)
                    {
                        return false;
                    }
                    else
                    {
                        elemId = elements_[elemIdx].id_;
                        return true;
                    }
                }

                bool PointInElement(const Point<DIM>& p, int elementIdx);

                VertexId GetVertexId(unsigned long vertex_idx)
                {
                    return vertIds_[vertex_idx];
                }

                int ElemIdxFromId(mpr::ElementNumber id)
                {
                    return elemId_to_idx_[id()];
                }

            protected:
                bool OrganizeElement(MeshElement<DIM>& elem);
                bool SetupFaceNormal(MeshElementFace<DIM>& f, int other_vertex_index);

            public:
                std::vector< Point<DIM> > vertices_;
                std::vector< MeshElement<DIM> > elements_;
            protected:
                MeshSearcher<DIM>* searcher_;
                mpr::MeshId id_;
                double time_;

                std::vector<VertexId> vertIds_;     //vertex IDs in the same order as points in vertices_
                std::unordered_map<VertexId, unsigned long> vertId_to_idx_; //dictionary VertexId -> index-in-vertices_
                std::unordered_map<int, unsigned long> elemId_to_idx_;      //dictionary ElementNumber -> index-in-elements_
            };


            //=======================================================================================

            template< int DIM >
            Mesh<DIM>::Mesh(mpr::MeshId id)
            {
                searcher_ = new DummyMeshSearcher<DIM>(this);
                id_ = id;
            }

            template< int DIM >
            Mesh<DIM>::~Mesh()
            {
                delete searcher_;
            }


            template< int DIM >
            bool Mesh<DIM>::PointInElement(const Point<DIM>& P, int elementIdx)
            {
                MeshElement<DIM>& element = elements_[elementIdx];

                for(size_t i = 0; i < element.faces_.size(); i++)
                {
                    MeshElementFace<DIM>& f = element.faces_[i];
                    Point<DIM>& A = vertices_[f.vert_indices_[0]];
                    if(dot_product(f.normal_, P-A) < 0)
                        return false;
                }
                return true;
            }

            static bool contains(std::vector<int>& v, int val)
            {
                for(size_t i = 0; i < v.size(); i++)
                {
                    if(v[i] == val)
                        return true;
                }
                return false;
            }

            template<>
            bool Mesh<3>::SetupFaceNormal(MeshElementFace<3>& f, int other_vertex_index);

            template<>
            bool Mesh<2>::SetupFaceNormal(MeshElementFace<2>& f, int other_vertex_index);

            template< int DIM >
            bool Mesh<DIM>::OrganizeElement(MeshElement<DIM>& elem)
            {
                //
                // Fill vert_indices_
                //
                for(size_t i = 0; i < elem.faces_.size(); i++)
                {
                    MeshElementFace<DIM>& f = elem.faces_[i];
                    for(size_t j = 0; j < f.vert_indices_.size(); j++)
                    {
                        int vert_idx = f.vert_indices_[j];
                        if(!contains(elem.vert_indices_, vert_idx))
                            elem.vert_indices_.push_back(vert_idx);
                    }
                }

                //
                // Setup Face::normal_ of all faces 
                //    
                bool ok = true;
                for(size_t i = 0; i < elem.faces_.size(); i++)
                {
                    MeshElementFace<DIM>& f = elem.faces_[i];

                    // Find vertex NOT from this face
                    //
                    int other_vertex_index = -1;

                    for(size_t j = 0; j < elem.vert_indices_.size(); j++)
                    {
                        int vert_idx = elem.vert_indices_[j];
                        if(!contains(f.vert_indices_, vert_idx))
                        {
                            other_vertex_index = vert_idx;
                            break;
                        }
                    }

                    if(other_vertex_index == -1)
                    {
                        //error!
                        ok = false;
                    }
                    else
                    {
                        if(!SetupFaceNormal(f, other_vertex_index))
                        {
                            ok = false;
                        }
                    }
                }
                return ok;
            }


            template< int DIM >
            bool Mesh<DIM>::Organize()
            {
                bool ok = true;
                for(size_t i = 0; i < elements_.size(); i++)
                {
                    if(!OrganizeElement(elements_[i]))
                    {
                        std::string str = "OrganizeElement failed for element at "+std::to_string((unsigned long long)i);
                        lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, str.c_str());
                        ok = false;
                    }
                }

                ok = ok && searcher_->Organize();
                return ok;
            }

            //==================================================================================================

            template< int DIM >
            int DummyMeshSearcher<DIM>::FindElementContaining(const Point<DIM>& p)
            {
                for(size_t i = 0; i < MeshSearcher<DIM>::mesh_->elements_.size(); i++)
                    if(MeshSearcher<DIM>::mesh_->PointInElement(p, i))
                        return i;
                return -1;
            }

        } //mesh
    } //math
} //am3

#endif //mesh_h__
