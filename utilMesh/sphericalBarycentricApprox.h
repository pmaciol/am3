/*! \file sphericalBarycentricApprox.h *******************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief  Approximation based on Generalized Spherical Mean-Value Barycentric Coordinates.
* \details   	
*********************************************************************/

#ifndef sphericalBarycentricApprox_h__
#define sphericalBarycentricApprox_h__

#include "meshApprox.h"
#include "../storage/makeData.h"

namespace am3
{
    namespace math
    {
        namespace mesh
        {
            //
            // Approximation based on Spherical Generalized Barycentric Coordinate System.
            //
            template< typename T_DATA, int DIM >
            class SphericalBarycentricApprox : public MeshApprox<T_DATA, DIM>
            {
            };

            //==================================================================================================

            template< typename T_DATA >
            class SphericalBarycentricApprox<T_DATA, 3> : public MeshApprox<T_DATA, 3>
            {
            public:
                SphericalBarycentricApprox(NodeDataProvider<T_DATA>* provider) : MeshApprox<T_DATA, 3>(provider) {}

            protected:
                virtual bool ApproximateCore(Mesh<3>& mesh, int elemIdx, const Point<3>& pt, T_DATA& result);
            };


            template< typename T_DATA >
            class SphericalBarycentricApprox<T_DATA, 2> : public MeshApprox<T_DATA, 2>
            {
            public:
                SphericalBarycentricApprox(NodeDataProvider<T_DATA>* provider) : MeshApprox<T_DATA, 2>(provider) {}

            protected:
                virtual bool ApproximateCore(Mesh<2>& mesh, int elemIdx, const Point<2>& pt, T_DATA& result);
            };



            template< typename T_DATA >
            bool SphericalBarycentricApprox<T_DATA, 3>::ApproximateCore(Mesh<3>& mesh, int elemIdx, const Point<3>& pt, T_DATA& result)
            {
                MeshElement<3>& elem = mesh.elements_[elemIdx];
                int n = elem.vert_indices_.size();

                std::vector<double> lambda;
                if(!sphericalBarycentric3D(mesh, elemIdx, pt, lambda))
                {
                    //error!
                    return false;
                }

                std::vector<T_DATA> d(n);
                for(int j = 0; j < n; j++)
                {
                    d[j] = *provider_->GetValue(mesh.GetVertexId(elem.vert_indices_[j]));
                }

                CopyVisitor<T_DATA>::VisitList(&result, &d[0]);     //result = d[0]
                MultVisitor<T_DATA>::VisitList(&result, lambda[0]); //result *= lambda[0]

                for(int j = 1; j < n; j++)
                {
                    T_DATA a;
                    CopyVisitor<T_DATA>::VisitList(&a, &d[j]);      //a = d[j]
                    MultVisitor<T_DATA>::VisitList(&a, lambda[j]);  //a *= lambda[j]
                    AddVisitor<T_DATA>::VisitList(&result, &a);     //result += a
                }

                return true;
            }

            //==============================================================================

            template< typename T_DATA >
            bool SphericalBarycentricApprox<T_DATA, 2>::ApproximateCore(Mesh<2>& mesh, int elemIdx, const Point<2>& pt, T_DATA& result)
            {
                MeshElement<2>& elem = mesh.elements_[elemIdx];
                int n = elem.vert_indices_.size();

                std::vector<double> lambda;
                if(!sphericalBarycentric2D(mesh, elemIdx, pt, lambda))
                {
                    //error!
                    return false;
                }

                std::vector<T_DATA> d(n);
                for(int j = 0; j < n; j++)
                {
                    d[j] = *provider_->GetValue(mesh.GetVertexId(elem.vert_indices_[j]));
                }

                CopyVisitor<T_DATA>::VisitList(&result, &d[0]);     //result = d[0]
                MultVisitor<T_DATA>::VisitList(&result, lambda[0]); //result *= lambda[0]

                for(int j = 1; j < n; j++)
                {
                    T_DATA a;
                    CopyVisitor<T_DATA>::VisitList(&a, &d[j]);      //a = d[j]
                    MultVisitor<T_DATA>::VisitList(&a, lambda[j]);  //a *= lambda[j]
                    AddVisitor<T_DATA>::VisitList(&result, &a);     //result += a
                }

                return true;
            }


            bool sphericalBarycentric3D(Mesh<3>& mesh, int elemIdx, const Point<3>& pt, std::vector<double>& lambda3D);

            bool sphericalBarycentric2D(Mesh<2>& mesh, int elemIdx, const Point<2>& pt, std::vector<double>& lambda);
        }
    }
}

#endif //sphericalBarycentricApprox_h__
