/*! \file BintreeMeshSearcher.h ***************************************
* \author	Grzegorz Wiaderek
* \copyright 	GNU Public License.
* \brief       
* \details   	
*********************************************************************/

#ifndef bintreeMeshSearcher_h__
#define bintreeMeshSearcher_h__

#include "mesh.h"
#include "cfloat" //DBL_MAX

namespace am3
{
    namespace math
    {
        namespace mesh
        {
            template< int DIM >
            class BintreeMeshSearcher;

            //========================================================
            // Bintree node for BintreeMeshSearcher.
            //
            template< int DIM >
            class BintreeNode
            {
            public:
                BintreeNode( BintreeMeshSearcher<DIM>* owner, const Point<DIM>& bounds_min, const Point<DIM>& bounds_max );
                ~BintreeNode();

                void Organize();
                int FindElementContaining( const Point<DIM>& p );
                int FindChildContaining( const Point<DIM>& p );

            protected:
                void CreateChildren();    
                bool Contains( const Point<DIM>& p );
                bool IntersectsElement( int elementIdx );
                void FindIntersectedElements( std::vector<int>& elem_indices );

            protected:
                BintreeMeshSearcher<DIM>* owner_;
                BintreeNode<DIM>** children_;
                Point<DIM> bounds_min_;
                Point<DIM> bounds_max_;
                Point<DIM> center_;
            public:
                std::vector<int> element_indices_;
            };


            //========================================================
            // Bintree based MeshSearcher implementation.
            //
            template< int DIM >
            class BintreeMeshSearcher : public MeshSearcher<DIM>
            {
            public:
                BintreeMeshSearcher( Mesh<DIM>* mesh );
                ~BintreeMeshSearcher();

                size_t get_split_threshold()           { return split_threshold_; }
                void set_split_threshold( size_t st )  { split_threshold_ = st; }

                //Overrides
                virtual bool Organize();
                virtual int FindElementContaining( const Point<DIM>& p );

            protected:
                BintreeNode<DIM>* main_node_;
                size_t split_threshold_; //node will be split if contains more than split_threshold_ elements
            };

            //========================================================================================================

            template< int DIM >
            BintreeNode<DIM>::BintreeNode( BintreeMeshSearcher<DIM>* owner, const Point<DIM>& bounds_min, const Point<DIM>& bounds_max )
                : owner_(owner),
                bounds_min_(bounds_min),
                bounds_max_(bounds_max),
                center_((bounds_min+bounds_max)*0.5)
            {
                children_ = 0; //nullptr;
            }

            template< int DIM >
            BintreeNode<DIM>::~BintreeNode()
            {
                if( children_ != 0 )//nullptr )
                {
                    for( int i = 0; i < 8; i++ )
                        delete children_[i];

                    delete children_;
                }
            }

            template< int DIM >
            void BintreeNode<DIM>::Organize()
            {
                if( element_indices_.size() > owner_->get_split_threshold() )
                {
                    CreateChildren();

                    for( int i = 0; i < 8; i++ )
                        children_[i]->FindIntersectedElements( element_indices_ );

                    for( int i = 0; i < 8; i++ )
                        children_[i]->Organize();

                    element_indices_.clear(); //they are no longer needed
                }
            }

            template< int DIM >
            void BintreeNode<DIM>::CreateChildren()
            {
                const int size = 1<<DIM; //2^DIM
                children_ = new BintreeNode*[size];

                for( int i = 0; i < size; i++ )
                {
                    Point<DIM> min;
                    Point<DIM> max;

                    for( int d = 0; d < DIM; d++ )
                    {
                        if( i & 1<<d )
                        {
                            min.v[d] = bounds_min_.v[d];
                            max.v[d] = center_.v[d];
                        }
                        else
                        {
                            min.v[d] = center_.v[d];
                            max.v[d] = bounds_max_.v[d];
                        }
                    }

                    children_[i] = new BintreeNode<DIM>( owner_, min, max );
                }
            }

            template< int DIM >
            int BintreeNode<DIM>::FindChildContaining( const Point<DIM>& p )
            {
                int idx = 0;
                for( int d = 0; d < DIM; d++ )
                {
                    if( p.v[d] > center_.v[d] )
                        idx += (1<<d);
                }
                return idx;
            }

            template< int DIM >
            int BintreeNode<DIM>::FindElementContaining( const Point<DIM>& p )
            {
                if( children_ != 0 )//nullptr )
                {
                    int idxChild = FindChildContaining(p);
                    return children_[idxChild]->FindElementContaining(p);
                }
                else
                {
                    for( size_t i = 0; i < element_indices_.size(); i++ )
                    {
                        int elem_idx = element_indices_[i];
                        if( owner_->mesh()->PointInElement(p, elem_idx) )
                            return elem_idx;
                    }
                    return -1;
                }
            }

            template< int DIM >
            bool BintreeNode<DIM>::Contains( const Point<DIM>& p )
            {
                for( int d = 0; d < DIM; d++ )
                {
                    if( p.v[d] < bounds_min_.v[d] || p.v[d] > bounds_max_.v[d] )
                        return false;
                }
                return true;
            }

            template< int DIM >
            bool BintreeNode<DIM>::IntersectsElement( int elementIdx )
            {
                //Test if we contain any element's vertex
                MeshElement<DIM>& elem = owner_->mesh()->elements_[elementIdx];
                for( size_t i = 0; i < elem.vert_indices_.size(); i++ )
                {
                    Point<DIM>& vrtx = owner_->mesh()->vertices_[elem.vert_indices_[i]];
                    if( Contains( vrtx ) )
                        return true;
                }

                //Test if element contains any our vertex
                const int size = 1<<DIM;
                for( int i = 0; i < size; i++ )
                {
                    Point<DIM> p;
                    for( int d = 0; d < DIM; d++ )
                    {
                        p.v[d] = (i & (1<<d)) ? bounds_min_.v[d] : bounds_max_.v[d];
                    }

                    if( owner_->mesh()->PointInElement( p, elementIdx ) )
                        return true;
                }

                return false;
            }

            template< int DIM >
            void BintreeNode<DIM>::FindIntersectedElements( std::vector<int>& elem_indices )
            {
                for( size_t i = 0; i < elem_indices.size(); i++ )
                {
                    int idx = elem_indices[i];
                    if( IntersectsElement( idx ) )
                    {
                        element_indices_.push_back(idx);
                    }
                }
            }


            //================================================================================================

            template< int DIM >
            BintreeMeshSearcher<DIM>::BintreeMeshSearcher( Mesh<DIM>* mesh )
                : MeshSearcher<DIM>(mesh)
            {
                main_node_ = 0; //nullptr;
                split_threshold_ = 20;
            }

            template< int DIM >
            BintreeMeshSearcher<DIM>::~BintreeMeshSearcher()
            {
                delete main_node_;
            }

            template< int DIM >
            bool BintreeMeshSearcher<DIM>::Organize()
            {
                //Find bounds
                Point<DIM> bmin;
                Point<DIM> bmax;
                for( int d = 0; d < DIM; d++ )
                {
                    bmin.v[d] = DBL_MAX;
                    bmax.v[d] = -DBL_MAX;
                }

                for( size_t i = 0; i < MeshSearcher<DIM>::mesh_->vertices_.size(); i++ )
                {
                    Point<DIM>& p = MeshSearcher<DIM>::mesh_->vertices_[i];

                    for( int d = 0; d < DIM; d++ )
                    {
                        if( p.v[d] < bmin.v[d] )
                            bmin.v[d] = p.v[d];

                        if( p.v[d] > bmax.v[d] )
                            bmax.v[d] = p.v[d];
                    }
                }

                //==========================

                main_node_ = new BintreeNode<DIM>( this, bmin, bmax );

                for( size_t i = 0; i < MeshSearcher<DIM>::mesh_->elements_.size(); i++ )
                    main_node_->element_indices_.push_back(i);

                main_node_->Organize();

                return true;
            }

            template< int DIM >
            int BintreeMeshSearcher<DIM>::FindElementContaining( const Point<DIM>& p )
            {
                return main_node_->FindElementContaining(p);
            }

        } //mesh
    } //math
} //am3

#endif //bintreeMeshSearcher_h__
