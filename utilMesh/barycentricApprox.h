/*! \file barycentricApprox.h ***************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief  Approximation based on Barycentric Coordinate System.    
* \details   	
*********************************************************************/

#ifndef barycentricApprox_h__
#define barycentricApprox_h__

#include "meshApprox.h"
#include "../storage/makeData.h"

namespace am3
{
    namespace math
    {
        namespace mesh
        {
            //
            // Approximation based on Barycentric Coordinate System.
            // Works only for triangles in 2D & for tetrahedra in 3D.
            //
            template< typename T_DATA, int DIM >
            class BarycentricApprox : public MeshApprox<T_DATA, DIM>
            {
            };

            //==================================================================================================

            template< typename T_DATA >
            class BarycentricApprox<T_DATA, 3> : public MeshApprox<T_DATA, 3>
            {
            public:
                BarycentricApprox(NodeDataProvider<T_DATA>* provider) : MeshApprox<T_DATA, 3>(provider) {}

            protected:
                virtual bool ApproximateCore(Mesh<3>& mesh, int elemIdx, const Point<3>& pt, T_DATA& result);
            };


            template< typename T_DATA >
            class BarycentricApprox<T_DATA, 2> : public MeshApprox<T_DATA, 2>
            {
            public:
                BarycentricApprox(NodeDataProvider<T_DATA>* provider) : MeshApprox<T_DATA, 2>(provider) {}

            protected:
                virtual bool ApproximateCore(Mesh<2>& mesh, int elemIdx, const Point<2>& pt, T_DATA& result);
            };



            template< typename T_DATA >
            bool BarycentricApprox<T_DATA, 3>::ApproximateCore(Mesh<3>& mesh, int elemIdx, const Point<3>& pt, T_DATA& result)
            {
                MeshElement<3>& elem = mesh.elements_[elemIdx];
                if(elem.vert_indices_.size() != 4)
                {
                    lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "BarycentricApprox<3> called for not a tetrahedron.");
                    return false;
                }

                double l[4];
                if(!barycentric3D(pt,
                    mesh.vertices_[elem.vert_indices_[0]],
                    mesh.vertices_[elem.vert_indices_[1]],
                    mesh.vertices_[elem.vert_indices_[2]],
                    mesh.vertices_[elem.vert_indices_[3]],
                    l[0], l[1], l[2], l[3]))
                {
                    //error!
                    return false;
                }


                T_DATA d[4];

                for(int j = 0; j < 4; j++)
                {
                    d[j] = *provider_->GetValue(mesh.GetVertexId(elem.vert_indices_[j]));
                }

                CopyVisitor<T_DATA>::VisitList(&result, &d[0]); //result = d[0]
                MultVisitor<T_DATA>::VisitList(&result, l[0]);  //result *= l[0]

                for(int j = 1; j < 4; j++)
                {
                    T_DATA a;
                    CopyVisitor<T_DATA>::VisitList(&a, &d[j]);  //a = d[j]
                    MultVisitor<T_DATA>::VisitList(&a, l[j]);   //a *= l[j]
                    AddVisitor<T_DATA>::VisitList(&result, &a); //result += a
                }

                return true;
            }

            //==============================================================================

            template< typename T_DATA >
            bool BarycentricApprox<T_DATA, 2>::ApproximateCore(Mesh<2>& mesh, int elemIdx, const Point<2>& pt, T_DATA& result)
            {
                MeshElement<2>& elem = mesh.elements_[elemIdx];
                if(elem.vert_indices_.size() != 3)
                {
                    lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "BarycentricApprox<2> called for not a triangle.");
                    return false;
                }

                double l[3];
                if(!barycentric2D(pt,
                    mesh.vertices_[elem.vert_indices_[0]],
                    mesh.vertices_[elem.vert_indices_[1]],
                    mesh.vertices_[elem.vert_indices_[2]],
                    l[0], l[1], l[2]))
                {
                    //error!
                    return false;
                }

                T_DATA d[3];

                for(int j = 0; j < 3; j++)
                {
                    d[j] = *provider_->GetValue(mesh.GetVertexId(elem.vert_indices_[j]));
                }

                CopyVisitor<T_DATA>::VisitList(&result, &d[0]);  //result = d[0]
                MultVisitor<T_DATA>::VisitList(&result, l[0]);   //result *= l[0]

                for(int j = 1; j < 3; j++)
                {
                    T_DATA a;
                    CopyVisitor<T_DATA>::VisitList(&a, &d[j]);   //a = d[j]
                    MultVisitor<T_DATA>::VisitList(&a, l[j]);    //a *= l[j]
                    AddVisitor<T_DATA>::VisitList(&result, &a);  //result += a
                }

                return true;
            }

            //=================================================================================================

            bool barycentric3D(
                const Point<3>& p, 
                const Point<3>& v1,
                const Point<3>& v2,
                const Point<3>& v3,
                const Point<3>& v4,
                double& l1,
                double& l2,
                double& l3,
                double& l4 );

            bool barycentric2D(
                const Point<2>& p, 
                const Point<2>& v1,
                const Point<2>& v2,
                const Point<2>& v3,
                double& l1,
                double& l2,
                double& l3 );

        }
    }
}

#endif //barycentricApprox_h__
