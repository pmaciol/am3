/*! \file sphericalBarycentricApprox.cpp ***************************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief Approximation based on Generalized Spherical Mean-Value Barycentric Coordinates.
* \details   	
********************************************************************************************/

/*
* Based on:
* [1] Torsten Langer, "On Generalized Barycentric Coordinates and Their Applications in Geometric Modeling"
* [2] Tao Ju, Scott Schaefer, Joe Warren, "Mean Value Coordinates for Closed Triangular Meshes"
*/

#include "sphericalBarycentricApprox.h"
#include <cfloat> //DBL_EPSILON
#include <math.h> //fabs

namespace am3
{
    namespace math
    {
        namespace mesh
        {
            bool sphericalBarycentric3D(Mesh<3>& mesh, int elemIdx, const Point<3>& pt, std::vector<double>& lambda3D)
            {
                const double EPSILON = 1000*DBL_EPSILON;

                MeshElement<3>& elem = mesh.elements_[elemIdx];
                lambda3D.resize(elem.vert_indices_.size());

                std::vector<double> d(elem.vert_indices_.size());
                std::vector< Point<3> > u(elem.vert_indices_.size());

                std::vector< Point<3> > vf(elem.faces_.size());
                std::vector<double> vf_len(elem.faces_.size());

                //
                // Compute vf (from formula 4.1)
                //
                for(size_t j = 0; j < elem.faces_.size(); j++)
                {
                    vf[j] = Point<3>(0, 0, 0);
                    MeshElementFace<3>& f = elem.faces_[j];

                    for(size_t i = 0; i < f.vert_indices_.size(); i++)
                    {
                        Point<3> uu = mesh.vertices_[f.vert_indices_[i]] - pt;
                        d[i] = length(uu);
                        if(d[i] < EPSILON)
                        {
                            //Special case: point almost equals a vertex

                            for(size_t k = 0; k < elem.vert_indices_.size(); k++)
                                lambda3D[k] = 0;

                            lambda3D[i] = 1;
                            return true;
                        }

                        u[i] = uu*(1/d[i]);
                    }

                    for(size_t i = 0; i < f.vert_indices_.size(); i++)
                    {
                        Point<3> ui = u[i];
                        Point<3> ui_1 = u[(i+1) % f.vert_indices_.size()];
                        double l = length(ui - ui_1);
                        double theta = 2*asin(l/2);
                        Point<3> n = cross_product(ui, ui_1);
                        Point<3> dif = n*(theta/2);
                        vf[j] = vf[j] + dif;
                    }

                    vf_len[j] = length(vf[j]);
                }

/*
                ///////////////////////////////////////
                //test 1
                //
                Point<3> sum1(0, 0, 0);
                for(int j = 0; j < elem.faces_.size(); j++)
                {
                    sum1 = sum1 + vf[j];
                }
                // sum1 == (0,0,0) ?
                ///////////////////////////////////////
*/

                //===============================================================

                std::vector<double> w(elem.vert_indices_.size());
                std::vector<double> lambda(elem.vert_indices_.size());
                std::vector<double> tan_half_alpha(elem.vert_indices_.size());
                std::vector<double> sin_theta(elem.vert_indices_.size());
                std::vector<double> cot_theta(elem.vert_indices_.size());
                std::vector< Point<3> > v(elem.vert_indices_.size());
                std::vector< Point<3> > v1(elem.vert_indices_.size());

                for(size_t k = 0; k < elem.faces_.size(); k++)
                {
                    MeshElementFace<3>& f = elem.faces_[k];

                    for(size_t i = 0; i < f.vert_indices_.size(); i++)
                    {
                        v[i] = mesh.vertices_[f.vert_indices_[i]] - pt;
                        v1[i] = v[i] * (1/length(v[i]));
                    }

                    for(size_t i = 0; i < f.vert_indices_.size(); i++)
                    {
                        int ii = (i+1) % f.vert_indices_.size();

                        Point<3> vf_x_vi = cross_product(vf[k], v[i]);
                        vf_x_vi = vf_x_vi * (1/length(vf_x_vi));

                        Point<3> vf_x_vii = cross_product(vf[k], v[ii]);
                        vf_x_vii = vf_x_vii * (1/length(vf_x_vii));

                        Point<3> half = vf_x_vi + (vf_x_vii - vf_x_vi)*0.5;
                        double len_half = length(half);

                        bool close_to_the_edge = false;
                        if(len_half < EPSILON)
                        {
                            close_to_the_edge = true;
                        }
                        else
                        {
                            half = half * (1/len_half);
                            double cos_half_alpha = dot_product(vf_x_vi, half);
                            if(cos_half_alpha < EPSILON)
                            {
                                close_to_the_edge = true;
                            }
                            else
                            {
                                tan_half_alpha[i] = length(cross_product(vf_x_vi, half)) / cos_half_alpha;
                            }
                        }

                        if(close_to_the_edge)
                        {
                            //Special case: point is very close to an edge

                            double l1 = length(v[i]);
                            double l2 = length(v[ii]);
                            double s = l1+l2;
                            l1 /= s;
                            l2 /= s;

                            for(size_t j = 0; j < elem.vert_indices_.size(); j++)
                            {
                                if(elem.vert_indices_[j] == f.vert_indices_[i])
                                    lambda3D[j] = l2;
                                else if(elem.vert_indices_[j] == f.vert_indices_[ii])
                                    lambda3D[j] = l1;
                                else
                                    lambda3D[j] = 0;
                            }
                            return true;
                        }

                        //---------------------------------------
                        Point<3> vf1 = vf[k] * (1/length(vf[k]));
                        sin_theta[i] = length(cross_product(vf1, v1[i]));
                        double cos_theta_i = dot_product(vf1, v1[i]);
                        cot_theta[i] = cos_theta_i / sin_theta[i];
                    }

                    double sigma = 0;
                    for(size_t j = 0; j < f.vert_indices_.size(); j++)
                    {
                        int pred_j = (j + f.vert_indices_.size() - 1) % f.vert_indices_.size();
                        sigma += cot_theta[j] * (tan_half_alpha[pred_j] + tan_half_alpha[j]);
                    }

                    for(size_t i = 0; i < f.vert_indices_.size(); i++)
                    {
                        int pred_i = (i + f.vert_indices_.size() - 1) % f.vert_indices_.size();

                        //lambda from formula 4.2
                        lambda[i] = (vf_len[k] / length(mesh.vertices_[f.vert_indices_[i]] - pt))
                            * ((tan_half_alpha[pred_i] + tan_half_alpha[i]) / (sin_theta[i] * sigma));
                    }

                    for(size_t i = 0; i < f.vert_indices_.size(); i++)
                    {
                        int vidx = f.vert_indices_[i];
                        for(size_t j = 0; j < elem.vert_indices_.size(); j++)
                        {
                            if(elem.vert_indices_[j] == vidx)
                            {
                                w[j] += lambda[i];
                                break;
                            }
                        }
                    }
                }

                double sum_w = 0;
                for(size_t j = 0; j < elem.vert_indices_.size(); j++)
                    sum_w += w[j];

                for(size_t i = 0; i < elem.vert_indices_.size(); i++)
                    lambda3D[i] = w[i]/sum_w;

                /*
                ///////////////////////////////////////
                    //test2
                    //
                    Point<3> pp(0, 0, 0);
                    for(int j = 0; j < elem.vert_indices_.size(); j++)
                    {
                        pp = pp + mesh.vertices_[elem.vert_indices_[j]]*lambda3D[j];
                    }
                    // pp == pt ?
                ///////////////////////////////////////
                */
                return true;
            }

            //==========================================================================================================================
            //==========================================================================================================================

            bool sphericalBarycentric2D(Mesh<2>& mesh, int elemIdx, const Point<2>& pt, std::vector<double>& lambda)
            {
                const double EPSILON = 1000*DBL_EPSILON;

                MeshElement<2>& elem = mesh.elements_[elemIdx];

                std::vector< Point<2> > u(elem.vert_indices_.size());
                std::vector<double> u_invlen(elem.vert_indices_.size());
                std::vector< Point<2> > ve(elem.vert_indices_.size());
                std::vector<double> w(elem.vert_indices_.size());
                std::vector<double> tan_half_alpha(elem.vert_indices_.size());

                lambda.resize(elem.vert_indices_.size());

                //
                // Compute u[i], u_invlen[i]
                //
                for(size_t i = 0; i < elem.vert_indices_.size(); i++)
                {
                    u[i] = mesh.vertices_[elem.vert_indices_[i]] - pt;
                    double len = length(u[i]);
                    if(len < EPSILON)
                    {
                        //Special case: point almost equals a vertex
                        for(size_t k = 0; k < elem.vert_indices_.size(); k++)
                            lambda[k] = 0;
                        lambda[i] = 1;
                        return true;
                    }

                    u_invlen[i] = 1/len;
                }

                //
                // Compute tan_half_alpha[i]
                //
                for(size_t i = 0; i < elem.vert_indices_.size(); i++)
                {
                    int next_i = (i+1) % elem.vert_indices_.size();
                    Point<2> v1 = u[i]*u_invlen[i];
                    Point<2> v2 = u[next_i]*u_invlen[next_i];
                    Point<2> vh = v1 + (v2-v1)*0.5;
                    vh = vh * (1/length(vh));
                    double sin_half_alpha = fabs(v1.x*vh.y - vh.x*v1.y);
                    tan_half_alpha[i] = sin_half_alpha / dot_product(v1, vh);
                }

                //
                // Compute ve (from formula 2.6)
                //
                for(size_t i = 0; i < elem.vert_indices_.size(); i++)
                {
                    int next_i = (i+1) % elem.vert_indices_.size();
                    ve[i] = (u[i]*u_invlen[i] + u[next_i]*u_invlen[next_i]) * tan_half_alpha[i];
                }

                //
                // Compute w[i] (from formula 2.7)
                //
                double sum_w = 0;
                for(size_t i = 0; i < elem.vert_indices_.size(); i++)
                {
                    int pred_i = (i + elem.vert_indices_.size() - 1) % elem.vert_indices_.size();
                    w[i] = (tan_half_alpha[pred_i] + tan_half_alpha[i])*u_invlen[i];
                    sum_w += w[i];
                }

                //
                // Compute lambda[i] (from formula 2.5)
                //
                for(size_t i = 0; i < elem.vert_indices_.size(); i++)
                {
                    lambda[i] = w[i]/sum_w;
                }

                /*
                    ///////////////////////////////////////
                    //test
                    //
                    Point<2> pp(0, 0);
                    for(int j = 0; j < elem.vert_indices_.size(); j++)
                    {
                        pp = pp + mesh.vertices_[elem.vert_indices_[j]]*lambda[j];
                    }
                    // pp == pt ?
                    ///////////////////////////////////////
                */

                //===================================================================
                return true;
            }

        } //mesh
    } //math
} //am3
