/*! \file barycentricApprox.cpp ***************************************
* \author Grzegorz Wiaderek
* \copyright GNU Public License.
* \brief Approximation based on Barycentric Coordinate System.
* \details   	
*********************************************************************/

/* Based on "Barycentric coordinate system" from Wikipedia.
*/

#include "barycentricApprox.h"

namespace am3
{
    namespace math
    {
        namespace mesh
        {

            //==============================================================================

            bool barycentric3D(
                const Point<3>& p, 
                const Point<3>& v1,
                const Point<3>& v2,
                const Point<3>& v3,
                const Point<3>& v4,
                double& l1,
                double& l2,
                double& l3,
                double& l4 )
            {
                double x = p.x;
                double y = p.y;
                double z = p.z;

                double x1 = v1.x;
                double y1 = v1.y;
                double z1 = v1.z;

                double x2 = v2.x;
                double y2 = v2.y;
                double z2 = v2.z;

                double x3 = v3.x;
                double y3 = v3.y;
                double z3 = v3.z;

                double x4 = v4.x;
                double y4 = v4.y;
                double z4 = v4.z;


                double det, det1, det2, det3;

                det = (x1-x4)*(y2-y4)*(z3-z4) + (x3-x4)*(y1-y4)*(z2-z4) + (x2-x4)*(y3-y4)*(z1-z4) - 
                    (x3-x4)*(y2-y4)*(z1-z4) - (x2-x4)*(y1-y4)*(z3-z4) - (x1-x4)*(y3-y4)*(z2-z4);
                det1 = (x-x4)*(y2-y4)*(z3-z4) + (x2-x4)*(y3-y4)*(z-z4) + (x3-x4)*(y-y4)*(z2-z4) - 
                    (x3-x4)*(y2-y4)*(z-z4) - (x2-x4)*(y-y4)*(z3-z4) - (x-x4)*(y3-y4)*(z2-z4);
                det2 = (x1-x4)*(y-y4)*(z3-z4) + (x-x4)*(y3-y4)*(z1-z4) + (x3-x4)*(y1-y4)*(z-z4) - 
                    (x3-x4)*(y-y4)*(z1-z4) - (x-x4)*(y1-y4)*(z3-z4) - (x1-x4)*(y3-y4)*(z-z4);
                det3 = (x1-x4)*(y2-y4)*(z-z4) + (x2-x4)*(y-y4)*(z1-z4) + (x-x4)*(y1-y4)*(z2-z4) - 
                    (x-x4)*(y2-y4)*(z1-z4) - (x2-x4)*(y1-y4)*(z-z4) - (x1-x4)*(y-y4)*(z2-z4);

                if( det != 0 )
                {
                    l1 = det1/det;
                    l2 = det2/det;
                    l3 = det3/det;
                    l4 = 1-l1-l2-l3;
                    return true;
                }

                return false;
            }

            //================================================================================================

            bool barycentric2D(
                const Point<2>& p, 
                const Point<2>& v1,
                const Point<2>& v2,
                const Point<2>& v3,
                double& l1,
                double& l2,
                double& l3 )
            {
                double x = p.x;
                double y = p.y;

                double x1 = v1.x;
                double y1 = v1.y;

                double x2 = v2.x;
                double y2 = v2.y;

                double x3 = v3.x;
                double y3 = v3.y;

                double det = (y2-y3)*(x1-x3)+(x3-x2)*(y1-y3);

                if( det != 0 )
                {
                    l1 = ((y2-y3)*(x-x3)+(x3-x2)*(y-y3))/det;
                    l2 = ((y3-y1)*(x-x3)+(x1-x3)*(y-y3))/det;
                    l3 = 1-l1-l2;
                    return true;
                }

                return false;
            }
        } //mesh
    } //math
} //am3
