/********************************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#include "logger.h"

#include <cstring>
using namespace std;
namespace am3
{
	namespace logger
	{

    const char* kNotImplemented = "Not yet implemented";
    int kModelDepth = 0;

    void NotImplemented( const char* whatIsNotImplemented )
    {
      char  msg[kMaxMsgLength];
      strcpy(msg,"");
      for (int i=0;i<kModelDepth;i++)
      {
        strcat(msg,"  ");
      }
      strcat(msg,am3::logger::kNotImplemented);
      strcat(msg,"\t");
      strcat(msg,whatIsNotImplemented);
      am3::logger::log<am3::logger::SEV_WARNING>(am3::logger::LOG_TESTS,msg);
    }


    void Starting( const char* what )
    {
      char  msg[kMaxMsgLength];
      strcpy(msg,"");
      for (int i=0;i<kModelDepth;i++)
      {
        strcat(msg,"  ");
      }
      strcat(msg,"Starting: ");
      strcat(msg,what);
      am3::logger::log<am3::logger::SEV_INFORMATIONAL>(am3::logger::LOG_TESTS,msg);  
      ++kModelDepth;
    }


    void Stopping( const char* what )
    {
      --kModelDepth; 
      char  msg[kMaxMsgLength];
      strcpy(msg,"");
      for (int i=0;i<kModelDepth;i++)
      {
        strcat(msg,"  ");
      }
      strcat(msg,"Stopping: ");
      strcat(msg,what);
      am3::logger::log<am3::logger::SEV_INFORMATIONAL>(am3::logger::LOG_TESTS,msg);
    }


    void TestingMsg( const char* what )
    {
      char  msg[kMaxMsgLength];
      strcpy(msg,"");
      for (int i=0;i<kModelDepth;i++)
      {
        strcat(msg,"  ");
      };
      strcat(msg,what);
      am3::logger::log<am3::logger::SEV_INFORMATIONAL>(am3::logger::LOG_TESTS,msg);
    }
  } //logger
} //am3