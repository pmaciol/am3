/*! \file logger.h **************************************************
 *  \author		Piotr Maciol
 *  \copyright 	GNU Public License.
 *  \brief       Header for logger
 *  \details   	
*********************************************************************/

#ifndef logger_h__
#define logger_h__

#if defined(_WIN32) || defined(_WIN64)
#pragma warning( disable : 4996)
#include <Windows.h>
#endif

/*	include *********************************************************/

#include <iostream>

/*	using ***********************************************************/

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
  namespace logger
  {
	///The length of chars tables in logging functions
	const int kMaxMsgLength=250;
//   
//   extern const char* kNotImplemented;
	
  ///Used to calculate indentations for logging messages printing 
//	extern int kModelDepth;

  // Numeric codes for sources of logged messages
	const int LOG_MANAGER = 1;
	const int LOG_ADAPTER = 2;
	const int LOG_INFERENCE = 3;
	const int LOG_LOGGER = 4;
	const int LOG_COMMON = 5;
	const int LOG_MODEL = 6;
	const int LOG_ALL = 10;
	const int LOG_TESTS = 20;

  // Numeric codes severity levels of logs
  const int SEV_EMERGENCY = 0;
  const int SEV_ALERT = 1;
  const int SEV_CRITICAL = 2;
  const int SEV_ERROR = 3;
  const int SEV_WARNING = 4;
  const int SEV_NOTICE = 5;
  const int SEV_INFORMATIONAL = 6;
  const int SEV_DEBUG = 7;

  // Present settings of severity levels for logs sources
  const int LOG_MANAGER_SEV=SEV_INFORMATIONAL;
  const int LOG_ADAPTER_SEV=SEV_INFORMATIONAL;
  const int LOG_INFERENCE_SEV=SEV_INFORMATIONAL;
  const int LOG_LOGGER_SEV=SEV_INFORMATIONAL;
  const int LOG_COMMON_SEV=SEV_INFORMATIONAL;
  const int LOG_MODEL_SEV=SEV_INFORMATIONAL;
  const int LOG_ALL_SEV=SEV_INFORMATIONAL;
  const int LOG_DEFAULT_SEV=SEV_INFORMATIONAL;
  const int LOG_TESTS_SEV=SEV_INFORMATIONAL;

/*    extern bool initialized;*/
  // Global functions for logging
  template<int LEVEL>
  inline void log(const int loggingStream,const char* textToLog);
  void NotImplemented(const char* whatIsNotImplemented);
  void Starting(const char* what);
  void Stopping(const char* what);
  void TestingMsg(const char* what);

////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////

  // TODO: Severity level is ignored; fix it just after introducing new logging framework
  
#ifdef _WIN32
  template<int LEVEL>
  inline void log(const int loggingStream, std::string textToLog)
  {
    log<LEVEL>(loggingStream, textToLog.c_str());
//     HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
//     if (hStdout == INVALID_HANDLE_VALUE)
//     {
//       std::cout << "Error while getting input handle" << std::endl;
//     }
//     //sets the color to intense red on blue background
// 
//     if (LEVEL < 4) SetConsoleTextAttribute(hStdout, FOREGROUND_RED | BACKGROUND_BLUE | FOREGROUND_INTENSITY);
// 
//     std::cout << "logging stream: " << loggingStream << ";logging level: " << LEVEL << "; message: " << textToLog << std::endl;
// 
//     if (LEVEL < 4) SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
  }


    template<int LEVEL>
    inline void log(const int loggingStream,const char* textToLog)
    {
// #define ANSI_COLOR_RED     "\x1b[31m"
// #define ANSI_COLOR_RESET   "\x1b[0m"

      HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
      if (hStdout == INVALID_HANDLE_VALUE)
      {
        std::cout << "Error while getting input handle" << std::endl;
      }
      //sets the color to intense red on blue background
      
      if (LEVEL <4) SetConsoleTextAttribute(hStdout, FOREGROUND_RED | BACKGROUND_BLUE | FOREGROUND_INTENSITY);

      std::cout << "logging stream: " << loggingStream << ";logging level: " << LEVEL << "; message: " << textToLog << std::endl;

      if (LEVEL <4) SetConsoleTextAttribute(hStdout, FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
      //std::cout << ANSI_COLOR_RED "logging stream: " << loggingStream << ";logging level: " << LEVEL << "; message: " << textToLog << ANSI_COLOR_RESET  << std::endl;
    }
#else
  template<int LEVEL>
  inline void log(const int loggingStream, std::string textToLog)
  {
    std::cout << "logging stream: " << loggingStream << ";logging level: " << LEVEL << "; message: " << textToLog << std::endl;
  }
  
   template<int LEVEL>
    inline void log(const int loggingStream,const char* textToLog)
    {
      std::cout << "logging stream: " << loggingStream << ";logging level: " << LEVEL << "; message: " << textToLog << std::endl;
    }
  
#endif    

    inline void debugLog(std::string textToLog)
    {
      log<SEV_DEBUG>(LOG_MODEL, textToLog);
    }
  }
}

#endif // logger_h__

