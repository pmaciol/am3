#include "problemPlasticAndHeat.h"

#include "../models/point.h"

namespace pah=am3::problem::plasticAndHeat;
namespace pnt=am3::model::point;

// void am3::problem::plasticAndHeat::FineModelsSelector::AddFineInterface( boost::shared_ptr<PlasticAndHeat2DFineContract> fineInterface )
// {
//   fineInterfacesContainer_.push_back(fineInterface);
//   fineInterface_=*(fineInterfacesContainer_.end()-1);
//   isFineInterfaceSet_=true;
// }
// 
// void am3::problem::plasticAndHeat::FineModelsSelector::SetCoarseInterface( boost::shared_ptr<PlasticAndHeat2DCoarseContract> coarseInterface )
// {
//   coarseInterface_=coarseInterface;
//   isCoarseInterfaceSet_=true;
// }
// 
// boost::shared_ptr<pah::PlasticAndHeat2DCoarseContract> am3::problem::plasticAndHeat::FineModelsSelector::GetCoarseInterface()
// {
//   if (!isCoarseInterfaceSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"Coarse interface of FineModelSelector not set");
//   return coarseInterface_;
// }
// 
// boost::shared_ptr<pah::PlasticAndHeat2DFineContract> am3::problem::plasticAndHeat::FineModelsSelector::GetFineInterface( pnt::Point<prop::Coordinates2D>* point )
// {
// 
//   //! \todo KBS should be asked here!!!
//   if (!isFineInterfaceSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"Fine interface of FineModelSelector not set");
//   if (!isI4AdapterSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"I4Adapter interface of FineModelSelector not set");
//   prop::ModelID mId;
//   mId=i4Adapter_->GetState(point,&mId);
//   fineInterface_=fineInterfacesContainer_.at(mId());
//   return fineInterface_;
// }
// 
// // prop::Strain2D am3::problem::plasticAndHeat::FineModelsSelector::GetState(pnt::Point<prop::Coordinates2D>* point,const prop::Strain2D* const typeTrait)
// // {
// //   IsCoarseInterfaceSet();
// //   return GetCoarseInterface()->GetState(point,typeTrait);
// // }
// 
// prop::TotalEffectiveStrain am3::problem::plasticAndHeat::FineModelsSelector::GetState(pnt::Point<prop::Coordinates2D>* point,const prop::TotalEffectiveStrain* const typeTrait)
// {
//   IsCoarseInterfaceSet();
//   return GetCoarseInterface()->GetState(point,typeTrait);
// }
// 
// 
// prop::StrainRate2D am3::problem::plasticAndHeat::FineModelsSelector::GetState( pnt::Point<prop::Coordinates2D>* point,const prop::StrainRate2D* const typeTrait )
// {
//   IsCoarseInterfaceSet();
//   return GetCoarseInterface()->GetState(point,typeTrait);
// }
// 
// prop::Temperature am3::problem::plasticAndHeat::FineModelsSelector::GetState( pnt::Point<prop::Coordinates2D>* point,const prop::Temperature* const typeTrait )
// {
//   IsCoarseInterfaceSet();
//   return GetCoarseInterface()->GetState(point,typeTrait);
// }
// 
// prop::Coordinates2D am3::problem::plasticAndHeat::FineModelsSelector::GetState( pnt::Point<prop::Coordinates2D>* point,const prop::Coordinates2D* const typeTrait )
// {
//   IsCoarseInterfaceSet();
//   return GetCoarseInterface()->GetState(point,typeTrait);
// }
// 
// prop::Time am3::problem::plasticAndHeat::FineModelsSelector::GetState( pnt::Point<prop::Coordinates2D>* point,const prop::Time* const typeTrait )
// {
//   IsCoarseInterfaceSet();
//   return GetCoarseInterface()->GetState(point,typeTrait);
// }
// 
// prop::Stress2D am3::problem::plasticAndHeat::FineModelsSelector::GetState(pnt::Point<prop::Coordinates2D>* point,const prop::Stress2D* const typeTrait)
// {
//   IsFineInterfaceSet();
//   return GetFineInterface(point)->GetState(point,typeTrait);
// }
// 
// bool am3::problem::plasticAndHeat::FineModelsSelector::IsFineInterfaceSet()
// {
//   if (!isFineInterfaceSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"Fine interface of FineModelSelector not set");
//   return isFineInterfaceSet_;
// }
// 
// bool am3::problem::plasticAndHeat::FineModelsSelector::IsCoarseInterfaceSet()
// {
//   if (!isCoarseInterfaceSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"Coarse interface of FineModelSelector not set");
//   return isCoarseInterfaceSet_;
// }
// 
// void am3::problem::plasticAndHeat::FineModelsSelector::SetI4Adapter( boost::shared_ptr<kno::I4Adapter<prop::Coordinates2D> > i4Adapter )
// {
//   i4Adapter_=i4Adapter;
//   isI4AdapterSet_=true;
// }
// 
// bool am3::problem::plasticAndHeat::FineModelsSelector::IsI4AdapterSet()
// {
//   if (!isI4AdapterSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"I4Adapter interface of FineModelSelector not set");
//   return isI4AdapterSet_;
// }
// 
