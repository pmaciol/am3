#include "fineModelSSCurves.h"
#include "../utilMath/interpolation.h"
#include "../models/models.h"
#include "../common/literals.h"

namespace mip=am3::math::interpolation;

//const std::string am3::model::SingleModelAvailable<am3::problem::plasticAndHeat::FineModelSSCurves>::name_ = "aaaaa";

void am3::problem::plasticAndHeat::FineModelSSCurves::SetCoarseModel( boost::shared_ptr<PlasticAndHeat2DCoarseContract> coarseModel )
{
  coarseModel_=coarseModel;
}

prop::YieldStress am3::problem::plasticAndHeat::FineModelSSCurves::GetState( pnt::Point<prop::Coordinates2D> * point,const prop::YieldStress *const stress )
{
  double youngModulus=1e7;
  prop::Temperature temp;
  prop::Stress2D str;
  prop::EffectiveStrainRate strRate;
  prop::TotalEffectiveStrain strain;
  temp=coarseModel_->GetState(point,&temp);
  strain=coarseModel_->GetState(point,&strain);
  strRate=coarseModel_->GetState(point,&strRate);
  mip::Point<3> state(strain(),strRate(),temp());
  prop::YieldStress yieldStress;
  yieldStress(inter_.GetValue(state).value);
  return yieldStress;
}

prop::StressDStrain am3::problem::plasticAndHeat::FineModelSSCurves::GetState( pnt::Point<prop::Coordinates2D> * point,const prop::StressDStrain *const )
{
  prop::StressDStrain stressDstrain;
  prop::Temperature temp;
  prop::EffectiveStrainRate strRate;
  prop::TotalEffectiveStrain strain;
  temp=coarseModel_->GetState(point,&temp);
  strain=coarseModel_->GetState(point,&strain);
  strRate=coarseModel_->GetState(point,&strRate);
  mip::Point<3> state(strain(),strRate(),temp());
  stressDstrain(inter_.GetValue(state).x_derivative);
  return stressDstrain;
}

prop::StressDStrainRate am3::problem::plasticAndHeat::FineModelSSCurves::GetState( pnt::Point<prop::Coordinates2D> * point,const prop::StressDStrainRate *const )
{
  prop::StressDStrainRate stressDStrainRate;
  prop::Temperature temp;
  prop::EffectiveStrainRate strRate;
  prop::TotalEffectiveStrain strain;
  temp=coarseModel_->GetState(point,&temp);
  strain=coarseModel_->GetState(point,&strain);
  strRate=coarseModel_->GetState(point,&strRate);
  mip::Point<3> state(strain(),strRate(),temp());
  stressDStrainRate(inter_.GetValue(state).y_derivative);
  return stressDStrainRate;
}

am3::problem::plasticAndHeat::FineModelSSCurves::FineModelSSCurves():mod::SingleModelAvailable<FineModelSSCurves>(am3::kTUGSsc)
{
  inter_.SetSize(6,5,8);
  std::vector<std::vector<std::vector<double> > > A;
  A.resize(6);
  for (std::vector<std::vector<std::vector<double> > >::iterator it=A.begin();it<A.end();it++)
  {
    it->resize(5);
    for (std::vector<std::vector<double> >::iterator it2=it->begin();it2<it->end();it2++)
    {
      it2->resize(8);
    }
  }
  A[0][0][0]=1.70E+02;
  A[1][0][0]=1.75E+02;
  A[2][0][0]=1.79E+02;
  A[3][0][0]=1.83E+02;
  A[4][0][0]=1.84E+02;
  A[5][0][0]=1.85E+02;
  A[0][1][0]=1.71E+02;
  A[1][1][0]=1.76E+02;
  A[2][1][0]=1.80E+02;
  A[3][1][0]=1.84E+02;
  A[4][1][0]=1.85E+02;
  A[5][1][0]=1.86E+02;
  A[0][2][0]=1.72E+02;
  A[1][2][0]=1.77E+02;
  A[2][2][0]=1.81E+02;
  A[3][2][0]=1.85E+02;
  A[4][2][0]=1.86E+02;
  A[5][2][0]=1.87E+02;
  A[0][3][0]=1.73E+02;
  A[1][3][0]=1.78E+02;
  A[2][3][0]=1.82E+02;
  A[3][3][0]=1.86E+02;
  A[4][3][0]=1.87E+02;
  A[5][3][0]=1.88E+02;
  A[0][4][0]=1.74E+02;
  A[1][4][0]=1.79E+02;
  A[2][4][0]=1.83E+02;
  A[3][4][0]=1.87E+02;
  A[4][4][0]=1.88E+02;
  A[5][4][0]=1.89E+02;
  A[0][0][1]=1.88E+01;
  A[1][0][1]=2.04E+01;
  A[2][0][1]=2.07E+01;
  A[3][0][1]=2.03E+01;
  A[4][0][1]=1.83E+01;
  A[5][0][1]=1.69E+01;
  A[0][1][1]=3.80E+01;
  A[1][1][1]=3.94E+01;
  A[2][1][1]=4.08E+01;
  A[3][1][1]=4.23E+01;
  A[4][1][1]=4.30E+01;
  A[5][1][1]=4.25E+01;
  A[0][2][1]=4.50E+01;
  A[1][2][1]=4.62E+01;
  A[2][2][1]=4.74E+01;
  A[3][2][1]=4.80E+01;
  A[4][2][1]=4.71E+01;
  A[5][2][1]=4.65E+01;
  A[0][3][1]=5.22E+01;
  A[1][3][1]=5.41E+01;
  A[2][3][1]=5.64E+01;
  A[3][3][1]=5.80E+01;
  A[4][3][1]=5.51E+01;
  A[5][3][1]=5.30E+01;
  A[0][4][1]=5.97E+01;
  A[1][4][1]=6.14E+01;
  A[2][4][1]=6.44E+01;
  A[3][4][1]=6.69E+01;
  A[4][4][1]=6.48E+01;
  A[5][4][1]=6.35E+01;
  A[0][0][2]=9.83E+00;
  A[1][0][2]=1.05E+01;
  A[2][0][2]=1.09E+01;
  A[3][0][2]=1.11E+01;
  A[4][0][2]=1.03E+01;
  A[5][0][2]=1.02E+01;
  A[0][1][2]=2.46E+01;
  A[1][1][2]=2.58E+01;
  A[2][1][2]=2.86E+01;
  A[3][1][2]=3.05E+01;
  A[4][1][2]=3.18E+01;
  A[5][1][2]=3.18E+01;
  A[0][2][2]=3.65E+01;
  A[1][2][2]=3.89E+01;
  A[2][2][2]=4.17E+01;
  A[3][2][2]=4.35E+01;
  A[4][2][2]=4.50E+01;
  A[5][2][2]=4.64E+01;
  A[0][3][2]=4.43E+01;
  A[1][3][2]=4.72E+01;
  A[2][3][2]=5.07E+01;
  A[3][3][2]=5.27E+01;
  A[4][3][2]=5.07E+01;
  A[5][3][2]=4.96E+01;
  A[0][4][2]=5.03E+01;
  A[1][4][2]=5.28E+01;
  A[2][4][2]=5.54E+01;
  A[3][4][2]=5.37E+01;
  A[4][4][2]=5.20E+01;
  A[5][4][2]=5.10E+01;
  A[0][0][3]=4.66E+00;
  A[1][0][3]=5.18E+00;
  A[2][0][3]=5.70E+00;
  A[3][0][3]=6.34E+00;
  A[4][0][3]=6.56E+00;
  A[5][0][3]=6.39E+00;
  A[0][1][3]=1.44E+01;
  A[1][1][3]=1.51E+01;
  A[2][1][3]=1.66E+01;
  A[3][1][3]=1.81E+01;
  A[4][1][3]=2.13E+01;
  A[5][1][3]=2.17E+01;
  A[0][2][3]=2.58E+01;
  A[1][2][3]=2.67E+01;
  A[2][2][3]=2.81E+01;
  A[3][2][3]=3.02E+01;
  A[4][2][3]=3.17E+01;
  A[5][2][3]=3.33E+01;
  A[0][3][3]=3.91E+01;
  A[1][3][3]=4.23E+01;
  A[2][3][3]=4.50E+01;
  A[3][3][3]=4.65E+01;
  A[4][3][3]=4.64E+01;
  A[5][3][3]=4.52E+01;
  A[0][4][3]=4.30E+01;
  A[1][4][3]=4.49E+01;
  A[2][4][3]=4.69E+01;
  A[3][4][3]=5.07E+01;
  A[4][4][3]=4.99E+01;
  A[5][4][3]=4.86E+01;
  A[0][0][4]=2.59E+00;
  A[1][0][4]=2.93E+00;
  A[2][0][4]=3.28E+00;
  A[3][0][4]=3.55E+00;
  A[4][0][4]=3.62E+00;
  A[5][0][4]=3.62E+00;
  A[0][1][4]=1.12E+01;
  A[1][1][4]=1.16E+01;
  A[2][1][4]=1.21E+01;
  A[3][1][4]=1.30E+01;
  A[4][1][4]=1.45E+01;
  A[5][1][4]=1.49E+01;
  A[0][2][4]=2.23E+01;
  A[1][2][4]=2.32E+01;
  A[2][2][4]=2.45E+01;
  A[3][2][4]=2.62E+01;
  A[4][2][4]=3.05E+01;
  A[5][2][4]=3.10E+01;
  A[0][3][4]=2.83E+01;
  A[1][3][4]=2.93E+01;
  A[2][3][4]=3.04E+01;
  A[3][3][4]=3.10E+01;
  A[4][3][4]=3.16E+01;
  A[5][3][4]=3.20E+01;
  A[0][4][4]=4.00E+01;
  A[1][4][4]=4.11E+01;
  A[2][4][4]=4.26E+01;
  A[3][4][4]=4.45E+01;
  A[4][4][4]=4.52E+01;
  A[5][4][4]=4.47E+01;
  A[0][0][5]=1.55E+00;
  A[1][0][5]=1.78E+00;
  A[2][0][5]=1.88E+00;
  A[3][0][5]=2.07E+00;
  A[4][0][5]=2.16E+00;
  A[5][0][5]=2.16E+00;
  A[0][1][5]=7.82E+00;
  A[1][1][5]=7.84E+00;
  A[2][1][5]=7.84E+00;
  A[3][1][5]=7.84E+00;
  A[4][1][5]=7.84E+00;
  A[5][1][5]=7.84E+00;
  A[0][2][5]=9.66E+00;
  A[1][2][5]=9.81E+00;
  A[2][2][5]=9.51E+00;
  A[3][2][5]=9.35E+00;
  A[4][2][5]=1.01E+01;
  A[5][2][5]=1.09E+01;
  A[0][3][5]=1.91E+01;
  A[1][3][5]=2.00E+01;
  A[2][3][5]=2.07E+01;
  A[3][3][5]=2.10E+01;
  A[4][3][5]=2.12E+01;
  A[5][3][5]=2.12E+01;
  A[0][4][5]=2.73E+01;
  A[1][4][5]=2.77E+01;
  A[2][4][5]=2.83E+01;
  A[3][4][5]=2.90E+01;
  A[4][4][5]=2.98E+01;
  A[5][4][5]=3.03E+01;
  A[0][0][6]=1.21E+00;
  A[1][0][6]=1.38E+00;
  A[2][0][6]=1.47E+00;
  A[3][0][6]=1.56E+00;
  A[4][0][6]=1.56E+00;
  A[5][0][6]=1.56E+00;
  A[0][1][6]=3.84E+00;
  A[1][1][6]=3.91E+00;
  A[2][1][6]=3.93E+00;
  A[3][1][6]=3.93E+00;
  A[4][1][6]=3.93E+00;
  A[5][1][6]=3.93E+00;
  A[0][2][6]=6.79E+00;
  A[1][2][6]=6.80E+00;
  A[2][2][6]=6.80E+00;
  A[3][2][6]=6.80E+00;
  A[4][2][6]=6.80E+00;
  A[5][2][6]=6.80E+00;
  A[0][3][6]=1.10E+01;
  A[1][3][6]=1.13E+01;
  A[2][3][6]=1.16E+01;
  A[3][3][6]=1.19E+01;
  A[4][3][6]=1.19E+01;
  A[5][3][6]=1.19E+01;
  A[0][4][6]=1.90E+01;
  A[1][4][6]=1.92E+01;
  A[2][4][6]=1.92E+01;
  A[3][4][6]=1.94E+01;
  A[4][4][6]=2.00E+01;
  A[5][4][6]=2.00E+01;
  A[0][0][7]=7.69E-01;
  A[1][0][7]=9.09E-01;
  A[2][0][7]=1.04E+00;
  A[3][0][7]=1.21E+00;
  A[4][0][7]=1.04E+00;
  A[5][0][7]=1.04E+00;
  A[0][1][7]=2.87E+00;
  A[1][1][7]=2.94E+00;
  A[2][1][7]=3.02E+00;
  A[3][1][7]=3.02E+00;
  A[4][1][7]=3.02E+00;
  A[5][1][7]=3.02E+00;
  A[0][2][7]=4.53E+00;
  A[1][2][7]=4.64E+00;
  A[2][2][7]=4.89E+00;
  A[3][2][7]=5.44E+00;
  A[4][2][7]=5.95E+00;
  A[5][2][7]=6.04E+00;
  A[0][3][7]=9.20E+00;
  A[1][3][7]=9.78E+00;
  A[2][3][7]=1.01E+01;
  A[3][3][7]=1.05E+01;
  A[4][3][7]=1.07E+01;
  A[5][3][7]=1.07E+01;
  A[0][4][7]=1.45E+01;
  A[1][4][7]=1.42E+01;
  A[2][4][7]=1.45E+01;
  A[3][4][7]=1.53E+01;
  A[4][4][7]=1.62E+01;
  A[5][4][7]=1.70E+01;

  std::vector<double> one,two,three;
  one.push_back(-0.001);
  one.push_back(2.5e-2);
  one.push_back(5e-2);
  one.push_back(1e-1);
  one.push_back(3e-1);
  one.push_back(5e-1);
  //two.push_back(1e-3);
  two.push_back(-0.001);
  two.push_back(1e-2);
  two.push_back(1e-1);
  two.push_back(1);
  two.push_back(1e1);
  //three.push_back(6.8e1);
  three.push_back(-0.001);
  three.push_back(1.292e3);
  three.push_back(1.382e3);
  three.push_back(1.472e3);
  three.push_back(1.562e3);
  three.push_back(1.652e3);
  three.push_back(1.742e3);
  three.push_back(1.832e3);
  inter_.SetGridPoints(one,two,three);
  inter_.SetValues(A);
}

int am3::problem::plasticAndHeat::FineModelSSCurves::Run()
{
	return -1;
}

int am3::problem::plasticAndHeat::FineModelSSCurves::PostProcess()
{
	return -1;
}

void am3::problem::plasticAndHeat::FineModelSSCurves::Configure()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::problem::plasticAndHeat::FineModelSSCurves::PrepareForRun()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::problem::plasticAndHeat::FineModelSSCurves::PrepareEnvironment()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::problem::plasticAndHeat::FineModelSSCurves::PrepareInputFiles()
{
	throw std::exception("The method or operation is not implemented.");
}
