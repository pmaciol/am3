
include_directories (../)
#add_executable(am3problemPlasticAndHeat 
add_library (am3problemPlasticAndHeat 
problemPlasticAndHeat.h
main.cpp
kbsConverter.h kbsConverter.cpp
fineModelSSCurves.h fineModelSSCurves.cpp
fineTugCa.h fineTugCa.cpp
coarseDeform.h coarseDeform.cpp
fineCoarseDumbAdapter.h fineCoarseDumbAdapter.cpp
)

target_link_libraries(am3problemPlasticAndHeat am3models am3common am3knowledgeRebit am3logger am3basicModels)



SET_TARGET_PROPERTIES(am3problemPlasticAndHeat  PROPERTIES LINKER_LANGUAGE CXX)
SET(CMAKE_EXE_LINKER_FLAGS /NODEFAULTLIB:\"LIBCMTD.lib;libcpmtd.lib\")