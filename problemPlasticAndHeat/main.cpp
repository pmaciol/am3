/*! \file main.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief  Example of am3 realization
 * \details All elements, necessary to design simple multiscale model are described here
*********************************************************************/

/*	include *********************************************************/

#include "../common/config.h"
#include "../basicModels/rebitInterface.h"
#include "problemPlasticAndHeat.h"
#include "kbsConverter.h"

/*	definitions *********************************************************/

//@{ \name globals_for_program_options Globals
//! Used by boost::program_options implementation of program options
extern int*  global_argc;
extern char ***global_argv;
//@}


using namespace am3::common;
/*	classes *********************************************************/

// int main(int argc, char **argv)
// {
// 
// //  adi::AdinaFCallsProcessor am3;
// 
//   //Passing argv to progam_options
//   global_argc=&argc;
//   global_argv=&argv;
// 
//   //Initialization od program_options, reading config files (check am3.conf in main binary directory)
//   kProgramOptions.Init();
// 
//   am3::knowledge::rebit::MockRebitKBS kbs;
// 
//   //am3::adapter::FineModelSelector<prop::Stress2D,am3::knowledge::rebit::MockRebitKBS,
// 
//   am3::knowledge::plasticAndHeat::PlasticAndHeat2RebitConverter conv;
// 
// 
//     // With example
// //     am3::adapter::adinaSimple::SimpleAdinaAdapterProxy<kno::example1::Example1KBS, kno::example1::Example1GeneralRebit<am3::adapter::adinaSimple::SimpleAdinaAdapter> > 
// //       adinaSimpleProxy(&adinaSimpleAdapter,&ex1Kbs);
// //     // With Rebit
// // //     am3::adapter::adinaSimple::SimpleAdinaAdapterProxy<am3::knowledge::rebit::MockRebitKBS, kno::example1::Example1GeneralRebit<am3::adapter::adinaSimple::SimpleAdinaAdapter> > 
// // //     adinaSimpleProxy(&adinaSimpleAdapter,&kbs);
// 
// 
// }