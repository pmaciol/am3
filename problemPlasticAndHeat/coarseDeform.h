
/*! \file coarseDeform.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef problemPlasticAndHeat_coarseDeform_h__
#define problemPlasticAndHeat_coarseDeform_h__
/*	include *********************************************************/
#include "../models/dataStructures.h"
#include "../softwareDeform/deformControl.h"
#include "problemPlasticAndHeat.h"

/*	using ***********************************************************/

namespace dfrm=am3::software::deform;
namespace prop=am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace problem
  {
    namespace plasticAndHeat
    {
      class CoarsePlasticAndHeatWithDeform
        //: public PlasticAndHeat2DCoarseContract
        : public dfrm::DeformModel
      {
      public:
//         prop::Strain2D GetState(pnt::Point<prop::Coordinates2D> *point ,const prop::Strain2D * prop  );
//         prop::StrainRate2D GetState(pnt::Point<prop::Coordinates2D> *point ,const prop::StrainRate2D *prop  );
//         prop::Temperature GetState(pnt::Point<prop::Coordinates2D> *point ,const prop::Temperature *prop  );
//         prop::Coordinates2D GetState(pnt::Point<prop::Coordinates2D> *point ,const prop::Coordinates2D *prop  );
//         prop::Time GetState(pnt::Point<prop::Coordinates2D> *point ,const prop::Time *prop  );
      protected:
      };
    } //plasticAndHeat
  } //problem
} //am3
#endif // problemPlasticAndHeat_coarseDeform_h__
