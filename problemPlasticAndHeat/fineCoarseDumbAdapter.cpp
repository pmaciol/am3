#include "fineCoarseDumbAdapter.h"

prop::YieldStress am3::problem::plasticAndHeat::FineCoarseDumbAdapter::GetState( pnt::Point<prop::Coordinates2D> *point,const prop::YieldStress *property )
{
  prop::YieldStress stress;
  //prop::Stress2D stress;
  stress=fineDataProvider_->GetState(point,&stress);
 
  return stress;
}

prop::StressDStrain am3::problem::plasticAndHeat::FineCoarseDumbAdapter::GetState( pnt::Point<prop::Coordinates2D> *point,const prop::StressDStrain *property )
{
  prop::StressDStrain sds;
  //prop::YieldStress stress;
  sds=fineDataProvider_->GetState(point,&sds);
  return sds;
}

prop::StressDStrainRate am3::problem::plasticAndHeat::FineCoarseDumbAdapter::GetState( pnt::Point<prop::Coordinates2D> *point,const prop::StressDStrainRate *property )
{
  prop::StressDStrainRate sdsr;
  sdsr=fineDataProvider_->GetState(point,&sdsr);
  return sdsr;
}
