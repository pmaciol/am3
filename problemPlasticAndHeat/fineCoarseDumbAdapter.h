
/*! \file fineCoarseDumbAdapter.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef problemPlasticAndHeat_fineCoarseDumpAdapter_h__
#define problemPlasticAndHeat_fineCoarseDumpAdapter_h__
/*	include *********************************************************/

#include "../softwareDeform/deformDataInterface.h"
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "problemPlasticAndHeat.h"
#include "../softwareDeform/storage.h"

#include "../models/contracts.h"
/*	using ***********************************************************/

namespace dfr=am3::model::deform;
namespace prop=am3::model::properties;
namespace pnt=am3::model::point;
namespace sds=am3::software::deform::storage;
namespace con=am3::model::contract;

/*	extern **********************************************************/



/*	classes *********************************************************/
namespace am3
{
  namespace problem
  {
    namespace plasticAndHeat
    {

      class FineCoarseDumbAdapter
        : public dfr::DeformMtrNeeds
      {
      public:
        FineCoarseDumbAdapter(boost::shared_ptr<PlasticAndHeat2DFineContract> fineDataProvider):fineDataProvider_(fineDataProvider){}; 
        prop::YieldStress GetState(pnt::Point<prop::Coordinates2D> *point,const prop::YieldStress *prop );
        prop::StressDStrain GetState(pnt::Point<prop::Coordinates2D> *point,const prop::StressDStrain *prop );
        prop::StressDStrainRate GetState(pnt::Point<prop::Coordinates2D> *point,const prop::StressDStrainRate *prop );
      protected:
        boost::shared_ptr<PlasticAndHeat2DFineContract> fineDataProvider_;
      };
    } //plasticAndHeat
  } //problem

} //am3

#endif // problemPlasticAndHeat_fineCoarseDumpAdapter_h__
