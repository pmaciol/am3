
/*! \file kbsConverter.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef pahKbsConverter_h__
#define pahKbsConverter_h__
/*	include *********************************************************/

#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>
#include "../common/literals.h"
#include "../knowledge/kbsInterfaces.h"
#include "../kbsRebit/reasoningData.h"
#include "../knowledgeRebit/rebitKnowledgeStructs.h"
#include "problemPlasticAndHeat.h"

/*	using ***********************************************************/

namespace kno=am3::knowledge;
namespace kre=am3::knowledge::rebit;
namespace prop=am3::model::properties;
namespace pah=am3::problem::plasticAndHeat;
namespace pnt=am3::model::common;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace knowledge
  {
    namespace plasticAndHeat
    {
      class PlasticAndHeat2RebitConverter 
        : public knowledge::I4KBS<VariableDTO>
        , public kno::I4Adapter<prop::Coordinates2D,pro::ModelID>
      {
      public:
//        PlasticAndHeat2RebitConverter(boost::shared_ptr<kre::RebitClient> rebitClient,boost::shared_ptr<pah::PlasticAndHeat2DCoarseContract> fineModels){};
        PlasticAndHeat2RebitConverter(boost::shared_ptr<kre::RebitClient> rebitClient,boost::shared_ptr<pah::PlasticAndHeat2DFineContract> fineModels,boost::shared_ptr<pah::PlasticAndHeat2DCoarseContract> coarseModels,boost::shared_ptr<pah::PlasticAndHeat2DSwitcherContract> switcher);
        virtual bool Fetch(boost::shared_ptr<VariableDTO> property, const VariableDTO &where, const VariableDTO &when);
        virtual bool Fetch(boost::shared_ptr<VariableDTO> property);
        virtual am3::model::ModelID GetState( const com::Point<prop::Coordinates2D> *const point ,const prop::ModelID * );

      protected:
        boost::shared_ptr<pah::PlasticAndHeat2DCoarseContract> coarsePropertiesProvider_;
        boost::shared_ptr<pah::PlasticAndHeat2DFineContract> finePropertiesProvider_;
		boost::shared_ptr<pah::PlasticAndHeat2DSwitcherContract> switcherPropertiesProvider_;
        boost::shared_ptr<kre::RebitClient> rebitClient_;
        boost::shared_ptr<com::ComputedPoint<prop::Coordinates2D> > lastCalledPoint_;

 //       VariableDTO Convert(const pro::Viscosity& visc);

      };

      //////////////////////////////////////////////////////////////////////////

//       VariableDTO PlasticAndHeat2RebitConverter::Convert(const pro::Viscosity& visc)
//       {
//         VariableDTO toReturn;
//         *(toReturn.Id)="viscosity";
//         return toReturn;
//       };


    } //plasticAndHeat
  } //knowledge
} //am3
#endif // pahKbsConverter_h__
