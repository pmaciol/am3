#include "kbsConverter.h"

bool am3::knowledge::plasticAndHeat::PlasticAndHeat2RebitConverter::Fetch( boost::shared_ptr<VariableDTO> property, const VariableDTO &where, const VariableDTO &when )
{
  bool isSuccess=false;
  std::string str1=kStrain2D;
  std::string str2=*(property->Id);
  if (property->Id->compare(kTotalEffStrain)==0)
  {
    prop::TotalEffectiveStrain tes;
    boost::shared_ptr<pnt::Point<prop::Coordinates2D> > point= kre::Point2DFromVariablDTO(where,when);
    pnt::Point<prop::Coordinates2D>* pointPtr=point.get();
    tes=coarsePropertiesProvider_->GetState(pointPtr,&tes);
    if (kre::SetVariableDTO(*property,tes)) isSuccess=true;
    else isSuccess=false;
  }
  else if (property->Id->compare(kStrainRate2D)==0)
  {
    prop::StrainRate2D am3prop;
    boost::shared_ptr<pnt::Point<prop::Coordinates2D> > point= kre::Point2DFromVariablDTO(where,when);
    pnt::Point<prop::Coordinates2D>* pointPtr=point.get();
    am3prop=coarsePropertiesProvider_->GetState(pointPtr,&am3prop);
    if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
    else isSuccess=false;
  } 
  else if (property->Id->compare(kTemperature)==0)
  {
    prop::Temperature am3prop;
    boost::shared_ptr<pnt::Point<prop::Coordinates2D> > point= kre::Point2DFromVariablDTO(where,when);
    pnt::Point<prop::Coordinates2D>* pointPtr=point.get();
    am3prop=coarsePropertiesProvider_->GetState(pointPtr,&am3prop);
    if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
    else isSuccess=false;
  } 
  else if (property->Id->compare(kCoordinates2D)==0)
  {
    prop::Coordinates2D am3prop;
    boost::shared_ptr<pnt::Point<prop::Coordinates2D> > point= kre::Point2DFromVariablDTO(where,when);
    pnt::Point<prop::Coordinates2D>* pointPtr=point.get();
    am3prop=coarsePropertiesProvider_->GetState(pointPtr,&am3prop);
    if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
    else isSuccess=false;
  } 
  else if (property->Id->compare(kTime)==0)
  {
    prop::Time am3prop;
    boost::shared_ptr<pnt::Point<prop::Coordinates2D> > point= kre::Point2DFromVariablDTO(where,when);
    pnt::Point<prop::Coordinates2D>* pointPtr=point.get();
    am3prop=coarsePropertiesProvider_->GetState(pointPtr,&am3prop);
    if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
    else isSuccess=false;
  } 
  else if (property->Id->compare(kYieldStress)==0)
  {
    prop::YieldStress am3prop;
    boost::shared_ptr<pnt::Point<prop::Coordinates2D> > point= kre::Point2DFromVariablDTO(where,when);
    pnt::Point<prop::Coordinates2D>* pointPtr=point.get();
    am3prop=finePropertiesProvider_->GetState(pointPtr,&am3prop);
    if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
    else isSuccess=false;
  } 
  else if (property->Id->compare(kGlobalElementNumber)==0)
  {
    prop::ElementNumberGlobal am3prop;
    boost::shared_ptr<pnt::Point<prop::Coordinates2D> > point= kre::Point2DFromVariablDTO(where,when);
    pnt::Point<prop::Coordinates2D>* pointPtr=point.get();
    am3prop=coarsePropertiesProvider_->GetState(pointPtr,&am3prop);
    if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
    else isSuccess=false;
  } 
  else if (property->Id->compare(kDeltaTemperature)==0)
  {
	  prop::DeltaTemperature am3prop;
	  boost::shared_ptr<pnt::Point<prop::Coordinates2D> > point= kre::Point2DFromVariablDTO(where,when);
	  pnt::Point<prop::Coordinates2D>* pointPtr=point.get();
	  am3prop=switcherPropertiesProvider_->GetState(pointPtr,&am3prop);
	  if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
	  else isSuccess=false;
  }
  return isSuccess;
}

bool am3::knowledge::plasticAndHeat::PlasticAndHeat2RebitConverter::Fetch( boost::shared_ptr<VariableDTO> property )
{
  bool isSuccess=false;


  if (property->Id->compare(kGlobalElementNumber)==0)
  {
    prop::ElementNumberGlobal am3prop;
    am3prop=coarsePropertiesProvider_->GetState(lastCalledPoint_.get(),&am3prop);
    if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
  } 
  else if (property->Id->compare(kTime)==0)
  {
    prop::Time am3prop;
    am3prop=coarsePropertiesProvider_->GetState(lastCalledPoint_.get(),&am3prop);
    if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
  }
  else if (property->Id->compare(kTemperature)==0)
  {
	  prop::Temperature am3prop;
	  am3prop=coarsePropertiesProvider_->GetState(lastCalledPoint_.get(),&am3prop);
	  if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
  }
  else if (property->Id->compare(kDeltaTemperature)==0)
  {
	  prop::DeltaTemperature am3prop;
	  am3prop=switcherPropertiesProvider_->GetState(lastCalledPoint_.get(),&am3prop);
	  if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
	  else isSuccess=false;
  }
  else if (property->Id->compare(kPreviousModelId)==0)
  {
	  prop::ModelID am3prop;
	  am3prop=switcherPropertiesProvider_->WhichModelLastHere(lastCalledPoint_.get());
	  if (kre::SetVariableDTO(*property,am3prop)) isSuccess=true;
	  else isSuccess=false;
  }
  return isSuccess;
}

am3::model::ModelID am3::knowledge::plasticAndHeat::PlasticAndHeat2RebitConverter::GetState( const com::Point<prop::Coordinates2D> * const point,const prop::ModelID * )
{
  prop::ModelID mId;
  VariableDTO mIdVariable;
  //lastCalledPoint_->Set(*point);
  //boost::shared_ptr<com::Point<prop::Coordinates2D> > newPoint(com::Point<prop::Coordinates2D>(point));
  lastCalledPoint_.reset(new com::ComputedPoint<prop::Coordinates2D>(point));
  mIdVariable.Id->assign(am3::kModelId);
  rebitClient_->GetVariableValue(mIdVariable);
  mId(boost::lexical_cast<int>(mIdVariable.Value->at(0)));
  return mId;
}

am3::knowledge::plasticAndHeat::PlasticAndHeat2RebitConverter::PlasticAndHeat2RebitConverter( boost::shared_ptr<kre::RebitClient> rebitClient,boost::shared_ptr<pah::PlasticAndHeat2DFineContract> fineModels,boost::shared_ptr<pah::PlasticAndHeat2DCoarseContract> coarseModels ,boost::shared_ptr<pah::PlasticAndHeat2DSwitcherContract> switcher)
{
  //lastCalledPoint_. reset(new com::ComputedPoint<prop::Coordinates2D>);
  rebitClient_=rebitClient;
  coarsePropertiesProvider_=coarseModels;//->GetCoarseInterface();
  finePropertiesProvider_=fineModels;//->GetFineInterface();
  switcherPropertiesProvider_=switcher;
//makes an SEH error in gTest. TRy this with final version without tests
// in tests initialization is made directly
//   boost::shared_ptr<I4Adapter<prop::Coordinates2D> > i4a(this); 
//   fineModels->SetI4Adapter(i4a);
}
