#include "fineTugCa.h"

#include <winsock.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include "../models/models.h"

//const std::string am3::model::SingleModelAvailable<am3::problem::plasticAndHeat::FineModelTugCA>::name_ = "nnnnnn";

prop::YieldStress am3::problem::plasticAndHeat::FineModelTugCA::GetState( pnt::Point<prop::Coordinates2D> * point,const prop::YieldStress *const stress)
{
	/*boost::shared_ptr<TmpSocket> socketToProceed;*/
	socketToProceed=this->WhichSocketForPoint(point);

  prop::Time time;
  prop::Temperature temp;
  prop::Stress2D str;
  prop::TotalEffectiveStrain strain;
  //time=coarseModel_->GetState(point,&time);
  time=point->GetTime();
  temp=coarseModel_->GetState(point,&temp);
  strain=coarseModel_->GetState(point,&strain);

  double actualTime,ratio1,dm1,ratio2,dm2;


//! \todo There is no feedback from grainSimulator. The same curves as in fineModelSSC should be used here!
  //grainSimulatorConnection_.Process(temp(),time(),actualTime,ratio1,dm1,ratio2,dm2);
  socketToProceed->timeShift=socketToProceed->timeShift<time() ? socketToProceed->timeShift: time();
  if (socketToProceed->Process(temp(),time()-socketToProceed->timeShift,actualTime,ratio1,dm1,ratio2,dm2))
  {
	  std::ofstream myfile;
	  std::string filename="grainOut"+boost::lexical_cast<std::string>(socketToProceed->NPort())+".dat";

	  myfile.open (filename.c_str(), std::ios::out | std::ios::app);
	  myfile <<point->Coordinates().x()<<"\t"<<point->Coordinates().y()<<"\t"<<temp()<<"\t"<<time()<<"\t"<<socketToProceed->timeShift<<"\t"<<actualTime<<"\t"<<ratio1<<"\t"<<dm1<<"\t"<<ratio2<<"\t"<< dm2 << std::endl;
	  myfile.close();
  }


  prop::YieldStress yieldStress;
  yieldStress=feedbackModel.GetState(point,&yieldStress);
  return yieldStress;
}

prop::StressDStrain am3::problem::plasticAndHeat::FineModelTugCA::GetState( pnt::Point<prop::Coordinates2D> * point,const prop::StressDStrain *const stress)
{
  prop::StressDStrain stressDStrain;
  stressDStrain=feedbackModel.GetState(point,&stressDStrain);
  return stressDStrain;
}
prop::StressDStrainRate am3::problem::plasticAndHeat::FineModelTugCA::GetState( pnt::Point<prop::Coordinates2D> * point,const prop::StressDStrainRate *const stress)
{
  prop::StressDStrainRate stressDStrainRate;
  stressDStrainRate=feedbackModel.GetState(point,&stressDStrainRate);
  return stressDStrainRate;
}
void am3::problem::plasticAndHeat::FineModelTugCA::SetCoarseModel( boost::shared_ptr<PlasticAndHeat2DCoarseContract> coarseModel )
{
  coarseModel_=coarseModel;
  feedbackModel.SetCoarseModel(coarseModel_);
}

am3::problem::plasticAndHeat::TmpSocket* am3::problem::plasticAndHeat::FineModelTugCA::WhichSocketForPoint( pnt::Point<prop::Coordinates2D> * point )
{
	TSocketMap::iterator it;
	prop::ElementNumberGlobal elNumer;
	elNumer=coarseModel_->GetState(point,&elNumer);
	it=grainSimulatorConnections_.find(elNumer);
	TmpSocket* socketPointer;
	if (it==grainSimulatorConnections_.end())
	{
		socketPointer=new TmpSocket;
		socketPointer=grainSimulatorConnections_.insert(TSocketPair(elNumer, socketPointer)).first->second;
		std::cout<<"CA model started for x = "<<point->Coordinates().x()<<"\ty = "<<point->Coordinates().y()<<"\t time = "<<point->GetTime()<<std::endl;
	}
	else 
		{
			socketPointer=(it->second);
	}
	return socketPointer;
}

int am3::problem::plasticAndHeat::FineModelTugCA::Run()
{
	return -1;
}

int am3::problem::plasticAndHeat::FineModelTugCA::PostProcess()
{
	return -1;
}

void am3::problem::plasticAndHeat::FineModelTugCA::Configure()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::problem::plasticAndHeat::FineModelTugCA::PrepareForRun()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::problem::plasticAndHeat::FineModelTugCA::PrepareEnvironment()
{
	throw std::exception("The method or operation is not implemented.");
}

void am3::problem::plasticAndHeat::FineModelTugCA::PrepareInputFiles()
{
	throw std::exception("The method or operation is not implemented.");
}



//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
SOCKET am3::problem::plasticAndHeat::TmpSocket::EstablishConnection( char* remoteAddr, u_short nPort )
{
  u_long SE=SOCKET_ERROR;
  u_long AI=AF_INET;
  u_long SS=SOCK_STREAM;
  WSAData wsaData;
  int nCode = WSAStartup(MAKEWORD(1, 1), &wsaData);

  SOCKET sd = socket(AF_INET, SOCK_STREAM, 0);
  std::cout<<"Error code:"<<GetLastError()<<std::endl;
  if (sd != INVALID_SOCKET) {
    
    sockaddr_in sinRemote;
    memset(&sinRemote, '0', sizeof(sinRemote));
    sinRemote.sin_port = htons(nPort); 
    sinRemote.sin_family = AF_INET;

    sinRemote.sin_addr.s_addr = inet_addr(remoteAddr); 
    if (connect(sd, (sockaddr*)&sinRemote, sizeof(sockaddr_in)) ==
      SOCKET_ERROR) {
        sd = INVALID_SOCKET;
    }
  }

  return sd;
}

#define buffer8ToNetBuffer(a,b,ind) {if(ind) {b[7] = a[7]; b[6] = a[6]; b[5] = a[5]; \
  b[4] = a[4]; b[3] = a[3]; b[2] = a[2]; b[1] = a[1]; b[0] = a[0];} \
     else {b[0] = a[7]; b[1] = a[6]; b[2] = a[5]; b[3] = a[4]; \
     b[4] = a[3]; b[5] = a[2]; b[6] = a[1]; b[7] = a[0];}}

double netBufToDouble(char *buf, int endianness) 
{
  double rval;
  char *buf2;
  buf2 = (char*)malloc(8);
  buffer8ToNetBuffer(buf, buf2, endianness);
  rval = *((double*)buf2);
  free(buf2);
  return rval;
}

void doubleToNetBuf(double value, char* buf, int endianness)
{
  char *buf2;
  buf2 = ((char*)&value);
  buffer8ToNetBuffer(buf2, buf, endianness);
}


bool am3::problem::plasticAndHeat::TmpSocket::Process( const double temperature, const double goalTime, double& actualTime, double& ratio1, double& dm1, double& ratio2, double& dm2 )
{
  char* buf1;
  char ok;
  bool isSucces=false;
  int test=1;
  int endianness;
  
  if(*(char *)&test == 1)
  {
    /* little endian */
    endianness = 0;
  }
  else
  {
    /* big endian */
    endianness = 1;
  }

  buf1 = (char*)malloc(8);

  printf("writing... ");
  doubleToNetBuf(temperature, buf1, endianness);
  send(sock_, buf1, 8,0);
  doubleToNetBuf(goalTime, buf1, endianness);
  send(sock_, buf1, 8,0);;
  printf("done.\n");
  printf("reading... ");
  recv(sock_, &ok, 1, 0);
  if (ok==1) isSucces=true;
  recv(sock_, buf1, 8, 0);
  actualTime = netBufToDouble(buf1, endianness);
  recv(sock_, buf1, 8, 0);
  ratio1 = netBufToDouble(buf1, endianness);
  recv(sock_, buf1, 8, 0);
  dm1 = netBufToDouble(buf1, endianness);
  recv(sock_, buf1, 8, 0);
  ratio2 = netBufToDouble(buf1, endianness);
  recv(sock_, buf1, 8, 0);
  dm2 = netBufToDouble(buf1, endianness);
  return isSucces;
// 
}

bool am3::problem::plasticAndHeat::TmpSocket::IsWsaStarted()
{
  if (GetLastError()==0)return true;
  else return false;
}

bool am3::problem::plasticAndHeat::TmpSocket::IsSocketValid()
{
  if (sock_!=INVALID_SOCKET) return true;
  else return false;
}

u_short am3::problem::plasticAndHeat::TmpSocket::lastPortN=am3::problem::plasticAndHeat::TmpSocket::startingPort;
