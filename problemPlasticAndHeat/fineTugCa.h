
/*! \file fineTugCa.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef fineTugCa_h__
#define fineTugCa_h__
/*	include *********************************************************/

#include <winsock.h>
#include <windows.h> 
#include <map>

#include "../models/models.h"
#include "../models/dataStructures.h"
#include "problemPlasticAndHeat.h"
#include "fineModelSSCurves.h"
#include "../common/literals.h"

/*	using ***********************************************************/

namespace mod=am3::model;
namespace prop=am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace problem
  {
    namespace plasticAndHeat
    {

      class TmpSocket
      {
      public:
        TmpSocket()
        {
          address_="129.27.96.156";
          nPort_=lastPortN++;
          sock_=EstablishConnection(address_,nPort_);
		  timeShift=1e10;
        }
        ~TmpSocket()
        {
          closesocket(sock_);
 //         close(sock_);
        }
        bool Process(const double temperature, const double goalTime, double& actualTime, double& ratio1, double& dm1, double& ratio2, double& dm2);
        bool IsWsaStarted();
        bool IsSocketValid();
		u_short NPort(){return nPort_;};
		double timeShift;
      protected:
        SOCKET EstablishConnection(char* remoteAddr, u_short nPort);
        SOCKET sock_;
        char* address_;
        u_short nPort_;
		static u_short lastPortN;
		static const u_short startingPort=40011;
		
      };

      class FineModelTugCA 
        : public PlasticAndHeat2DFineContract
        , public mod::SingleModelAvailable<FineModelTugCA>
//				, public mod::Model
      {
      public:

				FineModelTugCA():mod::SingleModelAvailable<FineModelTugCA>(am3::kTUGCaGrain){};
        virtual prop::YieldStress GetState(pnt::Point<prop::Coordinates2D> *,const prop::YieldStress *const );
        virtual prop::StressDStrain GetState(pnt::Point<prop::Coordinates2D> *,const prop::StressDStrain *const );
        virtual prop::StressDStrainRate GetState(pnt::Point<prop::Coordinates2D> *,const prop::StressDStrainRate *const );
        void SetCoarseModel(boost::shared_ptr<PlasticAndHeat2DCoarseContract> coarseModel);
		
	  protected:
		
		TmpSocket* socketToProceed;

		typedef std::map<prop::ElementNumberGlobal, TmpSocket*> TSocketMap;
		typedef std::pair<prop::ElementNumberGlobal, TmpSocket*> TSocketPair;

        TSocketMap grainSimulatorConnections_;

        boost::shared_ptr<PlasticAndHeat2DCoarseContract> coarseModel_;
        FineModelSSCurves feedbackModel;
		std::vector<prop::Temperature> currentTemperatures;
		TmpSocket* WhichSocketForPoint( pnt::Point<prop::Coordinates2D> * point );;

		virtual int Run();

		virtual int PostProcess();

		virtual void Configure();

		virtual void PrepareForRun();

		virtual void PrepareEnvironment();

		virtual void PrepareInputFiles();

			};
    } //plasticAndHeat
  } //problem
} //am3
#endif // fineTugCa_h__
