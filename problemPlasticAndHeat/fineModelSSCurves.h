
/*! \file fineModelSSCurves.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef fineModelSSCurves_h__
#define fineModelSSCurves_h__
/*	include *********************************************************/

#include "../models/models.h"
#include "../models/dataStructures.h"
#include "problemPlasticAndHeat.h"
#include "../utilMath/interpolation.h"


/*	using ***********************************************************/

namespace mod=am3::model;
namespace prop=am3::model::properties;
namespace mip=am3::math::interpolation;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace problem
  {
    namespace plasticAndHeat
    {
      class FineModelSSCurves 
        : public PlasticAndHeat2DFineContract
        //, public mod::Model
				, public mod::SingleModelAvailable<FineModelSSCurves>
      {
      public:
        FineModelSSCurves();
        inline virtual prop::YieldStress GetState(pnt::Point<prop::Coordinates2D> *,const prop::YieldStress *const );
        inline virtual prop::StressDStrain GetState(pnt::Point<prop::Coordinates2D> *,const prop::StressDStrain *const );
        inline virtual prop::StressDStrainRate  GetState(pnt::Point<prop::Coordinates2D> *,const prop::StressDStrainRate  *const );
        void SetCoarseModel(boost::shared_ptr<PlasticAndHeat2DCoarseContract> coarseModel);

				virtual int Run();

				virtual int PostProcess();

				virtual void Configure();

				virtual void PrepareForRun();

				virtual void PrepareEnvironment();

				virtual void PrepareInputFiles();

				//using mod::SingleModelAvailable<FineModelSSCurves>::Name;
      protected:
        boost::shared_ptr<PlasticAndHeat2DCoarseContract> coarseModel_;
        mip::TrilinearRegularInterpolation inter_;
      };

      //////////////////////////////////////////////////////////////////////////

    } //plasticAndHeat
  } //problem
} //am3
#endif // fineModelSSCurves_h__
