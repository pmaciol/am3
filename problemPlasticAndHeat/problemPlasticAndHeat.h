
/*! \file problemPlasticAndHeat.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef problemPlasticAndHeat_h__
#define problemPlasticAndHeat_h__
/*	include *********************************************************/

#include <boost/ptr_container/ptr_vector.hpp>
#include <vector>
#include <map>
#include <boost/shared_ptr.hpp>
#include "../models/dataStructures.h"
#include "../models/contracts.h"

//#include "../adapter/problemAdapter.h"
#include "../knowledge/kbsInterfaces.h"
#include "../logger/logger.h"

#include "../models/modelSelector.h"

/*	using ***********************************************************/

namespace con=am3::model::contract;
namespace prop=am3::model::properties;
namespace lo=am3::logger;
namespace kno=am3::knowledge;
namespace mo=am3::model;
/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace problem
  {
    namespace plasticAndHeat
    {
      struct PlasticAndHeat2DCoarseContract
        : public con::Provider<prop::TotalEffectiveStrain,prop::Coordinates2D>
        , public con::Provider<prop::StrainRate2D,prop::Coordinates2D>
        , public con::Provider<prop::EffectiveStrainRate,prop::Coordinates2D>
        , public con::Provider<prop::Temperature,prop::Coordinates2D>
        , public con::Provider<prop::Coordinates2D,prop::Coordinates2D>
        , public con::Provider<prop::Time,prop::Coordinates2D>
        , public con::Provider<prop::ElementNumberGlobal,prop::Coordinates2D>
      {
        using con::Provider<prop::TotalEffectiveStrain,prop::Coordinates2D>::GetState;
        using con::Provider<prop::StrainRate2D,prop::Coordinates2D>::GetState;
        using con::Provider<prop::EffectiveStrainRate,prop::Coordinates2D>::GetState;
        using con::Provider<prop::Temperature,prop::Coordinates2D>::GetState;
        using con::Provider<prop::Coordinates2D,prop::Coordinates2D>::GetState;
        using con::Provider<prop::Time,prop::Coordinates2D>::GetState;
        using con::Provider<prop::ElementNumberGlobal,prop::Coordinates2D>::GetState;
        virtual ~PlasticAndHeat2DCoarseContract(){}
      };

      template<typename TARGET>
      struct PlasticAndHeat2DCoarseCaller
        : public PlasticAndHeat2DCoarseContract
      {
        PlasticAndHeat2DCoarseCaller():isCoarseModelSet_(false){};
        void SetCalledModel(boost::shared_ptr<TARGET> toCall)
        {
          isCoarseModelSet_=true;
          toCall_=toCall;
        }
        bool IsCoarseInterfaceSet()
        {
          if (!isCoarseModelSet_)lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Coarse model not set");
          return isCoarseModelSet_;
        }
        virtual prop::TotalEffectiveStrain GetState(pnt::Point<prop::Coordinates2D>* point,const prop::TotalEffectiveStrain* typeTrait){return InternalGetState(point,typeTrait);};
        virtual prop::StrainRate2D GetState(pnt::Point<prop::Coordinates2D>* point,const prop::StrainRate2D* typeTrait){return InternalGetState(point,typeTrait);};
        virtual prop::EffectiveStrainRate GetState(pnt::Point<prop::Coordinates2D>* point,const prop::EffectiveStrainRate* typeTrait){return InternalGetState(point,typeTrait);};
        virtual prop::Temperature GetState(pnt::Point<prop::Coordinates2D>* point,const prop::Temperature* typeTrait){return InternalGetState(point,typeTrait);};
        virtual prop::Coordinates2D GetState(pnt::Point<prop::Coordinates2D>* point,const prop::Coordinates2D* typeTrait){return InternalGetState(point,typeTrait);};
        virtual prop::Time GetState(pnt::Point<prop::Coordinates2D>* point,const prop::Time* typeTrait){return InternalGetState(point,typeTrait);};
        virtual prop::ElementNumberGlobal GetState(pnt::Point<prop::Coordinates2D>* point,const prop::ElementNumberGlobal* typeTrait){return InternalGetState(point,typeTrait);};
        //virtual prop::Stress2D GetState(pnt::Point<prop::Coordinates2D>* point,const prop::Stress2D* typeTrait){return InternalGetState(point,typeTrait);};
      protected:
        bool isCoarseModelSet_;
        boost::shared_ptr<TARGET> toCall_;
        template<typename TARGET_PROPERTY>
        TARGET_PROPERTY InternalGetState(pnt::Point<prop::Coordinates2D>* point, const TARGET_PROPERTY* const property)
        {
          if (!isCoarseModelSet_) lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Coarse model refernece not initialized");
          return toCall_->GetState(point,property);
        }
      };

      struct PlasticAndHeat2DFineContract
        : public con::Provider<prop::YieldStress,prop::Coordinates2D>
        , public con::Provider<prop::StressDStrain,prop::Coordinates2D>
        , public con::Provider<prop::StressDStrainRate,prop::Coordinates2D>
      {
        using con::Provider<prop::YieldStress,prop::Coordinates2D>::GetState;
        using con::Provider<prop::StressDStrain,prop::Coordinates2D>::GetState;
        using con::Provider<prop::StressDStrainRate ,prop::Coordinates2D>::GetState;

        virtual ~PlasticAndHeat2DFineContract(){}
      };

	  struct PlasticAndHeat2DSwitcherContract
		  : public con::Provider<prop::DeltaTemperature,prop::Coordinates2D>
	  {
		  using con::Provider<prop::DeltaTemperature,prop::Coordinates2D>::GetState;
		  virtual prop::ModelID WhichModelLastHere(pnt::Point<prop::Coordinates2D> * point)=0;

		  virtual ~PlasticAndHeat2DSwitcherContract(){}
	  };

      //////////////////////////////////////////////////////////////////////////

      template<typename I_COARSE_SCALE, typename I_ADAPTER>
      class FineModelsSwitcher
        : public mo::ModelSelector<PlasticAndHeat2DFineContract
			,  I_COARSE_SCALE
			, PlasticAndHeat2DSwitcherContract
			,  I_ADAPTER
			, prop::Coordinates2D
		>
      {
	  protected:
		  std::map<prop::ElementNumberGlobal,prop::ModelID> whichModelLastUsed_;
		  std::map<prop::ElementNumberGlobal,prop::Temperature> TemperaturesInCAModels_;
      public:
        using I_COARSE_SCALE::GetState;
        
       virtual prop::YieldStress GetState(pnt::Point<prop::Coordinates2D>* point,const prop::YieldStress* typeTrait)
       {
		   if (!this->isFineInterfaceSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"Fine interface of FineModelSelector not set");
		   if (!this->isI4AdapterSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"I4Adapter interface of FineModelSelector not set");
		   typename mo::ModelSelector<PlasticAndHeat2DFineContract,  I_COARSE_SCALE, PlasticAndHeat2DSwitcherContract,  I_ADAPTER, prop::Coordinates2D>::INTERFACE_FOR_ADAPTER::MODEL_ID mId;
		   mId=this->i4Adapter_->GetState(point,&mId);
// 		   this->lastUsedModel_.first=point;
// 		   this->lastUsedModel_.second=mId;
		   

		   prop::YieldStress toReturn = this->fineInterfacesContainer_.at(mId())->GetState(point,typeTrait);//GetFineInterface(point)->GetState(point,typeTrait);
		   SetLastUsedModel(point,mId);
		   return toReturn;
	   };
       virtual prop::StressDStrain GetState(pnt::Point<prop::Coordinates2D>* point,const prop::StressDStrain * typeTrait)
       {return this->GetFineInterface(point)->GetState(point,typeTrait);};
       virtual prop::StressDStrainRate GetState(pnt::Point<prop::Coordinates2D>* point,const prop::StressDStrainRate* typeTrait)
       {return this->GetFineInterface(point)->GetState(point,typeTrait);};
	   virtual prop::DeltaTemperature GetState(pnt::Point<prop::Coordinates2D>* point,const prop::DeltaTemperature* typeTrait)
	   {
		   prop::Temperature temp,tempLower,tempUpper;
		   temp=this->GetState(point,&temp);
		   prop::DeltaTemperature tempDTemp,dTemp;
		   dTemp(1e8);

		   typedef std::map<prop::ElementNumberGlobal,prop::Temperature> map_type;

		   BOOST_FOREACH(const map_type::value_type& myPair, TemperaturesInCAModels_)
		   {
			   tempDTemp=(myPair.second-temp).absolute();
			   if (dTemp>tempDTemp) dTemp=tempDTemp;
		   }
		return dTemp;
	   }

	   void SetLastUsedModel( pnt::Point<prop::Coordinates2D>* point ,const prop::ModelID& lastUsedModelID) 
	   {
		   prop::ElementNumberGlobal number;
		   number=this->GetState(point,&number);
		   std::map<prop::ElementNumberGlobal,prop::ModelID>::iterator it=whichModelLastUsed_.find(number);
		   //prop::ModelID lastUsedModelID=WhichModelLastHere(point);
		   if (it==whichModelLastUsed_.end())
		   {
			   whichModelLastUsed_.insert(std::pair<prop::ElementNumberGlobal,prop::ModelID>(number,lastUsedModelID));
		   }
		   else
		   {
			   it->second=lastUsedModelID;
		   }
		   if (lastUsedModelID()==1)
		   {
	   
			   prop::Temperature temp;
			   temp=this->GetState(point,&temp);
			   std::map<prop::ElementNumberGlobal,prop::Temperature>::iterator itTemp=TemperaturesInCAModels_.find(number);
			   if (itTemp==TemperaturesInCAModels_.end())
			   {
				   TemperaturesInCAModels_.insert(std::pair<prop::ElementNumberGlobal,prop::Temperature>(number,temp));
			   }
			   else
			   {
				   itTemp->second=temp;
			   }
		   }
	   }

	   virtual prop::ModelID WhichModelLastHere(pnt::Point<prop::Coordinates2D> * point)
	   {
		   prop::ElementNumberGlobal number;
		   number=this->GetState(point,&number);		   
		   std::map<prop::ElementNumberGlobal,prop::ModelID>::iterator it=whichModelLastUsed_.find(number);
		   if (it==whichModelLastUsed_.end())
		   {
			   prop::ModelID tmp;
			   tmp(-1);
			   return tmp;
		   }
		   else
		   {
			   return it->second;
		   }
		   
	   }
      };


    } //plasticAndHeat
  } //problem
} //am3
#endif // problemPlasticAndHeat_h__
