#include "caseEsaform2015.h"
#include "../softwareDeformBuiltin/deformHandlerOutputsProviders.h"
#include "../modelDislocations/dislocationRandomEquation.h"
#include "../modelDislocations/dislocationWallEquation.h"
#include "../modelStress/stressSum.h"
#include "../modelStress/stressAthermalDislocationsEq.h"
#include "../modelStress/stressThermalOrowan.h"
#include "../modelStress/shearModulus.h"
//////////////////////////////////////////////////////////////////////////
//#include "../softwareMatCalcGeneric/matCalcGeneric.h"
#include "../adapter/adapter.h"
#include "kbs.h"
#include "matcalc.h"
//#include "../softwareDeformBuiltin/deformHandler.h"
//////////////////////////////////////////////////////////////////////////
//#include "../softwareDeformBuiltin/deformHandler.h"

namespace mst = am3::model::stress;
namespace mdi = am3::model::dislocations;
//////////////////////////////////////////////////////////////////////////
//namespace smc = am3::software::matcalc;
namespace ada = am3::adapter;

//////////////////////////////////////////////////////////////////////////


am3::cases::esaform2015::Main*  mainInstance = new am3::cases::esaform2015::Main;


namespace am3
{
  namespace cases
  {
    namespace esaform2015
    {

      void Main::Init()
      {
        DATABUS_INSTANCE(mainDatabus, CaseEsaform2015Databus);

        //******************************************************************************************************************************************************************
        // In and out connections to Deform
        //******************************************************************************************************************************************************************

        // StressProxy; gets stress and stress's derivatives to macroscale software (FEM)
        MODEL_INSTANCE_REDESIGN(mst::StressProxy, StressProxyModel, CaseEsaform2015Databus, stressReturn, mainDatabus);

        // Deform handler
        boost::shared_ptr<sdm::OneStepOneElementNoElementCheckStresses> stateProvider(new sdm::OneStepOneElementNoElementCheckStresses(stressReturn));

        // Representation of macroscopic state - interface to Deform
        // Submodel; handler of Deform procedure
        MODEL_INSTANCE_WITH_PARAM(MacroModelState, MacroModelStateModel, CaseEsaform2015Databus, macroModelState, mainDatabus, stateProvider);

        // IvManager gets new IV values from Databus. 
        MODEL_INSTANCE_REDESIGN(ad::IvManager, IvManager, CaseEsaform2015Databus, ivManager, mainDatabus);
        // ivProvider is a handler of Deform user variables procedure; it is responsible for both way communication
        boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivProvider(new sdm::OneStepOneElementNoElementCheckUserValues(ivManager));
        // This must be used when real Deform is used; here it is only for demonstration purposes 
        sdm::SetDeformHandlerFacadeUpd(ivProvider);

        // IV source for Random dislocation model
        boost::shared_ptr<ad::IVStorageAdaptedToDislRandEquat> dislRandEquatStorage(new ad::IVStorageAdaptedToDislRandEquat(ivProvider));

        // IV source for Wall dislocation model
        boost::shared_ptr<ad::IVStorageAdaptedToDislWalEquat> dislWallEquatStorage(new ad::IVStorageAdaptedToDislWalEquat(ivProvider));

        // Setting Deform-side pointers
        sdm::SetDeformHandlerFacadeMtr(stateProvider);
        sdm::SetDeformHandlerFacadeUpd(ivProvider);
        //******************************************************************************************************************************************************************
        // Other submodels
        //******************************************************************************************************************************************************************

        const double burgers = 0.286e-9;
        const double interObstacle = 2.5e-6;
        const double sizeObstacle = 1.0e-6; //!!!!!!!! Verify!
        const double activationEnergy = 2.4e-19;
        const double freqObstacle = 8e13;
        const double dislDens = 1e11;


        // Mock stresses are needed because stress derivatives  are not available in stress model
        MODEL_INSTANCE_REDESIGN(mot::MockStressDerivatives, MockStressesModel, CaseEsaform2015Databus, mockStressDerivatives, mainDatabus);

        //If not-default constructor needed, the macro MODEL_INSTANCE_WITH_PARAMS must be used
        MODEL_INSTANCE_WITH_PARAM(mst::StressThermalAthermalTaylor, StressThermalAthermalTaylorModel, CaseEsaform2015Databus, stressThermalAthermalTaylor, mainDatabus, 3.06);

        mst::StressThermalOrowanReduced_Params prms(burgers * interObstacle * sizeObstacle, activationEnergy, interObstacle, freqObstacle, dislDens, burgers);
        MODEL_INSTANCE_WITH_PARAM(mst::StressThermalOrowanReduced, StressThermalOrowanReducedModel, CaseEsaform2015Databus, stressThermalOrowanReduced, mainDatabus, prms);

        mst::StressAthermalDislocationsEq_Param strAth(0.3, 3.0, burgers);
        MODEL_INSTANCE_WITH_PARAM(mst::StressAthermalDislocationsEq, StressAthermalDislocationsEqModel, CaseEsaform2015Databus, stressAthermalDislocationsEq, mainDatabus, strAth);

        // Constructor parameters
        mdi::DislocationWallEquation_Param<ad::IVStorageAdaptedToDislWalEquat> dwParam(2.4e6, 6e7, 0.3, 7.0, 0.0, 0.0, dislWallEquatStorage);
        MODEL_INSTANCE_WITH_PARAM(DislocationWallEquation, DislocationWallEquationModel, CaseEsaform2015Databus, dislocationWallEquation, mainDatabus, dwParam);

        // Constructor parameters
        mdi::DislocationRandomEquation_Param<ad::IVStorageAdaptedToDislRandEquat> drParam(22e6, 80e7, 2.2, 105.0, 0.0, 0.0, dislRandEquatStorage);
        MODEL_INSTANCE_WITH_PARAM(DislocationRandomEquation, DislocationRandomEquationModel, CaseEsaform2015Databus, dislocationRandomEquation, mainDatabus, drParam);

        mpr::ShearModulus sm; sm(26e9);
        MODEL_INSTANCE_WITH_PARAM(mst::ShearModulusConst, ShearModulusConstModel, CaseEsaform2015Databus, shearModulusConst, mainDatabus, sm);

        MODEL_INSTANCE_REDESIGN(SecondPhasePrecipitationsConst, SecondPhasePrecipitationsConstModel, CaseEsaform2015Databus, secondPhasePrecipitationsConst, mainDatabus);


        // Adding MatCalc!
//   MODEL_INSTANCE_REDESIGN(PrecipitationsSwitcher, PrecipitationsModel, CaseEsaform2015Databus, precipitationsSwitcher, mainDatabus);
//   MatCalcScriptProvider scripts;
//   boost::shared_ptr<SecondPhasePrecipitationsConst> precConst (new SecondPhasePrecipitationsConst);
//   boost::shared_ptr<SecondPhasePrecipitationsMatCalc> gmm(new SecondPhasePrecipitationsMatCalc(scripts));
// 
//   precipitationsSwitcher->AddModel(precConst);
//   precipitationsSwitcher->AddModel(gmm);



        initialized = true;
      }

      Main::Main()
      {
        initialized = false;
        try
        {
          Init();
        }
        catch (std::exception& e)
        {
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, e.what());
          initialized = false;
        }
      }

      bool Main::IsInitialized() const
      {
        return initialized;
      }

    }
  }
}



