
/*! \file matcalcMPI.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseEsaform2015_matcalcMPI_h__
#define am3caseEsaform2015_matcalcMPI_h__
/*	include *********************************************************/

#include <vector>
#include "../models/inputTypelists.h"
#include "../models/point.h"
#include "../models/description.h"
#include "matcalcMpiConnector.h"

/*	using ***********************************************************/

namespace mo = am3::model;
namespace mpt = am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace esaform2015
		{
      class PointsIdsContainer
      {
      public:
        bool MadePointsIds(std::vector<mpt::Point<mpr::Coordinates2D>*> pointsToRun);
        bool CheckExistingIds(std::vector<mpt::Point<mpr::Coordinates2D>*> pointsToRun);
        MatCalcMpiInstance* GetNextInstance();
        bool LastInstance();
        MatCalcMpiInstance* GetInstance(const ProcessId& id) ;
        bool IdIsValid(const ProcessId& id) const;
        ProcessId GetId(mpt::Point<mpr::Coordinates2D>* point);

      protected:
        std::vector<MatCalcMpiInstance> matCalcMpiInstances_;

      };

      //******************************************************************************************************************************************************************

      template<typename INPUT>
      class SecondPhasePrecipitationsMatCalcMpiManager
        : public PrecipitationsContract
        , public mo::InputTypelist < INPUT > //<MatcalcInputContract>
      {
      public:
        
        
        SecondPhasePrecipitationsMatCalcMpiManager() :firstRun_(true), previousNewTime_(0){};
        void RunForPoints(std::vector<mpt::Point<mpr::Coordinates2D>*> pointsToRun);


        Alcrfemnsi_a_p0_Radius GetState(mpt::Point<mpr::Coordinates2D> *, const Alcrfemnsi_a_p0_Radius *const);
        Alcrfemnsi_a_p0_VolumeFraction GetState(mpt::Point<mpr::Coordinates2D> *, const Alcrfemnsi_a_p0_VolumeFraction *const);

        template <typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point);

        void ReRunAllPoints();
        void RunAllPoints();

      protected:
        bool WaitingForComputations(const ProcessId&) ;
        template <typename VAR>
        VAR GetStored(const ProcessId& id) ;
        

        bool MatchPointToIds(std::vector<mpt::Point<mpr::Coordinates2D>*> pointsToRun);


        bool NewTimeStep(mpr::Time) const;
        ProcessId GetId(mpt::Point<mpr::Coordinates2D>* point) { return pointsIdsContainer_.GetId(point); }

      protected: 
        PointsIdsContainer pointsIdsContainer_;
        bool firstRun_;
        mpr::Time previousNewTime_;
        mpr::Time newTime_;
      };

////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////

      ProcessId PointsIdsContainer::GetId(mpt::Point<mpr::Coordinates2D>* point)
      {
        ProcessId id = -1;
        for each (MatCalcMpiInstance instance in matCalcMpiInstances_)
        {
          if ((*instance.GetPoint()) == (*point))
          {
            id = instance.GetProcessId();
          }

        }
        return id;
      }



      MatCalcMpiInstance* PointsIdsContainer::GetInstance(const ProcessId& id) 
      {
        return &matCalcMpiInstances_[id];

      }


      bool PointsIdsContainer::IdIsValid(const ProcessId& id) const
      {
        if (id < matCalcMpiInstances_.size()) return true;
        else return false;
      }





//******************************************************************************************************************************************************************
      
      template<typename INPUT>
      template <typename VAR>
      VAR SecondPhasePrecipitationsMatCalcMpiManager<INPUT>::GetStored(const ProcessId& id) 
      {
        return pointsIdsContainer_.GetInstance(id)->GetValue<VAR>();
      }

      template<typename INPUT>
      bool am3::cases::esaform2015::SecondPhasePrecipitationsMatCalcMpiManager<INPUT>::WaitingForComputations(const ProcessId& id) 
      {
        if (pointsIdsContainer_.GetInstance(id)->RunFinished())
        {
          return true;
        }
        else
        {
          return false;
        }
      }



      template<typename INPUT>
      void SecondPhasePrecipitationsMatCalcMpiManager<INPUT>::RunAllPoints()
      {
        while (!pointsIdsContainer_.LastInstance())
        {
          MatCalcMpiInstance* instance = pointsIdsContainer_.GetNextInstance();
          mpr::Temperature temperature = bus_->GetFromDatabus<mpr::Temperature>(instance->GetPoint());
          instance.Run(newTime_, temperature);
        }
        previousNewTime_ = newTime_;
      }


      template<typename INPUT>
      void SecondPhasePrecipitationsMatCalcMpiManager<INPUT>::ReRunAllPoints()
      {
        assert(!"This is not yet implemented!");
      }


      template<typename INPUT>
      bool SecondPhasePrecipitationsMatCalcMpiManager<INPUT>::NewTimeStep(mpr::Time newTime) const
      {
        if (newTime <= previousNewTime_)
        {
          return false;
        } 
        else
        {
          previousNewTime_ = newTime;
          return true;
        }
      }

      //******************************************************************************************************************************************************************

      template<typename INPUT>
      bool am3::cases::esaform2015::SecondPhasePrecipitationsMatCalcMpiManager<INPUT>::MatchPointToIds(std::vector<mpt::Point<mpr::Coordinates2D>*> pointsToRun)
      {
        if (firstRun_)
        {
          firstRun_ = false;
          pointsIdsContainer_.MadePointsIds(pointsToRun);
          return true;
        } 
        else
        {
          return pointsIdsContainer_.CheckExistingIds(pointsToRun);
        }
      }

      //******************************************************************************************************************************************************************

      template<typename INPUT>
      template <typename VAR>
      VAR SecondPhasePrecipitationsMatCalcMpiManager<INPUT>::Get(mpt::Point<mpr::Coordinates2D> * point)
      {
        ProcessId id = GetId(point);
        if (pointsIdsContainer_.IdIsValid(id))
        {
          while (WaitingForComputations(id));
          return(GetStored<VAR>(id));
        }
        else
        {
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "No such MPI MatCalc process");
          return mo::WrongVariable< VAR >();
        }
      }

      //******************************************************************************************************************************************************************
      
      template<typename INPUT>
      Alcrfemnsi_a_p0_Radius SecondPhasePrecipitationsMatCalcMpiManager<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point, const Alcrfemnsi_a_p0_Radius *const)
      {
        return Get<Alcrfemnsi_a_p0_Radius>(point);
      }

      template<typename INPUT>
      Alcrfemnsi_a_p0_VolumeFraction SecondPhasePrecipitationsMatCalcMpiManager<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point, const Alcrfemnsi_a_p0_VolumeFraction *const)
      {
        return Get<Alcrfemnsi_a_p0_VolumeFraction>(point);
      }

      //******************************************************************************************************************************************************************

      template<typename INPUT>
      void SecondPhasePrecipitationsMatCalcMpiManager<INPUT>::RunForPoints(std::vector<mpt::Point<mpr::Coordinates2D>*> pointsToRun)
      {
        if (MatchPointToIds(pointsToRun))
        {
          newTime_ = pointsToRun.begin()->GetTimeProperty();
          if (NewTimeStep(newTime_))
          {
            RunAllPoints();
          } 
          else
          {
            ReRunAllPoints();
          }
        }
      }


		} //esaform2015
	} //cases
} //am3
#endif // am3caseEsaform2015_matcalcMPI_h__
