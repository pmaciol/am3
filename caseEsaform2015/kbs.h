
/*! \file kbs.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef esaformKbs_h__
#define esaformKbs_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../models/dataStructures.h"
//#include "../problemPolycrystalMicrostructure/contracts.h"
//#include "databus.h"
#include "contract.h"
#include "caseEsaform2015.h"

/*	using ***********************************************************/

namespace ce5 = am3::cases::esaform2015;
namespace mpr = am3::model::properties;
namespace pdi = am3::problem::dislocations;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace esaform2015
    {
			// UserGuide: This is ugly and simple "Knowledge Based System" for caseColdRolling.
      template<typename INPUT>
			class Kbs
        : public mo::InputTypelist < INPUT >
      {
      public:
        typedef ce5::MacroContract Inputs;
        typedef mot::EmptyContract2D Contract;


        Kbs(boost::shared_ptr<INPUT> databus)
        {
          temperature(0);
          SetModelInput(databus.get());
        }

        unsigned int instances;
        mpr::Temperature temperature;

        template <typename VARIABLE> 
        mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point)
				{
          if (bus_)
          {
            const VARIABLE* trait = nullptr;
            return GetModelId(point, trait);
          } 
          else
          {
            mpr::ModelID mId;
            mId(std::numeric_limits<int>::signaling_NaN());
            lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE, "Databus not initialized pointer not set");
            return mId;
          }
				}

			protected:
				// UserGuide:  Here is "knowledge"
				mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const mpr::StressAthermal* const)
				{
          mpr::ModelID mId;
  				mId(0);
					return mId;
				}
// 				// UserGuide: GetModelId must be provided for each variable in Contract of the Switcher, which use this KBS. If more than one Switcher is used, GetModelId must be provided for all variables from all Contracts
        mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const Alcrfemnsi_a_p0_Radius* const)
				{
          mpr::ModelID mId;
          mpr::Temperature localTemperature;
          localTemperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          if ((temperature() - localTemperature()) > 50)
          {
            mId(1);
          }
          else
          {
            mId(0);
          }
          return mId;
        }
        mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const Alcrfemnsi_a_p0_VolumeFraction* const)
        {
          mpr::ModelID mId;
          mpr::Temperature localTemperature;
          localTemperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          if ((temperature() - localTemperature()) > 50)
          {
            mId(1);
          }
          else
          {
            mId(0);
          }
          return mId;
        };
			};
    } //esaform2015
  } //cases
} //am3
#endif // esaformKbs_h__
