
/*! \file matcalcMpiConnector.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseEsaform2015_matcalcMpiConnector_h__
#define am3caseEsaform2015_matcalcMpiConnector_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/point.h"
#include "contract.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace esaform2015
    {
      typedef int ProcessId;

      

      class MatCalcMpiInstance
      {
      public:

        MatCalcMpiInstance();

        void Run(mpr::Temperature, mpr::Time);
        void Rerun(mpr::Temperature, mpr::Time);
        ProcessId GetProcessId() const;
        mpt::Point<mpr::Coordinates2D>* GetPoint() { return point_; };
        bool RunFinished() const;
        template<typename VAR>
        VAR GetValue() const;

        template<>
        Alcrfemnsi_a_p0_VolumeFraction GetValue() const
        {
          Alcrfemnsi_a_p0_VolumeFraction toRet;
          return toRet;
        }

        template<>
        Alcrfemnsi_a_p0_Radius GetValue()const
        {
          Alcrfemnsi_a_p0_Radius toRet;
          return toRet;
        }

      protected:
        bool isFinished_;
        ProcessId id_;
        mpt::Point<mpr::Coordinates2D>* point_;
        
        static ProcessId maxId_;
      };


////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////


      MatCalcMpiInstance::MatCalcMpiInstance()
      {
        id_ = maxId_++;
      }


      ProcessId MatCalcMpiInstance::GetProcessId() const
      {
        return id_;
      }

      //******************************************************************************************************************************************************************

      bool MatCalcMpiInstance::RunFinished() const
      {
        return isFinished_;
      }
    } //esaform2015
  } //cases
} //am3

#endif // am3caseEsaform2015_matcalcMpiConnector_h__
