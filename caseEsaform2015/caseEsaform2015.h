
/*! \file caseEsaform2015.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseEsaform2015_h__
#define caseEsaform2015_h__
/*	include *********************************************************/

#include "../adapter/ivstorage.h"
#include "../softwareDeform/policyOneStepOneElement.h"
#include "../modelTemplates/macroState.h"
#include "../modelTemplates/macroStress.h"
#include "../modelDislocations/dislocationRandomEquation.h"
#include "../modelDislocations/dislocationWallEquation.h"
#include "../modelStress/stressSum.h"
#include "../modelStress/stressAthermalDislocationsEq.h"
#include "../modelStress/stressThermalOrowan.h"
#include "../modelStress/shearModulus.h"
#include "../modelStress/stressProxy.h"
#include "ivMapping.h"
#include "matcalc.h"
#include "contract.h"
#include "kbs.h"

/*	using ***********************************************************/

 namespace ad  = am3::adapter;
 namespace sdm = am3::software::deform;
 namespace mst = am3::model::stress;
 namespace mdi = am3::model::dislocations;
 namespace mot = am3::model::templates;

/*	extern **********************************************************/

namespace am3
{
  namespace cases
  {
    namespace esaform2015
    {
      // Get material state (strain, temperature, time, etc) from FEM software (Deform2D in this case)
      // An adapter of mot::MacroModelState to 'Databus-entry' format (only one template parameter)
      template<typename INPUT>
      class MacroModelState 
        : public mot::MacroModelState < sdm::OneStepOneElementNoElementCheckStresses, INPUT >
      {
        public:
        MacroModelState(boost::shared_ptr<sdm::OneStepOneElementNoElementCheckStresses> macroStateHandler) : mot::MacroModelState < sdm::OneStepOneElementNoElementCheckStresses, INPUT >(macroStateHandler){};
      };

      //******************************************************************************************************************************************************************

      // An adapter of mdi::DislocationWallEquation to 'Databus-entry' format (only one template parameter)
      template <typename INPUT>
      struct DislocationWallEquation : public mdi::DislocationWallEquation < INPUT, ad::IVStorageAdaptedToDislWalEquat >
      {
        DislocationWallEquation(const mdi::DislocationWallEquation_Param<ad::IVStorageAdaptedToDislWalEquat >& param)
          : mdi::DislocationWallEquation < INPUT, ad::IVStorageAdaptedToDislWalEquat>::DislocationWallEquation(param){};
        typedef ad::IVStorageAdaptedToDislWalEquat IvStorage;
      };

      // An adapter of mdi::DislocationRandomEquation to 'Databus-entry' format (only one template parameter)
      template <typename INPUT>
      struct DislocationRandomEquation : public mdi::DislocationRandomEquation < INPUT, ad::IVStorageAdaptedToDislRandEquat >
      {
        DislocationRandomEquation(const mdi::DislocationRandomEquation_Param<ad::IVStorageAdaptedToDislRandEquat >& param) 
          : mdi::DislocationRandomEquation < INPUT, ad::IVStorageAdaptedToDislRandEquat>::DislocationRandomEquation(param){};
        typedef ad::IVStorageAdaptedToDislRandEquat IvStorage;
      };
      //******************************************************************************************************************************************************************

      // Precipitation switcher
      template<typename INPUT>
      struct PrecipitationsSwitcher
        : public ada::SwitcherStatic<Kbs, PrecipitationsContract, INPUT, MacroContract>
      {
      public:
        PrecipitationsSwitcher(boost::shared_ptr<Kbs<INPUT> > kbs) : ada::SwitcherStatic<Kbs, PrecipitationsContract, INPUT, MacroContract>(kbs){};
//        PrecipitationsSwitcher() {};
      };


      //******************************************************************************************************************************************************************

      // List of 'Databus' classes
      typedef TEMPLATELIST_11(
        mst::StressProxy,
        mot::MockStressDerivatives,
        ad::IvManager,
        mst::StressThermalAthermalTaylor,
        mst::StressThermalOrowanReduced,
        mst::StressAthermalDislocationsEq,
        DislocationWallEquation,
        DislocationRandomEquation,
        mst::ShearModulusConst,
        SecondPhasePrecipitationsConst, //precipitationsSwitcher
        MacroModelState
        ) CaseEsaform2015List;//,

      // Databus
      typedef db::DatabusTypelists<CaseEsaform2015List> CaseEsaform2015Databus;

      // List of 'Databus' classes
      typedef TEMPLATELIST_11(
        mst::StressProxy,
        mot::MockStressDerivatives,
        ad::IvManager,
        mst::StressThermalAthermalTaylor,
        mst::StressThermalOrowanReduced,
        mst::StressAthermalDislocationsEq,
        DislocationWallEquation,
        DislocationRandomEquation,
        mst::ShearModulusConst,
        PrecipitationsSwitcher,
        MacroModelState
        ) CaseEsaform2015PrecipitationsSwitchList;//,

      // Databus
      typedef db::DatabusTypelists<CaseEsaform2015PrecipitationsSwitchList> CaseEsaform2015PrecipitationsSwitchDatabus;

      //******************************************************************************************************************************************************************

      // This case is used as a dll library. Constructor of Main class works like a "main" procedure in single executable solution
      // Implementation in cpp file (probably caseVactorTUG.cpp)
      class Main: public am3::model::InitializableMainModel
      {
      public:
        Main();
        bool initialized;
        void Init();

        virtual bool IsInitialized() const;

        boost::shared_ptr<CaseEsaform2015Databus> mainDatabus_;
      };  
    }
  }
}

#endif // caseEsaform2015_h__
