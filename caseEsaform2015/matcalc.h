
/*! \file matcalc.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef matcalc_h__
#define matcalc_h__
/*	include *********************************************************/

//#include "../softwareMatCalc/matcalcController.h"

#include "contract.h"
#include <boost/shared_ptr.hpp>
#include "../models/inputTypelists.h"
#include "../modelTemplates/modelsTemplates.h"


#include "../softwareMatCalc/externs.h"

/*	using ***********************************************************/

//namespace smc = am3::software::matcalc;

//#include <Windows.h>
using boost::shared_ptr;

#define MC_FALSE false
#define MC_HANDLE int
#define MC_BOOL bool

#include <string>
//#include <vector>
//#include <numeric>
// 
// //#include "mc_types.h"
// #include "../softwareMatCalc/mc_defines.h"
// 
// #include "../softwareMatCalc/externs.h"
// #include <string>     // std::string, std::to_string
// 
// #include "../modelTemplates/modelsTemplates.h"
//#include <fstream>

/*	extern **********************************************************/

namespace mot = am3::model::templates;

/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace esaform2015
		{

      class MatCalcScriptProvider
      {
      public: 
        std::string Workspace() const {return "d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\";}
        std::string ProgramPath() const { return "c:\\Program Files\\MatCalc\\"; }
        std::string LicensePath() const {return "c:\\Program Files\\MatCalc\\license.dat";}
        std::string LicenseFile() const {return "license.dat";}
//         std::string InitScript() const {return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\esaform2015init.mcs";}
//         std::string InitModelScript() const {return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\esaform2015initModel.mcs";}
//         std::string FirstStepScript() const {return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\esaform2015first.mcs";}
//         std::string NextStepScript() const {return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\esaform2015next.mcs";}
//         std::string LastStepScript() const {return "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\esaform2015next.mcs";}

        std::string InitScript() const { return "C:\\Users\\pmaciol\\SkyDrive\\Konferencje\\2016 Thermec\\matcalc\\heat_treatment_9Cr_steel_init.mcs"; }
        std::string InitModelScript() const { return "C:\\Users\\pmaciol\\SkyDrive\\Konferencje\\2016 Thermec\\matcalc\\heat_treatment_9Cr_steel_initModel.mcs"; }
        std::string FirstStepScript() const { return "C:\\Users\\pmaciol\\SkyDrive\\Konferencje\\2016 Thermec\\matcalc\\heat_treatment_9Cr_steel_firstStep.mcs"; }
        std::string NextStepScript() const { return "C:\\Users\\pmaciol\\SkyDrive\\Konferencje\\2016 Thermec\\matcalc\\heat_treatment_9Cr_steel_nextStep.mcs"; }
        std::string LastStepScript() const { return "C:\\Users\\pmaciol\\SkyDrive\\Konferencje\\2016 Thermec\\matcalc\\heat_treatment_9Cr_steel_nextStep.mcs"; }
      };



      struct PrecipitationStructure
      {
        mpr::PrecipitationsMeanRadius precipitationsMeanRadius;
        Alcrfemnsi_a_p0_VolumeFraction alcrfemnsi_a_p0_VolumeFraction;
        Alcrfemnsi_a_p0_Radius alcrfemnsi_a_p0_Radius;
      };
      
      template <typename INPUT>
      class SecondPhasePrecipitationsConst
        : public PrecipitationsContract
        , public mo::InputTypelist < INPUT >
      {
      public:
        typedef mot::EmptyContract2D Inputs;

        Alcrfemnsi_a_p0_Radius GetState(mpt::Point<mpr::Coordinates2D> *,const Alcrfemnsi_a_p0_Radius *const ){Alcrfemnsi_a_p0_Radius toRet; toRet(0.2);return toRet;};
        Alcrfemnsi_a_p0_VolumeFraction GetState(mpt::Point<mpr::Coordinates2D> *,const Alcrfemnsi_a_p0_VolumeFraction *const ){Alcrfemnsi_a_p0_VolumeFraction toRet; toRet(0.3);return toRet;};
        template<typename VAR> VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> Alcrfemnsi_a_p0_Radius Get<Alcrfemnsi_a_p0_Radius>(mpt::Point<mpr::Coordinates2D> * point) { Alcrfemnsi_a_p0_Radius* trait;  return GetState(point, trait); };
        template<> Alcrfemnsi_a_p0_VolumeFraction Get<Alcrfemnsi_a_p0_VolumeFraction>(mpt::Point<mpr::Coordinates2D> * point) { Alcrfemnsi_a_p0_VolumeFraction* trait;  return GetState(point, trait); };
      };

      //******************************************************************************************************************************************************************

      template<typename INPUT>
			class SecondPhasePrecipitationsMatCalc
        : public PrecipitationsContract
        , public mo::InputTypelist<INPUT> //<MatcalcInputContract>
      {

     public:
       SecondPhasePrecipitationsMatCalc(const MatCalcScriptProvider& scripts):scripts_(scripts), lastPoint_(nullptr), firstStepRan_(false),initialized_(false){};

       typedef mot::EmptyContract2D Inputs;

      Alcrfemnsi_a_p0_Radius GetState(mpt::Point<mpr::Coordinates2D> *,const Alcrfemnsi_a_p0_Radius *const );
      Alcrfemnsi_a_p0_VolumeFraction GetState(mpt::Point<mpr::Coordinates2D> *,const Alcrfemnsi_a_p0_VolumeFraction *const );
    protected:
      void Compute( mpt::Point<mpr::Coordinates2D> * point);
      void Init(mpr::Temperature initTemperature , mpr::Time initTime);
      void RunFirstStep(mpr::Temperature initTemperature, mpr::Temperature afterStepTemperature, mpr::Time initTime, mpr::Time afterStepTime);
      void RunNextStep(mpr::Temperature initTemperature, mpr::Temperature afterStepTemperature, mpr::Time initTime, mpr::Time afterStepTime);
      mpr::Temperature GetPreviousTemperature() const {return lastTemperature_;};
      void SetEndTemperature(const mpr::Temperature temp){lastTemperature_ = temp;};
      void CalculateOutput();


      const MatCalcScriptProvider& scripts_;
      bool firstStepRan_,initialized_;
      mpr::Temperature lastTemperature_;
      mpr::Time lastTime_;
      mpr::Time GetLastTime() const { return lastTime_; }
      void SetLastTime(mpr::Time val) { lastTime_ = val; }

      Alcrfemnsi_a_p0_Radius radius_;
      Alcrfemnsi_a_p0_VolumeFraction volFrac_;
      PrecipitationStructure returnedFromMatCalc_;
      bool isComputedForPoint(mpt::Point<mpr::Coordinates2D> * point)
      {
        if (lastPoint_ != nullptr)
        {
          if (*lastPoint_ == *point) return true;
          else return false;
        }
        else return false;
      }
      mpt::Point<mpr::Coordinates2D>* lastPoint_;
    };

      template<typename INPUT>
      void SecondPhasePrecipitationsMatCalc<INPUT>::Init(mpr::Temperature initTemperature, mpr::Time initTime)
      {
//         std::string workspacePath = scripts_.Workspace();
//         char * workspacePathChar = new char[workspacePath.size() + 1];
//         std::copy(workspacePath.begin(), workspacePath.end(), workspacePathChar);
//         workspacePathChar [workspacePath.size()] = '\0'; // don't forget the terminating 0
//         MCC_SetApplicationDirectoryChar(workspacePathChar);
        std::string programPath = scripts_.ProgramPath();
        char * programPathChar = new char[programPath.size() + 1];
        std::copy(programPath.begin(), programPath.end(), programPathChar);
        programPathChar[programPath.size()] = '\0'; // don't forget the terminating 0
        MCC_SetApplicationDirectoryChar(programPathChar);
        delete[] programPathChar;
        std::string licensePath = scripts_.LicensePath();
        char * licensePathChar = new char[licensePath.size() + 1];
        std::copy(licensePath.begin(), licensePath.end(), licensePathChar );
        licensePathChar [licensePath.size()] = '\0'; // don't forget the terminating 0
        MCC_SetLicenseFilePathChar(licensePathChar);
        delete[] licensePathChar ;
        MCC_ReadLicenseInformation();
        char * licText = MCC_GetLicenseInformation(false);
        // Log!
        MCC_ReadDefaultSettings();
//        MCC_SetGlobalParameter(MCGV_COMPOSITION_TYPE, CT_WEIGHT_PERCENT);

        MCC_InitializeKernel();
        std::string initScript = scripts_.InitScript();
        char * initScriptChar = new char[initScript.size() + 1];
        std::copy(initScript.begin(), initScript.end(), initScriptChar);
        initScriptChar [initScript.size()] = '\0'; // don't forget the terminating 0
        MCCOL_RunScriptFileChar(initScriptChar);
        delete initScriptChar;

        MCC_SetVariableValue("initTime", initTime());
        MCC_SetVariableValue("initTemperature", initTemperature());

        std::string initModelScript = scripts_.InitModelScript();
        char * initModelScriptChar = new char[initModelScript.size() + 1];
        std::copy(initModelScript.begin(), initModelScript.end(), initScriptChar);
        initScriptChar [initModelScript.size()] = '\0'; // don't forget the terminating 0
        MCCOL_RunScriptFileChar(initModelScriptChar);
        delete initModelScriptChar;

      }

      template<typename INPUT>
      void SecondPhasePrecipitationsMatCalc<INPUT>::RunFirstStep(mpr::Temperature initTemperature, mpr::Temperature afterStepTemperature, mpr::Time initTime, mpr::Time afterStepTime)
      {
        MCC_SetVariableValue("initTemperature", initTemperature());
        MCC_SetVariableValue("afterStepTemperature", afterStepTemperature());
        MCC_SetVariableValue("initTime", initTime());
        MCC_SetVariableValue("afterStepTime", afterStepTime()-initTime());

        double d = afterStepTime.Value();
        std::string str = std::to_string((long double)d);
        char cStr[200];
        size_t len = str.copy(cStr, str.length());
        cStr[len] = '\0';
        MCC_SetVariableString("stateName", cStr);

        std::string firstStepScript = scripts_.FirstStepScript();
        char * firstStepScriptChar = new char[firstStepScript.size() + 1];
        std::copy(firstStepScript.begin(), firstStepScript.end(), firstStepScriptChar);
        firstStepScriptChar [firstStepScript.size()] = '\0'; // don't forget the terminating 0
        MCCOL_RunScriptFileChar(firstStepScriptChar);
        delete firstStepScriptChar;
      }

      template<typename INPUT>
      void SecondPhasePrecipitationsMatCalc<INPUT>::RunNextStep(mpr::Temperature initTemperature, mpr::Temperature afterStepTemperature, mpr::Time initTime, mpr::Time afterStepTime)
      {
        MCC_SetVariableValue("initTemperature", initTemperature());
        MCC_SetVariableValue("afterStepTemperature", afterStepTemperature());
        MCC_SetVariableValue("initTime", initTime());
        MCC_SetVariableValue("afterStepTime", afterStepTime()-initTime());
        double d = afterStepTime.Value();
        std::string str = std::to_string((long double)d);
        char cStr[200];
        size_t len = str.copy(cStr, str.length());
        cStr[len] = '\0';
        MCC_SetVariableString("nextStateName", cStr);

        std::string nextStepScript = scripts_.NextStepScript();
        char * nextStepScriptChar = new char[nextStepScript.size() + 1];
        std::copy(nextStepScript.begin(), nextStepScript.end(), nextStepScriptChar);
        nextStepScriptChar [nextStepScript.size()] = '\0'; // don't forget the terminating 0
        MCCOL_RunScriptFileChar(nextStepScriptChar);
        delete nextStepScriptChar;
      }

      template<typename INPUT>
      void SecondPhasePrecipitationsMatCalc<INPUT>::Compute(mpt::Point<mpr::Coordinates2D> * point)
      {
        mpr::Time time;
//        time = Get<mpr::Time,mpr::Coordinates2D>::InPoint(point);
//        time = bus_->GetFromDatabus<mpr::Time>(point);
//                //DebugBreak();
        mpr::Temperature temperature;
        //temperature = Get<mpr::Temperature,mpr::Coordinates2D>::InPoint(point);
        temperature = bus_->GetFromDatabus<mpr::Temperature>(point);

        if (initialized_)
        {
          if (firstStepRan_)
          {
            if ((GetLastTime()<=time)) //?
            {
              RunNextStep(GetPreviousTemperature(),temperature,GetLastTime(),time);
              SetEndTemperature(temperature);
              SetLastTime(time);
            }
            else
            {
              lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"MatCalc should not be fired more than once for the same time!");
            }
          } 
          else
          {
            RunFirstStep(GetPreviousTemperature(),temperature,0,time);
            firstStepRan_ = true;
          }
                  lastPoint_ = point->Snapshot().get();
        }
        else
        {
          Init(temperature,time);
          initialized_ = true;
        }
        CalculateOutput();
        radius_ = returnedFromMatCalc_.alcrfemnsi_a_p0_Radius;
        volFrac_ = returnedFromMatCalc_.alcrfemnsi_a_p0_VolumeFraction;
      }

      template<typename INPUT>
      Alcrfemnsi_a_p0_Radius SecondPhasePrecipitationsMatCalc<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point, const Alcrfemnsi_a_p0_Radius *const)
      {
        if (! isComputedForPoint(point)) Compute(point);
        return radius_;
      }

      template<typename INPUT>
      Alcrfemnsi_a_p0_VolumeFraction SecondPhasePrecipitationsMatCalc<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point, const Alcrfemnsi_a_p0_VolumeFraction *const)
      {
        if (! isComputedForPoint(point)) Compute(point);
        return volFrac_;
      }

      template<typename INPUT>
      void SecondPhasePrecipitationsMatCalc<INPUT>::CalculateOutput()
      {
        double val;
//        //DebugBreak();
        //MCC_GetCalcVariable("R_MEAN$ALCRFEMNSI_A_P0", &val, MC_FALSE, MC_TRUE);
        MCC_GetVariableValue("returnValue1", val);
        returnedFromMatCalc_.alcrfemnsi_a_p0_Radius(val);
        //MCC_GetCalcVariable("F_PREC$ALCRFEMNSI_A_P0", &val, MC_FALSE, MC_TRUE);
        //MCC_GetCalcVariable("NUM_PREC$CL_MGSI_P0", &val, MC_FALSE, MC_TRUE);
        MCC_GetVariableValue("returnValue2", val);
        returnedFromMatCalc_.alcrfemnsi_a_p0_VolumeFraction(val);

      }


      
		} //esaform2015
	} //cases
} //am3
#endif // matcalc_h__
