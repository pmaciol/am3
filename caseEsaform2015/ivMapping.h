
/*! \file caseEsaform2015.h **************************************************
* \author		Piotr Maciol
* \copyright 	GNU Public License.
* \brief
* \details
*********************************************************************/
#ifndef caseEsaform2015_ivmapping_h__
#define caseEsaform2015_ivmapping_h__
/*	include *********************************************************/

#include "boost/shared_ptr.hpp"
#include "../adapter/ivstorage.h"
#include "../modelDislocations/dislocationRandomEquation.h"
#include "../modelDislocations/dislocationWallEquation.h"
#include "../models/point.h"
#include "../modelTemplates/innerSideInterfacesMacro.h"
#include "../models/inputTypelists.h"
#include "contract.h"
#include "../softwareDeform/policyOneStepOneElement.h"
//#include "../softwareDeformBuiltin/deformHandler.h"

/*	using ***********************************************************/

namespace pdi = am3::problem::dislocations;
namespace ad  = am3::adapter;
namespace mpt = am3::model::point;
namespace mot = am3::model::templates;
namespace mo  = am3::model;
namespace sdm = am3::software::deform;
namespace ce5 = am3::cases::esaform2015;


/*	extern **********************************************************/


namespace am3
{
  namespace adapter
  {
    // Structure used to code/decode values in IV vector
    template<typename T>
    struct VariableID;

    template <> struct VariableID < pdi::DislocationsDensityWall > { static const unsigned int id = 0; typedef pdi::DislocationsDensityWall Type; };
    template <> struct VariableID<pdi::DislocationsDensityRandom>              { static const unsigned int id = 1; typedef pdi::DislocationsDensityRandom Type; };
    template <> struct VariableID<ce5::Alcrfemnsi_a_p0_Radius>                 {static const unsigned int id = 2; };
    template <> struct VariableID<ce5::Alcrfemnsi_a_p0_VolumeFraction>         {static const unsigned int id = 3; };

    //******************************************************************************************************************************************************************

    // Implements mot::OneStepOneElementIvNew interface for external IV requests; translates variables available in Databus into vectr of doubles
    template<typename INPUT>
    class IvManager
      : public mot::OneStepOneElementIvNew
      , public mo::InputTypelist<INPUT>
    {
    public:
      typedef typename ce5::StorageContract Inputs;
      typedef typename mot::EmptyContract2D Contract;

    protected:
      boost::shared_ptr<mot::OneStepOneElementIvPresent> ivStorage_;
      mpt::Point<mpr::Coordinates2D>* point_;

      virtual std::vector<double> GetInternalVariablesNew() const;
    };
    
    //******************************************************************************************************************************************************************


    // decodes double value identified with index into conrete type
    class IVStorageAdaptedToDislWalEquat
    {
    public:
      IVStorageAdaptedToDislWalEquat(boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivPrevious) : ivPrevious_(ivPrevious){};
      template<typename T>
      T GetPreviousState();
      template <>
      pdi::DislocationsDensityWall GetPreviousState<pdi::DislocationsDensityWall>();
    protected:
      boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivPrevious_;
    };

    // decodes double value identified with index into conrete type
    class IVStorageAdaptedToDislRandEquat
    {
    public:
      IVStorageAdaptedToDislRandEquat(boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivPrevious) : ivPrevious_(ivPrevious){};
      template<typename T>
      T GetPreviousState();
      template <>
      pdi::DislocationsDensityRandom GetPreviousState<pdi::DislocationsDensityRandom>();
    protected:
      boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivPrevious_;
    };

    ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////

    template<typename INPUT>
    std::vector<double> am3::adapter::IvManager<INPUT>::GetInternalVariablesNew() const
    {
      std::vector<double> toRet(4);
      toRet[VariableID<pdi::DislocationsDensityWall>::id] = bus_->GetFromDatabus<pdi::DislocationsDensityWall>(point_).Value();
      toRet[VariableID<pdi::DislocationsDensityRandom>::id] = bus_->GetFromDatabus<pdi::DislocationsDensityRandom>(point_).Value();
      toRet[VariableID<ce5::Alcrfemnsi_a_p0_Radius>::id] = bus_->GetFromDatabus<ce5::Alcrfemnsi_a_p0_Radius>(point_).Value();
      toRet[VariableID<ce5::Alcrfemnsi_a_p0_VolumeFraction>::id] = bus_->GetFromDatabus<ce5::Alcrfemnsi_a_p0_VolumeFraction>(point_).Value();
      return toRet;
    }

    //******************************************************************************************************************************************************************

    template <>
    pdi::DislocationsDensityWall IVStorageAdaptedToDislWalEquat::GetPreviousState()
    {
      pdi::DislocationsDensityWall toRet;
      if (ivPrevious_)  toRet(ivPrevious_->GetInternalVariable(VariableID<pdi::DislocationsDensityWall>::id));
      else lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Pointer to IV external storage not set");
      return toRet;
    }


    template <>
    pdi::DislocationsDensityRandom IVStorageAdaptedToDislRandEquat::GetPreviousState()
    {
      pdi::DislocationsDensityRandom toRet;
      if (ivPrevious_)  toRet(ivPrevious_->GetInternalVariable(VariableID<pdi::DislocationsDensityRandom>::id));
      else lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Pointer to IV external storage not set");
      return toRet;
    }

  } //adapter
} //am3

#endif //caseEsaform2015_ivmapping_h__