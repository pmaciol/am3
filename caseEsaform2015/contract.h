
/*! \file contract.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef esaform2015Contract_h__
#define esaform2015Contract_h__
/*	include *********************************************************/

#include "../models/macrosSimplifiedModels.h"
#include "../models/dataStructures.h"
#include "../modelPrecipitation/contracts.h"
#include "../modelDislocations/contracts.h"
#include "../softwareMatCalc/contracts.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace pdi = am3::problem::dislocations;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace esaform2015
    {
//       SIMPLE_PROP(Alcrfemnsi_a_p0_Radius, double);
//       SIMPLE_PROP(Alcrfemnsi_a_p0_VolumeFraction, double);

      using am3::software::matcalc::Alcrfemnsi_a_p0_Radius;
      using am3::software::matcalc::Alcrfemnsi_a_p0_VolumeFraction;

      
      CONTRACT(		MacroContract								, 5 , mpr::Coordinates2D, mpr::Temperature, mpr::EffectiveStrainRate, mpr::StepLength, mpr::Time, mpr::ElementNumber);
      CONTRACT(		StressContract							, 1 , mpr::Coordinates2D, mpr::Stress);
      CONTRACT(		PrecipitationsContract	  	, 2 , mpr::Coordinates2D, Alcrfemnsi_a_p0_Radius, Alcrfemnsi_a_p0_VolumeFraction);
      CONTRACT(		MatcalcInputContract	    	, 2 , mpr::Coordinates2D, mpr::Time, mpr::Temperature);
      CONTRACT(		StorageContract			        , 4 , mpr::Coordinates2D, pdi::DislocationsDensityWall, pdi::DislocationsDensityRandom,Alcrfemnsi_a_p0_Radius, Alcrfemnsi_a_p0_VolumeFraction);

      typedef moc::Contract<TYPELIST_1( EmptyType ),mpr::Coordinates2D> NoInput;
    }
  }
}
#endif // esaform2015Contract_h__
