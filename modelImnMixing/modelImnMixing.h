/********************************************************************
	author:		Piotr Maciol
  contact:  pmaciol@agh.edu.pl
*********************************************************************/

#ifndef adapter_h__
#define adapter_h__
namespace am3
{
  namespace adapter
  {
    class AdapterHydro
    {
    public:
      AdapterHydro();
      ~AdapterHydro();
      //coarse controller Interface
      int Init();
      int Run();
      //fine model interface
      void GetValue();
    protected:

    private:
    };
  }
}


#endif // adapter_h__
