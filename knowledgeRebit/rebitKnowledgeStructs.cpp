#include "rebitKnowledgeStructs.h"
#include <boost/lexical_cast.hpp>
#include <string>
#include "../common/literals.h"
#include "../logger/logger.h"

using std::string;
namespace lo=am3::logger;
using am3::gsoap::VariableDTO;

//VariableDTO * am3::knowledge::rebit::VariableProvider::GetVariableValue( std::string varId )
//{
//  VariableDTO *varDTO;
//
//  std::vector<VariableDTO>::size_type size = this->variableHolder->size();
//  for(int i = 0; i < size; i++)
//  {
//    varDTO = &(*this->variableHolder)[i];
//    if (*varDTO->Id == varId)
//      return varDTO;
//  }
//
//  return NULL;
//   VariableDTO *varDTO = new VariableDTO();
// 
//   if (varId.compare("month") == 0)
//   {
//     *varDTO->Id = "month";
//     varDTO->Value->push_back("January");
// 
//     return varDTO;
//   }
// 
//   if (varId.compare("day") == 0)
//   {
//     *varDTO->Id = "day";
//     varDTO->Value->push_back("Monday");
// 
//     return varDTO;
//   }
// 
//   if (varId.compare("time") == 0)
//   {
//     *varDTO->Id = "time";
//     varDTO->Value->push_back("11");
// 
//     return varDTO;
//   }
// 
//   return NULL;
//}

/*
HypothesisDTO * am3::knowledge::rebit::VariableProvider::GetHypothesis()
{
  HypothesisDTO *hypothesis = new HypothesisDTO();

  hypothesis->HypothesisOperator = new std::string("=");
  hypothesis->HypothesisVariable->Id = new std::string("thermostatSetting");
  hypothesis->HypothesisVariable->Value->push_back("24");

  return hypothesis;
}

std::vector<VariableDTO> * am3::knowledge::rebit::VariableProvider::GetSourceVariables()
{
  return this->variableHolder;
}

void am3::knowledge::rebit::VariableProvider::SetVariable(const VariableDTO & varDTO)
{
  this->variableHolder->push_back(varDTO);
}

am3::knowledge::rebit::VariableProvider::VariableProvider()
{
	this->variableHolder = new std::vector<VariableDTO>();
}
*/

bool am3::knowledge::rebit::SetVariableDTO(VariableDTO& variableToSet, const mpr::Strain2D& property )
{
  bool isSuccess=false;
  if (variableToSet.Id->compare(kStrain2D)==0)
  {
    std::vector<double> val;
    val.push_back(property.g11());
    val.push_back(property.g12());
    val.push_back(property.g21());
    val.push_back(property.g22());
    CopyVectorToVariable(variableToSet,val);
    isSuccess=true;
  } 
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    isSuccess=false;
  }
  return isSuccess;
}

bool am3::knowledge::rebit::SetVariableDTO(VariableDTO& variableToSet, const mpr::StrainRate2D& property )
{
  bool isSuccess=false;
  if (variableToSet.Id->compare(kStrainRate2D)==0)
  {
    std::vector<double> val;
    val.push_back(property.g11());
    val.push_back(property.g12());
    val.push_back(property.g21());
    val.push_back(property.g22());
    CopyVectorToVariable(variableToSet,val);
    isSuccess=true;
  } 
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    isSuccess=false;
  }
  return isSuccess;
}

bool am3::knowledge::rebit::SetVariableDTO(VariableDTO& variableToSet, const mpr::Temperature& property )
{
  bool isSuccess=false;
  if (variableToSet.Id->compare(kTemperature)==0)
  {
    std::vector<double> val;
    val.push_back(property.Value());
    CopyVectorToVariable(variableToSet,val);
    isSuccess=true;
  } 
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    isSuccess=false;
  }
  return isSuccess;
}

bool am3::knowledge::rebit::SetVariableDTO(VariableDTO& variableToSet, const mpr::DeltaTemperature& property )
{
	bool isSuccess=false;
	if (variableToSet.Id->compare(kDeltaTemperature)==0)
	{
		std::vector<double> val;
		val.push_back(property.Value());
		CopyVectorToVariable(variableToSet,val);
		isSuccess=true;
	} 
	else
	{
		lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
		isSuccess=false;
	}
	return isSuccess;
}

bool am3::knowledge::rebit::SetVariableDTO(VariableDTO& variableToSet, const mpr::TotalEffectiveStrain& property )
{
  bool isSuccess=false;
  if (variableToSet.Id->compare(kTotalEffStrain)==0)
  {
    std::vector<double> val;
    val.push_back(property.Value());
    CopyVectorToVariable(variableToSet,val);
    isSuccess=true;
  } 
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    isSuccess=false;
  }
  return isSuccess;
}

bool am3::knowledge::rebit::SetVariableDTO(VariableDTO& variableToSet, const mpr::Time& property )
{
  bool isSuccess=false;
  if (variableToSet.Id->compare(kTime)==0)
  {
    std::vector<double> val;
    val.push_back(property.Value());
    CopyVectorToVariable(variableToSet,val);
    isSuccess=true;
  } 
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    isSuccess=false;
  }
  return isSuccess;
}

bool am3::knowledge::rebit::SetVariableDTO(VariableDTO& variableToSet, const mpr::YieldStress& property )
{
  bool isSuccess=false;
  if (variableToSet.Id->compare(kYieldStress)==0)
  {
    std::vector<double> val;
    val.push_back(property.Value());
    CopyVectorToVariable(variableToSet,val);
    isSuccess=true;
  } 
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    isSuccess=false;
  }
  return isSuccess;
}

bool am3::knowledge::rebit::SetVariableDTO(VariableDTO& variableToSet, const mpr::ElementNumberGlobal& property )
{
  bool isSuccess=false;
  if (variableToSet.Id->compare(kGlobalElementNumber)==0)
  {
    std::vector<double> val;
    val.push_back(property.Value());
    CopyVectorToVariable(variableToSet,val);
    isSuccess=true;
  } 
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    isSuccess=false;
  }
  return isSuccess;
}

bool am3::knowledge::rebit::SetVariableDTO(VariableDTO& variableToSet, const mpr::ModelID& property )
{
	bool isSuccess=false;
	if ((variableToSet.Id->compare(kModelId)==0)||(variableToSet.Id->compare(kPreviousModelId)==0))
	{
		std::vector<double> val;
		val.push_back(property.Value());
		CopyVectorToVariable(variableToSet,val);
		isSuccess=true;
	} 
	else
	{
		lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
		isSuccess=false;
	}
	return isSuccess;
}

bool am3::knowledge::rebit::SetVariableDTO(VariableDTO& variableToSet, const mpr::Coordinates2D& property )
{
  bool isSuccess=false;
  if (variableToSet.Id->compare(kCoordinates2D)==0)
  {
    std::vector<double> val;
    val.push_back(property.x());
    val.push_back(property.y());
    CopyVectorToVariable(variableToSet,val);
    isSuccess=true;
  } 
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    isSuccess=false;
  }
  return isSuccess;
}

bool am3::knowledge::rebit::SetVariableDTO( VariableDTO& variableToSet, const mpr::Stress2D& property )
{
  bool isSuccess=false;
  if (variableToSet.Id->compare(kStress2D)==0)
  {
    std::vector<double> val;
    val.push_back(property.g11());
    val.push_back(property.g12());
    val.push_back(property.g21());
    val.push_back(property.g22());
    CopyVectorToVariable(variableToSet,val);
    isSuccess=true;
  } 
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    isSuccess=false;
  }
  return isSuccess;
}

boost::shared_ptr<pnt::ComputedPoint<mpr::Coordinates2D> > am3::knowledge::rebit::Point2DFromVariablDTO( const VariableDTO& coordinates, const VariableDTO& time )
{
  mpr::Coordinates2D coord;
  mpr::Time t;
  if ((coordinates.Id->compare(kCoordinates2D)==0)&&(time.Id->compare(kTime)==0))
  {
    coord.x(boost::lexical_cast<double>(coordinates.Value->at(0)));
    coord.y(boost::lexical_cast<double>(coordinates.Value->at(1)));	  
    t(boost::lexical_cast<double>(time.Value->at(0)));  
    boost::shared_ptr<pnt::ComputedPoint<mpr::Coordinates2D> > point(new pnt::ComputedPoint<mpr::Coordinates2D>(coord,t()));
    return point;
  } 
  else
  {
    boost::shared_ptr<pnt::ComputedPoint<mpr::Coordinates2D> > point;
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    return point;
  }
}

boost::shared_ptr<pnt::ComputedPoint<mpr::Coordinates3D> > am3::knowledge::rebit::Point3DFromVariablDTO( const VariableDTO& coordinates, const VariableDTO& time )
{
  mpr::Coordinates3D coord;
  mpr::Time t;
  if ((coordinates.Id->compare(kCoordinates3D)==0)&&(time.Id->compare(kTime)==0))
  {
    coord.x(boost::lexical_cast<double>(coordinates.Value->at(0)));
    coord.y(boost::lexical_cast<double>(coordinates.Value->at(1)));	  
    coord.z(boost::lexical_cast<double>(coordinates.Value->at(2)));	  
    t(boost::lexical_cast<double>(time.Value->at(0)));  
    boost::shared_ptr<pnt::ComputedPoint<mpr::Coordinates3D> > point(new pnt::ComputedPoint<mpr::Coordinates3D>(coord,t()));
    return point;
  } 
  else
  {
    boost::shared_ptr<pnt::ComputedPoint<mpr::Coordinates3D> > point;
    lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE,"Error in variables conversion - wrong VariableDTO Id");
    return point;
  }
}