
/*! \file mockRebitClient.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef mockRebitClient_h__
#define mockRebitClient_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "rebitKnowledgeStructs.h"
#include "../knowledge/kbsInterfaces.h"
#include "../kbsRebit/reasoningData.h"

/*	using ***********************************************************/

namespace kn=am3::knowledge;

/*	extern **********************************************************/



/*	classes *********************************************************/
namespace am3
{
  namespace knowledge
  {
    namespace rebit
    {
      class mockRebitClient: public RebitClient
      {
      public:
        mockRebitClient():tmp_(0),lastTime_(-1){};
        virtual int SetFact( const VariableDTO& prop );
        virtual int SetSource( VariableProvider* varProvider );
        virtual int GetVariableValue( VariableDTO & prop );
        void Init(boost::shared_ptr<kn::I4KBS<VariableDTO> > i4KBS)
        {
          i4KBS_=i4KBS;
        }
      protected:
        VariableProvider* varProvider_;
        boost::shared_ptr<kn::I4KBS<VariableDTO> > i4KBS_;
        int tmp_;
        double lastTime_;
      };
    }
  } //knowledge
} //am3
#endif // mockRebitClient_h__
