
/*! \file rebitKnowledgeStructs.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief  
 * \details   	
*********************************************************************/
#ifndef rebitKnowledgeStructs_h__
#define rebitKnowledgeStructs_h__
/*	include *********************************************************/

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include "../kbsRebit/reasoningData.h" //Tego headera trzeba przeniesc
#include "../models/point.h"
#include "../models/dataStructures.h"
/*	using ***********************************************************/

using std::string;
namespace pnt=am3::model::point;
namespace prop=am3::model::properties;
namespace mpr = am3::model::properties;

using am3::gsoap::VariableDTO;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace knowledge
  {
    namespace rebit
    {

      boost::shared_ptr<pnt::ComputedPoint<mpr::Coordinates> > Point3DFromVariablDTO(const VariableDTO& point, const VariableDTO& time);
      boost::shared_ptr<pnt::ComputedPoint<mpr::Coordinates2D> > Point2DFromVariablDTO(const VariableDTO& coordinates, const VariableDTO& time);
      
      template<typename TYPE>
      void CopyVectorToVariable(VariableDTO& variableToSet, const std::vector<TYPE>& valuesToSet);
      bool SetVariableDTO(VariableDTO& variableToSet, const mpr::Strain2D& property);
      bool SetVariableDTO(VariableDTO& variableToSet, const mpr::Stress2D& property);
      bool SetVariableDTO(VariableDTO& variableToSet, const mpr::StrainRate2D& property);
      bool SetVariableDTO(VariableDTO& variableToSet, const mpr::Temperature& property);
	    bool SetVariableDTO(VariableDTO& variableToSet, const mpr::DeltaTemperature& property);
      bool SetVariableDTO(VariableDTO& variableToSet, const mpr::TotalEffectiveStrain& property);
      bool SetVariableDTO(VariableDTO& variableToSet, const mpr::Coordinates2D& property);
      bool SetVariableDTO(VariableDTO& variableToSet, const mpr::Time& property);
      bool SetVariableDTO(VariableDTO& variableToSet, const mpr::YieldStress& property);
      bool SetVariableDTO(VariableDTO& variableToSet, const mpr::ElementNumberGlobal& property);
	    bool SetVariableDTO(VariableDTO& variableToSet, const mpr::ModelID& property);

      class VariableProvider //: public ReasoningDataProvider
      {
      public:
        VariableProvider();
        //virtual VariableDTO *GetVariableValue(string varId);
        //virtual HypothesisDTO *GetHypothesis();
        //virtual std::vector<VariableDTO> *GetSourceVariables();
        //virtual void SetVariable(const VariableDTO & varDTO);
      };

      class RebitClient
      {
      public:
        virtual int SetFact (const VariableDTO&  prop)=0;
        virtual int SetSource(VariableProvider* varProvider)=0;
        virtual int GetVariableValue(VariableDTO & prop)=0;
      };

      RebitClient* GetRebitClient(string filePath, string serviceAdress);

    } //rebit

    template<typename TYPE>
    void am3::knowledge::rebit::CopyVectorToVariable( VariableDTO& variableToSet, const std::vector<TYPE>& valuesToSet )
    {
      variableToSet.Value->clear();
      for (typename std::vector<TYPE>::const_iterator valuesIt=valuesToSet.begin();valuesIt<valuesToSet.end();valuesIt++)
      {
        variableToSet.Value->push_back(boost::lexical_cast<std::string>(*valuesIt));
      }
    }

  } //knowledge
} //am3

#endif // rebitKnowledgeStructs_h__
