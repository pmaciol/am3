#include <boost/lexical_cast.hpp>
#include "mockRebitClient.h"
#include "../common/literals.h"

int am3::knowledge::rebit::mockRebitClient::SetFact( const VariableDTO& prop )
{
  return -1;
}

int am3::knowledge::rebit::mockRebitClient::SetSource( VariableProvider* varProvider )
{
  varProvider_=varProvider;
  return 1;
}

int am3::knowledge::rebit::mockRebitClient::GetVariableValue( VariableDTO & prop )
{
  boost::shared_ptr<VariableDTO> property(new VariableDTO);
  property->Id->assign(am3::kGlobalElementNumber);
  i4KBS_->Fetch(property);
  boost::shared_ptr<VariableDTO> propertyTime(new VariableDTO);
  propertyTime->Id->assign(am3::kTime);
  i4KBS_->Fetch(propertyTime);
  boost::shared_ptr<VariableDTO> propertyDeltaTemp(new VariableDTO);
  propertyDeltaTemp->Id->assign(am3::kDeltaTemperature);
  i4KBS_->Fetch(propertyDeltaTemp);
  boost::shared_ptr<VariableDTO> propertyTemp(new VariableDTO);
  propertyTemp->Id->assign(am3::kTemperature);
  i4KBS_->Fetch(propertyTemp);
  boost::shared_ptr<VariableDTO> propertyPrevious(new VariableDTO);
  propertyPrevious->Id->assign(am3::kPreviousModelId);
  i4KBS_->Fetch(propertyPrevious);

  int isSuccess=-1;

  if (prop.Id->compare(am3::kModelId)==0)
  {
    prop.Value->clear();
    double presentTime=boost::lexical_cast<double>(*(propertyTime->Value->begin()));
	double dTemp=boost::lexical_cast<double>(*(propertyDeltaTemp->Value->begin()));
	double temp=boost::lexical_cast<double>(*(propertyTemp->Value->begin()));
	double time=boost::lexical_cast<double>(*(propertyTime->Value->begin()));
	int previousModelId=boost::lexical_cast<int>(*(propertyPrevious->Value->begin()));
	if ((previousModelId==1)||((dTemp>35.0)&&(temp>700)&&(time>0.01)))
    //if ((property->Value->begin()->compare("5")==0)&&(lastTime_!=presentTime))
    {
      prop.Value->push_back/*("0")*/("1");
	  //*prop.Value = "1";
      lastTime_=presentTime;
    }
    else
    {
      prop.Value->push_back("0");
	  //*prop.Value = "0";
    }
    isSuccess=1;
  }
  return isSuccess;
}