
/*! \file usr_uhard.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareAbaqus_usr_uhard_h__
#define am3softwareAbaqus_usr_uhard_h__
/*	include *********************************************************/



/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace abaqus
		{

			class AbaqusHandlerFacadeMtr;
			class AbaqusHandlerFacadeTime;
			class AbaqusHandlerFacadeElm;


			AbaqusHandlerFacadeMtr* GetAbaqusHandlerFacadeMtrPtr();
			AbaqusHandlerFacadeTime* GetAbaqusHandlerFacadeTimePtr();
			AbaqusHandlerFacadeElm*  GetAbaqusHandlerFacadeElmPtr();


			class AbaqusHandlerFacadeMtr
			{
			public:
				virtual double GetYieldStress(const int meshId, const int elementId) { return 0.0; };
				virtual double GetYieldStressDStrain(const int meshId, const int elementId) = 0;
				virtual double GetYieldStressDStrainRate(const int meshId, const int elementId) = 0;
				virtual int SetTemperature(const double* temperature, const int meshId, const int elementId) = 0;
				virtual int SetEffectiveStrainRate(const double* effStrainRate, const int meshId, const int elementId) = 0;
				virtual int SetEffectiveStrain(const double* effStrain, const int meshId, const int elementId) = 0;
			};

			//******************************************************************************************************************************************************************

			class AbaqusHandlerFacadeTime
			{
			public:
				virtual int SetStepLength(const double* timeStepLength, const int meshId, const int elementId) = 0;
				virtual int SetTime(const double* time, const int meshId, const int elementId) = 0;
			};

			//******************************************************************************************************************************************************************

			class AbaqusHandlerFacadeElm
			{
			public:
				virtual int SetElementNumber(const int meshId, const int elementId) = 0;
				virtual int SetElementalNodes(const int length, const double* values, const double time, const int meshId, const int elementId) = 0;
			};
		} //abaqus
	} //software
} //am3
#endif // am3softwareAbaqus_usr_uhard_h__
