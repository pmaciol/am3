
include_directories (../)

add_library(am3softwareAbaqus
usr_uhard.h usr_uhard.cpp
policyOneStepOneElement.h policyOneStepOneElement.cpp
)


SET_TARGET_PROPERTIES(am3softwareAbaqus PROPERTIES LINKER_LANGUAGE CXX)

target_link_libraries(am3softwareAbaqus 
)

