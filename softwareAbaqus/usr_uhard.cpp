
/*! \file usr_uhard.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "usr_uhard.h"
#include <boost/shared_ptr.hpp>
#include "../logger/logger.h"

/*	using ***********************************************************/

namespace lo = am3::logger;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace abaqus
		{

			boost::shared_ptr<AbaqusHandlerFacadeMtr> abaqusHandlerFacadeMtr;
			boost::shared_ptr<AbaqusHandlerFacadeTime> abaqusHandlerFacadeTime;
			boost::shared_ptr<AbaqusHandlerFacadeElm> abaqusHandlerFacadeElm;


			template<typename FACADE>
			bool CheckFacade(FACADE ptr, char* message)
			{
				if (ptr)	return true;
				else
				{
					lo::log<lo::SEV_CRITICAL>(lo::LOG_MODEL, message);
					return false;
				};
			};

			AbaqusHandlerFacadeMtr* am3::software::abaqus::GetAbaqusHandlerFacadeMtrPtr()
			{
				if (CheckFacade(abaqusHandlerFacadeMtr, "Abaqus Mtr handler not set")) return abaqusHandlerFacadeMtr.get();
				else return nullptr;
			}

			AbaqusHandlerFacadeTime* am3::software::abaqus::GetAbaqusHandlerFacadeTimePtr()
			{
				if (CheckFacade(abaqusHandlerFacadeMtr, "Abaqus Time handler not set")) return abaqusHandlerFacadeTime.get();
				else return nullptr;
			}

			AbaqusHandlerFacadeElm* am3::software::abaqus::GetAbaqusHandlerFacadeElmPtr()
			{
				if (CheckFacade(abaqusHandlerFacadeMtr, "Abaqus Elm handler not set")) return abaqusHandlerFacadeElm.get();
				else return nullptr;
			}

		} //abaqus
	} //software
} //am3
