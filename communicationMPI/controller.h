
/*! \file controller.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3communicationMPI_controller_h__
#define am3communicationMPI_controller_h__
/*	include *********************************************************/

#include <utility>
#include <map>
#include <mpi.h>
#include <exception>
#include "../models/namedPoint.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace common
	{
		namespace communication
		{
      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      class Controller
      {
      public:
        typedef INPUT_DATA InputData;
        typedef OUTPUT_DATA OutputData;
        typedef INSTANCE Instance;
        typedef POINT Point;
        typedef COORDINATES Coordinates;
        typedef std::pair<Point, InputData> PointWithInputDataPair;
        typedef std::pair<Point, OutputData> PointWithOutputDataPair;


        Controller();
        ~Controller();
        void Test(){};
        void Init(int argc, char **argv);
        void Init();
        void Run(const PointWithInputDataPair& pair);
        mpt::NamedPoint<mpt::NamedPointsContainer<mpt::NamedPointsStorageLastTwo<mpr::Coordinates2D> > > MakePoint(const unsigned int elemNumber, const mpr::Time& time);
        std::vector<POINT> GetPoints();
        
        void WaitForAll();
        PointWithOutputDataPair Get(const Point& point);
				PointWithInputDataPair GetInputs(const Point& point);
				bool IsPointAvailable(const Point& point);
        void Finalize();
      private:
        std::map<Coordinates, Instance> instances_;
      };

      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      mpt::NamedPoint<mpt::NamedPointsContainer<mpt::NamedPointsStorageLastTwo<mpr::Coordinates2D> > > am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::MakePoint(const unsigned int elemNumber, const mpr::Time& time)
      {
        lo::debugLog("Setting mock point for elemnNumber " + std::to_string(elemNumber));
        mpr::Coordinates2D coord(elemNumber, 0.0);
        return mpt::NamedPoint<mpt::NamedPointsContainer<mpt::NamedPointsStorageLastTwo<mpr::Coordinates2D> > >(elemNumber, coord, time);
      }

      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      void am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::Init()
      {
        int argc = 1;
        char text[] = "am3";
        char *charptr = text;
        Init(argc, &charptr);
      }

      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      void am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::Init(int argc, char **argv)
      {
        MPI_Init(&argc, &argv);
      }

      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////


			template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
			typename Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::PointWithInputDataPair
				am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::GetInputs(const Point& point)
			{
				auto found = instances_.find(point.Coordinates());
				if (found != instances_.end())
				{
					typename Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::PointWithInputDataPair toRet(point, /*IntputData()*/found->second.GetInputs());
					return toRet;
				}
				else
				{
					throw std::exception("Get method called for element not present in controller.");
				}
			}



			template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
			bool am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::IsPointAvailable(const Point& point)
			{
				auto found = instances_.find(point.Coordinates());
				return (found == instances_.end()) ? false : true;
			}



      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      std::vector<POINT> am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::GetPoints()
      {
        std::vector<POINT> toRet;
        for (auto& instance : instances_) {
          POINT tmpPoint(instance.second.GetPoint().get());
          toRet.push_back(tmpPoint);
        }
				return toRet;
      }


      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      void am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::WaitForAll()
      {
         int world_rank;
         MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
         lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Process " + std::to_string(world_rank) + " ask for finished processes"));
        for (auto& e : instances_)
        {
          e.second.CheckIsFinished();
        }
      }


      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      void am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::Finalize()
      {
        int size;
        MPI_Comm_size(MPI_COMM_WORLD, &size);
        for (int i = 1; i < size;i++)
        {
          int toSend = 0;
          lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending no-go flag to process " + std::to_string(i)));
          MPI_Send(&toSend, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
        }
        lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("All instances finalized"));
        MPI_Finalize();
        lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Main process finalized"));
      }



      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      typename Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::PointWithOutputDataPair 
        am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::Get(const Point& point)
      {
        auto found = instances_.find(point.Coordinates());
        if (found != instances_.end())
        {
          PointWithOutputDataPair toRet(point, /*OutputData()*/found->second.Get());
          return toRet;
        }
        else
        {
          throw std::exception("Get method called for element not present in controller.");
        }
      }


      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      void am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::Run(const PointWithInputDataPair& pair)
      {
        auto found = instances_.find(pair.first.Coordinates());
        if (found != instances_.end())
        {
          found->second.Run(pair);
        }
        else
        {
          std::pair<Coordinates, Instance> mapPair(pair.first.Coordinates(), Instance());
          instances_.insert(mapPair).first->second.Run(pair);
        }
      }


      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::Controller()
      {
       /* MPI_Init(nullptr, nullptr);*/
      }

      template<typename INPUT_DATA, typename OUTPUT_DATA, typename INSTANCE, typename POINT, typename COORDINATES>
      am3::common::communication::Controller<INPUT_DATA, OUTPUT_DATA, INSTANCE, POINT, COORDINATES>::~Controller()
      {
       /* MPI_Finalize();*/
      }

    } //communication
	} //common
} //am3
#endif // am3communicationMPI_controller_h__
