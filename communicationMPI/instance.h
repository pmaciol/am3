
/*! \file instance.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3communicationMPI_instance_h__
#define am3communicationMPI_instance_h__
/*	include *********************************************************/

#include <mpi.h>
#include <string>
#include "../models/dataStructures.h"
#include "../models/predefinedContracts.h"
#include "../logger/logger.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mda = am3::model::datapacks;
namespace lo = am3::logger;

/*	extern **********************************************************/



/*	classes *********************************************************/


inline void VectorToOutput(std::vector<double>& input, double& output)
{
  lo::debugLog("Vector of doubles to double conversion!");
  output = input[0];
}


namespace am3
{
  namespace common
  {
    namespace communication
    {

      void MpiSendData(const unsigned processId, const mda::Coord2dTimeTemp& toSend);
      void MpiSendData(const unsigned processId, const mda::Coord2dTimeTempVect& toSend);
      void MpiSendData(const unsigned processId, const int& toSend);


      
      void MpiSendFlag(const unsigned processId, int & flag);
      


template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      class MpiInstance
      {
      public:
        typedef INPUT_DATA InputData;
        typedef OUTPUT_DATA OutputData;
        //      typedef INSTANCE Instance;
        typedef POINT Point;
        typedef std::pair<Point, InputData> Pair;

        MpiInstance();
        void Run(const Pair& pair);
        bool CheckIsFinished();
        void Finalize();
        OutputData Get();
				InputData GetInputs();
        boost::shared_ptr<Point> GetPoint();

      protected:
        bool VerifyPoint(const Point& point) const;
        bool IsNewTime(const mpr::Time& time) const;
        void Calculate(const mpr::Time& time, const InputData& data);

      private:
        mpr::Time time_;
        boost::shared_ptr<Point> point_;
				InputData inputData_;
        unsigned processId_;
        static unsigned lastProcessId_;

        bool waitingForResponse_;

        void MpiSendContinue(const unsigned processId);
        void MpiSendFlag(const unsigned processId, int & flag);
      };

      
      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////


			template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
			INPUT_DATA am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::GetInputs()
			{
				return inputData_;
			}
			
			template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      boost::shared_ptr<POINT> am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::GetPoint()
      {
        return point_;
      }


      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      OUTPUT_DATA am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::Get()
      {
        OutputData toRet;
        int toSend = 3;
        lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending results request " + std::to_string(processId_)));
        MPI_Send(&toSend, 1, MPI_INT, processId_, 0, MPI_COMM_WORLD);
        int howManyVariables;
        MPI_Recv(&howManyVariables, 1, MPI_INT, processId_, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Variables to get = " + std::to_string(howManyVariables)));
        
        std::vector<double> transmittedVariables;
        double tempVar;
        lo::debugLog("Receiving values...");
        for (int i = 0; i < howManyVariables; i++)
        {
          MPI_Recv(&tempVar, 1, MPI_DOUBLE, processId_, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
          lo::debugLog("Received value = " + std::to_string(tempVar*1e7));
          transmittedVariables.push_back(tempVar);

        }
        VectorToOutput(transmittedVariables, toRet);
        return toRet;
      }

      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      bool am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::CheckIsFinished()
      {
        int toSend = 2;
        lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending REPORT flag to process " + std::to_string(processId_)));
        MPI_Send(&toSend, 1, MPI_INT, processId_, 0, MPI_COMM_WORLD);
        MPI_Recv(&toSend, 1, MPI_INT, processId_, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("REPORT flag from process " + std::to_string(processId_) + " confirmed"));
        return true;

      }



      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      void am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::Finalize()
      {
        int toSend = 0;
        lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending no-go flag to process " + std::to_string(processId_)));
        MPI_Send(&toSend, 1, MPI_INT, processId_, 0, MPI_COMM_WORLD);
      }

      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      void am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::MpiSendContinue(const unsigned processId)
      {
        int toSend = 1;
        lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending go flag to process " + std::to_string(processId)));
        MPI_Send(&toSend, 1, MPI_INT, processId, 0, MPI_COMM_WORLD);
      }

      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      void am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::MpiSendFlag(const unsigned processId, int & flag)
      {
        lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending flag " + std::to_string(flag) + " to process " + std::to_string(processId)));
        MPI_Send(&flag, 1, MPI_INT, processId, 0, MPI_COMM_WORLD);
      }

      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      void am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::Calculate(const mpr::Time& time, const InputData& data)
      {
        //MpiSendContinue(processId_);
        MpiSendData(processId_, data);
      }


      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      unsigned am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::lastProcessId_ = 0;

      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      bool am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::VerifyPoint(const Point& point) const
      {
        bool toRet = false;
        if (point_ != nullptr)
        {
          if (point_->SamePoint(point))
          {
            toRet = true;
          }
        }
        return toRet;

      }

      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      bool am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::IsNewTime(const mpr::Time& time) const
      {
        if (point_->GetTime()< time())
        {
          return true;
        } 
        else
        {
          return false;
        }
      }


      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::MpiInstance() :waitingForResponse_(false)
      {
        processId_ = ++lastProcessId_;
        std::cout << "Initializing new Instance" << std::endl;
      }


      template<typename INPUT_DATA, typename OUTPUT_DATA, /*, typename INSTANCE, */typename POINT>
      void am3::common::communication::MpiInstance<INPUT_DATA, OUTPUT_DATA, POINT>::Run(const Pair& pair)
      {
				inputData_ = pair.second;
        if (point_ == nullptr) 
        {
          mpr::Time time = pair.first.GetTime();
          point_ = boost::shared_ptr<Point>(new Point(pair.first));
          waitingForResponse_ = true;
          Calculate(time, pair.second);
        }
        else if (VerifyPoint(pair.first))
        {
          mpr::Time time = pair.first.GetTime();
          if (IsNewTime(time))
          {
            waitingForResponse_ = true;
            Calculate(time, pair.second);
          } 
        } 
        else
        {
          std::exception("Wrong point for Instance!");
        }
      }

    } //communication
  } //common
} //am3

#endif // am3communicationMPI_instance_h__
