/*! \file instance.cpp **************************************************
* \author		Piotr Maciol
* \copyright 	GNU Public License.
* \brief       
* \details   	
*********************************************************************/
/*	include *********************************************************/
      
#include <mpi.h>
#include "instance.h"
      
/*	using ***********************************************************/
      
      
      
/*	extern **********************************************************/
      
      
      
/*	classes *********************************************************/
      
void am3::common::communication::MpiSendData(const unsigned processId, const mda::Coord2dTimeTemp& toSend)
{
  int flag = 1;
  MpiSendFlag(processId, flag);
  double coordinates[2];
  coordinates[0] = std::get<0>(toSend).x();
  coordinates[1] = std::get<0>(toSend).y();
  double time = std::get<1>(toSend)();
  double temperature = std::get<2>(toSend)();
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending coords to process " + std::to_string(processId)));
  MPI_Send(coordinates, 2, MPI_DOUBLE, processId, 0, MPI_COMM_WORLD);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending time to process " + std::to_string(processId)));
  MPI_Send(&time, 1, MPI_DOUBLE, processId, 0, MPI_COMM_WORLD);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending temperature to process " + std::to_string(processId)));
  MPI_Send(&temperature, 1, MPI_DOUBLE, processId, 0, MPI_COMM_WORLD);
}

void am3::common::communication::MpiSendFlag(const unsigned processId, int & flag)
{
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending flag " + std::to_string(flag) + " to process " + std::to_string(processId)));
  MPI_Send(&flag, 1, MPI_INT, processId, 0, MPI_COMM_WORLD);
}

void am3::common::communication::MpiSendData(const unsigned processId, const mda::Coord2dTimeTempVect& toSend)
{
  int flag = 1;
  MpiSendFlag(processId, flag);
  double coordinates[2];
  coordinates[0] = std::get<0>(toSend).x();
  coordinates[1] = std::get<0>(toSend).y();
  double time = std::get<1>(toSend)();
  double temperature = std::get<2>(toSend)();
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending coords to process " + std::to_string(processId)));
  MPI_Send(coordinates, 2, MPI_DOUBLE, processId, 0, MPI_COMM_WORLD);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending time to process " + std::to_string(processId)));
  MPI_Send(&time, 1, MPI_DOUBLE, processId, 0, MPI_COMM_WORLD);
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending temperature to process " + std::to_string(processId)));
  MPI_Send(&temperature, 1, MPI_DOUBLE, processId, 0, MPI_COMM_WORLD);
  flag = 4;
  MpiSendFlag(processId, flag);
  int howManyVariables = std::get<3>(toSend).size();
  lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending " + std::to_string(howManyVariables) + " variables"));
  MPI_Send(&howManyVariables, 1, MPI_INT, processId, 0, MPI_COMM_WORLD);
  for (const double &var : std::get<3>(toSend))
  {
    double tempVar = 0;
    tempVar = var;
    lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, ("Sending " + std::to_string(tempVar)));
    std::cout << tempVar << std::endl;
    MPI_Send(&tempVar, 1, MPI_DOUBLE, processId, 0, MPI_COMM_WORLD);
  }
}

void am3::common::communication::MpiSendData(const unsigned processId, const int& toSend)
{
  int flag = 1;
  MpiSendFlag(processId, flag);
  MPI_Send(&toSend, 1, MPI_INT, processId, 0, MPI_COMM_WORLD);
}

