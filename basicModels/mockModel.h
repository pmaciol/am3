
/*! \file mockModel.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef mockModel_h__
#define mockModel_h__
/*	include *********************************************************/

#include <vector>
#include "../adapter/adapter.h"
#include "../models/point.h"
#include "../basicModels/rebitInterface.h"
#include "../models/contracts.h"
#include "../knowledge/basicKbsInterfaces.h"

/*	using ***********************************************************/

using std::vector;
using am3::model::Model;
namespace pro=am3::model;
namespace mod=am3::model;
namespace com=am3::model::common;
namespace def=am3::model::contract::defined;
namespace ada=am3::adapter;
namespace reb=am3::knowledge::rebit;
namespace kno=am3::knowledge;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace mock
    {
  /**
  * \brief
  * All classes, which should be implemented for new multiscale model
  *
  * This example includes simple multiscale model. It should be used as a ''template'' for new models.
  * Main project for this example is am3prototype.
  */
  /** \addtogroup Example1*/
  /*@{*/
  class MockAdapter;

  //! Mock adapter proxy class. Used for tests. Using MockAdapter and Mock KBS classes
   class MockAdapterProxy
     : public ada::AdapterProxy<reb::MockRebitKBS,MockAdapter,kno::ConverterGeneralRebit<MockAdapter> >
     , public kno::I4Adapter<>
   {
   public:
     ~MockAdapterProxy();
     MockAdapterProxy( MockAdapter* adp,reb::MockRebitKBS* kbs);
     //! Proxy function - call to am3::adapter::AdapterProxy
     mod::ModelID GetState(com::Point<> *,const pro::ModelID *const );
   };
  
  //! Mock class for tests. Viscosity = 1.0
  class MockVisc1
    : public Model
    , public def::ViscProvider
  {
  public:
    virtual ~MockVisc1(){};
    //! \name Realizes am3::model::contract::Provider (s) interface
    //@{
    virtual pro::Viscosity GetState(com::Point* point,const pro::Viscosity* const);
    //@}
    //! \name Realizes am3::model::Model interface
    //@{
    virtual int Init();
    virtual int Run();
    virtual int PostProcess();
    //@}
    //! Initialize adapter pointer
    virtual int Init(MockAdapter* myOwner);
    virtual ViscProvider* clone() const;
  protected:
    //! Pointer to adpater
    MockAdapter* myOwner_;
  };

  //! Mock class for tests. Viscosity = 2.0
  class MockVisc2 :public MockVisc1
  {
  public:
    virtual ~MockVisc2(){};
    //! \name Realizes am3::model::contract::Provider (s) interface
    //@{
    virtual pro::Viscosity GetState(com::Point* point,const pro::Viscosity* const);
    //@}
  };

  /** 
  * \brief Mock model class. Used for tests
  *
  * Realizes interfaces am3::model::Model and am3::model::contract::Provider (s)
  *  
  */
  class MockModel 
    : public Model
    , public def::VCTProvider
  {
  public:
    MockModel();
    //! \name Realizes am3::model::contract::Provider (s) interface
    //@{
    virtual pro::VelocityWithDerivatives GetState(com::Point* point,const pro::VelocityWithDerivatives* const);
    virtual pro::Coordinates GetState(com::Point* point,const pro::Coordinates* const);
    virtual pro::Time GetState(com::Point* point,const pro::Time* const);
    //@}
    //! \name Realizes am3::model::Model interface
    //@{
    virtual int Init();
    virtual int Run();
    virtual int PostProcess();
    //@}
    //! Initialize adapter pointer
    virtual int Init(MockAdapter* myOwner);
  protected:
    //! Pointer to adpater
    MockAdapter* myOwner_;
    //! \name Internal Properties - used for GetState methods
    //@{
    pro::VelocityWithDerivatives v;
    pro::Coordinates c;
    pro::Time t;
    //@}
  };

  /** 
  * \brief Mock adapter class. Used for tests. Using MockModel class
  *
  * Realizes interfaces am3::model::contract::Model (which is part of am3::model::Adapter) 
  * and public am3::model::contract::ContractItemDispatcher (s)
  */
  class MockAdapter
    : public ada::Adapter
    , public def::VCTContract
    , public def::ViscContract
  {
  protected:
    /**
    * \brief Specialized model pointer. 
    *
    * am3::adapter::Adapter::fine_scale_model_ pointer still availible. Could be used for virtual functions
    **/
    MockModel *macro_model_specialized;
    vector<def::ViscProvider*> micro_models_;
  public:
    using ada::Adapter::Init;
    typedef def::VCTContract Contract;
    MockAdapter();
    virtual ~MockAdapter();
    //! \name Realizes am3::model::contract::ContractItemDispatcher (s) interface
    //@{
    virtual pro::VelocityWithDerivatives GetState(com::Point* point,const pro::VelocityWithDerivatives* const p);
    virtual pro::Coordinates GetState(com::Point* point,const pro::Coordinates* const p);
    virtual pro::Time GetState(com::Point* point,const pro::Time* const p);
    virtual pro::Viscosity GetState(com::Point* point,const pro::Viscosity* const p);
    //@}
    //! \name Realizes am3::model::Model interface
    //@{
    virtual int Init();
    virtual int Run();
    virtual int PostProcess();
    //@}

  };
  /*@}*/
      } //mock
    } //model
  } //am3

#endif