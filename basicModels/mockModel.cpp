#include <boost/lexical_cast.hpp>
#include "mockModel.h"
#include "../logger/logger.h"

using am3::logger::TestingMsg;


pro::VelocityWithDerivatives am3::model::mock::MockModel::GetState( am3::model::common::Point* point,const pro::VelocityWithDerivatives* const )
{
  TestingMsg("MockModel calculates velocity");
  return v;
}

pro::Coordinates am3::model::mock::MockModel::GetState( am3::model::common::Point* point,const pro::Coordinates* const )
{
  TestingMsg("MockModel calculates coordinates");
  return c;
}

pro::Time am3::model::mock::MockModel::GetState( am3::model::common::Point* point,const pro::Time* const )
{
  TestingMsg("MockModel calculates time");
  return t;
}

int am3::model::mock::MockModel::Init()
{
  TestingMsg("MockModel initialization, nothing to do");
  return 1;
}

int am3::model::mock::MockModel::Init( MockAdapter* myOwner )
{
  am3::logger::Starting("MockModel initialization");
  TestingMsg("Setting owner");
  myOwner_=myOwner;
  am3::logger::Stopping("MockModel initialization");
  return Init();
}

int am3::model::mock::MockModel::Run()
{
  am3::logger::Starting("MockModel running");
  am3::model::common::ComputedPoint cp;
  am3::model::Coordinates coord;
  pro::Viscosity v;
  double x=0.0,dx=0.1;
  // Ask adapter for property (viscosity)
  for (int i=0;i<10;i++)
  {
    coord.x(x);
    cp.Coordinates(coord);
    x+=dx;
    myOwner_->GetState(&cp,&v);
  }
  am3::logger::Stopping("MockModel running");
  return 1;
}

int am3::model::mock::MockModel::PostProcess()
{
  TestingMsg("MockModel PostProcess. Nothing to do");
  return 1;
}

am3::model::mock::MockModel::MockModel()
{
  // Initialization of internal properties. Just for use in example
  t(0.0);
  c.x(0.1);
  c.y(0.2);
  c.z(0.3);
  v.x.dx(1);
  v.x.dy(1);
  v.x.dz(1);
  v.y.dx(1);
  v.y.dy(1);
  v.y.dz(1);
  v.z.dx(1);
  v.z.dy(1);
  v.z.dz(1);
  v.x(2);
  v.y(2);
  v.z(3);
}

int am3::model::mock::MockAdapter::Run()
{
  am3::logger::Starting("MockAdapter running");
  macro_model_specialized->Run();
  am3::logger::Stopping("MockAdapter running");
  return 1;
}

am3::model::mock::MockAdapter::MockAdapter()
{
  macro_model_specialized=new MockModel();
  macro_model_specialized->Init(this);
  Adapter::fine_scale_model_=macro_model_specialized;

  micro_models_.push_back(new MockVisc1);
  micro_models_.push_back(new MockVisc2);

  ContractItemDispatcher<pro::VelocityWithDerivatives>::SetCalculator(macro_model_specialized);
  ContractItemDispatcher<pro::Coordinates>::SetCalculator(macro_model_specialized);
  ContractItemDispatcher<pro::Time>::SetCalculator(macro_model_specialized);
  ContractItemDispatcher<pro::Viscosity>::SetCalculator(micro_models_[0]);
}

int am3::model::mock::MockAdapter::Init()
{
  am3::logger::Starting("MockAdapter initialization");
  macro_model_specialized->Init(this);
  am3::logger::Stopping("MockAdapter initialization");
  return 1;
}

int am3::model::mock::MockAdapter::PostProcess()
{
  am3::logger::Starting("MockAdapter PostProcess");
  macro_model_specialized->PostProcess();
  am3::logger::Stopping("MockAdapter PostProcess");
  return 1;
}

am3::model::mock::MockAdapter::~MockAdapter()
{
  if (macro_model_specialized!=nullptr) 
  {
    delete macro_model_specialized;
    Adapter::fine_scale_model_=nullptr;
  }
  delete micro_models_[0];
  delete micro_models_[1];
  //delete proxy;
}

pro::VelocityWithDerivatives am3::model::mock::MockAdapter::GetState( am3::model::common::Point* point,const pro::VelocityWithDerivatives* const p )
{
  TestingMsg("MockAdapter calls for velocity");
  return macro_model_specialized->GetState(point,p);
}

pro::Coordinates am3::model::mock::MockAdapter::GetState( am3::model::common::Point* point,const pro::Coordinates* const p )
{
  TestingMsg("MockAdapter calls for coordinates");
  return macro_model_specialized->GetState(point,p);
}

pro::Time am3::model::mock::MockAdapter::GetState( am3::model::common::Point* point,const pro::Time* const p )
{
  TestingMsg("MockAdapter calls for time");
  return macro_model_specialized->GetState(point,p);
}

pro::Viscosity am3::model::mock::MockAdapter::GetState( am3::model::common::Point* point,const pro::Viscosity* const visc )
{
  am3::logger::Starting("MockAdapter GetState Viscosity");
  pro::Viscosity v;
  am3::model::Coordinates coord;
  coord=point->Coordinates();
  am3::model::ModelID mId=this->a4Adapter_->GetState(point,&mId);
  //Code below is very simple. In real cases, add new method for switchning between micro models
  v= micro_models_[mId()]->GetState(point,visc);
  am3::logger::TestingMsg(boost::lexical_cast<std::string>(v()).c_str());
  am3::logger::Stopping("MockAdapter GetState Viscosity");
  return v;
}

pro::Viscosity am3::model::mock::MockVisc1::GetState( am3::model::common::Point* point,const pro::Viscosity* const )
{
  pro::Viscosity v;
  v(1.0);
  return v;
}

int am3::model::mock::MockVisc1::Init()
{
  am3::logger::Starting("MockModel initialization");
  am3::logger::TestingMsg("Nothing to do");
  am3::logger::Stopping("MockModel initialization");
  return 1;
}

int am3::model::mock::MockVisc1::Init( MockAdapter* myOwner )
{
  myOwner_=myOwner;
  return Init();
}

int am3::model::mock::MockVisc1::Run()
{
  am3::logger::Starting("MockModel Run");
  am3::logger::TestingMsg("Nothing to do");
  am3::logger::Stopping("MockModel Run");
  return 1;
}

int am3::model::mock::MockVisc1::PostProcess()
{
  am3::logger::Starting("MockModel PostProcess");
  am3::logger::TestingMsg("Nothing to do");
  am3::logger::Stopping("MockModel PostProcess");
  return 1;
}

def::ViscProvider* am3::model::mock::MockVisc1::clone() const
{
  am3::model::mock::MockVisc1* toReturn=new am3::model::mock::MockVisc1;
  toReturn->Init(this->myOwner_);
  return toReturn;
}

pro::Viscosity am3::model::mock::MockVisc2::GetState( am3::model::common::Point* point,const pro::Viscosity* const )
{
  pro::Viscosity v;
  v(2.0);
  return v;
}

am3::model::mock::MockAdapterProxy::~MockAdapterProxy()
{
  if (i4KBS!=nullptr) delete i4KBS;
}

am3::model::ModelID am3::model::mock::MockAdapterProxy::GetState( am3::model::common::Point * point,const am3::model::ModelID *const mId  )
{
  return ada::AdapterProxy<reb::MockRebitKBS,MockAdapter,kno::ConverterGeneralRebit<MockAdapter> >::GetState(point, mId);
}

am3::model::mock::MockAdapterProxy::MockAdapterProxy( MockAdapter* adp,am3::knowledge::rebit::MockRebitKBS* kbs ) : am3::adapter::AdapterProxy<am3::knowledge::rebit::MockRebitKBS,MockAdapter,am3::knowledge::ConverterGeneralRebit<MockAdapter> >::AdapterProxy(adp,kbs)
{
//   i4KBS=new am3::knowledge::ConverterGeneralRebit;
//   kbs->Init(i4KBS);
  adp->Init(this);
}
