// 
// /*! \file basicModels.h **************************************************
//  * \author		Piotr Maciol
//  * \copyright 	GNU Public License.
//  * \brief       
//  * \details   	
// *********************************************************************/
// #ifndef basicModels_h__
// #define basicModels_h__
// /*	include *********************************************************/
// 
// #include "../models/dataStructures.h"
// #include "../models/calculate.h"
// #include "../models/models.h"
// #include "../models/description.h"
// 
// /*	using ***********************************************************/
// /*	extern **********************************************************/
// /*	classes *********************************************************/
// namespace am3
// {
//   namespace model
//   {
// 
//     //!Simple viscosity/velocity model for tests
//     template<typename T_INPUT>
//     class TestViscosity123_54//:public BaseModel<T_INPUT,Viscosity>
//     {
//     public:
//  //     double  Value() const { return value; } //obsolete
//       Viscosity GetState(T_INPUT) const 
//         { 
//           Viscosity pv;
//           pv(value_);
//           return pv;
//         };
//       TestViscosity123_54(){value_=123.54;}
//     protected:
//       double value_;
//     };
// 
//     /** @name Predefined am3::model::Calulation specializations*/
//     ///@{
// 
//     /// Viscosity with Velocities and its Derivatives on input
//     typedef Calculation<
//       Viscosity, 
//       VelocityWithDerivatives,
//       TestViscosity123_54<VelocityWithDerivatives> > 
//       ViscosityWithVelocity;
// 
// 
//     ///@}
//   } //model
// } //am3
// 
// #endif // basicModels_h__
