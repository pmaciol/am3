#include "rebitInterface.h"
#include "../logger/logger.h"

namespace lo=am3::logger;


void am3::knowledge::rebit::MockRebitKBS::Init(kno::I4KBS<VariableDTO>* provider)
//void am3::knowledge::rebit::MockRebitKBS::Init( am3::knowledge::I4KBS<am3::knowledge::StringPropertyDescriptor>* provider )
{
  provider_=provider;
  rClient_=GetRebitClient("C:\\thermostat.xml","http://localhost:9878/ReasoningService");
  rClient_->SetSource(&vProv_);
}

//bool am3::knowledge::rebit::MockRebitKBS::Get( am3::knowledge::StringPropertyDescriptor* property )
bool am3::knowledge::rebit::MockRebitKBS::Get( VariableDTO* property )
{
  // Obsolete, should be remove
  bool isSuccess=false;
  if ( property->Id->compare("modelId")==0)
  {
    //Tu ma pyta� REBITA!!!!!!
    lo::TestingMsg("Asking (mock)Rebit about modelId");

    rClient_->GetVariableValue(*property);

    property->Value->push_back("1");
    isSuccess=true;
  } 
  return isSuccess;
}

bool am3::knowledge::rebit::MockRebitKBS::Get(VariableDTO* mId, const VariableDTO& typeTrait)
{
  bool isSuccess=false;
  if ( mId->Id->compare("modelId")==0)
  {
    //Tu ma pyta� REBITA, !!!!!!
    lo::TestingMsg("Asking (mock)Rebit about modelId - NOT IMPLEMENTED!!!!");

    if (rClient_->GetVariableValue(*mId)>=0) isSuccess=true;
// 
//     mId->Value->push_back("0");   
  } 
  return isSuccess;
}

am3::knowledge::rebit::MockRebitKBS::~MockRebitKBS()
{
  if (provider_!=nullptr) delete provider_;
  if (rClient_!=nullptr) delete rClient_;
}
