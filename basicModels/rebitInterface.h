/*! \file util.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef knowledgeRebit_util_h__
#define knowledgeRebit_util_h__
/*	include *********************************************************/

#include "../knowledge/kbsInterfaces.h"
#include "../knowledgeRebit/rebitKnowledgeStructs.h"


/*	using ***********************************************************/

namespace kno=am3::knowledge;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace knowledge
  {
    //! All stuff connected to REBIT system
    namespace rebit
    {
      /** \addtogroup Example1*/
      /**@{*/
      //! Replace real Rebit interface. Used for tests and as an example
      struct MockRebitKBS: public am3::knowledge::rebit::RebitClient
      {
        typedef VariableDTO Property;
        //void Init(kno::I4KBS<kno::StringPropertyDescriptor>* provider);
        void Init(kno::I4KBS<VariableDTO>* provider);
        //! Interface for askinf about property value 
        //! \todo Not finished yet
        //bool Get(kno::StringPropertyDescriptor* property);
        bool Get(VariableDTO* property);
        bool Get(VariableDTO* mId, const VariableDTO& typeTrait);
        ~MockRebitKBS();
      protected:
        //! Provides Getters to possible model properties
        //kno::I4KBS<kno::StringPropertyDescriptor>* provider_;
        kno::I4KBS<VariableDTO>* provider_;
        RebitClient* rClient_;
        VariableProvider vProv_;
      };
      /**@}*/
    } //rebit  
  } //knowledge
} //am3

#endif // knowledgeRebit_util_h__
