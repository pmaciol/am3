
/*! \file dislocationsModel.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef dislocationsModel_h__
#define dislocationsModel_h__
/*	include *********************************************************/

#include "../models/models.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "caseVectorTUG.h"

/*	using ***********************************************************/

namespace mo = am3::model;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace vectortug
    {
      template <typename STORAGE>
      class DislocationModelWithStorage
        : public DislocationStateContract
        , public mo::Inputs<DislocationModelInputs>
        , mo::ComputationsOnFirstCall<mpr::Coordinates2D>
      {
      public: 
        DislocationModelWithStorage(boost::shared_ptr<STORAGE> storage):storage_(storage) {};
        ~DislocationModelWithStorage(){};

        DislocationState GetState(mpt::Point<mpr::Coordinates2D> * point,const DislocationState *const);

        virtual void GetData(pnt::Point<mpr::Coordinates2D> * point );

        virtual void Calculate( pnt::Point<mpr::Coordinates2D> * point );

      protected:
        const boost::shared_ptr<STORAGE> storage_;
        std::vector<double> returnedVector_;
        std::vector<double> previousStep_;
        std::vector<double> macroscopicState_;
        boost::shared_ptr<pnt::Point<mpr::Coordinates2D> > actualPoint;
      };

////////////////////////////////////////////////////////////////////////// Implementation //////////////////////////////////////////////
     
      template <typename STORAGE>
      DislocationState DislocationModelWithStorage<STORAGE>::GetState( mpt::Point<mpr::Coordinates2D> * point,const DislocationState *const )
      {
        CheckOrGetDataInPoint(point);
        DislocationState toRet;
        toRet(returnedVector_);
        return toRet;
      }

      template <typename STORAGE>
      void DislocationModelWithStorage<STORAGE>::GetData( pnt::Point<mpr::Coordinates2D> * point )
      {
        actualPoint =  point->Copy();
        DislocationState tmpPreviousStep_=storage_->GetPreviousState<DislocationState>();
        previousStep_.insert(previousStep_.begin(),tmpPreviousStep_().begin(),tmpPreviousStep_().end());
        std::vector<double> tmpVec = Get<MacroscopicState,BaseCoordinates>::InPoint(point).Value();
        macroscopicState_.insert(macroscopicState_.begin(),tmpVec.begin(),tmpVec.end());
      }

      // GetState methods implementation in separate file
      #include "dislocationsModelCalculate.h"

      
    } //vectortug
  } //cases
} //am3
#endif // dislocationsModel_h__
