
/*! \file caseVectorTUG.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseVectorTUG_h__
#define caseVectorTUG_h__
/*	include *********************************************************/

#include "contracts.h"
#include "deformAdapter.h"
#include "../models/models.h"

/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/

// Global access for external (Deform) calls
extern am3::software::deform::DeformHandlerUserProc* deformHandlerProc;

namespace am3
{
	namespace cases
	{
		namespace vectortug
		{
      // Defines databus for this specific case
      DATABUS(CaseVectorTUGDatabus,Storage,
      MacroscopicStateContract,
      PrecipitationStateContract, 
      DislocationStateContract, 
      DeformStressContractRequest);

		  // This case is used as a dll library. Constructor of Main class works like a "main" procedure in single executable solution
      // Implementation in cpp file (probably caseVactorTUG.cpp)
			class Main : public am3::model::InitializableMainModel
			{
			public:
				Main();
        void Init(){};
        bool IsInitialized() const {return true;};
			}; 
		} //vectortug
	} //cases
} //am3
#endif // caseVectorTUG_h__
