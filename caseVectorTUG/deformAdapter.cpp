#include "deformAdapter.h"


namespace am3
{
  namespace cases
  {
    namespace vectortug
    {


// Copies Dislocations and Precipitation states to deform internal variables vector ...
std::vector<double> Storage::GetInternalVariables( mpt::Point<mpr::Coordinates2D> *point )
{
  std::vector<double> toRet; 
  std::vector<double> toRet2; 
  std::vector<double> toRet3; 
  toRet2 = Get<am3::cases::vectortug::DislocationState,mpr::Coordinates2D>::InPoint(point).Value();
  toRet3 = Get<am3::cases::vectortug::PrecipitationState,mpr::Coordinates2D>::InPoint(point).Value();
  toRet.insert(toRet.end(), toRet2.begin(), toRet2.end());
  toRet.insert(toRet.end(),toRet3.begin(), toRet3.end());
  return toRet;
}

template<>
PrecipitationState am3::cases::vectortug::Storage::GetPreviousState<PrecipitationState>()
{
  PrecipitationState toRet;
  std::vector<double> tmp,tmp2;
  tmp2 = pointerToHandler_->GetElementalVariablesVector();
  tmp.insert(tmp.begin(),tmp2.begin() + precipitationStorageStartsAt_,tmp2.begin()+precipitationStorageEndsBefore_);
  toRet(tmp);
  return toRet;
}

template<>
DislocationState am3::cases::vectortug::Storage::GetPreviousState<DislocationState>()
{
  DislocationState toRet;
  std::vector<double> tmp,tmp2;
  tmp2 = pointerToHandler_->GetElementalVariablesVector();
  tmp.insert(tmp.begin(),tmp2.begin() + dislocationStorageStartsAt_,tmp2.begin()+dislocationStorageEndsBefore_);
  toRet(tmp);
  return toRet;
}
    }
  }
}