#include "../models/macrosSimplifiedModels.h"
#include "caseVectorTUG.h"
#include "dislocationsModel.h"
#include "precipitationsModel.h"
#include "stressModel.h"
#include "deformAdapter.h"
#include "dislocationsModel_2.h"
#include "internalKbs.h"
#include "../softwareMatCalc/matcalcController.h"
#include "../softwareDeformBuiltin/deformHandlerOutputsProviders.h"
#include "../models/models.h"

/*	global **********************************************************/

// Must be created to initialize model
am3::cases::vectortug::Main mainInstance;
extern am3::model::InitializableMainModel *m = &mainInstance;

//am3::cases::vectortug::Main m;

namespace am3
{
	namespace cases
	{
		namespace vectortug
		{
      namespace cvt = am3::cases::vectortug;
      // In this method, all elements necessary for responding to Deform are created and initialized
      Main::Main()
      {
        // Macros does not work with namespaced strings (with '::')
        typedef cvt::DislocationModelWithStorage<cvt::Storage> DislocationModelWithStorage;
        typedef cvt::DislocationModelWithStorage_2<cvt::Storage> DislocationModelWithStorage_2;
        typedef cvt::PrecipitationModelWithStorage<cvt::Storage> PrecipitationModelWithStorage;
        typedef cvt::StressModelWithStorage<cvt::Storage> StressModelWithStorage;
        typedef cvt::CaseModel CaseModel;

        // Create Databus
        DATABUS_INSTANCE(mainDatabus,CaseVectorTUGDatabus)
        
        // Create Deform representation
        DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, mainDatabus, mainDatabus,mainDatabus,mainDatabus);
        CASE_MODEL(CaseModel,caseMacroImpl);

        // Links created Deform representation with global access pointer
        deformHandlerProc = caseMacroImpl->GetHandler().get();

        // Sets Deform representation as a IV source for databus
        mainDatabus->SetPreviousStepsStorage(caseMacroImpl);
        
        // Creates models (basic, defined in caseVectorTug project)
        MODEL_WITH_STORAGE_INSTANCE(stressModel,StressModelWithStorage,caseMacroImpl)
        MODEL_WITH_STORAGE_INSTANCE(dislocationModel,DislocationModelWithStorage,caseMacroImpl)
        MODEL_WITH_STORAGE_INSTANCE(dislocationModel_2,DislocationModelWithStorage_2,caseMacroImpl)
        MODEL_WITH_STORAGE_INSTANCE(precipModel,PrecipitationModelWithStorage,caseMacroImpl)

        // Internal knowledge based system, chooses proper model
        KBS_INSTANCE(kbs,Kbs,mainDatabus);

        // Switcher for dislocation models
        SWITCHER_INSTANCE(dislocationSwitcher,DislocationStateContract,kbs);
        ADD_MODELS_TO_SWITCHER(dislocationSwitcher,dislocationModel,dislocationModel_2);

        // Links models' outputs to databus
        DATABUS_ADD_MODELS(mainDatabus,stressModel, precipModel,dislocationSwitcher,caseMacroImpl)

          // Links databus with models' inputs
//         MODEL_INPUT_FROM_DATABUS(stressModel,StressModelWithStorage,mainDatabus);
//         MODEL_INPUT_FROM_DATABUS(dislocationModel,DislocationModelWithStorage,mainDatabus);
//         MODEL_INPUT_FROM_DATABUS(dislocationModel_2,DislocationModelWithStorage_2,mainDatabus);
//         MODEL_INPUT_FROM_DATABUS(precipModel,PrecipitationModelWithStorage,mainDatabus);
//         MODEL_INPUT_FROM_DATABUS(caseMacroImpl,CaseModel,mainDatabus);

        MODEL_INPUT_FROM_DATABUS(stressModel,mainDatabus);

//         typedef boost::type_of::msvc_typeid_wrapper<sizeof(*boost::type_of::encode_start(stressModel))>::type PointerTypeOfstressModel ; 
//         typedef PointerTypeOfstressModel::element_type TypeOfstressModel; 
//         ada::InputsProvider<TypeOfstressModel,boost::type_of::msvc_typeid_wrapper<sizeof(*boost::type_of::encode_start(mainDatabus))>::type::element_type> INPUTS_PROVIDER(TypeOfstressModel); 
//         INPUTS_PROVIDER(TypeOfstressModel).Set(mainDatabus,stressModel);;



//         typedef boost::type_of::msvc_typeid_wrapper<sizeof(*boost::type_of::encode_start(stressModel))>::type PointerTypeOfstressModel ; 
//         typedef PointerTypeOfstressModel::element_type TypeOfstressModel; 
//         //ada::InputsProvider<TypeOfstressModel,boost::type_of::msvc_typeid_wrapper<typename sizeof(*boost::type_of::encode_start(StressModelWithStorage))>::type::element_type> INPUTS_PROVIDER(TypeOfstressModel); 
// 
//         //ada::InputsProvider<databus,boost::type_of::msvc_typeid_wrapper<sizeof(*boost::type_of::encode_start())>::type::element_type> inputsProviderFordatabus; inputsProviderFordatabus.Set(,grainsModelStaticRecovery);;
//         ada::InputsProvider<TypeOfstressModel,BOOST_TYPEOF(mainDatabus) inputsProviderFordatabus;
//           //boost::type_of::msvc_typeid_wrapper<sizeof(*boost::type_of::encode_start())>::type::element_type> inputsProviderFordatabus; 
//         
//         inputsProviderFordatabus.Set(,grainsModelStaticRecovery);;
//         
// //        INPUTS_PROVIDER(TypeOfstressModel).Set(StressModelWithStorage,stressModel);;


        MODEL_INPUT_FROM_DATABUS(dislocationModel,mainDatabus);
        MODEL_INPUT_FROM_DATABUS(dislocationModel_2,mainDatabus);
        MODEL_INPUT_FROM_DATABUS(precipModel,mainDatabus);
        MODEL_INPUT_FROM_DATABUS(caseMacroImpl,mainDatabus);

//        am3::software::matcalc::MatcalcController mt("d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\", "d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\license.dat", "d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\test.mcs" );
      }

		} //vectortug
	} //cases
} //am3


