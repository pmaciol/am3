
/*! \file stresssModel.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef stresssModel_h__
#define stresssModel_h__
/*	include *********************************************************/

#include "../models/models.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "caseVectorTUG.h"

/*	using ***********************************************************/

namespace mo = am3::model;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace vectortug
    {
      template <typename STORAGE>
      class StressModelWithStorage
        : public DeformStressContractRequest
        , public mo::Inputs<StressModelInputs>
        , mo::ComputationsOnFirstCall<mpr::Coordinates2D>
      {
      public: 
        StressModelWithStorage(boost::shared_ptr<STORAGE> storage):storage_(storage) {};
        ~StressModelWithStorage(){};

        mpr::YieldStress GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::YieldStress *const );
        mpr::StressDStrain GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressDStrain *const );
        mpr::StressDStrainRate GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressDStrainRate *const );

        virtual void GetData(pnt::Point<mpr::Coordinates2D> * point );

        virtual void Calculate( pnt::Point<mpr::Coordinates2D> * point );

      protected:
        const boost::shared_ptr<STORAGE> storage_;
        mpr::YieldStress stress_;
        mpr::StressDStrain stressDstain_;
        mpr::StressDStrainRate stressDstrainRate_;
        std::vector<const double> macroscopicState_;
        std::vector<const double> dislocationsState_;
        std::vector<const double> precipitationsState_;
        boost::shared_ptr<pnt::Point<mpr::Coordinates2D> > actualPoint;
      };

////////////////////////////////////////////////////////////////////////// Implementation //////////////////////////////////////////////
     
      template <typename STORAGE>
      mpr::YieldStress StressModelWithStorage<STORAGE>::GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::YieldStress *const )
      {
        CheckOrGetDataInPoint(point);
        return stress_;
      }

      template <typename STORAGE>
      mpr::StressDStrain StressModelWithStorage<STORAGE>::GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressDStrain *const )
      {
        CheckOrGetDataInPoint(point);
        return stressDstain_;
      }

      template <typename STORAGE>
      mpr::StressDStrainRate StressModelWithStorage<STORAGE>::GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressDStrainRate *const )
      {
        CheckOrGetDataInPoint(point);
        return stressDstrainRate_;
      }

      template <typename STORAGE>
      void StressModelWithStorage<STORAGE>::GetData( pnt::Point<mpr::Coordinates2D> * point )
      {
        actualPoint =  point->Copy();
        std::vector<double> tmpVec = Get<MacroscopicState,BaseCoordinates>::InPoint(point).Value();
        macroscopicState_.resize(tmpVec.size());
        std::copy(tmpVec.begin(),tmpVec.end(),macroscopicState_.begin());
        tmpVec = Get<DislocationState,BaseCoordinates>::InPoint(point).Value();
        dislocationsState_.resize(tmpVec.size());
        std::copy(tmpVec.begin(),tmpVec.end(),dislocationsState_.begin());
        tmpVec = Get<PrecipitationState,BaseCoordinates>::InPoint(point).Value();
        precipitationsState_.resize(tmpVec.size());
        std::copy(tmpVec.begin(),tmpVec.end(),precipitationsState_.begin());
      }

      // GetState methods implementation in separate file
      #include "stressModelCalculate.h"

      
    } //vectortug
  } //cases
} //am3
#endif // stresssModel_h__
