
/*! \file precipitationsModel.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef precipitationsModel_h__
#define precipitationsModel_h__
/*	include *********************************************************/

#include "../models/models.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "caseVectorTUG.h"

/*	using ***********************************************************/

namespace mo = am3::model;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace vectortug
    {
      template <typename STORAGE>
      class PrecipitationModelWithStorage
        : public PrecipitationStateContract
        , public mo::Inputs<PrecipitationModelInputs>
        , mo::ComputationsOnFirstCall<mpr::Coordinates2D>
      {
      public: 
        PrecipitationModelWithStorage(boost::shared_ptr<STORAGE> storage):storage_(storage) {};
        ~PrecipitationModelWithStorage(){};

        PrecipitationState GetState(mpt::Point<mpr::Coordinates2D> * point,const PrecipitationState *const);

        virtual void GetData(pnt::Point<mpr::Coordinates2D> * point );

        virtual void Calculate( pnt::Point<mpr::Coordinates2D> * point );

      protected:
        const boost::shared_ptr<STORAGE> storage_;
        std::vector<double> returnedVector_;
        std::vector<const double> previousStep_;
        std::vector<const double> macroscopicState_;
        std::vector<const double> dislocationsState_;
        boost::shared_ptr<pnt::Point<mpr::Coordinates2D> > actualPoint;
      };

////////////////////////////////////////////////////////////////////////// Implementation //////////////////////////////////////////////
     
      template <typename STORAGE>
      PrecipitationState PrecipitationModelWithStorage<STORAGE>::GetState( mpt::Point<mpr::Coordinates2D> * point,const PrecipitationState *const )
      {
        CheckOrGetDataInPoint(point);
        PrecipitationState toRet;
        toRet(returnedVector_);
        return toRet;
      }

      template <typename STORAGE>
      void PrecipitationModelWithStorage<STORAGE>::GetData( pnt::Point<mpr::Coordinates2D> * point )
      {
        actualPoint =  point->Copy();
        DislocationState tmpPreviousStep_ = storage_->GetPreviousState<DislocationState>();
        previousStep_.insert(previousStep_.begin(),tmpPreviousStep_().begin(),tmpPreviousStep_().end());
        std::vector<double> tmpVec = Get<MacroscopicState,BaseCoordinates>::InPoint(point).Value();
        macroscopicState_.resize(tmpVec.size());
        macroscopicState_.insert(macroscopicState_.begin(),tmpVec.begin(),tmpVec.end());
        tmpVec = Get<DislocationState,BaseCoordinates>::InPoint(point).Value();
        dislocationsState_.resize(tmpVec.size());
        dislocationsState_.insert(dislocationsState_.begin(),tmpVec.begin(),tmpVec.end());
      }

      // GetState methods implementation in separate file
      #include "precipitationsModelCalculate.h"

      
    } //vectortug
  } //cases
} //am3
#endif // precipitationsModel_h__
