#include "internalKbs.h"


namespace am3
{
  namespace cases
  {
    namespace vectortug
    {
// template <typename VARIABLE> mpr::ModelID Kbs::GetModelId(mpt::Point<mpr::Coordinates2D> *point)
// {
//   const VARIABLE* trait=nullptr;
//   return GetModelId(point,trait);
// }

mpr::ModelID Kbs::GetModelId(mpt::Point<mpr::Coordinates2D> *point, const DislocationState* const)
{
  boost::shared_ptr<moc::Provider<MacroscopicState,mpr::Coordinates2D> > macroContract = databus_;
  MacroscopicState macroState = macroContract->GetState(point,&macroState);
  mpr::ModelID mId;
  if (macroState.Value().at(0) > 800)
  {
    mId(0);
  } 
  else
  {
    mId(1);
  }
  return mId;
}



mpr::ModelID Kbs::GetModelId(mpt::Point<mpr::Coordinates2D> *point, const PrecipitationState* const)
{
  boost::shared_ptr<moc::Provider<MacroscopicState,mpr::Coordinates2D> > macroContract = databus_;
  MacroscopicState macroState = macroContract->GetState(point,&macroState);
  boost::shared_ptr<moc::Provider<DislocationState,mpr::Coordinates2D> > dislContract = databus_;
  DislocationState dislState = dislContract->GetState(point,&dislState);
  mpr::ModelID mId;
  //         if (macroState.Value().at(0) > 800)
  //         {
  mId(0);
  //         } 
  //         else
  //         {
  //           mId(1);
  //         }
  return mId;
}

// 
// template<>
// mpr::ModelID Kbs::GetModelId<am3::cases::vectortug::DislocationState>(mpt::Point<mpr::Coordinates2D> *point);
    }
  }

}