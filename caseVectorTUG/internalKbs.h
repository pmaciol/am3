
/*! \file internalKbs.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef internalKbs_h__
#define internalKbs_h__
/*	include *********************************************************/

#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "caseVectorTUG.h"

/*	using ***********************************************************/

namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;
namespace moc = am3::model::contract;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace vectortug
		{
      class Kbs
      {
      public:
        template <typename VARIABLE> mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point)
        {
          const VARIABLE* trait=nullptr;
          return GetModelId(point,trait);
        }
        Kbs(boost::shared_ptr<CaseVectorTUGDatabus> databus): databus_(databus){};
      protected:
        boost::shared_ptr<CaseVectorTUGDatabus> databus_;
        mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const DislocationState* const);
        mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const PrecipitationState* const);
      };

      

      //////////////////////////////////////////////////////////////////////////
      // Implementation
      //////////////////////////////////////////////////////////////////////////

//       template <typename VARIABLE> mpr::ModelID Kbs::GetModelId(mpt::Point<mpr::Coordinates2D> *point)
//       {
//         const VARIABLE* trait=nullptr;
//         return GetModelId(point,trait);
//       }
		} //vectortug
	} //cases
} //am3

#endif // internalKbs_h__
