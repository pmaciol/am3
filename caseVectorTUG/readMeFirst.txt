This is a manual, how to use vector based model for Deform, including dislocations and precipitataions.

1. Starting configuraion
Starting configuration consist of stress computation part (stressModel*), single precipitation part (precipitationsModel*) and two alternative models for dislocations (dislocationsModel*). Additional files for grains models are included, but not used at this moment. Simple Knowledge Based implementation is included (internalKbs.h).

2. Creating own procedures for existing models
Computational procedures for each of models must be defined in files *Calulate*.h. Please read comments in those files.

3. Controlling KBS
It is done in internalKbs.h file, part "Implementation". Presently switcher works only for dislocations model (method mpr::ModelID Kbs::GetModelId(mpt::Point<mpr::Coordinates2D> *point, const DislocationState* const)).

4. Adding new models
Each of models must be implemented in new file. To simplify models structure, please use a pair of files, *Model* and *ModelCalculate*. Please compare to previously implemented models. New models must be included in caseVectorTUG.cpp file (please read comments in that file). Adding precipitations models needs adding switcher, again in caseVectorTUG.cpp file.