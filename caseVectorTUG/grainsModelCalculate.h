// template <typename STORAGE>
// void GrainsModelWithStorage<STORAGE>::Calculate( pnt::Point<mpr::Coordinates2D> * point )
// {
//   // Output values must be put into returnedVector_
//   // Sequence of variables must be controlled by programmer and must be the same as in other models, using this vector
//   // previousStep_ is a vector of internal variables
//   // macroscopicState_ is a vector filled by macro simulation
//   returnedVector_.clear();
//   // exemplary:
//   returnedVector_.push_back(previousStep_[0]*2);
//   returnedVector_.push_back(previousStep_[1]*2);
//   returnedVector_.push_back(macroscopicState_[0]*previousStep_[0]);
// }