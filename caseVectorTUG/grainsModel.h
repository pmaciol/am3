// 
// /*! \file grainsModel.h **************************************************
//  * \author		Piotr Maciol
//  * \copyright 	GNU Public License.
//  * \brief       
//  * \details   	
// *********************************************************************/
// #ifndef grainsModel_h__
// #define grainsModel_h__
// /*	include *********************************************************/
// 
// #include "../models/models.h"
// #include "../models/point.h"
// #include "../models/dataStructures.h"
// #include "caseVectorTUG.h"
// 
// /*	using ***********************************************************/
// 
// namespace mo = am3::model;
// namespace mpt = am3::model::point;
// namespace mpr = am3::model::properties;
// 
// /*	extern **********************************************************/
// /*	classes *********************************************************/
// 
// namespace am3
// {
//   namespace cases
//   {
//     namespace vectortug
//     {
//       template <typename STORAGE>
//       class GrainsModelWithStorage
//         : public GrainStateContract
//         , public mo::Inputs<GrainModelInputs>
//         , mo::ComputationsOnFirstCall<mpr::Coordinates2D>
//       {
//       public: 
//         GrainModelWithStorage(boost::shared_ptr<STORAGE> storage):storage_(storage) {};
//         ~GrainModelWithStorage(){};
// 
//         GrainState GetState(mpt::Point<mpr::Coordinates2D> * point,const GrainState *const);
// 
//         virtual void GetData(pnt::Point<mpr::Coordinates2D> * point );
// 
//         virtual void Calculate( pnt::Point<mpr::Coordinates2D> * point );
// 
//       protected:
//         const boost::shared_ptr<STORAGE> storage_;
//         std::vector<double> returnedVector_;
//         std::vector<const double> previousStep_;
//         std::vector<const double> macroscopicState_;
//         boost::shared_ptr<pnt::Point<mpr::Coordinates2D> > actualPoint;
//       };
// 
// ////////////////////////////////////////////////////////////////////////// Implementation //////////////////////////////////////////////
//      
//       template <typename STORAGE>
//       GrainState GrainModelWithStorage<STORAGE>::GetState( mpt::Point<mpr::Coordinates2D> * point,const GrainState *const )
//       {
//         CheckOrGetDataInPoint(point);
//         GrainState toRet;
//         toRet(returnedVector_);
//         return toRet;
//       }
// 
//       template <typename STORAGE>
//       void GrainModelWithStorage<STORAGE>::GetData( pnt::Point<mpr::Coordinates2D> * point )
//       {
//         actualPoint =  point->Copy();
//         std::vector<double> tmpVec = storage_->GetPreviousState<std::vector<double> >();
//         previousStep_.resize(tmpVec.size());
//         std::copy(tmpVec.begin(),tmpVec.end(),previousStep_.begin());
//         tmpVec = Get<MacroscopicState,BaseCoordinates>::InPoint(point).Value();
//         macroscopicState_.resize(tmpVec.size());
//         std::copy(tmpVec.begin(),tmpVec.end(),macroscopicState_.begin());
//       }
// 
//       // GetState methods implementation in separate file
//       #include "grainsModelCalculate.h"
// 
//       
//     } //vectortug
//   } //cases
// } //am3
// #endif // grainsModel_h__
