
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef tugcontracts_h__
#define tugcontracts_h__
/*	include *********************************************************/

#include <vector>
#include "../models/dataStructures.h"
#include "../models/macrosSimplifiedModels.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace vectortug
    {
      // Vectors of variables for each model
      SIMPLE_PROP(MacroscopicState, std::vector<double>)
      SIMPLE_PROP(PrecipitationState,std::vector<double>)
      SIMPLE_PROP(DislocationState,std::vector<double>)
      
      // Defines, which variables can be stored with Deform
      typedef moc::Contract<TYPELIST_2(	
        PrecipitationState,
        DislocationState
        ),mpr::Coordinates2D> DeformStoredVariables;

        #define SingleVectorContractLength 1
        #define DeformStressContractRequestLength 3
        #define MacroscopicStateContractLength 1

        // Contracts:
        CONTRACT(   MacroscopicStateContract    , MacroscopicStateContractLength, mpr::Coordinates2D, MacroscopicState)
        CONTRACT(   PrecipitationStateContract  , SingleVectorContractLength, mpr::Coordinates2D, PrecipitationState)
        CONTRACT(   DislocationStateContract    , SingleVectorContractLength, mpr::Coordinates2D, DislocationState)

        // Inputs:
        CONTRACT(		DeformStressContractRequest	, DeformStressContractRequestLength , mpr::Coordinates2D, mpr::YieldStress, mpr::StressDStrain, mpr::StressDStrainRate)
        // PrecipitationModelInputs and DislocationModelInputs cannot be cross-linked. Only one of them can call the second. Both can use internal variables from previous step.
        // During simulation, one of them must be computed first, then second one. If both could call the second as input, deadlock could be caused. 
        CONTRACT(		PrecipitationModelInputs    , 3, mpr::Coordinates2D, MacroscopicState, DislocationState)
        CONTRACT(		DislocationModelInputs      , 2, mpr::Coordinates2D, MacroscopicState/*, PrecipitationState*/)
        // or 
        // CONTRACT(		PrecipitationModelInputs    , 1, mpr::Coordinates2D, MacroscopicState /*, DislocationState*/)
        // CONTRACT(		DislocationModelInputs      , 2, mpr::Coordinates2D, MacroscopicState, PrecipitationState)
        //////////////////////////////////////////////////////////////////////////
        CONTRACT(		StressModelInputs           , 4, mpr::Coordinates2D, MacroscopicState, PrecipitationState, DislocationState)
    }
  }
}
#endif // tugcontracts_h__
