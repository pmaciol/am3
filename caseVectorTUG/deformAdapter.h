
/*! \file deformAdapter.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef deformAdapter_h__
#define deformAdapter_h__
/*	include *********************************************************/

#include "../softwareDeformBuiltin/deformHandler.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
//#include "caseVectorTUG.h"
#include "contracts.h"
#include "../adapter/ivstorage.h"


/*	using ***********************************************************/

namespace mpt = am3::model::point;
namespace sdm = am3::software::deform;
namespace mpr = am3::model::properties;
namespace ad = am3::adapter;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace vectortug
		{
      // ad::IVStorage does not work with vector variables. Derived class must be implemented
      class Storage
        : public ad::IVStorage<sdm::DeformHandler,DeformStoredVariables>
      {
      public:
        ~Storage(){};
        template<typename T>T GetPreviousState();
        std::vector<double> GetInternalVariables(mpt::Point<mpr::Coordinates2D> *point);
      protected:
        sdm::DeformHandler<typename DeformStoredVariables::CoordinatesType, Storage >* pointerToHandler_;
        // Defines, how to divide single internal variables vector from Deform into dislocations and precipitation vectors
        static const unsigned int dislocationStorageStartsAt_ = 0;
        static const unsigned int dislocationStorageEndsBefore_ = 2;
        static const unsigned int precipitationStorageStartsAt_ = 2;
        static const unsigned int precipitationStorageEndsBefore_ = 4;
      };

//       Copies Dislocations and Precipitation states to deform internal variables vector ...
//             std::vector<double> Storage::GetInternalVariables( mpt::Point<mpr::Coordinates2D> *point );
//             {
//               std::vector<double> toRet; 
//               std::vector<double> toRet2; 
//               std::vector<double> toRet3; 
//               toRet2 = Get<am3::cases::vectortug::DislocationState,mpr::Coordinates2D>::InPoint(point).Value();
//               toRet3 = Get<am3::cases::vectortug::PrecipitationState,mpr::Coordinates2D>::InPoint(point).Value();
//               toRet.insert(toRet.end(), toRet2.begin(), toRet2.end());
//               toRet.insert(toRet.end(),toRet3.begin(), toRet3.end());
//               return toRet;
//             }

      // and back (two following methods)
//       template<>
//       PrecipitationState am3::cases::vectortug::Storage::GetPreviousState<PrecipitationState>()
//       {
//         PrecipitationState toRet;
//         std::vector<double> tmp,tmp2;
//         tmp2 = pointerToHandler_->GetElementalVariablesVector();
//         tmp.insert(tmp.begin(),tmp2.begin() + precipitationStorageStartsAt_,tmp2.begin()+precipitationStorageEndsBefore_);
//         toRet(tmp);
//         return toRet;
//       }

//       template<>
//       DislocationState am3::cases::vectortug::Storage::GetPreviousState<DislocationState>()
//       {
//         DislocationState toRet;
//         std::vector<double> tmp,tmp2;
//         tmp2 = pointerToHandler_->GetElementalVariablesVector();
//         tmp.insert(tmp.begin(),tmp2.begin() + dislocationStorageStartsAt_,tmp2.begin()+dislocationStorageEndsBefore_);
//         toRet(tmp);
//         return toRet;
//       }

      // Copying macroscopic state from Deform into vector representation
      template<typename HANDLER>
      class MacroModel : public am3::model::MacroModel<HANDLER, MacroscopicStateContract>
      {
      public:
        MacroscopicState GetState(am3::model::common::Point<typename MacroscopicStateContract::CoordinatesType> * point,const MacroscopicState *const )
        {
          std::vector<double> vector;
          // Fragile! Model designer have to care about variables positions in vector!
          // Changes in this sequence must be accounted in any class using this contract.
          vector.push_back(pointerToMacroHandler_->GetTemperatureIn(*point).Value());
          vector.push_back(pointerToMacroHandler_->GetEffectiveStrainRateIn(*point).Value());
          vector.push_back(pointerToMacroHandler_->GetStepLength().Value());
          vector.push_back(pointerToMacroHandler_->GetMidPoint()->Coordinates().x());
          vector.push_back(pointerToMacroHandler_->GetMidPoint()->Coordinates().y());
          MacroscopicState toRet;
          toRet(vector);
          return toRet;
        }
      };

      typedef MacroModel<sdm::DeformHandler<mpr::Coordinates2D, Storage> > MacroModelDeform;
      typedef mo::CaseMacroImplementation<MacroModelDeform,Storage> CaseModel;

    } //vectortug
	} //cases
} //am3

#endif // deformAdapter_h__
