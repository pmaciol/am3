
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef strainContracts_h__
#define strainContracts_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../models/contractMacros.h"
#include "../models/macrosSimplifiedModels.h"

/*	using ***********************************************************/

namespace moc=am3::model::contract;
namespace mpr=am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace problem
  {
    namespace strain
    {
//       SIMPLE_PROP(DislocationsDensityWall,double);    // the dislocations composing the subgrain walls; [m-2]
//       SIMPLE_PROP(DislocationsDensityRandom,double);  //all other dislocations, which do not compose the subgrain walls; [m-2]
// 
//       namespace contract
//       {
//         CONTRACT(		WallsDislocationsContract			        	,1,mpr::Coordinates2D, DislocationsDensityWall);
//         CONTRACT(		RandomDislocationsContract			        ,1,mpr::Coordinates2D, DislocationsDensityRandom);
//         CONTRACT(		ShearRateTemperatureInput    	  ,2,mpr::Coordinates2D, mpr::ShearRate,mpr::Temperature);
//         CONTRACT(		ShearRateInput		  		,1,mpr::Coordinates2D, mpr::ShearRate);
//         CONTRACT(		ShearRateTemperatureInput    	  		  	,2,mpr::Coordinates2D, mpr::ShearRate,mpr::Temperature);
      }
    }
  }
}
#endif // strainContracts_h__
