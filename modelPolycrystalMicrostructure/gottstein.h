
/*! \file gottstein.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef gottstein_h__
#define gottstein_h__
/*	include *********************************************************/

#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../models/point.h"
#include "../models/models.h"
#include "../models/inputTypelists.h"
#include "../databus/databusTypelists.h"
#include "gottsteinContract.h"
#include "grainsDeformation.h"
#include "grainsNoDeformation.h"
#include "grainsStaticRecovery.h"
#include "dislocationsDeformation.h"
#include "dislocationsNoDeformation.h"
#include "dislocationsStaticRecovery.h"



/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace mpm = am3::model::polycrystal;
namespace pnt = am3::model::point;
namespace mo = am3::model;
namespace mpr = am3::model::properties;
namespace db  = am3::databus;

/*	extern **********************************************************/
// template < template <class G> class T_DB, 
//   template <class G> class T_Model>
// struct Adapter {
//   typedef Adapter< T_DB, T_Model>       Self;
//   typedef T_DB<Self>                 Db;
//   typedef T_Model<Self>                 Model;
// };


/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace polycrystal
		{
			// UserGuide: Models used by Pavel Sherstnev in PhD thesis (with some changes, commented in code)
			template<typename INPUT>
			class DiffusionBySherstnev
				// UserGuide: Each model must derive from realized Contract ...
				: public ppm::contract::DiffusionContract2D
				// UserGuide: ... and Inputs
				, public mo::InputTypelist<INPUT>//<mpm::DiffusionBySherstnevInput>
			{
			public:
				// UserGuide: Initialization of constants
				DiffusionBySherstnev(): b(0.286E-9), debye(19.3E+12), kb(1.38E-23), qgb(0.5*1.3), qsd(0.98) {};
				// UserGuide: GetState method must be provided for each variable from Contract
				virtual ppm::contract::SelfDiffusion GetState(pnt::Point<mpr::Coordinates2D> *,const ppm::contract::SelfDiffusion *const );
				virtual ppm::contract::GrainBoundaryDiffusion GetState(pnt::Point<mpr::Coordinates2D> *,const ppm::contract::GrainBoundaryDiffusion *const );
			protected:
				const double b,debye,kb, qsd, qgb;
			};

			template<typename INPUT>
			class YieldStressBySherstnev
				: public ppm::contract::YieldStressContract2D
				, public mo::InputTypelist<INPUT>//<mpm::YieldStressBySherstnevInput>
			{
			public:
				YieldStressBySherstnev():
						alpha_coeff(22537.0),
						temperature_coeff(0.014),
						sigma_ss_coeff(27.5),
						misfitCoeff(1.333),
						beta(0.6667),
						mTey(3.06),
						b(0.286E-9)
						{};
				virtual mpr::YieldStress GetState(pnt::Point<mpr::Coordinates2D> *,const mpr::YieldStress *const );
			protected:
				const double alpha_coeff;
				const double temperature_coeff;
				const double sigma_ss_coeff;
				const double misfitCoeff;
				const double beta;
				const double mTey;
				const double b;
			};
			
// 			template<typename INPUT>
// 			class ShearModulusBySherstnev
// 				: public ppm::contract::ShearModulusContract2D
// 				, public mo::Inputs<mpm::ShearModulusBySherstnevInput>
// 			{
// 			public:
// 				virtual mpr::ShearModulus GetState(pnt::Point<mpr::Coordinates2D> *,const mpr::ShearModulus *const );
// 			};
// 
// 			      class ShearModulusBySherstnevVirt
//         : public ppm::contract::ShearModulusContract2D
//       {
//       public:
// 
//         ppm::contract::MacroInputsContract2D* bus;
//         mpr::ShearModulus GetState( pnt::Point<mpr::Coordinates2D> * point,const mpr::ShearModulus *const )
//         {
//           mpr::ShearModulus toRet;
//           moc::Provider<mpr::Temperature,mpr::Coordinates2D>* ptr;
//           ptr = bus;
//           mpr::Temperature temperature = ptr->GetState(point, &temperature);
//           //toRet((84.828E+9 - 4.06E+7 * temperature()) / (2 * (1 + pois))); // Sherstnev values
//           toRet((32.102 - 0.0155 * temperature())*1e9); // Estimation based on Sherstnev phd, Abbildung 5-8:
//           return toRet;
//         }
// 
//       };

      template<typename INPUT>
      class ShearModulusBySherstnevTemp
        : public ppm::contract::ShearModulusContract2D
        , public mo::InputTypelist<INPUT>
      {
      public:
        typedef typename mpm::ShearModulusBySherstnevInput Inputs;
        typedef typename ppm::contract::ShearModulusContract2D Contract;

        virtual mpr::ShearModulus GetState(pnt::Point<mpr::Coordinates2D> *point ,const mpr::ShearModulus *const )
        {
         mpr::ShearModulus toRet;
         mpr::Temperature temperature; 
         temperature = bus_->GetFromDatabus<mpr::Temperature>(point);
        toRet((32.102 - 0.0155 * temperature())*1e9); // Estimation based on Sherstnev phd, Abbildung 5-8:
        return toRet;
        }
      };

      template<>
      class ShearModulusBySherstnevTemp<am3::databus::MockDatabus>
        : public ppm::contract::ShearModulusContract2D
        , public mo::InputTypelist<am3::databus::MockDatabus>
      {
      public:
        typedef  mpm::ShearModulusBySherstnevInput Inputs;
        typedef  ppm::contract::ShearModulusContract2D Contract;
      };

      template<typename INPUT>
      class DiffusionBySherstnevTemp
        : public ppm::contract::DiffusionContract2D
        , public mo::InputTypelist<INPUT>
      {
      public:
        typedef typename mpm::DiffusionBySherstnevInput Inputs;
        typedef typename ppm::contract::DiffusionContract2D Contract;

        DiffusionBySherstnevTemp(): b(0.286E-9), debye(19.3E+12), kb(1.38E-23), qgb(0.5*1.3), qsd(0.98) {};
        virtual ppm::contract::SelfDiffusion GetState(pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::SelfDiffusion *const )
        {
          ppm::contract::SelfDiffusion toRet;
          mpr::Temperature temperature;
          temperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          toRet (pow(b,2)*debye*exp(-qsd * 1.6e-19 / (kb * temperature()) ));
          return toRet;
        }
        virtual ppm::contract::GrainBoundaryDiffusion GetState(pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::GrainBoundaryDiffusion *const )
        {
          ppm::contract::GrainBoundaryDiffusion toRet;
          mpr::Temperature temperature;
          temperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          double grainDif = pow(b,2)*debye*exp(-qgb*1.6E-19/(kb * temperature()));
          toRet(grainDif);
          return toRet;
        }
      protected:
        const double b,debye,kb, qsd, qgb;
      };

      class DiffusionBySherstnevVirt
        : public ppm::contract::DiffusionContract2D
      {
      public:
        ppm::contract::MacroInputsContract2D* bus;

        DiffusionBySherstnevVirt(): b(0.286E-9), debye(19.3E+12), kb(1.38E-23), qgb(0.5*1.3), qsd(0.98) {};
        virtual ppm::contract::SelfDiffusion GetState(pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::SelfDiffusion *const )
        {
          ppm::contract::SelfDiffusion toRet;
          mpr::Temperature temperature;
                    moc::Provider<mpr::Temperature,mpr::Coordinates2D>* ptr;
          ptr = bus;
          temperature = ptr->GetState(point, &temperature);
          toRet (pow(b,2)*debye*exp(-qsd * 1.6e-19 / (kb * temperature()) ));
          return toRet;
        }
        virtual ppm::contract::GrainBoundaryDiffusion GetState(pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::GrainBoundaryDiffusion *const )
        {
          ppm::contract::GrainBoundaryDiffusion toRet;
          mpr::Temperature temperature;
                  moc::Provider<mpr::Temperature,mpr::Coordinates2D>* ptr;
          ptr = bus;
          temperature = ptr->GetState(point, &temperature);
          double grainDif = pow(b,2)*debye*exp(-qgb*1.6E-19/(kb * temperature()));
          toRet(grainDif);
          return toRet;
        }
      protected:
        const double b,debye,kb, qsd, qgb;
      };

      template<>
      class DiffusionBySherstnevTemp<am3::databus::MockDatabus>
        : public ppm::contract::DiffusionContract2D
        , public mo::InputTypelist<am3::databus::MockDatabus>
      {
      public:
        typedef mpm::DiffusionBySherstnevInput Inputs;
        typedef ppm::contract::DiffusionContract2D Contract;
      };

	  template<typename INPUT>
			class ZenerParamBySherstnev
				: public ppm::contract::ZenerContract2D
				, public mo::InputTypelist<INPUT>//<mpm::NoInput>
			{
			public:
				ZenerParamBySherstnev(): fv_disp(0.018), r_disp(0.6e-6) {};
				virtual ppm::contract::ZenerParam GetState(pnt::Point<mpr::Coordinates2D> *,const ppm::contract::ZenerParam *const );
			protected:
				const double fv_disp;
				const double r_disp;
			};

		}
	}
}
#endif // gottstein_h__
