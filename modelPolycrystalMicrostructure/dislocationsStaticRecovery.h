
/*! \file dislocationsStaticRecovery.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef dislocationsStaticRecovery_h__
#define dislocationsStaticRecovery_h__
/*	include *********************************************************/

#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../models/models.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "gottsteinContract.h"
#include "dislocationsBase.h"


/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace mo = am3::model;

/*	extern **********************************************************/

namespace am3
{
	namespace model
	{
		namespace polycrystal
		{
			// UserGuide: See in dislocationsDeformation.h  
			template<typename INPUT, typename STORAGE>
			class DislocationsGottsteinStaticRecovery
				: public DislocationsBase<STORAGE>
				, public mo::InputTypelist<INPUT>//<am3::model::polycrystal::DislocationsGottsteinStaticRecoveryInput2D>
			{
			public:
				DislocationsGottsteinStaticRecovery(boost::shared_ptr<STORAGE> previousStepData): DislocationsBase<STORAGE>::DislocationsBase(previousStepData){};

			protected:
				virtual void GetData(pnt::Point<mpr::Coordinates2D> * point);
				virtual void Calculate(pnt::Point<mpr::Coordinates2D> * point);
				void CalculateSubgrainSize();
				void CalculateDislDensityMobile(const ppm::contract::SubgrainSize& subgrainSize);
				void CalculateDislDensityImmobileInteriors();
				void CalculateDislDensityImmobileWalls();
				void CalculateDislDensityMobileDeformed();
				void CalculateDislDensityImmobileInteriorsDeformed();
				void CalculateDislDensityImmobileWallsDeformed();

				ppm::contract::DislDensityMobileDeformed dislDensityMobileDeformedPrevious;
				ppm::contract::DislDensityMobile dislDensityMobilePrevious;
				ppm::contract::SubgrainSize subgrainSizePrevious;

 				ppm::contract::SelfDiffusion selfDiffusion;
 				mpr::Temperature temperature;
 				mpr::StepLength timeStep;
 				ppm::contract::ZenerParam zenerDrag;
			};

			//////////////////////////////////////////////////////////////////////////
			// Implementation
			//////////////////////////////////////////////////////////////////////////

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinStaticRecovery<INPUT, STORAGE>::Calculate( pnt::Point<mpr::Coordinates2D> * point )
			{
				CalculateSubgrainSize();
				CalculateDislDensityMobile(subgrainSizeNew);
				CalculateDislDensityImmobileInteriors();
				CalculateDislDensityImmobileWalls();
				CalculateDislDensityMobileDeformed();
				CalculateDislDensityImmobileInteriorsDeformed();
				CalculateDislDensityImmobileWallsDeformed();
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinStaticRecovery<INPUT, STORAGE>::GetData( pnt::Point<mpr::Coordinates2D> * point )
			{
				actualPoint =  point->Copy();

				dislDensityMobileDeformedPrevious(previousStepData_->GetPreviousState<ppm::contract::DislDensityMobileDeformed>());
				dislDensityMobilePrevious(previousStepData_->GetPreviousState<ppm::contract::DislDensityMobile>());
				subgrainSizePrevious(previousStepData_->GetPreviousState<ppm::contract::SubgrainSize>());

 				selfDiffusion = Get<ppm::contract::SelfDiffusion,BaseCoordinates>::InPoint(point);
				temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point);
 				timeStep = Get<mpr::StepLength,BaseCoordinates>::InPoint(point);
 				zenerDrag = Get<ppm::contract::ZenerParam,BaseCoordinates>::InPoint(point);
			}

			//////////////////////////////////////////////////////////////////////////
			//  Here is the model! //
			//////////////////////////////////////////////////////////////////////////

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinStaticRecovery<INPUT, STORAGE>::CalculateDislDensityImmobileWallsDeformed()
			{
				dislDensityImmobileWallsDeformedNew(0.0);
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinStaticRecovery<INPUT, STORAGE>::CalculateDislDensityImmobileInteriorsDeformed()
			{
				dislDensityImmobileInteriorsDeformedNew(0.0);
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinStaticRecovery<INPUT, STORAGE>::CalculateDislDensityMobileDeformed()
			{
				dislDensityMobileDeformedNew(dislDensityMobileDeformedPrevious());
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinStaticRecovery<INPUT, STORAGE>::CalculateDislDensityImmobileWalls()
			{
				dislDensityImmobileWallsNew(0.0);
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinStaticRecovery<INPUT, STORAGE>::CalculateDislDensityImmobileInteriors()
			{
				dislDensityImmobileInteriorsNew(0.0);
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinStaticRecovery<INPUT, STORAGE>::CalculateDislDensityMobile(const ppm::contract::SubgrainSize& subgrainSize)
			{
				if (subgrainSize() < delta_cr) 
				{
					dislDensityMobileNew(pow((b1 / subgrainSize()),2));
				}
				else 
				{
					dislDensityMobileNew = dislDensityMobilePrevious;
				}
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinStaticRecovery<INPUT, STORAGE>::CalculateSubgrainSize()
			{
				double ddelta = 0;
				if (subgrainSizePrevious() < delta_cr)
				{
					double condition = 1.5 * gammas / subgrainSizePrevious() - 1.5 * gammas / delta_cr;
					if (condition > zenerDrag())
					{
						double ms = selfDiffusion() * pow(b,2) / (4 * kb * temperature() * gammas);
						ddelta = ms * (1.5 * gammas / subgrainSizePrevious() - 1.5 * gammas / delta_cr - zenerDrag()) * timeStep();
					} 
					subgrainSizeNew(subgrainSizePrevious() + ddelta);
				} 
				else subgrainSizeNew = subgrainSizePrevious;
			}
		}
	}
}
#endif // dislocationsStaticRecovery_h__
