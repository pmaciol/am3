
/*! \file gottsteinContract.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief Input and outputs for a Gottstein model      
 * \details   	
*********************************************************************/
#ifndef polycrystalMicrostructure_gottsteinContract_h__
#define polycrystalMicrostructure_gottsteinContract_h__
/*	include *********************************************************/

#include "../common/typelists.h"
#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../models/contractMacros.h"
#include "../problemPolycrystalMicrostructure/contracts.h"

/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace mpr = am3::model::properties;
namespace moc = am3::model::contract;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace polycrystal
		{
			// UserGuide: Definition of input variables for all models
			typedef moc::Contract<TYPELIST_10(	
				mpr::Temperature,
				mpr::ShearModulus,
				mpr::EffectiveStrainRate,
				mpr::StepLength,
				ppm::contract::DislDensityImmobileInteriors,
				ppm::contract::DislDensityImmobileWalls,
				ppm::contract::DislDensityMobile,
				ppm::contract::SubgrainSize,
				ppm::contract::SelfDiffusion,
				ppm::contract::ZenerParam
				),mpr::Coordinates2D> GrainsByShersnevNoDeformationInput2D;

			typedef moc::Contract<TYPELIST_7(	
				ppm::contract::DislDensityImmobileInteriors,
				ppm::contract::DislDensityImmobileWalls,
				ppm::contract::DislDensityMobile,
				ppm::contract::SubgrainSize,
				mpr::EffectiveStrainRate,
				mpr::StepLength,
				mpr::ShearModulus
				),mpr::Coordinates2D> GrainsByShersnevDeformationInput2D;

			typedef moc::Contract<TYPELIST_9(	
				mpr::Temperature,
				mpr::ShearModulus,
				ppm::contract::DislDensityImmobileInteriors,
				ppm::contract::DislDensityImmobileWalls,
				ppm::contract::DislDensityMobile,
				mpr::StepLength,
				ppm::contract::SelfDiffusion,
				ppm::contract::ZenerParam,
				ppm::contract::SubgrainSize
				),mpr::Coordinates2D> GrainsByShersnevStaticRecoveryInput2D;

			typedef moc::Contract<TYPELIST_1(	
				mpr::Temperature // Should be mpr::null or something
				),mpr::Coordinates2D> DislocationsGottsteinNoDeformation2DInputs;		

			typedef moc::Contract<TYPELIST_5(	
				mpr::Temperature,
				mpr::StepLength,
				mpr::EffectiveStrainRate,
				mpr::ShearModulus,
				ppm::contract::SelfDiffusion
				),mpr::Coordinates2D> DislocationsGottsteinDeformation2DInputs;		

			typedef moc::Contract<TYPELIST_4(	
				mpr::Temperature,
				mpr::StepLength,
				ppm::contract::SelfDiffusion,
				ppm::contract::ZenerParam
				),mpr::Coordinates2D> DislocationsGottsteinStaticRecoveryInput2D;		

			typedef moc::Contract<TYPELIST_6(	
				mpr::Temperature,
				mpr::ShearModulus,
				ppm::contract::DislDensityImmobileInteriors,
				ppm::contract::DislDensityImmobileWalls,
				ppm::contract::DislDensityMobile,
				ppm::contract::SubgrainSize
				),mpr::Coordinates2D> YieldStressBySherstnevInput;

			typedef moc::Contract<TYPELIST_1( mpr::Temperature ),mpr::Coordinates2D> DiffusionBySherstnevInput;
			typedef moc::Contract<TYPELIST_1( mpr::Temperature ),mpr::Coordinates2D> ShearModulusBySherstnevInput;
			typedef moc::Contract<TYPELIST_1( EmptyType ),mpr::Coordinates2D> NoInput; // Should be mpr::null or something
      //typedef moc::Contract<EmptyType,mpr::Coordinates2D> NoInput; // Should be mpr::null or something
		}
	}
}

#endif // polycrystalMicrostructure_gottsteinContract_h__
