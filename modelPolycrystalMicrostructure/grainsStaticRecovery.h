
/*! \file grainsStaticRecovery.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef grainsStaticRecovery_h__
#define grainsStaticRecovery_h__
/*	include *********************************************************/

#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../models/models.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "gottsteinContract.h"
#include "grainsBase.h"

/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace mo = am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace polycrystal
		{
			// UserGuide: See in dislocationsDeformation.h  
			template<typename INPUT,typename STORAGE>
			class GrainsBySherstnevStaticRecovery
				: public GrainsEvolutionBase<STORAGE>
				, public mo::InputTypelist<INPUT>//mo::Inputs<am3::model::polycrystal::GrainsByShersnevStaticRecoveryInput2D>
			{
			public:
//				using mo::Inputs<am3::model::polycrystal::GrainsByShersnevStaticRecoveryInput2D>::Set;
				GrainsBySherstnevStaticRecovery(boost::shared_ptr<STORAGE> previousStepData):GrainsEvolutionBase<STORAGE>(previousStepData) {};

			protected:
				virtual void GetData(pnt::Point<mpr::Coordinates2D> * point);
				virtual void Calculate(pnt::Point<mpr::Coordinates2D> * point);
				void CalculateGrainSize();
				void CalculateRecrystalizedGrainSize();
				void CalculateNumberOfRecrystalizedNuclei(const ppm::contract::GrainSize& grainSize);
				void CalculateVolumeFractionOfRecrystallizedGrains(const ppm::contract::NumberOfRecrystalizedNuclei& numberOfRecrystalized, const ppm::contract::RecrystalizedGrainSize& recrystalizedGrainSize);
				void CalculateStoredEnergy();

				ppm::contract::GrainSize grainSizePrevious;
				ppm::contract::NumberOfRecrystalizedNuclei numberOfRecrystalizedNucleiPrevious;
				ppm::contract::RecrystalizedGrainSize recrystalizedGrainSizePrevious;
				ppm::contract::VolumeFractionOfRecrystallizedGrains volumeFractionOfRecrystalizedPrevious;

				ppm::contract::SelfDiffusion selfDiffusion;
				mpr::Temperature temperature;
				ppm::contract::DislDensityImmobileInteriors dislocationDensityInt;
				ppm::contract::DislDensityImmobileWalls dislocationDensityWals;
				ppm::contract::DislDensityMobile dislocationDensityMob;
				ppm::contract::DislDensitySum dislocationDensityAvg;
				ppm::contract::SubgrainSize subgrainSize;
 				mpr::StepLength timeStep;
				ppm::contract::ZenerParam zenerDrag;
				mpr::ShearModulus shearModulus;
				double mobilityHighAngleGrainBoundary;
			};
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////

template<typename INPUT,typename STORAGE>
void am3::model::polycrystal::GrainsBySherstnevStaticRecovery<INPUT,STORAGE>::GetData(pnt::Point<mpr::Coordinates2D> * point)
{
	actualPoint =  point->Copy();

	grainSizePrevious(previousStepData_->GetPreviousState<ppm::contract::GrainSize>());
	numberOfRecrystalizedNucleiPrevious(previousStepData_->GetPreviousState<ppm::contract::NumberOfRecrystalizedNuclei>());
	recrystalizedGrainSizePrevious(previousStepData_->GetPreviousState<ppm::contract::RecrystalizedGrainSize>());
	volumeFractionOfRecrystalizedPrevious(previousStepData_->GetPreviousState<ppm::contract::VolumeFractionOfRecrystallizedGrains>());

	timeStep = Get<mpr::StepLength,BaseCoordinates>::InPoint(point);
	selfDiffusion = Get<ppm::contract::SelfDiffusion,BaseCoordinates>::InPoint(point);
	temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point);
	dislocationDensityInt = Get<ppm::contract::DislDensityImmobileInteriors,BaseCoordinates>::InPoint(point);
	dislocationDensityWals = Get<ppm::contract::DislDensityImmobileWalls,BaseCoordinates>::InPoint(point);
	dislocationDensityMob = Get<ppm::contract::DislDensityMobile,BaseCoordinates>::InPoint(point);
	zenerDrag = Get<ppm::contract::ZenerParam,BaseCoordinates>::InPoint(point);
	shearModulus = Get<mpr::ShearModulus, BaseCoordinates>::InPoint(point);
	subgrainSize = Get<ppm::contract::SubgrainSize, BaseCoordinates>::InPoint(point);

	dislocationDensityAvg((dislocationDensityInt() + dislocationDensityMob() + dislocationDensityWals())/3);
	mobilityHighAngleGrainBoundary = selfDiffusion() * pow(b,2) / (kb * temperature());
}

template<typename INPUT,typename STORAGE>
void am3::model::polycrystal::GrainsBySherstnevStaticRecovery<INPUT,STORAGE>::Calculate(pnt::Point<mpr::Coordinates2D> * point)
{
	CalculateGrainSize();
	CalculateRecrystalizedGrainSize();
	CalculateNumberOfRecrystalizedNuclei(grainSizeNew);
	CalculateVolumeFractionOfRecrystallizedGrains(numberOfRecrystalizedNucleiNew, recrystalizedGrainSizeNew);
	CalculateStoredEnergy();
}

//////////////////////////////////////////////////////////////////////////
//  Here is the model! //
//////////////////////////////////////////////////////////////////////////

template<typename INPUT,typename STORAGE>
void am3::model::polycrystal::GrainsBySherstnevStaticRecovery<INPUT,STORAGE>::CalculateGrainSize()
{
	if ((0.324 / grainSizePrevious()) > zenerDrag())
	{
		grainSizeNew(
			(grainSizePrevious() + mobilityHighAngleGrainBoundary * (0.324 / grainSizePrevious() - zenerDrag()) * timeStep()) * (1 - volumeFractionOfRecrystalizedPrevious()) + recrystalizedGrainSizePrevious() * volumeFractionOfRecrystalizedPrevious()
			);
	}
	else
	{
		grainSizeNew(
			grainSizePrevious() * (1 - volumeFractionOfRecrystalizedPrevious()) + recrystalizedGrainSizePrevious() * volumeFractionOfRecrystalizedPrevious()
			);
	}
}

template<typename INPUT,typename STORAGE>
void am3::model::polycrystal::GrainsBySherstnevStaticRecovery<INPUT,STORAGE>::CalculateRecrystalizedGrainSize()
{
	recrystalizedGrainSizeNew(recrystalizedGrainSizePrevious());
}

template<typename INPUT,typename STORAGE>
void am3::model::polycrystal::GrainsBySherstnevStaticRecovery<INPUT,STORAGE>::CalculateNumberOfRecrystalizedNuclei(const ppm::contract::GrainSize& GrainSize)
{
	numberOfRecrystalizedNucleiNew(numberOfRecrystalizedNucleiPrevious());
}

template<typename INPUT,typename STORAGE>
void am3::model::polycrystal::GrainsBySherstnevStaticRecovery<INPUT,STORAGE>::CalculateVolumeFractionOfRecrystallizedGrains(const ppm::contract::NumberOfRecrystalizedNuclei& numberOfRecrystalized, const ppm::contract::RecrystalizedGrainSize& recrystalizedGrainSize)
{
	volumeFractionOfRecrystallizedGrainsNew (volumeFractionOfRecrystalizedPrevious());
}

template<typename INPUT,typename STORAGE>
void am3::model::polycrystal::GrainsBySherstnevStaticRecovery<INPUT,STORAGE>::CalculateStoredEnergy()
{	
	if (dislocationDensityAvg() > 1e11) // In Sherstnvev dislocation from previous step is used
	{
		storedEnergyNew(shearModulus() * pow(b,2) * (dislocationDensityAvg() - 1e11) + 3 * 0.324 / subgrainSize());
	}
}
#endif // grainsStaticRecovery_h__
