
/*! \file dislocationsBase.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef dislocationsBase_h__
#define dislocationsBase_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../models/point.h"
#include "../models/models.h"
#include "../problemPolycrystalMicrostructure/contracts.h"

/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace pnt = am3::model::point;
namespace mo = am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace polycrystal
		{
			// UserGuide: Base class for dislocation models
			template<typename STORAGE>
			class DislocationsBase 
				// UserGuide: derives from Contract
				: public ppm::contract::DislocationsContract2D
				// UserGuide: All computations are done when called first time for specific point. Next call for the same point only returns data.
				, public mo::ComputationsOnFirstCall<mpr::Coordinates2D>
			{
			public:
				// UserGuide: Initialization of constants (based on Sherstnev)
				DislocationsBase(boost::shared_ptr<STORAGE> previousStepData):previousStepData_(previousStepData),
					b1(0.123),
					b(0.286E-9), 
					kb(1.38E-23), 
					gammas(0.049),
					delta_cr(b1 / 316227.766016838),
					mtey(3.06),
					pois(0.33),
					qf(0.69)
				{};
				// UserGuide: Base classes should use virtual destructor
				virtual ~DislocationsBase(){};
				// UserGuide: GetState must be provided for all Contract
				ppm::contract::DislDensityMobile GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::DislDensityMobile *const );
				ppm::contract::DislDensityImmobileInteriors GetState( pnt::Point<mpr::Coordinates2D> *point,const ppm::contract::DislDensityImmobileInteriors *const );
				ppm::contract::DislDensityImmobileWalls GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::DislDensityImmobileWalls *const );
				ppm::contract::DislDensityMobileDeformed GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::DislDensityMobileDeformed *const );
				ppm::contract::DislDensityImmobileInteriorsDeformed GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::DislDensityImmobileInteriorsDeformed *const );
				ppm::contract::DislDensityImmobileWallsDeformed GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::DislDensityImmobileWallsDeformed *const );
				ppm::contract::SubgrainSize GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::SubgrainSize *const );
			protected:
				// UserGuide: Data from previous step (external storage)
				boost::shared_ptr<STORAGE> previousStepData_;

				// UserGuide: For each variable in Contract. Used for pre-computations on first call for point
				ppm::contract::DislDensityMobile dislDensityMobileNew;
				ppm::contract::DislDensityImmobileInteriors dislDensityImmobileInteriorsNew;
				ppm::contract::DislDensityImmobileWalls dislDensityImmobileWallsNew;
				ppm::contract::DislDensityMobileDeformed dislDensityMobileDeformedNew; 
				ppm::contract::DislDensityImmobileInteriorsDeformed dislDensityImmobileInteriorsDeformedNew;
				ppm::contract::DislDensityImmobileWallsDeformed dislDensityImmobileWallsDeformedNew;
				ppm::contract::SubgrainSize subgrainSizeNew;

				// UserGuide: Constatnts
				const double b;
				const double b1;
				const double mtey;
				const double pois;
				const double qf;
				const double kb;
				const double gammas;
				const double delta_cr;
			};

			//////////////////////////////////////////////////////////////////////////
			// Implementation
			//////////////////////////////////////////////////////////////////////////

			template<typename STORAGE>
			ppm::contract::DislDensityImmobileWallsDeformed am3::model::polycrystal::DislocationsBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::DislDensityImmobileWallsDeformed *const )
			{
				// UserGuide: Derived from ComputationsOnFirstCall. Must be called to run pre-computations if needed. Otherwise, only returns previously computed value
				CheckOrGetDataInPoint(point);
				return dislDensityImmobileWallsDeformedNew;
			}

			template<typename STORAGE>
			ppm::contract::DislDensityImmobileInteriorsDeformed am3::model::polycrystal::DislocationsBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::DislDensityImmobileInteriorsDeformed *const )
			{
				CheckOrGetDataInPoint(point);
				return dislDensityImmobileInteriorsDeformedNew;
			}

			template<typename STORAGE>
			ppm::contract::DislDensityMobileDeformed am3::model::polycrystal::DislocationsBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::DislDensityMobileDeformed *const )
			{
				CheckOrGetDataInPoint(point);
				return dislDensityMobileDeformedNew;
			}

			template<typename STORAGE>
			ppm::contract::DislDensityImmobileWalls am3::model::polycrystal::DislocationsBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::DislDensityImmobileWalls *const )
			{
				CheckOrGetDataInPoint(point);
				return dislDensityImmobileWallsNew;
			}

			template<typename STORAGE>
			ppm::contract::DislDensityImmobileInteriors am3::model::polycrystal::DislocationsBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> *point,const ppm::contract::DislDensityImmobileInteriors *const )
			{
				CheckOrGetDataInPoint(point);
				return dislDensityImmobileInteriorsNew;
			}

			template<typename STORAGE>
			ppm::contract::DislDensityMobile am3::model::polycrystal::DislocationsBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::DislDensityMobile *const )
			{
				CheckOrGetDataInPoint(point);
				return dislDensityMobileNew;
			}

			template<typename STORAGE>
			ppm::contract::SubgrainSize am3::model::polycrystal::DislocationsBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::SubgrainSize *const )
			{
				CheckOrGetDataInPoint(point);
				return subgrainSizeNew;
			}
		}
	}
}
#endif // dislocationsBase_h__
