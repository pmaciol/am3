
/*! \file dislocationsNoDeformation.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef dislocationsNoDeformation_h__
#define dislocationsNoDeformation_h__
/*	include *********************************************************/

#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../models/models.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "gottsteinContract.h"
#include "dislocationsBase.h"


/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace mo = am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace polycrystal
		{
			// UserGuide: See in dislocationsDeformation.h  
			template<typename INPUT, typename STORAGE>
			class DislocationsGottsteinNoDeformation
				: public DislocationsBase<STORAGE>
				, public mo::InputTypelist<INPUT>//<am3::model::polycrystal::DislocationsGottsteinNoDeformation2DInputs>
			{
			public:
				DislocationsGottsteinNoDeformation(boost::shared_ptr<STORAGE> previousStepData): DislocationsBase<STORAGE>::DislocationsBase(previousStepData) {};
				
			protected:
				virtual void GetData(pnt::Point<mpr::Coordinates2D> * point);
				virtual void Calculate(pnt::Point<mpr::Coordinates2D> * point);
				void CalculateDislDensityMobile();
				void CalculateDislDensityImmobileInteriors();
				void CalculateDislDensityImmobileWalls();
				void CalculateDislDensityMobileDeformed(const ppm::contract::DislDensityMobileDeformed& dislDensityMobileDeformed);
				void CalculateDislDensityImmobileInteriorsDeformed();
				void CalculateDislDensityImmobileWallsDeformed();
				void CalculateSubgrainSize(const double dislocationDensityAvg);

				ppm::contract::DislDensityMobileDeformed dislDensityMobileDeformedPrevious;
				ppm::contract::VolumeFractionOfRecrystallizedGrains volumeFractionOfRecrystallizedGrainsPrevious;
			};

			//////////////////////////////////////////////////////////////////////////
			// Implementation
			//////////////////////////////////////////////////////////////////////////

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinNoDeformation<INPUT,STORAGE>::Calculate( pnt::Point<mpr::Coordinates2D> * point )
			{
				CalculateDislDensityMobile();
				CalculateDislDensityImmobileInteriors();
				CalculateDislDensityImmobileWalls();
				CalculateDislDensityMobileDeformed(dislDensityMobileDeformedNew);
				CalculateDislDensityImmobileInteriorsDeformed();
				CalculateDislDensityImmobileWallsDeformed();
				CalculateSubgrainSize(dislDensityImmobileInteriorsNew() + dislDensityMobileNew() + dislDensityImmobileWallsNew());
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinNoDeformation<INPUT,STORAGE>::GetData( pnt::Point<mpr::Coordinates2D> * point )
			{
				actualPoint =  point->Copy();

				dislDensityMobileDeformedPrevious(previousStepData_->GetPreviousState<ppm::contract::DislDensityMobileDeformed>());
				volumeFractionOfRecrystallizedGrainsPrevious(previousStepData_->GetPreviousState<ppm::contract::VolumeFractionOfRecrystallizedGrains>());
			}

			//////////////////////////////////////////////////////////////////////////
			//  Here is the model! //
			//////////////////////////////////////////////////////////////////////////

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinNoDeformation<INPUT,STORAGE>::CalculateSubgrainSize(const double dislocationDensityAvg)
			{
				subgrainSizeNew(b1 / sqrt(dislocationDensityAvg));
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinNoDeformation<INPUT,STORAGE>::CalculateDislDensityImmobileWallsDeformed()
			{
				dislDensityImmobileWallsDeformedNew(0.0);
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinNoDeformation<INPUT,STORAGE>::CalculateDislDensityImmobileInteriorsDeformed()
			{
				dislDensityImmobileInteriorsDeformedNew(0.0);
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinNoDeformation<INPUT,STORAGE>::CalculateDislDensityMobileDeformed(const ppm::contract::DislDensityMobileDeformed& dislDensityMobileDeformed)
			{
				dislDensityMobileDeformedNew(dislDensityMobileDeformed());
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinNoDeformation<INPUT,STORAGE>::CalculateDislDensityImmobileWalls()
			{
				dislDensityImmobileWallsNew(0.0);
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinNoDeformation<INPUT,STORAGE>::CalculateDislDensityImmobileInteriors()
			{
				dislDensityImmobileInteriorsNew(0.0);
			}

			template<typename INPUT, typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinNoDeformation<INPUT,STORAGE>::CalculateDislDensityMobile()
			{
				dislDensityMobileNew(dislDensityMobileDeformedPrevious() * (1 - volumeFractionOfRecrystallizedGrainsPrevious()) + 1E+11 * volumeFractionOfRecrystallizedGrainsPrevious());
			}
		}
	}
}
#endif // dislocationsNoDeformation_h__
