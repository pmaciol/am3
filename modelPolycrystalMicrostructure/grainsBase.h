
/*! \file grainsBase.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef grainsBase_h__
#define grainsBase_h__

/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../models/point.h"
#include "../models/models.h"
#include "../problemPolycrystalMicrostructure/contracts.h"

/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace pnt = am3::model::point;
namespace mo = am3::model;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace polycrystal
		{
			// UserGuide: See in dislocationsDeformation.h  
			template<typename STORAGE>
			class GrainsEvolutionBase 
				: public ppm::contract::GrainsContract2D
				, public mo::ComputationsOnFirstCall<mpr::Coordinates2D>
			{
			public:
				
				GrainsEvolutionBase(boost::shared_ptr<STORAGE> previousStepData):previousStepData_(previousStepData),
					b1( 0.123),
					b(0.286E-9), 
					kb(1.38E-23), 
					prefactorForNucleation(0.09), 
					qsd(0.98),
					gammas(0.049)
				{};

				ppm::contract::GrainSize GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::GrainSize *const );
				ppm::contract::RecrystalizedGrainSize GetState( pnt::Point<mpr::Coordinates2D> *point,const ppm::contract::RecrystalizedGrainSize *const );
				ppm::contract::NumberOfRecrystalizedNuclei GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::NumberOfRecrystalizedNuclei *const );
				ppm::contract::VolumeFractionOfRecrystallizedGrains GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::VolumeFractionOfRecrystallizedGrains *const );
				ppm::contract::StoredEnergy GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::StoredEnergy *const );
			protected:
				boost::shared_ptr<STORAGE> previousStepData_;

				const double b;
				const double kb;
				const double b1;
				const double prefactorForNucleation;
				const double qsd;
				const double gammas;

				ppm::contract::GrainSize grainSizeNew;
				ppm::contract::RecrystalizedGrainSize recrystalizedGrainSizeNew;
				ppm::contract::NumberOfRecrystalizedNuclei numberOfRecrystalizedNucleiNew;
				ppm::contract::VolumeFractionOfRecrystallizedGrains volumeFractionOfRecrystallizedGrainsNew;
				ppm::contract::StoredEnergy storedEnergyNew;
			};
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////


template<typename STORAGE>
ppm::contract::GrainSize am3::model::polycrystal::GrainsEvolutionBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::GrainSize *const )
{
	CheckOrGetDataInPoint(point);
	return grainSizeNew;
}

template<typename STORAGE>
ppm::contract::RecrystalizedGrainSize am3::model::polycrystal::GrainsEvolutionBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> *point,const ppm::contract::RecrystalizedGrainSize *const )
{
	CheckOrGetDataInPoint(point);
	return recrystalizedGrainSizeNew;
}

template<typename STORAGE>
ppm::contract::NumberOfRecrystalizedNuclei am3::model::polycrystal::GrainsEvolutionBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::NumberOfRecrystalizedNuclei *const )
{
	CheckOrGetDataInPoint(point);
	return numberOfRecrystalizedNucleiNew;
}

template<typename STORAGE>
ppm::contract::VolumeFractionOfRecrystallizedGrains am3::model::polycrystal::GrainsEvolutionBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::VolumeFractionOfRecrystallizedGrains *const )
{
	CheckOrGetDataInPoint(point);
	return volumeFractionOfRecrystallizedGrainsNew;
}

template<typename STORAGE>
ppm::contract::StoredEnergy am3::model::polycrystal::GrainsEvolutionBase<STORAGE>::GetState( pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::StoredEnergy *const )
{
	CheckOrGetDataInPoint(point);
	return storedEnergyNew;
}
#endif // grainsBase_h__
