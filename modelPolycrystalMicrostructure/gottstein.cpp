#include <math.h>
#include "gottstein.h"

template<typename INPUT>
ppm::contract::ZenerParam am3::model::polycrystal::ZenerParamBySherstnev<INPUT>::GetState(pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::ZenerParam *const )
{
	ppm::contract::ZenerParam toRet;
	double zener = 1.5 * 0.324 * fv_disp / r_disp;
	toRet(zener);
	return toRet;
}

template<typename INPUT>
ppm::contract::SelfDiffusion am3::model::polycrystal::DiffusionBySherstnev<INPUT>::GetState(pnt::Point<mpr::Coordinates2D> * point, const ppm::contract::SelfDiffusion *const)
{
	ppm::contract::SelfDiffusion toRet;
	mpr::Temperature temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point);
	toRet (pow(b,2)*debye*exp(-qsd * 1.6e-19 / (kb * temperature()) ));
	return toRet;
}

template<typename INPUT>
ppm::contract::GrainBoundaryDiffusion am3::model::polycrystal::DiffusionBySherstnev<INPUT>::GetState(pnt::Point<mpr::Coordinates2D> * point, const ppm::contract::GrainBoundaryDiffusion *const)
{
	ppm::contract::GrainBoundaryDiffusion toRet;
	mpr::Temperature temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point);
	double grainDif = pow(b,2)*debye*exp(-qgb*1.6E-19/(kb * temperature()));
	toRet(grainDif);
	return toRet;
}

// template<typename INPUT>
// mpr::ShearModulus am3::model::polycrystal::ShearModulusBySherstnev<INPUT>::GetState(pnt::Point<mpr::Coordinates2D> * point, const mpr::ShearModulus *const)
// {
// 	mpr::ShearModulus toRet;
// 	mpr::Temperature temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point);
// 	//toRet((84.828E+9 - 4.06E+7 * temperature()) / (2 * (1 + pois))); // Sherstnev values
// 	toRet((32.102 - 0.0155 * temperature())*1e9); // Estimation based on Sherstnev phd, Abbildung 5-8:
// 	return toRet;
// }

template<typename INPUT>
mpr::YieldStress am3::model::polycrystal::YieldStressBySherstnev<INPUT>::GetState(pnt::Point<mpr::Coordinates2D> *point, const mpr::YieldStress *const)
{
 	mpr::Temperature temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point);
 	mpr::ShearModulus shearModulus = Get<mpr::ShearModulus,BaseCoordinates>::InPoint(point);
 	ppm::contract::DislDensityImmobileInteriors dislInterior = Get<ppm::contract::DislDensityImmobileInteriors,BaseCoordinates>::InPoint(point);
	ppm::contract::DislDensityImmobileWalls dislWals = Get<ppm::contract::DislDensityImmobileWalls,BaseCoordinates>::InPoint(point);
	ppm::contract::DislDensityMobile dislMobile = Get<ppm::contract::DislDensityMobile,BaseCoordinates>::InPoint(point);
	ppm::contract::SubgrainSize subgSize = Get<ppm::contract::SubgrainSize,BaseCoordinates>::InPoint(point);

	mpr::YieldStress result;

	double atomicMasses[8] = {63.546, 55.845, 24.3051, 54.938, 28.086, 51.996, 65.38, 26.982};  // cu, fe, mg, mn, si, cr, zn, al
	double misfits[8] = {2.69, 0, 2.06, 3.39, 0.9, 0, 0.32, 0.0};
	double fractions[8] = {0.1, 0.35, 4.6, 0.5, 0.3, 0.1, 0.25, 93.95};
	double atomicFractions[8];
	double sigma_ss_comp[8];
	
	double totalM = 0; 
	for (int i=0;i<8;i++)
	{
		totalM += fractions[i]*atomicMasses[i];
	}
 	totalM *= 0.01;
	for (int i=0;i<8;i++)
	{
		atomicFractions[i] = fractions[i]*totalM/atomicMasses[i];
	}
	for (int i=0;i<8;i++)
	{
		sigma_ss_comp[i] = pow(atomicFractions[i],beta)*pow(misfits[i],misfitCoeff);
		//sigma_ss_comp[i] = pow(fractions[i],beta)*pow(misfits[i],misfitCoeff);
	}
	double sigma_ss=0;
	for (int i=0;i<8;i++)
	{
		sigma_ss += sigma_ss_comp[i];
	}
	sigma_ss *= sigma_ss_coeff;
	double alpha = alpha_coeff * exp(-temperature_coeff*temperature());


	double sumOfDisl = dislInterior() + dislMobile() + dislWals();
	// double tmp = 10 + alpha * sigma_ss + mTey * 0.5 * shearModulus() * b * sqrt(sumOfDisl) / 1e6; //Sherstnev version
	double tmp = 1e6 + alpha * sigma_ss + mTey * 0.5 * shearModulus() * b * sqrt(sumOfDisl) / 1e6; //More realistic version
	tmp += 3 * shearModulus() * b * (1/subgSize()) / 1e6;
	result(tmp);
	return result;
}
