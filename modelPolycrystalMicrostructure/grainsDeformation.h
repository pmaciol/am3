
/*! \file grainsDeformation.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef grainsDeformation_h__
#define grainsDeformation_h__
/*	include *********************************************************/

#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../models/models.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "gottsteinContract.h"
#include "grainsBase.h"

/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace mo = am3::model;
namespace mpm = am3::model::polycrystal;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace polycrystal
		{
			// UserGuide: See in dislocationsDeformation.h  
// 			template<typename INPUT>
// 			class PrecipitationsAmountExponential
// 				: public contract::PrecipitationsOnlyAmountContract
// 				, public mo::InputTypelist<INPUT>

			template<typename STORAGE, typename INPUT = am3::databus::MockDatabus>
			class GrainsBySherstnevDeformation 
				: public GrainsEvolutionBase<STORAGE>
				, public mo::InputTypelist<INPUT>
				//, public mo::Inputs<am3::model::polycrystal::GrainsByShersnevDeformationInput2D>
			{
			public:
				GrainsBySherstnevDeformation(boost::shared_ptr<STORAGE> previousStepData): GrainsEvolutionBase<STORAGE>::GrainsEvolutionBase(previousStepData) {};
//				using mo::Inputs<am3::model::polycrystal::GrainsByShersnevDeformationInput2D>::Set;
				typedef typename mpm::GrainsByShersnevDeformationInput2D Input;
				typedef typename ppm::contract::GrainsContract2D Contract;
				/*typedef typename ppm::contract::GrainsContract2D Contract;*/

			protected:
				virtual void GetData(mpt::Point<mpr::Coordinates2D> * point);
				virtual void Calculate(mpt::Point<mpr::Coordinates2D> * point);
				void CalculateGrainSize();
				void CalculateRecrystalizedGrainSize();
				void CalculateNumberOfRecrystalizedNuclei();
				void CalculateVolumeFractionOfRecrystallizedGrains();
				void CalculateStoredEnergy();

				ppm::contract::GrainSize grainSizePrevious;

				ppm::contract::DislDensityImmobileInteriors dislocationDensityInt;
				ppm::contract::DislDensityImmobileWalls dislocationDensityWals;
				ppm::contract::DislDensityMobile dislocationDensityMob;
				ppm::contract::DislDensitySum dislocationDensityAvg;
				ppm::contract::SubgrainSize subgrainSize;
				mpr::EffectiveStrainRate efepse;
				mpr::StepLength timeStep;
				mpr::ShearModulus shearModulus;
			};
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////////

template<typename STORAGE, typename INPUT>
void am3::model::polycrystal::GrainsBySherstnevDeformation<STORAGE, INPUT>::GetData(mpt::Point<mpr::Coordinates2D> * point)
{
	actualPoint = point->Copy();

	grainSizePrevious(previousStepData_->GetPreviousState<ppm::contract::GrainSize>());

	timeStep = Get<mpr::StepLength,BaseCoordinates>::InPoint(point);
	shearModulus = Get<mpr::ShearModulus,BaseCoordinates>::InPoint(point);
	dislocationDensityInt = Get<ppm::contract::DislDensityImmobileInteriors,BaseCoordinates>::InPoint(point);
	dislocationDensityWals = Get<ppm::contract::DislDensityImmobileWalls,BaseCoordinates>::InPoint(point);
	dislocationDensityMob = Get<ppm::contract::DislDensityMobile,BaseCoordinates>::InPoint(point);
	efepse = Get<mpr::EffectiveStrainRate,BaseCoordinates>::InPoint(point);
	subgrainSize = Get<ppm::contract::SubgrainSize,BaseCoordinates>::InPoint(point);
	dislocationDensityAvg((dislocationDensityInt() + dislocationDensityMob() + dislocationDensityWals())/3);
}

template<typename STORAGE, typename INPUT>
void am3::model::polycrystal::GrainsBySherstnevDeformation<STORAGE, INPUT>::Calculate(mpt::Point<mpr::Coordinates2D> * point)
{
	CalculateGrainSize();
	CalculateNumberOfRecrystalizedNuclei();
	CalculateVolumeFractionOfRecrystallizedGrains();
	CalculateRecrystalizedGrainSize();
	CalculateStoredEnergy();
}

//////////////////////////////////////////////////////////////////////////
//  Here is the model! //
//////////////////////////////////////////////////////////////////////////

template<typename STORAGE, typename INPUT>
void am3::model::polycrystal::GrainsBySherstnevDeformation<STORAGE, INPUT>::CalculateGrainSize()
{	
	grainSizeNew(grainSizePrevious() + grainSizePrevious() * efepse() * timeStep());
}

template<typename STORAGE, typename INPUT>
void am3::model::polycrystal::GrainsBySherstnevDeformation<STORAGE, INPUT>::CalculateRecrystalizedGrainSize()
{
	recrystalizedGrainSizeNew(subgrainSize);
}

template<typename STORAGE, typename INPUT>
void am3::model::polycrystal::GrainsBySherstnevDeformation<STORAGE, INPUT>::CalculateNumberOfRecrystalizedNuclei()
{
	numberOfRecrystalizedNucleiNew(0.0);
}

template<typename STORAGE, typename INPUT>
void am3::model::polycrystal::GrainsBySherstnevDeformation<STORAGE, INPUT>::CalculateVolumeFractionOfRecrystallizedGrains()
{
	volumeFractionOfRecrystallizedGrainsNew(0.0);
}

template<typename STORAGE, typename INPUT>
void am3::model::polycrystal::GrainsBySherstnevDeformation<STORAGE, INPUT>::CalculateStoredEnergy()
{	
	if (dislocationDensityAvg() > 1e11) // In Sherstnvev dislocation from previous step is used
	{
		storedEnergyNew(shearModulus() * pow(b,2) * (dislocationDensityAvg() - 1e11) + 3 * 0.324 / subgrainSize());
	}
	else 
	{
		storedEnergyNew(0.0);
	}
}
#endif // grainsDeformation_h__
