
/*! \file dislocationsDeformation.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef dislocationsDeformation_h__
#define dislocationsDeformation_h__
/*	include *********************************************************/

#include <boost/math/constants/constants.hpp>
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../models/models.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "gottsteinContract.h"
#include "dislocationsBase.h"

/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace mo = am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace polycrystal
		{
			// UserGuide: Implementation of dislocation model. No (or very small) deformation. Applicability is defined in Knowledge Based System
			template<typename INPUT,typename STORAGE>
			class DislocationsGottsteinDeformation
				: public DislocationsBase<STORAGE>
				// UserGuide: Derivation from Inputs provider
				, public mo::InputTypelist<INPUT>// <am3::model::polycrystal::DislocationsGottsteinDeformation2DInputs>
			{
			public:
				// UserGuide: Setting storage
				DislocationsGottsteinDeformation(boost::shared_ptr<STORAGE> previousStepData)
					: DislocationsBase<STORAGE>::DislocationsBase(previousStepData) {};

			protected:
				// UserGuide: Derived from BaseClass::ComputationsOnFirstCall. See implementation for guides
				virtual void GetData(pnt::Point<mpr::Coordinates2D> * point);
				// UserGuide: Derived from BaseClass::ComputationsOnFirstCall. See implementation for guides
				virtual void Calculate(pnt::Point<mpr::Coordinates2D> * point);

				// UserGuide: Used in Calculate method. See implementation for guides
				void CalculateDislDensityMobile();
				void CalculateDislDensityImmobileInteriors();
				void CalculateDislDensityImmobileWalls();
				void CalculateDislDensityMobileDeformed();
				void CalculateDislDensityImmobileInteriorsDeformed();
				void CalculateDislDensityImmobileWallsDeformed();
				void CalculateSubgrainSize(const double dislocationDensityAvg);

				// UserGuide: Used in GetData method. Stores data from previous step
 				ppm::contract::DislDensityMobile dislDensityMobilePrevious;
 
				// UserGuide: Supporting variables, initialized in GetData method
 				mpr::Temperature temperature;
 				mpr::EffectiveStrainRate efepse;
 				ppm::contract::SelfDiffusion selfDiffusion;
 				mpr::ShearModulus shearModulus;
 				mpr::StepLength timeStep;
			};
			//////////////////////////////////////////////////////////////////////////
			// Implementation
			//////////////////////////////////////////////////////////////////////////

			template<typename INPUT,typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinDeformation<INPUT,STORAGE>::Calculate( pnt::Point<mpr::Coordinates2D> * point )
			{
				// UserGuide: All variables from Contract (defined in BaseClass) must be computed here. 
				// UserGuide: If any variable from Contract is used in Calculate*** method, should be sent with method parameter. It shows dependencies between methods.
				// UserGuide: An order of calls depends on parameters. If no variables from Contract is used in Calculate***, it can be called anytime.
				// UserGuide: If Calculate*** uses one of those variables, it must be called AFTER used variables are calculated with its own Calculate***.
				// UserGuide: Ufffff ...
				CalculateDislDensityMobile();
				CalculateDislDensityImmobileInteriors();
				CalculateDislDensityImmobileWalls();
				CalculateDislDensityMobileDeformed();
				CalculateDislDensityImmobileInteriorsDeformed();
				CalculateDislDensityImmobileWallsDeformed();
				// UserGuide: This one must be called after CalculateDislDensityMobile(), CalculateDislDensityImmobileInteriors() and CalculateDislDensityImmobileWalls
				CalculateSubgrainSize(dislDensityImmobileInteriorsNew() + dislDensityMobileNew() + dislDensityImmobileWallsNew());
			}

			template<typename INPUT,typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinDeformation<INPUT,STORAGE>::GetData( pnt::Point<mpr::Coordinates2D> * point )
			{
				// UserGuide: GetData is called when no computations has been done for given point. Settings actualPoint means "Contract variables are computed for this point"
				actualPoint =  point->Copy();

				// UserGuide: Getting previous step data
				dislDensityMobilePrevious(previousStepData_->GetPreviousState<ppm::contract::DislDensityMobile>());

				// UserGuide: Getting data from Inputs (from databus)
				timeStep = Get<mpr::StepLength,BaseCoordinates>::InPoint(point);
				temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point);
				efepse = Get<mpr::EffectiveStrainRate,BaseCoordinates>::InPoint(point);
				selfDiffusion = Get<ppm::contract::SelfDiffusion,BaseCoordinates>::InPoint(point);
				shearModulus = Get<mpr::ShearModulus,BaseCoordinates>::InPoint(point);
			}

			//////////////////////////////////////////////////////////////////////////
			//  Here is the model itself! 
			//////////////////////////////////////////////////////////////////////////

			template<typename INPUT,typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinDeformation<INPUT,STORAGE>::CalculateDislDensityImmobileWallsDeformed()
			{
				dislDensityImmobileWallsDeformedNew(0.0);
			}

			template<typename INPUT,typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinDeformation<INPUT,STORAGE>::CalculateDislDensityImmobileInteriorsDeformed()
			{
				dislDensityImmobileInteriorsDeformedNew(0.0);
			}

			template<typename INPUT,typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinDeformation<INPUT,STORAGE>::CalculateDislDensityMobileDeformed()
			{
				dislDensityMobileDeformedNew(dislDensityMobilePrevious());
			}

			template<typename INPUT,typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinDeformation<INPUT,STORAGE>::CalculateDislDensityImmobileWalls()
			{
				dislDensityImmobileWallsNew(0.0);
			}

			template<typename INPUT,typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinDeformation<INPUT,STORAGE>::CalculateDislDensityImmobileInteriors()
			{
				dislDensityImmobileInteriorsNew(0.0);
			}

			template<typename INPUT,typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinDeformation<INPUT,STORAGE>::CalculateDislDensityMobile()
			{
				const double b2 = 74.9 - 0.09 * temperature();
				const double b3 = 0.03 * efepse() + 0.01;
				const double a1 = mtey / (b * b1);
				const double dann = shearModulus() * pow(b,4) / (2 * boost::math::constants::pi<double>() * (1 - pois) * (qf * 1E-19));
				const double a2 = b2 * 2 * dann * mtey / b;
				const double a3 = b3 * 2 * selfDiffusion() * shearModulus() * pow(b,3)/(kb * temperature());

				const int steps = 100;
				const double h = timeStep() / steps;
				double roh = dislDensityMobilePrevious();
				double droh = 0;
				for (int i = 0; i<steps; i++)
				{
					double t1 = (a1 * sqrt(roh) - a2 * roh);
					double t2 = t1 * efepse() * h;
					double t3 = (pow(roh,2) - 1e22);
					double t4 = a3 * t3 * h;
					droh = t2 - t4;
					roh += droh;
				}
				(roh > dislDensityMobilePrevious()) ? dislDensityMobileNew(roh) : dislDensityMobileNew = dislDensityMobilePrevious;
			}

			template<typename INPUT,typename STORAGE>
			void am3::model::polycrystal::DislocationsGottsteinDeformation<INPUT,STORAGE>::CalculateSubgrainSize(const double dislocationDensityAvg)
			{
				subgrainSizeNew(b1 / sqrt(dislocationDensityAvg));
			}

		}
	}
}

#endif // dislocationsDeformation_h__
