
/*! \file softwareFemMock.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareFemMock_softwareFemMock_h__
#define am3softwareFemMock_softwareFemMock_h__
/*	include *********************************************************/

#include <vector>
#include <iostream>

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace femmock
		{
      struct NodeData
      {
        unsigned id;
        double time;
        double temperature;
        double variable;
      };

      class MicroScaleModel
      {
      public:
        virtual void InitWithData(const NodeData & data) 
        {
          std::cout << "Initializing" << std::endl;
        }  //Replace or implement derived class
        virtual void CalculateWithData(const NodeData & data)
        {}  //Replace or implement derived class
        virtual double GetValue(const unsigned nodeId) // Node is not MPI process id!
        {return 0; }  //Replace or implement derived class
        virtual void Finalize()
        {}  //Replace or implement derived class
      };

      template<typename MICROSCALE_MODEL>
      class FemMock
      {
      public:
        void Init(int argc, char **argv);
        void Finalize();
        void Run();
        void SetMicroScaleModel(MICROSCALE_MODEL* microModel){ microModel_ = microModel; }
        typedef typename MICROSCALE_MODEL::InputData InputData;

      private:
        void FirstStep(InputData& data);
        void NextStep(InputData& data);

        std::vector<typename MICROSCALE_MODEL::InputData> nodeData;
        MICROSCALE_MODEL* microModel_;

      };

		} //femmock
	} //software
} //am3
#endif // am3softwareFemMock_softwareFemMock_h__
