
/*! \file softwareFemMock.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include <windows.h>
#include <limits>
#include <algorithm>
#include <functional>
#include "softwareFemMock.h"
#include "mockMpiMicroscale.h"

/*	using ***********************************************************/

namespace sfm = am3::software::femmock;
namespace mom = am3::model::mock;

/*	extern **********************************************************/



/*	classes *********************************************************/

// int main(int argc, char **argv)
// {
//   sfm::FemMock<mom::MicroScaleMpiModel> femMock;
//   //sfm::MicroScaleModel* microModelWithMpi = new sfm::MicroScaleModel;
//   mom::MicroScaleMpiModel* microModelWithMpi = new mom::MicroScaleMpiModel;
//   femMock.SetMicroScaleModel(microModelWithMpi);
//   femMock.Init(argc, argv);
//   femMock.Run();
//   femMock.Finalize();
//   delete microModelWithMpi;
// }


template<typename MICROSCALE_MODEL>
void am3::software::femmock::FemMock<MICROSCALE_MODEL>::Init(int argc, char **argv)
{
  microModel_->Init(argc, argv);
}

template<typename MICROSCALE_MODEL>
void am3::software::femmock::FemMock<MICROSCALE_MODEL>::Finalize()
{
  MPI_Finalize();
}


template<typename MICROSCALE_MODEL>
void am3::software::femmock::FemMock<MICROSCALE_MODEL>::FirstStep(InputData& data)
{
  data.temperature -= 10.0 * data.id; // Cooling -> delta T depends on id just to meake difference
  microModel_->CalculateWithData(data);
  data.variable = microModel_->GetValue(data.id);
}


template<typename MICROSCALE_MODEL>
void am3::software::femmock::FemMock<MICROSCALE_MODEL>::NextStep(InputData& data)
{
  data.temperature -= 10.0 * data.id; // Cooling -> delta T depends on id just to meake difference
  microModel_->CalculateWithData(data);
  data.variable = microModel_->GetValue(data.id);
}



template<typename MICROSCALE_MODEL>
void am3::software::femmock::FemMock<MICROSCALE_MODEL>::Run()
{
  // Initialization
  double time = 0;
  double temperature = 700.0;
  double var = std::numeric_limits<double>::quiet_NaN();
  for (unsigned i = 1; i <= 10; i++)
  {
    InputData data = { i * 2 , time, temperature, var }; // node Id is unrestricted, corresponds to node numbers in FEM
    nodeData.push_back(data);
  }

  

  for (InputData& e : nodeData)
  {
    microModel_->InitWithData(e);
  }

  // First Step
  time = 0.1;


  for (InputData& e : nodeData)
  {
    this->FirstStep(e);
  }

  // Next Step
  time = 0.2;

  for (InputData& e : nodeData)
  {
    this->NextStep(e);
  }

  microModel_->Finalize();

}
