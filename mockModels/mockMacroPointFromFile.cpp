/*! \file mockMacroPointFromFile.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include <fstream>
#include "mockMacroPointFromFile.h"

/*	using ***********************************************************/

namespace mmk = am3::model::mock;

/*	extern **********************************************************/



/*	classes *********************************************************/


mmk::MacroDataFromFile::MacroDataFromFile(std::string fileWithPath)
{
  presentStep = allDataContainer.begin();
  std::fstream fs;
  fs.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  try
  {
    fs.open(fileWithPath);
    try
    {
      Data d;
      double read;
      while (!fs.eof())
      {
        fs >> read; d.time(read);
        fs >> read; d.temperature(read);
        fs >> read; d.effStrainRate(read);
        allDataContainer.push_back(d);
        presentStep = allDataContainer.begin();
        if (!fs) break;
      }

    }
    catch (std::ifstream::failure e)
    {
      std::cerr << "Error in file; could be white space after last value" << fileWithPath << std::endl;
    }
  }
  catch (std::ifstream::failure e)
  {
    std::cerr << "File cannot be opened " << fileWithPath << std::endl;
  }




}

mpr::ElementNumber mmk::MacroDataFromFile::GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::ElementNumber *const)
{
  return elNumber;
}

mpr::EffectiveStrainRate mmk::MacroDataFromFile::GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::EffectiveStrainRate *const)
{
  return presentStep->effStrainRate;
}

mpr::Temperature mmk::MacroDataFromFile::GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::Temperature *const)
{
  return presentStep->temperature;
}

mpr::Time mmk::MacroDataFromFile::GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::Time *const)
{
  return presentStep->time;
}

mpr::StepLength mmk::MacroDataFromFile::GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::StepLength *const)
{
  return stepLength;
}

bool mmk::MacroDataFromFile::NextStep()
{
  if ((presentStep + 1) < allDataContainer.end())
  {
    ++presentStep;
    return true;
  }
  else
  {
    return false;
  }
}

