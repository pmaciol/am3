
/*! \file mockMicroscale.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3mockModels_mockMicroscale_h__
#define am3mockModels_mockMicroscale_h__
/*	include *********************************************************/

#include <iostream>

/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace mock
		{
      struct MicroModelData
      {
        unsigned id;
        double time;
        double temperature;
        double variable;
      };

      class MicroScaleModel
      {
      public:
        typedef MicroModelData InputData;

        virtual void InitWithData(const InputData & data)
        {
          std::cout << "Initialize, node = " << data.id << std::endl;
        }
        virtual void CalculateWithData(const InputData & data)
        {
          std::cout << "Calculate, node = " << data.id << " time = "<< data.time<<std::endl;
        } 
        virtual double GetValue(const unsigned nodeId)
        {
          return 0;
        }
        virtual void Finalize()
        {
          std::cout << "Finalize"<< std::endl;
        } 
      };
		} //mock
	} //model
} //am3
#endif // am3mockModels_mockMicroscale_h__
