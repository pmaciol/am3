
/*! \file mockFEMforMpiMatCalc.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include <utility>
#include <chrono>
#include <iostream>
#include <fstream>
#include <sstream>
#include "../communicationMPI/controller.h"
#include "../communicationMPI/instance.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../models/predefinedContracts.h"

/*	using ***********************************************************/

namespace cco = am3::common::communication;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;
namespace mda = am3::model::datapacks;

/*	extern **********************************************************/



/*	classes *********************************************************/

int main(int argc, char **argv)
{


// 
// 
//   void DeformMeshReceiver::SetTemperaturesInNodes(const int materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const int nodeStart, const int nodeEnd)
//   {


  int nodes;
  int steps;

  std::istringstream iss(argv[1]);
  iss >> nodes;
  std::istringstream iss2(argv[2]);
  iss2 >> steps;



  typedef cco::MpiInstance<mda::Coord2dTimeTemp, int, mpt::ComputedPoint<mpr::Coordinates2D> > Instance;
  cco::Controller<mda::Coord2dTimeTemp, int, Instance, mpt::ComputedPoint<mpr::Coordinates2D>, mpr::Coordinates2D > cont;

//  int nodes;
//   std::cout << "Nodes :";
//   std::cin >> nodes;

//  int steps;
//   std::cout << "Steps :";
//   std::cin >> steps;

  
  const int nodes_max = 20;
  const int steps_max = 20;
//   for (nodes = 1; nodes <= nodes_max; nodes++)
//   {
//     int steps;
//     for (steps = 1; steps <= steps_max; steps += 2)
//     {


      const double endTime = 1.0;
      //    const int steps = 10;
      double timeStep = endTime / steps;

      const int materialId = 1;
      double time = 0;

      //double temperatures[steps][nodes] =  {{600, 600, 600}, { 500, 500 , 500}};

      double temperatureStart = 900;
      double temperature = temperatureStart;
      double temperatarureDelta = -250;

      const int nodeStart = 0;
      const int nodeEnd = nodeStart + nodes - 1;

      std::chrono::time_point<std::chrono::system_clock> start, end;

      start = std::chrono::system_clock::now();
      cont.Init(argc, argv);


      for (int i = 0; i < steps; i++)
      {


        cont.WaitForAll();
        for (int j = nodeStart; j <= nodeEnd; j++)
        {
          const mpr::Coordinates2D coord = mpr::Coordinates2D(materialId, j); //This is a fake!

          // mda::Coord2dTimeTemp val(coord, time, temperatures[i][j]);
          mda::Coord2dTimeTemp val(coord, time, temperature);

          const mpr::Time t = mpr::Time(time);
          mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);

          std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair(point, val);
          cont.Run(pair);
          cont.WaitForAll();
        }
        time += timeStep;
        temperature += temperatarureDelta * timeStep;
      }

      cont.Finalize();

      end = std::chrono::system_clock::now();

      std::chrono::duration<double> elapsed_seconds = end - start;
      std::time_t end_time = std::chrono::system_clock::to_time_t(end);

      std::ofstream myfile;
      myfile.open("example.txt", std::fstream::app);


      myfile << "Steps: " << steps << " Nodes: " << nodes << " elapsed time: " << elapsed_seconds.count() << "s\n";
      myfile.close();
//     }
//   }

/*    sdm::OneStepOneMeshTemperatureTime::SetTemperaturesInNodes(materialId, time, timeStep, temperatures, deltaTemperatures, nodeStart, nodeEnd);
    std::vector<mmk::MicroModelData> dataToRun;
    //   if (IsInitialization(time))
    //   {
    cont.WaitForAll();
    for (int i = nodeStart; i <= nodeEnd; i++)
    {
      //      PushData(i, GetOldTemperature(i).Value(), GetTime().Value(), dataToRun);

      const mpr::Coordinates2D coord = mpr::Coordinates2D(materialId, i); //This is a fake!
      mda::Coord2dTimeTemp val(coord, time, temperatures[i]);

      const mpr::Time t = mpr::Time(time);
      mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);

      std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair(point, val);
      cont.Run(pair);
      //     }
      //     
      //     
      // 

      //    runner_->Initialize(dataToRun);
      //     Initialized();
      //   }
      //   else
      //   {
      //     for (int i = nodeStart; i <= nodeEnd; i++)
      //     {
      //       PushData(i, GetNewTemperature(i).Value(), GetNewTime().Value(), dataToRun);
      //     }
      //     runner_->Run(dataToRun);

    }
  }


  const mpr::Coordinates2D coord1 = mpr::Coordinates2D(0.5, 0.5);
  const mpr::Coordinates2D coord2 = mpr::Coordinates2D(1.5, 1.5);
  const mpr::Coordinates2D coord3 = mpr::Coordinates2D(2.5, 2.5);
  const mpr::Time t = mpr::Time(0);
  const mpr::Time t2 = mpr::Time(0.1);
  const mpr::Time t3 = mpr::Time(2);
  mpt::ComputedPoint<mpr::Coordinates2D> point1 = mpt::ComputedPoint<mpr::Coordinates2D>(coord1, t);
  mpt::ComputedPoint<mpr::Coordinates2D> point2 = mpt::ComputedPoint<mpr::Coordinates2D>(coord2, t);
  mpt::ComputedPoint<mpr::Coordinates2D> point1a = mpt::ComputedPoint<mpr::Coordinates2D>(coord1, t2);
  mpt::ComputedPoint<mpr::Coordinates2D> point1b = mpt::ComputedPoint<mpr::Coordinates2D>(coord1, t3);
  mpt::ComputedPoint<mpr::Coordinates2D> point2a = mpt::ComputedPoint<mpr::Coordinates2D>(coord2, t2);
  mpt::ComputedPoint<mpr::Coordinates2D> point2b = mpt::ComputedPoint<mpr::Coordinates2D>(coord2, t3);
  mpt::ComputedPoint<mpr::Coordinates2D> point3 = mpt::ComputedPoint<mpr::Coordinates2D>(coord3, t);

  typedef cco::MpiInstance<mda::Coord2dTimeTemp, int, mpt::ComputedPoint<mpr::Coordinates2D> > Instance;
//  Instance inst;

  cco::Controller<mda::Coord2dTimeTemp, int, Instance, mpt::ComputedPoint<mpr::Coordinates2D>, mpr::Coordinates2D > cont;

  mpr::Temperature temp, temp2, temp3;
  temp(600);
  temp2(600);
  temp3(700);
  mda::Coord2dTimeTemp val1(coord1, t, temp);
  mda::Coord2dTimeTemp val2(coord2, t, temp);
  mda::Coord2dTimeTemp val1a(coord1, t2, temp2);
  mda::Coord2dTimeTemp val1b(coord1, t3, temp3);
  mda::Coord2dTimeTemp val2a(coord2, t2, temp2);
  mda::Coord2dTimeTemp val2b(coord2, t3, temp3);
  mda::Coord2dTimeTemp val3(coord3, t, temp);
  std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair1(point1, val1);
  std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair2(point2, val2);
  std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair1a(point1a, val1a);
  std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair1b(point1b, val1b);
  std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair2a(point2a, val2a);
  std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair2b(point2b, val2b);
  std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTemp  > pair3(point3, val3);

  std::chrono::time_point<std::chrono::system_clock> start, end;
  
  start = std::chrono::system_clock::now();
  cont.Init(argc, argv);
  
  
  cont.WaitForAll();
    cont.Run(pair1);
    
  cont.Run(pair1);
  cont.WaitForAll();
 cont.Run(pair2);
 cont.WaitForAll();
    cont.Run(pair1a);
    cont.WaitForAll();
    cont.Run(pair2a);
    cont.WaitForAll();

  cont.Run(pair1b);
  cont.WaitForAll();
  cont.Run(pair2b);
   cont.WaitForAll();
   */
//     cont.Finalize();
// 
//     end = std::chrono::system_clock::now();
// 
//     std::chrono::duration<double> elapsed_seconds = end - start;
//     std::time_t end_time = std::chrono::system_clock::to_time_t(end);
// 
//     std::cout << "finished computation at " << std::ctime(&end_time)
//       << "elapsed time: " << elapsed_seconds.count() << "s\n";

//   cont.Get(point1);
//   cont.Get(point2);
 // cont.Get(point1a);
  return 0;
}
