
/*! \file mockMacroPointFromFile.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3mockModels_mockMacroPointFromFile_h__
#define am3mockModels_mockMacroPointFromFile_h__
/*	include *********************************************************/

#include "../models/predefinedContracts.h"
#include "../models/point.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace mcd = am3::model::contract::defined;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace mock
    {
      class MacroDataFromFile : public mcd::MacroWithTimeContract
      {
      public:
        MacroDataFromFile(std::string fileWithPath);
        virtual mpr::Time GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::Time *const);
        virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::Temperature *const);
        virtual mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::EffectiveStrainRate *const);
        virtual mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::StepLength *const);
        virtual mpr::ElementNumber GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::ElementNumber *const);
        bool NextStep();
      protected:
        struct Data
        {
          mpr::Time time;
          mpr::Temperature temperature;
          mpr::EffectiveStrainRate effStrainRate;
        };
        std::vector<Data> allDataContainer;
        std::vector<Data>::iterator presentStep;
        mpr::ElementNumber elNumber;
        mpr::StepLength stepLength;
      };
    } //mock	
  } //model
	
} //am3
#endif // am3mockModels_mockMacroPointFromFile_h__
