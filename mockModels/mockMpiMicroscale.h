
/*! \file mockMpiMicroscale.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3mockModels_mockMpiMicroscale_h__
#define am3mockModels_mockMpiMicroscale_h__
/*	include *********************************************************/

#include <mpi.h>

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace mock
    {
      class MpiController
      {
      public:
        void Init(int argc, char* argv[])
        {
          int par_rank = 0, par_size = 0;
          MPI_Init(&argc, &argv);
          MPI_Comm_rank(MPI_COMM_WORLD, &par_rank);
          MPI_Comm_size(MPI_COMM_WORLD, &par_size);
        }
      };

      struct MicroMpiModelData
      {
        unsigned id;
        double time;
        double temperature;
        double variable;
      };

      class MicroScaleMpiModel
      {
      public:
        typedef MicroMpiModelData InputData;
        typedef MpiController Controller;

        void Init(int argc, char* argv[])
        {
          controller_.Init(argc, argv);
        }
        virtual void InitWithData(const InputData & data)
        {
          std::cout << "Initialize, node = " << data.id << std::endl;
          int com = 1;
          double coordinates[2] = { 0.5, 0.5 }; double time = 0.0; double temperature = 700.0;
          int world_rank;
          MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
          int world_size;
          MPI_Comm_size(MPI_COMM_WORLD, &world_size);
          std::cout << "world_rank = " << world_rank << std::endl;
          if (world_rank == 0)
          {
            for (int i = 1; i < world_size; i++)
            {
              std::cout <<"World size "<< world_size<< " Sending data to " << i << std::endl;
              MPI_Send(&com, 1, MPI_INT, i, 0, MPI_COMM_WORLD);
              MPI_Send(coordinates, 2, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
              MPI_Send(&time, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
              MPI_Send(&temperature, 1, MPI_DOUBLE, i, 0, MPI_COMM_WORLD);
            }
          }
          
        }
        virtual void CalculateWithData(const InputData & data)
        {
          std::cout << "Calculate, node = " << data.id << " time = " << data.time << std::endl;
        }
        virtual double GetValue(const unsigned nodeId)
        {
          return 0;
        }
        virtual void Finalize()
        {
          std::cout << "Finalize" << std::endl;
        }
      protected:
        MpiController controller_;
      };
    } //mock
  } //model
} //am3

#endif // am3mockModels_mockMpiMicroscale_h__
