include_directories (../)
add_executable(am3mockModels
softwareFemMock.h softwareFemMock.cpp
mockMicroscale.h
mockMacroPointFromFile.h mockMacroPointFromFile.cpp
mockMpiMicroscale.h
mockFEMforMpiMatCalc.cpp
)

target_link_libraries(am3mockModels 
msmpi.lib
am3communicationMPI
)

SET_TARGET_PROPERTIES(am3mockModels PROPERTIES LINKER_LANGUAGE CXX)

IF($<CXX_COMPILER_ID:MSVC>)
  SET(CMAKE_EXE_LINKER_FLAGS /NODEFAULTLIB:\"LIBCMTD.lib;libcpmtd.lib;LIBCMT.lib;libcpmt.lib;\")
ENDIF()

