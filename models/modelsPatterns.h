
/*! \file modelsPatterns.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       Some common components for ModelScheme
 * \details   	
*********************************************************************/
#ifndef modelsPatterns_h__
#define modelsPatterns_h__
/*	include *********************************************************/

#include "../models/point.h"

/*	using ***********************************************************/

namespace mpt = am3::model::point;
using boost::shared_ptr;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace patterns
		{
      template<typename POINT>
      class InitTechPattern
      {
      public:
        typedef POINT Point;
        virtual void Exec(Point* point)=0;
        virtual bool Needed()=0;
      };

      template<typename POINT>
      class InitModelPattern
      {
      public:
        typedef POINT Point;
        virtual void Exec(Point* point)=0;
        virtual bool Needed()=0;
      };

      template <typename CONTRACT>
      class FirstStepPattern
      {
      public:
        typedef mpt::Point<typename CONTRACT::CoordinatesType> Point;

        virtual void Exec(Point* point)=0;
        virtual bool Needed()=0;
        virtual shared_ptr<CONTRACT> GetResults()=0;
      };

      template <typename CONTRACT>
      class NextStepPattern
      {
      public:
        typedef mpt::Point<typename CONTRACT::CoordinatesType> Point;

        virtual void Exec(Point* point)=0;
        virtual bool Available()=0;
        virtual shared_ptr<CONTRACT> GetResults()=0;
      };

      template <typename CONTRACT>
      class LastStepPattern
      {
      public:
        typedef mpt::Point<typename CONTRACT::CoordinatesType> Point;
        virtual void Exec(Point* point)=0;
        virtual bool Available()=0;
        virtual shared_ptr<CONTRACT> GetResults()=0;
      };

      template<typename T_INPUTS, typename T_OUTPUTS>
      struct NeverNeeded
        : public InitTechPattern<typename mpt::Point<typename T_INPUTS::CoordinatesType>>
      {
        inline bool Needed() {return false;}
        inline void Exec(Point* point) {}
        inline static void Set(shared_ptr<T_INPUTS>){};
        inline static shared_ptr<T_OUTPUTS> GetResults(){shared_ptr<T_OUTPUTS> toRet; return toRet;};
      };

      template<typename T_INPUTS, typename T_OUTPUTS>
      struct NeverAvailable
        : public NextStepPattern<T_OUTPUTS>
      {
        inline bool Available() {return false;}
        inline void Exec(Point* point) {}
        inline static void Set(shared_ptr<T_INPUTS>){};
        inline shared_ptr<T_OUTPUTS> GetResults(){shared_ptr<T_OUTPUTS> toRet; return toRet;};
      };

      ///
      /// Can be used when Contract of ModelScheme is equal to Contracts of each components of ModelScheme
      template<typename CONTRACT>
      class TransparentResults
//        : public CONTRACT
      {
      protected:
        shared_ptr<CONTRACT> results_;
      public:
        typedef CONTRACT Contract;
        void SetModelComputations(shared_ptr<CONTRACT> results) {results_=results;}
        template<typename VAR>
        VAR Get(mpt::Point<typename CONTRACT::CoordinatesType>* point)
        {
          const VAR* trait = nullptr;
          return results_->GetState(point,trait);
        };

      };
		} //patterns
	} //model
} //am3
#endif // modelsPatterns_h__
