
/*! \file predefinedContracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3models_predefinedContracts_h__
#define am3models_predefinedContracts_h__
/*	include *********************************************************/

#include <tuple>
#include "../models/macrosSimplifiedModels.h"
#include "../models/description.h"


/*	using ***********************************************************/

namespace mpr = am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace contract
		{
			namespace defined
			{
        CONTRACT(TemperatureContract, 1, mpr::Coordinates2D, mpr::Temperature);
        CONTRACT(TimeContract, 1, mpr::Coordinates2D, mpr::Time);
        CONTRACT(MacroContract, 3, mpr::Coordinates2D, mpr::Temperature, mpr::EffectiveStrainRate, mpr::StepLength);
        CONTRACT(MacroWithTimeContract, 5, mpr::Coordinates2D, mpr::Temperature, mpr::EffectiveStrainRate, mpr::StepLength, mpr::Time, mpr::ElementNumber);
        CONTRACT(MacroStressContract2D, 1, mpr::Coordinates2D, mpr::YieldStress);
        CONTRACT(MacroStressDstrainDstrainrateContract2D, 3, mpr::Coordinates2D, mpr::YieldStress, mpr::StressDStrain, mpr::StressDStrainRate);
        CONTRACT(MacroStressMockDstrainDstrainrateContract2D, 3, mpr::Coordinates2D, mpr::StressDStrain, mpr::StressDStrainRate);
			} //defined
		} //contract

    namespace datapacks
    {
      typedef std::tuple<mpr::Coordinates2D, mpr::Time, mpr::Temperature> Coord2dTimeTemp;
      typedef std::tuple<mpr::Coordinates2D, mpr::Time, mpr::Temperature, std::vector<double>> Coord2dTimeTempVect;
    } //datapacks 
	} //model
} //am3
#endif // am3models_predefinedContracts_h__
