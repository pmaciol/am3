
/*! \file simpleStorageModel.h **************************************************
* \author		Piotr Maciol
* \copyright 	GNU Public License.
* \brief       
* \details   	
*********************************************************************/
#ifndef simpleStorageModel_h__
#define simpleStorageModel_h__
/*	include *********************************************************/

#include <unordered_map>
#include <boost/shared_ptr.hpp>
#include <algorithm>
#include "../logger/logger.h"

/*	using ***********************************************************/

namespace lo=am3::logger;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{


		//! Works only with fixed mesh!
		//! no remeshing allowed
		template <typename VARIABLES_TUPLE, typename COORDINATES>
		class LastValueStorageModel
		{
		public:
			typedef VARIABLES_TUPLE Tuple;
			~LastValueStorageModel();
			LastValueStorageModel();			
		protected:
			void SetValue(const unsigned long pointID, const VARIABLES_TUPLE &values);
			VARIABLES_TUPLE GetValues(const unsigned long pointID) const;
			VARIABLES_TUPLE GetPreviousValues(const unsigned long pointID) const;
			void AddPoint(const COORDINATES coord, const unsigned long pointID);
		private:
			double time_;
			void AddPoint(const COORDINATES coord);
			void NewStep(double stepLength);
			std::unordered_map<unsigned long, VARIABLES_TUPLE > * previousValues_;
			std::unordered_map<unsigned long, VARIABLES_TUPLE > * actualValues_;
			std::unordered_map<unsigned long,COORDINATES> coordinates_;
		};

		//////////////////////////////////////////////////////////////////////////

		template <typename VARIABLES_TUPLE, typename COORDINATES>
		void am3::model::LastValueStorageModel<VARIABLES_TUPLE, COORDINATES>::SetValue( const unsigned long pointID, const VARIABLES_TUPLE &values )
		{
			std::unordered_map<unsigned long, COORDINATES >::iterator got = coordinates_.find (pointID);

			if ( got == coordinates_.end() )
				lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Point not found.");
			else
				actualValues_->emplace(std::pair<unsigned long, VARIABLES_TUPLE >(pointID,values));
		}


		template <typename VARIABLES_TUPLE, typename COORDINATES>
		void am3::model::LastValueStorageModel<VARIABLES_TUPLE, COORDINATES>::AddPoint( const COORDINATES coord )
		{
			unsigned long id=0;
			std::unordered_map<unsigned long,COORDINATES>::iterator it=coordinates_.begin();
			while (it<coordinates_.end()) id=std::max(id,it->first);
			this->AddPoint(coord,++id);
		}

		template <typename VARIABLES_TUPLE, typename COORDINATES>
		void am3::model::LastValueStorageModel<VARIABLES_TUPLE, COORDINATES>::AddPoint(const COORDINATES coord, const unsigned long pointID)
		{
			if (std::fabs(time_ ) < std::numeric_limits<double>::epsilon())
			{
				coordinates_.emplace (std::pair<unsigned long, COORDINATES>(pointID, coord));
			} 
			else
			{
				lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Cannot add points after initial step");
			}
		}

		template <typename VARIABLES_TUPLE, typename COORDINATES>
		am3::model::LastValueStorageModel<VARIABLES_TUPLE, COORDINATES>::~LastValueStorageModel()
		{
			delete actualValues_;
			delete previousValues_;
		}

		template <typename VARIABLES_TUPLE, typename COORDINATES>
		am3::model::LastValueStorageModel<VARIABLES_TUPLE, COORDINATES>::LastValueStorageModel():time_(0.0)
		{
			actualValues_=new std::unordered_map<unsigned long, VARIABLES_TUPLE >;
		};

		template <typename VARIABLES_TUPLE, typename COORDINATES>
		VARIABLES_TUPLE am3::model::LastValueStorageModel<VARIABLES_TUPLE, COORDINATES>::GetValues(const unsigned long pointID) const
		{
			return actualValues_->at(pointID);
		}

		template <typename VARIABLES_TUPLE, typename COORDINATES>
		VARIABLES_TUPLE am3::model::LastValueStorageModel<VARIABLES_TUPLE, COORDINATES>::GetPreviousValues(const unsigned long pointID) const
		{
			return previousValues_->at(pointID);
		}

		template <typename VARIABLES_TUPLE, typename COORDINATES>
		void am3::model::LastValueStorageModel<VARIABLES_TUPLE, COORDINATES>::NewStep(double stepLength)
		{
			time_+=stepLength;	
			delete previousValues_;
			previousValues_ = actualValues_;
			actualValues_= new std::unordered_map<unsigned long, VARIABLES_TUPLE >;

		}

	} //model
} //am3

#endif // simpleStorageModel_h__
