
/*! \file fineModelsContainer.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef fineModelsContainer_h__
#define fineModelsContainer_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>
#include <algorithm>
#include <limits> 
#include <map>
#include "./point.h"


/*	using ***********************************************************/

namespace mpt = am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
    template <typename COORD>
    class MovementCoordinates
    {
    public:
      virtual void GetNewPosition(mpt::Point<COORD> &oldPosition)=0;
    };

    template <typename FINE_SCALE_MODEL>
		class FineModelSelector
    {
    public:
      typedef typename FINE_SCALE_MODEL::Coordinates COORD;
      typedef boost::shared_ptr<mpt::Point<COORD> > PointPtr;
      typedef std::pair<boost::shared_ptr<mpt::Point<COORD> >,boost::shared_ptr<FINE_SCALE_MODEL> > Pair;
      FineModelSelector(boost::shared_ptr<MovementCoordinates<COORD> > movementCoordinates): movementCoordinates_(movementCoordinates){};
      typedef FINE_SCALE_MODEL FineScaleModel; 
      Pair GetClosest( const mpt::Point<COORD>& point);
      Pair GetExact( const PointPtr point);
      std::vector<Pair> GetClosests(const mpt::Point<COORD>&,const unsigned int howMany);
//       virtual Pair GetMostSimilar( const mpt::Point<COORD>& point);
//       virtual std::vector<Pair> GetMostSimilars(const mpt::Point<COORD>&,const unsigned int howMany);
      bool IsExactlyHere(boost::shared_ptr<FineScaleModel> modelInstance, const mpt::Point<COORD>&);
      //virtual bool IsNearEnough(boost::shared_ptr<FineScaleModel> modelInstance, const mpt::Point<COORD>&, const double inDistance);
      Pair AddModel(boost::shared_ptr<mpt::Point<COORD> > whereIsModel,boost::shared_ptr<FineScaleModel> modelToAdd);
      void MoveEachModel();
      bool IsAny();
      bool IsExact(const PointPtr point);
    protected:
      static void MoveModel(const std::pair<boost::shared_ptr<mpt::Point<COORD> >,boost::shared_ptr<FineScaleModel> >& pair) ;
      std::map<boost::shared_ptr<mpt::Point<COORD> >,boost::shared_ptr<FineScaleModel> > fineModels_;
      
      boost::shared_ptr<MovementCoordinates<COORD> > movementCoordinates_;


      //static void Test(int i){};
    };

    template <typename FINE_SCALE_MODEL>
    bool am3::model::FineModelSelector<FINE_SCALE_MODEL>::IsAny()
    {
      return ((fineModels_.size()>0)?true:false);
    }

    template <typename FINE_SCALE_MODEL>
    bool am3::model::FineModelSelector<FINE_SCALE_MODEL>::IsExact( const typename FineModelSelector<FINE_SCALE_MODEL>::PointPtr point )
    {
      return (fineModels_.find(point)==fineModels_.end()?true:false);
    }

		template <typename FINE_SCALE_MODEL>
		typename FineModelSelector<FINE_SCALE_MODEL>::Pair FineModelSelector<FINE_SCALE_MODEL>::GetExact(const typename FineModelSelector<FINE_SCALE_MODEL>::PointPtr point)
		{
			return *fineModels_.find(point);
		}

    template <typename FINE_SCALE_MODEL>
    std::pair<boost::shared_ptr<mpt::Point<typename FINE_SCALE_MODEL::Coordinates> >,boost::shared_ptr<FINE_SCALE_MODEL> > am3::model::FineModelSelector<FINE_SCALE_MODEL>::GetClosest( const mpt::Point<typename FINE_SCALE_MODEL::Coordinates>& point )
    {
        typedef std::pair<boost::shared_ptr<mpt::Point<COORD> > ,boost::shared_ptr<FineScaleModel> > Pair;
        typedef std::map<boost::shared_ptr<mpt::Point<COORD> >,boost::shared_ptr<FineScaleModel> > Container;
        double distance = 1.79769e+300;//std::numeric_limits<double>::max( );
        double distTmp;
        Pair toRet;
        BOOST_FOREACH(Pair pair, fineModels_ ) 
        {
          mpt::Point<COORD> &pointOfModel = *(pair.first);
          distTmp = pointOfModel.SpatialDistance(point);
          if (distTmp < distance) 
          {
            toRet = pair;
            distance = distTmp;
          }

        }
        return toRet;
    }

    template <typename FINE_SCALE_MODEL>
    class ModelMakerDump
    {
    public:
	  typedef typename FINE_SCALE_MODEL::Coordinates COORD;
      boost::shared_ptr<typename FINE_SCALE_MODEL> NewModel(const boost::shared_ptr<mpt::Point<COORD> > point)
      {
        boost::shared_ptr<typename FINE_SCALE_MODEL> toRet(new FINE_SCALE_MODEL);
        return toRet;
      }
    };

    template <typename FINE_SCALE_MODEL>
    class FineModelMaker
    {
	public:
		typedef typename FINE_SCALE_MODEL::Coordinates COORD;
    protected:
      boost::shared_ptr<typename FINE_SCALE_MODEL> NewModel(const boost::shared_ptr<mpt::Point<COORD> > point);
    };


    template <typename FINE_SCALE_MODEL, template <typename FINE_SCALE_MODEL> class CONTAINER, template <typename FINE_SCALE_MODEL> class  MAKER>
    //template <typename FINE_SCALE_MODEL, typename CONTAINER, typename MAKER>
    class OnlyExactAlgorithm
    {
    public:
      typedef typename FINE_SCALE_MODEL::Coordinates Coordinates;
      OnlyExactAlgorithm(boost::shared_ptr<MAKER<FINE_SCALE_MODEL> > modelMaker, boost::shared_ptr<CONTAINER<FINE_SCALE_MODEL> > modelContainer)
        :modelMaker_(modelMaker),modelContainer_(modelContainer){}
      boost::shared_ptr<typename FINE_SCALE_MODEL::Contract> GetModel(const boost::shared_ptr<mpt::Point<Coordinates> > point);
    protected:
      boost::shared_ptr<CONTAINER<FINE_SCALE_MODEL> > modelContainer_;
      boost::shared_ptr<MAKER<FINE_SCALE_MODEL> > modelMaker_;
    };

	} //model
} //am3

//////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION /////
//////////////////////////////////////////////////////////////////////////


template <typename FINE_SCALE_MODEL, template <typename FINE_SCALE_MODEL> class CONTAINER, template <typename FINE_SCALE_MODEL> class  MAKER>
boost::shared_ptr<typename FINE_SCALE_MODEL::Contract> am3::model::OnlyExactAlgorithm<FINE_SCALE_MODEL,CONTAINER,MAKER>::GetModel( const boost::shared_ptr<mpt::Point<Coordinates> > point )
{
  boost::shared_ptr<typename FINE_SCALE_MODEL::Contract> toRet;
  if (modelContainer_->IsAny()&&modelContainer_->IsExact(point))
  {
    toRet = modelContainer_->GetExact(point).second;
  }
  else
  {
    toRet = modelContainer_->AddModel(point,modelMaker_->NewModel(point)).second;
  }
  return toRet;
}






template <typename FINE_SCALE_MODEL>
typename am3::model::FineModelSelector<FINE_SCALE_MODEL>::Pair am3::model::FineModelSelector<FINE_SCALE_MODEL>::AddModel(boost::shared_ptr<mpt::Point<COORD> > whereIsModel,boost::shared_ptr<FineScaleModel> modelToAdd )
{
	Pair toIns(whereIsModel,modelToAdd);
	fineModels_.insert(toIns);
	return toIns;
}

template <typename FINE_SCALE_MODEL>
void am3::model::FineModelSelector<FINE_SCALE_MODEL>::MoveEachModel()
{
  typedef std::pair<boost::shared_ptr<mpt::Point<COORD> > ,boost::shared_ptr<FineScaleModel> > Pair;
  typedef std::map<boost::shared_ptr<mpt::Point<COORD> >,boost::shared_ptr<FineScaleModel> > Container;
   BOOST_FOREACH(Pair pair, fineModels_ ) 
   {
     mpt::Point<COORD> &point = *(pair.first);
     //   pair.first->Move(movementCoordinates_->GetNewPosition(point));
     movementCoordinates_->GetNewPosition(point);
    }
  //std::for_each(fineModels_.begin(), fineModels_.end(), &am3::model::FineModelSelector<FINE_SCALE_MODEL, typename COORD>::MoveModel);
  //std::for_each(fineModels_.begin(), fineModels_.end(), &am3::model::FineModelSelector<FINE_SCALE_MODEL, typename COORD>::Test);
}



template <typename FINE_SCALE_MODEL>
static void am3::model::FineModelSelector<FINE_SCALE_MODEL>::MoveModel(const std::pair<boost::shared_ptr<typename mpt::Point<COORD> >,boost::shared_ptr<FineScaleModel> >& pair) 
{
  mpt::Point<COORD> &point = *(pair.first);
//   pair.first->Move(movementCoordinates_->GetNewPosition(point));
  movementCoordinates_->GetNewPosition(point);
}

#endif // fineModelsContainer_h__
