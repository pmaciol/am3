
/*! \file modelsRunner.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3models_modelsManager_h__
#define am3models_modelsManager_h__
/*	include *********************************************************/

#include <vector>
#include <vector>
#include <assert.h>
#include <stdexcept>
#include "../logger/logger.h"
#include "../common/common.h"

/*	using ***********************************************************/

namespace lo = am3::logger;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace manager
		{
      template<typename INPUT_DATA>
      class ManagerAllTogether
      {
      public:
        virtual ~ManagerAllTogether(){};

        virtual void Run(const std::vector<INPUT_DATA> dataToRun) = 0;
        virtual void Initialize(const std::vector<INPUT_DATA> dataToRun) = 0;
        virtual void Finalize(const std::vector<INPUT_DATA> dataToRun) = 0;
        virtual void FinalizeAll() = 0;

      protected:
        ManagerAllTogether(){};
      };

      //******************************************************************************************************************************************************************

      template<typename MICROSCALE_MODEL>
      class BasicAllTogatherManager
        :public ManagerAllTogether < typename MICROSCALE_MODEL::InputData >
      {
      public:
        BasicAllTogatherManager(boost::shared_ptr<MICROSCALE_MODEL> model) : isInitialized_(false), microModel_(model), lastTime_(0.0){};
        virtual ~BasicAllTogatherManager(){};

        virtual void Run(const std::vector<typename MICROSCALE_MODEL::InputData > dataToRun) override;
        virtual void Initialize(const std::vector<typename MICROSCALE_MODEL::InputData > dataToRun) override;
        virtual void Finalize(const std::vector<typename MICROSCALE_MODEL::InputData > dataToRun) override;
        virtual void FinalizeAll() override;

      protected:
        bool isInitialized_;
        boost::shared_ptr<MICROSCALE_MODEL> microModel_;
        double lastTime_;
      };


      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////


      template<typename MICROSCALE_MODEL>
      void am3::model::manager::BasicAllTogatherManager<MICROSCALE_MODEL>::FinalizeAll()
      {
        microModel_->Finalize();
      }

      template<typename MICROSCALE_MODEL>
      void am3::model::manager::BasicAllTogatherManager<MICROSCALE_MODEL>::Finalize(const std::vector<typename MICROSCALE_MODEL::InputData > dataToRun)
      {
        throw std::logic_error("The method or operation is not implemented.");
      }

      template<typename MICROSCALE_MODEL>
      void am3::model::manager::BasicAllTogatherManager<MICROSCALE_MODEL>::Initialize(const std::vector<typename MICROSCALE_MODEL::InputData > dataToRun)
      {
        if (isInitialized_)
        {
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Fine scale model previously initialized, possible lost data");
          assert(false);
        }
        typename std::vector<typename MICROSCALE_MODEL::InputData >::const_iterator it = dataToRun.begin();
        while (it < dataToRun.end())
        {
          microModel_->InitWithData(*it);
          it++;
        }
        isInitialized_ = true;
      }

      template<typename MICROSCALE_MODEL>
      void am3::model::manager::BasicAllTogatherManager<MICROSCALE_MODEL>::Run(const std::vector<typename MICROSCALE_MODEL::InputData > dataToRun)
      {
        if (!isInitialized_)
        {
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Fine scale model not initialized");
          assert(false);
        }
        typename std::vector<typename MICROSCALE_MODEL::InputData >::const_iterator it = dataToRun.begin();
        if (! AlmostEqual(it->time,lastTime_))
        {
          lastTime_ = it->time;
          while (it < dataToRun.end())
          {
            microModel_->CalculateWithData(*it);
            it++;
          }
        } 
        else
        {
          lo::log<lo::SEV_NOTICE>(lo::LOG_MODEL, "Called for previously computed time step; computations skipped.");
        }

      }
		} //manager
	} //model
} //am3
#endif // am3models_modelsRunner_h__
