
/*! \file contractMacros.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef contractMacros_h__
#define contractMacros_h__
/*	include *********************************************************/

#include "../common/typelists.h"
#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../models/point.h"

/*	using ***********************************************************/

namespace mo=am3::model;
namespace moc=am3::model::contract;
namespace mpt=am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace contract
		{

			template<typename T, typename COORDINATES, typename LIST>
			struct ContractInternal;

			template<typename T1, typename T2, typename COORDINATES, typename LIST>
			struct ContractInternal<Typelist<T1, T2> , COORDINATES, LIST>
				: public moc::Provider<T1,COORDINATES>
				, public ContractInternal<T2, COORDINATES, LIST>
			{
			};

			template<typename T1, typename T2, typename T3, typename COORDINATES, typename LIST>
			struct ContractInternal<TypelistTuple<Typelist<T1, T2>, T3> , COORDINATES, LIST>
				: public moc::Provider<T1,COORDINATES>
				, public ContractInternal<T2, COORDINATES, LIST>
			{
				typedef T3 Tuple;
			};


			template<typename COORDINATES, typename LIST>
			struct ContractInternal<EmptyType, COORDINATES, LIST>
			{
				typedef LIST VariablesTypes;
				typedef COORDINATES CoordinatesType;
			};


			template<typename T, typename COORDINATES>
					struct Contract
				:public ContractInternal<T,COORDINATES,T>
				{
					typedef Contract<T,COORDINATES> ProvidedContract;
          typedef COORDINATES CoordinatesType;
			};

		}
	}
}

#endif // contractMacros_h__