
/*! \file models/dataStructures.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief  Data structures predefined with am3::model::SimpleProperty
 * \details   	
*********************************************************************/
#ifndef dataStructures_h__
#define dataStructures_h__
/*	include *********************************************************/

#include <string>
#include "description.h"
//#include "point.h"
#include <vector>

/*	using ***********************************************************/
/*	extern **********************************************************/

namespace am3
{
  namespace model
  {
    namespace common
    {
      template<typename T_COORDINATE/*=am3::model::Coordinates*/>
      class Point;
    } //common
  } //model
} //am3


/*	classes *********************************************************/

#define SIMPLE_PROP(T1,T2) struct T1 : public mpr::SimpleProperty<T2,mpr::HaveGetSetValue > \
{\
	using  mpr::SimpleProperty<T2, mpr::HaveGetSetValue >::operator();\
  /*T1(const T2 v): mpr::SimpleProperty<T2,mpr::HaveGetSetValue >::SimpleProperty(v){};*/\
	T1(): mpr::SimpleProperty<T2,mpr::HaveGetSetValue >::SimpleProperty(){};\
};
// #define SIMPLE_PROP(T1,T2) struct T1 : public mpr::SimpleProperty<T2,mpr::HaveGetSetValue > {};

namespace am3
{
  //! Namespace for model
  namespace model
  {

    
 
    //! Standard description of model. Derive from this making new model
    template<typename T_INPUTS,typename T_OUTPUTS,typename T_PHENOMENA>
    class ModelDescription
    {
    protected:
      T_INPUTS input_properties_;
      T_OUTPUTS output_properties_;
      T_PHENOMENA phenomena_;
    public:
      typedef T_OUTPUTS Outputs;
      T_INPUTS& Input_properties() { return input_properties_; }
      T_OUTPUTS& Output_properties() { return output_properties_; }
      T_PHENOMENA& Phenomena() { return phenomena_; }
    };
    //////////////////////////////////////////////////////////////////////////

    const double kBoltzmann = 1.3806488e-23;

    /** \name Typedefs for common properties*/
    /* @{ */
    
    /** \name CFD CFD specific*/
    //! Only for tests, normally used am3::model::VelocityWithDerivatives
    struct Velocity: public ComplexProperty<double, Dimensions3D<double> > 
		{
			Velocity():ComplexProperty<double, Dimensions3D<double> >::ComplexProperty(){};
		 Velocity(const double& velocity_x, const double& velocity_y, const double& velocity_z)
			{
				x(velocity_x);
			  y(velocity_y);
				z(velocity_z);
			};
		};

		struct MaterialPointVelocity3D: public ComplexProperty<double, Dimensions3D<double> > {};
		struct MaterialPointVelocity2D: public ComplexProperty<double, Dimensions2D<double> > {};
    struct Displacement2D: public ComplexProperty<double, Dimensions2DWithOperators<double, Displacement2D> >
	{
     Displacement2D& operator=(const Dimensions2DWithOperators<double, Displacement2D> & that)
     {
       this->x(that.x()) ;
       this->y(that.y()) ;
       return *this;
     }
	};


	struct Velocity2D: public ComplexProperty<double, Dimensions2D<double> > 
    {
      Velocity2D& operator=(const Velocity2D& that)
      {
        this->x(that.x());
        this->y(that.y());
        return *this;
      };
      Velocity2D& operator+=(const Velocity2D& that)
      {
        this->x(that.x()+this->x());
        this->y(that.y()+this->y());
        return *this;
      };
      Velocity2D& operator/=(const double& that)
      {
        this->x(this->x()/that);
        this->y(this->y()/that);
        return *this;
      };
    };
    struct VelocityWithDerivatives: public ComplexProperty<double, Dimensions3D<double, SpatialDerivative3D <double> > > 
    {
      VelocityWithDerivatives(
        const double& velocity_x, const double& velocity_x_dx, const double& velocity_x_dy, const double& velocity_x_dz, 
        const double& velocity_y, const double& velocity_y_dx, const double& velocity_y_dy, const double& velocity_y_dz, 
        const double& velocity_z, const double& velocity_z_dx, const double& velocity_z_dy, const double& velocity_z_dz)
      {
        x(velocity_x);
        x.dx(velocity_x_dx);
        x.dy(velocity_x_dy);
        x.dz(velocity_x_dz);
        y(velocity_y);
        y.dx(velocity_y_dx);
        y.dy(velocity_y_dy);
        y.dz(velocity_y_dz);
        z(velocity_z);
        z.dx(velocity_z_dx);
        z.dy(velocity_z_dy);
        z.dz(velocity_z_dz);
      }
      VelocityWithDerivatives():ComplexProperty<double, Dimensions3D<double, SpatialDerivative3D <double> > > ::ComplexProperty(){};
    };

		struct ShearRate : public SimpleProperty<double,HaveGetSetValue> {};
    struct Viscosity : public SimpleProperty<double,HaveGetSetValue> {};
    struct SurfaceTension : public SimpleProperty<double,HaveGetSetValue> {};
    struct BulkModulus : public SimpleProperty<double,HaveGetSetValue> {};
		struct Pressure : public SimpleProperty<double,HaveGetSetValue> {};
    struct PressureWithDerivatives: public ComplexProperty<double, SpatialDerivative3D<double>,HaveGetSetValue > 
    {
      PressureWithDerivatives(const double& pressure, const double& pressure_dx, const double& pressure_dy, const double& pressure_dz)
      {
        value_=pressure;
        dx(pressure_dx);
        dy(pressure_dy);
        dz(pressure_dz);
      }
      PressureWithDerivatives():ComplexProperty<double, SpatialDerivative3D<double>,HaveGetSetValue >::ComplexProperty(){};
    };
		struct TurbulenceK : public SimpleProperty<double,HaveGetSetValue> {};
		struct TurbulenceE : public SimpleProperty<double,HaveGetSetValue> {};
    struct TurbulenceKWithDerivatives: public ComplexProperty<double, SpatialDerivative3D<double>,HaveGetSetValue > 
    {
      TurbulenceKWithDerivatives(const double& kappa, const double& kappa_dx, const double& kappa_dy, const double& kappa_dz)
      {
        value_=kappa;
        dx(kappa_dx);
        dy(kappa_dy);
        dz(kappa_dz);
      }
      TurbulenceKWithDerivatives():ComplexProperty<double, SpatialDerivative3D<double>,HaveGetSetValue >::ComplexProperty(){};
    };
    struct TurbulenceEWithDerivatives: public ComplexProperty<double, SpatialDerivative3D<double>,HaveGetSetValue > 
    {
      TurbulenceEWithDerivatives(const double& epsilon, const double& epsilon_dx, const double& epsilon_dy, const double& epsilon_dz)
      {
        value_=epsilon;
        dx(epsilon_dx);
        dy(epsilon_dy);
        dz(epsilon_dz);
      }
      TurbulenceEWithDerivatives():ComplexProperty<double, SpatialDerivative3D<double>,HaveGetSetValue >::ComplexProperty(){};
    };

		struct TwoPhaseComposition: public ComplexProperty<double, Pair<double> > {}; 
    /*@}*/

    /** \name MassTransfer Mass transfer specific*/
    struct Ratio : public SimpleProperty<double,HaveGetSetValue> {};
    struct RatioSource : public SimpleProperty<double,HaveGetSetValue> {};
    struct RatiosSet : public SimpleProperty<std::vector<double>,HaveGetSetValue> {};
		struct SourceRH : public SimpleProperty<double,HaveGetSetValue> {};
		struct SourceLH : public SimpleProperty<double,HaveGetSetValue> {};
    /*@}*/

    /** \name SolidDeformation Solids deformation*/
		struct YieldStress : public SimpleProperty<double, HaveGetSetValue> { using SimpleProperty<double, HaveGetSetValue>::operator (); };
    struct Stress : public SimpleProperty<double, HaveGetSetValue> { using SimpleProperty<double, HaveGetSetValue>::operator (); };
    struct StressThermal : public SimpleProperty<double, HaveGetSetValue> { using SimpleProperty<double, HaveGetSetValue>::operator (); };
    struct StressAthermal : public SimpleProperty<double, HaveGetSetValue> { using SimpleProperty<double, HaveGetSetValue>::operator (); };
	struct ShearModulus : public SimpleProperty<double, HaveGetSetValue> { using SimpleProperty<double, HaveGetSetValue>::operator (); };
	struct StressDStrain : public SimpleProperty<double, HaveGetSetValue>  { using SimpleProperty<double, HaveGetSetValue>::operator (); };
	struct StressDStrainRate : public SimpleProperty<double, HaveGetSetValue>  { using SimpleProperty<double, HaveGetSetValue>::operator (); };
	struct Stress2D : public ComplexProperty<double, Tensor2D<double> >{};
	struct Strain2D : public ComplexProperty<double, Tensor2D<double> >{};
	struct StrainRate2D : public ComplexProperty<double, Tensor2D<double> >{};
//   struct Strain3D : public ComplexProperty < double, Tensor3D<double> > {};
//   struct StrainRate3D : public ComplexProperty < double, Tensor3D<double> > {};
	struct EffectiveStress : public SimpleProperty<double, HaveGetSetValue>  { using SimpleProperty<double, HaveGetSetValue>::operator (); };
	struct EffectiveStrainRate : public SimpleProperty<double, HaveGetSetValue>  { using SimpleProperty<double, HaveGetSetValue>::operator (); };
	struct TotalEffectiveStrain : public SimpleProperty<double, HaveGetSetValue>  { using SimpleProperty<double, HaveGetSetValue>::operator (); };

    /*@}*/

    /** \name Thermal Thermal*/

		struct NullProperty :public SimpleProperty<bool, HaveGetValue>{};

	struct DeltaTemperature;
	
    struct Temperature :public SimpleProperty<double,HaveGetSetValue> 
	{
      Temperature() {};
      Temperature(const double value) { this->value_ = value; };
		using SimpleProperty<double, HaveGetSetValue>::operator();
    using SimpleProperty<double, HaveGetSetValue>::operator=;

		Temperature absolute() const
		{
			Temperature toRet;
			double d=abs(this->value_);
			toRet(d);
			return toRet;
		}
		Temperature operator-(const Temperature& toSubtr) const
	{
		Temperature toReturn;
		toReturn(this->value_-toSubtr());
		return toReturn;
	};
	};
	struct DeltaTemperature :public SimpleProperty<double,HaveGetSetValue> 
	{
	    DeltaTemperature& operator=(const Temperature& that)
		{
			value_=that.Value();
			return *this;
		};

	};  
    struct TemperatureWithDerivatives
      : public ComplexProperty<double, SpatialDerivative3D<double>/*,HaveGetSetValue */>
      , public Temperature
    {
      TemperatureWithDerivatives(const double& temperature, const double& temperature_dx, const double& temperature_dy, const double& temperature_dz)
      {
        value_=temperature;
        dx(temperature_dx);
        dy(temperature_dy);
        dz(temperature_dz);
      }
      TemperatureWithDerivatives():ComplexProperty<double, SpatialDerivative3D<double>/*,HaveGetSetValue */>::ComplexProperty(){};

    }; 

    struct TemperatureRate : public SimpleProperty<double,HaveGetSetValue > {};
    struct Cp : public SimpleProperty<double,HaveGetSetValue > {};
    struct Cv : public SimpleProperty<double,HaveGetSetValue > {};
    struct ThermalConductivity : public SimpleProperty<double,HaveGetSetValue > {};
    struct CoeffVolumeExpansion : public SimpleProperty<double,HaveGetSetValue > {};
    struct ReferenceTemperature : public SimpleProperty<double,HaveGetSetValue > {};
    struct HeatPerVolume : public SimpleProperty<double,HaveGetSetValue > {};
    /*@}*/

    /** \name General General*/   

 
    struct MaterialName : public SimpleProperty<std::string,HaveGetSetValue > {};
    struct Time : public SimpleProperty<double, HaveGetSetValue> 
    {
       using SimpleProperty<double, HaveGetSetValue>::Value;
       using SimpleProperty<double, HaveGetSetValue>::operator();//!!!
       using SimpleProperty<double, HaveGetSetValue>::operator<=;//!!!
      Time(const double& val){value_=val;};
      Time():SimpleProperty<double, HaveGetSetValue>::SimpleProperty(){};
			bool operator>=( const Time& that) const
			{
				return (this->value_>=that())? true: false;
			}
      Time operator-(const Time& rightSide) const
      {
        Time toRet;
        toRet(this->value_ - rightSide.Value());
        return toRet;
      }
			bool operator<(const Time& rightSide) const
			{
				if (this->value_ < rightSide.Value()) return true;
				else return false;
			}
    };
    struct StepLength : public SimpleProperty<double, HaveGetSetValue> 
    {
		using SimpleProperty<double, HaveGetSetValue>::operator();
      StepLength(const double& val){value_=val;};
      StepLength():SimpleProperty<double, HaveGetSetValue>::SimpleProperty(){};
    };
    struct Density: public SimpleProperty<double, HaveGetSetValue > {};
		struct DensityWithDerivatives: public ComplexProperty<double, TemperaturePressureDerivative<double>, HaveGetSetValue > {};
    struct Gravitation: public ComplexProperty<double, Dimensions3D<double>,HaveGetSetValue > {};
    struct Coordinates: public ComplexProperty<double, Dimensions3D<double> > 
    {
			Coordinates(const double values[])
			{
				x(values[0]);
				y(values[1]);
				z(values[2]);
			}
      Coordinates(const double& cx, const double& cy, const double& cz)
      {
        x(cx);
        y(cy);
        z(cz);
      }
      Coordinates(): ComplexProperty<double, Dimensions3D<double> >::ComplexProperty(){};
    };

    typedef Coordinates Coordinates3D;

    struct Coordinates2D: public ComplexProperty<double, Dimensions2D<double> > 
    {
			Coordinates2D(const double values[])
			{
				x(values[0]);
				y(values[1]);
			}
      Coordinates2D(const Coordinates2D& that)
      {
        x(that.x());
        y(that.y());
      }
      Coordinates2D(const double& cx, const double& cy)
      {
        x(cx);
        y(cy);
      }
      Coordinates2D(): ComplexProperty<double, Dimensions2D<double> >::ComplexProperty(){};
      Coordinates2D& operator=(const Coordinates2D& that)
      {
        this->x(that.x());
        this->y(that.y());
        return *this;
      };
			bool operator!=(const Coordinates2D& that) const
			{
				bool isEqualX = (this->x()==that.x())? true:false;
				bool isEqualY = (this->y()==that.y())? true:false;
				return (isEqualX && isEqualY)? false : true;
			}

      bool operator<(const Coordinates2D& that) const
      {
        double radius1 = sqrt(pow(this->x(), 2) + pow(this->y(), 2));
        double radius2 = sqrt(pow(that.x(), 2) + pow(that.y(), 2));

        return (radius1 < radius2) ? true : false;
      }

			bool operator==(const Coordinates2D& that) const
			{
				bool isEqualX = (this->x()==that.x())? true:false;
				bool isEqualY = (this->y()==that.y())? true:false;
				return (isEqualX && isEqualY)? true: false;
			}
      double Distance(const Coordinates2D& that) const
      {
        double toRet;
        toRet = sqrt(pow(that.x() - this->x(),2) + pow(that.y() - this->y(),2));
        return toRet;
      }
    };
    /*@}*/

    /** \name Fracture Fracture mechanic*/
    /* @{ */
      struct DamageValue : public SimpleProperty<double,HaveGetSetValue> {};
    /*@}*/


    /** \name FEMSpec FEM specific*/
    /* @{ */
		struct ElementNodesNumber : public SimpleProperty<long long int> 
			{
				ElementNodesNumber(const long long int& val){value_=val;};
//				ElementNodesNumber(const unsigned __int64& val){ value_ = val; };
				ElementNodesNumber() :SimpleProperty<long long int>::SimpleProperty(){};
			};
    struct ElementGroupNumber : public SimpleProperty<int> 
    {
      ElementGroupNumber(const int& val){value_=val;};
      ElementGroupNumber():SimpleProperty<int>::SimpleProperty(){};
    };
	struct MeshId : public SimpleProperty<int> 
	{
		MeshId(const int& val){value_=val;};
		MeshId():SimpleProperty<int>::SimpleProperty(){};
	};
    struct ElementNumber : public SimpleProperty<int> 
    {
      ElementNumber(const int& val){value_=val;};
      ElementNumber():SimpleProperty<int>::SimpleProperty(){};
    };
    struct ElementNumberLocal : public SimpleProperty<int, HaveGetSetValue> 
    {
      ElementNumberLocal(const int& val){value_=val;};
      ElementNumberLocal():SimpleProperty<int, HaveGetSetValue>::SimpleProperty(){};
    };
    struct ElementNumberGlobal : public SimpleProperty<int, HaveGetSetValue> 
    {
      ElementNumberGlobal(const int& val){value_=val;};
      ElementNumberGlobal():SimpleProperty<int, HaveGetSetValue>::SimpleProperty(){};
	  bool operator< ( ElementNumberGlobal const &q) const {return this->Value() < q.Value();}
	  bool operator> ( ElementNumberGlobal const &q) const {return this->Value() > q.Value();}
    };
    struct MaterialGroupNumber : public SimpleProperty<int> 
    {
      MaterialGroupNumber(const int& val){value_=val;};
      MaterialGroupNumber():SimpleProperty<int>::SimpleProperty(){};
    };

		struct MaterialID : public SimpleProperty<int, HaveGetSetValue> {};

    struct UserValuesDouble : public SimpleProperty<std::vector<double>,HaveGetSetValue> {};
    struct UserValuesDouble_1 : public SimpleProperty<std::vector<double>,HaveGetSetValue> {};
    struct UserValuesDouble_2 : public SimpleProperty<std::vector<double>,HaveGetSetValue> {};
    struct UserValuesDoubleNodal : public SimpleProperty<std::vector<double>,HaveGetSetValue> 
	{
		void operator()(const std::vector<double>& that){value_=that;};
	};
     /*@}*/

    /** \name Contact Contact*/
    /* @{ */
    struct Connectivity : public SimpleProperty<double,HaveGetSetValue> {};
    /*@}*/
    /*@}*/

    /** \name Control Model controls*/
    /* @{ */
    struct ModelID : public SimpleProperty<unsigned int,HaveGetSetValue> 
	{
		using SimpleProperty<unsigned int, HaveGetSetValue>::operator();
	};
    struct ProblemID : public SimpleProperty<int,HaveGetSetValue> {};
    /*@}*/
    /*@}*/

    struct PrecipitationsMeanRadius : public SimpleProperty<double,HaveGetSetValue> {
		using SimpleProperty<double, HaveGetSetValue>::operator();//!!!};
	};
		struct PrecipitationsVolumeFraction : public SimpleProperty < double, HaveGetSetValue > {
			using SimpleProperty<double, HaveGetSetValue>::operator();//!!!};
		};
	struct PrecipitationsAmount : public SimpleProperty < double, HaveGetSetValue > {
		using SimpleProperty<double, HaveGetSetValue>::operator();//!!!};
	};

    extern const Viscosity kViscosityTrait;
    extern const Cp kCpTrait;
    extern const Cv kCvTrait;
    extern const ThermalConductivity kThermalConductivityTrait;
    extern const CoeffVolumeExpansion kCoeffVolumeExpansionTrait;
    extern const Density kDensityTrait;
		extern const DensityWithDerivatives kDensityWithDerivativesTrait;
    extern const ReferenceTemperature kReferenceTemperatureTrait;
    extern const Gravitation kGravitationTrait;
    extern const HeatPerVolume kHeatPerVolumeTrait;
    extern const SurfaceTension kSurfaceTensionTrait;
    extern const BulkModulus kBulkModulusTrait;
    extern const RatiosSet kRatiosSetTrait;
    extern const YieldStress kYieldStressTrait;
    extern const StressDStrain kStressDStrainTrait;
    extern const StressDStrainRate kStressDStrainRateTrait;

    namespace properties
    {
      using namespace am3::model;
    }

  } //model
} //am3

#endif // dataStructures_h__
