
/*! \file conversions.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef models_conversions_h__
#define models_conversions_h__
/*	include *********************************************************/

#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>
#include <string>
#include <vector>
#include "dataStructures.h"
#include "point.h"

/*	using ***********************************************************/

namespace com=am3::model::common;
namespace mpt = am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

template <typename PROPERTY, typename T>
PROPERTY Property(const T& val){ PROPERTY toRet; toRet(val); return toRet; };

namespace am3
{
  namespace model
  {
    namespace conversions
    {
      template<typename T_PROPERTY,typename T_PROPERTY_VALUE_TYPE>
      T_PROPERTY convert2D(std::vector<std::string>::const_iterator starts, std::vector<std::string>::const_iterator ends);
      template<typename T_PROPERTY>
      T_PROPERTY convertScalar(std::string value)
      {
        T_PROPERTY toReturn;
        toReturn(boost::lexical_cast<typename T_PROPERTY::Type>(value));
        return toReturn;
      }
      template<typename T_PROPERTY, typename T_PROPERTY_VALUE_TYPE>
      T_PROPERTY convert2Dtensor(std::vector<std::string>::const_iterator starts, std::vector<std::string>::const_iterator ends);
      Coordinates2D MeanPoint4Nodes(std::vector<std::string>::const_iterator coordinatesStart, std::vector<std::string>::const_iterator coordinatesEnds);
			Coordinates2D MeanPoint4Nodes(std::vector<boost::shared_ptr<mpt::Point<Coordinates2D> > >);
      Coordinates2D PointFromStringsVector(std::vector<std::string>::const_iterator coordinatesStart, std::vector<std::string>::const_iterator coordinatesEnds);
      Velocity2D VelocityFromStringsVector(std::vector<std::string>::const_iterator velocityStart, std::vector<std::string>::const_iterator velocityEnds);
      Stress2D StressFromStringsVector(std::vector<std::string>::const_iterator stressStart,std::vector<std::string>::const_iterator stressEnds);
      StrainRate2D StrainRateFromStringsVector(std::vector<std::string>::const_iterator strainRateStart,std::vector<std::string>::const_iterator strainRateEnds);

      template<typename PROPERTY>
      std::string ToString(const PROPERTY& property) { return std::to_string(property.Value()); }

      template<typename PROPERTY>
      std::string VariableName()
      {
        std::string variableName = typeid(PROPERTY).name();
        variableName = variableName.substr(variableName.find_last_of(':') + 1);
        return variableName;
      }
    } //conversions

template<typename T_PROPERTY,typename T_PROPERTY_VALUE_TYPE>
T_PROPERTY am3::model::conversions::convert2D( std::vector<std::string>::const_iterator starts, std::vector<std::string>::const_iterator ends )
{
	T_PROPERTY toReturn;
  if (std::distance(starts,ends)<2)
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Error in conversion from vector to 2D property");
  } 
  else
  {
    toReturn.x(boost::lexical_cast<T_PROPERTY_VALUE_TYPE>(*starts));
    starts++;
    toReturn.y(boost::lexical_cast<T_PROPERTY_VALUE_TYPE>(*starts));
  }
  return toReturn;
}

template<typename T_PROPERTY,typename T_PROPERTY_VALUE_TYPE>
T_PROPERTY am3::model::conversions::convert2Dtensor( std::vector<std::string>::const_iterator starts, std::vector<std::string>::const_iterator ends )
{
	T_PROPERTY toReturn;
  if (std::distance(starts,ends)<4)
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Error in conversion from vector to 2D property");
  } 
  else
  {
    toReturn.g11(boost::lexical_cast<T_PROPERTY_VALUE_TYPE>(*(starts)));
    starts++;
    toReturn.g12(boost::lexical_cast<T_PROPERTY_VALUE_TYPE>(*(starts)));
    starts++;
    toReturn.g21(boost::lexical_cast<T_PROPERTY_VALUE_TYPE>(*(starts)));
    starts++;
    toReturn.g22(boost::lexical_cast<T_PROPERTY_VALUE_TYPE>(*(starts)));
  }
  return toReturn;
}    
    
  } //model
} //am3


#endif // models_conversions_h__
