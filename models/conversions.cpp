#include "conversions.h"

#include <string>
#include <vector>
#include <iterator>
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>
#include "../logger/logger.h"
#include "dataStructures.h"
#include "point.h"


namespace com=am3::model::common;
namespace pro=am3::model;
namespace lo=am3::logger;

using boost::lexical_cast;

am3::model::Coordinates2D am3::model::conversions::MeanPoint4Nodes( std::vector<std::string>::const_iterator coordinatesStart, std::vector<std::string>::const_iterator coordinatesEnds )
{
  pro::Coordinates2D coor1,coor2,coor3,coor4,meanPoint;
  if (std::distance(coordinatesStart,coordinatesEnds)<8)
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Error in conversion of vector based coordinations");
  } 
  else
  {
    coor1=PointFromStringsVector(coordinatesStart,coordinatesEnds);
    coordinatesStart+=2;
    coor2=PointFromStringsVector(coordinatesStart,coordinatesEnds);
    coordinatesStart+=2;
    coor3=PointFromStringsVector(coordinatesStart,coordinatesEnds);
    coordinatesStart+=2;
    coor4=PointFromStringsVector(coordinatesStart,coordinatesEnds);
    coordinatesStart+=2;
    meanPoint.x((coor1.x()+coor2.x()+coor3.x()+coor4.x())/4);
    meanPoint.y((coor1.y()+coor2.y()+coor3.y()+coor4.y())/4);
  }
  return meanPoint;
}

am3::model::Coordinates2D am3::model::conversions::MeanPoint4Nodes( std::vector<boost::shared_ptr<mpt::Point<Coordinates2D> > > nodes)
{
	
	double x=0,y=0;
	std::vector<boost::shared_ptr<mpt::Point<Coordinates2D> > >::iterator it;
	for (it=nodes.begin();it!=nodes.end();it++)
	{
		x+=(*it)->Coordinates().x();
		y+=(*it)->Coordinates().y();
	}
	return am3::model::Coordinates2D(x/nodes.size(),y/nodes.size());
}

am3::model::Coordinates2D am3::model::conversions::PointFromStringsVector( std::vector<std::string>::const_iterator coordinatesStart, std::vector<std::string>::const_iterator coordinatesEnds )
{
  return convert2D<am3::model::Coordinates2D,double>(coordinatesStart,coordinatesEnds);
}

am3::model::Velocity2D am3::model::conversions::VelocityFromStringsVector( std::vector<std::string>::const_iterator velocityStart, std::vector<std::string>::const_iterator velocityEnds )
{
  return convert2D<am3::model::Velocity2D,double>(velocityStart,velocityEnds);
}

am3::model::Stress2D am3::model::conversions::StressFromStringsVector( std::vector<std::string>::const_iterator stressStart,std::vector<std::string>::const_iterator stressEnds )
{
  return convert2Dtensor<am3::model::Stress2D,double>(stressStart,stressEnds);
}

am3::model::StrainRate2D am3::model::conversions::StrainRateFromStringsVector( std::vector<std::string>::const_iterator strainRateStart,std::vector<std::string>::const_iterator strainRateEnds )
{
  return convert2Dtensor<am3::model::StrainRate2D,double>(strainRateStart,strainRateEnds);
}
