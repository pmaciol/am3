
/*! \file types.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef types_h__
#define types_h__
/*	include *********************************************************/



/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

#include <exception>
#include <iostream>
#include <functional>

template <class T, /*T lower, T upper, */class less=std::less<T> >
class bounded 
{
protected:
  T val;
  virtual T lower() const;
  virtual T upper() const;

  static bool check(T const &value) 
  {
    return less()(value, lower()) || less()(upper(), value);
  }

public:
  bounded() {val=lower(); }

  bounded(T const &v) {
    if (check(v))
      throw std::domain_error("out of range");
    val = v;
  }

  bounded(bounded const &init) : val(init.v) {}

  bounded &operator=(T const &v) {
    if (check(v))
      throw std::domain_error("Out of Range");
    val = v;
    return *this;
  }

  operator T() const { return val; }

  friend std::istream &operator>>(std::istream &is, bounded &b) {
    T temp;
    is >> temp;

    if (check(temp))
      is.setstate(std::ios::failbit);
    b.val = temp;
    return is;
  }
};

struct  double_01:public bounded<double>
{
  double_01(double const &init){val=init;};
  virtual double lower() const {return 0.0;};
  virtual double upper() const {return 1.0;};
}; 
#endif // types_h__
