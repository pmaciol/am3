
/*! \file point.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief Definitions of Point class and derivatives
 * \details   	
*********************************************************************/
#ifndef point_h__
#define point_h__
/*	include *********************************************************/

#include <climits>
#include <iostream>
#include <boost/shared_ptr.hpp>
#include "../logger/logger.h"
#include "dataStructures.h"

/*	using ***********************************************************/

namespace lo=am3::logger;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace common
    {
		///@defgroup point Definitions for points (spatial and temporal)
		///@{

			//@name basics Basic definitions
			// @{
			/// Abstraction of spatial representation of point
      template<typename T_COORDINATE=am3::model::Coordinates>
      struct Spatial
      {
      public:
        virtual const T_COORDINATE Coordinates() const =0;
        virtual ~Spatial(){};
      };
      
			/// Base class for time representation
			/** Provides comparators based on virtual GetTime() function. Works for all deriving classes */
      struct Time
      {
      public:
        virtual ~Time(){};
				bool operator==(const Time& time) {return (time.GetTime()==this->GetTime()) ? (true) : (false);};
				bool operator<(const Time& time) {return (time.GetTime()>this->GetTime()) ? (true) : (false);};
				bool operator>(const Time& time) {return (time.GetTime()<this->GetTime()) ? (true) : (false);};
				bool operator<=(const Time& time) {return (time.GetTime()>=this->GetTime()) ? (true) : (false);};
				bool operator>=(const Time& time) {return (time.GetTime()<=this->GetTime()) ? (true) : (false);};
        virtual double GetTime() const=0;
        virtual mpr::Time GetTimeProperty() const=0;
      };

			/// Base class representing a point as a combination of spatial and temporal "coordinates"
			template<typename T_COORDINATE=am3::model::Coordinates>
			class Point
			{
			public:
				typedef T_COORDINATE CoordinatesType;
				const virtual T_COORDINATE Coordinates() const = 0;
				double virtual GetTime() const = 0 ;
        virtual mpr::Time GetTimeProperty() const=0;
				/// \warning Will that work with no virtual function? Tests not done yet. If not, protected members should be removed from this class
				bool operator==(const Point& point) const; 
				virtual boost::shared_ptr<Point<CoordinatesType> > Copy()=0;
        virtual boost::shared_ptr<Point<CoordinatesType> > Snapshot() const = 0;
        virtual void Move(const Point<CoordinatesType>& newPoint) = 0;
        virtual double SpatialDistance(const Point<CoordinatesType>& newPoint);
        virtual bool SamePointDifferentTime(const Point<CoordinatesType>& thatPoint) = 0;
        virtual bool SamePoint(const Point<CoordinatesType>& thatPoint) = 0;

			protected:
// 				Spatial<T_COORDINATE>* spatial_;
// 				Time* time_;
			};

      inline std::ostream& operator<<(std::ostream& os, const Point<mpr::Coordinates2D>& point)
      {
        os << point.Coordinates().x() << " " << point.Coordinates().y() << " " << point.GetTime();
        return os;
      }

      template<typename T_COORDINATE/*=am3::model::Coordinates*/>
      double am3::model::common::Point<T_COORDINATE>::SpatialDistance( const Point<CoordinatesType>& that )
      {
        return this->Coordinates().Distance(that.Coordinates());
      }

      //@}
			
			//////////////////////////////////////////////////////////////////////////

			//@name approx Approximation based definitions
			/// \warning not implemented
			// @{
			template<typename T_COORDINATE=am3::model::Coordinates>
      struct ApproximatedSpatial: public Spatial<T_COORDINATE>
      {

      };

			struct ApproximatedTime: public Time
			{

			};
			//@}

			//////////////////////////////////////////////////////////////////////////

			//@name approx Approximation based definitions
			/// \warning not implemented
			// @{

			/// Implementation of spatial coordinates based on Property; coordinates given as a double values
      template<typename T_COORDINATE=am3::model::Coordinates>
      struct ComputedSpatial: public Spatial<T_COORDINATE>
      {
      public:
				// constructors
				ComputedSpatial(const T_COORDINATE& coord) :coord_(coord){};
				virtual ~ComputedSpatial(){};
				// derived
				virtual const T_COORDINATE Coordinates() const;
				// own
        void Coordinates(const T_COORDINATE& coord);

			protected:
				/// Stores coordinates
        T_COORDINATE coord_;
      };

			/// Implementation of time based on double value
      struct ComputedTime: public Time
      {
      public:
				// constructors
        ComputedTime(const am3::model::common::Time& time):time_(time.GetTime()){};
				ComputedTime(const double& time):time_(time){};
        virtual ~ComputedTime(){};
        // derived
				virtual double GetTime() const {return time_;};
        virtual mpr::Time GetTimeProperty() const 
        {
          mpr::Time t;
          t(time_);
          return t;
        };
				// own
				virtual void operator()(const am3::model::common::Time& time){time_=time.GetTime();}
        void operator()(const mpr::Time& time){time_=time();}

			protected:
				double time_;
      };

			/// Point description based on directly given values
			template<typename T_COORDINATE=am3::model::Coordinates>
      class ComputedPoint : public Point<T_COORDINATE>
      {
      public:
	typedef typename Point<T_COORDINATE>::CoordinatesType CoordinatesType;
				// constructors
				ComputedPoint(const Point<T_COORDINATE> *that);
        ComputedPoint(const ComputedPoint<T_COORDINATE> &that);
				ComputedPoint(const T_COORDINATE& coordinates, const am3::model::Time& time);
				ComputedPoint(const T_COORDINATE& coordinates, const double& time);
				virtual ~ComputedPoint();
				// derived
				const virtual T_COORDINATE Coordinates() const {return spatial_->Coordinates();};
				double virtual GetTime() const {return this->time_->GetTime();};
        mpr::Time virtual GetTimeProperty() const {return this->time_->GetTimeProperty();};
        virtual bool SamePointDifferentTime(const Point<CoordinatesType>& thatPoint) override;
        virtual bool SamePoint(const Point<CoordinatesType>& thatPoint) override;

	virtual boost::shared_ptr<Point<CoordinatesType> > Copy()
{
	boost::shared_ptr<Point<T_COORDINATE> > newPoint = boost::shared_ptr<Point<CoordinatesType> > (new ComputedPoint<T_COORDINATE>(this));
	return newPoint;
}
       
  virtual boost::shared_ptr<Point<CoordinatesType> > Snapshot() const
  {
	  boost::shared_ptr<Point<T_COORDINATE> > newPoint = boost::shared_ptr<Point<CoordinatesType> > (new ComputedPoint<T_COORDINATE>(this));
	  return newPoint;
  }
        virtual void Move( const Point<CoordinatesType>& newPoint);

				
			protected:
				ComputedSpatial<T_COORDINATE>* spatial_;
				ComputedTime* time_;
      };

      

			//@}

    } //common

    namespace point=am3::model::common; // for back compatibility. After am3::model::point namespace reorganization should be removed

  } //model
} //am3


//////////////////////////////////////////////////////////////////////////

template<typename T_COORDINATE/*=am3::model::Coordinates*/>
bool am3::model::common::ComputedPoint<T_COORDINATE>::SamePoint(const Point<CoordinatesType>& thatPoint)
{
  if (thatPoint.Coordinates() == this->Coordinates())
  {
    return true;
  }
  else
  {
    return false;
  }

}

template<typename T_COORDINATE/*=am3::model::Coordinates*/>
bool am3::model::common::ComputedPoint<T_COORDINATE>::SamePointDifferentTime(const Point<CoordinatesType>& thatPoint)
{
  if ((thatPoint.Coordinates() == this->Coordinates())&&
    thatPoint.GetTime() != this->GetTime())
  {
    return true;
  } 
  else
  {
    return false;
  }

}

template<typename T_COORDINATE/*=am3::model::Coordinates*/>
am3::model::common::ComputedPoint<T_COORDINATE>::ComputedPoint(const ComputedPoint &that)
{
  spatial_ = new ComputedSpatial<T_COORDINATE>(that.Coordinates());
  time_ = new ComputedTime(that.GetTime());
}

template<typename T_COORDINATE/*=am3::model::Coordinates*/>
void am3::model::common::ComputedPoint<T_COORDINATE>::Move( const Point<CoordinatesType>& newPoint )
{
  if (newPoint.GetTime()>this->GetTime())
  			{
  				(*time_)(newPoint.GetTimeProperty());
  				spatial_->Coordinates(newPoint.Coordinates());
  			} 
  			else
  			{
          //DebugBreak();
					lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Cannot add coordinates in time less than last time");
				}
}


template<typename T_COORDINATE>
bool am3::model::point::Point<T_COORDINATE>::operator==(const Point& point) const
{
	bool toReturn=true;
	T_COORDINATE c1,c2;
	c1 = point.Coordinates();
	c2 = this->Coordinates();
	if (c1!=c2) toReturn=false;
	if (point.GetTime()!=this->GetTime()) toReturn=false;
	return toReturn;
}

template<typename T_COORDINATE>
const T_COORDINATE  am3::model::point::ComputedSpatial<T_COORDINATE>::Coordinates() const
{
	return coord_;
};

template<typename T_COORDINATE>
void  am3::model::point::ComputedSpatial<T_COORDINATE>::Coordinates(const T_COORDINATE& coord)
{
	coord_=coord;
}

template<typename T_COORDINATE>
am3::model::point::ComputedPoint<T_COORDINATE>::ComputedPoint(const Point<T_COORDINATE> *that)
{
	spatial_=new ComputedSpatial<T_COORDINATE>(that->Coordinates());
	time_=new ComputedTime(that->GetTime());
}

template<typename T_COORDINATE>
am3::model::point::ComputedPoint<T_COORDINATE>::ComputedPoint(const T_COORDINATE& coordinates, const am3::model::Time& time)
{
	spatial_=new ComputedSpatial<T_COORDINATE>(coordinates);
	time_=new ComputedTime(time());
}

template<typename T_COORDINATE>
am3::model::point::ComputedPoint<T_COORDINATE>::ComputedPoint(const T_COORDINATE& coordinates, const double& time)
{
	spatial_=new ComputedSpatial<T_COORDINATE>(coordinates);
	time_=new ComputedTime(time);
}

template<typename T_COORDINATE>
am3::model::point::ComputedPoint<T_COORDINATE>::~ComputedPoint()
{
	if (spatial_!=nullptr) delete spatial_;
	if (time_!=nullptr) delete time_;
}

/*template<typename T_COORDINATE>
boost::shared_ptr<am3::model::point::Point<T_COORDINATE> > am3::model::point::ComputedPoint<T_COORDINATE>::Copy()
{
	boost::shared_ptr<am3::model::point::Point<T_COORDINATE> > newPoint = boost::shared_ptr<Point<CoordinatesType> > (new am3::model::point::ComputedPoint<T_COORDINATE>(this));
	return newPoint;
}
  */
/*template<typename T_COORDINATE>
boost::shared_ptr<am3::model::point::Point<T_COORDINATE> > am3::model::point::ComputedPoint<T_COORDINATE>::Snapshot()
{
	boost::shared_ptr<am3::model::point::Point<T_COORDINATE> > newPoint = boost::shared_ptr<Point<CoordinatesType> > (new am3::model::point::ComputedPoint<T_COORDINATE>(this));
	return newPoint;
}*/

#endif // point_h__
