
/*! \file modelScheme.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       The frame of Model
 * \details   	The ModelScheme describes the most typical frame of a model. It consist of some stages, which are given as template parameters.
*********************************************************************/
#ifndef modelScheme_h__
#define modelScheme_h__
/*	include *********************************************************/

#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../common/typelists.h"
#include "modelsPatterns.h"

/*	using ***********************************************************/

namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;
using boost::shared_ptr;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {

    class InitializableModel
    {
    public:
      InitializableModel():lastPoint_(nullptr), firstStepRan_(false), initialized_(false), lastComputationsFired_(false){};
      ~InitializableModel(){};

      void InitIfNeeded(mpt::Point<mpr::Coordinates2D> * point);
      void CalculateStepIfNeeded(mpt::Point<mpr::Coordinates2D> * point);
      bool IsInitialized() { return initialized_; }
      bool IsNotInitialized() { return (!initialized_); }
      bool IsFirstStepFinished() { return firstStepRan_; }
      bool WasLastCalculationFired() { return lastComputationsFired_; };
      bool IsComputedForPoint(mpt::Point<mpr::Coordinates2D> * point);
      bool IsNotComputedForPoint(mpt::Point<mpr::Coordinates2D> * point){ return (!IsComputedForPoint(point)); };

      protected:
        void SetInitilized(){ initialized_ = true; }
        void SetFirstStepFinished(){ firstStepRan_ = true; }
        void SetLastCalculationNotFired(){ lastComputationsFired_ = false; };
        void SetLastCalculationFired(){ lastComputationsFired_ = true; };
        
        virtual void Init(mpt::Point<mpr::Coordinates2D> * point) = 0;
        void Compute(mpt::Point<mpr::Coordinates2D> * point);
        
        virtual void RunFirstStep(mpt::Point<mpr::Coordinates2D> * point) = 0;
        virtual void RunNextStep(mpt::Point<mpr::Coordinates2D> * point) = 0;

        bool firstStepRan_, initialized_, lastComputationsFired_;
        boost::shared_ptr<mpt::Point<mpr::Coordinates2D> > lastPoint_;

    };


    void InitializableModel::InitIfNeeded(mpt::Point<mpr::Coordinates2D> * point)
    {
      if (IsNotInitialized())
      {
        Init(point);
        SetInitilized();
      }
    }
    

    void InitializableModel::CalculateStepIfNeeded(mpt::Point<mpr::Coordinates2D> * point)
    {      
      if (IsNotComputedForPoint(point))
      {
        Compute(point);
        lastPoint_ = point->Snapshot();
        SetLastCalculationFired();
      }
      else SetLastCalculationNotFired();
    }

    
    bool InitializableModel::IsComputedForPoint(mpt::Point<mpr::Coordinates2D> * point)
    {
      if (lastPoint_ != nullptr)
      {
        if (*lastPoint_ == *point) return true;
        else return false;
      }
      else return false;
    }

    void InitializableModel::Compute(mpt::Point<mpr::Coordinates2D> * point)
    {
      if (IsInitialized())
      {
        if (IsFirstStepFinished())
        {
          RunNextStep(point);
        }
        else
        {
          RunFirstStep(point);
          SetFirstStepFinished();
        }
      }
      else
      {
        lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "MatCalc was not initialized!");
      }
    }

    // This is old one, probably can be removed!

    /// @brief The ModelScheme is a frame for model with some substeps. It does not derive from Providers! Only Get<VAR> methods are available.
    /// For algorithm details please take a look on implementation of Calculate and Prerequisities methods
    /// @tparam T_INIT_TECH       Component for technical initialization of the model (license files, workspaces etc.)
    /// @tparam T_INIT_MODEL      Component for initialization of computations. Have access to inputs. Can generate initial microstructure etc.
    /// @tparam T_FIRST_STEP      The first step of computations. It is normal computations step, like the next one, but sometimes the first one step differs from others.
    /// @tparam T_STANDARD_STEP   The step of computations
    /// @tparam T_LAST_STEP       The last step of computations. Fired when T_STANDARD_STEP signalize that it is no more available
    /// @tparam T_CALC_RESULTS    The component converting outputs od the 'steps' into output of the whole model. It should be used to adapt the models to expected interface (usually restricts the set of outputs)
    /// @tparam T_INPUTS_PROVIDER Where input data should be get from (usually Databus)
    /// @tparam T_STORAGE         Where last step data should be get from
    template<typename T_INIT_TECH, typename T_INIT_MODEL, typename T_FIRST_STEP, typename T_STANDARD_STEP, typename T_LAST_STEP, typename T_CALC_RESULTS, typename T_INPUTS_PROVIDER, typename T_STORAGE = EmptyType>
    class ModelScheme
    {
    public:
      typedef mpt::Point<typename T_INPUTS_PROVIDER::CoordinatesType> Point;

      ModelScheme(): isSetup_(false), computedForPoint(nullptr){};
      void Setup(shared_ptr<T_INPUTS_PROVIDER> inputs, shared_ptr<T_CALC_RESULTS> results, shared_ptr<T_INIT_TECH> initTech, shared_ptr<T_INIT_MODEL> initModel, shared_ptr<T_FIRST_STEP> firstStep, shared_ptr<T_STANDARD_STEP> standardStep, shared_ptr<T_LAST_STEP> lastStep);
      void Setup(shared_ptr<T_CALC_RESULTS> results, shared_ptr<T_INIT_TECH> initTech, shared_ptr<T_INIT_MODEL> initModel, shared_ptr<T_FIRST_STEP> firstStep, shared_ptr<T_STANDARD_STEP> standardStep, shared_ptr<T_LAST_STEP> lastStep);
      void Setup(shared_ptr<T_INPUTS_PROVIDER> inputs, shared_ptr<T_CALC_RESULTS> results, shared_ptr<T_INIT_TECH> initTech, shared_ptr<T_INIT_MODEL> initModel, shared_ptr<T_FIRST_STEP> firstStep, shared_ptr<T_STANDARD_STEP> standardStep, shared_ptr<T_LAST_STEP> lastStep, shared_ptr<T_STORAGE> storage);
      // That methods returns outputs from the model. It includes an algorithm for checking, each stage of model is active and (if necessary) runs initialization of the model
      template <typename VAR>
      VAR Get(mpt::Point<typename T_INPUTS_PROVIDER::CoordinatesType>* point);

    protected:
      bool isSetup_;                                      ///< Setup method must be fired before the Model can be used; if no, isSetup == false
      Point* computedForPoint;                            ///< Points to nullptr (if just started) or the point and time, for which computations were performed
      shared_ptr<T_INPUTS_PROVIDER> inputs_;       
      shared_ptr<T_CALC_RESULTS> results_;         
      shared_ptr<T_INIT_TECH> initTech_;
      shared_ptr<T_INIT_MODEL> initModel_;
      shared_ptr<T_FIRST_STEP> firstStep_;
      shared_ptr<T_STANDARD_STEP> standardStep_;
      shared_ptr<T_LAST_STEP> lastStep_;
      shared_ptr<T_STORAGE> storage_;

      void Prerequisities(Point* point);  ///< Runs T_INIT_TECH::Exec() and T_INIT_MODEL::Exec() (if needed)
      void Calculate(Point* point);       ///< Runs one of FIRST, STANDARD or LAST_STEP::Exec and sets results
      bool NotComputedForThisPoint(Point* point) const;   ///< false if computations for given points has been executed previously 
    };

    ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////

    template<typename T_INIT_TECH, typename T_INIT_MODEL, typename T_FIRST_STEP, typename T_STANDARD_STEP, typename T_LAST_STEP, typename T_CALC_RESULTS, typename T_INPUTS_PROVIDER, typename T_STORAGE>
    void am3::model::ModelScheme<T_INIT_TECH, T_INIT_MODEL, T_FIRST_STEP, T_STANDARD_STEP, T_LAST_STEP, T_CALC_RESULTS, T_INPUTS_PROVIDER, T_STORAGE>::Calculate(Point* point)
    {
      if (firstStep_->Needed())
      {
        firstStep_->Exec(point);
        results_->SetModelComputations(firstStep_->GetResults());
      }
      else if (standardStep_->Available())
      {
        standardStep_->Exec(point);
        results_->SetModelComputations(standardStep_->GetResults());
      }
      else if (lastStep_->Available())
      {
        lastStep_->Exec(point);
        results_->SetModelComputations(lastStep_->GetResults());
      }
      else lo::log<lo::LOG_MODEL>(lo::SEV_CRITICAL, "Available standard steps exceeded");
    }

    //******************************************************************************************************************************************************************

    template<typename T_INIT_TECH, typename T_INIT_MODEL, typename T_FIRST_STEP, typename T_STANDARD_STEP, typename T_LAST_STEP, typename T_CALC_RESULTS, typename T_INPUTS_PROVIDER, typename T_STORAGE>
    void am3::model::ModelScheme<T_INIT_TECH, T_INIT_MODEL, T_FIRST_STEP, T_STANDARD_STEP, T_LAST_STEP, T_CALC_RESULTS, T_INPUTS_PROVIDER, T_STORAGE>::Prerequisities(Point* point)
    {
      if (initTech_->Needed()) initTech_->Exec(point);
      if (initModel_->Needed()) initModel_->Exec(point);
    }

    //******************************************************************************************************************************************************************

    template<typename T_INIT_TECH, typename T_INIT_MODEL, typename T_FIRST_STEP, typename T_STANDARD_STEP, typename T_LAST_STEP, typename T_CALC_RESULTS, typename T_INPUTS_PROVIDER, typename T_STORAGE>
    bool am3::model::ModelScheme<T_INIT_TECH, T_INIT_MODEL, T_FIRST_STEP, T_STANDARD_STEP, T_LAST_STEP, T_CALC_RESULTS, T_INPUTS_PROVIDER, T_STORAGE>::NotComputedForThisPoint( Point* point ) const
    {
      if (computedForPoint != nullptr)
      {
        if (*computedForPoint == *point) return false;
        else return true;
      }
      else return true;
    }

    //******************************************************************************************************************************************************************

    template<typename T_INIT_TECH, typename T_INIT_MODEL, typename T_FIRST_STEP, typename T_STANDARD_STEP, typename T_LAST_STEP, typename T_CALC_RESULTS, typename T_INPUTS_PROVIDER, typename T_STORAGE>
    void am3::model::ModelScheme<T_INIT_TECH, T_INIT_MODEL, T_FIRST_STEP, T_STANDARD_STEP, T_LAST_STEP, T_CALC_RESULTS, T_INPUTS_PROVIDER, T_STORAGE>::Setup( shared_ptr<T_INPUTS_PROVIDER> inputs, shared_ptr<T_CALC_RESULTS> results, shared_ptr<T_INIT_TECH> initTech, shared_ptr<T_INIT_MODEL> initModel, shared_ptr<T_FIRST_STEP> firstStep, shared_ptr<T_STANDARD_STEP> standardStep, shared_ptr<T_LAST_STEP> lastStep )
    {
      inputs_ = inputs;
      results_ = results;
      initTech_ = initTech;
      initModel_ = initModel; initModel_->Set(inputs);
      firstStep_ = firstStep; firstStep_->Set(inputs);
      standardStep_ = standardStep; standardStep_->Set(inputs);
      lastStep_ = lastStep; lastStep_->Set(inputs);
      isSetup_ = true;
    }

    //******************************************************************************************************************************************************************

    template<typename T_INIT_TECH, typename T_INIT_MODEL, typename T_FIRST_STEP, typename T_STANDARD_STEP, typename T_LAST_STEP, typename T_CALC_RESULTS, typename T_INPUTS_PROVIDER, typename T_STORAGE>
    void am3::model::ModelScheme<T_INIT_TECH, T_INIT_MODEL, T_FIRST_STEP, T_STANDARD_STEP, T_LAST_STEP, T_CALC_RESULTS, T_INPUTS_PROVIDER, T_STORAGE>::Setup( shared_ptr<T_INPUTS_PROVIDER> inputs, shared_ptr<T_CALC_RESULTS> results, shared_ptr<T_INIT_TECH> initTech, shared_ptr<T_INIT_MODEL> initModel, shared_ptr<T_FIRST_STEP> firstStep, shared_ptr<T_STANDARD_STEP> standardStep, shared_ptr<T_LAST_STEP> lastStep, shared_ptr<T_STORAGE> storage )
    {
      storage_ = storage;
      this->Setup(inputs, results, initTech, initModel, firstStep, standardStep);
    }

    //******************************************************************************************************************************************************************

    template<typename T_INIT_TECH, typename T_INIT_MODEL, typename T_FIRST_STEP, typename T_STANDARD_STEP, typename T_LAST_STEP, typename T_CALC_RESULTS, typename T_INPUTS_PROVIDER, typename T_STORAGE>
    template <typename VAR>
    VAR am3::model::ModelScheme<T_INIT_TECH, T_INIT_MODEL, T_FIRST_STEP, T_STANDARD_STEP, T_LAST_STEP, T_CALC_RESULTS, T_INPUTS_PROVIDER, T_STORAGE>::Get(mpt::Point<typename T_INPUTS_PROVIDER::CoordinatesType>* point )
    {
      if (isSetup_)
      {
        if (NotComputedForThisPoint(point))
        {
          Prerequisities(point);
          Calculate(point);
        }
        return results_->Get<VAR>(point);
      } 
      else
      {
        lo::log<lo::LOG_MODEL>(lo::SEV_CRITICAL,"Model was not set up properly");
        VAR emptyReturn;
        return emptyReturn;
      }
    }

  } //model
} //am3
#endif // modelScheme_h__
