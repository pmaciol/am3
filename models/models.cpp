#include "models.h"


// boost::shared_ptr<ada::Adapter> am3::model::Model::OtherModels()
// {
//   return boost::shared_ptr<ada::Adapter>(otherModels_);
// }


// void am3::model::Model::AssignToAdapter( boost::shared_ptr<ada::Adapter> myOwner )
// {
//   otherModels_=myOwner.get();
// }


// int am3::model::Model::Init( am3::adapter::Adapter* myOwner )
// {
//   AssignToAdapter(boost::shared_ptr<ada::Adapter>(myOwner));
//   return Init();
// }

int am3::model::Model::Init()
{
  this->Configure();
  this->PrepareForRun();
  return 0;
}

void am3::model::Model::PrepareForRun()
{
  this->PrepareEnvironment();
  this->PrepareInputFiles();
}
