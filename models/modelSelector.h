
/*! \file modelSelector.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef model_modelSelector_h__
#define model_modelSelector_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include <vector>
#include "models.h"
#include "point.h"

/*	using ***********************************************************/

namespace pnt=am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
		//! Selector gets models. It doesn't care about initialization, just checks are they ready for work. Selector's task is to switch between models basing on KBS
		template<typename PROVIDES_CONTRACT, typename USING_KBS_TYPE, typename POINT>
		class Selector
		{
		public:
			// internal types
			typedef PROVIDES_CONTRACT Contract;
			typedef USING_KBS_TYPE Kbs;
			typedef POINT Point;
			// interface
			void SetKbs(boost::shared_ptr<Kbs>);
			void AddModel(boost::shared_ptr<Contract>);
			bool IsKnowledgeProperForModel();
			boost::shared_ptr<Contract> GetContractProvider(const Point * const);
		};

		//////////////////////////////////////////////////////////////////////////


		//! That version was used for problemPlasticAndHeat. It looks other version will be better. Here only one fine scale interface could be used
    template<typename I_FINE_SCALE, typename I_COARSE_SCALE, typename I_SELECTOR, typename I_ADAPTER, typename COORDINATES>
    class ModelSelector
      : public I_FINE_SCALE
      , public I_COARSE_SCALE
	    , public I_SELECTOR
    {
    public:
      typedef I_FINE_SCALE FINE_SCALE_INTERFACE;
      typedef I_COARSE_SCALE COARSE_SCALE_INTERFACE;
	    typedef I_SELECTOR SELECTOR_INTERFACE;
      typedef I_ADAPTER INTERFACE_FOR_ADAPTER;
		protected:
			typedef std::vector<boost::shared_ptr<FINE_SCALE_INTERFACE> > CONTAINER_OF_INTERFACES;
			typedef std::vector<boost::shared_ptr<Model> > CONTAINER_OF_MODELS;
		public:
			typedef typename CONTAINER_OF_MODELS::iterator iterator;
			//typedef typename CONTAINER_OF_INTERFACES::iterator iterator;
            
      ModelSelector():isFineInterfaceSet_(false),isI4AdapterSet_(false)
	  {
// 		  mpr::ModelID mid;
// 		  mid(-1);
// 		  lastUsedModel_.second=mid;
	  };
      void AddFineInterface(boost::shared_ptr<FINE_SCALE_INTERFACE> fineInterface);
	    void AddModel(boost::shared_ptr<Model> model);
      void SetI4Adapter(boost::shared_ptr<INTERFACE_FOR_ADAPTER> i4Adapter);
      bool IsFineInterfaceSet() const;
      bool IsI4AdapterSet() const;

			iterator begin(){return modelsContainer_.begin();};
			iterator end(){return modelsContainer_.end();};

    protected:
      bool isFineInterfaceSet_;
      bool isI4AdapterSet_;
      CONTAINER_OF_INTERFACES fineInterfacesContainer_;
	    CONTAINER_OF_MODELS modelsContainer_;
      boost::shared_ptr<INTERFACE_FOR_ADAPTER> i4Adapter_;
//	  std::pair<const pnt::Point<COORDINATES>*, typename INTERFACE_FOR_ADAPTER::MODEL_ID> lastUsedModel_;
	 //pnt::Point<COORDINATES>* lastUsedPoint_;
      
      boost::shared_ptr<I_FINE_SCALE> GetFineInterface(const pnt::Point<COORDINATES>* const point ) ;
    };

///////////////////////////////////////////////////////////////////////////// 

    template<typename I_FINE_SCALE, typename I_COARSE_SCALE, typename I_SELECTOR, typename I_ADAPTER ,typename COORDINATES>
    boost::shared_ptr<I_FINE_SCALE>
		am3::model::ModelSelector<I_FINE_SCALE, I_COARSE_SCALE, I_SELECTOR, I_ADAPTER, COORDINATES>::GetFineInterface( const pnt::Point<COORDINATES>* const point ) 
    {
      if (!isFineInterfaceSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"Fine interface of FineModelSelector not set");
      if (!isI4AdapterSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"I4Adapter interface of FineModelSelector not set");
      typename INTERFACE_FOR_ADAPTER::MODEL_ID mId;
      mId=i4Adapter_->GetState(point,&mId);
// 	  this->lastUsedModel_.first=point;
// 	  this->lastUsedModel_.second=mId;
      return fineInterfacesContainer_.at(mId());
    }

    template<typename I_FINE_SCALE, typename I_COARSE_SCALE, typename I_SELECTOR, typename I_ADAPTER, typename COORDINATES>
    bool am3::model::ModelSelector<I_FINE_SCALE, I_COARSE_SCALE, I_SELECTOR, I_ADAPTER, COORDINATES>::IsI4AdapterSet() const
    {
      if (!isI4AdapterSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"I4Adapter interface of ModelSelector not set");
      return isI4AdapterSet_;
    }

    template<typename I_FINE_SCALE, typename I_COARSE_SCALE, typename I_SELECTOR, typename I_ADAPTER, typename COORDINATES>
    bool am3::model::ModelSelector<I_FINE_SCALE, I_COARSE_SCALE,  I_SELECTOR, I_ADAPTER,  COORDINATES>::IsFineInterfaceSet() const
    {
      if (!isFineInterfaceSet_) lo::log<lo::LOG_MODEL>(lo::SEV_ERROR,"Fine interface of ModelSelector not set");
      return isFineInterfaceSet_;
    }

    template<typename I_FINE_SCALE, typename I_COARSE_SCALE, typename I_SELECTOR, typename I_ADAPTER, typename COORDINATES>
    void am3::model::ModelSelector<I_FINE_SCALE, I_COARSE_SCALE,  I_SELECTOR, I_ADAPTER,  COORDINATES>::SetI4Adapter( boost::shared_ptr<INTERFACE_FOR_ADAPTER> i4Adapter )
    {
      i4Adapter_=i4Adapter;
      isI4AdapterSet_=true;
    }

    template<typename I_FINE_SCALE, typename I_COARSE_SCALE, typename I_SELECTOR, typename I_ADAPTER, typename COORDINATES>
    void am3::model::ModelSelector<I_FINE_SCALE, I_COARSE_SCALE,  I_SELECTOR, I_ADAPTER,  COORDINATES>::AddFineInterface( boost::shared_ptr< I_FINE_SCALE> fineInterface )
    {
      fineInterfacesContainer_.push_back(fineInterface);
      isFineInterfaceSet_=true;
    }

	template<typename I_FINE_SCALE, typename I_COARSE_SCALE, typename I_SELECTOR, typename I_ADAPTER, typename COORDINATES>
	void am3::model::ModelSelector<I_FINE_SCALE, I_COARSE_SCALE,  I_SELECTOR, I_ADAPTER,  COORDINATES>::AddModel(boost::shared_ptr<Model> model)
	{
		modelsContainer_.push_back(model);
		
	}
	
  } //model
} //am3
#endif // model_modelSelector_h__
