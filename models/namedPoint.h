
/*! \file namedPoint.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef namedPoint_h__
#define namedPoint_h__
/*	include *********************************************************/

#include <map>
#include <vector>
#include <boost/enable_shared_from_this.hpp>
#include "../models/dataStructures.h"
#include "../logger/logger.h"
#include "./point.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace lo  = am3::logger;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace common
    {

			template<typename T_COORDINATE>
			class NamedPointsStorageLastTwo
			{
			public:
				typedef std::pair<T_COORDINATE, mpr::Time> CoordinatesSinceTime;
				typedef T_COORDINATE Coordinate;

				NamedPointsStorageLastTwo(const typename CoordinatesSinceTime& point);
				bool IsCoordEqualToLastStored(const T_COORDINATE& coord ) const;
				void MovePoint(const CoordinatesSinceTime& point);
				mpr::Time GetLastTime() const;
				Coordinate GetCoordinates(const mpr::Time& time) const;

			protected:
				CoordinatesSinceTime actual_, previous_;
			};

//////////////////////////////////////////////////////////////////////////
			template<typename T_STORAGE>
			class NamedPointsContainer
			{
			protected:
				typedef const unsigned long Id;
				std::map<Id,T_STORAGE> mainNamedPointsContainer_;

			public:
				typedef T_STORAGE Storage;
				typedef typename Storage::Coordinate Coordinate;
				typedef typename std::map<Id,typename T_STORAGE>::const_iterator InternalID;

				InternalID AddNewPointOrMoveOld( const unsigned long &pointId , const typename T_STORAGE::Coordinate& coordinates , const mpr::Time time);
				bool IsPointAvailable(const unsigned long &pointId ) const;
				mpr::Time GetLastTime(const unsigned long &pointId) const;
				typename T_STORAGE::Coordinate GetLastCoordinates(const unsigned long &pointId) const;
				InternalID GetHandler(const unsigned long &pointId ) const;
				void ClearAllPoints();
				Id GetId(InternalID internalId) const
				{
					return internalId->first;
				}
			};

//////////////////////////////////////////////////////////////////////////
			
			template<typename T_CONTAINER>
			class NamedPoint : public Point<typename T_CONTAINER::Coordinate>, public boost::enable_shared_from_this<NamedPoint<T_CONTAINER> >
			{
			public:
				NamedPoint(const unsigned long &pointId , const typename T_CONTAINER::Coordinate& coordinates, const mpr::Time time);
        NamedPoint(const unsigned long &pointId);
				NamedPoint(const NamedPoint* const that);
				bool operator==(const NamedPoint<T_CONTAINER>& that) const;

				const virtual typename T_CONTAINER::Coordinate Coordinates() const;
				double virtual GetTime() const;
        mpr::Time virtual GetTimeProperty() const;
				static void ClearAllPoints();
				unsigned long GetId() const
				{
					return pointsContainer_.GetId(containerReturnedId_);
				}

				virtual boost::shared_ptr<Point<typename T_CONTAINER::Coordinate> > Copy();
        virtual boost::shared_ptr<Point<typename T_CONTAINER::Coordinate> > Snapshot() const;
        virtual void Move(const Point<typename T_CONTAINER::Coordinate>& newPoint );
        virtual void Move(const typename T_CONTAINER::Coordinate& newCoordinates, const double newTime );
//        static bool Exist(const unsigned long &pointId)  {return pointsContainer_.IsPointAvailable(pointId);}
        virtual bool SamePointDifferentTime(const Point<CoordinatesType>& thatPoint) override;
        virtual bool SamePoint(const Point<CoordinatesType>& thatPoint) override;
        bool SamePointDifferentTime(const NamedPoint<T_CONTAINER>& thatPoint);
        bool SamePoint(const NamedPoint<T_CONTAINER>& thatPoint);
        
			private:
				const unsigned long pointId_;
				static T_CONTAINER pointsContainer_;
				typename T_CONTAINER::InternalID containerReturnedId_;
			};
//////////////////////////////////////////////////////////////////////////
			
			typedef NamedPointsStorageLastTwo<mpr::Coordinates2D> NamedPointsStorageLastTwoSteps2D;
			typedef NamedPointsContainer<NamedPointsStorageLastTwoSteps2D > NamedPointsContainerLastTwoSteps2D;
			
			typedef NamedPoint<NamedPointsContainerLastTwoSteps2D> NamedPointLastTwoSteps2D;

			template <typename T_NAMED_POINTS_IMPLEMENTATION>
			inline void ClearAllPoints();

			template <>
			inline void ClearAllPoints<NamedPointLastTwoSteps2D>()
			{
				NamedPointLastTwoSteps2D::ClearAllPoints();
			};

//////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION
//////////////////////////////////////////////////////////////////////////


      template<typename T_CONTAINER>
      bool am3::model::common::NamedPoint<T_CONTAINER>::SamePointDifferentTime(const NamedPoint<T_CONTAINER>& thatPoint)
      {
        if ((thatPoint.GetId() == GetId()) &&
          this->GetTime()!= thatPoint.GetTime())
        {
          return true;
        }
        else
        {
          return false;
        }

      }

      template<typename T_CONTAINER>
      bool am3::model::common::NamedPoint<T_CONTAINER>::SamePoint(const NamedPoint<T_CONTAINER>& thatPoint)
      {
        if (thatPoint.GetId() == GetId())
        {
          return true;
        }
        else
        {
          return false;
        }

      }





      template<typename T_CONTAINER>
      bool am3::model::common::NamedPoint<T_CONTAINER>::SamePointDifferentTime(const Point<CoordinatesType>& thatPoint)
      {
        return false;
      }

      template<typename T_CONTAINER>
      bool am3::model::common::NamedPoint<T_CONTAINER>::SamePoint(const Point<CoordinatesType>& thatPoint)
      {
        return false;
      }



      template<typename T_CONTAINER>
      void am3::model::common::NamedPoint<T_CONTAINER>::Move(const typename T_CONTAINER::Coordinate& newCoordinates, const double newTime )
      {
        pointsContainer_.AddNewPointOrMoveOld(pointId_,newCoordinates,newTime);
      }

      template<typename T_CONTAINER>
      void am3::model::common::NamedPoint<T_CONTAINER>::Move(const Point<typename T_CONTAINER::Coordinate>& newPoint )
      {
        pointsContainer_.AddNewPointOrMoveOld(pointId_,newPoint.Coordinates(),newPoint.GetTime());
      }

			template<typename T_COORDINATE>
			T_COORDINATE am3::model::common::NamedPointsStorageLastTwo<T_COORDINATE>::GetCoordinates( const mpr::Time& time ) const
			{
				if (time==actual_.second)
				{
					return actual_.first;
				} 
				else if (time==previous_.second)
				{
					return previous_.first;
				}
				else 
				{
					lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Asked for coordinates of existing point in not existing time");
					T_COORDINATE errorBack;
					return (errorBack);
				}
			}

			template<typename T_COORDINATE>
			void am3::model::common::NamedPointsStorageLastTwo<T_COORDINATE>::MovePoint( const CoordinatesSinceTime& point )
			{
//                  //DebugBreak();
				if (point.second>=actual_.second)
				{
					previous_=actual_;
					actual_=point;
				} 
				else
				{

					lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Cannot add coordinates in time smaller than last time");
				}
				
			}

			template<typename T_COORDINATE>
			am3::model::common::NamedPointsStorageLastTwo<T_COORDINATE>::NamedPointsStorageLastTwo( const CoordinatesSinceTime& point ): actual_(point), previous_(point)
			{
			}

			template<typename T_COORDINATE>
			bool am3::model::common::NamedPointsStorageLastTwo<T_COORDINATE>::IsCoordEqualToLastStored( const T_COORDINATE& coord ) const
			{
				return (coord==actual_.first)? true:false;
			}

			template<typename T_COORDINATE>
			mpr::Time am3::model::common::NamedPointsStorageLastTwo<T_COORDINATE>::GetLastTime() const
			{
				return actual_.second;
			}

//////////////////////////////////////////////////////////////////////////

			template<typename T_STORAGE>
			typename T_STORAGE::Coordinate am3::model::common::NamedPointsContainer<T_STORAGE>::GetLastCoordinates( const unsigned long &pointId ) const
			{
				std::map<Id,T_STORAGE>::const_iterator itc;
				itc=mainNamedPointsContainer_.find(pointId);
				if (itc!=mainNamedPointsContainer_.end())
				{

					return itc->second.GetCoordinates(itc->second.GetLastTime());
				} 
				else
				{
					lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Asked for coordinates of non-existing point");
					T_STORAGE::Coordinate errorBack;
					return (errorBack);
				}
			}

			template<typename T_STORAGE>
			bool am3::model::common::NamedPointsContainer<T_STORAGE>::IsPointAvailable( const unsigned long &pointId ) const
			{
				std::map<Id,T_STORAGE>::const_iterator itc;
				itc=mainNamedPointsContainer_.find(pointId);
				return (itc==mainNamedPointsContainer_.end())?false:true;
			}


			template<typename T_STORAGE>
			typename am3::model::common::NamedPointsContainer<T_STORAGE>::InternalID am3::model::common::NamedPointsContainer<T_STORAGE>::AddNewPointOrMoveOld( const unsigned long &pointId , const typename T_STORAGE::Coordinate& coordinates , const mpr::Time time )
			{
				InternalID toRet;
				T_STORAGE::CoordinatesSinceTime newPoint(coordinates,time);
				if (!IsPointAvailable(pointId))
				{
					toRet=(mainNamedPointsContainer_.insert(std::pair<Id,T_STORAGE>(pointId,T_STORAGE(newPoint)))).first;;
				}
				else
				{
					std::map<Id,T_STORAGE>::iterator it;
					it=mainNamedPointsContainer_.find(pointId);
					it->second.MovePoint(newPoint);
					toRet=it;
				}
				return toRet;
			}

			template<typename T_STORAGE>
			mpr::Time am3::model::common::NamedPointsContainer<T_STORAGE>::GetLastTime( const unsigned long &pointId ) const
			{
				std::map<Id,T_STORAGE>::const_iterator itc;
				itc=mainNamedPointsContainer_.find(pointId);
				if (itc!=mainNamedPointsContainer_.end())
				{

					return itc->second.GetLastTime();
				} 
				else
				{
					lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Asked for time of non-existing point");
					mpr::Time errorBack;
					return (errorBack);
				}
			}

			template<typename T_STORAGE>
			typename am3::model::common::NamedPointsContainer<T_STORAGE>::InternalID am3::model::common::NamedPointsContainer<T_STORAGE>::GetHandler(const unsigned long &pointId ) const
			{
				std::map<Id,typename T_STORAGE>::const_iterator h;
				unsigned long pointId_ = pointId; 
				h=mainNamedPointsContainer_.find(pointId_);
				if (h!=mainNamedPointsContainer_.end())
				{
					return h;
				} 
				else
				{
					lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"No point with this Id");
					return (h);
				}
			}

			template<typename T_STORAGE>
			void am3::model::common::NamedPointsContainer<T_STORAGE>::ClearAllPoints()
			{
				mainNamedPointsContainer_.clear();
			}

//////////////////////////////////////////////////////////////////////////

			template<typename T_CONTAINER>
			const typename T_CONTAINER::Coordinate am3::model::common::NamedPoint<T_CONTAINER>::Coordinates() const
			{
				return pointsContainer_.GetLastCoordinates(pointId_);
			}

			template<typename T_CONTAINER>
			double am3::model::common::NamedPoint<T_CONTAINER>::GetTime() const
			{
				return (pointsContainer_.GetLastTime(pointId_)).Value();
			}

      template<typename T_CONTAINER>
      mpr::Time am3::model::common::NamedPoint<T_CONTAINER>::GetTimeProperty() const
      {
        return (pointsContainer_.GetLastTime(pointId_));
      }

			template<typename T_CONTAINER>
			bool am3::model::common::NamedPoint<T_CONTAINER>::operator==( const NamedPoint<T_CONTAINER>& that ) const
			{
				return (containerReturnedId_==that.containerReturnedId_)?true:false;
			}

			template<typename T_CONTAINER>
			am3::model::common::NamedPoint<T_CONTAINER>::NamedPoint( const unsigned long &pointId , const typename T_CONTAINER::Coordinate& coordinates, const mpr::Time time ): pointId_(pointId)
			{
				pointsContainer_.AddNewPointOrMoveOld(pointId,coordinates,time);
				containerReturnedId_=pointsContainer_.GetHandler(pointId);
			}

      template<typename T_CONTAINER>
			am3::model::common::NamedPoint<T_CONTAINER>::NamedPoint( const unsigned long &pointId): pointId_(pointId)
			{
				containerReturnedId_=pointsContainer_.GetHandler(pointId);
			}

			template<typename T_CONTAINER>
			am3::model::common::NamedPoint<T_CONTAINER>::NamedPoint(const NamedPoint* const that): pointId_(that->GetId())
			{
//				pointsContainer_.AddNewPointOrMoveOld(that->GetId() ,that->Coordinates(),that->GetTime());
				containerReturnedId_=pointsContainer_.GetHandler(pointId_);
			}

			template<typename T_CONTAINER>
			void am3::model::common::NamedPoint<T_CONTAINER>::ClearAllPoints()
			{
				pointsContainer_.ClearAllPoints();
			}
			
			template<typename T_CONTAINER>
			boost::shared_ptr<Point<typename T_CONTAINER::Coordinate> > am3::model::common::NamedPoint<T_CONTAINER>::Copy()
			{
				boost::shared_ptr<am3::model::point::Point<typename T_CONTAINER::Coordinate> > newPoint = boost::shared_ptr<Point<typename T_CONTAINER::Coordinate> > (new am3::model::common::NamedPoint<T_CONTAINER>(this));
				return newPoint;
			};

      template<typename T_CONTAINER>
      boost::shared_ptr<Point<typename T_CONTAINER::Coordinate> > am3::model::common::NamedPoint<T_CONTAINER>::Snapshot() const
      {
        boost::shared_ptr<am3::model::point::Point<typename T_CONTAINER::Coordinate> > newPoint = boost::shared_ptr<Point<typename T_CONTAINER::Coordinate> > (new am3::model::point::ComputedPoint<typename T_CONTAINER::Coordinate>(this));
        return newPoint;
      }

			template<typename T_CONTAINER> T_CONTAINER am3::model::common::NamedPoint<T_CONTAINER>::pointsContainer_;

    } //common
  } //model
} //am3

#endif // namedPoint_h__

