
/*! \file inputTypelists.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef inputTypelists_h__
#define inputTypelists_h__
/*	include *********************************************************/



/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		template<typename BUS>
    struct InputTypelist
    {
      InputTypelist(){ bus_ = nullptr; }
			typedef BUS Bus;
			void SetModelInput(BUS* bus)
      {
        bus_ = bus;
      }
    protected:
			BUS* bus_;
    };
	} //model
} //am3
#endif // inputTypelists_h__
