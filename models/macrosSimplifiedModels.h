
/*! \file macrosSimplifiedModels.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef macrosSimplifiedModels_h__
#define macrosSimplifiedModels_h__
/*	include *********************************************************/

#include <boost/preprocessor/repetition.hpp>
#include <boost/preprocessor/cat.hpp>
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/list.hpp>
#include <boost/preprocessor/debug/assert.hpp>
#include <boost/preprocessor/comparison/equal.hpp>
#include <boost/preprocessor/control/if.hpp>
#include <boost/preprocessor/list/adt.hpp>
#include <boost/typeof/typeof.hpp>
#include <boost/preprocessor/variadic/to_list.hpp>

#include "../models/dataStructures.h"
#include "../models/contractMacros.h"
#include "../common/typelists.h"
#include "../databus/databus.h"

/*	using ***********************************************************/

namespace moc = am3::model::contract;
namespace mpr = am3::model::properties;
namespace dtb = am3::databus;

/*	extern **********************************************************/



/*	classes *********************************************************/

#define EXPAND(x) x
#define PP_NARGS(...) \
	EXPAND(_xPP_NARGS_IMPL(__VA_ARGS__,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0))

#define VARIABLES(r, data, elem)  ,elem
#define TL(r, data, elem) ,Typelist<elem
#define TLCLOSE(r, data, elem)  > 
#define TYPELIST(VAR1, L) Typelist<VAR1 BOOST_PP_LIST_FOR_EACH(TL, _, L) ,EmptyType BOOST_PP_LIST_FOR_EACH(TLCLOSE, _, L) > 
#define CONTRACT(NAME,LENGTH,DIMENSION,...) \
	typedef moc::Contract<TypelistTuple<TYPELIST(\
	BOOST_PP_LIST_FIRST(BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__)), BOOST_PP_LIST_REST(BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__))),\
	std::tuple<BOOST_PP_LIST_FIRST(BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__)) BOOST_PP_LIST_FOR_EACH(VARIABLES, _, BOOST_PP_LIST_REST(BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__)))> >,DIMENSION> NAME;


#define ADD_HANDLER(r, data, CONTRACT) , public dtb::Handler<CONTRACT>
#define ADD_USING(r, data, CONTRACT) using dtb::Handler<CONTRACT>::Set; 
#define DATABUS(NAME,STORAGE,...) \
struct NAME : public dtb::PreviousStepStorage<STORAGE>\
  typedef dtb::PreviousStepStorage<STORAGE> PreviousStepStorage; typedef STORAGE Storage;\
	BOOST_PP_LIST_FOR_EACH(ADD_HANDLER,_,BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__)) { BOOST_PP_LIST_FOR_EACH(ADD_USING,_,BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__)) };

#define DATABUS_NO_STORAGE(NAME,FIRST,...) \
struct NAME : public dtb::Handler<FIRST>\
  BOOST_PP_LIST_FOR_EACH(ADD_HANDLER,_,BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__)) { using dtb::Handler<FIRST>::Set; BOOST_PP_LIST_FOR_EACH(ADD_USING,_,BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__)) };

#define MODEL_HEADER(NAME,CONTRACT,INPUTS) struct NAME	: public CONTRACT	, public mo::Inputs<INPUTS> {
#define MODEL_FOOTER };
#define GET_STATE_DECLARATION(z,n,CONTRACT)	virtual std::tuple_element<n,CONTRACT::VariablesTypes::Tuple>::type GetState(pnt::Point<CONTRACT::CoordinatesType> *,const std::tuple_element<n,CONTRACT::VariablesTypes::Tuple>::type *const );
#define MODEL(NAME,CONTRACT,CONTRACTLENGTH,INPUTS) MODEL_HEADER(NAME,CONTRACT,INPUTS) BOOST_PP_REPEAT(CONTRACTLENGTH,GET_STATE_DECLARATION,CONTRACT)	MODEL_FOOTER


#define MODEL_IMPL(NAME, VARIABLE) VARIABLE NAME::GetState(pnt::Point<BaseCoordinates> * point,const VARIABLE *const )

#define MODEL_INSTANCE(NAME,TYPE) boost::shared_ptr<TYPE> NAME (new TYPE);
#define MODEL_WITH_STORAGE_INSTANCE(NAME,TYPE,STORAGE_OBJECT) boost::shared_ptr<TYPE> NAME (new TYPE(STORAGE_OBJECT));
#define DATABUS_INSTANCE(NAME,TYPE) boost::shared_ptr<TYPE> NAME (new TYPE);

#define BIND_MODEL_TO_BUS(r,DB_NAME,MODEL_NAME) ada::BindModelToBus(DB_NAME,MODEL_NAME);
#define DATABUS_ADD_MODELS(NAME,...) BOOST_PP_LIST_FOR_EACH(BIND_MODEL_TO_BUS, NAME , BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__));

#define BIND_BUS_TO_MODEL_TYPES(r,DB_TYPE,MODEL_TYPE) ada::InputsProvider<MODEL_TYPE,DB_TYPE> inputsProviderFor##MODEL_TYPE;
#define DATABUS_OUTPUTS_FOR_MODELS(DB_TYPE,...) BOOST_PP_LIST_FOR_EACH(BIND_BUS_TO_MODEL_TYPES, DB_TYPE , BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__));

#define INPUTS_PROVIDER(MODEL_TYPE) inputsProviderFor##MODEL_TYPE

#define MODEL_INPUT_FROM_DATABUS(MODEL_NAME,DB_NAME) \
  typedef BOOST_TYPEOF(MODEL_NAME) PointerTypeOf##MODEL_NAME ; \
  typedef PointerTypeOf##MODEL_NAME::element_type TypeOf##MODEL_NAME;\
  ada::InputsProvider<TypeOf##MODEL_NAME,BOOST_TYPEOF(DB_NAME)::element_type> INPUTS_PROVIDER(TypeOf##MODEL_NAME); \
  INPUTS_PROVIDER(TypeOf##MODEL_NAME).Set(DB_NAME,MODEL_NAME);

#define KBS_INSTANCE(NAME,TYPE,SOURCE_OF_VARIABLES_NAME) boost::shared_ptr<TYPE> NAME (new TYPE(SOURCE_OF_VARIABLES_NAME));

//#define SWITCHER_INSTANCE(NAME,CONTRACT_NAME,KBS_NAME) boost::shared_ptr<ada::Switcher<BOOST_TYPEOF(KBS_NAME)::element_type, CONTRACT_NAME> > NAME(new ada::Switcher<BOOST_TYPEOF(KBS_NAME)::element_type, CONTRACT_NAME>(KBS_NAME));

//#define ADD_TO_SWITCHER(r,SWITCHER_NAME,MODEL_NAME) SWITCHER_NAME->AddModel(MODEL_NAME);
//#define ADD_MODELS_TO_SWITCHER(SWITCHER_NAME,...) BOOST_PP_LIST_FOR_EACH(ADD_TO_SWITCHER, SWITCHER_NAME , BOOST_PP_VARIADIC_TO_LIST(__VA_ARGS__));

//#define DEFORM_ENTRY(DB_NAME, STRESS_MODEL,STRESS_D_STRAIN,STRESS_D_STRAINRATE) boost::shared_ptr<ActualDeformHandler> deformHandlerPtr(new ActualDeformHandler(STRESS_MODEL,STRESS_D_STRAIN,STRESS_D_STRAINRATE)); deformHandler = deformHandlerPtr.get(); DB_NAME->SetPreviousStepsStorage(deformHandlerPtr);;
#define DEFORM_ENTRY(DEFORM_MODEL, DB_NAME, STRESS_MODEL,STRESS_D_STRAIN,STRESS_D_STRAINRATE) \
  boost::shared_ptr<DEFORM_MODEL> externalSoftwareModel (new DEFORM_MODEL(STRESS_MODEL,STRESS_D_STRAIN,STRESS_D_STRAINRATE));

#define CASE_MODEL(CASE_MODEL_TYPE,CASE_MODEL_NAME)\
  boost::shared_ptr<CASE_MODEL_TYPE> CASE_MODEL_NAME (new CASE_MODEL_TYPE(externalSoftwareModel));

#define MODEL_INSTANCE_REDESIGN(SOURCE_MODEL_TYPE, MODEL_TYPE, DATABUS_TYPE, MODEL_NAME, DATABUS_NAME)\
  typedef am3::databus::ModelInstanceType<SOURCE_MODEL_TYPE, DATABUS_TYPE>::Type MODEL_TYPE;\
  boost::shared_ptr<MODEL_TYPE> MODEL_NAME(new MODEL_TYPE);\
  am3::databus::SetModelInstance<DATABUS_TYPE, SOURCE_MODEL_TYPE>(DATABUS_NAME.get(), MODEL_NAME);\

#define MODEL_INSTANCE_WITH_PARAM(SOURCE_MODEL_TYPE, MODEL_TYPE, DATABUS_TYPE, MODEL_NAME, DATABUS_NAME, CONSTRUCTOR_PARAM)\
  typedef am3::databus::ModelInstanceType<SOURCE_MODEL_TYPE, DATABUS_TYPE>::Type MODEL_TYPE;\
  boost::shared_ptr<MODEL_TYPE> MODEL_NAME(new MODEL_TYPE(CONSTRUCTOR_PARAM));\
  am3::databus::SetModelInstance<DATABUS_TYPE, SOURCE_MODEL_TYPE>(DATABUS_NAME.get(), MODEL_NAME);\

#define MODEL_INSTANCE_IN_SWITCHER(SOURCE_MODEL_TYPE, MODEL_TYPE,  MODEL_NAME, DATABUS_NAME, SWITCHER_TYPE, SWITCHER_NAME)\
	typedef SOURCE_MODEL_TYPE<typename SWITCHER_TYPE::Bus> MODEL_TYPE;\
  boost::shared_ptr<MODEL_TYPE> MODEL_NAME(new MODEL_TYPE);\
  MODEL_NAME->SetModelInput(DATABUS_NAME.get());\
  SWITCHER_NAME->AddModel(MODEL_NAME); \


#endif // macrosSimplifiedModels_h__
