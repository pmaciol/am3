/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef contracts_h__
#define contracts_h__
/*	include *********************************************************/

#include <string>
//#include "../common/model.h"
#include "../logger/logger.h"
#include "dataStructures.h"
#include "boost/shared_ptr.hpp"


/*	using ***********************************************************/

namespace pro=am3::model;
namespace lo=am3::logger;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace contract
    {

	  #define USING_GET_STATE(T); //using moc::Provider<T,mpr::Coordinates2D>::GetState;

      template<typename TYPE>
      inline TYPE ZeroValue(const TYPE* const p)
      {
        TYPE retVal;
        retVal(0.0);
        return retVal;
      }

      template<>
      inline std::string ZeroValue<std::string>(const std::string* const p)
      {
        std::string retVal;
        return retVal;
      }
  //////////////////////////////////////////////////////////////////////////
  /* \brief Deriving from Provider ensures that derived class can provide PROPERTY
  *
  * This interface is used to _provide_, not _compute_ property. Property value can be provided, but have to be taken from somewhere else
  * Classes computing properties uses am3::model::contract::ContractItemDispatcher (s)
  */
  template <typename PROPERTY,typename T_COORDINATE=am3::model::Coordinates>
  struct Provider
  {
    virtual PROPERTY GetState(am3::model::common::Point<T_COORDINATE>* point,const PROPERTY* const typeTrait)=0;
		virtual PROPERTY GetState(am3::model::common::Point<T_COORDINATE>* point)
		{
			const PROPERTY* const typeTrait_=nullptr;
			return this->GetState(point,typeTrait_);
		}
        template <typename T>
    PROPERTY GetState(am3::model::common::Point<T_COORDINATE>* point)
	{
		static_assert(std::is_same<T, PROPERTY>::value, "This Provider does not provide this PROPERTY");
		const PROPERTY* const typeTrait_ = nullptr;
		return this->GetState(point, typeTrait_);
	}
  };

  //////////////////////////////////////////////////////////////////////////
  /* \brief Deriving from ContractItemDispatcher ensures that derived class can compute PROPERTY
  */
  template<typename PROPERTY,typename T_COORDINATE=am3::model::Coordinates>
  class ContractItemDispatcher:public Provider<PROPERTY,T_COORDINATE>
  {
    Provider<PROPERTY,T_COORDINATE>* calculator_;
  public:
    ContractItemDispatcher(){calculator_=nullptr;};
    void SetCalculator(Provider<PROPERTY>* calculator)
		{
			calculator_=calculator;
		}
    void SetCalculator(boost::shared_ptr<Provider<PROPERTY> >calculator){calculator_=calculator.get();}

    ContractItemDispatcher<PROPERTY>* Dispatcher(/*const PROPERTY* const propertyTrait*/){return this;};
    virtual PROPERTY GetState(am3::model::common::Point<T_COORDINATE>* point, const PROPERTY* const p)/*=0*/
    {
      if (calculator_!=nullptr) {return calculator_->GetState(point,p);}
      else 
        {
          lo::log<lo::SEV_ALERT>(lo::LOG_MODEL,"Calculator in ContractItemDispatcher not initialized");
          exit(-1);
          PROPERTY prop;
          return prop;
      }
    }
    ~ContractItemDispatcher(){};
  };
  //////////////////////////////////////////////////////////////////////////
  namespace defined
      {
  /** \defgroup Predefined Provider, used for tests*/
  /*@{*/
  struct VCTProvider
    : public Provider<pro::VelocityWithDerivatives>
    , public Provider<pro::Coordinates>
    , public Provider<pro::Time> {};

  struct ViscProvider
    :public Provider<pro::Viscosity>
  {
    virtual ViscProvider* clone() const = 0;
    virtual ~ViscProvider(){};
  };

  inline ViscProvider* new_clone(ViscProvider const& other)
  {
    return other.clone();
  }

  struct ModelIdProvider
    : public Provider<pro::ModelID> {};
  /*@}*/

  /** \defgroup Predefined Calculators, used for tests*/
  /*@{*/
  struct VCTContract
    : public ContractItemDispatcher<pro::VelocityWithDerivatives>,
    public ContractItemDispatcher<pro::Coordinates>,
    public ContractItemDispatcher<pro::Time>
  {virtual ~VCTContract(){}};

  struct ViscContract
    : public ContractItemDispatcher<pro::Viscosity>
    {virtual ~ViscContract(){}};

  /*@}*/

      } //defined
    } //contract
  } //model
} //am3

#endif // contracts_h__
