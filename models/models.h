
/*! \file models.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef models_h__
#define models_h__
/*	include *********************************************************/

#include "../adapter/adapter.h"
#include "../databus/adapter.h"
#include <string>
#include <boost/shared_ptr.hpp>
#include "../models/contracts.h"
#include "../models/point.h"

/*	using ***********************************************************/

// namespace am3
// {
//   namespace adapter
//   {
//     class Adapter;
//   } //adapter
// } //am3

namespace ada=am3::adapter;
namespace moc=am3::model::contract;
namespace pnt = am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    class InitializableMainModel
    {
    public:
      virtual bool IsInitialized() const = 0;
      virtual void Init() = 0;
    };

    // All below since 'HERE' is legacy. Instead use DatabusTypelist
/*		template<typename T1,typename COORDINATES>
		struct Get
		{
		public:
			void Set (boost::shared_ptr< moc::Provider<T1,COORDINATES> > input)
			{
				input_=input;
			};
			T1 InPoint(pnt::Point<COORDINATES> * point) const
			{
				T1* trait=nullptr;
				//return input_->GetState(point,trait);
        return input_->GetState<T1>(point);
			};
			boost::shared_ptr<boost::shared_ptr< moc::Provider<T1,COORDINATES> > > GetPointer(){return input_};
		protected:
			boost::shared_ptr< moc::Provider<T1,COORDINATES> > input_;
		};


		template <typename T1, typename CONTRACT>
		struct InputsInternal;

		template <typename T1,typename T2, typename CONTRACT>
		struct InputsInternal<TypelistTuple<T1,T2>, CONTRACT>
			: public InputsInternal<T1, CONTRACT>
		{};

		template <typename T1, typename T2, typename CONTRACT>
		struct InputsInternal<Typelist<T1, T2>, CONTRACT>
			: public InputsInternal<T2, CONTRACT>
			, public Get<T1,typename CONTRACT::CoordinatesType>
		{
			void Set (boost::shared_ptr<CONTRACT> input)
			{
				Get<T1,typename CONTRACT::CoordinatesType>::Set(static_cast<boost::shared_ptr< moc::Provider<T1,mpr::Coordinates2D> > >(input));
				InputsInternal<T2, CONTRACT>::Set(input);
			};
		};


		template<typename CONTRACT>
		struct InputsInternal<EmptyType, CONTRACT>
		{
			void Set(boost::shared_ptr<CONTRACT> contract){};
			typedef typename CONTRACT::CoordinatesType BaseCoordinates;
			typedef CONTRACT Input;
		};

		template<typename CONTRACT>
		struct Inputs : public InputsInternal<typename CONTRACT::VariablesTypes,CONTRACT>{};

		// HERE
    //////////////////////////////////////////////////////////////////////////
*/

    class Model
    {
    public:
//      Model():otherModels_(nullptr){};
      virtual ~Model(){};      
      
      virtual int Init();
//      virtual void AssignToAdapter(boost::shared_ptr<ada::Adapter> myOwner);
      virtual int Run()=0;
      virtual int PostProcess()=0;
	    //virtual const std::string Name() {return "";};
			virtual const std::string Name() const {return "";};

      
      /// legacy!
//      virtual int Init(am3::adapter::Adapter* myOwner);
    protected:
  //    am3::adapter::Adapter* otherModels_;
  //    boost::shared_ptr<ada::Adapter> OtherModels();
      virtual void Configure()=0;
      virtual void PrepareForRun();
      virtual void PrepareEnvironment()=0;
      virtual void PrepareInputFiles()=0;
    };


		template <typename T>
		class SingleModelAvailable: public Model
		{
		private:
			/*static*/ const std::string name_;
		public:
			virtual const std::string Name() const {return name_;};
			// /*static*/ const std::string Name() {return name_;};
		SingleModelAvailable(const std::string& name):name_(name) {};
		};

		class MultiplyModelAvailable: public Model
		{
		private:
			const std::string name_;
		public:
			MultiplyModelAvailable(const std::string& name):name_(name){};
			//! Obsolete constructor; defined to keep compatibility with old models.
			MultiplyModelAvailable():name_("NOT SET DUE TO OBSOLETE CREATION"){};
			virtual const std::string Name() const {return name_;};
		};

    //! Base class for all material models.
    template<typename T_INPUT,typename T_PROPERTY>
    class BaseModel:public T_PROPERTY
    {
    public:
      //! Main function of model. It have to be specialized in each model.
      T_PROPERTY GetState(T_INPUT) const;
      virtual ~BaseModel(){};
    };

		template<typename COORDINATES>
		class ComputationsOnFirstCall
		{
		protected:
			virtual ~ComputationsOnFirstCall(){};
			virtual void GetData(pnt::Point<COORDINATES> * point)=0;
			virtual void Calculate(pnt::Point<COORDINATES> * point)=0;

			bool HaveDataInPoint(pnt::Point<COORDINATES> * point) const;
			void CheckOrGetDataInPoint(pnt::Point<COORDINATES> * point);
    private:
			boost::shared_ptr<pnt::Point<COORDINATES> > actualPoint;
		};

		template<typename COORDINATES>
		void ComputationsOnFirstCall<COORDINATES>::CheckOrGetDataInPoint(pnt::Point<COORDINATES> * point)
		{
			if (!HaveDataInPoint(point))
			{
				this->GetData(point);
				this->Calculate(point);
        actualPoint = point->Copy();
			}
		}

		template<typename COORDINATES>
		bool ComputationsOnFirstCall<COORDINATES>::HaveDataInPoint(pnt::Point<COORDINATES> * point) const
		{
			bool toRet;
			if (actualPoint)
			{
				((*point) == (*actualPoint))? toRet = true  : toRet = false;
			} 
			else
			{
				toRet = false;
			}					
			return toRet;
		}

    template<typename HANDLER, typename CONTRACT>
    class MacroModel : public CONTRACT
    {
    public:
      typedef HANDLER HandlerType;
      MacroModel(boost::shared_ptr<HANDLER> pointerToMacroHandler): pointerToMacroHandler_(pointerToMacroHandler){};
//      ~MacroModel(){};
//       void SetHandler(boost::shared_ptr<HANDLER> pointerToMacroHandler)
//       {
//         pointerToMacroHandler_ = pointerToMacroHandler;
//       };
      boost::shared_ptr<HANDLER> GetHandler()
      {
        return pointerToMacroHandler_;
      }
    protected:
      boost::shared_ptr<HANDLER> pointerToMacroHandler_;
    };

    template<typename MACROSCALE_MODEL, typename IV_MODEL>
    class CaseMacroImplementation
      : public MACROSCALE_MODEL
      , public IV_MODEL
    {
    public:
      using IV_MODEL::pointerToHandler_;
      CaseMacroImplementation(boost::shared_ptr<typename MACROSCALE_MODEL::HandlerType::ExternalSoftwareInputs> externalSoftwareInputsModel)// : MACROSCALE_MODEL(externalSoftwareInputsModel)
      {		  
        typename MACROSCALE_MODEL::HandlerType* ptr;
        ptr = new typename MACROSCALE_MODEL::HandlerType(externalSoftwareInputsModel,this);
        handler_.reset(ptr);
        pointerToHandler_ = handler_.get();
        SetHandler(handler_);
      };
      boost::shared_ptr<typename MACROSCALE_MODEL::HandlerType> GetHandler() const {return handler_;};
    protected:
      boost::shared_ptr<typename MACROSCALE_MODEL::HandlerType> handler_;
    };

//     template<typename MACROSCALE_MODEL, typename IV_MODEL>
//     class CaseMacroNoIVImplementation
//       : public MACROSCALE_MODEL
//       , public IV_MODEL
//     {
//     public:
//       using IV_MODEL::pointerToHandler_;
//       CaseMacroImplementation(boost::shared_ptr<typename MACROSCALE_MODEL::HandlerType::ExternalSoftwareInputs> externalSoftwareInputsModel)// : MACROSCALE_MODEL(externalSoftwareInputsModel)
//       {
//         typename MACROSCALE_MODEL::HandlerType* ptr;
//         ptr = new typename MACROSCALE_MODEL::HandlerType(externalSoftwareInputsModel, this);
//         handler_.reset(ptr);
//         pointerToHandler_ = handler_.get();
//         SetHandler(handler_);
//       };
//       boost::shared_ptr<typename MACROSCALE_MODEL::HandlerType> GetHandler() const { return handler_; };
//     protected:
//       boost::shared_ptr<typename MACROSCALE_MODEL::HandlerType> handler_;
//     };

  } //model
} //am3


#endif // models_h__
