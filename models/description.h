
/*! \file description.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief Structures describing models - its inputs, outputs and (not implemented) phenomena      
 * \details   	
*********************************************************************/
#ifndef description_h__
#define description_h__
/*	include *********************************************************/

#include <string>
#include <limits>
#include <vector>
#include <assert.h>
#include <cmath>
#include <type_traits>
#include <boost/foreach.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/range/combine.hpp>
#include <boost/lexical_cast.hpp>
/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
//////////////////////////////////////////////////////////////////////////
/**
 * \defgroup Properties Properties description
 *
 * \brief Classes and structures for model properties description 
 *
 * @{
 */
    //! utility namespace for models descriptions
    namespace util
    {
 //     namespace mpl=boost::mpl;

			inline bool IsWrong(const double & val) { return (isnan(val)) ? true : false; }

      class Null{};

      template<typename T>
      class Null1{};

      template<typename TYPE>
      inline void Zero(TYPE & value);

      template<>
      inline void Zero(double & value){value=0.0;};

      template<>
      inline void Zero(int & value){value=0;};

	  template<>
	  inline void Zero(long long int & value){ value = 0; };

			template<>
			inline void Zero(unsigned int & value){value=0;};

      template<>
      inline void Zero(std::string & value){value="";};

      template<>
      inline void Zero(std::vector<double> & value){value.clear();};

      template<typename TYPE>
      inline void Wrong(TYPE & value)
      {
        value = std::numeric_limits<TYPE>::quiet_NaN();
      };


    } //util
//////////////////////////////////////////////////////////////////////////
    /*! \brief Used to describe any entities.
    
    There is no default initialization of \ref value. Double and int properties are initialized with 0 by template specializations.
    Other properties are initialized with 0, if there is suitable conversion. In other cases compilation will fail
    */

    /** \defgroup ComplexPropertyPolices Polices for am3::model::ComplexProperty traiting*/
    /* @{ */
    struct HaveGetValue{};
    struct HaveGetSetValue{};
    /*@}*/
//////////////////////////////////////////////////////////////////////////

    template<typename T>
    inline T Sum(T& addTo,const T& add)
    {
      addTo+=add;
      return addTo;
    }

    template<>
    inline std::vector<double> Sum(std::vector<double>& addTo,const std::vector<double>& add)
    {
      typedef boost::tuple<double&,const double&> tup;
      BOOST_FOREACH (tup d, boost::combine(addTo, add)) 
      {
        d.get<0>()= d.get<0>()+d.get<1>();
      }
      return add;
    }

    template<typename T>
    inline T Div(T& addTo,const double& add)
    {
      addTo/=add;
      return addTo;
    }

    template<>
    inline std::vector<double> Div(std::vector<double>& divide,const double& divideBy)
    {
      BOOST_FOREACH (double d, divide) 
      {
        d/= divideBy;
      }
      return divide;
    }

		template <typename PROPERTY>
		class OldNewPair
		{
		public:
			PROPERTY& GetNew() {return new_;};
			void SetNew(const PROPERTY& newValue) {new_ = newValue;};
			PROPERTY& GetOld() {return old_;};
			void SetOld(const PROPERTY& oldValue){old_ = oldValue;};
		protected:
			PROPERTY old_,new_;
		};

    /** \defgroup SimpleProperties SimpleProperties with specializations
    *
    * Properties describes physical or numerical values. they can be simple (one value) or complex (dimensional, with derivatives etc.)
    */
    /* @{ */
    //! Basic Property with single value and setter
    template<typename TYPE,typename HAVE_VALUE=HaveGetValue>
    struct SimpleProperty
    {
    protected:
//       template<typename T>
//       std::vector<T>& operator+=(const std::vector<T>& toAdd)
//       {
//         BOOST_FOREACH(T i,va)
//         value_+=toAdd.Value();
//         return *this;
//       };
    public:
      void SetWrong(){ util::Wrong(value_); };
      //bool IsWrong() const { return (isnan(value_)) ? true : false; }
			bool IsWrong() const { return am3::model::util::IsWrong(value_); }

      typedef TYPE Type;
      typedef SimpleProperty<TYPE,HAVE_VALUE> DerivedFrom;
      const Type& operator()() const 
			{
				return value_;
			};
      void operator()(SimpleProperty<TYPE,HAVE_VALUE> that){value_=that.Value();};

      void operator()(const TYPE that){value_=that;};
      void Value(const TYPE& that) {value_=that;};

//-      Type& Value() const {return value_;};  //!!!
//      const Type& Value() const { return value_; };
      Type Value() const { return value_; };

      SimpleProperty(){util::Wrong(value_);};      
			
      SimpleProperty& operator=(const SimpleProperty& prop)
      {
        this->value_=prop();
        return *this;
      } 
///\todo Will it works with double?
      const bool operator==(const SimpleProperty& prop) const
      {
        bool res=false;
        if (this->value_==prop.Value()) res=true;
        return res;
      }
      SimpleProperty& operator+=(const SimpleProperty& toAdd)
      {
        value_+=toAdd.Value();
//        (value_,toAdd.Value());
        return *this;
      };


	  const int operator<(const SimpleProperty& prop) const
	  {
		int res=0;
		if (this->value_<prop.Value()) res=1;
		return res;
	  }
	  const int operator>(const SimpleProperty& prop) const
	  {
		int res=0;
		if (this->value_>prop.Value()) res=1;
		return res;
	  }

    const int operator>=(const SimpleProperty& prop) const
    {
      int res=0;
      if (this->value_>=prop.Value()) res=1;
      return res;
    }
    const int operator<=(const SimpleProperty& prop) const
    {
      int res=0;
      if (this->value_<=prop.Value()) res=1;
      return res;
    }

      SimpleProperty& operator/=(const double& divideWith)
      {
        Div(value_,divideWith);
        //value_/=divideWith;
        return *this;
      };
      std::vector<std::string> ToStringsVector()
      {
        std::vector<std::string> toRet;
        toRet.push_back(boost::lexical_cast<std::string>(value_));
        return toRet;
      }
		  


    protected:
      TYPE value_;
    };

    template<typename FROM>
    double castToDouble(FROM from){return from.Value();}

    template<typename TO>
    TO castToType(const double & from){TO to; to(from); return to;}

	template<>
	struct SimpleProperty<std::vector<double>, HaveGetSetValue>
	{
	protected:
	public:
		typedef std::vector<double> Type;
    typedef SimpleProperty<std::vector<double>,HaveGetSetValue> DerivedFrom;
		const Type& operator()() const {return value_;};
		void operator()(SimpleProperty<std::vector<double>,HaveGetSetValue> that){value_=that.Value();};
    void operator()(const std::vector<double> that){value_=that;};
		const Type& Value() const{return value_;}; 
		Type& Value(const std::vector<double>&  value) 
		{
			value_=value;
			return value_;
		}; 
		SimpleProperty(){util::Zero(value_);};      
		SimpleProperty& operator=(const SimpleProperty& prop)
		{
			this->value_=prop();
			return *this;
		} 
		///\todo Will it works with double?
		const int operator==(const SimpleProperty& prop) const
		{
			int res=0;
			if (this->value_==prop.Value()) res=1;
			return res;
		}



		const int operator<(const SimpleProperty& prop) const
		{
			int res=0;
			if (this->value_<prop.Value()) res=1;
			return res;
		}
		const int operator>(const SimpleProperty& prop) const
		{
			int res=0;
			if (this->value_>prop.Value()) res=1;
			return res;
		}

		SimpleProperty& operator/=(const double& divideWith)
		{
			Div(value_,divideWith);
			//value_/=divideWith;
			return *this;
		};




	protected:
		std::vector<double> value_;
	};


    template<typename TYPE,typename PROPERTY>
    PROPERTY ToSimpleProperty(const TYPE& that){PROPERTY toReturn; toReturn(that); return toReturn;};

    //! Specialization of am3::model::SimpleProperty with getter
    template<typename TYPE>
    struct SimpleProperty<TYPE,HaveGetSetValue> :public SimpleProperty<TYPE,HaveGetValue>
    {
      using SimpleProperty<TYPE,HaveGetValue>::operator();
      //using SimpleProperty<TYPE,HaveGetValue>::operator=;
      SimpleProperty(){ util::Wrong(value_); };
      SimpleProperty& operator=(const SimpleProperty& that)
      {
        this->value_=that();
        return *this;
      }
      SimpleProperty& operator=(const TYPE& that)
      {
        this->value_ = that;
        return *this;
      }
      bool operator<=(const SimpleProperty<TYPE, HaveGetSetValue>& that) const
      {
        return (this->value_ <= that()) ? true : false;
      }

			bool operator<(const SimpleProperty<TYPE, HaveGetSetValue>& that) const
			{
				return (this->value_ < that()) ? true : false;
			}
			
	  SimpleProperty(const TYPE& v){SimpleProperty<TYPE,HaveGetValue>::value_=v;};      
//	  SimpleProperty():SimpleProperty<TYPE,HaveGetValue>(){};
      void operator()(TYPE value){SimpleProperty<TYPE,HaveGetValue>::value_=value;};
      const TYPE& Value() const {return SimpleProperty<TYPE,HaveGetValue>::value_;}; 
      
    };
    /*@}*/

    template<typename PROPERTY>
    PROPERTY MeanOf(const std::vector<const PROPERTY*> &nodes)
    {
      PROPERTY toReturn;
      BOOST_FOREACH( const PROPERTY* i, nodes)
      {
        toReturn+=*i;
      }
      toReturn/=(double)nodes.size();
      return toReturn;
    }

//////////////////////////////////////////////////////////////////////////

    /** \defgroup ComplexProperties ComplexProperties with specializations*/
    /* @{ */
    //! Basic complex property; only container, no own value
    template<typename TYPE, typename ADDITIONAL_COMPONENTS,typename HAVE_VALUE=util::Null>
    struct ComplexProperty: public ADDITIONAL_COMPONENTS
    {
        typedef TYPE Type;
        typedef ComplexProperty<TYPE,ADDITIONAL_COMPONENTS, HAVE_VALUE> DerivedFrom;
     };

    //! Complex property combining container and own value with getter (no setter)
    template<typename TYPE, typename ADDITIONAL_COMPONENTS>
    struct ComplexProperty<TYPE, ADDITIONAL_COMPONENTS,HaveGetValue>:
      public SimpleProperty<TYPE>, ADDITIONAL_COMPONENTS
    {
      typedef TYPE Type;
      typedef ComplexProperty<TYPE,ADDITIONAL_COMPONENTS, HaveGetValue> DerivedFrom;
    };

    //! Complex property combining container and own value with getter and setter
    template<typename TYPE, typename ADDITIONAL_COMPONENTS>
    struct ComplexProperty<TYPE, ADDITIONAL_COMPONENTS,HaveGetSetValue>:public 
      SimpleProperty<TYPE,HaveGetSetValue>, ADDITIONAL_COMPONENTS
    {
      typedef TYPE Type;
      using SimpleProperty<TYPE,HaveGetSetValue>::operator();
      typedef ComplexProperty<TYPE,ADDITIONAL_COMPONENTS, HaveGetSetValue> DerivedFrom;
    };

    /*@}*/

    /** \defgroup ADDITIONAL_COMPONENTS Elements of am3::model::ComplexProperty definitions
    *
    * They are used to decorate property with x,y,z components, derivatives or other. New decorations can be defined and used togather.
    */
    /* @{ */
    template<typename INTERNAL, typename ADDITIONAL_COMPONENTS=util::Null>
    struct Dimensions3D
    {
			static const int variables = 3;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> x;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> y;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> z;
      const int operator==(const Dimensions3D& d) const
      {
        int res=0;
        if (abs(this->x()-d.x())<std::numeric_limits<double>::epsilon()) res=1;
        return res;
      }
    };

    template<typename INTERNAL, typename ADDITIONAL_COMPONENTS=util::Null>
    struct Dimensions2D
    {
			static const int variables = 2;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> x;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> y;
      const int operator==(const Dimensions2D& d) const
      {
        int res=0;
        if (abs(this->x()-d.x())<std::numeric_limits<double>::epsilon()) res=1;
        return res;
      }
    };


      template<typename INTERNAL, typename OPERATOR_RETURNS,typename ADDITIONAL_COMPONENTS=util::Null>
      struct Dimensions2DWithOperators
      {
        static const int variables = 2;
        ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> x;
        ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> y;
        const int operator==(const OPERATOR_RETURNS& d) const
        {
          int res=0;
          if (abs(this->x()-d.x())<std::numeric_limits<double>::epsilon()) res=1;
          return res;
        }

      Dimensions2DWithOperators& operator*=(const double& that )
      {
        this->x(this->x() * that);
        this->y(this->y() * that);
        return *this;
      }

      Dimensions2DWithOperators& operator+=(const OPERATOR_RETURNS& that )
      {
        this->x(this->x()+that.x());
        this->y(this->y()+that.y());
        return *this;
      }

      Dimensions2DWithOperators& operator*(const double& that )
      {
        OPERATOR_RETURNS toRet;
        toRet.x(this->x() * that);
        toRet.y(this->y() * that);
        return toRet;
      }

      Dimensions2DWithOperators& operator+(const OPERATOR_RETURNS& that )
      {
        OPERATOR_RETURNS toRet;
        toRet.x(that.x()+this->x());
        toRet.y(that.y()+this->y());
        return toRet;
      }

      OPERATOR_RETURNS& operator()(const OPERATOR_RETURNS& that )
      {
        OPERATOR_RETURNS toRet;
        toRet.x(that.x);
        toRet.y(that.y);
        return toRet;
      }

    };
    template<typename INTERNAL, typename ADDITIONAL_COMPONENTS=util::Null>
    struct SpatialDerivative3D
    {
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> dx;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> dy;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> dz;
    };

    template<typename INTERNAL, typename ADDITIONAL_COMPONENTS=util::Null>
    struct Derivative
    {
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS> d;
    };

    template<typename INTERNAL, typename ADDITIONAL_COMPONENTS=util::Null>
    struct TemperaturePressureDerivative
    {
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> dp;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> dt;
    };

		template<typename INTERNAL, typename ADDITIONAL_COMPONENTS=util::Null>
		struct Pair
		{
			ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> one;
			ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> two;
		};

    template<typename INTERNAL, typename ADDITIONAL_COMPONENTS=util::Null>
    struct Tensor2D
    {
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> g11;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> g22;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> g12;
      ComplexProperty<INTERNAL,ADDITIONAL_COMPONENTS, HaveGetSetValue> g21;
      INTERNAL operator()()
            {
                return (g11()+g12()+g21()+g22())/4;
            }
    };
    /*@}*/


//////////////////////////////////////////////////////////////////////////

    /**@}*/

    //******************************************************************************************************************************************************************
    template<typename VAR> 
    VAR WrongVariable()
    {
      VAR toRet;
      toRet(std::numeric_limits<double>::signaling_NaN());
      return toRet;
    }

  } //model
} //am3

#endif // description_h__
