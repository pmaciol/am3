#include "dataStructures.h"

namespace am3
{
  //! Namespace for model
  namespace model
  {
    const Viscosity kViscosityTrait;
    const Cp kCpTrait;
    const Cv kCvTrait;
    const ThermalConductivity kThermalConductivityTrait;
    const CoeffVolumeExpansion kCoeffVolumeExpansionTrait;
    const Density kDensityTrait;
		const DensityWithDerivatives kDensityWithDerivativesTrait;
    const ReferenceTemperature kReferenceTemperatureTrait;
    const Gravitation kGravitationTrait;
    const HeatPerVolume kHeatPerVolumeTrait;
    const SurfaceTension kSurfaceTensionTrait;
    const BulkModulus kBulkModulusTrait;
    const RatiosSet kRatiosSetTrait;

    const YieldStress kYieldStressTrait;
    const StressDStrain kStressDStrainTrait;
    const StressDStrainRate kStressDStrainRateTrait;
  }
}