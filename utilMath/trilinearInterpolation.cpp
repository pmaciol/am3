#include "trilinearInterpolation.h"
#include "../logger/logger.h"
#include <iterator>

namespace lo=am3::logger;

void am3::math::interpolation::TrilinearRegularInterpolation::SetSize( const unsigned int x,const unsigned int y,const unsigned int z )
{
  size[0]=x;
  size[1]=y;
  size[2]=z;
  nodes.resize(dimensions);
}

void am3::math::interpolation::TrilinearRegularInterpolation::SetGridPoints(std::vector<double>& values1,std::vector<double>& values2,std::vector<double>& values3)
{
  //nodes[0].resize(size[which]);
  nodes[0]=values1;
  nodes[1]=values2;
  nodes[2]=values3;
}

am3::math::interpolation::ValueWithDerivatives<3>  am3::math::interpolation::TrilinearRegularInterpolation::GetValue( const Point<3>& point )
{ 
  if (!FindBoundaryNodes(point)) lo::log<lo::SEV_WARNING>(lo::LOG_MODEL,"Material state out of the bound");
  Point<3> localPoint=FindCoordinatesIn01Cube(point);

  am3::math::interpolation::ValueWithDerivatives<3> interpolatedValue = ComputeInterpolationIn01Cube(localPoint);

  return interpolatedValue;
}

void am3::math::interpolation::TrilinearRegularInterpolation::SetValues( const std::vector<std::vector<std::vector<double> > >& array )
{
  array_=array;
}

bool am3::math::interpolation::TrilinearRegularInterpolation::FindBoundaryNodes( const Point<3>& point )
{
  bool isInRange=true;
  chosenPoints.resize(3);
//  valuesInNodes_.clear();
  std::vector<double>::iterator it;
  std::vector<double> coord=point.Coordinates();

  for (int i=0;i<3;++i)
  {
    it=nodes[i].begin();
    while (((*it)<coord[i])&&(it<(nodes[i].end()-1)))
    {
      it++;
    }
    if (!(it<nodes[i].end())) isInRange=false;
    chosenPoints[i]=it-1;
  }
  return isInRange;
}

am3::math::interpolation::ValueWithDerivatives<3> am3::math::interpolation::TrilinearRegularInterpolation::ComputeInterpolationIn01Cube( const Point<3>& point )
{
  double delta;
  std::vector<double> valuesInNodes_;
  size_t distx,disty,distz;
  distx = std::distance( nodes[0].begin(), chosenPoints[0] );
  disty = std::distance( nodes[1].begin(), chosenPoints[1] );
  distz = std::distance( nodes[2].begin(), chosenPoints[2] );
  valuesInNodes_.push_back(array_[distx][disty][distz]);
  valuesInNodes_.push_back(array_[distx+1][disty][distz]);
  valuesInNodes_.push_back(array_[distx+1][disty+1][distz]);
  valuesInNodes_.push_back(array_[distx][disty+1][distz]);
  valuesInNodes_.push_back(array_[distx][disty][distz+1]);
  valuesInNodes_.push_back(array_[distx+1][disty][distz+1]);
  valuesInNodes_.push_back(array_[distx+1][disty+1][distz+1]);
  valuesInNodes_.push_back(array_[distx][disty+1][distz+1]);

  am3::math::interpolation::ValueWithDerivatives<3> interpolatedValue;
  double length;
   	interpolatedValue.value=
      Interpolate(valuesInNodes_, point);
   
    delta=1-point.x;
    Point<3> pointDx(point.x+delta,point.y,point.z); 
    interpolatedValue.x_derivative=Interpolate(valuesInNodes_, pointDx);
    interpolatedValue.x_derivative-= interpolatedValue.value;
    interpolatedValue.x_derivative/= delta;
    length=(*(chosenPoints[0]+1))-(*chosenPoints[0]);
    interpolatedValue.x_derivative/=length;

    delta=1-point.y;
    Point<3> pointDy(point.x,point.y+delta,point.z); 
    interpolatedValue.y_derivative=Interpolate(valuesInNodes_, pointDy);
    interpolatedValue.y_derivative-= interpolatedValue.value;
    interpolatedValue.y_derivative/= delta;
    length=(*(chosenPoints[1]+1))-(*chosenPoints[1]);
    interpolatedValue.y_derivative/=length;

    delta=1-point.z;
    Point<3> pointDz(point.x,point.y,point.z+delta); 
    interpolatedValue.z_derivative=Interpolate(valuesInNodes_, pointDz);
    interpolatedValue.z_derivative-= interpolatedValue.value;
    interpolatedValue.z_derivative/= delta;
    length=(*(chosenPoints[2]+1))-(*chosenPoints[2]);
    interpolatedValue.z_derivative/=length;


  return interpolatedValue;
}

am3::math::interpolation::Point<3> am3::math::interpolation::TrilinearRegularInterpolation::FindCoordinatesIn01Cube( const Point<3>& point )
{
  double x,y,z;
  double length;
  length=(*(chosenPoints[0]+1))-(*chosenPoints[0]);
  x=(point.x-(*chosenPoints[0]))/length;
  length=(*(chosenPoints[1]+1))-(*chosenPoints[1]);
  y=(point.y-(*chosenPoints[1]))/length;
  length=(*(chosenPoints[2]+1))-(*chosenPoints[2]);
  z=(point.z-(*chosenPoints[2]))/length;

  return am3::math::interpolation::Point<3>(x,y,z);
}

double am3::math::interpolation::TrilinearRegularInterpolation::Interpolate( std::vector<double> valuesInNodes_, const Point<3> &point )
{
  return valuesInNodes_[0] * (1 - point.x) * (1 - point.y) * (1 - point.z)+
    valuesInNodes_[1] * point.x * (1 - point.y) * (1 - point.z)+
    valuesInNodes_[2] * point.x * point.y * (1 - point.z)+
    valuesInNodes_[3] * (1 - point.x) * point.y * (1 - point.z)+
    valuesInNodes_[4] * (1 - point.x) * (1 - point.y) * point.z+
    valuesInNodes_[5] * point.x * (1 - point.y) * point.z+
    valuesInNodes_[6] * point.x * point.y * point.z+
  valuesInNodes_[7] * (1 - point.x) * point.y * point.z;
}
