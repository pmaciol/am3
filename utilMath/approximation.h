
/*! \file approximation.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3utilMath_approximation_h__
#define am3utilMath_approximation_h__
/*	include *********************************************************/



/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace math
	{
		namespace approximation
		{
			template<typename DEPENDENT_VALUES, typename INDEPENDENT_VALUES>
			class Approximation
			{
			public:
				virtual void AddNode(const INDEPENDENT_VALUES& independent, const DEPENDENT_VALUES& dependent) = 0;
				virtual DEPENDENT_VALUES GetApproximation(const INDEPENDENT_VALUES& independet) = 0;
				virtual void ClearAll() = 0;
			};
		} //approximation
	} //math
} //am3
#endif // am3utilMath_approximation_h__
