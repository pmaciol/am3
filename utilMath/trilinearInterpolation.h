
/*! \file interpolation.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef trilinearInterpolation_h__
#define trilinearInterpolation_h__
/*	include *********************************************************/

#include "boost/multi_array.hpp"
#include "boost/array.hpp"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace math
  {
    namespace interpolation
    {
      template <unsigned int dimensions>
      class Point;

      template <>
      class Point<3>
      {
      public: 
        Point(const double xval,const double yval,const double zval):x(xval),y(yval),z(zval){};
        const std::vector<double> Coordinates() const
        {
          std::vector<double> coord;
          coord.push_back(x);
          coord.push_back(y);
          coord.push_back(z);
          return coord;
        }
      
        double x,y,z;
      };

      template < unsigned int dimensions>
      struct ValueWithDerivatives;

      template <>
      struct ValueWithDerivatives<3>
      {
        double value,x_derivative,y_derivative,z_derivative;
      };


      //////////////////////////////////////////////////////////////////////////

      class TrilinearRegularInterpolation
      {
      public:
        static const unsigned int one=0, two=1, three=2;
        static const unsigned int dimensions=3;

        void SetSize(const unsigned int x,const unsigned int y,const unsigned int z);
        void SetGridPoints(std::vector<double>& values1,std::vector<double>& values2,std::vector<double>& values3);
        ValueWithDerivatives<3> GetValue(const Point<3>& point);
        void SetValues(const std::vector<std::vector<std::vector<double> > >& array);
        
      protected:
        unsigned int size[3];
        std::vector<std::vector<double> > nodes;
        std::vector<std::vector<std::vector<double> > > array_;
        
        std::vector<std::vector<double>::iterator > chosenPoints;
        

        bool FindBoundaryNodes( const Point<3>& point );
        Point<3> FindCoordinatesIn01Cube( const Point<3>& point );
        ValueWithDerivatives<3> ComputeInterpolationIn01Cube( const Point<3>& point );

        double Interpolate( std::vector<double> valuesInNodes_, const Point<3> &point );

      };
    } //interpolation
  } //math
} //am3
#endif // trilinearInterpolation_h__
