
/*! \file geometryOperations.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef geometryOperations_h__
#define geometryOperations_h__
/*	include *********************************************************/

#include <vector>
#include "../models/dataStructures.h"
#include "../models/point.h"

/*	using ***********************************************************/

namespace pnt=am3::model::point;
namespace pro=am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace math
  {
    namespace geometry
    {
      bool IsPointInPolygon( const pnt::Point<pro::Coordinates2D> & point, const std::vector<pro::Coordinates2D> & vertexes);
    } //geometry
  } //math
} //am3

#endif // geometryOperations_h__
