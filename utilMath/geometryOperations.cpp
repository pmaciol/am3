#include "geometryOperations.h"

bool am3::math::geometry::IsPointInPolygon( const pnt::Point<pro::Coordinates2D> & point, const std::vector<pro::Coordinates2D> & vertexes )
{
  std::vector<pro::Coordinates2D>::const_iterator it;
  double minX=0,maxX=0,minY=0,maxY=0;

  size_t nvert=vertexes.size();
  for (it=vertexes.begin();it<vertexes.end();it++)
  {
    if (minX>it->x()) minX=it->x();
    if (minY>it->y()) minY=it->y();
    if (maxX<it->x()) maxX=it->x();
    if (maxY<it->x()) maxY=it->y();
  }
  //algorithm from http://www.codeproject.com/Tips/84226/Is-a-Point-inside-a-Polygon
  bool c=false;
  if (point.Coordinates().x() > minX 
    || point.Coordinates().x() < maxX 
    || point.Coordinates().y() > minY 
    || point.Coordinates().y() < maxY) 
  {
    unsigned int i, j;
    for (i = 0, j = nvert-1; i < nvert; j = i++) 
    {
      if ((vertexes[i].y()>point.Coordinates().y()) != (vertexes[j].y()>point.Coordinates().y()))

      {
        if (((point.Coordinates().x() < (vertexes[j].x()-vertexes[i].x()) * (point.Coordinates().y()-vertexes[i].y()) / (vertexes[j].y()-vertexes[i].y()) + vertexes[i].x()) ))
        {
          c = !c;
        }
      }

    }
  }
  //if (!c) lo::log<lo::SEV_WARNING>(lo::LOG_MODEL,"Point is not in an element");
  return c;
}
