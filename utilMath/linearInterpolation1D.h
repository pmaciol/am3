
/*! \file linearInterpolation1D.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3utilMath_linearInterpolation1D_h__
#define am3utilMath_linearInterpolation1D_h__
/*	include *********************************************************/

#include <map>
#include "approximation.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/



namespace am3
{
	namespace math
	{
		namespace approximation
		{
			template<typename DEPENDENT_VALUE, typename INDEPENDENT_VALUE>
			class LinearInterpolation1D
				: public Approximation<DEPENDENT_VALUE, INDEPENDENT_VALUE>
			{
			public:
				typedef INDEPENDENT_VALUE IndependentValue;
				typedef DEPENDENT_VALUE DependentValue;
				virtual void AddNode(const IndependentValue& independent, const DependentValue& dependent);

				virtual DEPENDENT_VALUE GetApproximation(const IndependentValue& independet);
				virtual void ClearAll();

			protected:
				std::map<IndependentValue, DependentValue> values_;

				DependentValue LinearApproximation(const IndependentValue& independent, std::pair<const IndependentValue, DependentValue> lower, std::pair<const IndependentValue, DependentValue> upper);
				

			};
 		////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////


			template<typename DEPENDENT_VALUE, typename INDEPENDENT_VALUE>
			void am3::math::approximation::LinearInterpolation1D<DEPENDENT_VALUE, INDEPENDENT_VALUE>::ClearAll()
			{
				values_.clear();
			}


			template<typename DEPENDENT_VALUE, typename INDEPENDENT_VALUE>
			void am3::math::approximation::LinearInterpolation1D<DEPENDENT_VALUE, INDEPENDENT_VALUE>::AddNode(const typename INDEPENDENT_VALUE& independent, const typename DEPENDENT_VALUE& dependent)
			{
				values_.emplace(independent, dependent);
			}

			template<typename DEPENDENT_VALUE, typename INDEPENDENT_VALUE>
			struct IsClose
			{
				INDEPENDENT_VALUE val2_;
				void SetVal2(const INDEPENDENT_VALUE val2)
				{
					val2_ = val2;
				}
				bool operator()(std::pair<INDEPENDENT_VALUE, DEPENDENT_VALUE> val1)
				{
					double epsilon = abs(val1.second() / 1e3);
					return (abs(val1.second() - val2_()) < epsilon) ? true : false;
				}
			};
// 			bool IsClose(const double val1, const double val2)
// 			{
// 				double epsilon = abs(val1 / 1e3);
// 				(abs(val1 - val2) < epsilon) ? true : false;
// 			}

			template<typename DEPENDENT_VALUE, typename INDEPENDENT_VALUE>
			typename DEPENDENT_VALUE am3::math::approximation::LinearInterpolation1D<DEPENDENT_VALUE, INDEPENDENT_VALUE>::GetApproximation(const INDEPENDENT_VALUE& independent)
			{
				DependentValue toRet;
//				std::map<IndependentValue, DependentValue>::iterator found = values_.find(independent);
				IsClose<DEPENDENT_VALUE, INDEPENDENT_VALUE> isClose;
				isClose.SetVal2(independent);
				std::map<IndependentValue, DependentValue>::iterator found = find_if(values_.begin(), values_.end(),isClose);
				if (values_.size()>0)
				{
					if (independent < values_.begin()->first)
					{
						toRet = LinearApproximation(independent, *values_.begin(), *values_.begin());
					}

					else if (independent > values_.rbegin()->first)
					{
						toRet = LinearApproximation(independent, *values_.rbegin(), *values_.rbegin());
					}

					else if (found != values_.end())
					{
						toRet = found->second;
					}
					else
					{
						std::map<IndependentValue, DependentValue>::iterator lower = values_.lower_bound(independent);
						if (lower != values_.begin()) lower--;

						std::map<IndependentValue, DependentValue>::iterator upper = values_.upper_bound(independent);
						if (upper == values_.end()) upper--;

						toRet = LinearApproximation(independent, *lower, *upper);
					}
				}

				else
				{
					std::cout << "Something wrong, empty precomputations set"<< std::endl;
				}

				
				return toRet;
			}


			template<typename DEPENDENT_VALUE, typename INDEPENDENT_VALUE>
			DEPENDENT_VALUE am3::math::approximation::LinearInterpolation1D<DEPENDENT_VALUE, INDEPENDENT_VALUE>::LinearApproximation(const IndependentValue& independent, std::pair<const IndependentValue, DependentValue> lower, std::pair<const IndependentValue, DependentValue> upper)
			{
				double distance = upper.first() - lower.first();
				double distanceToRequested = independent.Value() - lower.first();
				double y0 = lower.second.Value();
				double y1 = upper.second.Value();
				double x0 = lower.first.Value();
				double x1 = upper.first.Value();
				double xi = independent.Value();

				double interpolated = y0 + ((xi - x0) / (x1 - x0))*(y1 - y0);
				DependentValue toRet;
				toRet(interpolated);
				return toRet;
			}


		} //approximation
	} //math
} //am3
#endif // am3utilMath_linearInterpolation1D_h__
