
/*! \file scriptBasedMatcalc.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef scriptBasedMatcalc_h__
#define scriptBasedMatcalc_h__
/*	include *********************************************************/

#include <string>
#include <vector>

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace matcalc
		{
      class MultiScriptBasedStep
      {
      public:
        MultiScriptBasedStep(std::vector<std::string> & stepsScripts): stepsScripts_(stepScripts){presentStepsScript_ = stepsScripts_.begin();};
        void CalculateStep(mpr::StepLength timeStepLength, mpr::Temperature lastTemperature, mpr::Temperature finalTemperature);
        mpr::PrecipitationsMeanRadius GetPrecipitationsMeanRadius();
      protected:
        double previousTemperature_;
        std::vector<std::string> stepsScripts_;
        std::vector<std::string>::iterator presentStepsScript_;
      };
// 
      class ScriptBasedPrecomputations
      {
      public:
        ScriptBasedPrecomputations(const std::string precomputationsScript): precomputationsScript_(precomputationsScript) {};
        void Precomputations();
      protected:
        std::string precomputationsScript_;
      };
// 
//       struct  TestResultsTransformer
//       {
//         void Set(boost::shared_ptr<TestStep> dataProvider){dataProvider_ = dataProvider;}
//         typedef mpt::ComputedPoint<mpr::Coordinates2D>::Point Point;
//         template<typename VARIABLE> VARIABLE Get(Point * point);
//         template<> mpr::PrecipitationsMeanRadius Get<mpr::PrecipitationsMeanRadius>(Point * point)
//         {
//           return dataProvider_->GetPrecipitationsMeanRadius();
//         }
//       protected:
//         boost::shared_ptr<TestStep> dataProvider_;
//       };
			
		} //matcalc
	} //software
} //am3

#endif // scriptBasedMatcalc_h__
