
/*! \file simpleEquations.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef precipitaionsSimpleEquations_h__
#define precipitaionsSimpleEquations_h__
/*	include *********************************************************/

#include "./contracts.h"
#include "../models/models.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../models/inputTypelists.h"
#include "../databus/databusTypelists.h"

/*	using ***********************************************************/

namespace mo = am3::model;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;

namespace db  = am3::databus;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace precipitations
    {
      class PrecipitationsAmountExponentialCoefficients
      {
      public:
        virtual double Y0() const = 0;
        virtual double K() const = 0;
        virtual double N() const = 0;
      };

      //////////////////////////////////////////////////////////////////////////

	  template<typename INPUT>
	  class PrecipitationsAmountExponential
		  : public contract::PrecipitationsOnlyAmountContract
		  , public mo::InputTypelist<INPUT>

		{
		public:
		PrecipitationsAmountExponential(){};
		PrecipitationsAmountExponential(const double& y0, const double& k, const double& n): y0_(y0),k_(k), n_(n) {};
		PrecipitationsAmountExponential(const PrecipitationsAmountExponentialCoefficients& coefficients): y0_(coefficients.Y0()),k_(coefficients.K()), n_(coefficients.N()) {};
		void Init(const PrecipitationsAmountExponentialCoefficients& coefficients)
		{
		    y0_ = coefficients.Y0();
		    k_ = coefficients.K(); 
		    n_ = coefficients.N();
		};
		mpr::PrecipitationsAmount GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::PrecipitationsAmount * const)
		{
			mpr::PrecipitationsAmount toRet;
			mpr::Temperature temperature = Get<mpr::Temperature, mpr::Coordinates2D>::InPoint(point);
			mpr::Time time = Get<mpr::Time, mpr::Coordinates2D>::InPoint(point);
			toRet(y0_ * (1 - exp(-pow((k_ * time()), n_))));
			return toRet;
		};
			protected:
			double y0_,k_,n_;
		};
//       class PrecipitationsAmountExponential
//         : public contract::PrecipitationsOnlyAmountContract
//         , public mo::Inputs<contract::PrecipitationsBasicInputs>
//       {
//       public:
//         PrecipitationsAmountExponential(){};
//         PrecipitationsAmountExponential(const double& y0, const double& k, const double& n): y0_(y0),k_(k), n_(n) {};
//         PrecipitationsAmountExponential(const PrecipitationsAmountExponentialCoefficients& coefficients): y0_(coefficients.Y0()),k_(coefficients.K()), n_(coefficients.N()) {};
//         void Init(const PrecipitationsAmountExponentialCoefficients& coefficients)
//         {
//           y0_ = coefficients.Y0();
//           k_ = coefficients.K(); 
//           n_ = coefficients.N();
//         };
//         mpr::PrecipitationsAmount GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::PrecipitationsAmount * const ) ;
//       protected:
//         double y0_,k_,n_;
//       };


      // Abe, 10Cr-30Mn, 0.26C
      struct PrecipitationsAmountExponentialCoefficientsAbe10Cr30Mn026C
        : public PrecipitationsAmountExponentialCoefficients
      { 
        virtual double Y0() const {return 1.2;}
        virtual double K() const {return 3e-6;}
        virtual double N() const {return 2.0/3.0;}
	  };

	}
  }
}
#endif // precipitaionsSimpleEquations_h__
