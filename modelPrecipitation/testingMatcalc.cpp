
/*! \file testingMatcalc.cpp **************************************************
* \author		Piotr Maciol
* \copyright 	GNU Public License.
* \brief       
* \details   	
*********************************************************************/
/*	include *********************************************************/

#include "testingMatcalc.h"
#include "../softwareMatCalc/externs.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace precipitations
    {

      void TestStep::CalculateStep( mpr::StepLength timeStepLength, mpr::Temperature lastTemperature, mpr::Temperature finalTemperature )
      {
        int log;
        int lastTreatment = MCC_GetNumberOfHeatTreatments();
        MC_HANDLE treatment = MCC_GetHTHandleByIndex(lastTreatment - 1);

        int lastSegment = MCC_GetNumberOfHTSegments(treatment);
        MC_HANDLE segement = MCC_GetHTSegmentHandleFromIndex(treatment,lastSegment - 1);
        log = MCC_RemoveHTSegment(treatment, segement);
        lastSegment = MCC_GetNumberOfHTSegments(treatment);
        log = MCC_AppendNewHTSegment(treatment);
        lastSegment = MCC_GetNumberOfHTSegments(treatment);
        MC_HANDLE hts_handle = MCC_GetHTSegmentHandleFromIndex(treatment, 0);
        char* expr ="martensite";
        MCC_SetHTSegmentProperty(hts_handle, HTP_PRECIPITATION_DOMAIN, false, 0, (void*)expr);
        double buff = 0;
        MCC_SetHTSegmentProperty(hts_handle, HTP_SEGMENT_TEMP_CODE, HTDT_TEND_TDOT, buff, NULL);
        MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_START, NULL, lastTemperature(), NULL);
        //MCC_SetHTSegmentProperty(hts_handle, HTP_INHERIT_TEMP_START, NULL, NULL, NULL); //???
        MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_END, NULL, finalTemperature(), NULL);
        MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_DOT, NULL,  (lastTemperature() - finalTemperature()) / timeStepLength() , NULL);
        //MCC_SetHTSegmentProperty(hts_handle, HTP_DELTA_TIME, NULL,  timeStepLength() , NULL); //?????

        int kinetic_flag;
        MCC_GetGlobalParameter(MCGV_KINETIC_FLAGS,&kinetic_flag);

        kinetic_flag &= ~(KF_LOAD_FROM_STATE|KF_RESET_PRECIPITATES);
        kinetic_flag |= KF_APPEND_TO_BUFFER;
        MCC_SetGlobalParameter(MCGV_KINETIC_FLAGS, kinetic_flag);

        MCC_CalcKineticStep(false,EO_NORMAL);
      }

      mpr::PrecipitationsMeanRadius TestStep::GetPrecipitationsMeanRadius()
      {
        mpr::PrecipitationsMeanRadius toRet;
        MC_HANDLE ph_handle = MCC_GetNextPhaseHandle(NULL);

        double val;
        std::vector<double> meanRadiusValues, amounts;
        std::string varName1;
        double sumOfPRecipitations = 0;

        while (ph_handle)
        {
          char* PhaseName = MCC_GetPhaseName(ph_handle);
          int flags = MCC_GetPhaseFlags(ph_handle);
          if (flags & FLAG_PHASEISKINETIC)

          {
            varName1 = "R_MEAN$";
            varName1 += PhaseName;

            char *cstr = new char[varName1.length() + 1];
            std::strcpy(cstr, varName1.c_str());


            MCC_GetCalcVariable(cstr, &val, MC_FALSE, MC_TRUE);
            delete [] cstr; 

            if (val > 0)
            {
              meanRadiusValues.push_back(val);
              varName1 = "NUM_PREC$";
              varName1 += PhaseName;
              char *cstr = new char[varName1.length() + 1];
              std::strcpy(cstr, varName1.c_str());
              MCC_GetCalcVariable(cstr, &val, MC_FALSE, MC_TRUE);
              delete [] cstr; 
              amounts.push_back(val);
              sumOfPRecipitations += val;
            }

          }
          ph_handle = MCC_GetNextPhaseHandle(ph_handle);
        }
        double globalMeanRadius = 0;
        std::vector<double>::iterator itMr;
        std::vector<double>::iterator itAm;

        if (sumOfPRecipitations > 0)
        {
          for (itMr  = meanRadiusValues.begin(),itAm = amounts.begin() ;itMr< meanRadiusValues.end();itMr++)
          {
            globalMeanRadius+=(*itAm)*(*itMr)/sumOfPRecipitations;
          }
        } 
        else
        {
          globalMeanRadius = 0;
        }
        toRet(globalMeanRadius);
        return toRet;
      }


      void TestPrecomputations::Precomputations( mpr::Temperature initialTemperature )
      {
        ////DebugBreak();
        MC_HANDLE treatment = MCC_CreateNewHeatTreatment("AM3_HEAT_TREATMENT");
        MCC_AppendNewHTSegment(treatment);
        MC_HANDLE hts_handle = MCC_GetHTSegmentHandleFromIndex(treatment, 0);
        char* expr ="austenite";
        MCC_SetHTSegmentProperty(hts_handle, HTP_PRECIPITATION_DOMAIN, false, 0, (void*)expr);
        double buff = 0;
        MCC_SetHTSegmentProperty(hts_handle, HTP_SEGMENT_TEMP_CODE, HTDT_TEND_TDOT, buff, NULL);
        //MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_START, NULL, previousTemperature(), NULL);
        double tStart = initialTemperature() + 10;
        double tEnd = initialTemperature();
        MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_START, NULL, tStart, NULL);
        //  MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_END, NULL, temperature(), NULL);
        MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_END, NULL, tEnd, NULL);
        //MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_DOT, NULL,  (previousTemperature() - temperature()) / time() , NULL);
        MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_DOT, NULL,  10.0 / 10.0 , NULL);


        int kinetic_flag;
        MCC_GetGlobalParameter(MCGV_KINETIC_FLAGS,&kinetic_flag);

        //kinetic_flag &= ~(KF_LOAD_FROM_STATE|KF_RESET_PRECIPITATES);
        kinetic_flag &= ~(KF_RESET_PRECIPITATES);
        kinetic_flag |= KF_APPEND_TO_BUFFER;
        kinetic_flag |= KF_RESET_PRECIPITATES;
        MCC_SetGlobalParameter(MCGV_KINETIC_FLAGS, kinetic_flag);

        MC_HANDLE haln_p0 = MCC_GetPhaseHandleByName("aln_p0");

        MC_HANDLE buffer = MCC_CreateNewBuffer("solution");
        MC_HANDLE state = MCC_CreateNewState("after_solution");
        buffer = MCC_CreateNewBuffer("next");
        int check = MCC_SelectBuffer(buffer);


        MCC_SetGlobalParameter(MCGV_KIN_T_CONTROL_TYPE, STT_FROM_HEAT_TREATMENT);
        MCC_SetGlobalParameter(MCGV_KIN_T_HEAT_TREATMENT_INDEX, 0);
        MCC_SetGlobalDoubleParameter(MCGV_KIN_MAX_T_STEP_WIDTH, 10);

        MCC_CalcKineticStep(false,EO_NORMAL);
      }
    }
  }
}