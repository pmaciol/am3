
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef precipitationsContracts_h__
#define precipitationsContracts_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../models/contractMacros.h"
#include "../models/macrosSimplifiedModels.h"

/*	using ***********************************************************/

namespace moc=am3::model::contract;
namespace mpr=am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace precipitations
    {
      namespace contract
      {
        CONTRACT(		PrecipitationsOnlyAmountContract		  		,1,mpr::Coordinates2D, mpr::PrecipitationsAmount);
        CONTRACT(		PrecipitationsBasicContract		  		,2,mpr::Coordinates2D, mpr::PrecipitationsMeanRadius, mpr::PrecipitationsAmount);
        CONTRACT(		PrecipitationsBasicInputs	  	  		,2,mpr::Coordinates2D, mpr::Time, mpr::Temperature);
        CONTRACT(   PrecipitationsMeanRadiusContract    , 1, mpr::Coordinates2D, mpr::PrecipitationsMeanRadius);
				CONTRACT(		PrecipitationsMeanRadiusPhaseFractionContract, 2, mpr::Coordinates2D, mpr::PrecipitationsMeanRadius, mpr::PrecipitationsVolumeFraction);

        typedef moc::Contract<TYPELIST_1( EmptyType ),mpr::Coordinates2D> NoInput; // Should be mpr::null or something


      }
    }
  }
}
#endif // precipitationsContracts_h__
