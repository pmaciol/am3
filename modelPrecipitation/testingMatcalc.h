
/*! \file testingMatcalc.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef testingMatcalc_h__
#define testingMatcalc_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/point.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;


/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace precipitations
		{
      class TestStep
      {
      public:
        void CalculateStep(mpr::StepLength timeStepLength, mpr::Temperature lastTemperature, mpr::Temperature finalTemperature);
        mpr::PrecipitationsMeanRadius GetPrecipitationsMeanRadius();
      protected:
        double previousTemperature_;
      };

      struct TestPrecomputations
      {
        void Precomputations(mpr::Temperature initialTemperature);
      };

      struct  TestResultsTransformer
      {
        void Set(boost::shared_ptr<TestStep> dataProvider){dataProvider_ = dataProvider;}
        typedef mpt::ComputedPoint<mpr::Coordinates2D>::Point Point;
        template<typename VARIABLE> VARIABLE Get(Point * point);
        template<> mpr::PrecipitationsMeanRadius Get<mpr::PrecipitationsMeanRadius>(Point * point)
        {
          return dataProvider_->GetPrecipitationsMeanRadius();
        }
      protected:
        boost::shared_ptr<TestStep> dataProvider_;
      };
		} //precipitations
	} //model
} //am3
#endif // testingMatcalc_h__
