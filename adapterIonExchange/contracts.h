
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef adapterIonExchange_contracts_h__
#define adapterIonExchange_contracts_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../modelIonExchange/problemDependentDataStructures.h"
//#include "dataStructures.h"

/*	using ***********************************************************/

namespace pro=am3::model;
namespace iom=am3::model::ionex;


/*	extern **********************************************************/



/*	classes *********************************************************/
namespace am3
{
  namespace model
  {
    namespace contract
    {
      namespace defined
      {

        struct IonExchangeProvider
          : public Provider<pro::Ratio>
          , public Provider<pro::VelocityWithDerivatives>
          , public Provider<ionex::WaterPhaseRatio>
          , public Provider<ionex::OrganicPhaseRatio>
          , public Provider<ionex::IonRatioInOrganic>
          , public Provider<ionex::IonRatioInWater>
          , public Provider<ionex::BubbleCoefficient>
          , public Provider<pro::TemperatureWithDerivatives>
          , public Provider<pro::TurbulenceEWithDerivatives>
          , public Provider<pro::Coordinates>
          , public Provider<pro::Time>
          , public Provider<pro::ElementGroupNumber>
          , public Provider<pro::ElementNumber>{};


        struct IonExchangeMacro
          : public Provider<pro::VelocityWithDerivatives>
          , public Provider<pro::PressureWithDerivatives>
          , public Provider<pro::TemperatureWithDerivatives> //Try with "Temperature"
          , public Provider<pro::TurbulenceKWithDerivatives>
          , public Provider<pro::TurbulenceEWithDerivatives>
          , public Provider<pro::Coordinates>
          , public Provider<pro::Time>{};

        struct IonExchangeMacroContract
          : public ContractItemDispatcher<pro::VelocityWithDerivatives>
          , public ContractItemDispatcher<pro::PressureWithDerivatives>
          , public ContractItemDispatcher<pro::TemperatureWithDerivatives> //Try with "Temperature"
          , public ContractItemDispatcher<pro::TurbulenceKWithDerivatives>
          , public ContractItemDispatcher<pro::TurbulenceEWithDerivatives>
          , public ContractItemDispatcher<pro::Coordinates>
          , public ContractItemDispatcher<pro::Time>
          {virtual ~IonExchangeMacroContract(){}};

        struct IonExchangeViscosityContract
        : public ContractItemDispatcher<pro::Viscosity>
        {virtual ~IonExchangeViscosityContract(){}};

        struct IonExchangeHeatSourceContract{};

        struct IonExchangeRatioContract
          : public ContractItemDispatcher<iom::WaterPhaseRatio>
          , public ContractItemDispatcher<iom::OrganicPhaseRatio>
        {virtual ~IonExchangeRatioContract(){}};

        struct IonExchangeBubblesContract
          : public ContractItemDispatcher<iom::BubbleCoefficient>
        {virtual ~IonExchangeBubblesContract(){}};
        
        struct IonExchangeZnIIContentContract
          : public ContractItemDispatcher<iom::ZnIIRatioInOrganic>
          , public ContractItemDispatcher<iom::ZnIIRatioInWater>
        {virtual ~IonExchangeZnIIContentContract(){}};



      }//defined

 
    } //contract
  } //model
} //am3
#endif // adapterIonExchange_contracts_h__