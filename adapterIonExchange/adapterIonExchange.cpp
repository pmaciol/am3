
/*! \file adapterIonExchange.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

/*	include *********************************************************/
#include <string>
#include "adapterIonExchange.h"
#include "../modelIonExchange/problemDependentDataStructures.h"
#include "../models/point.h"
#include "../models/contracts.h"
#include "../logger/logger.h"
#include "../common/literals.h"
#include "../models/dataStructures.h"
#include "../modelIonExchange/baseIonExchangeModels.h"

/*	using ***********************************************************/

using am3::logger::TestingMsg;
namespace pro=am3::model;
namespace mod=am3::model;
namespace iom=am3::model::ionex;
using std::string;
/*	extern **********************************************************/



/*	classes *********************************************************/

const int am3::adapter::ionex::kWaterRatioIndex=0;
const int am3::adapter::ionex::kOrganicRatioIndex=1;
const int am3::adapter::ionex::kZnIIWaterRatioIndex=2;
const int am3::adapter::ionex::kZnIIOrganicRatioIndex=3;
const int am3::adapter::ionex::kBubbleCoefficientIndex=4;

am3::adapter::ionex::IonExchangeAdapter::IonExchangeAdapter()
{
  lo::Starting("IonExchangeAdapter constructor");

  con::ContractItemDispatcher<pro::Gravitation>::SetCalculator(gravitation_model_.get());
  //Create new models
  znIIRatioInOrganicSource_models_.push_back(new iom::ZnIIInOrganic_onlyConvection);;
  znIIRatioInWaterSource_models_.push_back(new iom::ZnIIInWater_onlyConvection);

  pro::Density density;
  density.dp(0.0);
  density.dt(0.0);
  density(1000.0);

  organicPhase_.reset(new iom::MaterialProperties(
    density,
    pro::ToSimpleProperty<double,pro::CoeffVolumeExpansion>(-5),
    pro::ToSimpleProperty<double,pro::SurfaceTension>(0.0),
    pro::ToSimpleProperty<double,pro::Cp>(420.0),
    pro::ToSimpleProperty<double,pro::ThermalConductivity>(100.0)));

  density(900.0);

  waterPhase_.reset(new iom::MaterialProperties(
    density,
    pro::ToSimpleProperty<double,pro::CoeffVolumeExpansion>(-5),
    pro::ToSimpleProperty<double,pro::SurfaceTension>(0.0),
    pro::ToSimpleProperty<double,pro::Cp>(420.0),
    pro::ToSimpleProperty<double,pro::ThermalConductivity>(100.0)));

//  mixedPhases_.reset(new iom::MeanMaterialPropertiesProvider(organicPhase_,this,waterPhase_,this));

  //If more models, repeat
  lo::Stopping("IonExchangeAdapter constructor");

}


am3::adapter::ionex::IonExchangeAdapter::~IonExchangeAdapter()
{
  lo::Starting("IonExchangeAdapter destructor");
//   vector<def::ViscProvider*>::iterator itVisc;
//   itVisc=viscosity_models_.begin();
//   for (itVisc=viscosity_models_.begin();itVisc<viscosity_models_.end();itVisc++) delete (*itVisc);
//   vector<exa::AgglomerationFactorProvider*>::iterator itAgg;
//   itAgg=agglomeration_models_.begin();
//   for (itAgg=agglomeration_models_.begin();itAgg<agglomeration_models_.end();itAgg++) delete (*itAgg);
  lo::Stopping("IonExchangeAdapter destructor");
}



pro::Viscosity am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const pro::Viscosity* const p )
{
  return GetStateTempl(point,viscosityModelSelector_,viscosity_models_,&mod::kViscosityTrait);
}


// pro::Gravitation am3::adapter::ionex::IonExchangeAdapter::GetState( com::Point* point, const pro::Gravitation* const p )
// {
//   return gravitation_model_->GetState(point,p);
// }

pro::HeatPerVolume am3::adapter::ionex::IonExchangeAdapter::GetState( com::Point* point, const pro::HeatPerVolume* const p )
{
  pro::HeatPerVolume pa;
  return pa;
}

pro::SurfaceTension am3::adapter::ionex::IonExchangeAdapter::GetState( com::Point* point, const pro::SurfaceTension* const p )
{
  pro::SurfaceTension pa;
  return pa;
}

pro::Density am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const pro::Density* const p )
{
  pro::Density pa;
  return pa;
}

pro::Cp am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const pro::Cp* const p )
{
  pro::Cp pa;
  return pa;
}

pro::ThermalConductivity am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const pro::ThermalConductivity* const p )
{
  pro::ThermalConductivity pa;
  return pa;
}

pro::CoeffVolumeExpansion am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const pro::CoeffVolumeExpansion* const p )
{
  pro::CoeffVolumeExpansion pa;
  return pa;
}

// pro::VelocityWithDerivatives am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const pro::VelocityWithDerivatives* const p )
// {
//   lo::Starting("Finding velocity by SimpleAdinaAdapter");
//   // Computed by macro model, get from storage
//   pro::VelocityWithDerivatives vel=adinaFStorage_->GetState(point,&vel);
//   lo::Stopping("Finding velocity by SimpleAdinaAdapter");
//   return vel;
// }

// pro::Temperature am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const pro::Temperature* const p )
// {
//   lo::Starting("Finding velocity by SimpleAdinaAdapter");
//   // Computed by macro model, get from storage
//   pro::TemperatureWithDerivatives temp=adinaFStorage_->GetState(point,&temp);
//   lo::Stopping("Finding velocity by SimpleAdinaAdapter");
//   return temp;
// }

iom::WaterPhaseRatio am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const iom::WaterPhaseRatio* const p )
{
  return this->GetPropertyFromRatio<iom::WaterPhaseRatio,kWaterRatioIndex>(point,p);
}

iom::OrganicPhaseRatio am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const iom::OrganicPhaseRatio* const p )
{
   return this->GetPropertyFromRatio<iom::OrganicPhaseRatio,kOrganicRatioIndex>(point,p);
}

iom::ZnIIRatioInWater am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const iom::ZnIIRatioInWater* const p )
{
   return this->GetPropertyFromRatio<iom::ZnIIRatioInWater,kZnIIWaterRatioIndex>(point,p);
}

iom::ZnIIRatioInOrganic am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const iom::ZnIIRatioInOrganic* const p )
{
   return this->GetPropertyFromRatio<iom::ZnIIRatioInOrganic,kZnIIOrganicRatioIndex>(point,p);
}

iom::BubbleCoefficient am3::adapter::ionex::IonExchangeAdapter::GetState( am3::model::common::Point* point, const iom::BubbleCoefficient* const p )
{
   return this->GetPropertyFromRatio<iom::BubbleCoefficient,kBubbleCoefficientIndex>(point,p);
}

int am3::adapter::ionex::IonExchangeAdapter::Init( boost::shared_ptr<def::AdinaFProvider> adinaFStorage )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (Adina-CFD storage)");
  adinaFStorage_=adinaFStorage;
  ratios_model_=adinaFStorage_;

  con::ContractItemDispatcher<pro::VelocityWithDerivatives>::SetCalculator(adinaFStorage_.get()); ;
  con::ContractItemDispatcher<pro::PressureWithDerivatives>::SetCalculator(adinaFStorage_.get()); ;
  con::ContractItemDispatcher<pro::TemperatureWithDerivatives>::SetCalculator(adinaFStorage_.get());;
  con::ContractItemDispatcher<pro::TurbulenceKWithDerivatives>::SetCalculator(adinaFStorage_.get());;
  con::ContractItemDispatcher<pro::TurbulenceEWithDerivatives>::SetCalculator(adinaFStorage_.get());;
  con::ContractItemDispatcher<pro::Coordinates>::SetCalculator(adinaFStorage_.get());;
  con::ContractItemDispatcher<pro::Time>::SetCalculator(adinaFStorage_.get());;

  lo::Stopping("Initialization of SimpleAdinaAdapter (Adina-CFD storage)");
  return true;
}

int am3::adapter::ionex::IonExchangeAdapter::Init( boost::shared_ptr<am3::knowledge::I4Adapter<pro::Viscosity> > a4Adapter)
{
  lo::Starting("Initialization of SimpleAdinaAdapter (viscosity adapter)");
  int isSuccess=false;
//   if (viscosityModelSelector_!=nullptr)
//   {
    viscosityModelSelector_=a4Adapter;
    isSuccess=1;
/*  }*/
  lo::Stopping("Initialization of SimpleAdinaAdapter (viscosity adapter)");
  return isSuccess;
}

int am3::adapter::ionex::IonExchangeAdapter::Init( am3::knowledge::I4Adapter<iom::WaterPhaseRatio>* a4Adapter )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (WaterPhaseRatio adapter)");
  int isSuccess=false;
  if (waterPhaseRatioSelector_!=nullptr)
  {
    waterPhaseRatioSelector_=a4Adapter;
    isSuccess=1;
  }
  lo::Stopping("Initialization of SimpleAdinaAdapter (WaterPhaseRatio adapter)");
  return isSuccess;
}

int am3::adapter::ionex::IonExchangeAdapter::Init( am3::knowledge::I4Adapter<iom::WaterPhaseRatioSource>* a4Adapter )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (WaterPhaseRatioSource adapter)");
  int isSuccess=false;
  if (waterPhaseRatioSourceSelector_!=nullptr)
  {
    waterPhaseRatioSourceSelector_=a4Adapter;
    isSuccess=1;
  }
  lo::Stopping("Initialization of SimpleAdinaAdapter (WaterPhaseRatioSource adapter)");
  return isSuccess;
}

int am3::adapter::ionex::IonExchangeAdapter::Init( am3::knowledge::I4Adapter<iom::OrganicPhaseRatio>* a4Adapter )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (OrganicPhaseRatio adapter)");
  int isSuccess=false;
  if (organicPhaseRatioSelector_!=nullptr)
  {
    organicPhaseRatioSelector_=a4Adapter;
    isSuccess=1;
  }
  lo::Stopping("Initialization of SimpleAdinaAdapter (OrganicPhaseRatio adapter)");
  return isSuccess;
}

int am3::adapter::ionex::IonExchangeAdapter::Init( am3::knowledge::I4Adapter<iom::OrganicPhaseRatioSource>* a4Adapter )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (OrganicPhaseRatioSource adapter)");
  int isSuccess=false;
  if (organicPhaseRatioSourceSelector_!=nullptr)
  {
    organicPhaseRatioSourceSelector_=a4Adapter;
    isSuccess=1;
  }
  lo::Stopping("Initialization of SimpleAdinaAdapter (OrganicPhaseRatioSource adapter)");
  return isSuccess;
}

int am3::adapter::ionex::IonExchangeAdapter::Init( am3::knowledge::I4Adapter<iom::ZnIIRatioInOrganicSource>* a4Adapter )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (IonRatioInOrganic adapter)");
  int isSuccess=false;
  if (znIIRatioInOrganicSelector_!=nullptr)
  {
    znIIRatioInOrganicSelector_=a4Adapter;
    isSuccess=1;
  }
  lo::Stopping("Initialization of SimpleAdinaAdapter (IonRatioInOrganic adapter)");
  return isSuccess;
}

int am3::adapter::ionex::IonExchangeAdapter::Init( am3::knowledge::I4Adapter<iom::ZnIIRatioInWaterSource>* a4Adapter )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (IonRatioInWater adapter)");
  int isSuccess=false;
  if (znIIRatioInWaterSelector_!=nullptr)
  {
    znIIRatioInWaterSelector_=a4Adapter;
    isSuccess=1;
  }
  lo::Stopping("Initialization of SimpleAdinaAdapter (IonRatioInWater adapter)");
  return isSuccess;
}

int am3::adapter::ionex::IonExchangeAdapter::Init( am3::knowledge::I4Adapter<iom::BubbleCoefficient>* a4Adapter )
{
  lo::Starting("Initialization of SimpleAdinaAdapter (BubbleCoefficient adapter)");
  int isSuccess=false;
  if (bubbleCoefficientSelector_!=nullptr)
  {
    bubbleCoefficientSelector_=a4Adapter;
    isSuccess=1;
  }
  lo::Stopping("Initialization of SimpleAdinaAdapter (BubbleCoefficient adapter)");
  return isSuccess;
}



// am3::adapter::IonExchangeAdapter a;
// 
// am3::adapter::IonExchangeAdapterProxy::IonExchangeAdapterProxy( IonExchangeAdapter* adp,reb::MockRebitKBS* kbs )
//    : am3::adapter::AdapterProxy<am3::knowledge::rebit::MockRebitKBS,IonExchangeAdapter,am3::knowledge::ConverterGeneralRebit<IonExchangeAdapter> >::AdapterProxy(adp,kbs)
// {
//   adp->Init(this);
// }
// 
// am3::adapter::IonExchangeAdapterProxy::~IonExchangeAdapterProxy()
// {
//   if (i4KBS!=nullptr) delete i4KBS;
// }
// 

