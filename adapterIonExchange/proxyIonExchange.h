
/*! \file proxyIonExchange.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef adapterIonExchange_proxyIonExchange_h__
#define adapterIonExchange_proxyIonExchange_h__
/*	include *********************************************************/



/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace adapter
  {
    namespace ionex
    {
      //Adapter class declaration, to remove cyclic headers includes
      class IonExchangeAdapter;
      
        //! Proxy class. Joins Knowledge Based System and Macro adapter. Uses converter to translate between KBS and Adapter
      //!
      //! \tparam KBS Knowlege Based System. As example, see am3::knowledge::rebit::MockRebitKBS
      //! \tparam CONVERTER Converts between am3 Properties and KBS variables description. As example, see am3::knowledge::ConverterGeneralRebit
      //!
      //! Must derive from: \n
      //!  - specialization of AdapterProxy with proper template\n
      //!  - I4Adapter(s) for all variables, for which model is chosen by KBS
      template<typename KBS, typename CONVERTER>
      class IonExchangeAdapterProxy
        : public ada::AdapterProxy<KBS,IonExchangeAdapter,CONVERTER>
//         , public kno::I4Adapter<pro::Viscosity>
//         , public kno::I4Adapter<exa::AgglomerationFactor>
        //! \todo Not implemented yet: , public kno::I4Adapter<pro::Temperature>
      {
      public:
        //! Must provide virtual destructor class could be derived
        virtual ~IonExchangeAdapterProxy(){};
        //! Constructor setting Adapter and KBS pointers. This is only one way to create object. 
        //! Initialization of members also here.
        IonExchangeAdapterProxy( IonExchangeAdapter* adp,KBS* kbs);
        //! \name Provides ModelIDs for models, providing Properties values
        //! In this methods, usually KBS is asked. In simple cases, chose can be done in code.
        //@{
//         mod::ModelID GetState(com::Point * point ,const pro::Viscosity* const visc);
//         mod::ModelID GetState(com::Point * point ,const exa::AgglomerationFactor* const agg);
        //@}
      };
    } //
  }
}
template<typename KBS, typename CONVERTER>
am3::adapter::ionex::IonExchangeAdapterProxy<KBS,CONVERTER>::IonExchangeAdapterProxy( IonExchangeAdapter* adp,KBS* kbs )
  : am3::adapter::AdapterProxy<KBS,IonExchangeAdapter,CONVERTER>::AdapterProxy(adp,kbs)
{
  lo::Starting("Initialization of SimpleAdinaAdapterProxy");
  //! AdapterProxy provides constructor with Adapter and KBS setting. Must be done here.
  //! Adapter needs initialization of all I4Adapter<PROPERTY> for all used Properties
  //! AdapterProxy derives from I4Adapters and could be itself used as I4Adapters.
  //! Projection must be done to chose proper Adapter.Init().
  //! It is possible (but not recommended) to use other than AdapterProxy classes as I4Adapters
//   adp->Init((kno::I4Adapter<exa::AgglomerationFactor>*)this);
//   adp->Init((kno::I4Adapter<pro::Viscosity>*)this);
  kbs->Init(&descriptor_);
  lo::Stopping("Initialization of SimpleAdinaAdapterProxy");
}

//! Usually just calls AdapterProxy method, but it can be processed in this class
// template<typename KBS, typename CONVERTER>
// mod::ModelID am3::adapter::ionex::IonExchangeAdapterProxy<KBS,CONVERTER>::GetState( com::Point * point ,const pro::Viscosity* const visc )
// {
//   lo::TestingMsg("Getting ModelId for viscosity from SimpleAdinaAdapterProxy");
//   return ada::AdapterProxy<KBS,IonExchangeAdapter,CONVERTER>::GetState(point,visc);
// }
// 
// //! Usually just calls AdapterProxy method, but it can be processed in this class
// template<typename KBS, typename CONVERTER>
// mod::ModelID am3::adapter::ionex::IonExchangeAdapterProxy<KBS,CONVERTER>::GetState( com::Point * point ,const exa::AgglomerationFactor* const agg )
// {
//   lo::TestingMsg("Getting ModelId for agglomeration from SimpleAdinaAdapterProxy");
//   return ada::AdapterProxy<KBS,SimpleAdinaAdapter,CONVERTER>::GetState(point,agg);
// 
// }




#endif // adapterIonExchange_proxyIonExchange_h__
