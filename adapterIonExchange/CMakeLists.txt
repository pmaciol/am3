set (BOOST_LINK_LIB )

if(UNIX)
	set (BOOST_LINK_LIB boost_program_options)
endif(UNIX)

include_directories (../)
add_library(am3adapterIonExchange adapterIonExchange.h adapterIonExchange.cpp)

target_link_libraries(am3adapterIonExchange am3modelIonExchange)