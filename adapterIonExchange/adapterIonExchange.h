
/*! \file adapterIonExchange.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief  
 * \details   	
*********************************************************************/
#ifndef adapterIonExchange_adapterIonExchange_h__
#define adapterIonExchange_adapterIonExchange_h__
/*	include *********************************************************/

#include <vector>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/shared_ptr.hpp>
#include "../adapter/adapter.h"
#include "../models/point.h"
#include "contracts.h"

#include "../modelIonExchange/modelIonExchange.h"
#include "../adinaModels/adinaDataInterface.h"
#include "../basicModels/rebitInterface.h"
#include "../knowledge/basicKbsInterfaces.h"
#include "../modelIonExchange/problemDependentDataStructures.h"
#include "../modelIonExchange/baseIonExchangeModels.h"

/*	using ***********************************************************/

using std::vector;
namespace pro=am3::model;
namespace iom=am3::model::ionex;
namespace def=am3::model::contract::defined;
namespace con=am3::model::contract;
namespace mod=am3::model;
namespace ada=am3::adapter;
namespace reb=am3::knowledge::rebit;
namespace kno=am3::knowledge;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
  namespace adapter
  {
    namespace ionex
    {
      extern const int kWaterRatioIndex;
      extern const int kOrganicRatioIndex;
      extern const int kZnIIWaterRatioIndex;
      extern const int kZnIIOrganicRatioIndex;
      extern const int kBubbleCoefficientIndex;

      template<typename RETURNED_TYPE,typename SELECTOR,typename MODELS_CONTAINER>
      RETURNED_TYPE GetStateTempl
        ( am3::model::common::Point* point, SELECTOR select, MODELS_CONTAINER container, RETURNED_TYPE* const p )
      {
        int i=select->GetState(point,p).Value();
        return container[i].GetState(point,p);
      }


    class IonExchangeAdapter 
      : public ada::Adapter
      //, public con::Provider<pro::RatiosSet> 
      , public def::IonExchangeMacroContract
      , public def::IonExchangeBubblesContract
      , public def::IonExchangeHeatSourceContract
      , public def::IonExchangeZnIIContentContract
      , public def::IonExchangeRatioContract
      , public def::AdinaFContract
    {

    protected:
      boost::shared_ptr<kno::I4Adapter<pro::Viscosity> > viscosityModelSelector_;
      //kno::I4Adapter<pro::Viscosity>* viscosityModelSelector_;
      kno::I4Adapter<iom::WaterPhaseRatio>* waterPhaseRatioSelector_;
      kno::I4Adapter<iom::WaterPhaseRatioSource>* waterPhaseRatioSourceSelector_;
      kno::I4Adapter<iom::OrganicPhaseRatio>* organicPhaseRatioSelector_;
      kno::I4Adapter<iom::OrganicPhaseRatioSource>* organicPhaseRatioSourceSelector_;
      kno::I4Adapter<iom::ZnIIRatioInOrganicSource>* znIIRatioInOrganicSelector_;
      kno::I4Adapter<iom::ZnIIRatioInWaterSource>* znIIRatioInWaterSelector_;
      kno::I4Adapter<iom::BubbleCoefficient>* bubbleCoefficientSelector_;

      boost::ptr_vector<def::ViscProvider> viscosity_models_;
      boost::ptr_vector<iom::ZnIIRatioInOrganicSourceProvider> znIIRatioInOrganicSource_models_;
      boost::ptr_vector<iom::ZnIIRatioInWaterSourceProvider> znIIRatioInWaterSource_models_;
      boost::shared_ptr<con::Provider<pro::Gravitation> > gravitation_model_;
      boost::shared_ptr<iom::MaterialProperties> organicPhase_;
      boost::shared_ptr<iom::MaterialProperties> waterPhase_;
      boost::shared_ptr<con::Provider<pro::RatiosSet> > ratios_model_;

    //  boost::shared_ptr<iom::MeanMaterialPropertiesProvider> mixedPhases_;

      def::IonExchangeMacro *macro_model_cfd_;
      def::IonExchangeProvider *macro_model_ion_;

      boost::shared_ptr<def::AdinaFProvider> adinaFStorage_;

      template <typename RATIO_BASED_TYPE, int INDEX>
      RATIO_BASED_TYPE GetPropertyFromRatio(am3::model::common::Point* point, const RATIO_BASED_TYPE* const p)
      {
        //assert(this->ratios_model_->GetState(point,&mod::kRatiosSetTrait).Value().size>INDEX)
        double ratio=this->ratios_model_->GetState(point,&mod::kRatiosSetTrait).Value().at(INDEX);
        RATIO_BASED_TYPE wp;
        wp(ratio);
        return wp;
      }

    public:

      IonExchangeAdapter();
      virtual ~IonExchangeAdapter();

      /// Must be used to omit ambiguous calls of GetState
      using con::ContractItemDispatcher<pro::VelocityWithDerivatives>::GetState ;
      using con::ContractItemDispatcher<pro::PressureWithDerivatives>::GetState ;
      using con::ContractItemDispatcher<pro::TemperatureWithDerivatives>::GetState;
      using con::ContractItemDispatcher<pro::TurbulenceKWithDerivatives>::GetState;
      using con::ContractItemDispatcher<pro::TurbulenceEWithDerivatives>::GetState;
      using con::ContractItemDispatcher<pro::Coordinates>::GetState;
      using con::ContractItemDispatcher<pro::Time>::GetState;

      // etc???
    
      //! \name "Dump" methods to provide full AdinaFContract
      //@{
      virtual pro::ReferenceTemperature GetState(com::Point* point, const pro::ReferenceTemperature* const p){return con::ZeroValue(p);}
      virtual pro::BulkModulus GetState(com::Point* point, const pro::BulkModulus* const p){return con::ZeroValue(p);}
      virtual pro::Cv GetState(com::Point* point, const pro::Cv* const p){return con::ZeroValue(p);}
      //@}

      //! \name Implementation of non-trivial (non-dump) GetState methods derived from Contracts
      //@{
      virtual pro::CoeffVolumeExpansion GetState(com::Point* point, const pro::CoeffVolumeExpansion* const p);
      //virtual pro::Gravitation GetState(com::Point* point, const pro::Gravitation* const p);
      virtual pro::HeatPerVolume GetState(com::Point* point, const pro::HeatPerVolume* const p);
      virtual pro::SurfaceTension GetState(com::Point* point, const pro::SurfaceTension* const p);
      virtual pro::Density GetState(am3::model::common::Point* point, const pro::Density* const p);
      virtual pro::Cp GetState(am3::model::common::Point* point, const pro::Cp* const p);
      virtual pro::ThermalConductivity GetState(am3::model::common::Point* point, const pro::ThermalConductivity* const p);
      virtual pro::Viscosity GetState(am3::model::common::Point* point, const pro::Viscosity* const p);
      //virtual pro::VelocityWithDerivatives GetState(am3::model::common::Point* point, const pro::VelocityWithDerivatives* const p);
      //virtual pro::Temperature GetState(am3::model::common::Point* point, const pro::Temperature* const p);
      virtual iom::WaterPhaseRatio GetState(am3::model::common::Point* point, const iom::WaterPhaseRatio* const p);
      virtual iom::OrganicPhaseRatio GetState(am3::model::common::Point* point, const iom::OrganicPhaseRatio* const p);
      virtual iom::ZnIIRatioInWater GetState(am3::model::common::Point* point, const iom::ZnIIRatioInWater* const p);
      virtual iom::ZnIIRatioInOrganic GetState(am3::model::common::Point* point, const iom::ZnIIRatioInOrganic* const p);
      virtual iom::BubbleCoefficient GetState(am3::model::common::Point* point, const iom::BubbleCoefficient* const p);
      //@}

      int Init( boost::shared_ptr<def::AdinaFProvider> adinaFStorage);
      int Init( boost::shared_ptr<am3::knowledge::I4Adapter<pro::Viscosity> > a4Adapter );
      int Init( am3::knowledge::I4Adapter<iom::WaterPhaseRatio>* a4Adapter );
      int Init( am3::knowledge::I4Adapter<iom::WaterPhaseRatioSource>* a4Adapter );
      int Init( am3::knowledge::I4Adapter<iom::OrganicPhaseRatio>* a4Adapter );
      int Init( am3::knowledge::I4Adapter<iom::OrganicPhaseRatioSource>* a4Adapter );
      int Init( am3::knowledge::I4Adapter<iom::ZnIIRatioInOrganicSource>* a4Adapter );
      int Init( am3::knowledge::I4Adapter<iom::ZnIIRatioInWaterSource>* a4Adapter );
      int Init( am3::knowledge::I4Adapter<iom::BubbleCoefficient>* a4Adapter );

      //@}
      //! \name Realizes am3::model::Model interface
      //@{
      virtual int Init(){return -1;};
      virtual int Run(){return -1;};
      virtual int PostProcess(){return -1;};
      //@}
      };

    }
  } //adapter
} //am3
#endif // adapterIonExchange_adapterIonExchange_h__
