
/*! \file caseTugMatCalc.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief   The case, where Deform2D uses MatCalc to calculate precipitation kinetics. All other computations are done in usr_upd.f procedures
 * \details
 *********************************************************************/
#ifndef am3caseTugMatCalcToUSP_caseTugMatCalc_h__
#define am3caseTugMatCalcToUSP_caseTugMatCalc_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../softwareDeform/policyOneStepOneElement.h"
#include "databus.h"

/*	using ***********************************************************/

namespace sdm = am3::software::deform;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
      sdm::DeformHandlerFacadeUpd* GetDeformHandlerFacadeUpdate();

      typedef sdm::OneStepOneElementNoElementCheckUserValues DeformCaseIV;

		} //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalcToUSP_caseTugMatCalc_h__
