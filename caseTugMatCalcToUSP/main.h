
/*! \file main.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_main_h__
#define am3caseTugMatCalc_main_h__
/*	include *********************************************************/

#include "../models/models.h"
#include "databus.h"
#include "caseTugMatCalcToUSP.h"
/*	using ***********************************************************/


/*	extern **********************************************************/


/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
			// This case is used as a dll library. Init method of Main class works like a "main" procedure in single executable solution
			// Implementation in cpp file
			// private members are necessary to keep all models 'alive' during computations. If sub-models are not bind to databus, member shared_ptr must be kept for them
			class Main : public am3::model::InitializableMainModel
			{
			public:
				Main();
				bool initialized_ = false;
				void Init();

				virtual bool IsInitialized() const;
        sdm::DeformHandlerFacadeUpd* GetDeformCaseIV();
			private:
			//public: 
				boost::shared_ptr<CaseTugMatCalcToUSPDatabus> mainDatabus_;
        boost::shared_ptr<DeformCaseIV> deformCaseIV_;
			};
		} //tugmc
	} //cases
} //am3


#endif // am3caseTugMatCalc_main_h__
