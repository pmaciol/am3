
/*! \file ivMapping.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalcToUSP_ivMapping_h__
#define am3caseTugMatCalcToUSP_ivMapping_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;


/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
      // Structure used to code/decode values in IV vector
      template<typename T>
      struct VariableID;

			template <> struct VariableID < mpr::PrecipitationsMeanRadius > { static const unsigned int id = 2; typedef mpr::PrecipitationsMeanRadius Type; };
			template <> struct VariableID < mpr::PrecipitationsVolumeFraction > { static const unsigned int id = 3; typedef mpr::PrecipitationsVolumeFraction Type; };

		} //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalcToUSP_ivMapping_h__
