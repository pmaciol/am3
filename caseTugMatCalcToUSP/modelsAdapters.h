
/*! \file modelsAdapters.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalcToUSP_modelsAdapters_h__
#define am3caseTugMatCalcToUSP_modelsAdapters_h__
/*	include *********************************************************/

#include "ivStorage.h"
#include "../softwareDeform/policyOneStepOneElement.h"
#include "../modelTemplates/macroState.h"
#include "kbs.h"
#include "contract.h"

/*	using ***********************************************************/

namespace sdm = am3::software::deform;
namespace mot = am3::model::templates;
namespace ada = am3::adapter;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
			// Precipitation switcher
			template<typename INPUT>
			struct PrecipitationsSwitcher
				: public ada::SwitcherStatic < Kbs, PrecipitationsContract, INPUT, MacroContract >
			{
			public:

				boost::shared_ptr<Kbs<INPUT> > kbs_;
				PrecipitationsSwitcher(boost::shared_ptr<Kbs<INPUT> > kbs) : ada::SwitcherStatic<Kbs, PrecipitationsContract, INPUT, MacroContract>(kbs){};
				PrecipitationsSwitcher()// : ada::SwitcherStatic<Kbs, PrecipitationsContract, INPUT, MacroContract>(kbs_) 
				{
					boost::shared_ptr<INPUT> bus(bus_);
					kbs_ = boost::shared_ptr<Kbs<INPUT> >(new Kbs<INPUT>(bus));
					ada::SwitcherStatic<Kbs, PrecipitationsContract, INPUT, MacroContract>::SetKbs(kbs_);
				};
			};


			template <typename INPUT>
			class SecondPhasePrecipitationsConst
				: public PrecipitationsContract
				, public mo::InputTypelist < INPUT >
			{
			public:
				typedef mot::EmptyContract2D Inputs;

				mpr::PrecipitationsMeanRadius GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::PrecipitationsMeanRadius *const){ mpr::PrecipitationsMeanRadius toRet; toRet(0.2); return toRet; };
				mpr::PrecipitationsVolumeFraction GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::PrecipitationsVolumeFraction *const){ mpr::PrecipitationsVolumeFraction toRet; toRet(0.3); return toRet; };
				template<typename VAR> VAR Get(mpt::Point<mpr::Coordinates2D> * point);
				template<> mpr::PrecipitationsMeanRadius Get<mpr::PrecipitationsMeanRadius>(mpt::Point<mpr::Coordinates2D> * point) { mpr::PrecipitationsMeanRadius* trait;  return GetState(point, trait); };
				template<> mpr::PrecipitationsVolumeFraction Get<mpr::PrecipitationsVolumeFraction>(mpt::Point<mpr::Coordinates2D> * point) { mpr::PrecipitationsVolumeFraction* trait;  return GetState(point, trait); };
			};


  
      //MAcroModelState should beremoved just when MacroModelTemperature will work
      template<typename INPUT>
      class MacroModelState
        : public mot::MacroModelState<sdm::OneStepOneElementNoElementCheckUserValues, INPUT >
      {
      public:
        //inherited constructor - just when changed to VS2015
		  MacroModelState(boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> macroStateHandler) : mot::MacroModelState < sdm::OneStepOneElementNoElementCheckUserValues, INPUT >(macroStateHandler){};
      };

	  // Get material state (strain, temperature, time, etc) from FEM software (Deform2D in this case)
	  // An adapter of mot::MacroModelState to 'Databus-entry' format (only one template parameter)
	  template<typename INPUT>
	  class MacroModelTemperature
		  : public mot::MacroModelTemperature<sdm::OneStepOneElementNoElementCheckUserValues, INPUT >
	  {
	  public:
		  //inherited constructor - just when changed to VS2015
      MacroModelTemperature(boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> macroStateHandler) : mot::MacroModelTemperature < sdm::OneStepOneElementNoElementCheckUserValues, INPUT >(macroStateHandler){};
	  };

		} //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalcToUSP_modelsAdapters_h__
