
/*! \file kbs.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalcToUSP_kbs_h__
#define am3caseTugMatCalcToUSP_kbs_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "../modelTemplates/macroState.h"


/*	using ***********************************************************/

namespace ctm = am3::cases::tugmc;
namespace mpr = am3::model::properties;
namespace mot = am3::model::templates;
namespace mpt = am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
			// UserGuide: This is ugly and simple "Knowledge Based System" for caseColdRolling.
			template<typename INPUT>
			class Kbs
				: public mo::InputTypelist < INPUT >
			{
			public:
				typedef mot::MacroContract Inputs;
				typedef mot::EmptyContract2D Contract;

				typedef mpr::Coordinates2D CoordinatesType;


				Kbs(boost::shared_ptr<INPUT> databus)
				{
					temperature(0);
					SetModelInput(databus.get());
				}

				unsigned int instances;
				mpr::Temperature temperature;

				template <typename VARIABLE>
				mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point)
				{
					if (bus_)
					{
						const VARIABLE* trait = nullptr;
						return GetModelId(point, trait);
					}
					else
					{
						mpr::ModelID mId;
						mId(std::numeric_limits<int>::signaling_NaN());
						lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE, "Databus not initialized pointer not set");
						return mId;
					}
				}

			protected:
				// UserGuide:  Here is "knowledge"
				mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const mpr::StressAthermal* const)
				{
					mpr::ModelID mId;
					mId(0);
					return mId;
				}
				// 				// UserGuide: GetModelId must be provided for each variable in Contract of the Switcher, which use this KBS. If more than one Switcher is used, GetModelId must be provided for all variables from all Contracts
				mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const mpr::PrecipitationsMeanRadius* const)
				{
          
					mpr::ModelID mId;
					mpr::Temperature localTemperature;
					localTemperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          //DebugBreak();
					if ((temperature() - localTemperature()) < 50)
					{
						mId(1);
					}
					else
					{
						mId(0);
					}
          lo::debugLog("mId chosen = " + std::to_string(mId()));
					return mId;
				}
				mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const mpr::PrecipitationsVolumeFraction* const)
				{
					mpr::ModelID mId;
					mpr::Temperature localTemperature;
					localTemperature = bus_->GetFromDatabus<mpr::Temperature>(point);
					if ((temperature() - localTemperature()) < 50)
					{
						mId(1);
					}
					else
					{
						mId(0);
					}
					return mId;
				};
			};
		} //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalcToUSP_kbs_h__
