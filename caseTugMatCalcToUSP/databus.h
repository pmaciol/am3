/*! \file databus.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalcToUSP_databus_h__
#define am3caseTugMatCalcToUSP_databus_h__
/*	include *********************************************************/

#include "../common/typelists.h"
#include "modelsAdapters.h"
#include "ivStorage.h"
#include "../databus/databusTypelists.h"

/*	using ***********************************************************/

namespace db = am3::databus;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
      // List of 'Databus' classes
      typedef TEMPLATELIST_3(
		        IvManager,
		        PrecipitationsSwitcher, 
		        MacroModelTemperature
		        ) CaseTugMatCalcToUSPList;
		
		      // Databus
	  typedef db::DatabusTypelists<CaseTugMatCalcToUSPList> CaseTugMatCalcToUSPDatabus;
				} //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalcToUSP_databus_h__
