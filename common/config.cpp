/*!
 * \file config.cpp
 *
 * \author Piotr Maciol
 * \date 02.07.2012
 */

/*	include *********************************************************/

#include <fstream>
#include <boost/foreach.hpp>
#include "config.h"
#include "../logger/logger.h"

/*	using ***********************************************************/

namespace po = boost::program_options;
namespace lo = am3::logger;

/*	extern **********************************************************/

extern int*  global_argc;
extern char ***global_argv;

/*	classes *********************************************************/

namespace am3
{
  namespace common
  {
    //////////////////////////////////////////////////////////////////////////
    Configuration kProgramOptions;
    //////////////////////////////////////////////////////////////////////////

int ConfigurableModule::CheckIsModuleRegistered( const string moduleName )
{
  int success=0;
  po::variables_map& vm=kProgramOptions.GetMainVariableMap();
  const vector<string>& registerdModules = vm["Module"].as< vector<string> >();
  if (std::find(registerdModules.begin(), registerdModules.end(), moduleName)!=registerdModules.end())
  {
    lo::log<lo::SEV_NOTICE>(lo::LOG_COMMON,("Module "+moduleName+" configuration description found").c_str());
    success=1;
  }
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_COMMON,("There is no configuration description for module "+moduleName).c_str());
  }
  return success;
}

po::variables_map& ConfigurableModule::ReadModuleConfigFileAndCmdLineOpts( const string moduleName )
{
  //! \Todo You can try to refactor this removing returned value, but it will affect a lot of modules!
  string path;
  string filename;

  po::variables_map& vm = kProgramOptions.RegisterNewVariableMap();
  po::variables_map& vmMain = kProgramOptions.GetMainVariableMap();

  const vector<string>& registerdModules = vmMain["Module"].as< vector<string> >();
  const vector<string>& registerdModulesPaths = vmMain["ModuleCfgPath"].as< vector<string> >();
  const vector<string>& registerdModulesCfg = vmMain["ModuleCfg"].as< vector<string> >();
  vector<string>::const_iterator it=std::find(registerdModules.begin(), registerdModules.end(), moduleName);

  if (it!=registerdModules.end())
  {
    vector<string>::size_type dist=std::distance(registerdModules.begin(),it);
    path=registerdModulesPaths[dist];
    filename=registerdModulesCfg[dist];
    po::options_description& od = DefineOptionsForModule();
    kProgramOptions.StoreCommandLineOptions(*global_argc,*global_argv,od,vm);
    kProgramOptions.StoreProgramOptions(path+filename,od,vm);
    lo::log<lo::SEV_NOTICE>(lo::LOG_COMMON,("Options read from module configuration file "+path+filename).c_str());
  }
  else
  {
    lo::log<lo::SEV_ERROR>(lo::LOG_COMMON,("There is no configuration description for module "+moduleName+". Use CheckConfFile(...) before").c_str());
  }
  this->module_variable_map=&vm;
  return vm;
}

ConfigurableModule::~ConfigurableModule()
{
  if (module_variable_map!=nullptr)
  {
    delete module_variable_map;
  }
}

void ConfigurableModule::SetModuleOptionsTemplateMethod()
{
  if (IsConfigured()) lo::log<lo::SEV_WARNING>(lo::LOG_MODEL,(GetModuleName()+" was configured before").c_str());
  else
  {
    if (CheckIsModuleRegistered(GetModuleName()))
    {
      ReadModuleConfigFileAndCmdLineOpts(GetModuleName());
      CopyReadOptionsToInternal();
      isConfigured_=true;
    }
    else
    {
      lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,(GetModuleName()+" configuration error").c_str());
    }
  }
}

//////////////////////////////////////////////////////////////////////////

void Configuration::StoreCommandLineOptions(int argc, char **argv)
{
  if (vms_.size()<1) RegisterNewVariableMap()/* vms_.push_back(new po::variables_map())*/;
  BOOST_FOREACH(po::variables_map* vm, vms_)
  {
    BOOST_FOREACH(po::options_description* opt, options_)
    {
      po::store(po::command_line_parser(argc, argv).options(*opt).allow_unregistered().run(), *vm);
    }
    Notify(*vm);
  }
}

void Configuration::StoreCommandLineOptions(int argc, char **argv, po::options_description& opt,po::variables_map& vm)
{
  po::store(po::command_line_parser(argc, argv).options(opt).allow_unregistered().run(), vm);
  Notify(vm);
  if (vm.count("help")) 
  {
    cout << opt << "\n";
  }
}

po::options_description& Configuration::GetNewProgramOptions( const string optionDescName )
{
  options_.push_back(new po::options_description(optionDescName));
//  options_[0]->add(*(options_[options_.size()-1])); // REMOVE after tests
  //return *(options_[options_.size()-1]); // REMOVE after tests
   return *options_.back();// WHY!!! It does not work! - now it works?
}

int Configuration::StoreProgramOptions( const string confFileName, po::options_description& options,po::variables_map& vm,bool allowUnregistered)
{
  int success=0;
  std::ifstream myfile(confFileName.c_str());
  if (myfile.good())
  {
    po::store(po::parse_config_file(myfile, options,allowUnregistered ), vm);
    am3::logger::log<am3::logger::SEV_NOTICE>(am3::logger::LOG_COMMON,("Program options read from file "+confFileName).c_str());
    success=1;
  }
  else
  {
    am3::logger::log<am3::logger::SEV_CRITICAL>(am3::logger::LOG_COMMON,("Program options from file "+confFileName+" failed").c_str());
  }
  myfile.close();
  return success;
}

po::variables_map& Configuration::RegisterNewVariableMap()
{
  vms_.push_back(new po::variables_map());
  //return *(vms_[vms_.size()-1]);
  return *vms_.back();// Now it works
}

void Configuration::Init()
{
  assert(vms_.empty());
  assert(options_.empty());
  po::variables_map& firstVariableMap=RegisterNewVariableMap();

  GetNewProgramOptions("All options");
  StoreCommandLineOptions(*global_argc,*global_argv,GetConfiguredCommandLine(),firstVariableMap);
  if (firstVariableMap.count("mainCfg")) {
     main_conf_file_name_=new string(firstVariableMap["mainCfg"].as<string>());
  }
  else
  {
    am3::logger::log<am3::logger::SEV_ERROR>(am3::logger::LOG_COMMON,"No main config file name");
    assert(false);
  }
  if (firstVariableMap.count("mainCfgPath")) {
    main_conf_file_path_=new string(firstVariableMap["mainCfgPath"].as<string>());
  }
  else
  {
    am3::logger::log<am3::logger::SEV_ERROR>(am3::logger::LOG_COMMON,"No path to main config file");
    assert(false);
  }
  ReadMainConfigFile();
  initialized_=true;
}

po::variables_map& Configuration::GetMainVariableMap()
{

  if (vms_.empty())
  {
    am3::logger::log<am3::logger::SEV_CRITICAL>(am3::logger::LOG_COMMON,"Access to non-initialized variable map container");
  }
  return *vms_[0];
}

void Configuration::RemoveLastVariableMap()
{
  delete (vms_.back());
  vms_.erase(vms_.end()-1);
}

Configuration::~Configuration()
{
  std::vector<po::variables_map*>::size_type i;
  for ( i=0;i++;(i<vms_.size()))
  {
    delete vms_[i];
  }
  for ( i=0;i++;(i<options_.size()))
  {
    delete options_[i];
  }
  delete main_conf_file_name_;
  delete main_conf_file_path_;
}

po::options_description& Configuration::AllOptions()
{
  if (options_.empty())
  {
    am3::logger::log<am3::logger::SEV_CRITICAL>(am3::logger::LOG_COMMON,"Access to non-initialized All Options container");
    exit(1);
  }
  return *(options_[0]);
}

po::options_description& Configuration::CommandLineOptions()
{
  if (options_.size()<2)
  {
    am3::logger::log<am3::logger::SEV_CRITICAL>(am3::logger::LOG_COMMON,"Access to non-initialized Command Line Options container");
    exit(1);
  }
  return *(options_[1]);
}

int Configuration::ReadMainConfigFile()
{
 int success= StoreProgramOptions(*main_conf_file_path_+*main_conf_file_name_,GetConfiguredMainOptions(),*vms_[0]);
 lo::log<lo::SEV_NOTICE>(lo::LOG_COMMON,("Options read from main configuration file "+*main_conf_file_path_+*main_conf_file_name_).c_str());
 return success;
}

 std::vector<po::variables_map*> Configuration::vms_;
 std::vector<po::options_description*> Configuration::options_;
 std::string*  Configuration::main_conf_file_name_;
 std::string*  Configuration::main_conf_file_path_;
 bool Configuration::initialized_;

  }//namespace common
}//namespace am3