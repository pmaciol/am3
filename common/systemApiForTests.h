/********************************************************************
  author:		Piotr Maciol
  contact:  pmaciol@agh.edu.pl
*********************************************************************/

#ifndef systemApiForTests_h__
#define systemApiForTests_h__

#include <string>
#include "systemApi.h"
namespace am3
{

  namespace systemApi
  {
    using std::string;
    class SystemApiForTests
    {
    public:
      virtual int IsProcessActive(string process_name)=0;
      virtual void SystemSleep(long time_in_miliseconds)=0;
      virtual void CpFile(string file1,string file2)=0;
			virtual int FileExists(string filename,string pathname) const = 0;
    };
    class WindowsApiForTests:public SystemApiForTests
    {
    public:
      int IsProcessActive(string process_name);
      void SystemSleep(long time_in_miliseconds);
      void CpFile(string file1,string file2);
			virtual int FileExists(string filename,string pathname) const;
    protected:
    private:
    };

        class LinuxApiForTests:public SystemApiForTests
    {
    public:
      
    protected:
    private:
    };

    class SystemApiHandlerForTests:public SystemApiHandler
    {
    public:
      SystemApiHandlerForTests();
      SystemApiForTests* const GetSystemApiForTests() const { return systemApiForTests; }
    protected:
#if defined(_WIN32) || defined(_WIN64)
      static WindowsApiForTests system_api_for_tests;
#elif defined(_linux)
      static LinuxApiForTests system_api_for_tests;
#else 
      assert(false);
#endif 
      SystemApiForTests* const systemApiForTests;
    };
  }
}
#endif // systemApiForTests_h__
