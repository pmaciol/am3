
/*! \file literals.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef common_literals_h__
#define common_literals_h__
/*	include *********************************************************/

//#include <string>

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  extern const char* kModelId;
  extern const char* kTemperature;
  extern const char* kVelocity;
  extern const char* kVelocityWithDerivatives;
  extern const char* kViscosity;
  extern const char* kAgglomerationFactor;
  extern const char* kDensity;
  extern const char* kCharacteristicLength;
  extern const char* kDensity;
  extern const char* kMaterialName;
  extern const char* kShearRate;

  extern const int kThixotropySolidState;
  extern const int kThixotropySemiSolidState;
  extern const int kThixotropyLiquidState;

  extern const char* kStrain2D;
  extern const char* kTotalEffStrain;
  extern const char* kStress2D;
  extern const char* kYieldStress;
  extern const char* kStrainRate2D;
  extern const char* kCoordinates2D;
  extern const char* kCoordinates3D;  
  extern const char* kTime;
  extern const char* kPoint2D;
  extern const char* kPoint3D;
  extern const char* kGlobalElementNumber;
  extern const char* kDeltaTemperature;
  extern const char* kPreviousModelId;

	// Names of models
	extern const char* kTUGCaGrain;
	extern const char* kTUGSsc;

} //am3


#endif // common_literals_h__
