/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/
#ifndef systemApi_h__
#define systemApi_h__
/*	include *********************************************************/

#include <string>
#include <assert.h>

/*	classes *********************************************************/

namespace am3
{

  namespace systemApi
  {
    using std::string;

    

    class SystemApi
    {
    public:
      virtual int AsynchronicExecute(string program_path, string program_name,string program_parameters, string working_directory) const =0;
      virtual int SynchronicExecute(string program_path, string program_name,string program_parameters, string working_directory) const =0;
      virtual int DelFile(string file_path, string file_name) const = 0;
	  virtual int FileExists(string filename,string pathname) const = 0;
    };

    class WindowsApi:public SystemApi
    {
    public:
      virtual int AsynchronicExecute(string program_path, string program_name,string program_parameters, string working_directory) const;
      virtual int SynchronicExecute(string program_path, string program_name,string program_parameters, string working_directory) const;
      virtual int DelFile(string file_path, string file_name) const;
	  virtual int FileExists(string filename,string pathname) const;
    protected:
    private:
    };

    class LinuxApi:public SystemApi
    {
    public:
      virtual int AsynchronicExecute(string program_path, string program_name,string program_parameters, string working_directory) const;
      virtual int SynchronicExecute(string program_path, string program_name,string program_parameters, string working_directory) const;
      virtual int DelFile(string file_path, string file_name) const;
    protected:
    private:
    };
    
    class SystemApiHandler
    {
    public:
      SystemApiHandler();
      SystemApi* const GetSystemApi() const { return systemApi; }
    protected:
      #if defined(_WIN32) || defined(_WIN64)
        static WindowsApi system_api;
      #elif defined(_linux)
        static LinuxApi system_api;
      #else 
        assert(false);
      #endif 
      SystemApi* const systemApi;
    };
  }
}

#endif // systemApi_h__
