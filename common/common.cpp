#include <limits>
#include <cmath>
#include "common.h"

std::string am3::common::FileWithExt( const std::string file,const std::string ext )
{
	return file+"."+ext;
}

bool AlmostEqual(const double& a, const double& b)
{
  return std::fabs(a - b) < std::numeric_limits<double>::epsilon();
}

bool AlmostZero(const double& a)
{
  return std::fabs(a) < std::numeric_limits<double>::epsilon();
}
