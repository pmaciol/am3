#include "literals.h"

const char* am3::kModelId ="modelId"; 

const char* am3::kTemperature ="temperature";
const char* am3::kVelocity ="velocity";
const char* am3::kVelocityWithDerivatives ="velocityWithDer";
const char* am3::kViscosity ="viscosity";
const char* am3::kAgglomerationFactor ="agglomerationFactor";
const char* am3::kDensity ="density";
const char* am3::kCharacteristicLength = "characteristicLength";
const char* am3::kMaterialName = "materialName";
const char* am3::kShearRate = "shearRate";


const int am3::kThixotropySolidState = 0;
const int am3::kThixotropySemiSolidState = 1;
const int am3::kThixotropyLiquidState =2;

extern const char* am3::kStrain2D="strain2D";
extern const char* am3::kStress2D="stress2D";
extern const char* am3::kYieldStress="yieldStress";
extern const char* am3::kTotalEffStrain="totalEffStrain";
extern const char* am3::kStrainRate2D="strainRate2D";
extern const char* am3::kCoordinates2D="coordinates2D";
extern const char* am3::kCoordinates3D="coordinates3D";
extern const char* am3::kTime="time";
extern const char* am3::kPoint2D="point2D";
extern const char* am3::kPoint3D="point3D";
extern const char* am3::kGlobalElementNumber="elementNumberGlobal";
extern const char* am3::kDeltaTemperature="deltaTemperature";
extern const char* am3::kPreviousModelId="kPreviousModelId";

// Names of models
extern const char* am3::kTUGCaGrain="TUGCaGrain";
extern const char* am3::kTUGSsc="TUGSsc";
