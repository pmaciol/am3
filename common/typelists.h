
/*! \file typelists.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef typelists_h__
#define typelists_h__
/*	include *********************************************************/

#include <tuple>

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

class NullType {};

class EmptyType {};

template<typename H>
class EmptyTemplate;

template<>
class EmptyTemplate<EmptyType>
{};


inline bool operator==(const EmptyType&, const EmptyType&)
{
	return true;
}   

inline bool operator<(const EmptyType&, const EmptyType&)
{
	return false;
}

inline bool operator>(const EmptyType&, const EmptyType&)
{
	return false;
}

class null_typelist {};

template <class H, class T>
struct Typelist
{
	typedef H head;
	typedef T tail;
  typedef Typelist <H, T> List;
};

//////////////////////////////////////////////////////////////////////////
template <
  template <typename G> class H, 
  class T>
struct Templatelist
{
//   typedef H<G> head;
//   typedef T<G> tail;
};


#define TEMPLATELIST_1(T1) Templatelist<T1, EmptyType >
#define TEMPLATELIST_2(T1, T2) Templatelist<T1, TEMPLATELIST_1(T2) >
#define TEMPLATELIST_3(T1, T2, T3) Templatelist<T1, TEMPLATELIST_2(T2, T3) >
#define TEMPLATELIST_4(T1, T2, T3, T4) Templatelist<T1, TEMPLATELIST_3(T2, T3, T4) >
#define TEMPLATELIST_5(T1, T2, T3, T4, T5) Templatelist<T1, TEMPLATELIST_4(T2, T3, T4, T5) >
#define TEMPLATELIST_6(T1, T2, T3, T4, T5, T6) Templatelist<T1, TEMPLATELIST_5(T2, T3, T4, T5, T6) >
#define TEMPLATELIST_7(T1, T2, T3, T4, T5, T6, T7) Templatelist<T1, TEMPLATELIST_6(T2, T3, T4, T5, T6, T7) >
#define TEMPLATELIST_8(T1, T2, T3, T4, T5, T6, T7, T8) Templatelist<T1, TEMPLATELIST_7(T2, T3, T4, T5, T6, T7, T8) >
#define TEMPLATELIST_9(T1, T2, T3, T4, T5, T6, T7, T8, T9) Templatelist<T1, TEMPLATELIST_8(T2, T3, T4, T5, T6, T7, T8, T9) >
#define TEMPLATELIST_10(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10) Templatelist<T1, TEMPLATELIST_9(T2, T3, T4, T5, T6, T7, T8, T9, T10) >
#define TEMPLATELIST_11(T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11) Templatelist<T1, TEMPLATELIST_10(T2, T3, T4, T5, T6, T7, T8, T9, T10, T11) >
//////////////////////////////////////////////////////////////////////////

template <class LIST, class TUPLE>
struct TypelistTuple
{
};

template <class H, class T, class TUPLE>
struct TypelistTuple<Typelist <H, T>, TUPLE>
//	: public Typelist<H,T>
{
	typedef Typelist <H, T> List;
	typedef TUPLE Tuple;
};

//////////////////////////////////////////////////////////////////////////

template <typename TL1,typename TL2>
struct InvertInternal;

template <typename TL2>
struct InvertInternal<EmptyType, TL2>
{
  typedef TL2 Inverted;
};

template <typename H1, typename T1, typename TL2>
struct InvertInternal<Typelist<H1,T1>, TL2>
  : public InvertInternal<T1,Typelist<H1, TL2> >
{
  //typedef TL2 Inverted;
};

template <typename TL>
struct Invert;

template <>
struct Invert<EmptyType>
{
  typedef EmptyType Inverted;

};


template <typename H,typename T>
struct Invert<Typelist<H,T> >
  : public InvertInternal<T,Typelist<H,EmptyType> >
{};

template <typename H,typename T>
struct Invert<TypelistTuple<H,T> >
  : public Invert<H>
{};


template <class ADD_TO, class ADD_FROM>
struct MergeTypelistsInternal;

template <typename TO, typename HFROM, typename TFROM>
struct MergeTypelistsInternal<TO, Typelist<HFROM,TFROM> >
  : public MergeTypelistsInternal<Typelist<HFROM,TO>,TFROM>
{ };

template <typename TO>
struct MergeTypelistsInternal<TO, EmptyType>
{
  typedef TO Merged;
};

template <typename TO>
struct MergeTypelistsInternal<TO, Typelist<EmptyType,EmptyType> >
{
  typedef TO Merged;
};

template <class ADD_TO, class ADD_FROM>
struct MergeTypelists
  : public MergeTypelistsInternal<ADD_TO,typename Invert<ADD_FROM>::Inverted>
{ };

//////////////////////////////////////////////////////////////////////////


struct TRUETYPE{};
struct FALSETYPE{};

// template <typename T1, typename T2>
// struct IsSame;


template <typename T1, typename T2>
struct IsSame
{
typedef FALSETYPE Type;
};


template <typename T1>
struct IsSame<T1,T1>
{
typedef TRUETYPE Type;
};


//////////////////////////////////////////////////////////////////////////
template<typename TYPE, typename TYPETOCHECK, typename H, typename MAP>
struct GetFromMapInternal;

template<typename TYPE, typename H, typename H1, typename T1>
struct GetFromMapInternal<TYPE, FALSETYPE, H, Typelist<H1,T1> >
: public GetFromMapInternal<TYPE, typename  IsSame <typename H1::First, TYPE>::Type , H1, T1>
{

};

template<typename TYPE, typename H>
struct GetFromMapInternal<TYPE, FALSETYPE, H, EmptyType>
{
  typedef EmptyType Second;
};

template<typename TYPE, typename H, typename H1, typename T1>
struct GetFromMapInternal<TYPE, TRUETYPE, H, Typelist<H1,T1> >
//: public GetFromMapInternal<TYPE, IsSame <typename H1::First, H> , H1, T1>
{
typedef typename H::Second Second;
};

template<typename TYPE, typename H>
struct GetFromMapInternal<TYPE, TRUETYPE, H, EmptyType >
//: public GetFromMapInternal<TYPE, IsSame <typename H1::First, H> , H1, T1>
{
typedef typename H::Second Second;
};

// 
// template<typename TYPE, typename H, typename T>
// struct GetFromMapInternal<TYPE, TYPE, H, T>
// {
//   typedef typename H::Second Second;
// };

// / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / 

template<typename TYPE, typename MAP>
struct GetFromMap 
{
  typedef EmptyType Second;
  typedef EmptyType First;
};

template<typename TYPE, typename H, typename T>
struct GetFromMap<TYPE, Typelist<H,T> >
: public GetFromMapInternal<TYPE, typename IsSame<typename H::First,TYPE>::Type , H, T>
{ };


//////////////////////////////////////////////////////////////////////////

template <typename TL>
struct ToTypelist;

template <typename H, typename T>
struct ToTypelist<Typelist<H,T>>
{
  typedef Typelist<H,T> PureTypelist;
};

template <typename H, typename T>
struct ToTypelist<TypelistTuple<H,T>>
{
  typedef H PureTypelist;
};

template <typename FIRST, typename SECOND>
struct PairTemplate
{
  typedef FIRST First;
  typedef SECOND Second;
};


//////////////////////////////////////////////////////////////////////////
#define TYPELIST_1(T1) Typelist<T1, EmptyType>
#define TYPELIST_2(T1, T2) Typelist<T1, TYPELIST_1(T2) >
#define TYPELIST_3(T1, T2, T3) Typelist<T1, TYPELIST_2(T2, T3) >
#define TYPELIST_4(T1, T2, T3,T4) Typelist<T1, TYPELIST_3(T2, T3, T4) >
#define TYPELIST_5(T1, T2, T3,T4,T5) Typelist<T1, TYPELIST_4(T2, T3, T4,T5) >
#define TYPELIST_6(T1, T2, T3,T4,T5,T6) Typelist<T1, TYPELIST_5(T2, T3, T4,T5,T6) >
#define TYPELIST_7(T1, T2, T3,T4,T5,T6,T7) Typelist<T1, TYPELIST_6(T2, T3, T4,T5,T6,T7) >
#define TYPELIST_8(T1, T2, T3,T4,T5,T6,T7,T8) Typelist<T1, TYPELIST_7(T2, T3, T4,T5,T6,T7,T8) >
#define TYPELIST_9(T1, T2, T3,T4,T5,T6,T7,T8,T9) Typelist<T1, TYPELIST_8(T2, T3, T4,T5,T6,T7,T8,T9) >
#define TYPELIST_10(T1, T2, T3,T4,T5,T6,T7,T8,T9,T10) Typelist<T1, TYPELIST_9(T2, T3, T4,T5,T6,T7,T8,T9,T10) >


#define TYPELIST_TUPLE_1(T1) TypelistTuple<TYPELIST_1(T1) ,std::tuple<T1> >
#define TYPELIST_TUPLE_2(T1, T2) TypelistTuple<TYPELIST_2(T1, T2) ,std::tuple<T1, T2>  >
#define TYPELIST_TUPLE_3(T1, T2, T3) TypelistTuple<TYPELIST_3(T1, T2, T3) ,std::tuple<T1, T2, T3>  >
#define TYPELIST_TUPLE_4(T1, T2, T3,T4) TypelistTuple<TYPELIST_4(T1, T2, T3, T4) ,std::tuple<T1, T2, T3, T4>  >
#define TYPELIST_TUPLE_5(T1, T2, T3,T4,T5) TypelistTuple<TYPELIST_5(T1, T2, T3, T4,T5) ,std::tuple<T1, T2, T3, T4, T5>  >
#define TYPELIST_TUPLE_6(T1, T2, T3,T4,T5,T6) TypelistTuple<TYPELIST_6(T1, T2, T3, T4,T5,T6) ,std::tuple<T1, T2, T3, T4, T5, T6> >
#define TYPELIST_TUPLE_7(T1, T2, T3,T4,T5,T6,T7) TypelistTuple<TYPELIST_7(T1, T2, T3, T4,T5,T6,T7) ,std::tuple<T1, T2, T3, T4, T5, T6, T7> >
#define TYPELIST_TUPLE_8(T1, T2, T3,T4,T5,T6,T7,T8) TypelistTuple<TYPELIST_8(T1, T2, T3, T4,T5,T6,T7,T8) ,std::tuple<T1, T2, T3, T4, T5, T6, T7,T8> >
#define TYPELIST_TUPLE_9(T1, T2, T3,T4,T5,T6,T7,T8,T9) TypelistTuple<TYPELIST_9(T1, T2, T3, T4,T5,T6,T7,T8,T9) ,std::tuple<T1, T2, T3, T4, T5, T6, T7,T8,T9> >
#define TYPELIST_TUPLE_10(T1, T2, T3,T4,T5,T6,T7,T8,T9,T10) TypelistTuple<TYPELIST_10(T1, T2, T3, T4,T5,T6,T7,T8,T9,T10) ,std::tuple<T1, T2, T3, T4, T5, T6, T7,T8,T9,T10> >


#endif // typelists_h__
