/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/

/*	include *********************************************************/
#include "config.h"


/*	using ***********************************************************/

namespace po = boost::program_options;

/*	extern **********************************************************/



/*	classes *********************************************************/

po::options_description& am3::common::Configuration::GetConfiguredMainOptions()
{
  po::options_description& od=GetNewProgramOptions("Main program options");
  od.add_options()
    ("Module", po::value<std::vector<string> >()->multitoken(), "Loaded module")
    ("ModuleCfg", po::value<std::vector<string> >()->multitoken(), "Loaded module")
    ("ModuleCfgPath", po::value<std::vector<string> >()->multitoken(), "Loaded module")
    ;
  AddToAllNewProgramOptions(od);
  return od;
}

po::options_description& am3::common::Configuration::GetConfiguredCommandLine()
{
  po::options_description& commLineOpt=GetNewProgramOptions("Command line options");
  commLineOpt.add_options()
    ("help,v", "help") //"v", not "h"; h is used by google tests
    ("mainCfg,c", po::value<string>()->default_value("am3.conf"), "main config file name")
    ("mainCfgPath,p", po::value<string>()->default_value(""), "main config file path");
  AddToAllNewProgramOptions(commLineOpt);
  return commLineOpt;
}