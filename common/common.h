/********************************************************************
	author:		Piotr Maciol
  contact:  pmaciol@agh.edu.pl
*********************************************************************/
#ifndef common_h__
#define common_h__

#include <string>

bool AlmostEqual(const double& a, const double& b);
bool AlmostZero(const double& a);

namespace am3
{
  /// Defines classes usefull in all simulations 
  namespace common
  {
    class Point3D 
    {
    public:
      double x;
      double y;
      double z;
      Point3D (double const xCoord,double const yCoord,double const zCoord):x(xCoord),y(yCoord),z(zCoord){};
    };




	std::string FileWithExt(const std::string file,const std::string ext);

  template<typename RETURN>
    void StringToFunction(std::string str, RETURN (*functionPtr)(char*))
    {
      char * strToChar = new char[str.size() + 1];
      std::copy(str.begin(), str.end(), strToChar);
      strToChar[str.size()] = '\0'; // don't forget the terminating 0
      (*functionPtr)(strToChar);
      delete[] strToChar ;
    }

    
  }
}


#endif // common_h__
