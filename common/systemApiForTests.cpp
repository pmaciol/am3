#include "systemApiForTests.h"

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#include <psapi.h>
#include <stdio.h>
#include <fstream>

#pragma comment(lib, "psapi.lib")
// #define _OPEN _open
// #define _FILE_LENGTH _filelength
// #define _CLOSE _close
// #define _APPEND _O_APPEND
#else
// #define _OPEN open
// #define _FILE_LENGTH filelength
// #define _CLOSE close
// #define _APPEND O_APPEND
#endif

am3::systemApi::WindowsApiForTests am3::systemApi::SystemApiHandlerForTests::system_api_for_tests;

am3::systemApi::SystemApiHandlerForTests::SystemApiHandlerForTests():systemApiForTests(&system_api_for_tests)
{
  //Only static member initialization
}

int am3::systemApi::WindowsApiForTests::IsProcessActive( string process_name )
{
  int process_active=-1;
  DWORD aProcesses[1024], cbNeeded, cProcesses;
  unsigned int i;
  if ( !EnumProcesses( aProcesses, sizeof(aProcesses), &cbNeeded ) )  {}
  cProcesses = cbNeeded / sizeof(DWORD);
  for ( i = 0; i < cProcesses; i++ )
  {
    if( aProcesses[i] != 0 )
    {
      TCHAR szProcessName[MAX_PATH];
      HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, aProcesses[i] );
      if (NULL != hProcess )
      {
        HMODULE hMod;
        DWORD cbNeeded;
        if ( EnumProcessModules( hProcess, &hMod, sizeof(hMod), &cbNeeded) )
        {
          GetModuleBaseName( hProcess, hMod, szProcessName, sizeof(szProcessName)/sizeof(TCHAR) );
        }
      }
      process_active*=strcmp(szProcessName,process_name.c_str());
      CloseHandle( hProcess );
    }
  }
  return process_active;
}

void am3::systemApi::WindowsApiForTests::SystemSleep( long time_in_miliseconds )
{
  Sleep((DWORD)time_in_miliseconds);
}

void am3::systemApi::WindowsApiForTests::CpFile(std::string file1,std::string file2)
{
  CopyFile(file1.c_str(), file2.c_str(), true);
}

int am3::systemApi::WindowsApiForTests::FileExists( string filename,string pathname ) const
{
	std::ifstream myfile(pathname+filename);
	int ret=false;
	if (myfile.good()) ret=true;
	myfile.close();
	return ret;
}


