/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/

/*	include *********************************************************/

#include <windows.h>
#include <iostream>
#include <fstream>
#include "systemApi.h"

/*	using ***********************************************************/


/*	definitions *********************************************************/

am3::systemApi::WindowsApi am3::systemApi::SystemApiHandler::system_api;

/*	classes *********************************************************/

int am3::systemApi::WindowsApi::AsynchronicExecute( string program_path, string program_name,string program_parameters, string working_directory ) const
{
  HINSTANCE fired_program=ShellExecute( NULL, NULL, (program_path+program_name).c_str(), program_parameters.c_str(), working_directory.c_str(), SW_MAXIMIZE );
  int a = (int) fired_program;
  return a;
}

int am3::systemApi::WindowsApi::SynchronicExecute( string program_path, string program_name,string program_parameters, string working_directory ) const
{ 
  SHELLEXECUTEINFO sei = { sizeof(sei) };
  sei.fMask = SEE_MASK_DOENVSUBST|SEE_MASK_NOCLOSEPROCESS;
  sei.nShow = SW_SHOWNORMAL; // added 27 Nov
  sei.lpVerb = "open";
  std::string str;
  str=program_path+program_name;
  LPCTSTR str2;
  str2=str.c_str();
  sei.lpFile = str2;
  sei.lpParameters = program_parameters.c_str();
  sei.lpDirectory = working_directory.c_str();
  sei.hInstApp = (HINSTANCE) SE_ERR_DDEFAIL ;
  int i=ShellExecuteEx(&sei);
  if(sei.hProcess !=NULL)
  {
    ::WaitForSingleObject(sei.hProcess, INFINITE);
    ::CloseHandle(sei.hProcess);
  }
  return i;
}

int am3::systemApi::WindowsApi::DelFile(string file_path, string file_name) const
{
  return DeleteFile(TEXT(file_name.c_str()));  
}

am3::systemApi::SystemApiHandler::SystemApiHandler():systemApi(&system_api)
{
  //Only static member initialization
}


int am3::systemApi::WindowsApi::FileExists( string filename,string pathname ) const
{
	std::ifstream myfile(pathname+filename);
	int ret=false;
	if (myfile.good()) ret=true;
	myfile.close();
	return ret;
}