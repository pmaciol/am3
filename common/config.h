/*!
 * \file config.h
 *
 * \author Piotr Maciol
 * \date 02.07.2012
 *
 *\brief boost::program_options based configuration classes for managing and deriving.
 * Configuration based on boost::program_options. 
 * Monostate class Configuration for managing of whole configuration definition
 * ConfigurableModule class have to be derivedby all classes (modules) integrated with common options interface.
 * 
 * At present, no named access to variables_maps and options_descriptions. When new vm or od is added, reference or pointer have to be stored - if you lost reference, you will lost access to vm or od.
 * It is possible, that named access will be better. Then std::vectors of vms and ods should be changed to std::maps.
 */

#ifndef common_config_h__
#define common_config_h__
/*	include *********************************************************/

#include <iostream>
#include <string>
#include <vector>
#include <boost/program_options.hpp>

/*	using ***********************************************************/

namespace am3
{
  namespace common
  {
    namespace po = boost::program_options;
    using std::cout;
    using std::string;
    using std::vector;
  }
}

/*	extern **********************************************************/
extern int*  global_argc;
extern char ***global_argv;

namespace am3
{
  namespace common
  {
    class Configuration;
    extern Configuration kProgramOptions;
  }
}
    
/*	classes *********************************************************/

namespace am3
{
  namespace common
  {

  /*!
   * \class Configuration
   *
   * \brief Monostate class for storing all options in simulation
   *
   * Monostate class for storing all options in simulation
   */
    class Configuration
    {
    public:
      /// Returns all options defined in simulation
      static po::options_description& AllOptions();
      /// Returns options given in command line
      static po::options_description& CommandLineOptions();
      /// Adds new program options description with given name
      static po::options_description& GetNewProgramOptions(const string optionDescName);
      /// Adds newly designed program options description to "All"
      ///
      /// It should be run by designer after configuration of new program options description  
      static void AddToAllNewProgramOptions(/*static*/ const po::options_description& od) {options_[0]->add(od);}
      //{options_[0]->add(*(options_[options_.size()-1]));}
      /// Presently there is only one Variable Map in simulation. Method adds new - can be used in future problems
      static po::variables_map& RegisterNewVariableMap();
      /// Used in tests. When new variable map created in gTest test, it have to be removed in the same test.
      static void RemoveLastVariableMap();
      /// Stores program options from config file
      /// \param allowUnregistered Controls boost::program_options behaviour. Default is false to allow passing arguments to gTests environment
      static int StoreProgramOptions(const string confFileName, po::options_description& options,po::variables_map& vm,bool allowUnregistered=false);
      /// Reads command line options to all program options descriptions
      static void StoreCommandLineOptions(int argc, char **argv);
      /// Reads command line options to given program options description
      static void StoreCommandLineOptions(int argc, char **argv, po::options_description& options,po::variables_map& vm);
      inline static void Print(po::options_description& opt){cout << (opt) << "\n";};
      inline static void Notify(po::variables_map& vm){po::notify(vm);};
      
      static po::variables_map& GetMainVariableMap();
      /// Initialization of program_options module
      ///
      ///After calling this method, you will get main config variable map (vms_[0], access with GetMainVariableMap().
      ///It is configured with command line options and main config file options
      static void Init();
      inline static bool IsInitialized(){return initialized_;};
      ~Configuration();
      Configuration(){initialized_=0;};
    protected:
      /// Configures main options - list of availible modules; edit to add new options in main config file
      static po::options_description& GetConfiguredMainOptions();
      /// Configures cmd line options; edit to add new options availible in cmd line
      static po::options_description& GetConfiguredCommandLine();
      static int ReadMainConfigFile();
      static string* main_conf_file_name_;
      static string* main_conf_file_path_;
      static vector<po::variables_map*> vms_;
      static vector<po::options_description*> options_;
      static bool initialized_;
    };
    /*! Implements common methods for configurable modules; have to be derived
    *
    * 
    */
    class ConfigurableModule
    {
    public:
      ConfigurableModule():module_variable_map(NULL),isConfigured_(false){}
      virtual ~ConfigurableModule();
      /*!
     * \brief boost::program_options based configuration. Run before module is used
     *
     * Method implemented for typical cases. 
     * \ref OptionsTemplateMethod have to be implemented in deriving class
     * SetModuleOptionsTemplateMethod can be overridden in specific cases.
     */
      virtual void SetModuleOptionsTemplateMethod();
      bool IsConfigured(){return isConfigured_;};
    protected:
      po::variables_map* module_variable_map;
      bool isConfigured_;
      bool IsConfigured() const {return isConfigured_;}
      //* \defgroup OptionsTemplateMethod Methods called inside ReadModuleConfigFile.*/
      /*@{*/
      int CheckIsModuleRegistered(const string moduleName);
      po::variables_map& ReadModuleConfigFileAndCmdLineOpts(const string moduleName);
      
      virtual po::options_description& DefineOptionsForModule()=0;
      virtual void CopyReadOptionsToInternal()=0; // should be =0
      virtual std::string GetModuleName()=0; // should be =0
      /*@}*/
    };
  }
}

#endif // common_config_h__
