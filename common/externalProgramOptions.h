
/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/
#ifndef externalProgramOptions_h__
#define externalProgramOptions_h__
/*	include *********************************************************/

#include <string>
#include <map>
#include <vector>

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

extern int*  global_argc;
extern char ***global_argv;

namespace am3
{
  namespace common
  {
    using std::string;

class Option
{
public:
  std::string code,value;
  Option(const string optionCode, const string optionValue=""):code(optionCode),value(optionValue){};
  std::string toString();
};

class OptionsContainer
{
public:
  string OptionsToString(const std::vector<string>& activeOptions);
  string OptionsToString();
  void AddOption(const string name, const Option opt);

protected:
  std::map<string,Option> options_;
  void AddOptionToString(const string optionName);
  string optionsToAdd_;
};

class ExtProgramOpt : public OptionsContainer
{
public:
  std::string Program_name() const;
  void Program_name(std::string val);
  std::string Program_path() const;
  void Program_path(std::string val);
  std::string Working_directory() const ;
  void Working_directory(std::string val);
  ExtProgramOpt(){};
  ExtProgramOpt(const string program_name, const string program_path, const string working_directory):program_name_(program_name), program_path_(program_path),working_directory_(working_directory)
  {

  };

protected:
  std::string working_directory_;
  std::string program_name_;
  std::string program_path_;
}; //class extProgramOpt

  }
}

#endif // externalProgramOptions_h__

