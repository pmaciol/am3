/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/

/*	include *********************************************************/

#include <string>
#include "boost/foreach.hpp"
//#include <boost/foreach.hpp>
#include "../logger/logger.h"
#include "externalProgramOptions.h"

/*	using ***********************************************************/

using std::string;

/*	extern **********************************************************/



/*	classes *********************************************************/

int*  global_argc;
char ***global_argv;

std::string am3::common::Option::toString()
{
  return (code+" "+value+" ");
}

string am3::common::OptionsContainer::OptionsToString( const std::vector<string>& activeOptions )
{
  optionsToAdd_.clear();
  BOOST_FOREACH(string optionName,activeOptions)
  {
    AddOptionToString(optionName);
  }
  return optionsToAdd_;
  optionsToAdd_.clear();
}

string am3::common::OptionsContainer::OptionsToString( )
{
  optionsToAdd_.clear();
  std::map<string, Option>::iterator it=this->options_.begin();
  //Here should be BOOST_FOREACH, bu it didn't work(?)
  while (it!=this->options_.end())
  {
    optionsToAdd_+=it->second.toString();
    it++;
  }
  return optionsToAdd_;
  optionsToAdd_.clear();
}

void am3::common::OptionsContainer::AddOptionToString( const string optionName)
{
  std::map<string,Option>::iterator it;
  it=options_.find(optionName);
  if (it!=options_.end())
  {
    optionsToAdd_+=it->second.toString();
  }
  else
  {
    string comment="Program option you asked ("+optionName+") is not defined!\n";
    am3::logger::log<am3::logger::SEV_WARNING>(am3::logger::LOG_COMMON,comment.c_str());
  }
  
}

void am3::common::OptionsContainer::AddOption(const string name, const Option opt)
{
  options_.insert(std::pair<string, Option>(name,opt));
};

std::string am3::common::ExtProgramOpt::Program_name() const { return program_name_; }
void am3::common::ExtProgramOpt::Program_name(std::string val) { program_name_ = val; }
std::string am3::common::ExtProgramOpt::Program_path() const { return program_path_; }
void am3::common::ExtProgramOpt::Program_path(std::string val) { program_path_ = val; }
std::string am3::common::ExtProgramOpt::Working_directory() const { return working_directory_; }
void am3::common::ExtProgramOpt::Working_directory(std::string val) { working_directory_ = val; }
