
/*! \file ivstorage.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef adapter_ivstorage_h__
#define adapter_ivstorage_h__
/*	include *********************************************************/

#include "../models/models.h"
#include "../models/inputTypelists.h"

/*	using ***********************************************************/

namespace mo = am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace adapter
	{

    class IvProvider
    {
    public:
      virtual std::vector<double> GetInternalVariables(mpt::Point<mpr::Coordinates2D> *point) = 0;
    };
    

    template<typename IV_PROVIDER>
    class IvInterface
    {
    public:
      virtual std::vector<double> GetElementalVariablesVector() const = 0;
      virtual void SetIvProvider(IV_PROVIDER* ivProvider) = 0;
    };


    template<typename T>
    struct VariableID;

    template <template<class, class> class HANDLER,typename IV_CONTRACT>
    class IVStorage
    {
    public:
      template<typename T>T GetPreviousState();
      template<typename T> void SetNewState(const T& newValue);
      std::vector<double> GetInternalVariables(mpt::Point<mpr::Coordinates2D> *point);
    protected:
      HANDLER<typename IV_CONTRACT::CoordinatesType, IVStorage<HANDLER,IV_CONTRACT> >* pointerToHandler_;
    };

    template <template<class, class> class HANDLER, typename COORDINATES>
    class IVStorageRedesign
    {
    public:
      template<typename T>T GetPreviousState()
      {
        T toRet;
        try
        {
          if (!pointerToHandler_) throw "pointerToHandler_ not set";
          toRet(pointerToHandler_->GetElementalVariable(VariableID<T>::id));
        }
        catch(const char* msg)
        {
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, msg);
        }
        return toRet;
      }

      template<typename T> void SetNewState(const T& newValue)
      {
        pointerToHandler_->SetElementalVariable(VariableID<T>::id);
        return toRet;
      }
      std::vector<double> GetInternalVariables(mpt::Point<mpr::Coordinates2D> *point);
      void SetPointer(HANDLER<typename COORDINATES, IVStorageRedesign<HANDLER, COORDINATES> >* ptr){ pointerToHandler_ = ptr; }
    protected:
      HANDLER<typename COORDINATES, IVStorageRedesign<HANDLER, COORDINATES> >* pointerToHandler_;
    };


  ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////

    template <template<class, class> class HANDLER, typename IV_CONTRACT>
    template<typename T>
    T IVStorage<HANDLER,IV_CONTRACT>::GetPreviousState()
      {
        T toRet;
        toRet(pointerToHandler_->GetElementalVariable(VariableID<T>::id));
        return toRet;
      }

    template <template<class, class> class HANDLER, typename IV_CONTRACT>
    template<typename T> 
    void IVStorage<HANDLER, IV_CONTRACT>::SetNewState(const T& newValue)
    {
      pointerToHandler_->SetElementalVariable(VariableID<T>::id);
      return toRet;
    }

} //adapter
} //am3
#endif // adapter_ivstorage_h__
