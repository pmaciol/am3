
/*! \file ivStorageAdapted.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3adapter_ivStorageAdapted_h__
#define am3adapter_ivStorageAdapted_h__
/*	include *********************************************************/

#include <type_traits>
#include <boost/shared_ptr.hpp>
#include "../logger/logger.h"

/*	using ***********************************************************/

namespace lo = am3::logger;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace adapter
	{
    template<typename IV_STORAGE, typename STORED_VARIABLE_CODE>
    class IVStorageAdaptedOneValue
    {
    public:
      typedef IV_STORAGE IvStorage;
      typedef STORED_VARIABLE_CODE StoredVariable;
      typedef typename STORED_VARIABLE_CODE::Type StoredVariableType;

      IVStorageAdaptedOneValue(boost::shared_ptr<IvStorage> ivPrevious) : ivPrevious_(ivPrevious){};
			template<class T, typename = std::enable_if<std::is_same<StoredVariableType,T>::value>>  StoredVariableType GetPreviousState()
 			{
				StoredVariableType toRet;
				if (ivPrevious_)  toRet(ivPrevious_->GetInternalVariable(StoredVariable::id));
				else lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Pointer to IV external storage not set");
				return toRet;
			}

			


    protected:
      boost::shared_ptr<IvStorage> ivPrevious_;
    };

    ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////
// 
//     template<typename IV_STORAGE, typename STORED_VARIABLE_CODE>
// 		template<>
// 		typename STORED_VARIABLE_CODE::Type IVStorageAdaptedOneValue<IV_STORAGE, STORED_VARIABLE_CODE>::GetPreviousState<typename STORED_VARIABLE_CODE::Type>()
//     {
//       StoredVariableType toRet;
//       if (ivPrevious_)  toRet(ivPrevious_->GetInternalVariable(StoredVariable::id));
//       else lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Pointer to IV external storage not set");
//       return toRet;
//     }

  } //adapter
} //am3
#endif // am3adapter_ivStorageAdapted_h__
