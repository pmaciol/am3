
/*! \file adapter.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef adapter_adapter_h__
#define adapter_adapter_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../common/typelists.h"
#include "../logger/logger.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../models/inputTypelists.h"
#include "../models/contracts.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace lo = am3::logger;
namespace mpt = am3::model::point;
namespace mo  = am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace adapter
  {
// 
// 		template<typename LIST, typename BUS, typename IMPL,typename POINT>
// 		struct Db2ModelList;

// 		template<typename T1, typename T2, typename BUS, typename IMPL,typename POINT>
// 		struct Db2ModelList<Typelist<T1, T2>, BUS, IMPL,POINT>
// 			: public Db2ModelList<T2, BUS, IMPL,POINT>
// 		{
// 		  using Db2ModelList<T2, BUS, IMPL,POINT>::databus_;
// 			virtual T1 GetState(POINT * point,const T1 * const typeTrait)
// 			{
// 				return static_cast<boost::shared_ptr<moc::Provider<T1,typename  POINT::CoordinatesType> > >(databus_)->GetState(point,typeTrait);
// 			};
// 		};

//     template<typename T1, typename T2, typename BUS, typename IMPL,typename POINT>
//     struct Db2ModelList<TypelistTuple<T1, T2>, BUS, IMPL,POINT>
//       : public Db2ModelList<T1, BUS, IMPL,POINT>
//     {
//     };

// 		template<typename BUS, typename IMPL,typename POINT>
// 		struct Db2ModelList<EmptyType, BUS, IMPL,POINT>
// 			: public IMPL
// 		{
// 			boost::shared_ptr<BUS> databus_;
// 		};

// 		template<typename LIST, typename BUS, typename MODEL,typename POINT>
// 		class BusModelAdapterList
// 		{
// 		protected:
// 			boost::shared_ptr<Db2ModelList<LIST, BUS,typename MODEL::Input,POINT> > db2m_;
// 		public:
// 			void Set(boost::shared_ptr<BUS> databus,boost::shared_ptr<MODEL> model)
// 			{
// 				db2m_=boost::shared_ptr<Db2ModelList<LIST, BUS,typename MODEL::Input,POINT> >(new Db2ModelList<LIST, BUS,typename MODEL::Input,POINT> );
// 				db2m_->databus_=databus;
// 				model->Set(db2m_);
// 			};
// 		};

// 		template<typename MODEL, typename BUS>
// 		struct InputsProvider : public BusModelAdapterList<typename MODEL::Input::VariablesTypes, BUS, MODEL, mpt::Point<typename MODEL::BaseCoordinates> >
// 		{};

// 		template<typename BUS, typename CONTRACT>
// 		void BindModelToBus(boost::shared_ptr<BUS> databus,boost::shared_ptr<CONTRACT> contract)
// 		{
// 			databus->Set(contract);
// 		};

		//////////////////////////////////////////////////////////////////////////

		template<typename KBS, typename LIST, typename COMMON_INTERFACE>
		class AskKbs;

    template<typename KBS, typename COMMON_INTERFACE>
		class AskKbs<KBS, EmptyType,COMMON_INTERFACE>
			: public COMMON_INTERFACE
		{
		protected:
			boost::shared_ptr<KBS> kbs_;
			std::vector<boost::shared_ptr<COMMON_INTERFACE> > models_;
		};

		template<typename KBS, typename T1, typename T2, typename COMMON_INTERFACE>
		class AskKbs<KBS, Typelist<T1, T2> ,COMMON_INTERFACE>
			: public AskKbs<KBS, T2,COMMON_INTERFACE>
		{
    public:
      T1 Get(mpt::Point<mpr::Coordinates2D> * point);
      virtual T1 GetState(mpt::Point<mpr::Coordinates2D> * point, const T1 * const typeTrait);
		protected:
		  using AskKbs<KBS, T2,COMMON_INTERFACE>::kbs_;
		  using AskKbs<KBS, T2,COMMON_INTERFACE>::models_;
		};

    
		template<typename KBS, typename T1, typename T2,  typename COMMON_INTERFACE>
		class AskKbs<KBS, TypelistTuple<T1,T2> ,COMMON_INTERFACE>
			: public AskKbs<KBS, T1,COMMON_INTERFACE>
		{
		protected:
		  using AskKbs<KBS, T1,COMMON_INTERFACE>::kbs_;
		  using AskKbs<KBS, T1,COMMON_INTERFACE>::models_;
		};

    //******************************************************************************************************************************************************************
    // Should be replaced by SwitcherStat

		template<typename KBS, typename COMMON_INTERFACE>
		class Switcher : public AskKbs<KBS, typename COMMON_INTERFACE::VariablesTypes, COMMON_INTERFACE>
		{
		public:
   		  using AskKbs<KBS, typename COMMON_INTERFACE::VariablesTypes, COMMON_INTERFACE>::kbs_;
   		  using AskKbs<KBS, typename COMMON_INTERFACE::VariablesTypes, COMMON_INTERFACE>::models_;

        Switcher(boost::shared_ptr<KBS> kbs) { kbs_ = kbs; };
			void AddModel(boost::shared_ptr<COMMON_INTERFACE> nextModel)
			{
				models_.push_back(nextModel);
			}
		};

    //******************************************************************************************************************************************************************

    template<template <typename  INPUT> class KBS, typename COMMON_INTERFACE, typename INPUT, typename INPUT_CONTRACT>
    class SwitcherStatic 
      : public AskKbs < KBS<INPUT>, typename COMMON_INTERFACE::VariablesTypes, COMMON_INTERFACE >
      , public mo::InputTypelist<INPUT>
    {
    public:
      typedef INPUT_CONTRACT Inputs;
      typedef COMMON_INTERFACE Contract;

      using AskKbs<KBS<INPUT>, typename COMMON_INTERFACE::VariablesTypes, COMMON_INTERFACE>::kbs_;
      using AskKbs<KBS<INPUT>, typename COMMON_INTERFACE::VariablesTypes, COMMON_INTERFACE>::models_;

      SwitcherStatic(boost::shared_ptr<KBS<INPUT> > kbs) { kbs_ = kbs; };
      void AddModel(boost::shared_ptr<COMMON_INTERFACE> nextModel);

		protected:
			SwitcherStatic() {};
			void SetKbs(boost::shared_ptr<KBS<INPUT> > kbs) { kbs_ = kbs; }
      
    };

    ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////


    template<typename KBS, typename T1, typename T2, typename COMMON_INTERFACE>
    T1 AskKbs<KBS, Typelist<T1, T2>, COMMON_INTERFACE>::GetState(am3::model::common::Point<mpr::Coordinates2D> * point, const T1 * const typeTrait)
    {
			lo::debugLog("Asking KBS");
        if (kbs_)
         {
					long tmp = (long)kbs_;
					lo::debugLog("Asking KBS2 "+ std::to_string(tmp));
					lo::debugLog("Checking KBS... " + std::to_string(kbs_->Check()));
//           mpr::ModelID mId = kbs_->template GetModelId<T1>(point);
					mpr::ModelID mId = kbs_->GetModelId<T1>(point); //Tu jest blad
//					mpr::ModelID mId;
//					mId(0);
					 lo::debugLog("Asking KBS3");
           if (mId() < models_.size())
           {
						 lo::debugLog("Asking KBS3");
             boost::shared_ptr<am3::model::contract::Provider<T1, am3::model::Coordinates2D> > provider = models_.at(mId());
             return provider->GetState(point, typeTrait);
           }
           else
           {
             T1 wrongModelState;
             lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE, "Wrong modelId returned by KBS");
             return wrongModelState;
           }
         } 
         else
         {
           T1 wrongModelState;
           lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE, "KBS pointer not set");
           return wrongModelState;
         }
    };


    template<typename KBS, typename T1, typename T2, typename COMMON_INTERFACE>
    T1 AskKbs<KBS, Typelist<T1, T2>, COMMON_INTERFACE>::Get(am3::model::common::Point<mpr::Coordinates2D> * point)
    {
      T1 toRet;
      return toRet;
    }


    template<template <typename  INPUT> class KBS, typename COMMON_INTERFACE, typename INPUT, typename INPUT_CONTRACT>
    void SwitcherStatic<KBS, COMMON_INTERFACE, INPUT, INPUT_CONTRACT>::AddModel(boost::shared_ptr<COMMON_INTERFACE> nextModel)
    {
      models_.push_back(nextModel);
    }

	}
}

#endif // adapter_adapter_h__
