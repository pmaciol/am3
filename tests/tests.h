/*! \file tests.h **************************************************
* \author		Piotr Maciol
* \copyright 	GNU Public License.
* \brief Header for tests
* \details
*********************************************************************/
#ifndef tests_h__
#define tests_h__
/*	include *********************************************************/
/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
	namespace tests
	{
		// Register*() functions are necessary for gtests (at least in VS2013) to see tests in cpp files
    void RegisterDeformHandler();
#ifdef MATCALC
    void RegisterSoftwareMatCalcMain();
#endif
    
     void RegisterSoftwareDeform();
     void RegisterVariableSource();
	}
}

#endif // tests_h__
