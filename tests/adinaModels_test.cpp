// /********************************************************************
// 	author:		Piotr Maciol
// 	
// 	purpose:	
// *********************************************************************/
// 
#if defined(_WIN32) || defined(_WIN64)
#define _OPEN _open
#define _FILE_LENGTH _filelength
#define _CLOSE _close
#define _APPEND _O_APPEND
#else
#define _OPEN open
#define _FILE_LENGTH filelength
#define _CLOSE close
#define _APPEND O_APPEND
#endif

/*	include *********************************************************/

#include "gtest/gtest.h"
#include <boost/fusion/container/vector.hpp>
#include <boost/fusion/include/at_c.hpp>
#include "../softwareAdina/adinaDataInterface.h"
#include "../softwareAdina/adinaFControls.h"
//#include "../prototype/adinaTest.h"
#include "../models/dataStructures.h"
//#include "globalDefs.h"

/*	using ***********************************************************/

namespace fusion=boost::fusion;

/*	extern ***********************************************************/


/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterImnAdina(){};}}

namespace am3
{
  namespace model
  {
//     using am3::common::ExtProgramOpt;
//     using am3::common::Option;

    TEST(Descriptions,modelDescriptionTest)
    {
      am3::model::AdinaFModelDescription testmodel;
      EXPECT_DOUBLE_EQ(0.0,testmodel.Output_properties().pressure());//.Value());
    }

    TEST(Descriptions,modelDescriptionSetTest)
    {
      am3::model::AdinaFModelDescription testmodel;
      //Use this one:
      testmodel.Output_properties().pressure(2.0);
      EXPECT_DOUBLE_EQ(2.0,testmodel.Output_properties().pressure());
    }

//    TEST(Descriptions,modelGetSet)
//    {
//        AdinaAdapter testmodel;
//        am3::model::common::Point p;
//        testmodel.ContractItemDispatcher<am3::model::Density>::GetState(&p);

//       //Use this one:
//       fusion::vector<Coordinates,Time> ct;
//       am3::model::Coordinates c;
//       c.x(0.5);
//       c.y(0.3);
//       c.z(0.542);
//       am3::model::Time t;
//       t(1.0);
//       fusion::at_c<0>(ct)=c;
//       fusion::at_c<1>(ct)=t;
//       am3::model::Pressure p;
//       testmodel.GetState(ct,&p);
//      FAIL();
//    }

/************************************************************************
 * ModelAdinaTests causes gTest errors. There is a problem with complex objects creation on a heap
 ************************************************************************/
/* TEST(ModelAdinaTests,InitTest)
{
    //Real tests needs private access. Maybe it is not necessary?
    am3::model::ModelAdina ma;
    EXPECT_EQ(ma.Init(),1);
	kProgramOptions.RemoveLastVariableMap();
	std::cout<<"\nThis test is probably OK. Exceptions is caused by a problem of gTest and complex objects creation on a heap\nMore details in gTest comments\n";
}

TEST(ModelAdinaTests,Binaries)
{
	am3::model::ModelAdinaTest ma;
	EXPECT_EQ(ma.Init(),1);
	am3::systemApi::WindowsApi w_api;
	EXPECT_GT(w_api.FileExists(ma.InProgramOptions().Program_name(),ma.InProgramOptions().Program_path()),0);
	EXPECT_GT(w_api.FileExists(ma.DatProgramOptions().Program_name(),ma.DatProgramOptions().Program_path()),0);
	EXPECT_GT(w_api.FileExists(ma.RunProgramOptions().Program_name(),ma.RunProgramOptions().Program_path()),0);
	kProgramOptions.RemoveLastVariableMap();
}
TEST(ModelAdinaTests,Files)
{
	am3::model::ModelAdinaTest ma;
	EXPECT_EQ(ma.Init(),1);
	am3::systemApi::WindowsApi w_api;
	EXPECT_GT(w_api.FileExists(ma.FileDetails().inFileName+"."+ma.FileDetails().inFileExt,ma.InProgramOptions().Working_directory()),0);
	EXPECT_GT(w_api.FileExists(ma.FileDetails().datFileName+"."+ma.FileDetails().datFileExt,ma.InProgramOptions().Working_directory()),0);
	EXPECT_GT(w_api.FileExists(ma.FileDetails().runFileName+"."+ma.FileDetails().runFileExt,ma.InProgramOptions().Working_directory()),0);
	kProgramOptions.RemoveLastVariableMap();
}*/
  }
}