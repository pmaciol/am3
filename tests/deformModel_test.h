
/*! \file deformModel_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef deformModel_test_h__
#define deformModel_test_h__
/*	include *********************************************************/

//#include "gtest/gtest_prod.h"
#include "../softwareDeform/configuration.h"
#include <boost/program_options.hpp>


/*	using ***********************************************************/

namespace dfm=am3::common::communication::deform;
namespace po = boost::program_options;

/*	extern **********************************************************/



/*	classes *********************************************************/

class DeformConfigurationTest : public dfm::DeformConfiguration
{
public:
  bool IsConfigured(){return DeformConfiguration::IsConfigured();}
  virtual po::options_description& DefineOptionsForModule(){return DeformConfiguration::DefineOptionsForModule();}

  virtual std::string GetModuleName(){return DeformConfiguration::GetModuleName();};

};

#endif // deformModel_test_h__
