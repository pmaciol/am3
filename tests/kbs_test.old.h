
/*! \file kbs_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef kbs_test_h__
#define kbs_test_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string.hpp>
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "../models/contracts.h"
#include "../models/contractMacros.h"
#include "../common/typelists.h"
#include "../databus/databus.h"
#include "../knowledge/kbsInterfaces.h"


#include "../models/description.h"
#include "../kbsRebit/reasoningData.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace moc = am3::model::contract;
namespace dtb = am3::databus;
namespace kno = am3::knowledge;

using am3::gsoap::VariableDTO;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace tests
{
  namespace kbs
  {

		typedef moc::Contract<TYPELIST_TUPLE_2( mpr::Temperature , mpr::EffectiveStrainRate ),mpr::Coordinates2D> MacroInputsContract2D;
		typedef moc::Contract<TYPELIST_TUPLE_1( mpr::ShearModulus ),mpr::Coordinates2D> ShearModulusContract2D;

		typedef dtb::Handler<MacroInputsContract2D> MacroInputs2DHandlers;
		typedef dtb::Handler<ShearModulusContract2D> ShearModulus2DHandlers;
		
		class DatabusForKbsTests
			: public MacroInputs2DHandlers
			, public ShearModulus2DHandlers

		{
		public:
			typedef mpr::Coordinates2D CoordinatesType;
			using ShearModulus2DHandlers::Set;
			using MacroInputs2DHandlers::Set;
		};


		class MacroInputsTest
			: public MacroInputsContract2D
		{
		public:
			mpr::Temperature temperature_;
			MacroInputsTest(){temperature_(1000.0);}
			virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Temperature *const ) {return temperature_;};
			virtual mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::EffectiveStrainRate *const ) {mpr::EffectiveStrainRate toRet;toRet(0.1);return toRet;};
		};

		class ShearModulusTest
			: public ShearModulusContract2D
		{
		public:
			ShearModulusTest(){shearModulus_(0.25);}
			mpr::ShearModulus shearModulus_;
			virtual mpr::ShearModulus GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::ShearModulus *const ) {return shearModulus_;};
		};


		class ExternalTestingKbs
		{
		public:
			typedef std::string PropertyDescription;
			void SetDataSource(kno::I4KBS<PropertyDescription> * dataSource){dataSource_=dataSource;};
			std::string GetModelId(std::string & coordinates, std::string & time, std::string & typeTrait)
			{
				std::string toReturn;
				if ((time == "0")&&(typeTrait == "ShearModulus"))
				{
					toReturn = "0";
				} 
				else if ((time == "2")&&(typeTrait == "ShearModulus"))
				{
					boost::shared_ptr<std::string> propertyToGet (new std::string("Temperature"));
					dataSource_->Fetch(propertyToGet,coordinates,time);
					if ((*propertyToGet)=="1000")
					{
						toReturn = "1";
					} 
					else
					{
						toReturn = "1000";
					}
				} 
				else
				{
					toReturn = "100";
				}
				return toReturn;
			}
		protected:
			kno::I4KBS<PropertyDescription> * dataSource_;
		};

		// To replace it with a real Rebit Client:
		// * in constructor - connect to Rebit service
		// * set 
		class MockRebitClient
		{
		public:
			typedef VariableDTO PropertyDescription;
			void SetDataSource(kno::I4KBS<PropertyDescription> * dataSource){dataSource_=dataSource;};
			VariableDTO GetModelId(VariableDTO & coordinates, VariableDTO & time, VariableDTO & typeTrait)
			{
				VariableDTO toReturn;
        toReturn.Id = new string;
        toReturn.Value = new vector<string>;

				if ((time.Value->at(0) == "0")&&(*(typeTrait.Id) == "ShearModulus"))
				{
					toReturn.Value->push_back("0");
				} 
				else if ((time.Value->at(0) == "2")&&(*(typeTrait.Id) == "ShearModulus"))
				{
					boost::shared_ptr<VariableDTO> propertyToGet (new VariableDTO);
          propertyToGet->Id = new string;
          propertyToGet->Value = new vector<string>;
					propertyToGet->Id->assign("Temperature");
					dataSource_->Fetch(propertyToGet,coordinates,time);
					if (propertyToGet->Value->at(0) == "1000")
					{
						toReturn.Value->push_back("1");
					} 
					else
					{
						toReturn.Value->push_back("1000");
					}
				} 
				else
				{
					toReturn.Value->push_back("100");
				}
				return toReturn;
			};
		protected:
			kno::I4KBS<PropertyDescription> * dataSource_;
		};


  } //kbs
} //tests
namespace am3
{
	namespace knowledge
	{
		
		//////////////////////////////////////////////////////////////////////////
		// Remark: Specializations for testing KBS with string representation. Not safe, do not use in production code!
		//////////////////////////////////////////////////////////////////////////

		template<>
		bool ConvertToKbsValue<std::string,am3::model::SimpleProperty<double,am3::model::HaveGetSetValue> >( const am3::model::SimpleProperty<double,am3::model::HaveGetSetValue>* from, std::string* to )
		{
			(*to) = boost::lexical_cast<std::string>((*from)());
			return true;
		}

		template<>
		bool ConvertToAm3Coordinates<std::string,mpr::Coordinates2D>( const std::string& from, mpr::Coordinates2D* to )
		{

			std::vector<std::string> strs;
			boost::split(strs, from, boost::is_any_of(","));
			(*to).x(boost::lexical_cast<double>(strs[0]));
			(*to).y(boost::lexical_cast<double>(strs[1]));
			return true;
		}

		template<>
		bool ConvertToAm3Time<std::string>( const std::string& from, double* to )
		{
			(*to) = boost::lexical_cast<double>(from);
			return true;
		}

		template<>
		bool ConvertToModelIdProperty<std::string>( const std::string& from, mpr::ModelID* to )
		{
			(*to)(boost::lexical_cast<unsigned int>(from));
			// should catch an exception!
			return true;
		}

		template<>
		bool ConvertToKbsTypeTrait<std::string, mpr::ShearModulus>( const mpr::ShearModulus* const from, std::string* to )
		{
			(*to) = "ShearModulus";
			return true;
		}

		template<>
		bool ConvertToKbsCoordinates<std::string,mpr::Coordinates2D>( const mpr::Coordinates2D& from, std::string* to )
		{
			(*to) = boost::lexical_cast<std::string>(from.x()) + "," + boost::lexical_cast<std::string>(from.x());
			return false;
		}

		template<>
		bool ConvertToKbsTime<std::string>( const double& from, std::string* to )
		{
			(*to) = boost::lexical_cast<std::string>(from);
			return false;
		}

		template<typename DATABUS>
		struct QueryDatabus<std::string, DATABUS>
		{
			template <typename POINT>
			bool static Query (std::string& toFill, boost::shared_ptr<DATABUS> fillFrom, POINT* point)
			{
				if (toFill == "Temperature")
				{
					mpr::Temperature temperature = boost::dynamic_pointer_cast<moc::Provider<mpr::Temperature,mpr::Coordinates2D> >(fillFrom)->GetState(point,&temperature);
					am3::model::SimpleProperty<double,am3::model::HaveGetSetValue>* ptr = &temperature;
					kno::ConvertToKbsValue(ptr,&toFill);
				}
				return false;
			}
		};

		// End of remark


		//////////////////////////////////////////////////////////////////////////
		// Remark: Specializations for testing Rebit KBS. When finised, should be moved to Rebit interface headers
		//////////////////////////////////////////////////////////////////////////

		template<>
		bool ConvertToKbsValue<VariableDTO,am3::model::SimpleProperty<double,am3::model::HaveGetSetValue> >( const am3::model::SimpleProperty<double,am3::model::HaveGetSetValue>* from, VariableDTO* to )
		{
			to->Value->clear();
			to->Value->push_back(boost::lexical_cast<std::string>((*from)()));
			return true;
		}

		template<>
		bool ConvertToAm3Coordinates<VariableDTO,mpr::Coordinates2D>( const VariableDTO& from, mpr::Coordinates2D* to )
		{
			to->x(boost::lexical_cast<double>(from.Value->at(0)));
			to->y(boost::lexical_cast<double>(from.Value->at(1)));	  
			return true;
		}
// 
		template<>
		bool ConvertToAm3Time<VariableDTO>( const VariableDTO& from, double* to )
		{
			(*to) = boost::lexical_cast<double>(from.Value->at(0));
			return true;
		}
// 
		template<>
		bool ConvertToModelIdProperty<VariableDTO>( const VariableDTO& from, mpr::ModelID* to )
		{
			(*to)(boost::lexical_cast<unsigned int>(from.Value->at(0)));
			return true;
		}
// 
		template<>
		bool ConvertToKbsTypeTrait<VariableDTO, mpr::ShearModulus>( const mpr::ShearModulus* const from, VariableDTO* to )
		{
      if (to->Id== nullptr)
      {
        to->Id = new std::string;
      }
			*(to->Id) = "ShearModulus";
			return true;
		}
// 
		template<>
		bool ConvertToKbsCoordinates<VariableDTO,mpr::Coordinates2D>( const mpr::Coordinates2D& from, VariableDTO* to )
		{

      if (to->Value == nullptr)
      {
        to->Value = new vector<string>;
      }
      to->Value->clear();
			to->Value->push_back(boost::lexical_cast<std::string>(from.x()));
			to->Value->push_back(boost::lexical_cast<std::string>(from.y()));
			return false;
		}
// 
		template<>
		bool ConvertToKbsTime<VariableDTO>( const double& from, VariableDTO* to )
		{

      if (to->Value == nullptr)
      {
        to->Value = new vector<string>;
      }
      			to->Value->clear();
			to->Value->push_back(boost::lexical_cast<std::string>(from));
			return false;
		}
// 
		template<typename DATABUS>
		struct QueryDatabus<VariableDTO, DATABUS>
		{
			template <typename POINT>
			bool static Query (VariableDTO& toFill, boost::shared_ptr<DATABUS> fillFrom, POINT* point)
			{
				if (*(toFill.Id) == "Temperature")
				{
					mpr::Temperature temperature = boost::dynamic_pointer_cast<moc::Provider<mpr::Temperature,mpr::Coordinates2D> >(fillFrom)->GetState(point,&temperature);
					am3::model::SimpleProperty<double,am3::model::HaveGetSetValue>* ptr = &temperature;
					kno::ConvertToKbsValue(ptr,&toFill);
				}
				return false;
			}
		};

		// End of remark


	} //knowledge  
} //am3


#endif // kbs_test_h__
