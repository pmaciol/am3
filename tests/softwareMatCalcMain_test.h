
/*! \file softwareMatCalcMain_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef softwareMatCalcMain_test_h__
#define softwareMatCalcMain_test_h__
/*	include *********************************************************/

#ifdef MATCALC

#include "../models/macrosSimplifiedModels.h"
#include "../models/description.h"
#include "../softwareMatCalcGeneric/contracts.h"
#include "../modelPrecipitation/contracts.h"
#include "./mockMacroModels.h"
#include "../softwareMatCalcGeneric/matCalcScriptsBased.h"
#include "../models/modelsScheme.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace smc = am3::software::matcalc;
namespace ad = am3::adapter;
namespace mpc = am3::model::precipitations;
namespace mo  = am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/




namespace tests
{
  namespace matcalc
  {
    using mpc::contract::PrecipitationsMeanRadiusContract;
    using mpc::contract::PrecipitationsBasicContract;
    using tests::MacroInputsTest;


    DATABUS_NO_STORAGE(MatCalcGenericDatabus,
      tests::MacroWithTimeContract,
      PrecipitationsMeanRadiusContract);

    DATABUS_NO_STORAGE(MatCalcTempDependentDatabus,
      TemperatureContract,
      PrecipitationsMeanRadiusContract);

    struct Output
      : public mpc::contract::PrecipitationsMeanRadiusContract
    {

    };


    template<typename CONTRACT>
    class MatCalcTestOutput
    {
    protected:
      double val1_,val2_;

    public:
      typedef CONTRACT Contract;
      void GetFromMatcalc()
      {
        MCC_GetVariableValue("returnValue1", val1_);
        MCC_GetVariableValue("returnValue2", val2_);
      }
      boost::shared_ptr<CONTRACT> ConvertToContract();

    };

    template<>
    boost::shared_ptr<mpc::contract::PrecipitationsMeanRadiusContract> tests::matcalc::MatCalcTestOutput<mpc::contract::PrecipitationsMeanRadiusContract>::ConvertToContract()
    {
      boost::shared_ptr<Output> output;
      return output;
    }

    template<typename INPUT>
    class MacroInputsTestTempl
      : public tests::MacroWithTimeContract
    {
    public:
       typedef mpc::contract::NoInput Inputs;
      typedef tests::MacroWithTimeContract VariablesTypes;
      typedef tests::MacroWithTimeContract Contract;
      typedef mpr::Coordinates2D CoordinatesType;
      mpr::Temperature temperature_;
      mpr::EffectiveStrainRate efepse_;
      MacroInputsTestTempl(){temperature_(773.15);efepse_(1.0);}  //Sherstnev
      virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Temperature *const ) {return temperature_;};
      virtual mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::EffectiveStrainRate *const ) {return efepse_;};  //Sherstnev
      virtual mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StepLength *const ) {mpr::StepLength toRet;toRet(0.001);return toRet;};
    };

	} //matcalc
} //tests

#endif
#endif // softwareMatCalcMain_test_h__
