#ifndef softwareAdina_test_h__
#define softwareAdina_test_h__

#include "gtest/gtest.h"
#include "../softwareAdina/adinaFControls.h"
#include <string>

namespace am3
{
  namespace model
  {

    class ModelFilesManagerAdinaTest:public ::testing::Test
    {
    public:
      ModelFilesManagerAdinaTest();
      ~ModelFilesManagerAdinaTest();
      ModelAdina* test_model;
    };
	class ModelAdinaTest: public ModelAdina
	{
	public:
		ExtProgramOpt InProgramOptions(){return ModelAdina::inProgramOptions_;}
		ExtProgramOpt DatProgramOptions(){return ModelAdina::datProgramOptions_;}
		ExtProgramOpt RunProgramOptions(){return ModelAdina::runProgramOptions_;}
		AdinaFilesDescription FileDetails(){return ModelAdina::fileDetails_;}

	};
  }


}

#endif // softwareAdina_test_h__
