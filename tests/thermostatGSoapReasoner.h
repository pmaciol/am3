/*! \file thermostatGSoapReasoner.h **************************************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef thermostatGSoapReasoner_h__
#define thermostatGSoapReasoner_h__

#include "../kbsRebit/reasoner.h"
#include "../kbsRebit/gSoapReasoningService.h"

namespace am3r = am3::gsoap;

namespace tests
{
	// user defined class for reasoning on "thermostat" knowledgebase
	class ThermostatGSoapReasoner : public am3r::Reasoner
	{
		public:
			// callback methods for retrieving (or dynamically calculating) 
			// values of new variables needed in inference process
			virtual std::vector<std::string> *TemplateGetVariableValue(std::string varId);

			// ctor
			ThermostatGSoapReasoner(am3r::ReasoningServiceProxy *proxy, am3r::Logger * logger) : am3r::Reasoner(proxy, logger) 
			{
        
      };

	};
}

#endif