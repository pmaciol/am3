
/*! \file mockMacroModels_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef mockMacroModels_test_h__
#define mockMacroModels_test_h__
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "mockMacroModels.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace pnt = am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterMockMacroModelsTest (){};}}

TEST(MockMacroModels, ReadFileProperly)
{
  tests::MacroDataFromFile macroDataFromFile("../../bin/tests/macroDataFromFile.dat");

  const mpr::Coordinates2D coord = mpr::Coordinates2D(0.0,0.0);
  const mpr::Time t = mpr::Time(0.0);
  pnt::ComputedPoint<mpr::Coordinates2D> point = pnt::ComputedPoint<mpr::Coordinates2D>(coord,t);

  mpr::Time actualTime;
  mpr::Time referenceTime;
  referenceTime(0.0);

  actualTime = macroDataFromFile.GetState(&point,&actualTime);
  EXPECT_DOUBLE_EQ(referenceTime(),actualTime());
}

TEST(MockMacroModels, NextStep)
{
  tests::MacroDataFromFile macroDataFromFile("../../bin/tests/macroDataFromFile.dat");

  const mpr::Coordinates2D coord = mpr::Coordinates2D(0.0,0.0);
  const mpr::Time t = mpr::Time(0.0);
  pnt::ComputedPoint<mpr::Coordinates2D> point = pnt::ComputedPoint<mpr::Coordinates2D>(coord,t);

  mpr::Time actualTime;
  mpr::Time referenceTime;
  referenceTime(0.1);

  macroDataFromFile.NextStep();

  actualTime = macroDataFromFile.GetState(&point,&actualTime);
  EXPECT_DOUBLE_EQ(referenceTime(),actualTime());
}
#endif // mockMacroModels_test_h__
