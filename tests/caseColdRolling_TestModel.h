
/*! \file caseColdRolling_TestModel.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseColdRolling_TestModel_h__
#define caseColdRolling_TestModel_h__
/*	include *********************************************************/

#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace tests
{
	namespace caseColdRolling
	{
		struct ShearModulusBySherstnevVirt
			: public ppm::contract::ShearModulusContract2D
		{
		public:
			ppm::contract::MacroInputsContract2D* bus;
			mpr::ShearModulus GetState(pnt::Point<mpr::Coordinates2D> * point, const mpr::ShearModulus *const);
		};
	}
}
#endif // caseColdRolling_TestModel_h__
