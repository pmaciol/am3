#ifndef config_test_h__
#define config_test_h__

#include "gtest/gtest.h"
#include "../common/config.h"

namespace am3
{
  namespace common
  {
    class ConfigurableModuleMock:public ConfigurableModule
    {
    protected:
      po::options_description od;
      virtual po::options_description& GetConfiguredModuleOptions(){od.add_options()("Test", po::value<string>(),"Test options"); return od;};
    public: 
//       bool TestTrue(){return (CheckConfFile("test")==1) ? (true) : (false);}
//       bool TestFalse(){return (CheckConfFile("Notest")==1) ? (true) : (false);}
//      po::variables_map& ReadModuleConfigFileTest(){return ReadModuleConfigFile("test");}


			virtual po::options_description& DefineOptionsForModule();

			virtual void CopyReadOptionsToInternal();

			virtual std::string GetModuleName();

		};
  }

}

#endif // config_test_h__