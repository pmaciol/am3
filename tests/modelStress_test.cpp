
/*! \file modelStress_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../modelStress/stressThermalOrowan.h"
#include "../modelStress/stressAthermalDislocationsEq.h"
#include "../modelStress/stressSum.h"

/*	using ***********************************************************/

namespace mst = am3::model::stress;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterModelStress (){};}}

// TEST(ACASCAC, ASDASDASd)
// {
//   mst::StressThermalOrowan test;
//   mst::StressAthermalDislocationsEq test2(0.3, 3.0, 0.286e-9);
//   mst::StressThermalAthermalTaylor test3(3.06);
//}