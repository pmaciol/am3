
/*! \file caseCreepPrecipitations_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../caseCreepPrecipitations/caseCreepPrecipitations.h"
#include "../modelPrecipitation/simpleEquations.h"
#include "mockMacroModels.h"
#include "caseCreepPrecipitations_test.h"

/*	using ***********************************************************/

namespace ccp = am3::cases::creepPrecipiation;
namespace mpc = am3::model::precipitations;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterCaseCreepPrecipitations (){};}}

TEST(PrecipitationsEquation, TemperatureInTimeModel)
{
   DATABUS_INSTANCE(mainDatabus,ccp::CaseCreepPrecipiationDatabus);
   MODEL_INSTANCE(temperatureInTime,tests::creepPrecipitations::TemperatureInTime);
   temperatureInTime->Init(1e-3,100.0,600.0);
   mpr::Time time;
   double timeDouble = (temperatureInTime->GetState(&tests::testPoint,&time)).Value();
   EXPECT_DOUBLE_EQ(1e-3,timeDouble);
   temperatureInTime->NextStep();
   timeDouble = (temperatureInTime->GetState(&tests::testPoint,&time)).Value();
   EXPECT_DOUBLE_EQ((100.001),timeDouble);
}

TEST(PrecipitationsEquation, Databus)
{
  DATABUS_INSTANCE(mainDatabus,ccp::CaseCreepPrecipiationDatabus);
  MODEL_INSTANCE(temperatureInTime,tests::creepPrecipitations::TemperatureInTime);
  temperatureInTime->Init(1e-3,100.0,600.0);

  DATABUS_ADD_MODELS(mainDatabus,temperatureInTime);
  mpr::Time time;
  boost::shared_ptr<moc::Provider<mpr::Time,mpr::Coordinates2D> > timeProv;
  timeProv = mainDatabus;
  time = timeProv->GetState(&tests::testPoint,&time);
  EXPECT_DOUBLE_EQ(1e-3,time());
  temperatureInTime->NextStep();
  time = timeProv->GetState(&tests::testPoint,&time);
  EXPECT_DOUBLE_EQ((100.001),time());
}


TEST(PrecipitationsEquation, SimplePrecipitationModel)
{
  DATABUS_INSTANCE(mainDatabus,ccp::CaseCreepPrecipiationDatabus);
  MODEL_INSTANCE(temperatureInTime,tests::creepPrecipitations::TemperatureInTime);
  temperatureInTime->Init(1e-3,100.0,600.0);
  MODEL_INSTANCE(precipitations,mpc::PrecipitationsAmountExponential);
  mpc::PrecipitationsAmountExponentialCoefficientsAbe10Cr30Mn026C coeff;
  precipitations->Init(coeff);

  DATABUS_ADD_MODELS(mainDatabus,temperatureInTime,precipitations);
  MODEL_INPUT_FROM_DATABUS(precipitations,mainDatabus);

   mpr::PrecipitationsAmount precAm;
   mpr::Time time;
   boost::shared_ptr<moc::Provider<mpr::PrecipitationsAmount,mpr::Coordinates2D> > precAmProv;
   precAmProv = mainDatabus;
   boost::shared_ptr<moc::Provider<mpr::Time,mpr::Coordinates2D> > timeProv;
   timeProv = mainDatabus;
   
   std::map<double,double> precipitationsAmounts;
   for (int i = 0;i<100;i++)
   {
     precAm = precAmProv->GetState(&tests::testPoint,&precAm);
     time = timeProv->GetState(&tests::testPoint,&time);
     temperatureInTime->NextStep();
     std::pair<double,double> toPrecAms(time(),precAm());
     precipitationsAmounts.emplace(toPrecAms);
   }
}

