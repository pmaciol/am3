/*! \file caseTrivialModels_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseTrivialModels_test_h__
#define caseTrivialModels_test_h__
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../modelTemplates/consoleInputModel.h"
#include "../models/contractMacros.h"
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "../models/inputTypelists.h"
#include "../databus/databusTypelists.h"
#include "../kbs/kbs.h"
#include "../adapter/adapter.h"
#include "../modelPolycrystalMicrostructure/gottstein.h"
#include "../caseTrivialModels/caseTrivialModels.h"

/*	using ***********************************************************/

namespace moc = am3::model::contract;
namespace mpr = am3::model::properties;
using namespace am3::databus;
namespace mo = am3::model;
namespace kbs = am3::kbs;
namespace ada = am3::adapter;
namespace mpm = am3::model::polycrystal;
using namespace am3::cases::trivialModels;
namespace ctr = am3::cases::trivialModels;

/*	extern **********************************************************/



/*	classes *********************************************************/

#ifdef CASETRIVIAL

namespace am3 { namespace tests { void RegisterTrivialModels(){}; } }



const double yieldStressCheck = 123.0;
const double yieldStressDStrainCheck = 12.30;
const double yieldStressDStrainRateCheck = 1.230;
const int nodesNumber = 4;
const double coordinates[8] = { 0, 0, 1, 0, 1, 1, 0, 1 };
const double time1 = 0;
const int elementId = 1;
const int meshId = 0;
const double time2 = 0.1;
const double coordinates2[8] = { 1, 0, 1, 1, 0, 1, 0, 0 };
const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
const mpr::Time t = mpr::Time(0.0);
mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);



TEST(TrivialModels, HardeningSwitcherTest)
{
  boost::shared_ptr<ctr::DatabusHardening> db(new ctr::DatabusHardening);
  boost::shared_ptr<kbs::DatabusRequester<ctr::DatabusHardening> >  dbr(new kbs::DatabusRequester<ctr::DatabusHardening>);
  boost::shared_ptr<ctr::Kbs> kbs(new ctr::Kbs(dbr));
  dbr->SetDatabus(db);


  typedef ModelInstanceType<ConsoleInputModelForMacroInputsContract2D, ctr::DatabusHardening>::Type MacroModel;
  boost::shared_ptr<MacroModel> macroModel(new MacroModel);
  SetModelInstance<ctr::DatabusHardening, ConsoleInputModelForMacroInputsContract2D>(db.get(), macroModel);

//   typedef ModelInstanceType<ConsoleInputModelForMacroInputsContract2D, ctr::DatabusHardening>::Type MacroModel;
//   boost::shared_ptr<MacroModel> macroModel(new MacroModel);
//   SetModelInstance<ctr::DatabusHardening, ConsoleInputModelForMacroInputsContract2D>(db.get(), macroModel);

  typedef ModelInstanceType<ctr::SwitcherYStress, ctr::DatabusHardening>::Type YieldStressSwitcher;
  boost::shared_ptr<YieldStressSwitcher> yieldStressSwitcher(new YieldStressSwitcher(kbs));
  SetModelInstance<ctr::DatabusHardening, ctr::SwitcherYStress>(db.get(), yieldStressSwitcher);

  typedef ModelInstanceType<ctr::SwitcherHardening, ctr::DatabusHardening>::Type HardeningSwitcher;
  boost::shared_ptr<HardeningSwitcher> hardeningSwitcher(new HardeningSwitcher(kbs));
  SetModelInstance<ctr::DatabusHardening, ctr::SwitcherHardening>(db.get(), hardeningSwitcher);


  boost::shared_ptr<ctr::MacroModelHand> macroModelHand(new ctr::MacroModelHand);
  boost::shared_ptr<ctr::YieldModelHand> yModelHand(new ctr::YieldModelHand);
  boost::shared_ptr<ctr::HardeningModelHand> hardeningModelHand(new ctr::HardeningModelHand);

  boost::shared_ptr<ctr::ConstantMacroModel> macroModelConst(new ctr::ConstantMacroModel(Property<mpr::Temperature>(0.1), Property<mpr::EffectiveStrainRate>(10), Property<mpr::StepLength>(100)));
  boost::shared_ptr<ctr::ConstantStressModel> yModelConst(new ctr::ConstantStressModel(Property<mpr::YieldStress>(1e6)));
  boost::shared_ptr<ctr::ConstantHardeningModel> hardeningModelConst(new ctr::ConstantHardeningModel(Property<HardeningFactor>(2.0)));

  boost::shared_ptr<ctr::TempDependentHardeningModel<ctr::DatabusHardening> > hardeningModelTemp(new ctr::TempDependentHardeningModel<ctr::DatabusHardening>);
  boost::shared_ptr<ctr::StrainTempDependentHardeningModel<ctr::DatabusHardening> > hardeningModelStrainTemp(new ctr::StrainTempDependentHardeningModel<ctr::DatabusHardening>);

  boost::shared_ptr<ctr::TempDependentStressModel<ctr::DatabusHardening> > stressModelTemp(new ctr::TempDependentStressModel<ctr::DatabusHardening>);
  boost::shared_ptr<ctr::HardeningTempDependentStressModel<ctr::DatabusHardening> > stressModelHardeningTemp(new ctr::HardeningTempDependentStressModel<ctr::DatabusHardening>);

//   yModelHand->SetModelInput(db.get());
//   macroModelHand->SetModelInput(db.get());
//  hardeningModelHand->SetModelInput(db.get());
  hardeningModelTemp->SetModelInput(db.get());
  hardeningModelStrainTemp->SetModelInput(db.get());
  stressModelTemp->SetModelInput(db.get());
  stressModelHardeningTemp->SetModelInput(db.get());

  yieldStressSwitcher->AddModel(yModelHand);
  yieldStressSwitcher->AddModel(yModelConst);
  yieldStressSwitcher->AddModel(stressModelTemp);
  yieldStressSwitcher->AddModel(stressModelHardeningTemp);

  hardeningSwitcher->AddModel(hardeningModelHand);
  hardeningSwitcher->AddModel(hardeningModelConst);
  hardeningSwitcher->AddModel(hardeningModelTemp);
  hardeningSwitcher->AddModel(hardeningModelStrainTemp);


   mpr::YieldStress ys;
//   ys = yModelHand->Get<mpr::YieldStress>(&point);
//   ys = yModelConst->Get<mpr::YieldStress>(&point);
   ys = db->GetFromDatabus<mpr::YieldStress>(&point);

//   ctr::HardeningFactor hard;
//   hard = db->GetFromDatabus<ctr::HardeningFactor>(&point);

}
#endif// (CASETRIVIAL)
#endif // caseTrivialModels_test_h__
