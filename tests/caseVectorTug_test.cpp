
/*! \file caseVectorTug_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../models/dataStructures.h"
#include "../models/macrosSimplifiedModels.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "../softwareDeformBuiltin/deformHandlerOutputsProviders.h"
#include "../caseVectorTUG/caseVectorTUG.h"
#include "../caseVectorTUG/deformAdapter.h"
#include "../caseVectorTUG/dislocationsModel.h"
#include "../caseVectorTUG/dislocationsModel_2.h"
#include "../caseVectorTUG/precipitationsModel.h"
#include "../caseVectorTUG/stressModel.h"
#include "../caseVectorTUG/internalKbs.h"
#include "caseVectorTUG_test.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace cvt = am3::cases::vectortug;
namespace mpt = am3::model::point;
namespace tsv = tests::caseVectorTug;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterCaseVectorTug (){};}}

using cvt::CaseVectorTUGDatabus;
const double tests::caseVectorTug::Consts::yieldStressCheck = 123.0;
const double tests::caseVectorTug::Consts::yieldStressDStrainCheck = 12.30;
const double tests::caseVectorTug::Consts::yieldStressDStrainRateCheck = 1.230;
const int tests::caseVectorTug::Consts::nodesNumber=4;
const double tests::caseVectorTug::Consts::coordinates[8]={0,0,1,0,1,1,0,1};
const double tests::caseVectorTug::Consts::time = 0;
const int tests::caseVectorTug::Consts::elementId = 1;
const int tests::caseVectorTug::Consts::meshId = 0;
const double tests::caseVectorTug::Consts::time2 = 0.1;
const double tests::caseVectorTug::Consts::coordinates2[8]={1,0,1,1,0,1,0,0};
const mpr::Coordinates2D tests::caseVectorTug::Consts::coord = mpr::Coordinates2D(0.5,0.5);
const mpr::Time tests::caseVectorTug::Consts::t = mpr::Time(0.0);
const double tests::caseVectorTug::Consts::temperature = 1000.0;
pnt::ComputedPoint<mpr::Coordinates2D> tests::caseVectorTug::Consts::point = pnt::ComputedPoint<mpr::Coordinates2D>(tsv::Consts::coord,tsv::Consts::t);



TEST(CaseVectorTug, DataFromDeform)
{
  DATABUS_INSTANCE(mainDatabus,CaseVectorTUGDatabus)
  DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, mainDatabus, mainDatabus,mainDatabus,mainDatabus);
  CASE_MODEL(cvt::CaseModel,caseMacroImpl);

  sdm::DeformHandlerUserProc * deformHandlerUserProc;
  deformHandlerUserProc = caseMacroImpl->GetHandler().get();
  mainDatabus->SetPreviousStepsStorage(caseMacroImpl);

  mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
  deformHandlerUserProc->SetElementalNodes(tsv::Consts::nodesNumber,tsv::Consts::coordinates,tsv::Consts::time, tsv::Consts::meshId,tsv::Consts::elementId);
  int userVariablesNumber=4;
  double userVariablesValues[4] = {1,2,3,4};	
  deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,tsv::Consts::meshId,tsv::Consts::elementId);
  deformHandlerUserProc->SetTemperature(&tsv::Consts::temperature,tsv::Consts::meshId,tsv::Consts::elementId);
 
   cvt::MacroscopicState macroState; 
   macroState = caseMacroImpl->GetState(&tsv::Consts::point,&macroState);

  EXPECT_DOUBLE_EQ(tsv::Consts::temperature,macroState().at(0));
}

TEST(CaseVectorTug,YieldStressReturnDirect)
{
  MODEL_INSTANCE(stressModel,tsv::MacroStressModelNoInput)
  DATABUS_INSTANCE(mainDatabus,CaseVectorTUGDatabus)
  DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, mainDatabus,  stressModel,stressModel,stressModel);
  CASE_MODEL(cvt::CaseModel,caseMacroImpl);

  sdm::DeformHandlerUserProc * deformHandlerUserProc;
  deformHandlerUserProc = caseMacroImpl->GetHandler().get();
  mainDatabus->SetPreviousStepsStorage(caseMacroImpl);

  mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
  deformHandlerUserProc->SetElementalNodes(tsv::Consts::nodesNumber,tsv::Consts::coordinates,tsv::Consts::time, tsv::Consts::meshId,tsv::Consts::elementId);

  EXPECT_DOUBLE_EQ(1e5,deformHandlerUserProc->GetYieldStress(tsv::Consts::meshId,tsv::Consts::elementId));
  EXPECT_DOUBLE_EQ(1e4,deformHandlerUserProc->GetYieldStressDStrain(tsv::Consts::meshId,tsv::Consts::elementId));
  EXPECT_DOUBLE_EQ(1e3,deformHandlerUserProc->GetYieldStressDStrainRate(tsv::Consts::meshId,tsv::Consts::elementId));
}

TEST(CaseVectorTug,YieldStressReturnDatabus)
{
  MODEL_INSTANCE(stressModel,tsv::MacroStressModelNoInput)
  DATABUS_INSTANCE(mainDatabus,CaseVectorTUGDatabus)
  DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, mainDatabus,  mainDatabus,mainDatabus,mainDatabus);
  CASE_MODEL(cvt::CaseModel,caseMacroImpl);
  DATABUS_ADD_MODELS(mainDatabus,stressModel)

  sdm::DeformHandlerUserProc * deformHandlerUserProc;
  deformHandlerUserProc = caseMacroImpl->GetHandler().get();
  mainDatabus->SetPreviousStepsStorage(caseMacroImpl);

  mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
  deformHandlerUserProc->SetElementalNodes(tsv::Consts::nodesNumber,tsv::Consts::coordinates,tsv::Consts::time, tsv::Consts::meshId,tsv::Consts::elementId);

  mpr::YieldStress stressForTest;
  boost::shared_ptr<moc::Provider<mpr::YieldStress,mpr::Coordinates2D> > stressProvider = mainDatabus;
  stressForTest = stressProvider->GetState(&tsv::Consts::point,&stressForTest);

  mpr::StressDStrain stressDstrainForTest;
  boost::shared_ptr<moc::Provider<mpr::StressDStrain,mpr::Coordinates2D> > stressDstrainForTestProvider = mainDatabus;
  stressDstrainForTest = stressDstrainForTestProvider->GetState(&tsv::Consts::point,&stressDstrainForTest);

  mpr::StressDStrainRate stressDstrainRateForTest;
  boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,mpr::Coordinates2D> > stressDstrainRateForTestProvider = mainDatabus;
  stressDstrainRateForTest = stressDstrainRateForTestProvider->GetState(&tsv::Consts::point,&stressDstrainRateForTest);

  EXPECT_DOUBLE_EQ(1e5,stressForTest());
  EXPECT_DOUBLE_EQ(1e4,stressDstrainForTest());
  EXPECT_DOUBLE_EQ(1e3,stressDstrainRateForTest());
}

TEST(CaseVectorTug,MacroBasedStressModel)
{
    MODEL_INSTANCE(stressModel,tsv::MacroStressModelNoInput)
  DATABUS_INSTANCE(mainDatabus,CaseVectorTUGDatabus)
  DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, mainDatabus, stressModel,stressModel,stressModel);
  CASE_MODEL(cvt::CaseModel,caseMacroImpl);

  sdm::DeformHandlerUserProc * deformHandlerUserProc;
  deformHandlerUserProc = caseMacroImpl->GetHandler().get();
  mainDatabus->SetPreviousStepsStorage(caseMacroImpl);

  mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
  deformHandlerUserProc->SetElementalNodes(tsv::Consts::nodesNumber,tsv::Consts::coordinates,tsv::Consts::time, tsv::Consts::meshId,tsv::Consts::elementId);
  
  EXPECT_DOUBLE_EQ(1e5,deformHandlerUserProc->GetYieldStress(tsv::Consts::meshId,tsv::Consts::elementId));
  EXPECT_DOUBLE_EQ(1e4,deformHandlerUserProc->GetYieldStressDStrain(tsv::Consts::meshId,tsv::Consts::elementId));
  EXPECT_DOUBLE_EQ(1e3,deformHandlerUserProc->GetYieldStressDStrainRate(tsv::Consts::meshId,tsv::Consts::elementId));
}

TEST(CaseVectorTug,MacroBasedStressModelWithInputs)
{
  using tsv::MacroStressModelWithInputs;
  using tsv::PrecipitationModelWithNoInputs;

  MODEL_INSTANCE(stressModel,tsv::MacroStressModelWithInputs)
    MODEL_INSTANCE(precipModel,PrecipitationModelWithNoInputs)


  DATABUS_INSTANCE(mainDatabus,CaseVectorTUGDatabus)
    DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, mainDatabus, stressModel,stressModel,stressModel);
  CASE_MODEL(cvt::CaseModel,caseMacroImpl);

  sdm::DeformHandlerUserProc * deformHandlerUserProc;
  deformHandlerUserProc = caseMacroImpl->GetHandler().get();
  mainDatabus->SetPreviousStepsStorage(caseMacroImpl);
  //DEFORM_ENTRY(mainDatabus,stressModel,stressModel,stressModel);
  DATABUS_ADD_MODELS(mainDatabus,stressModel, precipModel)
//  MODEL_INPUT_FROM_DATABUS(stressModel,MacroStressModelWithInputs,mainDatabus);
  MODEL_INPUT_FROM_DATABUS(stressModel,mainDatabus);

  mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
  deformHandlerUserProc->SetElementalNodes(tsv::Consts::nodesNumber,tsv::Consts::coordinates,tsv::Consts::time, tsv::Consts::meshId,tsv::Consts::elementId);
  
  EXPECT_DOUBLE_EQ(1e5*2,deformHandlerUserProc->GetYieldStress(tsv::Consts::meshId,tsv::Consts::elementId));
  EXPECT_DOUBLE_EQ(1e4*2,deformHandlerUserProc->GetYieldStressDStrain(tsv::Consts::meshId,tsv::Consts::elementId));
  EXPECT_DOUBLE_EQ(1e3*2,deformHandlerUserProc->GetYieldStressDStrainRate(tsv::Consts::meshId,tsv::Consts::elementId));
}


TEST(CaseVectorTug,PrecipitationModelReturn)
{
  using tsv::StressModelWithInputs;
  using tsv::PrecipitationModelWithNoInputs;
  
  DATABUS_INSTANCE(mainDatabus,CaseVectorTUGDatabus)
  MODEL_INSTANCE(stressModel,StressModelWithInputs)
  MODEL_INSTANCE(precipModel,PrecipitationModelWithNoInputs)
  DATABUS_ADD_MODELS(mainDatabus,stressModel, precipModel)
//  MODEL_INPUT_FROM_DATABUS(stressModel,StressModelWithInputs,mainDatabus);
  MODEL_INPUT_FROM_DATABUS(stressModel,mainDatabus);
  DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, mainDatabus, stressModel,stressModel,stressModel);
  CASE_MODEL(cvt::CaseModel,caseMacroImpl);

  sdm::DeformHandlerUserProc * deformHandlerUserProc;
  deformHandlerUserProc = caseMacroImpl->GetHandler().get();
  mainDatabus->SetPreviousStepsStorage(caseMacroImpl);

  mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
  deformHandlerUserProc->SetElementalNodes(tsv::Consts::nodesNumber,tsv::Consts::coordinates,tsv::Consts::time, tsv::Consts::meshId,tsv::Consts::elementId);
  EXPECT_DOUBLE_EQ(1e5*2,deformHandlerUserProc->GetYieldStress(tsv::Consts::meshId,tsv::Consts::elementId));
  EXPECT_DOUBLE_EQ(1e4,deformHandlerUserProc->GetYieldStressDStrain(tsv::Consts::meshId,tsv::Consts::elementId));
  EXPECT_DOUBLE_EQ(1e3,deformHandlerUserProc->GetYieldStressDStrainRate(tsv::Consts::meshId,tsv::Consts::elementId));
}

TEST(CaseVectorTug, PreviousStepInternalVariable)
{
  using tsv::StressModelWithInputs;
  using tsv::PrecipitationModelWithNoInputs;
  typedef cvt::DislocationModelWithStorage<cvt::Storage> DislocationModelWithStorage;
  typedef cvt::PrecipitationModelWithStorage<cvt::Storage> PrecipitationModelWithStorage;
  typedef cvt::StressModelWithStorage<cvt::Storage> StressModelWithStorage;

  // Makes Databus
    DATABUS_INSTANCE(mainDatabus,CaseVectorTUGDatabus)
  // Links databus output with Deform inputs (stresses and derivatives)
  DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, mainDatabus, mainDatabus,mainDatabus,mainDatabus);

    CASE_MODEL(cvt::CaseModel,caseMacroImpl);
  // Makes models (basic, defined in caseVectorTug project)
    MODEL_WITH_STORAGE_INSTANCE(stressModel,StressModelWithStorage,caseMacroImpl)
    MODEL_WITH_STORAGE_INSTANCE(dislocationModel,DislocationModelWithStorage,caseMacroImpl)
    MODEL_WITH_STORAGE_INSTANCE(precipModel,PrecipitationModelWithStorage,caseMacroImpl)
  // With adapter, Deform automatically feeds databus with macroscopic data
  //  boost::shared_ptr<cvt::DeformOutputAdapter> adapter (new cvt::DeformOutputAdapter(caseMacroImpl));
  // Links models' outputs to databus
    DATABUS_ADD_MODELS(mainDatabus,stressModel, precipModel,dislocationModel,caseMacroImpl)
  // Links databus with models' inputs
    MODEL_INPUT_FROM_DATABUS(stressModel,mainDatabus);
    MODEL_INPUT_FROM_DATABUS(dislocationModel,mainDatabus);
    MODEL_INPUT_FROM_DATABUS(precipModel,mainDatabus);

//     MODEL_INPUT_FROM_DATABUS(stressModel,StressModelWithStorage,mainDatabus);
//     MODEL_INPUT_FROM_DATABUS(dislocationModel,DislocationModelWithStorage,mainDatabus);
//     MODEL_INPUT_FROM_DATABUS(precipModel,PrecipitationModelWithStorage,mainDatabus);

  // Internal Variables instead of real Deform :
    sdm::DeformHandlerUserProc * deformHandlerUserProc;
    deformHandlerUserProc = caseMacroImpl->GetHandler().get();
    mainDatabus->SetPreviousStepsStorage(caseMacroImpl);
  mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
  deformHandlerUserProc->SetElementalNodes(tsv::Consts::nodesNumber,tsv::Consts::coordinates,tsv::Consts::time, tsv::Consts::meshId,tsv::Consts::elementId);
  double userVariablesValues[4] = {1,2,3,4};	
  deformHandlerUserProc->SetElementalVariables(4,userVariablesValues,tsv::Consts::meshId,tsv::Consts::elementId);
  
  // Macrostate instead of real Deform
  const double temperature = 893;
  const double effStrainRate = 0.1;
  const double stepLength = 0.1;
  deformHandlerUserProc->SetTemperature(&temperature,	tsv::Consts::meshId,	tsv::Consts::elementId);
  deformHandlerUserProc->SetEffectiveStrainRate(&effStrainRate,	tsv::Consts::meshId,	tsv::Consts::elementId);
  deformHandlerUserProc->SetStepLength(&stepLength,	tsv::Consts::meshId,	tsv::Consts::elementId);

  // Checking values in returned vectors
  const cvt::DislocationState* ds=nullptr;
  EXPECT_DOUBLE_EQ(1,dislocationModel->GetState(&tsv::Consts::point,ds).Value().at(0));
  EXPECT_DOUBLE_EQ(4,dislocationModel->GetState(&tsv::Consts::point,ds).Value().at(1));
  const cvt::PrecipitationState* ps=nullptr;
  EXPECT_DOUBLE_EQ(0.5,precipModel->GetState(&tsv::Consts::point,ps).Value().at(0));
  EXPECT_DOUBLE_EQ(1,precipModel->GetState(&tsv::Consts::point,ps).Value().at(1));
  const mpr::YieldStress* ss=nullptr;
  EXPECT_DOUBLE_EQ(1e10,stressModel->GetState(&tsv::Consts::point,ss).Value());
}

TEST(CaseVectorTug, SwitcherTest)
{
  using tsv::StressModelWithInputs;
  using tsv::PrecipitationModelWithNoInputs;
  typedef cvt::DislocationModelWithStorage<cvt::Storage> DislocationModelWithStorage;
  typedef cvt::DislocationModelWithStorage_2<cvt::Storage> DislocationModelWithStorage_2;
  typedef cvt::PrecipitationModelWithStorage<cvt::Storage> PrecipitationModelWithStorage;
  typedef cvt::StressModelWithStorage<cvt::Storage> StressModelWithStorage;

  // Makes Databus
  DATABUS_INSTANCE(mainDatabus,CaseVectorTUGDatabus)
    // Links databus output with Deform inputs (stresses and derivatives)
    DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, mainDatabus, mainDatabus,mainDatabus,mainDatabus);

  CASE_MODEL(cvt::CaseModel,caseMacroImpl);
    // Makes models (basic, defined in caseVectorTug project)
    MODEL_WITH_STORAGE_INSTANCE(stressModel,StressModelWithStorage,caseMacroImpl)
    MODEL_WITH_STORAGE_INSTANCE(dislocationModel,DislocationModelWithStorage,caseMacroImpl)
    MODEL_WITH_STORAGE_INSTANCE(dislocationModel_2,DislocationModelWithStorage_2,caseMacroImpl)
    MODEL_WITH_STORAGE_INSTANCE(precipModel,PrecipitationModelWithStorage,caseMacroImpl)
    // With adapter, Deform automatically feeds databus with macroscopic data
  //  boost::shared_ptr<cvt::DeformOutputAdapter> adapter (new cvt::DeformOutputAdapter(deformHandlerPtr));

    KBS_INSTANCE(kbs,cvt::Kbs,mainDatabus);
    SWITCHER_INSTANCE(dislocationSwitcher,cvt::DislocationStateContract,kbs);
    ADD_MODELS_TO_SWITCHER(dislocationSwitcher,dislocationModel,dislocationModel_2);


  // Links models' outputs to databus
  DATABUS_ADD_MODELS(mainDatabus,stressModel, precipModel,dislocationSwitcher,caseMacroImpl)
  // Links databus with models' inputs
  MODEL_INPUT_FROM_DATABUS(stressModel,mainDatabus);
  MODEL_INPUT_FROM_DATABUS(dislocationModel,mainDatabus);
  MODEL_INPUT_FROM_DATABUS(dislocationModel_2,mainDatabus);
  MODEL_INPUT_FROM_DATABUS(precipModel,mainDatabus);

//   MODEL_INPUT_FROM_DATABUS(stressModel,StressModelWithStorage,mainDatabus);
//   MODEL_INPUT_FROM_DATABUS(dislocationModel,DislocationModelWithStorage,mainDatabus);
//   MODEL_INPUT_FROM_DATABUS(dislocationModel_2,DislocationModelWithStorage_2,mainDatabus);
//   MODEL_INPUT_FROM_DATABUS(precipModel,PrecipitationModelWithStorage,mainDatabus);
  // Internal Variables instead of real Deform :
  sdm::DeformHandlerUserProc * deformHandlerUserProc;
  deformHandlerUserProc = caseMacroImpl->GetHandler().get();
  mainDatabus->SetPreviousStepsStorage(caseMacroImpl);
  mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
  deformHandlerUserProc->SetElementalNodes(tsv::Consts::nodesNumber,tsv::Consts::coordinates,tsv::Consts::time, tsv::Consts::meshId,tsv::Consts::elementId);
  double userVariablesValues[4] = {1,2,3,4};	
  deformHandlerUserProc->SetElementalVariables(4,userVariablesValues,tsv::Consts::meshId,tsv::Consts::elementId);

  // Macrostate instead of real Deform
  double temperature = 793;
  double effStrainRate = 0.1;
  double stepLength = 0.1;
  deformHandlerUserProc->SetTemperature(&temperature,	tsv::Consts::meshId,	tsv::Consts::elementId);
  deformHandlerUserProc->SetEffectiveStrainRate(&effStrainRate,	tsv::Consts::meshId,	tsv::Consts::elementId);
  deformHandlerUserProc->SetStepLength(&stepLength,	tsv::Consts::meshId,	tsv::Consts::elementId);

  // Checking values in returned vectors
  const cvt::DislocationState* ds=nullptr;
  EXPECT_DOUBLE_EQ(20,dislocationSwitcher->GetState(&tsv::Consts::point,ds).Value().at(0));
  EXPECT_DOUBLE_EQ(40,dislocationSwitcher->GetState(&tsv::Consts::point,ds).Value().at(1));
  const cvt::PrecipitationState* ps=nullptr;
  EXPECT_DOUBLE_EQ(0.5,precipModel->GetState(&tsv::Consts::point,ps).Value().at(0));
  EXPECT_DOUBLE_EQ(1,precipModel->GetState(&tsv::Consts::point,ps).Value().at(1));
}