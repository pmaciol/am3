
/*! \file mainForProvider.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef mainForProvider_h__
#define mainForProvider_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "caseColdRolling_test.h"
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../modelPolycrystalMicrostructure/gottstein.h"

#include "../databus/adapter.h"
#include "../databus/databusTypelists.h"
#include <iostream>
#include <windows.h>
/*	using ***********************************************************/

namespace mpt = am3::model::point;
namespace ppm = am3::problem::polycrystal;
namespace ccr = am3::cases::coldRolling;
namespace ada = am3::adapter;
namespace sdm = am3::software::deform;
namespace tsc = tests::caseColdRolling;
namespace mpm = am3::model::polycrystal;


using tests::caseColdRolling::Consts;


/*	extern **********************************************************/



/*	classes *********************************************************/

namespace testProvider
{
	
  mpr::Coordinates2D* coord;
  mpr::Time time;
  mpt::ComputedPoint<mpr::Coordinates2D>* point;

mpr::ShearModulus m;
mpr::Temperature t;
ppm::contract::SelfDiffusion sd;

boost::shared_ptr<moc::Provider<ppm::contract::SelfDiffusion,mpr::Coordinates2D> > selfDiffusionProvider;
boost::shared_ptr<moc::Provider<mpr::ShearModulus,mpr::Coordinates2D> > shearModulusProvider;
boost::shared_ptr<moc::Provider<mpr::Temperature,mpr::Coordinates2D> > temperatureProvider;

const unsigned steps = 2e7;
//const unsigned steps = 5e5;
const unsigned multipV = 1;
const unsigned multipG = 1;

void Init()
{
  
  coord = new mpr::Coordinates2D(0.5,0.5);
  time(0.1);
  point = new mpt::ComputedPoint<mpr::Coordinates2D>(*coord,time);
}
void Virtual()
{
  t = temperatureProvider->GetState(point,&t);
  sd = selfDiffusionProvider->GetState(point,&sd);
  m = shearModulusProvider->GetState(point,&m);
//  std::cout<<"Virtual: "<<m()<<std::endl;
}

void VirtualInit()
{
  boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
  boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
  boost::shared_ptr<mpm::ShearModulusBySherstnev> shearModulusModel (new mpm::ShearModulusBySherstnev);
  boost::shared_ptr<mpm::DiffusionBySherstnev> diffusionModel (new mpm::DiffusionBySherstnev);

  ada::BindModelToBus(testDatabus,macroModel);
  ada::BindModelToBus(testDatabus,shearModulusModel );
  ada::BindModelToBus(testDatabus,diffusionModel );

  ada::BusModelAdapterList<mpm::ShearModulusBySherstnevInput::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::ShearModulusBySherstnev,pnt::Point<mpr::Coordinates2D> > busModelAdapterShearModulus;
  busModelAdapterShearModulus.Set(testDatabus,shearModulusModel );
  ada::BusModelAdapterList<mpm::DiffusionBySherstnevInput::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::DiffusionBySherstnev,pnt::Point<mpr::Coordinates2D> > busModelAdapterDiffusion;
  busModelAdapterDiffusion.Set(testDatabus,diffusionModel);

  ppm::contract::SelfDiffusion selfDiffusionToCheck;
  selfDiffusionProvider = testDatabus;

  mpr::ShearModulus shearModulusToCheck;
  shearModulusProvider = testDatabus;

  mpr::Temperature temperature;
  temperatureProvider = testDatabus;

  for (unsigned int i = 0;i<steps*multipV;i++)
  {
    Virtual();
  }
  
 // std::cout<<"AfterVirtual"<<std::endl;
}

template<typename DB>
void Generic(DB* tdb)
{
  m = db::GetFromDatabus<mpr::ShearModulus>(tdb,point);
  t = db::GetFromDatabus<mpr::Temperature>(tdb,point);
  sd = db::GetFromDatabus<ppm::contract::SelfDiffusion>(tdb,point);
//  std::cout<<"Generic: "<<m()<<std::endl;
}


void GenericInit()
{
  using namespace am3::databus;

  using tsc::MacroInputsTestTempl;

  typedef TEMPLATELIST_3( mpm::DiffusionBySherstnevTemp, mpm::ShearModulusBySherstnevTemp , MacroInputsTestTempl) ThreeModelsTemplatesList;
  typedef DatabusTypelists<ThreeModelsTemplatesList> ThreeDatabus;

  ThreeDatabus tdb;

  // Add MacroInputsTestTempl instance
  typedef ModelInstanceType<MacroInputsTestTempl,ThreeDatabus>::Type MacroModel;
  // The line below cannot be included in template, due to undetermined constructors
  boost::shared_ptr<MacroModel> macroModel = boost::shared_ptr<MacroModel>(new MacroModel);
  SetModelInstance<ThreeDatabus,MacroInputsTestTempl>(&tdb,macroModel);

  // Add ShearModulusBySherstnevTemp instance
  typedef ModelInstanceType<mpm::ShearModulusBySherstnevTemp,ThreeDatabus>::Type ShearModulusModel;
  // The line below cannot be included in template, due to undetermined constructors
  boost::shared_ptr<ShearModulusModel> smModel = boost::shared_ptr<ShearModulusModel>(new ShearModulusModel);
  SetModelInstance<ThreeDatabus,mpm::ShearModulusBySherstnevTemp>(&tdb,smModel);

  typedef ModelInstanceType<mpm::DiffusionBySherstnevTemp,ThreeDatabus>::Type DiffusionModel;
  boost::shared_ptr<DiffusionModel> difModel = boost::shared_ptr<DiffusionModel>(new DiffusionModel);
  SetModelInstance<ThreeDatabus,mpm::DiffusionBySherstnevTemp>(&tdb,difModel);

  for (unsigned int i = 0;i<steps * multipG;i++)
  {
    testProvider::Generic(&tdb);
  }
  
//  std::cout<<"AfterGeneric"<<std::endl;
}

void FullVirtual()
{
  t = temperatureProvider->GetState(point,&t);
  sd = selfDiffusionProvider->GetState(point,&sd);
  m = shearModulusProvider->GetState(point,&m);
  //  std::cout<<"Virtual: "<<m()<<std::endl;
}

void FullVirtualInit()
{
  boost::shared_ptr<tsc::TestVirtualDatabus> db (new tsc::TestVirtualDatabus);
  //   boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
  boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
  boost::shared_ptr<mpm::ShearModulusBySherstnevVirt> shearModulusModel (new mpm::ShearModulusBySherstnevVirt);
  boost::shared_ptr<mpm::DiffusionBySherstnevVirt> diffusionModel (new mpm::DiffusionBySherstnevVirt);

  db->DiffPtr(diffusionModel.get());
  db->SmPtr(shearModulusModel.get());
  db->MacrPtr(macroModel.get());

  shearModulusModel->bus = db.get();
  diffusionModel->bus = db.get();

  
  ppm::contract::SelfDiffusion selfDiffusionToCheck;
  selfDiffusionProvider = db;

  mpr::ShearModulus shearModulusToCheck;
  shearModulusProvider = db;

  mpr::Temperature temperature;
  temperatureProvider = db;

  for (unsigned int i = 0;i<steps*multipV;i++)
  {
    FullVirtual();
  }

  //std::cout<<"AfterFullVirtual"<<std::endl;
};

struct Diffs
{
  unsigned time,difVirt, difGeneric, difFullVirt;
};

unsigned timeDiff(SYSTEMTIME& tstart, SYSTEMTIME& tend)
{
  unsigned timeStart = tstart.wSecond * 1000 + tstart.wMilliseconds;
  unsigned timeEnd = (tend.wMinute - tstart.wMinute) * 1000 * 60 + tend.wSecond * 1000 + tend.wMilliseconds;
  return timeEnd - timeStart;
};
} //testProvider
#endif // mainForProvider_h__
