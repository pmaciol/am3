
/*! \file modelPolycrystalMicrostructure.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelPolycrystalMicrostructure_h__
#define modelPolycrystalMicrostructure_h__
/*	include *********************************************************/

#include "../models/models.h"
#include "../models/dataStructures.h"
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "../caseColdRolling/databus.h"
#include "../caseColdRolling/caseColdRolling.h"

#include "../softwareDeformBuiltin/deformHandler.h"
namespace sdm = am3::software::deform;

/*	using ***********************************************************/

namespace mo = am3::model;
namespace mpr = am3::model::properties;
namespace ppm = am3::problem::polycrystal;
namespace sdm = am3::software::deform;
namespace ccr = am3::cases::coldRolling;

/*	extern **********************************************************/

typedef moc::Contract<TYPELIST_8(	
  ppm::contract::DislDensityMobile,
  ppm::contract::SubgrainSize,
  ppm::contract::StoredEnergy,
  ppm::contract::GrainSize,
  ppm::contract::NumberOfRecrystalizedNuclei,
  ppm::contract::DislDensityMobileDeformed,
  ppm::contract::RecrystalizedGrainSize,
  ppm::contract::VolumeFractionOfRecrystallizedGrains
  ),mpr::Coordinates2D> DeformInput2D;
typedef am3::software::deform::DeformHandler<mpr::Coordinates2D>  ActualDeformHandler;

/*	classes *********************************************************/

namespace tests
{
	struct Consts
	{
		static const double yieldStressCheck;
		static const double yieldStressDStrainCheck;
		static const double yieldStressDStrainRateCheck;
		static const int nodesNumber;
		static const double coordinates[8],coordinates2[8];
		static const double time,time2;
		static const int elementId;
		static const int meshId;
	};


	//////////////////////////////////////////////////////////////////////////


	class GrainsTest 
		: public ppm::contract::GrainsContract2D
	{

	public:
		typedef ppm::contract::GrainsContract2D Contract;
		
		virtual ppm::contract::GrainSize GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::GrainSize *const ) {ppm::contract::GrainSize toRet;toRet(50e-6);return toRet;};  //Sherstnev
		virtual ppm::contract::RecrystalizedGrainSize GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::RecrystalizedGrainSize *const ) {ppm::contract::RecrystalizedGrainSize toRet;toRet(25e-6);return toRet;};
		virtual ppm::contract::NumberOfRecrystalizedNuclei GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::NumberOfRecrystalizedNuclei *const ) {ppm::contract::NumberOfRecrystalizedNuclei toRet;toRet(2e15);return toRet;};
		virtual ppm::contract::VolumeFractionOfRecrystallizedGrains GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::VolumeFractionOfRecrystallizedGrains *const ) {ppm::contract::VolumeFractionOfRecrystallizedGrains toRet;toRet(0.5);return toRet;};  //Sherstnev
		virtual ppm::contract::StoredEnergy GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::StoredEnergy *const ) {ppm::contract::StoredEnergy toRet;toRet(20e3);return toRet;};  //Sherstnev
	};

	class DislocationTest
		: public ppm::contract::DislocationsContract2D
	{
	public:
		virtual ppm::contract::DislDensityImmobileInteriors GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityImmobileInteriors *const ) {ppm::contract::DislDensityImmobileInteriors toRet;toRet(0.1);return toRet;};
		virtual ppm::contract::DislDensityImmobileWalls GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityImmobileWalls *const ) {ppm::contract::DislDensityImmobileWalls toRet;toRet(0.1);return toRet;};
		virtual ppm::contract::DislDensityMobile GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityMobile *const ) {ppm::contract::DislDensityMobile toRet;toRet(0.1);return toRet;};
		virtual ppm::contract::DislDensityImmobileInteriorsDeformed GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityImmobileInteriorsDeformed *const ) {ppm::contract::DislDensityImmobileInteriorsDeformed toRet;toRet(0.1);return toRet;};
		virtual ppm::contract::DislDensityImmobileWallsDeformed GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityImmobileWallsDeformed *const ) {ppm::contract::DislDensityImmobileWallsDeformed toRet;toRet(0.1);return toRet;};
		virtual ppm::contract::DislDensityMobileDeformed GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityMobileDeformed *const ) {ppm::contract::DislDensityMobileDeformed toRet;toRet(0.1);return toRet;};
		virtual ppm::contract::SubgrainSize GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::SubgrainSize *const ) {ppm::contract::SubgrainSize toRet;toRet(0.1);return toRet;};
	};

	class DiffusionTest
		: public ppm::contract::DiffusionContract2D
	{
	public:
		virtual ppm::contract::SelfDiffusion GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::SelfDiffusion *const ) {ppm::contract::SelfDiffusion toRet;toRet(0.1);return toRet;};
		virtual ppm::contract::GrainBoundaryDiffusion GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::GrainBoundaryDiffusion *const ) {ppm::contract::GrainBoundaryDiffusion toRet;toRet(0.1);return toRet;};
	};

	class MacroInputsTest
		: public ppm::contract::MacroInputsContract2D
	{
	public:
		mpr::Temperature temperature_;
		MacroInputsTest(){temperature_(1000.0);}
		virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Temperature *const ) {return temperature_;};
		virtual mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::EffectiveStrainRate *const ) {mpr::EffectiveStrainRate toRet;toRet(0.1);return toRet;};
		virtual mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StepLength *const ) {mpr::StepLength toRet;toRet(0.1);return toRet;};
	};

	class YieldStressTest
		: public ppm::contract::YieldStressContract2D
	{
	public:
		virtual mpr::YieldStress GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::YieldStress *const ) {mpr::YieldStress toRet;toRet(0.1);return toRet;};
	};

	class ShearModulusTest
		: public ppm::contract::ShearModulusContract2D
	{
	public:
		ShearModulusTest(){shearModulus_(0.25);}
		mpr::ShearModulus shearModulus_;
		virtual mpr::ShearModulus GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::ShearModulus *const ) {return shearModulus_;};
	};

	class ZenerParamTest
		: public ppm::contract::ZenerContract2D
	{
	public:
		virtual ppm::contract::ZenerParam GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::ZenerParam *const ) {ppm::contract::ZenerParam toRet;toRet(0.1);return toRet;};
	};

	typedef moc::Contract<TYPELIST_1( mpr::Temperature ),mpr::Coordinates2D> ShearModulusTemperatureDependentTestInput;

	class ShearModulusTemperatureDependentTest
		: public ppm::contract::ShearModulusContract2D
		, public mo::Inputs<ShearModulusTemperatureDependentTestInput>
	{
	public:
		virtual mpr::ShearModulus GetState(mpt::Point<mpr::Coordinates2D> *point,const mpr::ShearModulus *const )
			{
				mpr::ShearModulus toRet;
				mpr::Temperature temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point);

				toRet(0.1 * temperature.Value());
				return toRet;
		};
	};
	class Kbs
	{
	public:
		template <typename VARIABLE> mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point)
		{
			const VARIABLE* trait=nullptr;
			return GetModelId(point,trait);
		}
		Kbs(boost::shared_ptr<ccr::DatabusForGottsteinDeform> databus): databus_(databus){};
	protected:
		boost::shared_ptr<ccr::DatabusForGottsteinDeform> databus_;
		mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const mpr::ShearModulus* const)
		{
			boost::shared_ptr<moc::Provider<mpr::Temperature,mpr::Coordinates2D> > tempProvider;
			tempProvider = databus_;
			mpr::Temperature temperature = tempProvider->GetState(point,&temperature);
			mpr::ModelID mId;
			if (temperature() > 750.0 )
			{
				mId(0);
			} 
			else
			{
				mId(1);
			}
			return mId;
		}
	
  };

  class ConstantYieldStress
    : public sdm::DeformStressContract2D
    , public ppm::contract::YieldStressContract2D
  {
  public:
    ConstantYieldStress(const double yieldStress,const double yieldStressDstrain,const double yieldStressDstrainRate){ys(yieldStress);sdstr(yieldStressDstrain);sdstrRate(yieldStressDstrainRate);};
    virtual mpr::YieldStress GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::YieldStress* const)
    {
      return ys;
    };
    virtual mpr::StressDStrain GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrain* const)
    {
      return sdstr;
    };
    virtual mpr::StressDStrainRate GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrainRate* const)
    {
      return sdstrRate;
    };
  protected:
    mpr::YieldStress ys;
    mpr::StressDStrain sdstr;
    mpr::StressDStrainRate sdstrRate;
  };

  // template <typename HANDLER>
  class IVStorage
    : public mo::Inputs<DeformInput2D>
  {
  public:
    using mo::Inputs<DeformInput2D>::Set;
	  template<typename T>T GetPreviousState();
	  template<> ppm::contract::DislDensityMobile GetPreviousState() //Dislocations
	  {
		  ppm::contract::DislDensityMobile toRet;
		  //toRet(pointerToHandler_->elementalVariables_[0]);
      toRet(pointerToHandler_->GetElementalVariable(0));
		  return toRet;
	  }
	  template<> ppm::contract::SubgrainSize GetPreviousState()
	  {
		  ppm::contract::SubgrainSize toRet;
//		  toRet(elementalVariables_[1]);
		  return toRet;
	  }
	  template<> ppm::contract::StoredEnergy GetPreviousState() // Grains
	  {
		  ppm::contract::StoredEnergy toRet;
//		  toRet(elementalVariables_[2]);
		  return toRet;
	  }
	  template<> ppm::contract::GrainSize GetPreviousState() // Grains
	  {
		  ppm::contract::GrainSize toRet;
//		  toRet(elementalVariables_[3]);
		  return toRet;
	  }
	  template<> ppm::contract::NumberOfRecrystalizedNuclei GetPreviousState() //GRains
	  {
		  ppm::contract::NumberOfRecrystalizedNuclei toRet;
//		  toRet(elementalVariables_[4]);
		  return toRet;
	  }
	  template<> ppm::contract::DislDensityMobileDeformed GetPreviousState() 
	  {
		  ppm::contract::DislDensityMobileDeformed toRet;
//		  toRet(elementalVariables_[5]);
		  return toRet;
	  }
	  template<> ppm::contract::RecrystalizedGrainSize GetPreviousState() //GRains
	  {
		  ppm::contract::RecrystalizedGrainSize toRet;
//		  toRet(elementalVariables_[6]);
		  return toRet;
	  }
	  template<> ppm::contract::VolumeFractionOfRecrystallizedGrains GetPreviousState() //GRains
	  {
		  ppm::contract::VolumeFractionOfRecrystallizedGrains toRet;
//		  toRet(elementalVariables_[7]);
		  return toRet;
	  }
	  std::vector<double> GetInternalVariables(mpt::Point<mpr::Coordinates2D> *point) 
	  {
		  std::vector<double> toRet; 
		  toRet.push_back(Get<ppm::contract::DislDensityMobile,mpr::Coordinates2D>::InPoint(point).Value());
		  toRet.push_back(Get<ppm::contract::SubgrainSize,mpr::Coordinates2D>::InPoint(point).Value());
		toRet.push_back(Get<ppm::contract::StoredEnergy,mpr::Coordinates2D>::InPoint(point).Value());
			  toRet.push_back(Get<ppm::contract::GrainSize,mpr::Coordinates2D>::InPoint(point).Value());
			  toRet.push_back(Get<ppm::contract::NumberOfRecrystalizedNuclei,mpr::Coordinates2D>::InPoint(point).Value());
			  toRet.push_back(Get<ppm::contract::DislDensityMobileDeformed,mpr::Coordinates2D>::InPoint(point).Value());
			  toRet.push_back(Get<ppm::contract::RecrystalizedGrainSize,mpr::Coordinates2D>::InPoint(point).Value());
			  toRet.push_back(Get<ppm::contract::VolumeFractionOfRecrystallizedGrains,mpr::Coordinates2D>::InPoint(point).Value());
		  return toRet;
	  }
  protected:
    sdm::DeformHandler<mpr::Coordinates2D,IVStorage>* pointerToHandler_;
  };

  template<typename HANDLER>
  class MacroModel : public ppm::contract::MacroInputsContract2D
  {
  public:
    void SetDeformHandler(HANDLER* pointerToDeform)
    {
      pointerToDeform_ = pointerToDeform;
    };
    mpr::EffectiveStrainRate GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::EffectiveStrainRate *const )
    {
      return pointerToDeform_->GetEffectiveStrainRateIn(*point);
    }
    mpr::Temperature GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::Temperature *const )
    {
      return pointerToDeform_->GetTemperatureIn(*point);
    }
    mpr::StepLength GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::StepLength *const )
    {
      return pointerToDeform_->GetStepLength();
    }
  protected:
    HANDLER* pointerToDeform_;
  };


  class CaseMacroImplementation
    : public MacroModel<sdm::DeformHandler<mpr::Coordinates2D> >
    , public IVStorage
  {
  public:
	  
	  CaseMacroImplementation(boost::shared_ptr<sdm::DeformStressContract2D > yieldStressModel) 
	  {		  handler_.reset(
			  new sdm::DeformHandler<mpr::Coordinates2D,IVStorage>
			  (yieldStressModel,this)
 			  );
	  pointerToHandler_ = handler_.get();
	  };
	  CaseMacroImplementation(boost::shared_ptr<moc::Provider<mpr::YieldStress,mpr::Coordinates2D> > yieldStressModel,
		  boost::shared_ptr<moc::Provider<mpr::StressDStrain,mpr::Coordinates2D> > yieldStressStrainModel,
		  boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,mpr::Coordinates2D> > yieldStressStrainRateModel)

	  {		  handler_.reset(
	  new sdm::DeformHandler<mpr::Coordinates2D,IVStorage>
	  (yieldStressModel,yieldStressStrainModel,yieldStressStrainRateModel,this)
	  );
	  pointerToHandler_ = handler_.get();
	  };
	  boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D,IVStorage > > GetHandler() const {return handler_;};
  protected:
	  //boost::shared_ptr<IVUpdater> ivUpdater_;
    boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D,IVStorage> > handler_;
  };

  class DeformHandlerToColdRollingAdapter : public ppm::contract::MacroInputsContract2D
  {
  public:
	  void SetDeformHandler(am3::software::deform::DeformHandler<mpr::Coordinates2D,IVStorage>* pointerToDeform)
	  {
		  pointerToDeform_ = pointerToDeform;
	  };
	  mpr::EffectiveStrainRate GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::EffectiveStrainRate *const )
	  {
		  return pointerToDeform_->GetEffectiveStrainRateIn(*point);
	  }
	  mpr::Temperature GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::Temperature *const )
	  {
		  return pointerToDeform_->GetTemperatureIn(*point);
	  }
	  mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StepLength *const ) 
	  {
		  return pointerToDeform_->GetStepLength();
	  };
  protected:
	  am3::software::deform::DeformHandler<mpr::Coordinates2D,IVStorage>* pointerToDeform_;
  };



}
#endif // modelPolycrystalMicrostructure_h__

