
/*! \file caseVectorTUG.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseVectorTUG_test_h__
#define caseVectorTUG_test_h__
/*	include *********************************************************/

#include "../caseVectorTUG/caseVectorTUG.h"
#include "../models/dataStructures.h"
#include "../models/models.h"
#include "../models/macrosSimplifiedModels.h"

/*	using ***********************************************************/

namespace cvt = am3::cases::vectortug;
namespace mpr = am3::model::properties;
namespace mo  = am3::model;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace tests
{
  namespace caseVectorTug
  {
    struct Consts
    {
      static const double yieldStressCheck;
      static const double yieldStressDStrainCheck;
      static const double yieldStressDStrainRateCheck;
      static const int nodesNumber;
      static const double coordinates[8],coordinates2[8];
      static const double time,time2;
      static const int elementId;
      static const int meshId;
      static const mpr::Coordinates2D coord;
      static const mpr::Time t;
      static const double temperature;
      static pnt::ComputedPoint<mpr::Coordinates2D> point;
    };

    // Simplest models for testing
    class StressModelNoInput : public cvt::DeformStressContractRequest
    {
    public:
      mpr::YieldStress GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::YieldStress *const )
      {
        mpr::YieldStress toRet;
        toRet(1e5);
        return toRet;
      }

      mpr::StressDStrain GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::StressDStrain *const )
      {
        mpr::StressDStrain toRet;
        toRet(1e4);
        return toRet;
      }

      mpr::StressDStrainRate GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::StressDStrainRate *const )
      {
        mpr::StressDStrainRate toRet;
        toRet(1e3);
        return toRet;
      }
    };

    class StressModelWithInputs
        : public cvt::DeformStressContractRequest
        , public mo::Inputs<cvt::StressModelInputs>
      {
      public:
        StressModelWithInputs() {};
        mpr::YieldStress GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::YieldStress *const )
        {
          mpr::YieldStress toRet;
          cvt::PrecipitationState precipState = Get<cvt::PrecipitationState,BaseCoordinates>::InPoint(point);
          toRet(1e5*precipState().at(0));
          return toRet;
        }

        mpr::StressDStrain GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::StressDStrain *const )
        {
          mpr::StressDStrain toRet;
          toRet(1e4);
          return toRet;
        }

        mpr::StressDStrainRate GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::StressDStrainRate *const )
        {
          mpr::StressDStrainRate toRet;
          toRet(1e3);
          return toRet;
        }
    };

    class PrecipitationModelWithNoInputs
      :public cvt::PrecipitationStateContract
    {
    public:
      cvt::PrecipitationState GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const cvt::PrecipitationState *const )
      {
        cvt::PrecipitationState toRet;
        std::vector<double> vec;
        vec.push_back(2.0);
        toRet(vec);
        return toRet;
      }
    };

    // Models defined with simplified macros
    // Declarations:

    CONTRACT(NullInput,1,mpr::Coordinates2D,mpr::Temperature)
    MODEL(MacroStressModelNoInput,cvt::DeformStressContractRequest,3,NullInput)
    MODEL(MacroStressModelWithInputs,cvt::DeformStressContractRequest,3,cvt::StressModelInputs)

    // Implementation:

    mpr::YieldStress MacroStressModelNoInput::GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::YieldStress *const )
    {
      mpr::YieldStress toRet;
      toRet(1e5);
      return toRet;
    }

    mpr::StressDStrain MacroStressModelNoInput::GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressDStrain *const )
    {
      mpr::StressDStrain toRet;
      toRet(1e4);
      return toRet;
    }

    mpr::StressDStrainRate MacroStressModelNoInput::GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressDStrainRate *const )
    {
      mpr::StressDStrainRate toRet;
      toRet(1e3);
      return toRet;
    }

    mpr::YieldStress MacroStressModelWithInputs::GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::YieldStress *const )
    {
      mpr::YieldStress toRet;
      cvt::PrecipitationState precipState = Get<cvt::PrecipitationState,BaseCoordinates>::InPoint(point);
      toRet(1e5*precipState().at(0));
      return toRet;
    }

    mpr::StressDStrain MacroStressModelWithInputs::GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressDStrain *const )
    {
      mpr::StressDStrain toRet;
      cvt::PrecipitationState precipState = Get<cvt::PrecipitationState,BaseCoordinates>::InPoint(point);
      toRet(1e4*precipState().at(0));
      return toRet;
    }

    mpr::StressDStrainRate MacroStressModelWithInputs::GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressDStrainRate *const )
    {
      mpr::StressDStrainRate toRet;
      cvt::PrecipitationState precipState = Get<cvt::PrecipitationState,BaseCoordinates>::InPoint(point);
      toRet(1e3*precipState().at(0));
      return toRet;
    }
  }

}
#endif // caseVectorTUG_test_h__
