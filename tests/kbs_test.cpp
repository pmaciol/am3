
/*! \file kbs_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../kbs/kbs.h"
#include "../modelPolycrystalMicrostructure/gottstein.h"
#include "caseColdRolling_test.h"
#include "../adapter/adapter.h"

/*	using ***********************************************************/

namespace kbs = am3::kbs;
namespace mpm = am3::model::polycrystal;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace ada = am3::adapter;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterKbs (){};}}

using namespace am3::databus;

using tests::caseColdRolling::MacroInputsTestTempl;
using tests::caseColdRolling::Consts;




typedef TEMPLATELIST_3(mpm::DiffusionBySherstnevTemp, mpm::ShearModulusBySherstnevTemp, MacroInputsTestTempl) ModelsTemplatesList;
typedef DatabusTypelists<ModelsTemplatesList> Databus;

TEST(KbsTests, GetFromDatabus)
{
  boost::shared_ptr<Databus> db(new Databus);
  Databus::AllInputs ai;
  Databus::AllOutputs ao;

   typedef ModelInstanceType<MacroInputsTestTempl, Databus>::Type MacroModel;
   boost::shared_ptr<MacroModel> macroModel = boost::shared_ptr<MacroModel>(new MacroModel);
   SetModelInstance<Databus, MacroInputsTestTempl>(db.get(), macroModel);
   typedef ModelInstanceType<mpm::ShearModulusBySherstnevTemp, Databus>::Type ShearModulusModel;
   boost::shared_ptr<ShearModulusModel> smModel = boost::shared_ptr<ShearModulusModel>(new ShearModulusModel);
   SetModelInstance<Databus, mpm::ShearModulusBySherstnevTemp>(db.get(), smModel);
   typedef ModelInstanceType<mpm::DiffusionBySherstnevTemp, Databus>::Type DiffusionModel;
   boost::shared_ptr<DiffusionModel> difModel = boost::shared_ptr<DiffusionModel>(new DiffusionModel);
   SetModelInstance<Databus, mpm::DiffusionBySherstnevTemp>(db.get(), difModel);
// 
//   mpr::ShearModulus m;


  typedef kbs::DatabusRequester<Databus> Kbs;
  Kbs kbs;
  kbs.SetDatabus(db);
  std::string test = kbs.GetVal("ShearModulus", &Consts::point);
//	EXPECT_EQ(0,mIdProperty());
}

#ifdef KBSTESTS_MANUAL


TEST(KbsTests, ModelId)
{
  boost::shared_ptr<Databus> db(new Databus);
  Databus::AllInputs ai;
  Databus::AllOutputs ao;

  typedef ModelInstanceType<MacroInputsTestTempl, Databus>::Type MacroModel;
  boost::shared_ptr<MacroModel> macroModel = boost::shared_ptr<MacroModel>(new MacroModel);
  SetModelInstance<Databus, MacroInputsTestTempl>(db.get(), macroModel);
  
  typedef ModelInstanceType<mpm::ShearModulusBySherstnevTemp, Databus>::Type ShearModulusModel;
  boost::shared_ptr<ShearModulusModel> smModel = boost::shared_ptr<ShearModulusModel>(new ShearModulusModel);
  SetModelInstance<Databus, mpm::ShearModulusBySherstnevTemp>(db.get(), smModel);
  typedef ModelInstanceType<mpm::ShearModulusBySherstnevTemp, Databus>::Type ShearModulusModel2;
  boost::shared_ptr<ShearModulusModel2> smModel2 = boost::shared_ptr<ShearModulusModel2>(new ShearModulusModel2);
  SetModelInstance<Databus, mpm::ShearModulusBySherstnevTemp>(db.get(), smModel2);
  
  typedef ModelInstanceType<mpm::DiffusionBySherstnevTemp, Databus>::Type DiffusionModel;
  boost::shared_ptr<DiffusionModel> difModel = boost::shared_ptr<DiffusionModel>(new DiffusionModel);
  SetModelInstance<Databus, mpm::DiffusionBySherstnevTemp>(db.get(), difModel);
  
  
  typedef kbs::KbsStringByHand<kbs::DatabusRequester<Databus> > Kbs;
  boost::shared_ptr<kbs::DatabusRequester<Databus> >  dbr(new kbs::DatabusRequester<Databus>);
  boost::shared_ptr<Kbs> kbs(new Kbs(dbr));
  dbr->SetDatabus(db);

//  boost::shared_ptr<ada::Switcher<Kbs, ppm::contract::ShearModulusContract2D> > shearModulusSwitcher(new ada::Switcher<Kbs, ppm::contract::ShearModulusContract2D>(kbs));


  mpr::ModelID mId;
  mId = kbs->GetModelId<mpr::ShearModulus>(&Consts::point);
  //std::string test = kbs->GetVal("ShearModulus", &Consts::point);
  //	EXPECT_EQ(0,mIdProperty());
}

#endif // KBSTESTS_MANUAL