
/*! \file mockMacroModels.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef mockMacroModels_h__
#define mockMacroModels_h__
/*	include *********************************************************/

#include "../models/macrosSimplifiedModels.h"
#include "../models/description.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "../databus/adapter.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace sdm = am3::software::deform;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace tests
{
  CONTRACT(TemperatureContract					,1,mpr::Coordinates2D, mpr::Temperature);
  CONTRACT(TimeContract					        ,1,mpr::Coordinates2D, mpr::Time);
  CONTRACT(MacroContract								,3,mpr::Coordinates2D, mpr::Temperature , mpr::EffectiveStrainRate , mpr::StepLength);
  CONTRACT(MacroWithTimeContract				,5,mpr::Coordinates2D, mpr::Temperature , mpr::EffectiveStrainRate , mpr::StepLength, mpr::Time,mpr::ElementNumber);
  CONTRACT(YieldStressContract2D				,1,mpr::Coordinates2D, mpr::YieldStress);

  class MacroInputsTest
    : public MacroContract
  {
  public:
    MacroInputsTest(const double temperature, const double effStrainRate, const double stepLength){};
    MacroInputsTest(){temperature_(773.15);efepse_(1.0);stepLength_(0.001);}  //Sherstnev
    mpr::Temperature temperature_;
    mpr::EffectiveStrainRate efepse_;
    mpr::StepLength stepLength_;
    
    virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Temperature *const ) {return temperature_;};
    virtual mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::EffectiveStrainRate *const ) {return efepse_;};  //Sherstnev
    virtual mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StepLength *const ) {return stepLength_;};

  };

    class MacroWithTimeInputsTest : public MacroWithTimeContract
    {
  public:
  virtual mpr::Time GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Time *const ) {return time_;};
    mpr::Temperature temperature_;
    mpr::EffectiveStrainRate efepse_;
    mpr::Time time_;
     MacroWithTimeInputsTest(){temperature_(1000);efepse_(1.0);time_(0);}  //Sherstnev
    virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Temperature *const ) {return temperature_;};
    virtual mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::EffectiveStrainRate *const ) {return efepse_;};  //Sherstnev
    virtual mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StepLength *const ) {mpr::StepLength toRet;toRet(10);return toRet;};
    virtual mpr::ElementNumber GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::ElementNumber *const ) {mpr::ElementNumber toRet;toRet(1);return toRet;};
};


    // Moved to mockModels

//     class MacroDataFromFile : public MacroWithTimeContract
//     {
//     public:
//       MacroDataFromFile(std::string fileWithPath);
//       virtual mpr::Time GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Time *const );
//       virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Temperature *const );
//       virtual mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::EffectiveStrainRate *const );
//       virtual mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StepLength *const );
//       virtual mpr::ElementNumber GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::ElementNumber *const );
//       bool NextStep();
//     protected:
//       struct Data 
//       {
//         mpr::Time time;
//         mpr::Temperature temperature;
//         mpr::EffectiveStrainRate effStrainRate;
//       };
//       std::vector<Data> allDataContainer;
//       std::vector<Data>::iterator presentStep;
//       mpr::ElementNumber elNumber;
//       mpr::StepLength stepLength;
//     };

  class ConstantStress
    : public sdm::DeformStressContract2D
    , public YieldStressContract2D
  {
  public:
    ConstantStress(const double yieldStress,const double yieldStressDstrain,const double yieldStressDstrainRate){ys(yieldStress);sdstr(yieldStressDstrain);sdstrRate(yieldStressDstrainRate);};
    virtual mpr::YieldStress GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::YieldStress* const)
    {
      return ys;
    };
    virtual mpr::StressDStrain GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrain* const)
    {
      return sdstr;
    };
    virtual mpr::StressDStrainRate GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrainRate* const)
    {
      return sdstrRate;
    };
  protected:
    mpr::YieldStress ys;
    mpr::StressDStrain sdstr;
    mpr::StressDStrainRate sdstrRate;
  };

   CONTRACT(NullInput,1,mpr::Coordinates2D,mpr::Temperature)
  template<typename T>
  struct ConstantStressTempl
    : public sdm::DeformStressContract2D
  {
    typedef NullInput Inputs;
    typedef typename sdm::DeformStressContract2D Contract;
    ConstantStressTempl(const double yieldStress,const double yieldStressDstrain,const double yieldStressDstrainRate): ConstantStress(yieldStress,yieldStressDstrain,yieldStressDstrainRate){};
  };

  class YieldStressMock
    : public YieldStressContract2D
  {
  public:
    typedef YieldStressContract2D Contract;
    typedef mpr::Coordinates2D Coordinates;
    mpr::YieldStress ys_;
    YieldStressMock(){ys_(1e5);}
    YieldStressMock(const double ys){ys_(ys);}
    virtual mpr::YieldStress GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::YieldStress *const ) {return ys_;};
  };

  typedef sdm::DeformHandler<mpr::Coordinates2D> ExternalFemHandler;

   class ExternalFemMock
   {
   public:
     void RunStep1Element1();
   protected:
     double yieldStressDStrainRate;
     double yieldStressDStrain;
     double yieldStress;
     double timeStepLength;
     double temperature;
     double effStrainRate;
     static const int nodesNumber = 4;
     double coordinates[8];
     double time;
     int elementId;
     int meshId;
     static const int userVariablesNumber = 4;
     double userVariablesValues[4];	

     boost::shared_ptr<ExternalFemHandler> externalFemHandler_;
   };

   //////////////////////////////////////////////////////////////////////////

   class TemperatureConstant
     : public TemperatureContract
   {
   public:
     TemperatureConstant(double const & temperature): temperature_(temperature){};
     virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Temperature *const ) { mpr::Temperature toRet; toRet(temperature_); return toRet;};
   protected:
     const double temperature_;
   };


   template <typename VARIABLE, typename CONTRACT>
   class VariableLinear
     : public CONTRACT
   {
   public:
     VariableLinear(){};
     VariableLinear(const double & initValue, const double& changeForOneStep): value_(initValue), changeForOneStep_(changeForOneStep), stepNumber_(0){};
     void Set(const double & initValue, const double& changeForOneStep) {value_=initValue; changeForOneStep_ = changeForOneStep; stepNumber_  = 0;};
     virtual VARIABLE GetState(mpt::Point<mpr::Coordinates2D> *,const VARIABLE *const ) 
     { 
       VARIABLE toRet; toRet(value_ + changeForOneStep_ * stepNumber_); return toRet;
     };
     void NextStep() {++stepNumber_;}
     int StepNumber() {return stepNumber_;}
   protected:
     double value_;
     double changeForOneStep_;
     int stepNumber_;
   };

   typedef VariableLinear<mpr::Time, TimeContract> TimeLinear;
   typedef VariableLinear<mpr::Temperature, TemperatureContract> TemperatureLinear;

   //////////////////////////////////////////////////////////////////////////

   extern mpr::Coordinates2D testCoord;
   extern mpr::Time testTime;
   extern pnt::ComputedPoint<mpr::Coordinates2D> testPoint;

//   DEFORM_ENTRY(sdm::DeformHandlerOutputExternalModels<mpr::Coordinates2D>, databus, databus,strMnull,strMnull);
}//namespace tests


#endif // mockMacroModels_h__
