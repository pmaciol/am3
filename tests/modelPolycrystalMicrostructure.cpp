
/*! \file modelPolycrystalMicrostructure.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../adapter/adapter.h"
#include "../models/point.h"
//#include "../problemPolycrystalMicrostructure/switcher.h"
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../caseColdRolling/databus.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "modelPolycrystalMicrostructure.h"



/*	using ***********************************************************/

namespace mpt = am3::model::point;
namespace ppm = am3::problem::polycrystal;
namespace ccr = am3::cases::coldRolling;
namespace ada = am3::adapter;
namespace sdm = am3::software::deform;
using tests::Consts;

/*	extern **********************************************************/

/*extern "C" */ __declspec( dllimport ) int DEFORM_USRE1_SEND(const int * length,const double * values	,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport) int DEFORM_USRE1_GET(const int * length, double * values,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport ) int DEFORM_NODES_SEND(const int * nodesNumber, const double * coordinates,	const double * time,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport) int DEFORM_TEMPERATURE(const double * temperature	,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport) int DEFORM_EFF_STR_RATE(const double * temperature	,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport) int DEFORM_YIELD_STRESS_GET(double * yieldStress,	const int * meshId,	const int * elementId);
///*extern "C" */  __declspec( dllimport) int DEFORM_IUSRVL_SEND(const int * length,	const double * values,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport) int DEFORM_TIME_STEP_LENGTH(const double * timeStepLength,	const int * meshId,	const int * elementId);

extern ActualDeformHandler* deformHandler;

/*	classes *********************************************************/

//boost::shared_ptr<tests::CaseMacroImplementation> caseHandler (new tests::CaseMacroImplementation);
sdm::DeformHandlerUserProc * deformHandlerUserProc;

namespace am3 {namespace tests {void RegisterModelPolycrystalMicrostructure (){};}}

const double tests::Consts::yieldStressCheck = 123.0;
const double tests::Consts::yieldStressDStrainCheck = 12.30;
const double tests::Consts::yieldStressDStrainRateCheck = 1.230;
const int tests::Consts::nodesNumber=4;
const double tests::Consts::coordinates[8]={0,0,1,0,1,1,0,1};
const double tests::Consts::time = 0;
const int tests::Consts::elementId = 1;
const int tests::Consts::meshId = 0;
const double tests::Consts::time2 = 0.1;
const double tests::Consts::coordinates2[8]={1,0,1,1,0,1,0,0};


boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tests::ConstantYieldStress(Consts::yieldStressCheck,Consts::yieldStressDStrainCheck,Consts::yieldStressDStrainRateCheck));
	
boost::shared_ptr<ActualDeformHandler> deformHandlerPtr(new ActualDeformHandler(ySmodelPtr));

sdm::DeformHandler<mpr::Coordinates2D>* testDh = deformHandlerPtr.get();
sdm::DeformHandler<mpr::Coordinates2D>* deformHandler = testDh;



TEST(ColdRolling, UpdatesElementalVariables)
{

	boost::shared_ptr<tests::CaseMacroImplementation> caseMacroImpl (new tests::CaseMacroImplementation(ySmodelPtr));
	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
sdm::DeformHandler<mpr::Coordinates2D,tests::IVStorage >* deformHandler = caseMacroImpl->GetHandler().get();

	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
	deformHandlerUserProc->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);
  
	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
	ada::BusModelAdapterList<DeformInput2D::VariablesTypes , ccr::DatabusForGottsteinDeform,tests::CaseMacroImplementation,pnt::Point<mpr::Coordinates2D> > busModelAdapterDeform;
	
	busModelAdapterDeform.Set(testDatabus,caseMacroImpl);
	

	boost::shared_ptr<tests::DeformHandlerToColdRollingAdapter> deformAdapter (new tests::DeformHandlerToColdRollingAdapter);
	deformAdapter->SetDeformHandler(deformHandler);

	ada::BindModelToBus(testDatabus,deformAdapter);
		
	boost::shared_ptr<tests::DiffusionTest> diffusionModel (new tests::DiffusionTest);
	boost::shared_ptr<tests::DislocationTest> dislocationModel (new tests::DislocationTest);
	boost::shared_ptr<tests::GrainsTest> grainsModel (new tests::GrainsTest);
	
	boost::shared_ptr<tests::ShearModulusTest> shearModel (new tests::ShearModulusTest);
	boost::shared_ptr<tests::YieldStressTest> yieldModel (new tests::YieldStressTest);
	boost::shared_ptr<tests::ZenerParamTest> zenerModel (new tests::ZenerParamTest);


	ada::BindModelToBus(testDatabus,diffusionModel);
	ada::BindModelToBus(testDatabus,dislocationModel);
	ada::BindModelToBus(testDatabus,grainsModel);
	
	ada::BindModelToBus(testDatabus,shearModel);
	ada::BindModelToBus(testDatabus,yieldModel);
// 	ada::BindModelToBus(testDatabus,zenerModel);

	double userVariablesValuesExpected[8] = {0.1,0.1,20e3,50e-6,2e15,0.1,25e-6,0.5};	
	double userVariablesValuestoCheck[8];	
	deformHandlerUserProc->GetElementalVariables(8,userVariablesValuestoCheck,Consts::meshId,Consts::elementId);
	for (int i=0;i<8;i++)
	{
		EXPECT_DOUBLE_EQ(userVariablesValuestoCheck[i],userVariablesValuesExpected[i]);
	}
}


TEST(ColdRolling,ReturnsProperElementalVariables)
{
	boost::shared_ptr<tests::CaseMacroImplementation> caseMacroImpl (new tests::CaseMacroImplementation(ySmodelPtr));
	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
	deformHandlerUserProc->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);

	int userVariablesNumber=8;
	double userVariablesValues[8] = {1,2,3,4,5,6,7,8};	
	double userVariablesValuestoCheck[8];	
	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,Consts::meshId,Consts::elementId);
	EXPECT_DOUBLE_EQ(userVariablesValues[0],(caseMacroImpl->GetPreviousState<ppm::contract::DislDensityMobile>().Value()));
 	double userVariablesValues2[8] = {2,3,4,5,6,7,8,9};	
 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues2,Consts::meshId,Consts::elementId);
	EXPECT_DOUBLE_EQ(userVariablesValues2[0],(caseMacroImpl->GetPreviousState<ppm::contract::DislDensityMobile>().Value()));
}


TEST(ColdRolling, DeformFetchDatabus)
{
	const double temperature = 893;
	
	boost::shared_ptr<moc::Provider<mpr::YieldStress,mpr::Coordinates2D> > testYS (new tests::YieldStressTest);
	boost::shared_ptr<tests::CaseMacroImplementation> caseMacroImpl (new tests::CaseMacroImplementation(testYS, ySmodelPtr,ySmodelPtr));
	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
	sdm::DeformHandler<mpr::Coordinates2D,tests::IVStorage >* deformHandler = caseMacroImpl->GetHandler().get();


	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
	deformHandlerUserProc->SetTemperature(&temperature,	Consts::meshId,	Consts::elementId);
	deformHandlerUserProc->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);

	boost::shared_ptr<tests::DeformHandlerToColdRollingAdapter> deformAdapter (new tests::DeformHandlerToColdRollingAdapter);
	deformAdapter->SetDeformHandler(caseMacroImpl->GetHandler().get());

	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
	
	ada::BindModelToBus(testDatabus,deformAdapter);
	mpr::Temperature temperatureToCheck;
	mpt::NamedPointLastTwoSteps2D point = (*deformHandler->GetMidPoint());

	boost::shared_ptr<moc::Provider<mpr::Temperature,mpr::Coordinates2D> > tempProvider;
	tempProvider = testDatabus;

	temperatureToCheck=tempProvider->GetState(&point,&temperatureToCheck);
	EXPECT_DOUBLE_EQ(temperature,temperatureToCheck.Value());
}


TEST(ColdRolling, TestingDatabus)
{

	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
	boost::shared_ptr<tests::DiffusionTest> diffusionModel (new tests::DiffusionTest);
	boost::shared_ptr<tests::DislocationTest> dislocationModel (new tests::DislocationTest);
	boost::shared_ptr<tests::GrainsTest> grainsModel (new tests::GrainsTest);
	boost::shared_ptr<tests::MacroInputsTest> macroModel (new tests::MacroInputsTest);
	boost::shared_ptr<tests::ShearModulusTest> shearModel (new tests::ShearModulusTest);
	boost::shared_ptr<tests::YieldStressTest> yieldModel (new tests::YieldStressTest);
	boost::shared_ptr<tests::ZenerParamTest> zenerModel (new tests::ZenerParamTest);
	

	ada::BindModelToBus(testDatabus,diffusionModel);
	ada::BindModelToBus(testDatabus,dislocationModel);
	ada::BindModelToBus(testDatabus,grainsModel);
	ada::BindModelToBus(testDatabus,macroModel);
	ada::BindModelToBus(testDatabus,shearModel);
	ada::BindModelToBus(testDatabus,yieldModel);
	ada::BindModelToBus(testDatabus,zenerModel);

	mpr::Coordinates2D coord(0.5,0.5);
	mpr::Time t(0.0);
	pnt::ComputedPoint<mpr::Coordinates2D> point(coord,t);
	mpr::Temperature temperatureToCheck;
	mpr::ShearModulus shearModulusToCheck;
	boost::shared_ptr<moc::Provider<mpr::Temperature,mpr::Coordinates2D> > tempProvider;
	boost::shared_ptr<moc::Provider<mpr::ShearModulus,mpr::Coordinates2D> > shearModulusProvider;
	tempProvider = testDatabus;
	shearModulusProvider = testDatabus;

	temperatureToCheck=tempProvider->GetState(&point,&temperatureToCheck);
	EXPECT_DOUBLE_EQ(1000.0,temperatureToCheck.Value());

	shearModulusToCheck=shearModulusProvider->GetState(&point,&shearModulusToCheck);
	EXPECT_DOUBLE_EQ(0.25,shearModulusToCheck.Value());
}

TEST(DeformBuiltIn, DefromGetDataFromDatabus)
{
	const double temperature = 893;

	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform);

	boost::shared_ptr<moc::Provider<mpr::YieldStress,mpr::Coordinates2D> > testYS (new tests::YieldStressTest);
	boost::shared_ptr<tests::CaseMacroImplementation> caseMacroImpl (new tests::CaseMacroImplementation(testYS, ySmodelPtr,ySmodelPtr));
	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
	sdm::DeformHandler<mpr::Coordinates2D,tests::IVStorage >* deformHandler = caseMacroImpl->GetHandler().get();

	deformHandler->SetTemperature(&temperature,	Consts::meshId,	Consts::elementId);
	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();

	deformHandler->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);

	boost::shared_ptr<tests::DeformHandlerToColdRollingAdapter> deformAdapter (new tests::DeformHandlerToColdRollingAdapter);
	deformAdapter->SetDeformHandler(deformHandler);

	boost::shared_ptr<tests::YieldStressTest> yieldModel (new tests::YieldStressTest);

	ada::BindModelToBus(testDatabus,yieldModel);

	double yieldStress = deformHandler->GetYieldStress( Consts::meshId, Consts::elementId);
	EXPECT_DOUBLE_EQ(0.1,yieldStress);
}

TEST(ColdRolling, GettingDataThroughDatabus)
{

	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
	boost::shared_ptr<tests::MacroInputsTest> macroModel (new tests::MacroInputsTest);
	boost::shared_ptr<tests::ShearModulusTemperatureDependentTest> shearModel (new tests::ShearModulusTemperatureDependentTest);


	ada::BindModelToBus(testDatabus,macroModel);
	ada::BindModelToBus(testDatabus,shearModel);

	mpr::Coordinates2D coord(0.5,0.5);
	mpr::Time t(0.0);
	pnt::ComputedPoint<mpr::Coordinates2D> point(coord,t);
	mpr::Temperature temperatureToCheck;
	mpr::ShearModulus shearToCheck;
	boost::shared_ptr<moc::Provider<mpr::Temperature,mpr::Coordinates2D> > tempProvider;
	tempProvider = testDatabus;

	temperatureToCheck=tempProvider->GetState(&point,&temperatureToCheck);
	EXPECT_DOUBLE_EQ(1000.0,temperatureToCheck.Value());


	ada::BusModelAdapterList<tests::ShearModulusTemperatureDependentTestInput::VariablesTypes , ccr::DatabusForGottsteinDeform,tests::ShearModulusTemperatureDependentTest,pnt::Point<mpr::Coordinates2D> > busModelAdapterShearModulus;
	busModelAdapterShearModulus.Set(testDatabus,shearModel);


	boost::shared_ptr<moc::Provider<mpr::ShearModulus,mpr::Coordinates2D> > shearProvider;
	shearProvider = testDatabus;

	shearToCheck=shearProvider->GetState(&point,&shearToCheck);
	EXPECT_DOUBLE_EQ(100.0,shearToCheck.Value());
}

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////


TEST(ColdRolling, CreateSwitcher)
{

	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
	boost::shared_ptr<tests::MacroInputsTest> macroModel (new tests::MacroInputsTest);
 	boost::shared_ptr<tests::ShearModulusTest> shearModel1 (new tests::ShearModulusTest);
	boost::shared_ptr<tests::ShearModulusTest> shearModel2 (new tests::ShearModulusTest);


	ada::BindModelToBus(testDatabus,macroModel);

	boost::shared_ptr<tests::Kbs> kbs (new tests::Kbs(testDatabus));
	boost::shared_ptr<ada::Switcher<tests::Kbs, ppm::contract::ShearModulusContract2D> > shearModulusSwitcher(new ada::Switcher<tests::Kbs, ppm::contract::ShearModulusContract2D>(kbs));
	
 	shearModulusSwitcher->AddModel(shearModel1);
	shearModel2->shearModulus_(0.25);
 	shearModulusSwitcher->AddModel(shearModel2);
	shearModel2->shearModulus_(0.5);
	
	ada::BindModelToBus(testDatabus,shearModulusSwitcher);

	mpr::Coordinates2D coord(0.5,0.5);
	mpr::Time t(0.0);
	pnt::ComputedPoint<mpr::Coordinates2D> point(coord,t);
	mpr::ShearModulus shearToCheck;
	boost::shared_ptr<moc::Provider<mpr::ShearModulus,mpr::Coordinates2D> > shearProvider;
	shearProvider = testDatabus;
	shearToCheck=shearProvider->GetState(&point,&shearToCheck);
	EXPECT_DOUBLE_EQ(0.25,shearToCheck.Value());

	macroModel->temperature_(500.0);
	shearToCheck=shearProvider->GetState(&point,&shearToCheck);
	EXPECT_DOUBLE_EQ(0.5,shearToCheck.Value());
}
