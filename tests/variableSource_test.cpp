
/*! \file variableSource_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../modelTemplates/variableSource.h"
#include "../models/dataStructures.h"
#include "../models/point.h"

/*	using ***********************************************************/

namespace mvs = am3::model::varSource;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3 { namespace tests { void RegisterVariableSource(){}; } }


TEST(VariableSources, PushableSingleValueSource)
{
  boost::shared_ptr<mvs::PushableSingleValueSource<mpr::Temperature> > temperatureSrc(new mvs::PushableSingleValueSource<mpr::Temperature>);
  mpr::Temperature temperature;
  temperature(700);
  const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
  mpr::Time t = mpr::Time(0.0);
  mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);
  temperatureSrc->SetVariable(point, temperature);

  EXPECT_DOUBLE_EQ(700, temperatureSrc->GetVariable(point).Value());
 
  temperature(780);
  temperatureSrc->SetVariable(point,temperature);
  EXPECT_DOUBLE_EQ(780, temperatureSrc->GetVariable(point).Value());

  mpr::Temperature returned;

  mpr::Time t2(1.0);
  point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord, t2));
  temperature(680);

  temperatureSrc->SetVariable(point, temperature);
  EXPECT_DOUBLE_EQ(680, temperatureSrc->GetVariable(point).Value());
  EXPECT_FALSE(temperatureSrc->GetVariable(point).IsWrong());

  mpr::Time t3(2.0);
  point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord, t3));
  EXPECT_TRUE(temperatureSrc->GetVariable(point).IsWrong());
}

