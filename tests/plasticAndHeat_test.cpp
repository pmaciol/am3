namespace am3 {namespace tests {void RegisterPlasticAndHeat(){};}}

#include "gtest/gtest.h"
#include <boost/shared_ptr.hpp>
#include "../models/dataStructures.h"
#include "../softwareDeform/storage.h"
#include "../problemPlasticAndHeat/fineCoarseDumbAdapter.h"
#include "plasticAndHeat_test.h"
#include "../problemPlasticAndHeat/kbsConverter.h"
#include "../common/literals.h"
#include "../communicationSocket/socketServer.h"
#include "../models/dataStructures.h"
#include "../problemPlasticAndHeat/fineTugCa.h"
#include "../problemPlasticAndHeat/fineModelSSCurves.h"
#include "../problemPlasticAndHeat/coarseDeform.h"
#include "../common/literals.h"

#include "../models/modelSelector.h"
#include "../knowledge/kbsInterfaces.h"



namespace kph=am3::knowledge::plasticAndHeat;
namespace prop=am3::model::properties;
namespace pah=am3::problem::plasticAndHeat;
namespace soc=am3::common::communication;
namespace sds=am3::software::deform::storage;

namespace kno=am3::knowledge;


// TEST(coarseDeformModel, worksOnlyWithExternalDeformSimulation)
// {
// 
//   pah::CoarsePlasticAndHeatWithDeform deformModel;
//   deformModel.Init();
//   soc::SocketServer deformSocketServer(deformModel.GetCallProcessor());
//   deformSocketServer.SetModuleOptionsTemplateMethod();
//   deformModel.Run();
//   deformSocketServer.Communicate();
//   FAIL();
// }


TEST(plasticAndHeat_FineModelSelector,AccessWithIterator)
{

	typedef pah::FineModelsSwitcher<
		tests::mockCoarseModel, 
		kno::I4Adapter<prop::Coordinates2D>
	> FMS;


	boost::shared_ptr<FMS> fineModSel(new FMS);

	boost::shared_ptr<tests::mockCoarseModel> mFCoarseModel(new tests::mockCoarseModel);
	boost::shared_ptr<pah::FineModelSSCurves> fmSSC(new pah::FineModelSSCurves);
	boost::shared_ptr<pah::FineModelTugCA> fmTUGCA(new pah::FineModelTugCA);

	fineModSel->AddModel(fmSSC);
	fineModSel->AddModel(fmTUGCA);

	FMS::iterator it;
	it=fineModSel->begin();
	std::string str1=(*it)->Name();
	EXPECT_STREQ(am3::kTUGSsc,str1.c_str());
	it++;
	std::string str2=(*it)->Name();
	EXPECT_STREQ(am3::kTUGCaGrain,str2.c_str());
}

TEST(plasticAndHeat_FineModelSelector,RealModels)
{

  typedef pah::FineModelsSwitcher<
    tests::mockCoarseModel, 
    kno::I4Adapter<prop::Coordinates2D>
  > FMS;


  boost::shared_ptr<FMS> fineModSel(new FMS);
   boost::shared_ptr<tests::mockCoarseModel> mFCoarseModel(new tests::mockCoarseModel);

   boost::shared_ptr<pah::PlasticAndHeat2DCoarseContract> t;
   t=fineModSel;

   boost::shared_ptr<pah::PlasticAndHeat2DFineContract> t2;
   t2=fineModSel;

   boost::shared_ptr<pah::FineModelSSCurves> fmSSC(new pah::FineModelSSCurves);
   boost::shared_ptr<pah::FineModelTugCA> fmTUGCA(new pah::FineModelTugCA);

   fineModSel->AddFineInterface(fmSSC);
   EXPECT_TRUE(fineModSel->IsFineInterfaceSet());
   fineModSel->AddFineInterface(fmTUGCA);
   EXPECT_TRUE(fineModSel->IsFineInterfaceSet());
}

TEST(plasticAndHeat_FineModelSelector,GetStateFromFine_test)
{

  typedef pah::FineModelsSwitcher<
    tests::mockCoarseModel, 
    kno::I4Adapter<prop::Coordinates2D>
  > FMS;
  boost::shared_ptr<FMS> fineModSel(new FMS);
  
  boost::shared_ptr<tests::mockFineModel> mFineMod(new tests::mockFineModel);
  fineModSel->AddFineInterface(mFineMod);
  
  boost::shared_ptr<tests::mockRebitClient> mockRebit(new tests::mockRebitClient);
  boost::shared_ptr<kph::PlasticAndHeat2RebitConverter> converter (new kph::PlasticAndHeat2RebitConverter(mockRebit, fineModSel, fineModSel,fineModSel));
  fineModSel->SetI4Adapter(converter);

  prop::Coordinates2D coord(0.1,0.2);
  prop::Time t(100.0);
  com::ComputedPoint<prop::Coordinates2D> point(coord,t);
  prop::YieldStress stress;

  stress=fineModSel->GetState(&point,&stress);
  EXPECT_DOUBLE_EQ(0.1,stress());
}

TEST(plasticAndHeat_FineModelSelector,GetStateFromCoarse_test)
{
  typedef pah::FineModelsSwitcher<
    tests::mockCoarseModel, 
    kno::I4Adapter<prop::Coordinates2D>
  > FMS;
  boost::shared_ptr<FMS> fineModSel(new FMS);

  prop::Coordinates2D coord(0.1,0.2);
  prop::Time t(100.0);
  com::ComputedPoint<prop::Coordinates2D> point(coord,t);
  prop::TotalEffectiveStrain strain;

  strain=fineModSel->GetState(&point,&strain);
  EXPECT_DOUBLE_EQ(0.0251,strain());
}

TEST(plasticAndHeat_FineModelSelector,SetI4Adapter_test)
{
  typedef pah::FineModelsSwitcher<
    tests::mockCoarseModel, 
    kno::I4Adapter<prop::Coordinates2D>
  > FMS;
  boost::shared_ptr<FMS> fineModSel(new FMS);

  EXPECT_FALSE(fineModSel->IsI4AdapterSet());

   boost::shared_ptr<tests::mockRebitClient> mockRebit(new tests::mockRebitClient);
   boost::shared_ptr<kph::PlasticAndHeat2RebitConverter> converter (new kph::PlasticAndHeat2RebitConverter(mockRebit, fineModSel, fineModSel, fineModSel));
   fineModSel->SetI4Adapter(converter);
   EXPECT_TRUE(fineModSel->IsI4AdapterSet());
}
TEST(plasticAndHeat_FineModelSelector,SetFineInterface_test)
{
  typedef pah::FineModelsSwitcher<
    tests::mockCoarseModel, 
    kno::I4Adapter<prop::Coordinates2D>
  > FMS;
  boost::shared_ptr<FMS> fineModSel(new FMS);
  
  EXPECT_FALSE(fineModSel->IsFineInterfaceSet());

  boost::shared_ptr<tests::mockFineModel> mFineMod(new tests::mockFineModel);
  fineModSel->AddFineInterface(mFineMod);
  EXPECT_TRUE(fineModSel->IsFineInterfaceSet());
}


TEST(plasticAndHeat_Converter, Fetch_test)
{
  typedef pah::FineModelsSwitcher<
    tests::mockCoarseModel, 
    kno::I4Adapter<prop::Coordinates2D>
  > FMS;
  boost::shared_ptr<FMS> fineModSel(new FMS);

  boost::shared_ptr<tests::mockFineModel> mFineMod(new tests::mockFineModel);

  fineModSel->AddFineInterface(mFineMod);

  boost::shared_ptr<tests::mockRebitClient> mockRebit(new tests::mockRebitClient);

  boost::shared_ptr<kph::PlasticAndHeat2RebitConverter> converter(new kph::PlasticAndHeat2RebitConverter(mockRebit, fineModSel, fineModSel, fineModSel));
  fineModSel->SetI4Adapter(converter);

  boost::shared_ptr<VariableDTO> property(new VariableDTO);
  VariableDTO where;
  VariableDTO when;

  EXPECT_FALSE(converter->Fetch(property,where,when));
  
  property->Id->assign(am3::kTotalEffStrain);
  where.Id->assign(am3::kCoordinates2D);
  where.Value->push_back("0.1");
  where.Value->push_back("0.1");
  when.Value->push_back("10.0");
  when.Id->assign(am3::kTime);
  EXPECT_TRUE(converter->Fetch(property,where,when));
  EXPECT_DOUBLE_EQ(0.0251,boost::lexical_cast<double>(property->Value->at(0)));
  
  property->Id->assign(am3::kStrainRate2D);
  EXPECT_TRUE(converter->Fetch(property,where,when));
  EXPECT_DOUBLE_EQ(0.3,boost::lexical_cast<double>(property->Value->at(2)));

  property->Id->assign(am3::kTemperature);
  EXPECT_TRUE(converter->Fetch(property,where,when));
  EXPECT_DOUBLE_EQ(68.1,boost::lexical_cast<double>(property->Value->at(0)));

  property->Id->assign(am3::kCoordinates2D);
  EXPECT_TRUE(converter->Fetch(property,where,when));
  EXPECT_DOUBLE_EQ(0.1,boost::lexical_cast<double>(property->Value->at(0)));

  property->Id->assign(am3::kTime);
  EXPECT_TRUE(converter->Fetch(property,where,when));
  EXPECT_DOUBLE_EQ(100,boost::lexical_cast<double>(property->Value->at(0)));

  property->Id->assign(am3::kYieldStress);
  EXPECT_TRUE(converter->Fetch(property,where,when));
  EXPECT_DOUBLE_EQ(0.1,boost::lexical_cast<double>(property->Value->at(0)));
}

TEST(plasticAndHeat_Converter, GetModelId_test)
{
  typedef pah::FineModelsSwitcher<
    tests::mockCoarseModel, 
    kno::I4Adapter<prop::Coordinates2D>
  > FMS;
  boost::shared_ptr<FMS> fineModSel(new FMS);

  boost::shared_ptr<tests::mockFineModel> mFineMod(new tests::mockFineModel);

  fineModSel->AddFineInterface(mFineMod);

  boost::shared_ptr<tests::mockRebitClient> mockRebit(new tests::mockRebitClient);

  kph::PlasticAndHeat2RebitConverter converter(mockRebit, fineModSel, fineModSel, fineModSel);

  prop::Coordinates2D coord(0.1,0.2);
  prop::Time t(100.0);
  com::ComputedPoint<prop::Coordinates2D> point(coord,t);
  prop::ModelID mId;
  
  mId=converter.GetState(&point,&mId);
  EXPECT_EQ(0,mId());

}

TEST(plasticAndHeatSSCurvesModel, GetState_test)
{
  pah::FineModelSSCurves fmSSC;
  boost::shared_ptr<tests::mockCoarseModel> mCoarseMod(new tests::mockCoarseModel);
  prop::YieldStress stress;
  prop::StressDStrainRate stressDStrainRate;
  prop::StressDStrain stressDstrain;
  prop::Coordinates2D coord(0.1,0.2);
  prop::Time t(100.0);
  com::ComputedPoint<prop::Coordinates2D> point(coord,t);
  fmSSC.SetCoarseModel(mCoarseMod);
  stress=fmSSC.GetState(&point,&stress);
  stressDstrain=fmSSC.GetState(&point,&stressDstrain);
  stressDStrainRate=fmSSC.GetState(&point,&stressDStrainRate);
  EXPECT_NEAR(175,stress(),1.0);
  EXPECT_NEAR(160,stressDstrain(),1.0);
  EXPECT_NEAR(111,stressDStrainRate(),1.0);
}

TEST(tmpSocketTest, GetSocket_test)
{
  pah::TmpSocket sock;
  
  EXPECT_TRUE(sock.IsWsaStarted());
  EXPECT_TRUE(sock.IsSocketValid());
}
// Long lasting test. Must be fired in debug mode with breakpoint at first line. gTest Creates TmpSock somewhere else and it need a time for restarting.
// TEST(tmpSocketTest,Processing_test)
// {
//   pah::TmpSocket sock;
//   double temperature=900.0;
//   double goalTime=1.2;
//   double actualTime,ratio1,dm1,ratio2,dm2;
//   sock.Process(temperature,goalTime,actualTime,ratio1,dm1,ratio2,dm2);
//   double sum=ratio1+ratio2;
//   EXPECT_DOUBLE_EQ(1.0,sum);
// }


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

//const string tests::mockCoarseModel::SingleModelAvailable::name_ = "foo name";

prop::YieldStress tests::mockFineModel::GetState( pnt::Point<prop::Coordinates2D> *,const prop::YieldStress *const )
{
  prop::YieldStress prop;
  prop(0.1);
  return prop;
}

prop::StressDStrain tests::mockFineModel::GetState( pnt::Point<prop::Coordinates2D> *,const prop::StressDStrain *const )
{
  prop::StressDStrain prop;
  prop(0.1);
  return prop;
}

prop::StressDStrainRate tests::mockFineModel::GetState( pnt::Point<prop::Coordinates2D> *,const prop::StressDStrainRate *const )
{
  prop::StressDStrainRate prop;
  prop(0.1);
  return prop;
}

prop::TotalEffectiveStrain tests::mockCoarseModel::GetState( pnt::Point<prop::Coordinates2D> *,const prop::TotalEffectiveStrain *const )
{
  prop::TotalEffectiveStrain prop;
  prop(2.51e-2);
  return prop;
}

prop::StrainRate2D tests::mockCoarseModel::GetState( pnt::Point<prop::Coordinates2D> *,const prop::StrainRate2D *const )
{
  prop::StrainRate2D prop;
  prop.g11(0.1); prop.g12(0.2); prop.g21(0.3); prop.g22(0.4);
  return prop;
}

prop::EffectiveStrainRate tests::mockCoarseModel::GetState( pnt::Point<prop::Coordinates2D> *,const prop::EffectiveStrainRate *const )
{
  prop::EffectiveStrainRate prop;
  prop(1.1e-3);
  return prop;
}

prop::Temperature tests::mockCoarseModel::GetState( pnt::Point<prop::Coordinates2D> *,const prop::Temperature *const )
{
  prop::Temperature property;
  property(68.1);
  return property;
}

prop::Coordinates2D tests::mockCoarseModel::GetState( pnt::Point<prop::Coordinates2D> *,const prop::Coordinates2D *const )
{
  prop::Coordinates2D property;
  property.x(0.1);
  property.y(0.2);
  return property;
}

prop::Time tests::mockCoarseModel::GetState( pnt::Point<prop::Coordinates2D> *,const prop::Time *const )
{
  prop::Time ti;
  ti(100.0);
  return ti;
}

prop::ElementNumberGlobal tests::mockCoarseModel::GetState( pnt::Point<prop::Coordinates2D> *,const prop::ElementNumberGlobal *const )
{
  prop::ElementNumberGlobal ti;
  ti(1);
  return ti;
}

int tests::mockRebitClient::SetFact( const VariableDTO& prop )
{
  return -1;
}

int tests::mockRebitClient::SetSource( kre::VariableProvider* varProvider )
{
  varProvider_=varProvider;
  return 1;
}

int tests::mockRebitClient::GetVariableValue( VariableDTO & prop )
{
  int isSuccess=-1;
  if (prop.Id->compare(am3::kModelId)==0)
  {
    prop.Value->clear();
    prop.Value->push_back("0");
    isSuccess=1;
  }
  return isSuccess;
}
