//storage_test.cpp
//

#include <iostream>
using namespace std;

#include "../storage/storage.h"
#include "../storage/cons.h"
//#include "../storage/dataMaker.h"
#include "../storage/simpleTypes.h"
//#include "../models/dataStructures.h"
#include "../storage/makeData.h"


namespace am3 {namespace tests {void RegisterStorage(){};}}

#include "gtest/gtest.h"



using namespace am3::math::storage;

namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

TEST(StorageTest, BasicTest)
{
    const mpr::Coordinates2D coord(0.1,0.2);
	mpr::Time time(0.1);
	const unsigned long int pointId = 123;
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point(pointId,coord,time);

	StorageRAM<mpt::NamedPointsContainerLastTwoSteps2D, double> storage;

    double data1 = 5;
    double data2 = 10;

    storage.StoreData(point, &data1);
    EXPECT_TRUE(storage.IsDataAvailable(point));

    storage.GetState(point, &data2); 
    EXPECT_TRUE(data2 == 5);
}

//***************************************************************************************************************************


//=============================================================================
// OneFieldVisitor
//

template< class T_LIST >
class OneFieldVisitor
{
public:
    static void Visit()     { 
    //  cout << "???" << endl; 
      cout << "DerivedSimpleProperty "; 
      OneFieldVisitor<T_LIST::DerivedFrom>::Visit();
    }
};

template<typename INTERNAL, typename ADDITIONAL>
class OneFieldVisitor<am3::model::Dimensions3D<INTERNAL, ADDITIONAL> >
{
public:
  static void Visit()     { 
    //  cout << "???" << endl; 
    cout << "Dimensions3d "; 
    OneFieldVisitor<am3::model::ComplexProperty<INTERNAL,ADDITIONAL, am3::model::HaveGetSetValue> >::Visit();
  }
};

template<typename INTERNAL, typename ADDITIONAL>
class OneFieldVisitor<am3::model::SpatialDerivative3D<INTERNAL, ADDITIONAL> >
{
public:
  static void Visit()     { 
    //  cout << "???" << endl; 
    cout << "SpatialDerivative3d "; 
    OneFieldVisitor<am3::model::ComplexProperty<INTERNAL,ADDITIONAL, am3::model::HaveGetSetValue> >::Visit();
  }
};

template<>
class OneFieldVisitor<am3::model::util::Null>
{
public:
  static void Visit()     { cout << "util::Null" << endl; }
};

template<>
class OneFieldVisitor<EmptyType>
{
public:
  static void Visit()     { cout << "EmptyType" << endl; }
};

template<>
class OneFieldVisitor<double>
{
public:
    static void Visit()     { cout << "double" << endl; }
};

template<>
class OneFieldVisitor<float>
{
public:
    static void Visit()     { cout << "float" << endl; }
};

template<>
class OneFieldVisitor<int>
{
public:
    static void Visit()     { cout << "int" << endl; }
};

template<typename T, typename T_HAVE_VALUE>
class OneFieldVisitor< am3::model::SimpleProperty<T, T_HAVE_VALUE> >
{
public:
    static void Visit()     
    { 
        cout << "SimpleProperty "; 
        OneFieldVisitor<T>::Visit();
    }
};

template<typename T, typename T_ADDITIONAL_COMPONENETS, typename T_HAVE_VALUE>
class OneFieldVisitor< am3::model::ComplexProperty<T, T_ADDITIONAL_COMPONENETS, T_HAVE_VALUE > >
{
public:
    static void Visit()     
    { 
        cout << "ComplexProperty ";
        OneFieldVisitor<T>::Visit();
        OneFieldVisitor<T_ADDITIONAL_COMPONENETS>::Visit();
    }
};


// template<typename T>
// class OneFieldVisitor< T >
// {
// public:
//   static void Visit()     
//   { 
//     cout << "DerivedSimpleProperty "; 
//     OneFieldVisitor<T::Type>::Visit();
//   }
// };

//===========================================================
// StructVisitor
//

template< class T_LIST >
class StructVisitor;

template< class T_HEAD, class T_TAIL >
class StructVisitor< Typelist<T_HEAD, T_TAIL> >
	: public StructVisitor<T_TAIL>
{
public:
	void VisitList()
	{
        OneFieldVisitor<T_HEAD>::Visit();
		StructVisitor<T_TAIL>::VisitList();
	}
};

template<>
class StructVisitor< Typelist<EmptyType, EmptyType> >
{
public:
	void VisitList()
	{
	}
};


//========================================================================================================================================

typedef cons<
    int,
//    am3::model::SimpleProperty<double,am3::model::HaveGetSetValue>,
//    am3::model::ComplexProperty<double, am3::model::SimpleProperty<int,am3::model::HaveGetSetValue>, am3::model::HaveGetSetValue >,
    float,
    am3::model::SurfaceTension,
    float,
    am3::model::VelocityWithDerivatives,
    int
>::type MyData;


TEST(StorageTest, TypelistTest)
{
    typedef StructSimpleTypes<MyData>::simpleTypes MySimpleTypes;

//    StructVisitor<MyData> visitor;
    StructVisitor<MySimpleTypes> visitor;
	visitor.VisitList();

    cout << endl;
    cout << "sizeof(am3::model::VelocityWithDerivatives) == " << sizeof(am3::model::VelocityWithDerivatives) << endl;
    cout << "sizeof(am3::model::SurfaceTension) == " << sizeof(am3::model::SurfaceTension) << endl;
    cout << endl;

    StructVisitor<MyData> visitor2;
    visitor2.VisitList();
}

//****************************************************************************************************************************
//****************************************************************************************************************************



//-------------------------------------------------------------------
/*-
template< class T_DATA >
class PrintVisitor;

template< class T_HEAD, class T_TAIL >
class PrintVisitor< DataMaker< Typelist<T_HEAD, T_TAIL> > >
    : public PrintVisitor< DataMaker< T_TAIL > >
{
public:
    static void VisitList(char* data)
    {
        T_HEAD& value = (T_HEAD&)(*(data + DataMaker< Typelist<T_HEAD, T_TAIL> >::offset));
        cout << value << endl;

        PrintVisitor< DataMaker< T_TAIL > >::VisitList(data);
    }
};

template<>
class PrintVisitor< DataMaker< Typelist<EmptyType, EmptyType> > >
{
public:
    static void VisitList(char* data)
    {
    }
};
-*/

//-------------------------------------------------------------------


typedef cons<
    double,
    double,
    double
//    float,
//    int
>::type MySimpleTypes;


/*-

typedef DataMaker<MySimpleTypes> MySimpleData;


TEST(StorageTest, DataMaker_Test)
{
    MySimpleData myData;

    cout << endl;
    cout << "sizeof(myData) == " << sizeof(myData) << endl;

    //cout << sizeof(TypeAt<MySimpleTypes, 0>::Result) << endl;
    //cout << sizeof(TypeAt<MySimpleTypes, 1>::Result) << endl;
    //cout << sizeof(TypeAt<MySimpleTypes, 2>::Result) << endl;
    //cout << sizeof(TypeAt<MySimpleTypes, 3>::Result) << endl;

    Field<0>(myData) = 10;
    Field<1>(myData) = 20; // 'Z';
//    Field<2>(myData) = 12.34567890123456789;
//    Field<3>(myData) = 13.34567890123456789;
//    Field<4>(myData) = 12.34567890123456789;

    PrintVisitor<MySimpleData>::VisitList(myData.data_);

//    Field<MySimpleData, 0>(myData) = 10;
//    GetField<myData, 1> = 2.234;

//    StructVisitor<MyData> visitor2;
//    visitor2.VisitList();
}

-*/

//***********************************************************************************************************************

typedef MakeData<MySimpleTypes> MD_Data;

TEST(StorageTest, MakeData_Test)
{
    MD_Data myData;
    MD_Data myData2;

    cout << endl;
    cout << "sizeof(myData) == " << sizeof(myData) << endl;

    //cout << sizeof(TypeAt<MySimpleTypes, 0>::Result) << endl;
    //cout << sizeof(TypeAt<MySimpleTypes, 1>::Result) << endl;
    //cout << sizeof(TypeAt<MySimpleTypes, 2>::Result) << endl;
    //cout << sizeof(TypeAt<MySimpleTypes, 3>::Result) << endl;

    Field<0>(myData) = 10;
    Field<1>(myData) = 13.45; //'Z';
//    DField<2>(myData) = 12.34567890123456789;
//    DField<3>(myData) = 13.34567890123456789;
//    DField<4>(myData) = 12.34567890123456789;

//    PrintVisitor<MySimpleData>::VisitList(myData.data_);

    CopyVisitor<MD_Data> visitor;
    visitor.VisitList(&myData2, &myData);

    cout << Field<0>(myData2) << endl;
    cout << Field<1>(myData2) << endl;
//    cout << DField<2>(myData) << endl;
//    cout << DField<3>(myData) << endl;
//    cout << DField<4>(myData) << endl;


    //    Field<MySimpleData, 0>(myData) = 10;
    //    GetField<myData, 1> = 2.234;

    //    StructVisitor<MyData> visitor2;
    //    visitor2.VisitList();
}
