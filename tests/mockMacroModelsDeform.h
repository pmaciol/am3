
/*! \file mockMacroModelsDeform.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3tests_mockMacroModelsDeform_h__
#define am3tests_mockMacroModelsDeform_h__
/*	include *********************************************************/

#include "../models/point.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;


/*	extern **********************************************************/



/*	classes *********************************************************/

namespace tests
{
  class ConstantStressDeform
  {
  public:
    ConstantStressDeform(const double yieldStress, const double yieldStressDstrain, const double yieldStressDstrainRate)
    { 
      ys(yieldStress); 
      sdstr(yieldStressDstrain); 
      sdstrRate(yieldStressDstrainRate); 
    };
    template<typename T>
    T GetFromDatabus(mpt::Point<mpr::Coordinates2D>* pt)
    {
      const T* trait = nullptr;
      return GetFromDatabus(pt,trait);
    }

    mpr::YieldStress GetFromDatabus(mpt::Point < mpr::Coordinates2D>* point, const mpr::YieldStress* ) { return ys; }
    mpr::StressDStrain GetFromDatabus(mpt::Point < mpr::Coordinates2D>* point, const mpr::StressDStrain* ) { return sdstr; }
    mpr::StressDStrainRate GetFromDatabus(mpt::Point < mpr::Coordinates2D>* point, const mpr::StressDStrainRate* ) { return sdstrRate; }
    
    
  //protected:
    mpr::YieldStress ys;
    mpr::StressDStrain sdstr;
    mpr::StressDStrainRate sdstrRate;
    
    
  };

  //******************************************************************************************************************************************************************

  class SimpleNewIV
    : public mot::OneStepOneElementIvNew
  {
    virtual std::vector<double> GetInternalVariablesNew() const
    {
      std::vector<double> toRet;
      toRet.push_back(0);
      toRet.push_back(1);
      toRet.push_back(2);
      toRet.push_back(3);
      return toRet;
    }
  };
}


#endif // am3tests_mockMacroModelsDeform_h__
