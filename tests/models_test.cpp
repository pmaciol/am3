

namespace am3 {namespace tests {void RegisterModels(){};}}


#include "gtest/gtest.h"
#include <string>
#include <vector>
#include "../models/conversions.h"
#include "../models/point.h"

namespace pro=am3::model;
namespace com=am3::model::common;
namespace con=am3::model::conversions;
namespace mpt=am3::model::point;

using std::vector;
using std::string;


TEST(Conversions, pointFromStringsVector)
{
  vector<string> testingValues;
  testingValues.push_back("0.1");
  testingValues.push_back("0.2");
  pro::Coordinates2D point;
  point=con::PointFromStringsVector(testingValues.begin(),testingValues.end());
  EXPECT_DOUBLE_EQ(point.x(),0.1);
  EXPECT_DOUBLE_EQ(point.y(),0.2);
}

TEST(Conversions, meanPoint4NodesWithBug)
{
  vector<string> testingValues;
  testingValues.push_back("0.1");
  testingValues.push_back("0.2");
  pro::Coordinates2D point;
  point=con::MeanPoint4Nodes(testingValues.begin(),testingValues.end());
  EXPECT_FALSE(point.x()==0.1);
  EXPECT_FALSE(point.y()==0.2);
}

TEST(Conversions, meanPoint4Nodes)
{
  vector<string> testingValues;
  testingValues.push_back("0.0");
  testingValues.push_back("0.0");
  testingValues.push_back("1.0");
  testingValues.push_back("0.5");
  testingValues.push_back("1.0");
  testingValues.push_back("0.0");
  testingValues.push_back("0.0");
  testingValues.push_back("0.5");
  pro::Coordinates2D point;
  point=con::MeanPoint4Nodes(testingValues.begin(),testingValues.end());
  EXPECT_DOUBLE_EQ(point.x(),0.5);
  EXPECT_DOUBLE_EQ(point.y(),0.25);
}

TEST(Conversions, meanPoint4NodesVector)
{
	vector<boost::shared_ptr<mpt::Point<pro::Coordinates2D> > > testingValues;
	pro::Coordinates2D coord;
	pro::Time t(0.1);
	
	coord.x(0.0);coord.y(0.0);
	boost::shared_ptr<mpt::Point<pro::Coordinates2D> > pointPtr1(new mpt::ComputedPoint<pro::Coordinates2D>(coord,t));
	coord.x(1);coord.y(0);
	boost::shared_ptr<mpt::Point<pro::Coordinates2D> > pointPtr2(new mpt::ComputedPoint<pro::Coordinates2D>(coord,t));
	coord.x(1);coord.y(1);
	boost::shared_ptr<mpt::Point<pro::Coordinates2D> > pointPtr3(new mpt::ComputedPoint<pro::Coordinates2D>(coord,t));
	coord.x(0);coord.y(1);
	boost::shared_ptr<mpt::Point<pro::Coordinates2D> > pointPtr4(new mpt::ComputedPoint<pro::Coordinates2D>(coord,t));

	testingValues.push_back(pointPtr1);
	testingValues.push_back(pointPtr2);
	testingValues.push_back(pointPtr3);
	testingValues.push_back(pointPtr4);

 	coord=con::MeanPoint4Nodes(testingValues);
 	EXPECT_DOUBLE_EQ(coord.x(),0.5);
 	EXPECT_DOUBLE_EQ(coord.y(),0.5);
}

TEST(Conversions, velocityFromStringsVector)
{
  vector<string> testingValues;
  testingValues.push_back("0.1");
  testingValues.push_back("0.2");
  pro::Velocity2D vel;
  vel=con::VelocityFromStringsVector(testingValues.begin(),testingValues.end());
  EXPECT_DOUBLE_EQ(vel.x(),0.1);
  EXPECT_DOUBLE_EQ(vel.y(),0.2);
}

TEST(Conversions, stressFromStringsVector)
{
  vector<string> testingValues;
  testingValues.push_back("0.1");
  testingValues.push_back("0.2");
  testingValues.push_back("0.3");
  testingValues.push_back("0.4");
  pro::Stress2D stress;
  stress=con::StressFromStringsVector(testingValues.begin(),testingValues.end());
  EXPECT_DOUBLE_EQ(stress.g11(),0.1);
  EXPECT_DOUBLE_EQ(stress.g12(),0.2);
  EXPECT_DOUBLE_EQ(stress.g21(),0.3);
  EXPECT_DOUBLE_EQ(stress.g22(),0.4);
}

TEST(Conversions, strainRateFromStringsVector)
{
  vector<string> testingValues;
  testingValues.push_back("0.1");
  testingValues.push_back("0.2");
  testingValues.push_back("0.3");
  testingValues.push_back("0.4");
  pro::StrainRate2D strainRate;
  strainRate=con::StrainRateFromStringsVector(testingValues.begin(),testingValues.end());
  EXPECT_DOUBLE_EQ(strainRate.g11(),0.1);
  EXPECT_DOUBLE_EQ(strainRate.g12(),0.2);
  EXPECT_DOUBLE_EQ(strainRate.g21(),0.3);
  EXPECT_DOUBLE_EQ(strainRate.g22(),0.4);
}
