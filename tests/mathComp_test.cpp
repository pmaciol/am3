namespace am3 {namespace tests {void RegisterMathComp(){};}}

#include "gtest/gtest.h"
#include <vector>

#include "../utilMath/trilinearInterpolation.h"
#include "../utilMath/linearInterpolation1D.h"
#include "../utilMath/geometryOperations.h"
#include "../models/description.h"
#include "../models/point.h"
#include "../models/dataStructures.h"



namespace apr = am3::math::approximation;
namespace mip=am3::math::interpolation;
namespace mig=am3::math::geometry;
namespace mpr=am3::model::properties;
namespace pnt=am3::model::point;

TEST(ApproximationTests, linearInterpolation)
{
	typedef apr::LinearInterpolation1D<mpr::Temperature, mpr::Time> Interpolation;
	Interpolation interp;
	mpr::Temperature temp;
	mpr::Time t;

	temp(0); t(0);
	interp.AddNode(t, temp);

	temp(100); t(1);
	interp.AddNode(t, temp);

	temp(200); t(2);
	interp.AddNode(t, temp);
	
	t(0.5);
	temp = interp.GetApproximation(t);
	EXPECT_DOUBLE_EQ(50.0, temp());

	t(1.5);
	temp = interp.GetApproximation(t);
	EXPECT_DOUBLE_EQ(150.0, temp());

	t(1);
	temp = interp.GetApproximation(t);
	EXPECT_DOUBLE_EQ(100.0, temp());

	t(0);
	temp = interp.GetApproximation(t);
	EXPECT_DOUBLE_EQ(0.0, temp());

	t(2);
	temp = interp.GetApproximation(t);
	EXPECT_DOUBLE_EQ(200.0, temp());

	t(3);
	EXPECT_THROW(temp = interp.GetApproximation(t), std::range_error);
	
	t(-1);
	EXPECT_THROW(temp = interp.GetApproximation(t), std::range_error);
}

TEST(GeomtertyOperationsTests, isInPolygon)
{
  std::vector<mpr::Coordinates2D> vertexes;
  vertexes.push_back(mpr::Coordinates2D(0,0));
  vertexes.push_back(mpr::Coordinates2D(0,1));
  vertexes.push_back(mpr::Coordinates2D(1,1));
  vertexes.push_back(mpr::Coordinates2D(1,0));
  mpr::Coordinates2D coord(0.5,0.5);
  mpr::Time t(0.0);
  pnt::ComputedPoint<mpr::Coordinates2D> point(coord,t);
  EXPECT_TRUE(mig::IsPointInPolygon(point,vertexes));
  
  coord.x(2.0);
  pnt::ComputedPoint<mpr::Coordinates2D> point2(coord,t);
  EXPECT_FALSE(mig::IsPointInPolygon(point2,vertexes));
}

TEST(TrilinearInterpolation, rescale)
{
  std::vector<std::vector<std::vector<double> > > A;

  // Assign values to the elements
  int values = 0;
  A.resize(3);
  A[0].resize(2);
  A[1].resize(2);
  A[2].resize(2);
  A[0][0].resize(2);
  A[0][1].resize(2);
  A[1][0].resize(2);
  A[1][1].resize(2);
  A[2][0].resize(2);
  A[2][1].resize(2);
  for(int i = 0; i != 3; ++i)
  {
    for(int j = 0; j != 2; ++j)
      for(int k = 0; k != 2; ++k)
        A[i][j][k] = values++;
  }
 mip::TrilinearRegularInterpolation inter;
 inter.SetSize(3,2,2);
 std::vector<double> one,two,three;
 one.push_back(0);
 one.push_back(1);
 one.push_back(2);
 two.push_back(0);
 two.push_back(1);
 three.push_back(0);
 three.push_back(1);
 inter.SetGridPoints(one,two,three);
 inter.SetValues(A);

 mip::Point<3> p(0.5,0.5,0.5);
 double value=inter.GetValue(p).value;
 EXPECT_NEAR(4.0,inter.GetValue(p).x_derivative,0.01);
 EXPECT_NEAR(2.0,inter.GetValue(p).y_derivative,0.01);
 EXPECT_NEAR(1.0,inter.GetValue(p).z_derivative,0.01);
 EXPECT_NEAR(3.5,value,0.01);
 mip::Point<3> p2(1.5,0.5,0.5);
 value=inter.GetValue(p2).value;
 EXPECT_NEAR(7.5,value,0.01);
 EXPECT_NEAR(4.0,inter.GetValue(p).x_derivative,0.01);
 EXPECT_NEAR(2.0,inter.GetValue(p).y_derivative,0.01);
 EXPECT_NEAR(1.0,inter.GetValue(p).z_derivative,0.01);
  mip::Point<3> p3(1.99,0.01,0.01);
  value=inter.GetValue(p3).value;
  EXPECT_NEAR(7.99,value,0.01);
  EXPECT_NEAR(4.0,inter.GetValue(p).x_derivative,0.01);
  EXPECT_NEAR(2.0,inter.GetValue(p).y_derivative,0.01);
  EXPECT_NEAR(1.0,inter.GetValue(p).z_derivative,0.01);
}

TEST(TrilinearInterpolation, rescaleDeriv)
{
  std::vector<std::vector<std::vector<double> > > A;

  // Assign values to the elements
  A.resize(2);
  A[0].resize(2);
  A[1].resize(2);
  A[0][0].resize(2);
  A[0][1].resize(2);
  A[1][0].resize(2);
  A[1][1].resize(2);


  A[0][0][0]=0;
  A[1][0][0]=0;
  A[0][0][1]=0;
  A[1][0][1]=0;
  A[0][1][0]=1;
  A[1][1][0]=1;
  A[0][1][1]=1;
  A[1][1][1]=1;


  mip::TrilinearRegularInterpolation inter;
  inter.SetSize(2,2,2);
  std::vector<double> one,two,three;
  one.push_back(0);
  one.push_back(1);
  two.push_back(0);
  two.push_back(1);
  three.push_back(0);
  three.push_back(1);
  inter.SetGridPoints(one,two,three);
  inter.SetValues(A);

  mip::Point<3> p(0.5,0.01,0.5);
  double value=inter.GetValue(p).value;
  EXPECT_NEAR(0.0,inter.GetValue(p).x_derivative,0.01);
  EXPECT_NEAR(1.0,inter.GetValue(p).y_derivative,0.01);
  EXPECT_NEAR(0.0,inter.GetValue(p).z_derivative,0.01);
  EXPECT_DOUBLE_EQ(0.01,value/*,0.01*/);
  mip::Point<3> p2(0.1,0.5,0.5);
  value=inter.GetValue(p2).value;
  EXPECT_NEAR(0.0,inter.GetValue(p).x_derivative,0.01);
  EXPECT_NEAR(1.0,inter.GetValue(p).y_derivative,0.01);
  EXPECT_NEAR(0.0,inter.GetValue(p).z_derivative,0.01);
  EXPECT_DOUBLE_EQ(0.5,value/*,0.01*/);
}