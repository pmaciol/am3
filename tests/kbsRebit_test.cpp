/*! \file kbsRebit_test.cpp **************************************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#include "gtest/gtest.h"
#include "thermostatGSoapReasoner.h"
#include "../kbsRebit/BasicHttpBinding_USCOREIRebitEngineService.nsmap"
#include "../kbsRebit/gSoapReasoningService.h"

namespace am3r = am3::gsoap;

using namespace std;
using namespace am3::gsoap;

// In order to pass all these tests you should start REBIT engine (file: ConsoleRebitHost.exe) as an administrator.
// The REBIT service is available at http://localhost:8888/RebitEngineService. 
// If you want the change this URI open file "EngineSettings.config" and edit the appropriate key.
// Two of these tests use knowledegebase "thermostat_KB.xml". 
// It is assumed that this knowledgebase is stored in the file "C:/thermostat_KB.xml"


// Tests connection with REBIT engine
TEST(DISABLED_ResoningService, Connection)
{
	string serviceEndPoint = "http://localhost:8888/RebitEngineService";	// address of the rebit service

	// create GSoapReasoner and check connection with REBIT
	am3r::ReasoningServiceProxy *proxy = new am3r::GSoapReasoningService(serviceEndPoint);
	bool result = proxy->Hello();
	
	//checks if Hello command (send to Rebit Server) finished without errors
	ASSERT_TRUE(result) << "Connection to Rebit failed!";
}

// The static case, i.e. all data requiered by the inference engine are available before starting of inference process
// In such case you can use general gSoapResoner. After reading knowledegbase, initializing the reasoner 
// you have to set static values to all potential variables requeired in inference process (SetVariable())
TEST(DISABLED_ThermostatKB, StaticDataProvider)
{
	string serviceEndPoint = "http://localhost:8888/RebitEngineService";	// address of the rebit service
	string thermostatPath = "c:/thermostat_KB.xml";	// full path for thermostat KB
	string ruleSetId = "ThermostatSettingRules";		// identifier of ruleset in KB
	string finalVariableId = "thermostatSetting";		// id of final variable 
	
	// reads thermostat knowledgebase
	string kb = GetXmlFromFile(thermostatPath);
	
	// creates GSoapReasoner and set static facts (method GetVariableValue() will use these facts during inference process)
	Reasoner *mixReasoner = new Reasoner(new GSoapReasoningService(serviceEndPoint), new ScreenLogger());
	mixReasoner->SetVariable(new string("day"), new string("Monday"));
	mixReasoner->SetVariable(new string("time"), new string("11"));
	mixReasoner->SetVariable(new string("month"), new string("January"));

	// start inference process
	ResponseDTO *resp = mixReasoner->Run(kb, ruleSetId, finalVariableId);

	// test if inference process finished without errors
	ASSERT_TRUE(resp != NULL) << "Reasoning on Rebit Server failed!";
	ASSERT_TRUE(*resp->Status == 0) << "Reasoning on Rebit Server failed!";
	ASSERT_TRUE(resp->InteractionVariables != NULL) << "Reasoning on Rebit Server failed!";
	ASSERT_TRUE(resp->InteractionVariables->size() > 0) << "Reasoning on Rebit Server failed!";
	ASSERT_TRUE((*resp->InteractionVariables)[0].Value != NULL) << "Reasoning on Rebit Server failed!";
	ASSERT_TRUE((*resp->InteractionVariables)[0].Value->size() > 0) << "Reasoning on Rebit Server failed!";

	// reads the value of final variable and map it to string
	VariableDTO *varDTO = &(*resp->InteractionVariables)[0];
	string result = (*varDTO->Value)[0];
	string expected = "24";

	// tests if the value of final variable is correct
	ASSERT_STREQ(result.c_str(), expected.c_str()) << "The value of variable \"thermostatSetting\" is not as expected!";

	//delete [] resp;
}

// Dynamic case - retrieving variable values generates high costs, so the value of requiered variables 
// are retrieved (or calculated) not at the begininning of the inference process but on demand. 
// In this case you have to create dedicated class (ThermostatGSoapReasoner) and and override method for data retrieving (TemplateGetVariableValue())
TEST(DISABLED_ThermostatKB, DynamicDataProvider)
{
	string serviceEndPoint = "http://localhost:8888/RebitEngineService";
	string thermostatPath = "c:/thermostat_KB.xml";
	string ruleSetId = "ThermostatSettingRules";
	string finalVariableId = "thermostatSetting";

	// reads thermostat knowledgebase
	string kb = am3r::GetXmlFromFile(thermostatPath);
	
	// creates ThermostatGSoapReasoner, i.e. a Reasoner with overriden GetVariableValue() method for data retrieving during inference process)
	Reasoner *mixReasoner = new tests::ThermostatGSoapReasoner(new GSoapReasoningService(serviceEndPoint), new am3r::NullLogger()); // change to NullLogger() if do not want screen prints 

	// starts inference process
	ResponseDTO *resp = mixReasoner->Run(kb, ruleSetId, finalVariableId); 

	// test if inference process finished without errors
	ASSERT_TRUE(resp != NULL) << "Reasoning on Rebit Server failed!";
	ASSERT_TRUE(*resp->Status == 0) << "Reasoning on Rebit Server failed!";
	ASSERT_TRUE(resp->InteractionVariables != NULL) << "Reasoning on Rebit Server failed!";
	ASSERT_TRUE(resp->InteractionVariables->size() > 0) << "Reasoning on Rebit Server failed!";
	ASSERT_TRUE((*resp->InteractionVariables)[0].Value != NULL) << "Reasoning on Rebit Server failed!";
	ASSERT_TRUE((*resp->InteractionVariables)[0].Value->size() > 0) << "Reasoning on Rebit Server failed!";

	// reads the value of final variable and map it to string
	VariableDTO *varDTO = &(*resp->InteractionVariables)[0];	
	string result = (*varDTO->Value)[0];
	string expected = "24";

	// tests if the value of final variable is correct
	ASSERT_STREQ(result.c_str(), expected.c_str()) << "The value of variable \"thermostatSetting\" is not as expected!";
}