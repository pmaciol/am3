ADD_DEFINITIONS( -DTESTS_ON=1 ) 

if(NOT CASETRIVIAL)

if (MATCALC)
  SET(MATCALCLIB am3softwareMatCalc )
  SET(MATCALC_DEPENDENT_FILES )
  add_definitions(-DMATCALC)
else()
  SET(MATCALCLIB)
  SET(MATCALC_DEPENDENT_FILES)
endif()

add_executable(am3tests 
  main.cpp 
  tests.h 
#  systemApi_test.cpp 
#  extProgOpt_test.cpp 
#  config_test.cpp
#  models_test.cpp 
#	modelsStorage_test.cpp 
#  modelPrecipitations_test.cpp
#  modelStress_test.cpp
#	namedPoint_test.cpp
#  kbs_test.cpp
#  modelDislocations_test.cpp
mesh_test.cpp
mathComp_test.cpp

# caseColdRolling_test.cpp caseColdRolling_test.h
# caseColdRolling_TestModel.h caseColdRolling_TestModel.cpp
# caseTrivialModels_test.cpp
# caseTugMatCalc_test.cpp
  ${MATCALC_DEPENDENT_FILES}

#  mockMacroModels.h mockMacroModels.cpp mockMacroModels_test.cpp macroDataFromFile.dat

 # deformHandler_test.cpp
 softwareMatCalcMain_test.cpp 
 # softwareMatCalcMain_test.h

 # mainForProfiler.cpp
 # modelPatterns_test.cpp modelPatterns_test.h
#  softwareDeform_test.h softwareDeform_test.cpp

#  caseEsaform2015_MpiTest.cpp
variableSource_test.cpp
#communicationProvider_test.cpp
#communicationMPI_test.cpp
#caseMatCalcMPI_test.cpp
 )



target_link_libraries(am3tests 
am3common  
am3logger
am3utilMath
am3utilMesh
# am3models 
# am3softwareDeformBuiltin
# am3softwareDeform


# am3modelPrecipitation
# am3modelDislocations
# am3communicationProvider
# am3communicationMPI
${MATCALCLIB}

msmpi.lib
gtest

)

if($<CXX_COMPILER_ID:MSVC>)
  SET(CMAKE_EXE_LINKER_FLAGS /NODEFAULTLIB:\"LIBCMTD.lib;libcpmtd.lib;LIBCMT.lib;libcpmt.lib;\")
endif()

ENDIF() #CASERTIVIAL

