
/*! \file modelCalulations.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

/*	include *********************************************************/

#include "gtest/gtest.h"
//#include "../basicModels/basicModels.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/


namespace am3 {namespace tests {void RegisterModelCalulations(){};}}

//void RegisterModelCalulations(){};

namespace am3
{
  namespace model
  {

      TEST(DataStructures,complexity)
      {
        double temp;
        Velocity tmp;
        tmp.x(1.0);
        temp=tmp.x();
        EXPECT_DOUBLE_EQ(1.0,temp);
      }
  } //model
} //am3


