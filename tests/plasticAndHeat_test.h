
/*! \file plasticAndHeat_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef plasticAndHeat_test_h__
#define plasticAndHeat_test_h__
/*	include *********************************************************/

#include "../knowledgeRebit/rebitKnowledgeStructs.h"
#include "../problemPlasticAndHeat/problemPlasticAndHeat.h"
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "../models/models.h"

/*	using ***********************************************************/

namespace kre=am3::knowledge::rebit;
namespace pah=am3::problem::plasticAndHeat;
namespace mpr=am3::model::properties;
namespace mpt=am3::model::point;
namespace mo=am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace tests
{
  class mockFineModel : public 
		  pah::PlasticAndHeat2DFineContract
		, mo::MultiplyModelAvailable
  {
  public:
    mpr::YieldStress GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::YieldStress *const );
    mpr::StressDStrain GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StressDStrain *const );
    mpr::StressDStrainRate GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StressDStrainRate *const );


		virtual int Run(){return -1;};

		virtual int PostProcess(){return -1;};

		virtual void Configure(){};

		virtual void PrepareForRun(){};

		virtual void PrepareEnvironment(){};

		virtual void PrepareInputFiles(){};

  };

  class mockCoarseModel : public 
		 pah::PlasticAndHeat2DCoarseContract
		, public mo::SingleModelAvailable<mockCoarseModel>

  {
  public:
		mockCoarseModel():mod::SingleModelAvailable<mockCoarseModel>("mockCoarseModel"){};
    mpr::TotalEffectiveStrain GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::TotalEffectiveStrain *const );
    mpr::StrainRate2D GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StrainRate2D *const );
    mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::EffectiveStrainRate *const );
    mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Temperature *const );
    mpr::Coordinates2D GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Coordinates2D *const );
    mpr::Time GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Time *const );
    mpr::ElementNumberGlobal GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::ElementNumberGlobal *const );


		virtual int Run(){return -1;};

		virtual int PostProcess(){return -1;};

		virtual void Configure(){};

		virtual void PrepareForRun(){};

		virtual void PrepareEnvironment(){};

		virtual void PrepareInputFiles(){};

  };

  class mockRebitClient: public kre::RebitClient
  {
  public:
    virtual int SetFact( const VariableDTO& mpr );
    virtual int SetSource( kre::VariableProvider* varProvider );
    virtual int GetVariableValue( VariableDTO & mpr );
  protected:
    kre::VariableProvider* varProvider_;
  };
} //tests

#endif // plasticAndHeat_test_h__
