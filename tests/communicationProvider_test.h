/*! \file communicationProvider_test.h **************************************************
 * \author		Kazimierz Michalik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef tests_communicationProvider_test_h__
#define tests_communicationProvider_test_h__
/*	include *********************************************************/

#include "gtest/gtest_prod.h"
#include "src/communicationProvider/predefinedStreams.h"

/*	using ***********************************************************/

using namespace am3::common::communication;

/*	extern **********************************************************/



/*	classes *********************************************************/



#endif // tests_communicationSocket_test_h__
