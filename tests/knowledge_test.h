
/*! \file knowledge_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef knowledge_test_h__
#define knowledge_test_h__
/*	include *********************************************************/

//#include "../models/description.h"
#include "../softwareAdina/adinaDataInterface.h"

/*	using ***********************************************************/

//using am3::model::Property;
//using am3::model::Descriptive;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace knowledge
  {
//////////////////////////////////////////////////////////////////////////
    // Is this really needed?
    //{
    class PropertyViscosity
    {
    public:
 //     Property<double> viscosity;
    };

//////////////////////////////////////////////////////////////////////////
    class HaveViscosity
    {
    public:
//      virtual Property<double>& Viscosity()=0;
    };
//////////////////////////////////////////////////////////////////////////
    class HaveNeeds
    {
    public:
//       virtual PropertySet& Needs()=0;
//       virtual bool Check(PropertySet& needs)=0;
    };
//////////////////////////////////////////////////////////////////////////
    class HaveResults
    {
    public:
/*      virtual PropertySet& Results()=0;*/
    };
    ///} Is this really needed?

  } //knowledge
} //am3

#endif // knowledge_test_h__
