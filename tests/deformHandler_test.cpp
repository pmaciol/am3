
/*! \file caseVectorTug_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "mockMacroModels.h"
// #include "../models/dataStructures.h"
// #include "../models/macrosSimplifiedModels.h"
// #include "../softwareDeformBuiltin/deformHandler.h"
// #include "../caseVectorTUG/caseVectorTUG.h"
// #include "../caseVectorTUG/deformAdapter.h"
// #include "../caseVectorTUG/dislocationsModel.h"
// #include "../caseVectorTUG/dislocationsModel_2.h"
// #include "../caseVectorTUG/precipitationsModel.h"
// #include "../caseVectorTUG/stressModel.h"
// #include "../caseVectorTUG/internalKbs.h"
// #include "caseVectorTUG_test.h"

/*	using ***********************************************************/
// 
namespace sdm = am3::software::deform;
// namespace mpr = am3::model::properties;
// namespace cvt = am3::cases::vectortug;
// namespace mpt = am3::model::point;
// namespace tsv = tests::caseVectorTug;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterDeformHandler (){};}}

const double yieldStressCheck = 123.0;
const double yieldStressDStrainCheck = 12.30;
const double yieldStressDStrainRateCheck = 1.230;
const int nodesNumber=6;
const int nodesInElem=4;
const int elementsNumber=2;
const int connectivityMatrix[10]={0,0,1,2,3,1,1,4,5,2};
const double coordinates[12]={0,0,1,0,1,1,0,1,2,0,2,1};
const double displacements[12]={0,0,0,0,0,0,0,0,0,0,0,0};
const double time1 = 0;
const double timeStep = 0.01;
const int elementId = 0;
const int meshId = 0;
const double time2 = 0.1;


TEST(DeformBuiltIn, SetMesh_FromDeform)
{
  sdm::DeformHandler<mpr::Coordinates2D>::NamedPoint::ClearAllPoints();
  boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tests::ConstantStress(yieldStressCheck,yieldStressDStrainCheck,yieldStressDStrainRateCheck));
  boost::shared_ptr<sdm::DeformHandlerMesh<mpr::Coordinates2D> > handler(new sdm::DeformHandlerMesh<mpr::Coordinates2D>(ySmodelPtr));
  boost::shared_ptr<sdm::DeformHandlerUserProcMesh> handlerPtr(handler);
  /*ASSERT_TRUE int to bool*/ASSERT_EQ(handlerPtr->SetMesh(coordinates, displacements, time1, timeStep, elementsNumber, nodesNumber, connectivityMatrix), 1);
  /*ASSERT_TRUE int to bool*/ASSERT_EQ(handlerPtr->SetElement(time1, meshId, 0), 1);
  ASSERT_DOUBLE_EQ(handler->GetNodalPoint(2).Coordinates().x(),coordinates[4]);
  ASSERT_DOUBLE_EQ(handler->GetNodalPoint(2).Coordinates().y(),coordinates[5]);
  /*ASSERT_TRUE int to bool*/ASSERT_EQ(handlerPtr->SetElement(time1, meshId, 1), 1);
  ASSERT_DOUBLE_EQ(handler->GetNodalPoint(2).Coordinates().x(),coordinates[10]);
  ASSERT_DOUBLE_EQ(handler->GetNodalPoint(2).Coordinates().y(),coordinates[11]);
}

TEST(DeformBuiltIn, SetMesh_TooMuchNodes)
{
  boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tests::ConstantStress(yieldStressCheck,yieldStressDStrainCheck,yieldStressDStrainRateCheck));
  boost::shared_ptr<sdm::DeformHandlerUserProcMesh> handler(new sdm::DeformHandlerMesh<mpr::Coordinates2D>(ySmodelPtr));
  ASSERT_FALSE(handler->SetMesh(coordinates,displacements,time1,timeStep,elementsNumber,kMaxNodesFromDeform * 2,connectivityMatrix));
}


TEST(DeformBuiltIn,SetElementalNodes)
{
  const double coordinates2[8]={1,0,1,1,0,1,0,0};

  sdm::DeformHandler<mpr::Coordinates2D>::NamedPoint::ClearAllPoints();
  boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tests::ConstantStress(yieldStressCheck,yieldStressDStrainCheck,yieldStressDStrainRateCheck));
  boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > handler(new sdm::DeformHandler<mpr::Coordinates2D>(ySmodelPtr));
  boost::shared_ptr<sdm::DeformHandlerUserProc> handlerPtr(handler);

  handlerPtr->SetElementalNodes(nodesInElem,coordinates,time1, meshId,elementId);
  mpr::ElementNodesNumber nodesMaxNumber = handler->GetNodesNumber();
  EXPECT_EQ(nodesInElem,nodesMaxNumber());
  mpt::NamedPointLastTwoSteps2D point1=handler->GetNodalPoint(0);
  mpr::Coordinates2D coordCheck1(coordinates[0],coordinates[1]);
  EXPECT_TRUE(coordCheck1==point1.Coordinates());
  mpt::NamedPointLastTwoSteps2D point2=handler->GetNodalPoint(1);
  mpr::Coordinates2D coordCheck2(coordinates[2],coordinates[3]);
  EXPECT_TRUE(coordCheck2==point2.Coordinates());
  mpt::NamedPointLastTwoSteps2D pointMid=(*handler->GetMidPoint());
  mpr::Coordinates2D coordCheckMid(0.5,0.5);
  EXPECT_TRUE(coordCheckMid==pointMid.Coordinates());

  handlerPtr->SetElementalNodes(nodesInElem,coordinates2,time2, meshId,elementId+1);
  mpt::NamedPointLastTwoSteps2D point1New=handler->GetNodalPoint(0);
  mpr::Coordinates2D coordCheck1New(coordinates2[0],coordinates2[1]);
  EXPECT_TRUE(coordCheck1New==point1New.Coordinates());
}


TEST(DeformBuiltIn, SetTemperature)
{
  sdm::DeformHandler<mpr::Coordinates2D>::NamedPoint::ClearAllPoints();
  boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tests::ConstantStress(yieldStressCheck,yieldStressDStrainCheck,yieldStressDStrainRateCheck));

  boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > handler(new sdm::DeformHandler<mpr::Coordinates2D>(ySmodelPtr));
  boost::shared_ptr<sdm::DeformHandlerUserProc> handlerPtr(handler);

//   boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > handler(new sdm::DeformHandler<mpr::Coordinates2D>(ySmodelPtr));
//   boost::shared_ptr<sdm::DeformHandlerUserProc> handlerPtr(handler);

  handler->SetElementalNodes(nodesInElem,coordinates,time1, meshId,elementId);
  mpt::NamedPointLastTwoSteps2D point1=handler->GetNodalPoint(0);

  const double temperature = 893;
  handlerPtr->SetTemperature(&temperature,	meshId,	elementId);
  mpt::NamedPointLastTwoSteps2D point2=(*handler->GetMidPoint());
  EXPECT_DOUBLE_EQ(temperature,(handler->GetTemperatureIn(point1)).Value());
  EXPECT_DOUBLE_EQ(temperature,(handler->GetTemperatureIn(point2)).Value());
}

TEST(DeformBuiltIn, SetStepLength)
{
  sdm::DeformHandler<mpr::Coordinates2D>::NamedPoint::ClearAllPoints();
  boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tests::ConstantStress(yieldStressCheck,yieldStressDStrainCheck,yieldStressDStrainRateCheck));
  boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > handler(new sdm::DeformHandler<mpr::Coordinates2D>(ySmodelPtr));
  boost::shared_ptr<sdm::DeformHandlerUserProc> handlerPtr(handler);

  handlerPtr->SetElementalNodes(nodesInElem,coordinates,time1, meshId,elementId);

  const double timeStepLength = 0.001;
  handlerPtr->SetStepLength(&timeStepLength,	meshId,	elementId);
  EXPECT_DOUBLE_EQ(timeStepLength,(handler->GetStepLength()).Value());
}

TEST(DeformBuiltIn, SetEffectiveStrainRate)
{
  sdm::DeformHandler<mpr::Coordinates2D>::NamedPoint::ClearAllPoints();
  boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tests::ConstantStress(yieldStressCheck,yieldStressDStrainCheck,yieldStressDStrainRateCheck));
  boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > handler(new sdm::DeformHandler<mpr::Coordinates2D>(ySmodelPtr));
  boost::shared_ptr<sdm::DeformHandlerUserProc> handlerPtr(handler);

  handlerPtr->SetElementalNodes(nodesInElem,coordinates,time1, meshId,elementId);
  mpt::NamedPointLastTwoSteps2D point1=handler->GetNodalPoint(0);

  mpr::Coordinates2D coordCheck1(coordinates[0],coordinates[1]);
  EXPECT_TRUE(coordCheck1==point1.Coordinates());

  const double strainRate = 0.1;
  handlerPtr->SetEffectiveStrainRate(&strainRate,	meshId,	elementId);
  mpt::NamedPointLastTwoSteps2D point2=(*handler->GetMidPoint());
  EXPECT_DOUBLE_EQ(strainRate,(handler->GetEffectiveStrainRateIn(point1)).Value());
  EXPECT_DOUBLE_EQ(strainRate,(handler->GetEffectiveStrainRateIn(point2)).Value());
}

TEST(DeformBuiltIn, GetYieldStress)
{
  sdm::DeformHandler<mpr::Coordinates2D>::NamedPoint::ClearAllPoints();
  boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tests::ConstantStress(yieldStressCheck,yieldStressDStrainCheck,yieldStressDStrainRateCheck));
  boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > handler(new sdm::DeformHandler<mpr::Coordinates2D>(ySmodelPtr));
  boost::shared_ptr<sdm::DeformHandlerUserProc> handlerPtr(handler);

  handlerPtr->SetElementalNodes(nodesInElem,coordinates,time1, meshId,elementId);

  int userVariablesNumber=8;
  double userVariablesValues[8] = {1,2,3,4,5,6,7,8};	
  handlerPtr->SetElementalVariables(userVariablesNumber,userVariablesValues,meshId,elementId);
  const double strainRate = 0.1;
  handlerPtr->SetEffectiveStrainRate(&strainRate,	meshId,	elementId);
  const double temperature = 893;
  handlerPtr->SetTemperature(&temperature,	meshId,	elementId);

  double yieldStress=0;
  double yieldStressDStrain=0;
  double yieldStressDStrainRate=0;

  yieldStress = handlerPtr->GetYieldStress( meshId, elementId);
  EXPECT_DOUBLE_EQ(yieldStressCheck,yieldStress);
  yieldStressDStrain = handlerPtr->GetYieldStressDStrain(meshId, elementId);
  EXPECT_DOUBLE_EQ(yieldStressDStrainCheck,yieldStressDStrain);
  yieldStressDStrainRate = handlerPtr->GetYieldStressDStrainRate(meshId, elementId);
  EXPECT_DOUBLE_EQ(yieldStressDStrainRateCheck,yieldStressDStrainRate);
}

TEST(DeformBuiltIn,SetElementalVariables)
{
  sdm::DeformHandler<mpr::Coordinates2D>::NamedPoint::ClearAllPoints();
  boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tests::ConstantStress(yieldStressCheck,yieldStressDStrainCheck,yieldStressDStrainRateCheck));
  boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> > handler(new sdm::DeformHandler<mpr::Coordinates2D>(ySmodelPtr));
  boost::shared_ptr<sdm::DeformHandlerUserProc> handlerPtr(handler);

  int userVariablesNumber=8;
  double userVariablesValues[8] = {1,2,3,4,5,6,7,8};	
//  double userVariablesValuestoCheck[8];	
  handlerPtr->SetElementalVariables(userVariablesNumber,userVariablesValues,meshId,elementId);
  EXPECT_DOUBLE_EQ(userVariablesValues[0],handlerPtr->GetElementalVariables(0));
  double userVariablesValues2[8] = {2,3,4,5,6,7,8,9};	
  handlerPtr->SetElementalVariables(userVariablesNumber,userVariablesValues2,meshId,elementId);
  EXPECT_DOUBLE_EQ(userVariablesValues2[0],handlerPtr->GetElementalVariables(0));
}