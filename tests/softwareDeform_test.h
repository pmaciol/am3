
/*! \file softwareDeform_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3tests_softwareDeform_test_h__
#define am3tests_softwareDeform_test_h__
/*	include *********************************************************/

#include <vector>
#include "../modelTemplates/innerSideInterfacesMacro.h"

/*	using ***********************************************************/

namespace mot = am3::model::templates;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace tests
{
  // This structure emulates real submodels updating IV values
  struct MockNewIvSource
    : public mot::OneStepOneElementIvNew
  {
    virtual std::vector<double> GetInternalVariablesNew() const { return toRet; }
    void Resize(const std::size_t size){ toRet.resize(size); };
    std::vector < double > toRet;
  };
}
#endif // am3tests_softwareDEform_test_h__
