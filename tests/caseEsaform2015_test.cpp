
/*! \file caseEsaform2015_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../caseEsaform2015/caseEsaform2015.h"
#include "../databus/databusTypelists.h"

//#include "../caseEsaform2015/matcalcMPI.h"

/*	using ***********************************************************/

using namespace am3::cases::esaform2015;
namespace da = am3::databus;
namespace sdm = am3::software::deform;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterCaseEsaform2015 (){};}}

TEST(CaseEsaform2015, MacroStateTest)
{
  // Main 'databus' for the case; necessary for submodel
  DATABUS_INSTANCE(mainDatabus, CaseEsaform2015Databus);

  // StressProxy; gets stress and stress's derivatives to macroscale software (FEM)
  MODEL_INSTANCE_REDESIGN(mst::StressProxy, StressProxyModel, CaseEsaform2015Databus, stressReturn, mainDatabus);

  // Deform handler; source of stresses must be provided
  boost::shared_ptr<sdm::OneStepOneElementNoElementCheckStresses> stateProvider(new sdm::OneStepOneElementNoElementCheckStresses(stressReturn));

  // Submodel; handler of Deform procedure
  MODEL_INSTANCE_WITH_PARAM(MacroModelState, MacroModelStateModel, CaseEsaform2015Databus, macroModelState, mainDatabus, stateProvider);

  // testing values
  const double temperature = 500;
  const double stepLength = 0.001;
  const double effStrRate = 0.1;
  const int elementId = 0;
  const int meshId = 0;

  // This is usually done by Deform
  stateProvider->SetTemperature(&temperature, meshId, elementId);
  stateProvider->SetStepLength(&stepLength, meshId, elementId);
  stateProvider->SetEffectiveStrainRate(&effStrRate, meshId, elementId);

  // The point, necessary for Get<VAR> interface. In this model the point is ignored inside the submodel
  const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
  const mpr::Time t = mpr::Time(0.0);
  pnt::ComputedPoint<mpr::Coordinates2D> point = pnt::ComputedPoint<mpr::Coordinates2D>(coord, t);

  // Testing
  mpr::Temperature temperatureBack;
  temperatureBack = macroModelState->Get<mpr::Temperature>(&point);
  EXPECT_DOUBLE_EQ(temperature, temperatureBack());
  mpr::StepLength stepLengthBack;
  stepLengthBack = macroModelState->Get<mpr::StepLength>(&point);
  EXPECT_DOUBLE_EQ(stepLength, stepLengthBack());
  mpr::EffectiveStrainRate effStrRateBack;
  effStrRateBack = macroModelState->Get<mpr::EffectiveStrainRate>(&point);
  EXPECT_DOUBLE_EQ(effStrRate, effStrRateBack());
}

TEST(CaseEsaform2015, IVTest)
{

  // Main 'databus' for the case; necessary for submodel
  DATABUS_INSTANCE(mainDatabus, CaseEsaform2015Databus);


  // StressProxy; gets stress and stress's derivatives to macroscale software (FEM)
  MODEL_INSTANCE_REDESIGN(mst::StressProxy, StressProxyModel, CaseEsaform2015Databus, stressReturn, mainDatabus);

  // IvManager gets new IV values from Databus. 
  MODEL_INSTANCE_REDESIGN(ad::IvManager, IvManager, CaseEsaform2015Databus, ivManager, mainDatabus);
  // ivProvider is a handler of Deform user variables procedure; it is responsible for both way communication
  boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivProvider(new sdm::OneStepOneElementNoElementCheckUserValues(ivManager));
  // This must be used when real Deform is used; here it is only for demonstration purposes 
  sdm::SetDeformHandlerFacadeUpd(ivProvider);

  //******************************************************************************************************************************************************************
  // Testing
  //******************************************************************************************************************************************************************

  const double temperature = 500;
  const double stepLength = 0.001;
  const double effStrRate = 0.1;
  const int elementId = 0;
  const int meshId = 0;
  int userVariablesNumber = 8;
  double userVariablesValues[8] = { 1e7, 1e7, 3, 4, 5, 6, 7, 8 };

  ivProvider->SetElementalVariables(userVariablesNumber, userVariablesValues, meshId, elementId);
  EXPECT_DOUBLE_EQ(userVariablesValues[0], ivProvider->GetElementalVariables(0));
  
  double userVariablesValues2[8] = { 1e7, 1e7, 4, 5, 6, 7, 8, 9 };
  ivProvider->SetElementalVariables(userVariablesNumber, userVariablesValues2, meshId, elementId);
  EXPECT_DOUBLE_EQ(userVariablesValues2[0], ivProvider->GetElementalVariables(0));
}

TEST(CaseEsaform2015, DisclaimerTest)
{
  // testing models parameters
  const double burgers = 0.286e-9;
  const double interObstacle = 2.5e-6;
  const double sizeObstacle = 1.0e-6; //!!!!!!!! Verify!
  const double activationEnergy = 2.4e-19;
  const double freqObstacle = 8e13;
  const double dislDens = 1e11;

  // Main 'databus' for the case; necessary for submodel
  DATABUS_INSTANCE(mainDatabus, CaseEsaform2015Databus);

  //******************************************************************************************************************************************************************
  // In and out connections to Deform (emulated here)
  //******************************************************************************************************************************************************************

  // StressProxy; gets stress and stress's derivatives to macroscale software (FEM)
  MODEL_INSTANCE_REDESIGN(mst::StressProxy, StressProxyModel, CaseEsaform2015Databus, stressReturn, mainDatabus);

  // Deform handler
  boost::shared_ptr<sdm::OneStepOneElementNoElementCheckStresses> stateProvider(new sdm::OneStepOneElementNoElementCheckStresses(stressReturn));
  // This must be used when real Deform is used;, here it is only for demonstration purposes 
  sdm::SetDeformHandlerFacadeMtr(stateProvider);

  // Representation of macroscopic state - interface to Deform
  // Submodel; handler of Deform procedure
  MODEL_INSTANCE_WITH_PARAM(MacroModelState, MacroModelStateModel, CaseEsaform2015Databus, macroModelState, mainDatabus, stateProvider);

  // IvManager gets new IV values from Databus. 
  MODEL_INSTANCE_REDESIGN(ad::IvManager, IvManager, CaseEsaform2015Databus, ivManager, mainDatabus);
  // ivProvider is a handler of Deform user variables procedure; it is responsible for both way communication
  boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivProvider(new sdm::OneStepOneElementNoElementCheckUserValues(ivManager));
  // This must be used when real Deform is used; here it is only for demonstration purposes 
  sdm::SetDeformHandlerFacadeUpd(ivProvider);

  // IV source for Random dislocation model
  boost::shared_ptr<ad::IVStorageAdaptedToDislRandEquat> dislRandEquatStorage(new ad::IVStorageAdaptedToDislRandEquat(ivProvider));

  // IV source for Wall dislocation model
  boost::shared_ptr<ad::IVStorageAdaptedToDislWalEquat> dislWallEquatStorage(new ad::IVStorageAdaptedToDislWalEquat(ivProvider));

  //******************************************************************************************************************************************************************
  // Other submodels
  //******************************************************************************************************************************************************************

  // Since the databus and the submodel are cross-binded (both are inputs for each others), the code below replaces simple objects definitions
  

  // Mock stresses are needed because stress derivatives  are not available in stress model
  MODEL_INSTANCE_REDESIGN(mot::MockStressDerivatives, MockStressesModel, CaseEsaform2015Databus, mockStressDerivatives, mainDatabus);

  //If not-default constructor needed, the macro MODEL_INSTANCE_WITH_PARAMS must be used
  MODEL_INSTANCE_WITH_PARAM(mst::StressThermalAthermalTaylor, StressThermalAthermalTaylorModel, CaseEsaform2015Databus, stressThermalAthermalTaylor, mainDatabus, 3.06);

  mst::StressThermalOrowanReduced_Params prms(burgers * interObstacle * sizeObstacle, activationEnergy, interObstacle, freqObstacle, dislDens, burgers);
  MODEL_INSTANCE_WITH_PARAM(mst::StressThermalOrowanReduced, StressThermalOrowanReducedModel, CaseEsaform2015Databus, stressThermalOrowanReduced, mainDatabus, prms);

  mst::StressAthermalDislocationsEq_Param strAth(0.3, 3.0, burgers);
  MODEL_INSTANCE_WITH_PARAM(mst::StressAthermalDislocationsEq, StressAthermalDislocationsEqModel, CaseEsaform2015Databus, stressAthermalDislocationsEq, mainDatabus, strAth);

  // Constructor parameters
  mdi::DislocationWallEquation_Param<ad::IVStorageAdaptedToDislWalEquat> dwParam(2.4e6, 6e7, 0.3, 7.0, 0.0, 0.0, dislWallEquatStorage);
  MODEL_INSTANCE_WITH_PARAM(DislocationWallEquation, DislocationWallEquationModel, CaseEsaform2015Databus, dislocationWallEquation, mainDatabus, dwParam);

  // Constructor parameters
  mdi::DislocationRandomEquation_Param<ad::IVStorageAdaptedToDislRandEquat> drParam(22e6, 80e7, 2.2, 105.0, 0.0, 0.0, dislRandEquatStorage);
  MODEL_INSTANCE_WITH_PARAM(DislocationRandomEquation, DislocationRandomEquationModel, CaseEsaform2015Databus, dislocationRandomEquation, mainDatabus, drParam);

  mpr::ShearModulus sm; sm(26e9);
  MODEL_INSTANCE_WITH_PARAM(mst::ShearModulusConst, ShearModulusConstModel, CaseEsaform2015Databus, shearModulusConst, mainDatabus, sm);

  MODEL_INSTANCE_REDESIGN(SecondPhasePrecipitationsConst, SecondPhasePrecipitationsConstModel, CaseEsaform2015Databus, secondPhasePrecipitationsConst, mainDatabus);

  //******************************************************************************************************************************************************************
  // Testing
  //******************************************************************************************************************************************************************

  const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
  const mpr::Time t = mpr::Time(0.0);
  pnt::ComputedPoint<mpr::Coordinates2D> point = pnt::ComputedPoint<mpr::Coordinates2D>(coord, t);
  mpr::YieldStress ys;

  // testing values
  const double temperature = 500;
  const double stepLength = 0.001;
  const double effStrRate = 0.1;
  const int elementId = 0;
  const int meshId = 0;
  int userVariablesNumber = 8;
  double userVariablesValues[8] = { 1e7, 1e7, 3, 4, 5, 6, 7, 8 };

  ivProvider->SetElementalVariables(userVariablesNumber, userVariablesValues, meshId, elementId);

  stateProvider->SetTemperature(&temperature, meshId, elementId);
  stateProvider->SetStepLength(&stepLength, meshId, elementId);
  stateProvider->SetEffectiveStrainRate(&effStrRate, meshId, elementId);

  ys = stressReturn->GetState(&point, &ys);

  mpr::StressAthermal stressAT;
  stressAT = stressAthermalDislocationsEq->GetState(&point, &stressAT);

  mpr::StressThermal stressT;
  stressT = stressThermalOrowanReduced->GetState(&point, &stressT);

  pdi::DislocationsDensityRandom dislRandom;
  dislRandom = dislocationRandomEquation->GetState(&point, &dislRandom);

  pdi::DislocationsDensityWall dislWall;
  dislWall = dislocationWallEquation->GetState(&point, &dislWall);

  EXPECT_NEAR(163304116.51816642, dislRandom(), 0.01e8);
  EXPECT_NEAR(12003156.314206921, dislWall(), 0.01e7);
  EXPECT_NEAR(105794.84565491234, stressAT(), 0.01e5);
  EXPECT_NEAR(96.62, stressT(), 0.15);
}

TEST(CaseEsaform2015, SwitcherTest)
{
  // testing models parameters
  const double burgers = 0.286e-9;
  const double interObstacle = 2.5e-6;
  const double sizeObstacle = 1.0e-6; //!!!!!!!! Verify!
  const double activationEnergy = 2.4e-19;
  const double freqObstacle = 8e13;
  const double dislDens = 1e11;

  // Main 'databus' for the case; necessary for submodel
  DATABUS_INSTANCE(mainDatabus, CaseEsaform2015PrecipitationsSwitchDatabus);

  //******************************************************************************************************************************************************************
  // In and out connections to Deform (emulated here)
  //******************************************************************************************************************************************************************

  MODEL_INSTANCE_REDESIGN(mst::StressProxy, StressProxyModel, CaseEsaform2015PrecipitationsSwitchDatabus, stressReturn, mainDatabus);
  boost::shared_ptr<sdm::OneStepOneElementNoElementCheckStresses> stateProvider(new sdm::OneStepOneElementNoElementCheckStresses(stressReturn));
  MODEL_INSTANCE_WITH_PARAM(MacroModelState, MacroModelStateModel, CaseEsaform2015PrecipitationsSwitchDatabus, macroModelState, mainDatabus, stateProvider);
  MODEL_INSTANCE_REDESIGN(ad::IvManager, IvManager, CaseEsaform2015PrecipitationsSwitchDatabus, ivManager, mainDatabus);
  boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivProvider(new sdm::OneStepOneElementNoElementCheckUserValues(ivManager));
  
  boost::shared_ptr<ad::IVStorageAdaptedToDislRandEquat> dislRandEquatStorage(new ad::IVStorageAdaptedToDislRandEquat(ivProvider));
  boost::shared_ptr<ad::IVStorageAdaptedToDislWalEquat> dislWallEquatStorage(new ad::IVStorageAdaptedToDislWalEquat(ivProvider));

  //******************************************************************************************************************************************************************
  // Other submodels
  //******************************************************************************************************************************************************************

  MODEL_INSTANCE_REDESIGN(mot::MockStressDerivatives, MockStressesModel, CaseEsaform2015PrecipitationsSwitchDatabus, mockStressDerivatives, mainDatabus);
  MODEL_INSTANCE_WITH_PARAM(mst::StressThermalAthermalTaylor, StressThermalAthermalTaylorModel, CaseEsaform2015PrecipitationsSwitchDatabus, stressThermalAthermalTaylor, mainDatabus, 3.06);
  mst::StressThermalOrowanReduced_Params prms(burgers * interObstacle * sizeObstacle, activationEnergy, interObstacle, freqObstacle, dislDens, burgers);
  MODEL_INSTANCE_WITH_PARAM(mst::StressThermalOrowanReduced, StressThermalOrowanReducedModel, CaseEsaform2015PrecipitationsSwitchDatabus, stressThermalOrowanReduced, mainDatabus, prms);
  mst::StressAthermalDislocationsEq_Param strAth(0.3, 3.0, burgers);
  MODEL_INSTANCE_WITH_PARAM(mst::StressAthermalDislocationsEq, StressAthermalDislocationsEqModel, CaseEsaform2015PrecipitationsSwitchDatabus, stressAthermalDislocationsEq, mainDatabus, strAth);
  mdi::DislocationWallEquation_Param<ad::IVStorageAdaptedToDislWalEquat> dwParam(2.4e6, 6e7, 0.3, 7.0, 0.0, 0.0, dislWallEquatStorage);
  MODEL_INSTANCE_WITH_PARAM(DislocationWallEquation, DislocationWallEquationModel, CaseEsaform2015PrecipitationsSwitchDatabus, dislocationWallEquation, mainDatabus, dwParam);
  mdi::DislocationRandomEquation_Param<ad::IVStorageAdaptedToDislRandEquat> drParam(22e6, 80e7, 2.2, 105.0, 0.0, 0.0, dislRandEquatStorage);
  MODEL_INSTANCE_WITH_PARAM(DislocationRandomEquation, DislocationRandomEquationModel, CaseEsaform2015PrecipitationsSwitchDatabus, dislocationRandomEquation, mainDatabus, drParam);
  mpr::ShearModulus sm; sm(26e9);
  MODEL_INSTANCE_WITH_PARAM(mst::ShearModulusConst, ShearModulusConstModel, CaseEsaform2015PrecipitationsSwitchDatabus, shearModulusConst, mainDatabus, sm);
  
  
  //******************************************************************************************************************************************************************
  // Submodel for switcher
  //******************************************************************************************************************************************************************

  //Get the list of submodels available in Switcher
  typedef am3::databus::ModelInstanceType<PrecipitationsSwitcher, CaseEsaform2015PrecipitationsSwitchDatabus>::VariableProvidersTypelist PrecipitationSwitcherInputs;

  //Define and declare KBS
  typedef  am3::databus::DatabusTypelistsInternal < Kbs, PrecipitationSwitcherInputs>::Model KbsPrecip;
  boost::shared_ptr<KbsPrecip> kbsPrecip(new KbsPrecip(mainDatabus) );
  

  //Make instance of the switcher
  MODEL_INSTANCE_WITH_PARAM(PrecipitationsSwitcher, SecondPhasePrecipitationsConstSwitcher, CaseEsaform2015PrecipitationsSwitchDatabus, secondPhasePrecipitationsConstSwitcher, mainDatabus, kbsPrecip);

  // submodel
  typedef am3::databus::DatabusTypelistsInternal < SecondPhasePrecipitationsConst, PrecipitationSwitcherInputs>::Model SecondPhasePrecipitationsConstModel;
  boost::shared_ptr<SecondPhasePrecipitationsConstModel> secondPhasePrecipitationsConst(new SecondPhasePrecipitationsConstModel);
  secondPhasePrecipitationsConst->SetModelInput(mainDatabus.get());
  // next submodel ...

  // Adding submodels to Switcher
  secondPhasePrecipitationsConstSwitcher->AddModel(secondPhasePrecipitationsConst);

  //******************************************************************************************************************************************************************
  // Testing
  //******************************************************************************************************************************************************************

  const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
  const mpr::Time t = mpr::Time(0.0);
  pnt::ComputedPoint<mpr::Coordinates2D> point = pnt::ComputedPoint<mpr::Coordinates2D>(coord, t);
  
  // testing values
  const double temperature = 500;
  const double stepLength = 0.001;
  const double effStrRate = 0.1;
  const int elementId = 0;
  const int meshId = 0;
  int userVariablesNumber = 8;
  double userVariablesValues[8] = { 1e7, 1e7, 3, 4, 5, 6, 7, 8 };

  ivProvider->SetElementalVariables(userVariablesNumber, userVariablesValues, meshId, elementId);

  stateProvider->SetTemperature(&temperature, meshId, elementId);
  stateProvider->SetStepLength(&stepLength, meshId, elementId);
  stateProvider->SetEffectiveStrainRate(&effStrRate, meshId, elementId);

  Alcrfemnsi_a_p0_Radius rad;
  rad = secondPhasePrecipitationsConstSwitcher->GetState(&point, &rad);
  EXPECT_NEAR(rad(), 0.2, 1e-5);
}

TEST(CaseEsaform2015, SingleMatCalcProcess_Test)
{
  DATABUS_INSTANCE(mainDatabus, CaseEsaform2015PrecipitationsSwitchDatabus);

  MODEL_INSTANCE_REDESIGN(mst::StressProxy, StressProxyModel, CaseEsaform2015PrecipitationsSwitchDatabus, stressReturn, mainDatabus);
  boost::shared_ptr<sdm::OneStepOneElementNoElementCheckStresses> stateProvider(new sdm::OneStepOneElementNoElementCheckStresses(stressReturn));
  MODEL_INSTANCE_WITH_PARAM(MacroModelState, MacroModelStateModel, CaseEsaform2015PrecipitationsSwitchDatabus, macroModelState, mainDatabus, stateProvider);

  //Get the list of submodels available in Switcher
  typedef am3::databus::ModelInstanceType<PrecipitationsSwitcher, CaseEsaform2015PrecipitationsSwitchDatabus>::VariableProvidersTypelist PrecipitationSwitcherInputs;

  //Define and declare KBS
  typedef  am3::databus::DatabusTypelistsInternal < Kbs, PrecipitationSwitcherInputs>::Model KbsPrecip;
  boost::shared_ptr<KbsPrecip> kbsPrecip(new KbsPrecip(mainDatabus));


  //Make instance of the switcher
  MODEL_INSTANCE_WITH_PARAM(PrecipitationsSwitcher, SecondPhasePrecipitationsConstSwitcher, CaseEsaform2015PrecipitationsSwitchDatabus, secondPhasePrecipitationsConstSwitcher, mainDatabus, kbsPrecip);

  // submodel
  typedef am3::databus::DatabusTypelistsInternal < SecondPhasePrecipitationsMatCalc, PrecipitationSwitcherInputs>::Model SecondPhasePrecipitationsMatCalcModel;
  MatCalcScriptProvider scripts;
  boost::shared_ptr<SecondPhasePrecipitationsMatCalcModel> secondPhasePrecipitationsMatCalc(new SecondPhasePrecipitationsMatCalcModel(scripts));

  secondPhasePrecipitationsMatCalc->SetModelInput(mainDatabus.get());
  

  // Adding submodels to Switcher
  secondPhasePrecipitationsConstSwitcher->AddModel(secondPhasePrecipitationsMatCalc);


  
  //boost::shared_ptr<SecondPhasePrecipitationsMatCalc> gmm(new SecondPhasePrecipitationsMatCalc(scripts));

  const double temperature = 500;
  const double stepLength = 0.001;
  const double effStrRate = 0.1;
  const int elementId = 0;
  const int meshId = 0;

  const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
  const mpr::Time t = mpr::Time(0.0);
  pnt::ComputedPoint<mpr::Coordinates2D> point = pnt::ComputedPoint<mpr::Coordinates2D>(coord, t);

  stateProvider->SetTemperature(&temperature, meshId, elementId);
  stateProvider->SetStepLength(&stepLength, meshId, elementId);
  stateProvider->SetEffectiveStrainRate(&effStrRate, meshId, elementId);

  Alcrfemnsi_a_p0_Radius rad;
  rad = secondPhasePrecipitationsConstSwitcher->GetState(&point, &rad);
    FAIL();
}
// 
// TEST(CaseEsaform2015, MpiMatCalcProcess_Test)
// {
// 
//  
// 
//   DATABUS_INSTANCE(mainDatabus, CaseEsaform2015PrecipitationsSwitchDatabus);
// 
//   MODEL_INSTANCE_REDESIGN(mst::StressProxy, StressProxyModel, CaseEsaform2015PrecipitationsSwitchDatabus, stressReturn, mainDatabus);
//   boost::shared_ptr<sdm::OneStepOneElementNoElementCheckStresses> stateProvider(new sdm::OneStepOneElementNoElementCheckStresses(stressReturn));
//   MODEL_INSTANCE_WITH_PARAM(MacroModelState, MacroModelStateModel, CaseEsaform2015PrecipitationsSwitchDatabus, macroModelState, mainDatabus, stateProvider);
// 
//   //Get the list of submodels available in Switcher
//   typedef am3::databus::ModelInstanceType<PrecipitationsSwitcher, CaseEsaform2015PrecipitationsSwitchDatabus>::VariableProvidersTypelist PrecipitationSwitcherInputs;
// 
//   //Define and declare KBS
//   typedef  am3::databus::DatabusTypelistsInternal < Kbs, PrecipitationSwitcherInputs>::Model KbsPrecip;
//   boost::shared_ptr<KbsPrecip> kbsPrecip(new KbsPrecip(mainDatabus));
// 
// 
//   //Make instance of the switcher
//   MODEL_INSTANCE_WITH_PARAM(PrecipitationsSwitcher, SecondPhasePrecipitationsConstSwitcher, CaseEsaform2015PrecipitationsSwitchDatabus, secondPhasePrecipitationsConstSwitcher, mainDatabus, kbsPrecip);
// 
//   // submodel
//   typedef am3::databus::DatabusTypelistsInternal < SecondPhasePrecipitationsMatCalcMpiManager, PrecipitationSwitcherInputs>::Model SecondPhasePrecipitationsMatCalcModel;
//   boost::shared_ptr<SecondPhasePrecipitationsMatCalcModel> secondPhasePrecipitationsMatCalc(new SecondPhasePrecipitationsMatCalcModel);
// 
//   secondPhasePrecipitationsMatCalc->SetModelInput(mainDatabus.get());
// 
// 
//   // Adding submodels to Switcher
//   secondPhasePrecipitationsConstSwitcher->AddModel(secondPhasePrecipitationsMatCalc);
// 
// 
// 
//   //boost::shared_ptr<SecondPhasePrecipitationsMatCalc> gmm(new SecondPhasePrecipitationsMatCalc(scripts));
// 
//   const double temperature = 500;
//   const double stepLength = 0.001;
//   const double effStrRate = 0.1;
//   const int elementId = 0;
//   const int meshId = 0;
// 
//   const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
//   const mpr::Time t = mpr::Time(0.0);
//   pnt::ComputedPoint<mpr::Coordinates2D> point = pnt::ComputedPoint<mpr::Coordinates2D>(coord, t);
// 
//   stateProvider->SetTemperature(&temperature, meshId, elementId);
//   stateProvider->SetStepLength(&stepLength, meshId, elementId);
//   stateProvider->SetEffectiveStrainRate(&effStrRate, meshId, elementId);
// 
//   Alcrfemnsi_a_p0_Radius rad;
//   rad = secondPhasePrecipitationsConstSwitcher->GetState(&point, &rad);
//   FAIL();
// }