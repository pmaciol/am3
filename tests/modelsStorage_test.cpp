

namespace am3 {namespace tests {void RegisterModelsStorage(){};}}


#include "gtest/gtest.h"
#include "../models/simpleStorageModel.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../modelPolycrystalMicrostructure/gottstein.h"

namespace mo=am3::model;
namespace mpt=am3::model::point;
namespace mo=am3::model;
namespace ppm=am3::problem::polycrystal;
namespace mpm=am3::model::polycrystal;

// struct TestedModel
// 	: public mo::LastValueStorageModel<ppm::contract::GrainsContract2D::Tuple, ppm::contract::GrainsContract2D::CoordinatesType>
// 	, public mpm::GrainsBySherstnev 
// {
// 	typedef mo::LastValueStorageModel<ppm::contract::GrainsContract2D::Tuple, ppm::contract::GrainsContract2D::CoordinatesType> Model;
// 	void AddPoint(const mpr::Coordinates2D & coord, unsigned long pointID)
// 	{
// 		mo::LastValueStorageModel<ppm::contract::GrainsContract2D::Tuple, ppm::contract::GrainsContract2D::CoordinatesType>::AddPoint(coord, pointID);
// 	}
// 	void StoreData(unsigned long pointID, ppm::contract::GrainsContract2D::Tuple tuple)
// 	{
// 		SetValue(pointID,tuple);
// 	}
// 	bool NewStep();
// 	ppm::contract::GrainsContract2D::Tuple  GetPreviousStepData(const mpr::Coordinates2D & coord) const;
// 	ppm::contract::GrainsContract2D::Tuple  GetThisStepData(const unsigned long pointID) const 
// 	{
// 		return GetValues( pointID);
// 	};
// };
// 
// 
// 
// TEST(SimpleStorage,AddingPoint)
// {
// 	TestedModel model;
// 	mpr::Coordinates2D coord(1.0,2.0);
// 	
// 	model.AddPoint(coord,0);
// 	ppm::contract::GrainsContract2D::Tuple tuple(0.5,0.5,0.5,0.5,0.5);
// 	model.StoreData(0,tuple);
// 	ppm::contract::GrainsContract2D::Tuple tuple2=model.GetThisStepData(0);
// 
// 	double tuple2value=std::get<2>(tuple2).Value();
// 
// 	EXPECT_NEAR(0.5,tuple2value,1e-3);
// }