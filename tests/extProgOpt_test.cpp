/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/

/*	include *********************************************************/

#include <iostream>
#include <vector>
#include <string>
#include "gtest/gtest.h"
#include "../common/externalProgramOptions.h"
#include "../logger/logger.h"
//#include "globalDefs.h"

/*	using ***********************************************************/



/*	extern **********************************************************/

namespace am3
{
//   extern std::string kAdinaWorkingDirectory;
//   extern std::string kAdinaTestName;
//   extern std::string kAdinaBinPath;
}

/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterExtProgOpt(){};}}

namespace am3
{
  namespace common
  {
    TEST(OptionsContainerTest,BasicOptionTest)
    {
      Option opt("-m","8000");
      EXPECT_STREQ("-m 8000 ", opt.toString().c_str());
    }
    TEST(OptionsContainerTest,BasicOptionsContainerTest)
    {
      Option opt("-m","8000");
      OptionsContainer optCont;
      optCont.AddOption("mem",opt);
      optCont.AddOption("cpus",Option("-t","3"));
      std::vector<std::string> actOpt;
      actOpt.push_back("mem");
      actOpt.push_back("cpus");
      EXPECT_STREQ("-m 8000 -t 3 ",optCont.OptionsToString(actOpt).c_str());
    }
    TEST(OptionsContainerTest,AllOptions)
    {
      Option opt("-m","8000");
      Option opt2("-t","3");
      OptionsContainer optCont;
      optCont.AddOption("mem",opt);
      optCont.AddOption("cpus",opt2);
      optCont.AddOption("batch",Option("-s",""));
      EXPECT_STREQ("-s  -t 3 -m 8000 ",optCont.OptionsToString().c_str());
    }

    TEST(OptionsContainerTest,NoAskedOption)
    {
      Option opt("-m","8000");
      OptionsContainer optCont;
      optCont.AddOption("mem",opt);
      optCont.AddOption("cpus",Option("-t","3"));
      std::vector<std::string> actOpt;
      actOpt.push_back("mem");
      actOpt.push_back("no_such_option");
      EXPECT_STREQ("-m 8000 ",optCont.OptionsToString(actOpt).c_str());
    }

    TEST(ExtProgramOptTest,WithOptions)
    {
      ExtProgramOpt progWithOptions("adinaf.exe","path","workDir");
      progWithOptions.AddOption("cpus",Option("-t","3"));
      EXPECT_STREQ("-t 3 ",progWithOptions.OptionsToString().c_str());
      EXPECT_STREQ("workDir",progWithOptions.Working_directory().c_str());
    }
  }
}