
/*! \file softwareMatCalcMain_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"

#ifdef MATCALC

#include <boost/shared_ptr.hpp>
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "../softwareMatCalcGeneric/matCalcScriptsBased.h"
#include "../softwareMatCalcGeneric/matCalcResults.h"
#include "../modelTemplates/variableSource.h"

#endif


/*	using ***********************************************************/

namespace am3 { namespace tests { void RegisterSoftwareMatCalcMain(){}; } }


#ifdef MATCALC
namespace smc = am3::software::matcalc;
namespace mvs = am3::model::varSource;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;


/*	extern **********************************************************/

/*	classes *********************************************************/

#ifdef MATCALC_TESTS
TEST(MatCalcScripts, BasicMatCalcTest)
{
  boost::shared_ptr<mvs::PushableSingleValueSource<mpr::Temperature> > temperatureSrc(new mvs::PushableSingleValueSource<mpr::Temperature>);
  mpr::Temperature temperature;
  temperature(600);
  const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
  mpr::Time t = mpr::Time(0.0);
  mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);

  temperatureSrc->SetVariable(point, temperature);

  smc::MatCalcScriptProvider scripts;

  smc::MatCalcScriptsBased<mvs::PushableSingleValueSource<mpr::Temperature>> matcalc(scripts, temperatureSrc);

    
  EXPECT_FALSE(matcalc.IsInitialized());
  matcalc.InitIfNeeded(&point);
  EXPECT_TRUE(matcalc.IsInitialized());
  EXPECT_FALSE(matcalc.IsComputedForPoint(&point));
  EXPECT_FALSE(matcalc.WasLastCalculationFired());

  mpr::Time t2 = mpr::Time(0.0001);
  point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord, t2));
  temperature(600);
  temperatureSrc->SetVariable(point, temperature);

  EXPECT_FALSE(matcalc.IsComputedForPoint(&point));
  matcalc.CalculateStepIfNeeded(&point);
  EXPECT_TRUE(matcalc.IsComputedForPoint(&point));
  EXPECT_TRUE(matcalc.WasLastCalculationFired());
  matcalc.CalculateStepIfNeeded(&point); //shouldn't execute computations
  EXPECT_FALSE(matcalc.WasLastCalculationFired());

  temperature(600);
  temperatureSrc->SetVariable(point, temperature);

  mpr::Time t3 = mpr::Time(0.01);
  point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord, t3));
  temperatureSrc->SetVariable(point, temperature);
  EXPECT_FALSE(matcalc.IsComputedForPoint(&point));
  matcalc.CalculateStepIfNeeded(&point);
  EXPECT_TRUE(matcalc.IsComputedForPoint(&point));
  EXPECT_TRUE(matcalc.WasLastCalculationFired());

  temperature(700);
  temperatureSrc->SetVariable(point, temperature);

  mpr::Time t4 = mpr::Time(1);
  point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord, t4));
  temperatureSrc->SetVariable(point, temperature);
  EXPECT_FALSE(matcalc.IsComputedForPoint(&point));
  matcalc.CalculateStepIfNeeded(&point);
  EXPECT_TRUE(matcalc.IsComputedForPoint(&point));
  EXPECT_TRUE(matcalc.WasLastCalculationFired());

  temperature(600);
  temperatureSrc->SetVariable(point, temperature);

  mpr::Time t5 = mpr::Time(2);
  point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord, t5));
  temperatureSrc->SetVariable(point, temperature);
  EXPECT_FALSE(matcalc.IsComputedForPoint(&point));
  matcalc.CalculateStepIfNeeded(&point);
  EXPECT_TRUE(matcalc.IsComputedForPoint(&point));
  EXPECT_TRUE(matcalc.WasLastCalculationFired());

}

TEST(MatCalc, StringFromTypeTest)
{
  std::string str;
  str = smc::GetName < smc::Alcrfemnsi_a_p0_Radius >() ;
  EXPECT_STREQ("R_MEAN$ALCRFEMNSI_A_P0", str.c_str());
}

// TEST(MatCalcScripts, ReturnVariable)
// {
//   smc::TemperatureSource temperatureSrc;
//   mpr::Temperature temperature;
//   temperature(700);
//   temperatureSrc.SetTemperature(temperature);
// 
//   smc::MatCalcScriptProvider scripts;
//   smc::MatCalcScriptsBased<smc::TemperatureSource> matcalc(scripts);
// 
//   const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
//   mpr::Time t = mpr::Time(0.0);
//   mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);
//   matcalc.InitIfNeeded(&point, &temperatureSrc);
//   mpr::Time t2 = mpr::Time(1.0);
//   point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord, t2));
//   matcalc.CalculateStepIfNeeded(&point, &temperatureSrc);
// 
//   smc::TestValue valToTest;
//   valToTest = smc::GetVariable<smc::TestValue>();
//   EXPECT_DOUBLE_EQ(1.0,valToTest.Value());
// }




// 
// TEST(MatCalc, DatabusWithMatCalc)
// {
//   DATABUS_INSTANCE(testDatabus,tests::matcalc::MatCalcGenericDatabus);
//     typedef smc::InputsProvider<tests::MacroWithTimeContract> InputsProvider;
//   typedef smc::MatcalcCoreContract<smc::MatCalcSetupScripts, mpc::TestPrecomputations, mpc::TestStep, mpc::TestResultsTransformer, mpc::contract::PrecipitationsMeanRadiusContract, InputsProvider> MatCalcTest;
//   MODEL_INSTANCE(inputsProvider,InputsProvider);
//   boost::shared_ptr<smc::MatCalcSetupScripts> matCalcSetupScripts(new smc::MatCalcSetupScripts);
//   boost::shared_ptr<mpc::TestPrecomputations> testPrecomputations(new mpc::TestPrecomputations);
//   boost::shared_ptr<mpc::TestStep> testStep(new mpc::TestStep);
//   boost::shared_ptr<mpc::TestResultsTransformer> testResultsTransformer(new mpc::TestResultsTransformer);
//   boost::shared_ptr<MatCalcTest> gmm(new MatCalcTest);
//   gmm->Set(matCalcSetupScripts,testPrecomputations, testStep, testResultsTransformer,inputsProvider);
//   gmm->Setup("d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\", "d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\", "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\test_on_heat_treatment_9Cr_steel_batch.mcs");
// 
// //   MODEL_INSTANCE(temperatureProvider,tests::TemperatureLinear);
// //   temperatureProvider->Set(1400,-10);
//   MODEL_INSTANCE(macroModel,tests::MacroWithTimeInputsTest);
// 
//   DATABUS_ADD_MODELS(testDatabus,macroModel,gmm/*,temperatureProvider*/);
//   MODEL_INPUT_FROM_DATABUS(inputsProvider,testDatabus);
//   mpr::PrecipitationsMeanRadius pmr;
// 
//   const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5,0.5);
//   mpr::Time t = mpr::Time(0.0);
//   mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord,t);
// 
//   boost::shared_ptr<moc::Provider<mpr::PrecipitationsMeanRadius,mpr::Coordinates2D> > provider = testDatabus;
// 
//   std::vector<mpr::PrecipitationsMeanRadius> results;
//   macroModel->temperature_(1500);
//   macroModel->time_(0.0);
//   results.push_back(provider->GetState(&point,&pmr));
//   t(1000.0);
//   point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord,t));
//   macroModel->temperature_(1400);
//   macroModel->time_(1000.0);
//   results.push_back(provider->GetState(&point,&pmr));
//   macroModel->temperature_(1300);
//   macroModel->time_(2000.0);
//   t(2000.0);
//   point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord,t));
//   results.push_back(provider->GetState(&point,&pmr));
//   macroModel->temperature_(1200);
//   macroModel->time_(3000.0);
//   t(3000.0);
//   point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord,t));
//   results.push_back(provider->GetState(&point,&pmr));
//   macroModel->temperature_(1100);
//   macroModel->time_(4000.0);
//   t(4000.0);
//   point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord,t));
//   results.push_back(provider->GetState(&point,&pmr));
//   macroModel->temperature_(1000);
//   macroModel->time_(5000.0);
//   t(5000.0);
//   point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord,t));
//   results.push_back(provider->GetState(&point,&pmr));
//   macroModel->temperature_(900);
//   macroModel->time_(6000.0);
//   t(6000.0);
//   point.Move(mpt::ComputedPoint<mpr::Coordinates2D>(coord,t));
//   results.push_back(provider->GetState(&point,&pmr));
//   EXPECT_NEAR(1.6e-8,results[6].Value(),1e-9);
// }

#endif //MATCALC_TESTS

#endif