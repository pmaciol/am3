namespace am3 {namespace tests {void RegisterNamedPoint (){};}}

#include <boost/shared_ptr.hpp>
#include "gtest/gtest.h"
#include "../models/namedPoint.h"
#include "../models/dataStructures.h"


namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

namespace tests
{
	typedef mpt::NamedPointsContainerLastTwoSteps2D TestContainer;
	typedef mpt::NamedPointsStorageLastTwo<mpr::Coordinates2D> TestContainerStorage;
}

TEST(NamedPointStorage,IsCoordEqualToLastStored)
{
	const mpr::Coordinates2D coord(0.1,0.2);
	const unsigned long int pointId = 123;
	const mpr::Time time(0.1);
	tests::TestContainerStorage::CoordinatesSinceTime point(coord,time);
	mpt::NamedPointsStorageLastTwoSteps2D pointsContainerStorage(point);
	bool isPosEqualTo = pointsContainerStorage.IsCoordEqualToLastStored(coord);
	EXPECT_TRUE(isPosEqualTo);
	const mpr::Coordinates2D coord2(0.2,0.2);
	const mpr::Time time2(0.2);
	isPosEqualTo = pointsContainerStorage.IsCoordEqualToLastStored(coord2);
	EXPECT_FALSE(isPosEqualTo);
}

TEST(NamedPointStorage,MovePoint)
{
	const mpr::Coordinates2D coord(0.1,0.2);
	const unsigned long int pointId = 123;
	const mpr::Time time(0.1);
	tests::TestContainerStorage::CoordinatesSinceTime point(coord,time);
	mpt::NamedPointsStorageLastTwoSteps2D pointsContainerStorage(point);
	bool isPosEqualTo = pointsContainerStorage.IsCoordEqualToLastStored(coord);
	EXPECT_TRUE (isPosEqualTo);
	const mpr::Coordinates2D coord2(0.2,0.2);
	const mpr::Time time2(0.2);
	tests::TestContainerStorage::CoordinatesSinceTime point2(coord2,time2);
	pointsContainerStorage.MovePoint(point2);
	isPosEqualTo = pointsContainerStorage.IsCoordEqualToLastStored(coord2);
	EXPECT_TRUE (isPosEqualTo);
}

TEST(NamedPointStorage,MovePointWithWrongTime)
{
	const mpr::Coordinates2D coord(0.1,0.2);
	const unsigned long int pointId = 123;
	const mpr::Time time(0.1);
	tests::TestContainerStorage::CoordinatesSinceTime point(coord,time);
	mpt::NamedPointsStorageLastTwoSteps2D pointsContainerStorage(point);
	bool isPosEqualTo = pointsContainerStorage.IsCoordEqualToLastStored(coord);
	EXPECT_TRUE (isPosEqualTo);
	const mpr::Coordinates2D coord2(0.2,0.2);
	tests::TestContainerStorage::CoordinatesSinceTime point2(coord2,time);
	pointsContainerStorage.MovePoint(point2);
	isPosEqualTo = pointsContainerStorage.IsCoordEqualToLastStored(coord);
	EXPECT_TRUE (isPosEqualTo);
}

TEST(NamedPointStorage,getLastTime)
{
	const mpr::Coordinates2D coord(0.1,0.2);
	const unsigned long int pointId = 123;
	const mpr::Time time(0.1);
	tests::TestContainerStorage::CoordinatesSinceTime point(coord,time);
	mpt::NamedPointsStorageLastTwoSteps2D pointsContainerStorage(point);
	mpr::Time timeBack=pointsContainerStorage.GetLastTime();
	EXPECT_TRUE(timeBack==time);
	const mpr::Coordinates2D coord2(0.2,0.2);
	const mpr::Time time2(0.2);
	tests::TestContainerStorage::CoordinatesSinceTime point2(coord2,time2);
	pointsContainerStorage.MovePoint(point2);
	timeBack=pointsContainerStorage.GetLastTime();
	EXPECT_FALSE(timeBack==time);
}

TEST(NamedPointStorage,GetCoordinates)
{
	const mpr::Coordinates2D coord(0.1,0.2);
	const unsigned long int pointId = 123;
	const mpr::Time time(0.1);
	tests::TestContainerStorage::CoordinatesSinceTime point(coord,time);
	mpt::NamedPointsStorageLastTwoSteps2D pointsContainerStorage(point);
	const mpr::Coordinates2D coord2(0.2,0.2);
	const mpr::Time time2(0.2);
	tests::TestContainerStorage::CoordinatesSinceTime point2(coord2,time2);
	pointsContainerStorage.MovePoint(point2);
	mpr::Coordinates2D firstCoord = pointsContainerStorage.GetCoordinates(time);
	EXPECT_TRUE(coord==firstCoord);
	mpr::Coordinates2D secondCoord = pointsContainerStorage.GetCoordinates(time2);
	EXPECT_TRUE(coord2==secondCoord);
}

//////////////////////////////////////////////////////////////////////////

TEST(NamedPointContainer,IsPointAvailable)
{
	mpt::NamedPointsContainerLastTwoSteps2D pointsContainer;
	const mpr::Coordinates2D coord(0.1,0.2);
	const unsigned long int pointId = 123;
	bool isIdAvailable = pointsContainer.IsPointAvailable(pointId);
	EXPECT_FALSE(isIdAvailable);
	pointsContainer.AddNewPointOrMoveOld(pointId , coord, mpr::Time(0.1));
	isIdAvailable = pointsContainer.IsPointAvailable(pointId);
	EXPECT_TRUE(isIdAvailable);
	isIdAvailable = pointsContainer.IsPointAvailable(pointId+1);
	EXPECT_FALSE(isIdAvailable);
}

TEST(NamedPointContainer,GetLastTime)
{
	mpt::NamedPointsContainerLastTwoSteps2D pointsContainer;
	const mpr::Coordinates2D coord(0.1,0.2);
	mpr::Time time(0.1);
	const unsigned long int pointId = 123;
	mpr::Time timeToCheck = pointsContainer.GetLastTime(pointId+1);
	EXPECT_FALSE(time==timeToCheck);	
	pointsContainer.AddNewPointOrMoveOld(pointId , coord, time);
	timeToCheck = pointsContainer.GetLastTime(pointId);
	EXPECT_TRUE(time==timeToCheck);	
	timeToCheck = pointsContainer.GetLastTime(pointId+1);
	EXPECT_FALSE(time==timeToCheck);	
}

TEST(NamedPointContainer,GetLastCoord)
{
	mpt::NamedPointsContainerLastTwoSteps2D pointsContainer;
	const mpr::Coordinates2D coord(0.1,0.2);
	const unsigned long int pointId = 123;
	mpr::Coordinates2D coordToCheck = pointsContainer.GetLastCoordinates(pointId+1);
	EXPECT_FALSE(coord==coordToCheck);	
	pointsContainer.AddNewPointOrMoveOld(pointId , coord, mpr::Time(0.1));
	coordToCheck = pointsContainer.GetLastCoordinates(pointId);
	EXPECT_TRUE(coord==coordToCheck);	
	coordToCheck = pointsContainer.GetLastCoordinates(pointId+1);
	EXPECT_FALSE(coord==coordToCheck);	
}

TEST(NamedPointContainer,AddNewPoint)
{
	mpt::NamedPointsContainerLastTwoSteps2D pointsContainer;
	const mpr::Coordinates2D coord(0.1,0.2);
	const unsigned long int pointId = 123;
	pointsContainer.AddNewPointOrMoveOld(pointId , coord, mpr::Time(0.1));
	bool isIdAvailable = pointsContainer.IsPointAvailable(pointId);
	EXPECT_TRUE(isIdAvailable);
	isIdAvailable = pointsContainer.IsPointAvailable(pointId+1);
	EXPECT_FALSE(isIdAvailable);
}

TEST(NamedPointContainer,MovePoint)
{
	mpt::NamedPointsContainerLastTwoSteps2D pointsContainer;
	const mpr::Coordinates2D coord(0.1,0.2);
	const unsigned long int pointId = 123;
	pointsContainer.AddNewPointOrMoveOld(pointId , coord, mpr::Time(0.1));
	mpr::Coordinates2D coordToCheck = pointsContainer.GetLastCoordinates(pointId);
	EXPECT_TRUE(coord==coordToCheck);
	const mpr::Coordinates2D newCoord(0.2,0.2);
	pointsContainer.AddNewPointOrMoveOld(pointId , newCoord, mpr::Time(0.2));
	coordToCheck = pointsContainer.GetLastCoordinates(pointId);
	EXPECT_TRUE(newCoord==coordToCheck);
}

TEST(NamedPointContainer,MorePoints)
{
	mpt::NamedPointsContainerLastTwoSteps2D pointsContainer;
	mpr::Time time(0.1);
	mpr::Time time2(0.2);
	const mpr::Coordinates2D coord(0.1,0.2);
	const mpr::Coordinates2D coord2(0.2,0.2);
	const unsigned long int pointId = 123;
	pointsContainer.AddNewPointOrMoveOld(pointId , coord, time);
	pointsContainer.AddNewPointOrMoveOld(pointId+1 , coord2, time);
	pointsContainer.AddNewPointOrMoveOld(pointId-1 , coord2, time2);
	mpr::Coordinates2D coordToCheck = pointsContainer.GetLastCoordinates(pointId);
	EXPECT_TRUE(coord==coordToCheck);	
	coordToCheck = pointsContainer.GetLastCoordinates(pointId+1);
	EXPECT_TRUE(coord2==coordToCheck);	
	coordToCheck = pointsContainer.GetLastCoordinates(pointId-1);
	EXPECT_TRUE(coord2==coordToCheck);	
	mpr::Time timeToCheck = pointsContainer.GetLastTime(pointId-1);
	EXPECT_TRUE(time2==timeToCheck);	
}

//////////////////////////////////////////////////////////////////////////

TEST(NamedPoint,AreEqual)
{
	const mpr::Coordinates2D coord(0.1,0.2);
	mpr::Time time(0.1);
	const unsigned long int pointId = 123;
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point(pointId,coord,time);
	const mpr::Coordinates2D coord2(0.2,0.2);
	mpr::Time time2(0.2);
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point2(pointId,coord2,time2);
	EXPECT_TRUE(point==point2);
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point3(pointId+1,coord,time);
	EXPECT_FALSE(point==point3);
	unsigned long int pointIdToCheck = point.GetId();
	EXPECT_EQ(pointId,pointIdToCheck );
}

TEST(NamedPoint,GetTime)
{
	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
	const mpr::Coordinates2D coord(0.1,0.2);
	mpr::Time time(0.1);
	const unsigned long int pointId = 123;
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point(pointId,coord,time);
	double t=point.GetTime();
	EXPECT_DOUBLE_EQ(time(),t);
	const mpr::Coordinates2D coord2(0.2,0.2);
	mpr::Time time2(0.2);
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point2(pointId,coord2,time2);
	double t2=point2.GetTime();
	EXPECT_DOUBLE_EQ(time2(),t2);
}

TEST(NamedPoint,Copy)
{
	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
	const mpr::Coordinates2D coord(0.1,0.2);
	mpr::Time time(0.1);
	const unsigned long int pointId = 123;
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D>* point = new mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D>(pointId,coord,time);

	boost::shared_ptr<mpt::Point<mpr::Coordinates2D> > pointBoost = point->Copy();
	delete point;

	double t=pointBoost->GetTime();
	EXPECT_DOUBLE_EQ(time(),t);
}

TEST(NamedPoint,Move)
{
  mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
  const mpr::Coordinates2D coord(0.1,0.2);
  const mpr::Coordinates2D coord2(0.2,0.2);
  mpr::Time time(0.1);
  mpr::Time time2(0.2);
  const unsigned long int pointId = 123;
  mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D>* point = new mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D>(pointId,coord,time);
  mpt::ComputedPoint<mpr::Coordinates2D> point2(coord2,time2);

  point->Move(point2);
  EXPECT_DOUBLE_EQ(0.2,point->Coordinates().x());
  delete point;

}