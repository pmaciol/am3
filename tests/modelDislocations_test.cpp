
/*! \file modelDislocations_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../modelDislocations/contracts.h"
#include "../modelDislocations/dislocationWallEquation.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "../adapter/ivstorage.h"

/*	using ***********************************************************/

namespace mdi = am3::model::dislocations;
namespace sdm = am3::software::deform;
namespace ad = am3::adapter;
namespace pdi = am3::problem::dislocations;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterModelDislocations (){};}}


namespace am3
{
  namespace adapter
  {
    template <>
    struct VariableID<pdi::DislocationsDensityWall>
    {
      static const unsigned int id = 0;
    };
  }
}
TEST(AAA, BBB)
{
//   typedef ad::IVStorage<sdm::DeformHandler,pdi::contract::DislocationsStorageContract> Storage;
//   mdi::DislocationWallEquation<Storage> test(0.1,0.1,0.1);
}