
/*! \file modelCreep_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include <math.h>
#include <map>
#include "../modelCreep/modelLogisticCreepStrain.h"
#include "../modelCreep/contracts.h"
#include "../models/dataStructures.h"
#include "modelCreep_test.h"

/*	using ***********************************************************/

namespace mcr = am3::model::creep;
namespace mpr = am3::model::properties;
namespace ppc = am3::problem::creep;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterModelCreep (){};}}

TEST(CreepModelP91, timeToRupture)
{
  // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
  // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
  // Values should agree to fig. 19 with data from table 7, row 1.

  mcr::TimeToRuptureMansonBrown4P91EN ttrModel;
  double ttrValue = ttrModel.TimeToRupture(575,100e6);
  EXPECT_NEAR(log10(ttrValue),5.76,0.01);
  ttrValue = ttrModel.TimeToRupture(575,120e6);
  EXPECT_NEAR(log10(ttrValue),5.19,0.01);
  ttrValue = ttrModel.TimeToRupture(575,140e6);
  EXPECT_NEAR(log10(ttrValue),4.59,0.01);
  ttrValue = ttrModel.TimeToRupture(575,160e6);
  EXPECT_NEAR(log10(ttrValue),3.95,0.01);
  ttrValue = ttrModel.TimeToRupture(575,180e6);
  EXPECT_NEAR(log10(ttrValue),3.26,0.01);
  ttrValue = ttrModel.TimeToRupture(575,200e6);
  EXPECT_NEAR(log10(ttrValue),2.50,0.01);
}

TEST(CreepModelP91, x0Computation)
{
  // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
  // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
  // Values should agree to table 10 with data from table 19. 

  boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> ttrModel = boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> (new mcr::TimeToRuptureMansonBrown4P91EN);
  tests::creep::PublicModelLogisticCreepStrain lcsModel(ttrModel);
  double x0 = lcsModel.X0(560,100e6);
  EXPECT_NEAR(x0,-3.306,0.001);
}

TEST(CreepModelP91, pComputation)
{
  // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
  // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
  // Values should agree to table 10 with data from table 19. 


  boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> ttrModel = boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> (new mcr::TimeToRuptureMansonBrown4P91EN);
  tests::creep::PublicModelLogisticCreepStrain lcsModel(ttrModel);
  double p = lcsModel.P(560,100e6);
  EXPECT_NEAR(p,6.717,0.001);
}



// TEST(CreepModelP91, timeToStrain)
// {
//   // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
//   // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
//   // Values should agree to fig. 3 with data from table 19. 
// 
//   tests::creep::PublicModelLogisticCreepStrain lcsModel;
//   double tts;
//   std::map<double,double> timesToStrain;
// 
//   for (double strain = 1e-3; strain <= 2e-1; strain += 0.05e-1)
//   {
//     tts = lcsModel.TimeToStrain(600,100e6,strain);
//     std::pair<double,double> toTts(strain,tts);
//     timesToStrain.emplace(toTts);
//   }
//   EXPECT_NEAR(timesToStrain.at(0.0060000000000000001),2.50,0.01);
//   EXPECT_NEAR(timesToStrain.at(0.0110000000000000001),2.50,0.01);
//   EXPECT_NEAR(timesToStrain.at(0.0160000000000000001),2.50,0.01);
// }

//////////////////////////////////////////////////////////////////////////
// !!!
// This method does not work. strainRateIncremented method (below) works fine
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

TEST(CreepModelP91, strainRate)
{
  // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
  // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
  // Values should agree to fig. 4 with data from table 19. 
  // Not exactly agreeable, but quite similar

  boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> ttrModel = boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> (new mcr::TimeToRuptureMansonBrown4P91EN);
  tests::creep::PublicModelLogisticCreepStrain lcsModel(ttrModel);
  
  double creepRate;
  double tts;
  std::map<double,double> strainRates;

  for (double strain = 1e-3; strain <= 0.2e-1; strain += 0.005e-1)
  {
    creepRate = lcsModel.CreepRate(600,100e6,strain);
    tts = lcsModel.TimeToStrain(600,100e6,strain);
    std::pair<double,double> toStrainRates(tts,creepRate);
    strainRates.emplace(toStrainRates);
  }

  EXPECT_NEAR(strainRates.at(77.213199243801384),8.0e-6,2e-6);
  EXPECT_NEAR(strainRates.at(1005.0595593236349),1.0e-6,3e-7);
  EXPECT_NEAR(strainRates.at(11220.911098910803),0.4e-6,1e-7);
}

TEST(CreepModelP91, strainIncremented)
{
  // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
  // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
  // Values should agree to fig. 4 with data from table 19. 

  boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> ttrModel = boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> (new mcr::TimeToRuptureMansonBrown4P91EN);
  tests::creep::PublicModelLogisticCreepStrain lcsModel(ttrModel);

  std::map<double,double> strains;

  double strain = 0.003;
  double time = lcsModel.TimeToStrain(600,100e6,strain);
  double creepRate, creepRatePrev = lcsModel.CreepRate(600,100e6,strain);;
  double dt = 2000;

  for (time += dt ; time<=80000 ; time += dt)
  {
    creepRate = lcsModel.CreepRate(600,100e6,strain);
    strain += (creepRate + creepRatePrev) * dt / 2;
    std::pair<double,double> toStrains(time,strain);
    strains.emplace(toStrains);
  }


  EXPECT_NEAR(strains.at(11005.059559323636),0.01254,1e-4);
  EXPECT_NEAR(strains.at(41005.059559323636),0.0585,1e-4);
  EXPECT_NEAR(strains.at(51005.059559323636),0.1807,1e-4);
}


TEST(CreepModelP22, timeToRupture)
{
  // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
  // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
  // Values should agree to fig. 18 with data from table 3. 

  mcr::TimeToRuptureMansonBrown4P22 ttrModel;
  double ttrValue = ttrModel.TimeToRupture(575,100e6);
  EXPECT_NEAR(ttrValue,3.244e+03,1.0);
  EXPECT_NEAR(log10(ttrValue),3.5112,0.01);
  ttrValue = ttrModel.TimeToRupture(575,120e6);
  EXPECT_NEAR(log10(ttrValue),2.9198,0.01);
  ttrValue = ttrModel.TimeToRupture(575,140e6);
  EXPECT_NEAR(log10(ttrValue),2.2977,0.01);
  ttrValue = ttrModel.TimeToRupture(575,160e6);
  EXPECT_NEAR(log10(ttrValue),1.6433,0.01);
  ttrValue = ttrModel.TimeToRupture(575,180e6);
  EXPECT_NEAR(log10(ttrValue),0.958,0.01);
}


TEST(CreepModelP22, x0Computation)
{
  // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
  // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
  // Values should agree to table 10 with data from table 19. 

  boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P22> ttrModel = boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P22> (new mcr::TimeToRuptureMansonBrown4P22);
  tests::creep::PublicModelLogisticCreepStrainP22 lcsModel(ttrModel);
  double x0 = lcsModel.X0(560,100e6);
  EXPECT_NEAR(x0,-3.072,0.001);
}

TEST(CreepModelP22, pComputation)
{
  // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
  // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
  // Values should agree to table 10 with data from table 19. 

  boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P22> ttrModel = boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P22> (new mcr::TimeToRuptureMansonBrown4P22);
  tests::creep::PublicModelLogisticCreepStrainP22 lcsModel(ttrModel);
  double p = lcsModel.P(560,100e6);
  EXPECT_NEAR(p,4.473,0.001);
}


TEST(CreepModelP22, timeToStrain)
{
  // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
  // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
  // Values should agree to fig. 2 from page IV/3 with data from table 19. 
  // Results are not exact, despite the same coefficients

  boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P22> ttrModel = boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P22> (new mcr::TimeToRuptureMansonBrown4P22);
  tests::creep::PublicModelLogisticCreepStrainP22 lcsModel(ttrModel);
  double tts;
  std::map<double,double> timesToStrain;

  for (double strain = 1e-3; strain <= 2e-1; strain += 0.05e-1)
  {
    tts = lcsModel.TimeToStrain(598,100e6,strain);
    std::pair<double,double> toTts(strain,tts);
    timesToStrain.emplace(toTts);
  }
  EXPECT_NEAR(timesToStrain.at(0.13600000000000004),480,30);
  EXPECT_NEAR(timesToStrain.at(0.026000000000000002),220,30);
  EXPECT_NEAR(timesToStrain.at(0.016000000000000000),110,10);

}

// Results not agreeable - something wrong with properties? Cannot be compared with the source, lack of data.
// TEST(CreepModelP22, strainIncremented)
// {
//   // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
//   // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
//   // Values should agree to fig. 4 with data from table 19. 
// 
//   tests::creep::PublicModelLogisticCreepStrainP22 lcsModel;
// 
//   std::map<double,double> strains;
// 
//   double strain = 0.005;
//   double time = lcsModel.TimeToStrain(600,100e6,strain);
//   double creepRate, creepRatePrev = lcsModel.CreepRate(600,100e6,strain);;
//   double dt = 10;
// 
//   for (time += dt ; time<=1000 ; time += dt)
//   {
//     creepRate = lcsModel.CreepRate(600,100e6,strain);
//     strain += (creepRate + creepRatePrev) * dt / 2;
//     std::pair<double,double> toStrains(time,strain);
//     strains.emplace(toStrains);
//   }
// 
// 
//   EXPECT_NEAR(strains.at(11005.059559323636),0.01254,1e-4);
//   EXPECT_NEAR(strains.at(41005.059559323636),0.0585,1e-4);
//   EXPECT_NEAR(strains.at(51005.059559323636),0.1807,1e-4);
//  }





TEST(CreepModelPrec, timeToRupture)
{
//  boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> ttrModel = boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN>(new boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN>);

  
  
  //boost::shared_ptr<tests::creep::PublicModelLogisticCreepStrain> lcsModel = boost::shared_ptr<tests::creep::PublicModelLogisticCreepStrain>(new tests::creep::PublicModelLogisticCreepStrain) ;
  boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> ttrModel = boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> (new mcr::TimeToRuptureMansonBrown4P91EN);
  tests::creep::TimeToRupturePrecipitationTest test(ttrModel);

  std::map<double,double> ttr;
  std::map<double,double> ttrP;
  

  for (double stress = 100e6 ; stress <= 200e6; stress += 20e6)
  {
    
    std::pair<double,double> toTtrP (stress,test.LogTimeToRupture(575,stress));
    ttrP.emplace(toTtrP);
    std::pair<double,double> toTtr(stress,ttrModel->LogTimeToRupture(575,stress));
    ttr.emplace(toTtr);
    EXPECT_GT(test.LogTimeToRupture(575,stress),ttrModel->LogTimeToRupture(575,stress));
  }

}

TEST(CreepModelPrec, strainRate)
{
  // Testing values from Engineering Tools for Robust Creep Modeling by Stefan Holmstrom
  // http://www.vtt.fi/inf/pdf/publications/2010/P728.pdf
  // Values should agree to fig. 4 with data from table 19. 
  // Not exactly agreeable, but quite similar

  boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> ttrBaseModel = boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> (new mcr::TimeToRuptureMansonBrown4P91EN);
  boost::shared_ptr<tests::creep::PublicModelLogisticCreepStrain> lcsModel = boost::shared_ptr<tests::creep::PublicModelLogisticCreepStrain> (new tests::creep::PublicModelLogisticCreepStrain(ttrBaseModel));

  double creepRate;
  double tts;
  std::map<double,double> strainRates;

  for (double strain = 1e-3; strain <= 0.2e-1; strain += 0.005e-1)
  {
    creepRate = lcsModel->CreepRate(600,100e6,strain);
    tts = lcsModel->TimeToStrain(600,100e6,strain);
    std::pair<double,double> toStrainRates(tts,creepRate);
    strainRates.emplace(toStrainRates);
  }
  /// 

  boost::shared_ptr<tests::creep::TimeToRupturePrecipitationTest> ttrModel = boost::shared_ptr<tests::creep::TimeToRupturePrecipitationTest> (new tests::creep::TimeToRupturePrecipitationTest(ttrBaseModel));
  boost::shared_ptr<tests::creep::PublicModelLogisticCreepStrainPrecipitation> lcsModelBase = boost::shared_ptr<tests::creep::PublicModelLogisticCreepStrainPrecipitation> (new tests::creep::PublicModelLogisticCreepStrainPrecipitation(ttrModel));
  tests::creep::PublicModelLogisticCreepStrainP91Precipitations lcsModelP(lcsModelBase);

  std::map<double,double> strainRatesP;

  for (double strain = 1e-3; strain <= 0.2e-1; strain += 0.005e-1)
  {
    creepRate = lcsModelP.CreepRate(600,100e6,strain);
    tts = lcsModelP.TimeToStrain(600,100e6,strain);
    std::pair<double,double> toStrainRates(tts,creepRate);
    strainRatesP.emplace(toStrainRates);
  }
FAIL();
}