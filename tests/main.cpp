/*! \file main.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       Main file will all tests
 * \details   	
*********************************************************************/
/*	include *********************************************************/

//#include "../softwareDeformBuiltin/deformHandlerUserProc.h"
#include "../common/config.h"
#include "../models/models.h"
#include "tests.h"

#ifdef TESTS_ON
#include "gtest/gtest.h"
#endif

/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/

int main(int argc, char **argv)
{

#ifdef TESTS_ON

	#ifdef MATCALC //matcalc.lib necessary for this tests. 
		 am3::tests::RegisterSoftwareMatCalcMain();
	#endif

  // Each *_test.cpp file must define Register*() function; declaration in tests.h
	 am3::tests::RegisterVariableSource();

  ::testing::InitGoogleTest(&argc,argv);

  RUN_ALL_TESTS();
#endif
}

