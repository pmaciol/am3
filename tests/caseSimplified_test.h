
/*! \file caseSimplified_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseSimplified_test_h__
#define caseSimplified_test_h__
/*	include *********************************************************/

// Must be included first due to stupid winsock problems!
#include "../kbsRebit/reasoner.h"
#include "../kbsRebit/gSoapReasoningService.h"

#include "../caseSimplified/caseSimplified.h"



/*	using ***********************************************************/

namespace csi = am3::cases::simpified;
namespace kre = am3::gsoap;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace tests
{
	namespace caseSimplified
	{
		using namespace csi;
		MODEL(NullStressModel,DeformStressContract,3,NullInput)
			mpr::YieldStress NullStressModel::GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::YieldStress *const ) {mpr::YieldStress toRet;toRet(0);return toRet;};  //Sherstnev
		mpr::StressDStrain NullStressModel::GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StressDStrain *const ) {mpr::StressDStrain toRet;toRet(0);return toRet;};
		mpr::StressDStrainRate NullStressModel::GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StressDStrainRate *const ) {mpr::StressDStrainRate toRet;toRet(0);return toRet;};

		struct Consts
		{
			static const double yieldStressCheck;
			static const double yieldStressDStrainCheck;
			static const double yieldStressDStrainRateCheck;
			static const int nodesNumber;
			static const double coordinates[8],coordinates2[8];
			static const double time,time2;
			static const int elementId;
			static const int meshId;
			static const mpr::Coordinates2D coord;
			static const mpr::Time t;
			static pnt::ComputedPoint<mpr::Coordinates2D> point;
		};

    class Kbs
		{
		public:
			template <typename VARIABLE> mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point)
			{
				const VARIABLE* trait=nullptr;
				return GetModelId(point,trait);
			}
			Kbs(boost::shared_ptr<csi::CaseSimplifiedDatabus> databus): databus_(databus){};
		protected:
			boost::shared_ptr<csi::CaseSimplifiedDatabus> databus_;
			mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::RecrystalizedGrainSize* const)
			{
				boost::shared_ptr<moc::Provider<ppm::contract::ZenerParam,mpr::Coordinates2D> > zenerDragProvider;
				boost::shared_ptr<moc::Provider<mpr::EffectiveStrainRate,mpr::Coordinates2D> > efepseProvider;
				efepseProvider = databus_;
				zenerDragProvider = databus_;
				mpr::EffectiveStrainRate efepse = efepseProvider->GetState(point,&efepse);

 				ppm::contract::StoredEnergy storedEnergy = databus_->PreviousData()->GetPreviousState<ppm::contract::StoredEnergy>();
				ppm::contract::ZenerParam zenerDrag = zenerDragProvider->GetState(point,&zenerDrag);
				mpr::ModelID mId;

				if (efepse() > 0.001)
				{
					mId(2);
				} 
				else
				{
					if (storedEnergy() < zenerDrag())
					{
						mId(0);
					} 
					else
					{
						mId(1);
					}
				}
				return mId;
			}
			mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::GrainSize* const)
			{
				mpr::ModelID mId;
				mId(0);
				return mId;
			}
			mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::NumberOfRecrystalizedNuclei* const)
			{
				mpr::ModelID mId;
				mId(0);
				return mId;
			}
			mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::VolumeFractionOfRecrystallizedGrains* const)
			{
				mpr::ModelID mId;
				mId(0);
				return mId;
			}
			mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::StoredEnergy* const)
			{
				mpr::ModelID mId;
				mId(0);
				return mId;
			}
		};


    class TestGSoapReasoner : public kre::Reasoner
    {
    public:
      virtual std::vector<std::string> *TemplateGetVariableValue(std::string varId)
      {
        resultingVariable.clear();
        resultingVariable = databusInterface->Value(varId);
        return &resultingVariable;
      }
      TestGSoapReasoner(kre::ReasoningServiceProxy *proxy, kre::Logger * logger,boost::shared_ptr<csi::CaseSimplifiedDatabus> databus) : kre::Reasoner(proxy, logger), databusInterface(new csi::CaseSimplifiedDatabusStringInterface(databus))   { };
    protected:
      boost::shared_ptr<csi::CaseSimplifiedDatabusStringInterface> databusInterface;
      std::vector<std::string> resultingVariable;
    };

    class KbsWithRebit
    {
    public:
      template <typename VARIABLE> mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point)
      {
        const VARIABLE* trait=nullptr;
        return GetModelId(point,trait);
      }
      KbsWithRebit(boost::shared_ptr<csi::CaseSimplifiedDatabus> databus): databus_(databus){};
    protected:
      boost::shared_ptr<csi::CaseSimplifiedDatabus> databus_;
      mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::RecrystalizedGrainSize* const)
      {

        string serviceEndPoint = "http://localhost:8888/RebitEngineService";
        string thermostatPath = "c:/PiotrOsobno.xml";
        string ruleSetId = "PiotrOsobno";
        string finalVariableId = "ModelId";
        string kb = kre::GetXmlFromFile(thermostatPath);
        kre::Reasoner *mixReasoner = new TestGSoapReasoner(new kre::GSoapReasoningService(serviceEndPoint), new kre::NullLogger(), databus_); // change to ScreenLogger() if do want screen prints 
        kre::ResponseDTO *resp = mixReasoner->Run(kb, ruleSetId, finalVariableId); 
        kre::VariableDTO *varDTO = &(*resp->InteractionVariables)[0];	
        string result = (*varDTO->Value)[0];
        mpr::ModelID mId;
        mId(boost::lexical_cast<int>(result));
        return mId;
      }
      mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::GrainSize* const)
      {
        mpr::ModelID mId;
        mId(0);
        return mId;
      }
      mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::NumberOfRecrystalizedNuclei* const)
      {
        mpr::ModelID mId;
        mId(0);
        return mId;
      }
      mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::VolumeFractionOfRecrystallizedGrains* const)
      {
        mpr::ModelID mId;
        mId(0);
        return mId;
      }
      mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::StoredEnergy* const)
      {
        mpr::ModelID mId;
        mId(0);
        return mId;
      }
    };
	}

}

#endif // caseSimplified_test_h__
