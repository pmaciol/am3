
/*! \file softwareDeform_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../softwareDeform/policyOneStepOneElement.h"
#include "../softwareDeform/policyOneStepOneMesh.h"
#include "../modelTemplates/macroStress.h"
#include "mockMacroModelsDeform.h"
#include "softwareDeform_test.h"

#include "../softwareDeform/policyOneStepOneMeshTemperatureTime.h"
#include "../softwareDeform/policyOneStepOneMesh.h"
#include "../models/modelsManager.h"
#include "../mockModels/mockMicroscale.h"
#include "../caseMatCalcMPI/caseMatCalcMPI.h"


/*	using ***********************************************************/

namespace am3 { namespace tests { void RegisterSoftwareDeform(){}; } }

namespace sdm = am3::software::deform;
namespace mot = am3::model::templates;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;
namespace mom = am3::model::manager;
namespace mmk = am3::model::mock;


/*	extern **********************************************************/

TEST(SoftwareDeformMesh, OneStepOneMeshTemperatureTime)
{
	sdm::OneStepOneMeshTemperatureTime test1;

	unsigned int materialId = 0;
	double temperatures[5] {0, 1, 2, 3, 4};
	double deltaTemperatures[5] {0, -0.1, -0.2, -0.3, -0.4};
	unsigned nodeStart = 0;
	unsigned nodeEnd = 4;


	test1.SetTemperaturesInNodes(materialId, 0.1, 0.2, temperatures, deltaTemperatures, nodeStart, nodeEnd);
	EXPECT_EQ(nodeEnd, test1.GetNodeLast());
	EXPECT_EQ(nodeStart, test1.GetNodeStart());
	EXPECT_EQ(5, test1.GetNodesNumber());

	EXPECT_DOUBLE_EQ(temperatures[0] + deltaTemperatures[0], test1.GetNewTemperature(0).Value());
	EXPECT_DOUBLE_EQ(temperatures[4] + deltaTemperatures[4], test1.GetNewTemperature(4).Value());

	EXPECT_DOUBLE_EQ(temperatures[0], test1.GetOldTemperature(0).Value());
	EXPECT_DOUBLE_EQ(temperatures[4], test1.GetOldTemperature(4).Value());

	try
	{
		test1.GetOldTemperature(5);
		FAIL();
	}
	catch (...)  {}

	nodeStart = 2;
	nodeEnd = 6;
	test1.SetTemperaturesInNodes(materialId, 0.1, 0.2, temperatures, deltaTemperatures, nodeStart, nodeEnd);
	EXPECT_DOUBLE_EQ(temperatures[0] + deltaTemperatures[0], test1.GetNewTemperature(2).Value());
	EXPECT_DOUBLE_EQ(temperatures[4] + deltaTemperatures[4], test1.GetNewTemperature(6).Value());

	try
	{
		test1.GetOldTemperature(1);
		FAIL();
	}
	catch (...)  {}

	EXPECT_DOUBLE_EQ(0.1, test1.GetTime().Value());
	EXPECT_DOUBLE_EQ(0.2, test1.GetStepLength().Value());
}

TEST(SoftwareDeformMesh, OneStepOneMeshTime)
{
	sdm::OneStepOneMeshTime time;

	time.SetStepLength(0.1);
	time.SetTime(0.2);

	EXPECT_DOUBLE_EQ(0.1, time.GetStepLength().Value());
	EXPECT_DOUBLE_EQ(0.2, time.GetTime().Value());
}


TEST(SoftwareDeform, MtrHandlerExistanceCheck)
{
	const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
	const mpr::Time t = mpr::Time(0.0);
	mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);
	typedef mot::MacroStressFromDatabus_GivenPoint2D<tests::ConstantStressDeform> EmulateStressesFromDatabus;
	boost::shared_ptr<EmulateStressesFromDatabus> emulatedDatabus(new EmulateStressesFromDatabus(&point));

	// Object communicating to Deform (usr_mtr procedure)
	boost::shared_ptr<sdm::OneStepOneElementNoElementCheckStresses> stateProvider(new sdm::OneStepOneElementNoElementCheckStresses(emulatedDatabus));

	// Global function sdm::GetDeformHandlerFacadeMtrPtr() checks, is pointer to the object communicating to Deform is set
	sdm::UnsetDeformHandlerFacadeMtr();
	EXPECT_FALSE(sdm::GetDeformHandlerFacadeMtrPtr() != 0);
	sdm::SetDeformHandlerFacadeMtr(stateProvider);
	EXPECT_TRUE(sdm::GetDeformHandlerFacadeMtrPtr() != 0);
}

TEST(SoftwareDeform, OneStepOneElementNoElementCheckStresses_ProvidesAllData)
{
	const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
	const mpr::Time t = mpr::Time(0.0);
	mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);
	typedef mot::MacroStressFromDatabus_GivenPoint2D<tests::ConstantStressDeform> EmulateStressesFromDatabus;
	boost::shared_ptr<EmulateStressesFromDatabus> emulatedDatabus(new EmulateStressesFromDatabus(&point));
	// Object communicating to Deform (usr_mtr procedure)
	boost::shared_ptr<sdm::OneStepOneElementNoElementCheckStresses> stateProvider(new sdm::OneStepOneElementNoElementCheckStresses(emulatedDatabus));
	// Setting the pointer to the object communicating to Deform
	sdm::SetDeformHandlerFacadeMtr(stateProvider);

	// Interface for getting macroscopic state from Deform (usr_mtr procedure)
	boost::shared_ptr<mot::OneStepOneElementMacroState> macroState = stateProvider;

	// Testing values
	double effStrain = 0.1;
	double effStrainRate = 1.0;
	double temperature = 700;
	const int meshId = 0;
	const int elementId = 0;
	double time = 0.12;
	double timestep = 0.001;

	// 'usr_mtr procedure action' - normally it is done by Deform
	stateProvider->SetEffectiveStrain(&effStrain, meshId, elementId);
	stateProvider->SetEffectiveStrainRate(&effStrainRate, meshId, elementId);
	stateProvider->SetTemperature(&temperature, meshId, elementId);
	stateProvider->SetTime(&time, meshId, elementId);
	stateProvider->SetStepLength(&timestep, meshId, elementId);

	// Testing 'am3-side' interface
	mpr::TotalEffectiveStrain tes;
	tes = macroState->GetEffectiveStrain();
	mpr::EffectiveStrainRate esr;
	esr = macroState->GetEffectiveStrainRate();
	mpr::Temperature temp;
	temp = macroState->GetTemperature();
	mpr::Time ti;
	ti = macroState->GetTime();
	mpr::StepLength stLength;
	stLength = macroState->GetStepLength();

	EXPECT_DOUBLE_EQ(effStrain, tes());
	EXPECT_DOUBLE_EQ(effStrainRate, esr());
	EXPECT_DOUBLE_EQ(temperature, temp());
	EXPECT_DOUBLE_EQ(time, ti());
	EXPECT_DOUBLE_EQ(timestep, stLength());
}

TEST(SoftwareDeform, OneStepOneElementNoElementCheckStresses_GetsStressesBack)
{
	const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
	const mpr::Time t = mpr::Time(0.0);
	mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);
	typedef mot::MacroStressFromDatabus_GivenPoint2D<tests::ConstantStressDeform> EmulateStressesFromDatabus;
	boost::shared_ptr<EmulateStressesFromDatabus> emulatedDatabus(new EmulateStressesFromDatabus(&point));
	// Object communicating to Deform (usr_mtr procedure)
	boost::shared_ptr<sdm::OneStepOneElementNoElementCheckStresses> stateProvider(new sdm::OneStepOneElementNoElementCheckStresses(emulatedDatabus));

	tests::ConstantStressDeform testingStresses(0.1, 0.2, 0.3);

	emulatedDatabus->SetModelInput(&testingStresses);

	// Setting, where Deform handler should take stresses from
	//  stateProvider->SetStressesHandler(emulatedDatabus);

	const int meshId = 0;
	const int elementId = 0;
	double ys = stateProvider->GetYieldStress(meshId, elementId);
	double ysds = stateProvider->GetYieldStressDStrain(meshId, elementId);
	double ysdsr = stateProvider->GetYieldStressDStrainRate(meshId, elementId);
	EXPECT_DOUBLE_EQ(0.1, ys);
	EXPECT_DOUBLE_EQ(0.2, ysds);
	EXPECT_DOUBLE_EQ(0.3, ysdsr);
}

TEST(SoftwareDeform, UpdHandlerExistanceCheck)
{
	// Emulates real submodels updating IV values
	boost::shared_ptr<tests::MockNewIvSource> mockNewIvSource(new tests::MockNewIvSource);

	// Object linking Deform (usr_upd procedure) and subbmodels (both directions)
	boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivProvider(new sdm::OneStepOneElementNoElementCheckUserValues(mockNewIvSource));

	// Global function sdm::GetDeformHandlerFacadeUpdPtr() checks, is pointer to the object communicating to Deform is set
	sdm::UnsetDeformHandlerFacadeUpd();
	EXPECT_FALSE(sdm::GetDeformHandlerFacadeUpdPtr() != 0);
	sdm::SetDeformHandlerFacadeUpd(ivProvider);
	EXPECT_TRUE(sdm::GetDeformHandlerFacadeUpdPtr() != 0);
}

TEST(SoftwareDeform, OneStepOneElementNoElementCheckIV_ProvidesAllData)
{
	// Emulates real submodels updating IV values
	boost::shared_ptr<tests::MockNewIvSource> mockNewIvSource(new tests::MockNewIvSource);

	// Object linking Deform (usr_upd procedure) and subbmodels (both directions)
	boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivProvider(new sdm::OneStepOneElementNoElementCheckUserValues(mockNewIvSource));
	sdm::SetDeformHandlerFacadeUpd(ivProvider);

	// Interface for getting IVs from Deform (usr_upd procedure)
	boost::shared_ptr<mot::OneStepOneElementIvPresent> presentIVs = ivProvider;

	// Testing values
	const int length = 4;
	const double values[4] = { 0, 1, 2, 3 };
	const unsigned meshId = 0;
	const unsigned elementId = 0;

	// 'usr_upd procedure action' - normally it is done by Deform
	ivProvider->SetElementalVariables(length, values, meshId, elementId);

	// Testing 'am3-side' interface
	std::vector<double> ivValues;
	ivValues = presentIVs->GetInternalVariables();

	EXPECT_DOUBLE_EQ(values[0], ivValues[0]);
	EXPECT_DOUBLE_EQ(values[3], ivValues[3]);
	EXPECT_DOUBLE_EQ(values[0], presentIVs->GetInternalVariable(0));
	EXPECT_DOUBLE_EQ(values[3], presentIVs->GetInternalVariable(3));

	bool check = (presentIVs->GetInternalVariable(4) == presentIVs->GetInternalVariable(4)) ? true : false;
	EXPECT_FALSE(check);
	check = (presentIVs->GetInternalVariable(-1) == presentIVs->GetInternalVariable(-1)) ? true : false;
	EXPECT_FALSE(check);
}


TEST(SoftwareDeform, OneStepOneElementNoElementCheckIV_GetsIVsBack)
{
	// Emulates real submodels updating IV values
	boost::shared_ptr<tests::MockNewIvSource> mockNewIvSource(new tests::MockNewIvSource);

	// Object linking Deform (usr_upd procedure) and submodels (both directions)
	boost::shared_ptr<sdm::OneStepOneElementNoElementCheckUserValues> ivProvider(new sdm::OneStepOneElementNoElementCheckUserValues(mockNewIvSource));
	sdm::SetDeformHandlerFacadeUpd(ivProvider);

	// Testing values
	const int length = 4;
	double values[4];
	const unsigned meshId = 0;
	const unsigned elementId = 0;

	// Resizes mock IV source internal iv vector to length
	mockNewIvSource->Resize(length);

	EXPECT_EQ(1, ivProvider->GetElementalVariables(length, values, meshId, elementId));
	EXPECT_EQ(0, ivProvider->GetElementalVariables(length + 1, values, meshId, elementId));
}

// TEST(SoftwareDeform, OneStepOneMesh)
// {
//   boost::shared_ptr<sdm::OneStepOneMeshTemperaturesUserValues> stateProvider(new sdm::OneStepOneMeshTemperaturesUserValues);
//   sdm::SetDeformHandlerFacadeMshTemperatureUsrElemVal(stateProvider);
//   EXPECT_TRUE(sdm::GetDeformHandlerFacadeMshTempElemPtr() != 0);
// 
//   double time = 0.1;
//   stateProvider->SetTime(&time);
//   mpr::Time timeProp;
//   timeProp = stateProvider->GetTime();
//   EXPECT_DOUBLE_EQ(time, timeProp());
// 
//   double stepLength = 0.2;
//   stateProvider->SetStepLength(&stepLength);
//   mpr::StepLength stepLengthProp;
//   stepLengthProp = stateProvider->GetStepLength();
//   EXPECT_DOUBLE_EQ(stepLength, stepLengthProp());
// 
// 
//   int maxEl = 3;
//   int maxNodes = 8;
//   int matId = 0;
//   int nodesPerElem = 4;
// 
//   double temperatures[8] = { 800, 800, 800 , 800, 700, 700, 700, 700};
//   double deltaTemperatures[8] = { -10, -10, -20, -20, -20, -20, -10, -10};
//   int connectivityMat[12] = { 0, 1, 2, 3, 3, 2, 5, 4, 4, 5, 6, 7 };
// 
//   mpr::Temperature prevTempProp, newTempProp;
//   mpr::ElementNumber elNum;
//   
// 
//   stateProvider->SetTemperaturesInNodes(temperatures, deltaTemperatures, matId, maxEl, nodesPerElem, connectivityMat );
//   
//   elNum(1);
//   prevTempProp = stateProvider->GetOldTemperature(elNum());
//   newTempProp = stateProvider->GetNewTemperature(elNum());
// 
//   EXPECT_DOUBLE_EQ(750, prevTempProp());
//   EXPECT_DOUBLE_EQ(730, newTempProp());
// 
//   double internalVariable;
// 
//   int numUsVar = 2;
//   double elemVals[6] = { 1, 1, 2, 2, 3, 3 };
//   stateProvider->SetAllElementalVariables(elemVals, &matId, &numUsVar, &maxEl);
//   
//   internalVariable = stateProvider->GetInternalVariable(elNum(), 0);
//   EXPECT_DOUBLE_EQ(2, internalVariable);
//   internalVariable = stateProvider->GetInternalVariable(elNum(), 1);
//   EXPECT_DOUBLE_EQ(2, internalVariable);
// 
// 
// }

/*	classes *********************************************************/





