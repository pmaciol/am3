
/*! \file caseTugMatCalc_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../caseTugMatCalc/dllExport.h"
#include "../caseTugMatCalc/main.h"

/*	using ***********************************************************/

namespace ctm = am3::cases::tugmc;

/*	extern **********************************************************/



/*	classes *********************************************************/

//namespace am3 { namespace tests { void RegisterCaseTugMC(){}; } }

TEST(caseTugMC_deformAPI, mtrTest)
{


	unsigned int materialId = 0;
	double temperatures[5] {0, 1, 2, 3, 4};
	double deltaTemperatures[5] {0, -0.1, -0.2, -0.3, -0.4};
	unsigned nodeStart = 0;
	unsigned nodeEnd = 4;
	double time = 0.1, timeStep = 0.1;

	int test = DEFORM_TEMPERATURES_SET(materialId, time, timeStep, temperatures, deltaTemperatures, nodeStart, nodeEnd);
	double ys, ysdstr, ysdstrrate; 
	int mId, eId;
	double effStrain = 0.1, effStrainRate = 0.1, temperature = 700;

	int length = 4;
	double values[4] = { 0.1, 0.2, 0.3, 0.4 };


	test = DEFORM_TIME(&time, &timeStep, &mId, &eId);
  test = DEFORM_USRE1_SEND(&length, values, &temperature, &time, &timeStep, &mId, &eId);
	test = DEFORM_TEMPERATURE(&temperature, &mId, &eId);
	test = DEFORM_EFF_STR(&effStrain, &mId, &eId);
	test = DEFORM_EFF_STR_RATE(&effStrainRate, &mId, &eId);
	test = DEFORM_ALL_STRESS_GET(&ys, &ysdstr, &ysdstrrate, &mId, &eId);


	EXPECT_EQ(test, 0);

	
}

TEST(caseTugMC_deformAPI, DatabusTests)
{
	extern am3::cases::tugmc::Main*  mainInstance;
	mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
	const mpr::Time t = mpr::Time(0.0);
	mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);

	mpr::PrecipitationsMeanRadius mRadius = mainInstance->mainDatabus_->GetFromDatabus<mpr::PrecipitationsMeanRadius>(&point);

}