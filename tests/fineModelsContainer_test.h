
/*! \file fineModelsContainer_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef fineModelsContainer_test_h__
#define fineModelsContainer_test_h__
/*	include *********************************************************/

#include "../models/fineModelsContainer.h"
#include "../models/description.h"
#include "../storage/cons.h"
#include "../utilMesh/barycentricApprox.h"
//#include "../softwareDeformBuiltin/deformHandler.h"

/*	using ***********************************************************/

namespace mo = am3::model;
namespace mpr = am3::model::properties;
namespace mtm = am3::math::mesh;
//namespace sdm = am3::software::deform;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace tests
{
	namespace model
	{
//     class MeshDeform: public mtm::Mesh<2>
//     {
//     public:
//       bool ImportMesh(boost::shared_ptr<sdm::DeformHandlerApprox<mpr::Coordinates2D>>>);
//     };


    template<typename COORD>
		class MovementCoordinatesMock: public mo::MovementCoordinates<COORD>
    {
      virtual void GetNewPosition( mpt::Point<COORD> &oldPosition );
    };

    
    typedef cons<mpr::Displacement2D>::type Displacement2DData;
    typedef MakeData<Displacement2DData> Displacement2DStore;

    class DataProvider: public mtm::NodeDataProvider<Displacement2DStore>
    {
    protected:
      std::map<int,Displacement2DStore> data_;
    public:
      virtual Displacement2DStore* GetValue(int node_id)
      {
        Displacement2DStore data_[10];
        mpr::Displacement2D disp1,disp2;
        disp1.x(1);
        disp1.y(1);
        disp2.x(2);
        disp2.y(1);
        Field<0>(data_[0]) = disp1;
        Field<0>(data_[1]) = disp1;
        Field<0>(data_[2]) = disp1;
        Field<0>(data_[3]) = disp2;
        Field<0>(data_[4]) = disp1;
        return &data_[node_id];
        //return &(data_.find(node_id)->second);

      }
      void Add(const int node_id, const Displacement2DStore& data)
      {
        data_.emplace(std::pair<int,Displacement2DStore>(node_id,data));
      }
    };

  } //model
} //tests

//////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION
//////////////////////////////////////////////////////////////////////////

template<>
void tests::model::MovementCoordinatesMock<mpr::Coordinates2D>::GetNewPosition( mpt::Point<mpr::Coordinates2D> &oldPosition )
{
  mpr::Coordinates2D oldPoint = oldPosition.Coordinates();
  mpr::Coordinates2D newCoord(oldPoint.x()*2,oldPoint.y()*2);
  double oldTime = oldPosition.GetTime();
  mpr::Time newTime(oldTime + 0.1);
  mpt::ComputedPoint<mpr::Coordinates2D> newPoint (newCoord,newTime);
  oldPosition.Move(newPoint);
}
#endif // fineModelsContainer_test_h__
