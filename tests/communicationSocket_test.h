// 
// /*! \file communicationSocket_test.h **************************************************
//  * \author		Piotr Maciol
//  * \copyright 	GNU Public License.
//  * \brief       
//  * \details   	
// *********************************************************************/
// #ifndef tests_communicationSocket_test_h__
// #define tests_communicationSocket_test_h__
// /*	include *********************************************************/
// 
// #include "gtest/gtest_prod.h"
// #include "../communicationSocket/socketServer.h"
// 
// /*	using ***********************************************************/
// 
// 
// 
// /*	extern **********************************************************/
// 
// 
// 
// /*	classes *********************************************************/
// 
// class CallProcessorTest: public am3::common::communication::CallProcessor
// {
//   virtual std::vector<std::string> ProcessExternalCall( const std::vector<std::string>& cameFromSocket );
// };
// 
// class SocketServerPimpl 
//   :public am3::common::communication::SocketServer
// 
// {
// public:
//   SocketServerPimpl(boost::shared_ptr<am3::common::communication::CallProcessor> callProcessorPtr)
//     :SocketServer(callProcessorPtr){};
//   bool isSocketInitialized(){return am3::common::communication::SocketServer::isSocketInitialized();}
// };
// 
// class SocketServerFixt: public ::testing::Test
// {
// 
// };
// 
// #endif // tests_communicationSocket_test_h__
