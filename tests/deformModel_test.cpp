#include "deformModel_test.h"
#include "../softwareDeform/deformCallProcessor.h"
#include "../softwareDeform/deformDataInterface.h"
#include "../softwareDeform/storage.h"


namespace am3 {namespace tests {void RegisterDeformModel(){};}}
#include <vector>
#include <string>
#include <boost/lexical_cast.hpp>

#include "gtest/gtest.h"
#include "deformModel_test.h"
#include "../common/config.h"

#include "../problemPlasticAndHeat/fineCoarseDumbAdapter.h"
#include "../problemPlasticAndHeat/fineTugCa.h"
#include "../problemPlasticAndHeat/fineModelSSCurves.h"
#include "../problemPlasticAndHeat/kbsConverter.h"

#include "plasticAndHeat_test.h"

//#include "communicationSocket_test.h"

namespace dfc=am3::common::communication::deform;
namespace sdfrm=am3::software::deform;
namespace pah=am3::problem::plasticAndHeat;
namespace kph=am3::knowledge::plasticAndHeat;



TEST(DeformConfiguration_Test, isNotInititlized)
{
  DeformConfigurationTest defConf;

  ASSERT_FALSE(defConf.IsConfigured());
}

TEST(DeformMtrCallProcessor_test, start)
{

  //////////////////////////////////////////////////////////////////////////
  // DEFORM side
  //////////////////////////////////////////////////////////////////////////

  // Process Deform calls. Gets data from MTR procedure, have to return material properties.
  // Must be initialized with:
  // * Configuration (connection details)
  // * Storage; data from Deform are stored in this class, it is polymorphic.
  // * OtherModels; reference to fine scale models manager
  dfc::DeformMtrCallProcessor callProc;

  // Storage for coarse scale data.
  // This object is passive, does not call any other objects. 
  // Some variants could be active and have ability to call coarse model for additional data
  boost::shared_ptr<sdfrm::storage::OnePointOneElementStorage> storage(new sdfrm::storage::OnePointOneElementStorage);

  // Configuration of Deform model
  boost::shared_ptr<dfc::DeformConfiguration> configuration;

  //////////////////////////////////////////////////////////////////////////
  // P&H problem side
  //////////////////////////////////////////////////////////////////////////
  // pah::PlasticAndHeat2DCoarseCaller<sdfrm::storage::OnePointOneElementStorage> implements PlasticAndHeat2DCoarseContract interface
  // Have a reference to implementation of template defined interface of coarse model software (Deform here)
  // Must be initialized with a reference of realization of interface which is at least equal to PlasticAndHeat2DCoarseContract
  // Usually templated interface is wider than PlasticAndHeat2DCoarseContract
  // If at least equal interface is not available, specialized class must be derived, 
  // completing lacking methods in interface with internal methods or additional calls to other objects
  
  // pah::FineModelsSwitcher inherits from coarse scale model (Deform Caller here) 
  // and implements interface for P&H problems in fine scale.
  // Coarse scale calls are delegated to associated class, implementing PlasticAndHeat2DCoarseContract interface
  // Fine scale calls are delegated to "on-fly" chosen fine model; KBS should be used here.
  // Must be initialized with 
  // * I4Adapter interface realization (KBS)
  // * More than zero fine scale models have to be added. It must be compatible with KB. There is no mechanism to check this compatibility
  // * Depending on coarse scale model, it could be necessary to inititlize a model (SetCalledModel here)

  typedef pah::FineModelsSwitcher<
    pah::PlasticAndHeat2DCoarseCaller<sdfrm::storage::OnePointOneElementStorage>, 
    kno::I4Adapter<prop::Coordinates2D>
  > FMS;
  boost::shared_ptr<FMS> selector(new FMS);


  selector->SetCalledModel(storage);

  // Definitions of fine scale models
  boost::shared_ptr<pah::FineModelSSCurves> fmSSC(new pah::FineModelSSCurves);
  boost::shared_ptr<pah::FineModelTugCA> fmTUGCA(new pah::FineModelTugCA);

  // fine reference setting
  fmSSC->SetCoarseModel(selector);
  fmTUGCA->SetCoarseModel(selector);
  selector->AddFineInterface(fmSSC);
  selector->AddFineInterface(fmTUGCA);
  
  // KBS object
  boost::shared_ptr<tests::mockRebitClient> mockRebit(new tests::mockRebitClient);
  boost::shared_ptr<kph::PlasticAndHeat2RebitConverter> converter
    (new kph::PlasticAndHeat2RebitConverter(mockRebit, selector, selector,selector));

  // KBS reference setting
  selector->SetI4Adapter(converter);

  // A "decoration" of P&H problem fine contract to plug it into DeformMtrNeeds
  // Problem contract is different from needs of coarse software needs and it must be adapted
  // Implements DeformMtrNeeds interface
  // Have a reference to PlasticAndHeat2DFineContract
  // Must be initialized with PlasticAndHeat2DFineContract realization
  pah::FineCoarseDumbAdapter* adapter = new pah::FineCoarseDumbAdapter(selector);

  
  callProc.Init(adapter,storage.get()/*,configuration*/);


  std::vector<std::string> cameFromSocket;
  for (int i=0;i<100;i++)//ILE!!!!!!!!!!!!!!!!!!!
  {
    cameFromSocket.push_back(boost::lexical_cast<std::string>(i));
  }
  std::vector<std::string> modelResponse(0);
  modelResponse=callProc.ProcessExternalCall(cameFromSocket);
  EXPECT_GT(modelResponse.size(),0);

  delete adapter;
}
// The same?
// /************************************************************************
//  * ????? ModelAdinaTests causes gTest errors. There is a problem with complex objects creation on a heap
//  ************************************************************************/
// TEST(DeformConfiguration_Test, checkGetConfiguredModuleOptions)
// {
//   DeformConfigurationTest defConf;
//   defConf.GetConfiguredModuleOptions();
//   defConf.Init();
//   EXPECT_TRUE(am3::common::kProgramOptions.GetMainVariableMap()["DeformUsrVar1"].as<int>()>0);
// }
