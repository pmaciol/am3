
/*! \file kbs_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

/*	include *********************************************************/

#include "gtest/gtest.h"
#include <boost/lexical_cast.hpp>
#include "kbs_test.h"
#include "../adapter/adapter.h"
#include "../knowledge/kbsInterfaces.h"

/*	using ***********************************************************/

namespace ada = am3::adapter;
namespace tsk = tests::kbs;
namespace kno = am3::knowledge;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterKbs (){};}}

TEST(ExternalKbs, JustTest)
{
	tsk::ExternalTestingKbs externalKbs;
	tsk::ExternalTestingKbs::PropertyDescription mId;
	tsk::ExternalTestingKbs::PropertyDescription time = "0";
	tsk::ExternalTestingKbs::PropertyDescription coordinates = "0.5, 0.5";
	tsk::ExternalTestingKbs::PropertyDescription typeTrait = "ShearModulus";
	mId = externalKbs.GetModelId(coordinates, time, typeTrait);
	mpr::ModelID mIdProperty;
	mIdProperty(boost::lexical_cast<unsigned int>(mId));
	EXPECT_EQ(0,mIdProperty());
}

TEST(ExternalKbs, QueryForMoreData)
{
	boost::shared_ptr<tsk::DatabusForKbsTests> testDatabus (new tsk::DatabusForKbsTests) ;
	boost::shared_ptr<tsk::MacroInputsTest> macroModel (new tsk::MacroInputsTest);
	ada::BindModelToBus(testDatabus,macroModel);

	tsk::ExternalTestingKbs externalKbs;

	boost::shared_ptr<kno::Kbs<tsk::DatabusForKbsTests, tsk::ExternalTestingKbs> > kbs (new kno::Kbs<tsk::DatabusForKbsTests, tsk::ExternalTestingKbs>(testDatabus,externalKbs));
	mpr::Coordinates2D coord(0.5,0.5);
	mpr::Time t(2.0);
	mpt::ComputedPoint<mpr::Coordinates2D> point(coord,t);

	mpr::ModelID mId = kbs->GetModelId<mpr::ShearModulus>(&point);
	EXPECT_EQ(1 , mId());
}

TEST(RebitMock, QueryForMoreData)
{
	boost::shared_ptr<tsk::DatabusForKbsTests> testDatabus (new tsk::DatabusForKbsTests) ;
	boost::shared_ptr<tsk::MacroInputsTest> macroModel (new tsk::MacroInputsTest);
	ada::BindModelToBus(testDatabus,macroModel);

	tsk::MockRebitClient externalKbs;

 	boost::shared_ptr<kno::Kbs<tsk::DatabusForKbsTests, tsk::MockRebitClient> > kbs (new kno::Kbs<tsk::DatabusForKbsTests, tsk::MockRebitClient>(testDatabus,externalKbs));
	mpr::Coordinates2D coord(0.5,0.5);
	mpr::Time t(2.0);
	mpt::ComputedPoint<mpr::Coordinates2D> point(coord,t);

	mpr::ModelID mId = kbs->GetModelId<mpr::ShearModulus>(&point);
	EXPECT_EQ(1 , mId());

	mpr::Time t2(0.0);
	mpt::ComputedPoint<mpr::Coordinates2D> point2(coord,t2);
	mId = kbs->GetModelId<mpr::ShearModulus>(&point2);
	EXPECT_EQ(0 , mId());
}


TEST(Kbs, GetModelId)
{
	boost::shared_ptr<tsk::DatabusForKbsTests> testDatabus (new tsk::DatabusForKbsTests) ;
	boost::shared_ptr<tsk::MacroInputsTest> macroModel (new tsk::MacroInputsTest);
	ada::BindModelToBus(testDatabus,macroModel);

	tsk::ExternalTestingKbs externalKbs;

	boost::shared_ptr<kno::Kbs<tsk::DatabusForKbsTests, tsk::ExternalTestingKbs> > kbs (new kno::Kbs<tsk::DatabusForKbsTests, tsk::ExternalTestingKbs>(testDatabus,externalKbs));
	mpr::Coordinates2D coord(0.5,0.5);
	mpr::Time t(0.0);
	mpt::ComputedPoint<mpr::Coordinates2D> point(coord,t);
	
	mpr::ModelID mId = kbs->GetModelId<mpr::ShearModulus>(&point);
	EXPECT_EQ(0,mId());

	mpr::Time t2(1.0);
	mpt::ComputedPoint<mpr::Coordinates2D> point2(coord,t2);
	
	mId = kbs->GetModelId<mpr::ShearModulus>(&point2);
	EXPECT_EQ(100,mId());
}



TEST(DISABLED_Kbs, CreateSimpleKbs)
{

 	boost::shared_ptr<tsk::DatabusForKbsTests> testDatabus (new tsk::DatabusForKbsTests) ;
 	boost::shared_ptr<tsk::MacroInputsTest> macroModel (new tsk::MacroInputsTest);
	boost::shared_ptr<tsk::ShearModulusTest> shearModel1 (new tsk::ShearModulusTest);
	boost::shared_ptr<tsk::ShearModulusTest> shearModel2 (new tsk::ShearModulusTest);


	ada::BindModelToBus(testDatabus,macroModel);

	tsk::ExternalTestingKbs externalKbs;

	boost::shared_ptr<kno::Kbs<tsk::DatabusForKbsTests, tsk::ExternalTestingKbs> > kbs (new kno::Kbs<tsk::DatabusForKbsTests, tsk::ExternalTestingKbs>(testDatabus,externalKbs));
	boost::shared_ptr<ada::Switcher<kno::Kbs<tsk::DatabusForKbsTests, tsk::ExternalTestingKbs>, tsk::ShearModulusContract2D> > shearModulusSwitcher(new ada::Switcher<kno::Kbs<tsk::DatabusForKbsTests, tsk::ExternalTestingKbs>, tsk::ShearModulusContract2D>(kbs));

 	shearModulusSwitcher->AddModel(shearModel1);
	shearModel2->shearModulus_(0.25);
	shearModulusSwitcher->AddModel(shearModel2);
	shearModel2->shearModulus_(0.5);

	ada::BindModelToBus(testDatabus,shearModulusSwitcher);

	mpr::Coordinates2D coord(0.5,0.5);
	mpr::Time t(0.0);
	mpt::ComputedPoint<mpr::Coordinates2D> point(coord,t);
	mpr::ShearModulus shearToCheck;
	boost::shared_ptr<moc::Provider<mpr::ShearModulus,mpr::Coordinates2D> > shearProvider;
	shearProvider = testDatabus;
	shearToCheck=shearProvider->GetState(&point,&shearToCheck);
	EXPECT_DOUBLE_EQ(0.25,shearToCheck.Value());

	macroModel->temperature_(500.0);
	shearToCheck=shearProvider->GetState(&point,&shearToCheck);
	EXPECT_DOUBLE_EQ(0.5,shearToCheck.Value());
}