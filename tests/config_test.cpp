/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/

/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../common/config.h"
#include "config_test.h"

/*	using ***********************************************************/



/*	extern **********************************************************/

extern int*  global_argc;
extern char ***global_argv;
namespace am3
{
  namespace common
  {
    extern Configuration kProgramOptions;



		po::options_description& ConfigurableModuleMock::DefineOptionsForModule()
		{
			po::options_description& od=kProgramOptions.GetNewProgramOptions("Test configuration");
// 			od.add_options()
// 				; 
			kProgramOptions.AddToAllNewProgramOptions(od);
			return od;
		}

		void ConfigurableModuleMock::CopyReadOptionsToInternal()
		{
// 			deformUserProcedureConf_.userValuesMax1=((*module_variable_map)["DeformUsrVar1"].as<int>());
// 			deformUserProcedureConf_.userValuesMax2=((*module_variable_map)["DeformUsrVar2"].as<int>());
// 			deformUserProcedureConf_.userValuesNodalMax=((*module_variable_map)["DeformUsrVarNod"].as<int>());
// 			deformUserProcedureConf_.runPreprocessor=((*module_variable_map)["RunPreprocessor"].as<bool>());
// 			deformUserProcedureConf_.preprocessorName=((*module_variable_map)["PreprocessorName"].as<string>());
// 			deformUserProcedureConf_.preprocessorPath=((*module_variable_map)["PreprocessorPath"].as<string>());
// 			deformUserProcedureConf_.preprocessorInputFile=((*module_variable_map)["PreprocessorInputFile"].as<string>());
// 			deformUserProcedureConf_.preprocessorWorkDir=((*module_variable_map)["PreprocessorWorkDir"].as<string>());
// 			deformUserProcedureConf_.runEnvironment=((*module_variable_map)["RunEnvironment"].as<bool>());
// 			deformUserProcedureConf_.environmentScript=((*module_variable_map)["EnvironmentScript"].as<string>());
// 			deformUserProcedureConf_.environmentPath=((*module_variable_map)["EnvironmentPath"].as<string>());
// 			deformUserProcedureConf_.environmentInputFile=((*module_variable_map)["EnvironmentInputFile"].as<string>());
// 			deformUserProcedureConf_.environmentWorkDir=((*module_variable_map)["EnvironmentWorkDir"].as<string>());
// 			deformUserProcedureConf_.runSimulation=((*module_variable_map)["RunSimulation"].as<bool>());
// 			deformUserProcedureConf_.simulationName=((*module_variable_map)["SimulationName"].as<string>());
// 			deformUserProcedureConf_.simulationPath=((*module_variable_map)["SimulationPath"].as<string>());
// 			deformUserProcedureConf_.simulationInputFile=((*module_variable_map)["SimulationInputFile"].as<string>());
// 			deformUserProcedureConf_.simulationWorkDir=((*module_variable_map)["SimulationWorkDir"].as<string>());
		}

		std::string ConfigurableModuleMock::GetModuleName()
		{
			return ("test");
		}

	}
}

/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterConfig(){};}}

void RegisterConfigTests(){};

namespace am3
{
  namespace common
  {
	  
//////////////////////////////////////////////////////////////////////////
// Tests for ConfigurableModule class
//////////////////////////////////////////////////////////////////////////
//     TEST(ConfigurableModuleTest,Check_conf_file)
//     {
//       ConfigurableModuleMock tc;
//        EXPECT_TRUE(kProgramOptions.IsInitialized());
//        EXPECT_TRUE(tc.TestTrue());  
//        EXPECT_FALSE(tc.TestFalse());
//     }

//     TEST(ConfigurableModuleTest,Read_module_config_file)
//     {
//       ConfigurableModuleMock tc;
//       EXPECT_TRUE(kProgramOptions.IsInitialized());  
//       po::variables_map& vm=tc.ReadModuleConfigFileTest();
//       if (!(vm.count("Test") && (vm["Test"].as< string>()=="test"))) 
//       {
//         FAIL();
//       }
//     }


//////////////////////////////////////////////////////////////////////////
// Tests for Configuration class
// If any function is not directly tested with FunctionNameTest, it is tested indirectly with other tests
//////////////////////////////////////////////////////////////////////////
    TEST(ConfTest,AllOptionsTest)
    {
      EXPECT_TRUE(kProgramOptions.IsInitialized());  
      po::options_description& od=kProgramOptions.AllOptions();
      bool isOK=false;
      isOK=(od.find("Module",false).key("Module")=="Module") ? true: false;
      EXPECT_TRUE(isOK); 
    }

    TEST(ConfTest,GetNewProgramOptionsTest)
    {
      EXPECT_TRUE(kProgramOptions.IsInitialized());  
      po::options_description& od=kProgramOptions.GetNewProgramOptions("Just for tests");
      od.add(kProgramOptions.AllOptions());
      bool isOK=false;
      isOK=(od.find("Module",false).key("Module")=="Module") ? true: false;
      EXPECT_TRUE(isOK); 
    }

    TEST(ConfTest,RegisterNewVariableMapTest)
    {
      EXPECT_TRUE(kProgramOptions.IsInitialized());  
      po::variables_map& vm=kProgramOptions.RegisterNewVariableMap();
      po::options_description& od=kProgramOptions.AllOptions();
      po::store(po::command_line_parser(*global_argc, *global_argv).options(od).allow_unregistered().run(), vm);
      if (!vm.count("mainCfg")) FAIL();
      kProgramOptions.RemoveLastVariableMap();
    }

    TEST(ConfTest,StoreNewProgramOptionsTest)
    {
      string confFileName=kProgramOptions.GetMainVariableMap()["mainCfg"].as<string>();
      po::variables_map vm;
      po::options_description& od=kProgramOptions.AllOptions();
      EXPECT_EQ(kProgramOptions.StoreProgramOptions(confFileName, od,vm),1);
      bool isOK=false;
      isOK=(od.find("Module",false).key("Module")=="Module") ? true: false;
      EXPECT_TRUE(isOK); 
    }
    TEST(ConfTest,StoreCommandLineOptionsTest)
    {
      EXPECT_TRUE(kProgramOptions.IsInitialized());  
      po::variables_map& vm=kProgramOptions.RegisterNewVariableMap();
      po::options_description& od=kProgramOptions.GetNewProgramOptions("Tests");
      od.add_options()("test,a","For test");
/*	  od.add_options()("help,v","For test");*/
      kProgramOptions.StoreCommandLineOptions(*global_argc,*global_argv);
      std::cout<<"\nIf fail, probably -a option is not added to command line\n";
//	  if (!vm.count("help")) FAIL();
      if (!vm.count("test")) FAIL();
      kProgramOptions.RemoveLastVariableMap();
    }

    TEST(ConfTest,StoreCommandLineOptionsParamsTest)
    {
      EXPECT_TRUE(kProgramOptions.IsInitialized());  
      po::variables_map& vm=kProgramOptions.RegisterNewVariableMap();
      po::variables_map& vm1=kProgramOptions.RegisterNewVariableMap();
      po::options_description& od=kProgramOptions.GetNewProgramOptions("Tests");
      od.add_options()("test,a","For test");
      kProgramOptions.StoreCommandLineOptions(*global_argc,*global_argv,od,vm);
      if (!vm.count("test"))
        {
          FAIL();
          std::cout<<"\nTry to add '-a' to tested program's command line. Probably Cmake failed with this\n\n";
        }
      if (vm1.count("test")) FAIL();
      kProgramOptions.RemoveLastVariableMap();
      kProgramOptions.RemoveLastVariableMap();
    }
    
    TEST(ConfTest,GetMainVariableMapTest)
    {
      EXPECT_TRUE(kProgramOptions.IsInitialized());  
      string confFileName=kProgramOptions.GetMainVariableMap()["mainCfg"].as<string>();
      if (confFileName=="") FAIL();
    }

//     Private member
//     TEST(ConfTest,GetConfiguredMainOptionsTest)
//     {
// 
//     }
//     Private member
//         TEST(ConfTest,GetConfiguredCommandLineTest)
//     {
// 
//     }
    TEST(ConfTest,ReadMainConfigFileTest)
    {
      SUCCEED();
      std::cout<<"if ReadMainConfigFile() fails, all other tests will fail\n";
    }
  }
}