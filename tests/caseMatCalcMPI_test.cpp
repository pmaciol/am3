
/*! \file caseMatCalcMPI_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include <boost/shared_ptr.hpp>
#include "../softwareDeform/oneStepOneMesh.h"
#include "../caseMatCalcMPI/caseMatCalcMPI.h"
#include "../caseEsaform2015/caseEsaform2015.h"

/*	using ***********************************************************/

//namespace cmm = am3::cases::matcalcMPi;
namespace sdm = am3::software::deform;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 { namespace tests { void RegisterCaseMatCalcMPI(){}; } }
