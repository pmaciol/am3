
/*! \file caseColdRolling_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseColdRolling_test_h__
#define caseColdRolling_test_h__
/*	include *********************************************************/
/*	include *********************************************************/

#include "../models/models.h"
#include "../models/dataStructures.h"
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "../caseColdRolling/databus.h"
#include "../caseColdRolling/caseColdRolling.h"
#include "../adapter/ivstorage.h"
#include "../modelPolycrystalMicrostructure/gottsteinContract.h"

#include "caseColdRolling_TestModel.h"

/*	using ***********************************************************/

namespace mo = am3::model;
namespace mpr = am3::model::properties;
namespace ppm = am3::problem::polycrystal;
namespace sdm = am3::software::deform;
namespace ccr = am3::cases::coldRolling;
namespace mpm = am3::model::polycrystal;



/*	extern **********************************************************/



/*	classes *********************************************************/



namespace tests
{
	namespace caseColdRolling
	{
		struct Consts
		{
			static const double yieldStressCheck;
			static const double yieldStressDStrainCheck;
			static const double yieldStressDStrainRateCheck;
			static const int nodesNumber;
			static const double coordinates[8],coordinates2[8];
			static const double time,time2;
			static const int elementId;
			static const int meshId;
			static const mpr::Coordinates2D coord;
			static const mpr::Time t;
			static pnt::ComputedPoint<mpr::Coordinates2D> point;
		};



		class DeformHandlerToColdRollingAdapter : public ppm::contract::MacroInputsContract2D
		{
		public:
			void SetDeformHandler(sdm::DeformHandler<mpr::Coordinates2D>* pointerToDeform)
			{
				pointerToDeform_ = pointerToDeform;
			};
			mpr::EffectiveStrainRate GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::EffectiveStrainRate *const )
			{
				return pointerToDeform_->GetEffectiveStrainRateIn(*point);
			}
			mpr::Temperature GetState(am3::model::common::Point<mpr::Coordinates2D> * point,const mpr::Temperature *const )
			{
				return pointerToDeform_->GetTemperatureIn(*point);
			}
		protected:
			sdm::DeformHandler<mpr::Coordinates2D>* pointerToDeform_;
		};

    class ConstantYieldStress
      : public sdm::DeformStressContract2D
      , public ppm::contract::YieldStressContract2D
    {
    public:
      ConstantYieldStress(const double yieldStress,const double yieldStressDstrain,const double yieldStressDstrainRate){ys(yieldStress);sdstr(yieldStressDstrain);sdstrRate(yieldStressDstrainRate);};
      virtual mpr::YieldStress GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::YieldStress* const)
      {
        return ys;
      };
      virtual mpr::StressDStrain GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrain* const)
      {
        return sdstr;
      };
      virtual mpr::StressDStrainRate GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrainRate* const)
      {
        return sdstrRate;
      };
    protected:
      mpr::YieldStress ys;
      mpr::StressDStrain sdstr;
      mpr::StressDStrainRate sdstrRate;
    };

		//////////////////////////////////////////////////////////////////////////


		class GrainsTest 
			: public ppm::contract::GrainsContract2D
		{

		public:
			typedef ppm::contract::GrainsContract2D Contract;
			virtual ppm::contract::GrainSize GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::GrainSize *const ) {ppm::contract::GrainSize toRet;toRet(50e-6);return toRet;};  //Sherstnev
			virtual ppm::contract::RecrystalizedGrainSize GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::RecrystalizedGrainSize *const ) {ppm::contract::RecrystalizedGrainSize toRet;toRet(25e-6);return toRet;};
			virtual ppm::contract::NumberOfRecrystalizedNuclei GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::NumberOfRecrystalizedNuclei *const ) {ppm::contract::NumberOfRecrystalizedNuclei toRet;toRet(2e15);return toRet;};
			virtual ppm::contract::VolumeFractionOfRecrystallizedGrains GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::VolumeFractionOfRecrystallizedGrains *const ) {ppm::contract::VolumeFractionOfRecrystallizedGrains toRet;toRet(0.5);return toRet;};  //Sherstnev
			virtual ppm::contract::StoredEnergy GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::StoredEnergy *const ) {ppm::contract::StoredEnergy toRet;toRet(20e3);return toRet;};  //Sherstnev
		};

		class DislocationTest
			: public ppm::contract::DislocationsContract2D
		{
		public:
			virtual ppm::contract::DislDensityImmobileInteriors GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityImmobileInteriors *const ) {ppm::contract::DislDensityImmobileInteriors toRet;toRet(1400e10);return toRet;};  //Sherstnev
			virtual ppm::contract::DislDensityImmobileWalls GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityImmobileWalls *const ) {ppm::contract::DislDensityImmobileWalls toRet;toRet(1500e10);return toRet;};  //Sherstnev
			virtual ppm::contract::DislDensityMobile GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityMobile *const ) {ppm::contract::DislDensityMobile toRet;toRet(1500e10);return toRet;};
			virtual ppm::contract::DislDensityImmobileInteriorsDeformed GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityImmobileInteriorsDeformed *const ) {ppm::contract::DislDensityImmobileInteriorsDeformed toRet;toRet(150e10);return toRet;};
			virtual ppm::contract::DislDensityImmobileWallsDeformed GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityImmobileWallsDeformed *const ) {ppm::contract::DislDensityImmobileWallsDeformed toRet;toRet(150e10);return toRet;};
			virtual ppm::contract::DislDensityMobileDeformed GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::DislDensityMobileDeformed *const ) {ppm::contract::DislDensityMobileDeformed toRet;toRet(150e10);return toRet;};
			virtual ppm::contract::SubgrainSize GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::SubgrainSize *const ) {ppm::contract::SubgrainSize toRet;toRet(5e-6);return toRet;};  //Sherstnev
		};

		class DiffusionTest
			: public ppm::contract::DiffusionContract2D
		{
		public:
			virtual ppm::contract::SelfDiffusion GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::SelfDiffusion *const ) {ppm::contract::SelfDiffusion toRet;toRet(6.5e-13);return toRet;};
			virtual ppm::contract::GrainBoundaryDiffusion GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::GrainBoundaryDiffusion *const ) {ppm::contract::GrainBoundaryDiffusion toRet;toRet(9.2e-11);return toRet;};
		};

		class MacroInputsTest
			: public ppm::contract::MacroInputsContract2D
		{
		public:
			mpr::Temperature temperature_;
			mpr::EffectiveStrainRate efepse_;
			MacroInputsTest(){temperature_(773.15);efepse_(1.0);}  //Sherstnev
			virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Temperature *const ) {return temperature_;};
			virtual mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::EffectiveStrainRate *const ) {return efepse_;};  //Sherstnev
			virtual mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StepLength *const ) {mpr::StepLength toRet;toRet(0.001);return toRet;};
		};

    template<typename INPUT>
    class MacroInputsTestTempl
      : public ppm::contract::MacroInputsContract2D
//      , public mo::InputsGen<mpm::NoInput,INPUT>
    {
    public:
      typedef mpm::NoInput Inputs;
      typedef MacroInputsTestTempl Template;
      typedef ppm::contract::MacroInputsContract2D Contract;
      mpr::Temperature temperature_;
      mpr::EffectiveStrainRate efepse_;
      MacroInputsTestTempl(){temperature_(773.15);efepse_(1.0);}  //Sherstnev
      virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::Temperature *const ) {return temperature_;};
      virtual mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::EffectiveStrainRate *const ) {return efepse_;};  //Sherstnev
      virtual mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::StepLength *const ) {mpr::StepLength toRet;toRet(0.001);return toRet;};
    };

		class YieldStressTest
			: public ppm::contract::YieldStressContract2D
		{
		public:
			virtual mpr::YieldStress GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::YieldStress *const ) {mpr::YieldStress toRet;toRet(20e6);return toRet;};
		};

		class ShearModulusTest
			: public ppm::contract::ShearModulusContract2D
		{
		public:
			ShearModulusTest(){shearModulus_(20.1e9);}
			mpr::ShearModulus shearModulus_;
			virtual mpr::ShearModulus GetState(mpt::Point<mpr::Coordinates2D> *,const mpr::ShearModulus *const ) {return shearModulus_;};
		};

		class ZenerParamTest
			: public ppm::contract::ZenerContract2D
		{
		public:
			virtual ppm::contract::ZenerParam GetState(mpt::Point<mpr::Coordinates2D> *,const ppm::contract::ZenerParam *const ) {ppm::contract::ZenerParam toRet;toRet(14600.0);return toRet;};  //Sherstnev
		};

		typedef moc::Contract<TYPELIST_1( mpr::Temperature ),mpr::Coordinates2D> ShearModulusTemperatureDependentTestInput;

		template<typename INPUT>
		class ShearModulusTemperatureDependentTest
			: public ppm::contract::ShearModulusContract2D
			, public mo::InputTypelist<INPUT>//<ShearModulusTemperatureDependentTestInput>
		{
		public:
			virtual mpr::ShearModulus GetState(mpt::Point<mpr::Coordinates2D> *point,const mpr::ShearModulus *const )
			{
				mpr::ShearModulus toRet;
				mpr::Temperature temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point);

				toRet(0.1 * temperature.Value());
				return toRet;
			};
		};




		class Kbs
		{
		public:
			template <typename VARIABLE> mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point)
			{
				const VARIABLE* trait=nullptr;
				return GetModelId(point,trait);
			}
			Kbs(boost::shared_ptr<ccr::DatabusForGottsteinDeform> databus): databus_(databus){};
		protected:
			boost::shared_ptr<ccr::DatabusForGottsteinDeform> databus_;
			mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::RecrystalizedGrainSize* const)
			{
				boost::shared_ptr<moc::Provider<ppm::contract::ZenerParam,mpr::Coordinates2D> > zenerDragProvider;
				boost::shared_ptr<moc::Provider<mpr::EffectiveStrainRate,mpr::Coordinates2D> > efepseProvider;
				efepseProvider = databus_;
				zenerDragProvider = databus_;
				mpr::EffectiveStrainRate efepse = efepseProvider->GetState(point,&efepse);

				ppm::contract::StoredEnergy storedEnergy = databus_->PreviousData()->GetPreviousState<ppm::contract::StoredEnergy>();
				ppm::contract::ZenerParam zenerDrag = zenerDragProvider->GetState(point,&zenerDrag);
				mpr::ModelID mId;

				if (efepse() > 0.001)
				{
					mId(2);
				} 
				else
				{
					if (storedEnergy() < zenerDrag())
					{
						mId(0);
					} 
					else
					{
						mId(1);
					}
				}
				return mId;
			}
			mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::GrainSize* const)
			{
				mpr::ModelID mId;
				mId(0);
				return mId;
			}
			mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::NumberOfRecrystalizedNuclei* const)
			{
				mpr::ModelID mId;
				mId(0);
				return mId;
			}
			mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::VolumeFractionOfRecrystallizedGrains* const)
			{
				mpr::ModelID mId;
				mId(0);
				return mId;
			}
			mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const ppm::contract::StoredEnergy* const)
			{
				mpr::ModelID mId;
				mId(0);
				return mId;
			}
		};

    //////////////////////////////////////////////////////////////////////////

    class TestVirtualDatabus
      : public ppm::contract::DiffusionContract2D
      , public ppm::contract::ShearModulusContract2D
      , public ppm::contract::MacroInputsContract2D
//      , public moc::Provider<mpr::ShearModulus,mpr::Coordinates2D>
    {
    public:
      mpr::Temperature GetState(am3::model::common::Point<mpr::Coordinates2D>* point,const mpr::Temperature* const typeTrait)
      {
        moc::Provider<mpr::Temperature,mpr::Coordinates2D>* ptr;
        ptr = macrPtr_;
        return ptr->GetState(point,typeTrait);
      };
      mpr::StepLength GetState(am3::model::common::Point<mpr::Coordinates2D>* point,const mpr::StepLength* const typeTrait)     
      {
        moc::Provider<mpr::StepLength,mpr::Coordinates2D>* ptr;
        ptr = macrPtr_;
        return ptr->GetState(point,typeTrait);
      };
      mpr::EffectiveStrainRate GetState(am3::model::common::Point<mpr::Coordinates2D>* point,const mpr::EffectiveStrainRate* const typeTrait)
      {
        moc::Provider<mpr::EffectiveStrainRate,mpr::Coordinates2D>* ptr;
        ptr = macrPtr_;
        return ptr->GetState(point,typeTrait);
      };
      mpr::ShearModulus GetState(am3::model::common::Point<mpr::Coordinates2D>* point,const mpr::ShearModulus* const typeTrait)
      {
        moc::Provider<mpr::ShearModulus,mpr::Coordinates2D>* ptr;
        ptr = smPtr_;
        return ptr->GetState(point,typeTrait);
      };
      am3::problem::polycrystal::contract::SelfDiffusion GetState(am3::model::common::Point<mpr::Coordinates2D>* point,const am3::problem::polycrystal::contract::SelfDiffusion* const typeTrait)
      {
        moc::Provider< am3::problem::polycrystal::contract::SelfDiffusion,mpr::Coordinates2D>* ptr;
        ptr = diffPtr_;
        return ptr->GetState(point,typeTrait);
      };
      am3::problem::polycrystal::contract::GrainBoundaryDiffusion GetState(am3::model::common::Point<mpr::Coordinates2D>* point,const am3::problem::polycrystal::contract::GrainBoundaryDiffusion* const typeTrait)
      {
        moc::Provider< am3::problem::polycrystal::contract::GrainBoundaryDiffusion,mpr::Coordinates2D>* ptr;
        ptr = diffPtr_;
        return ptr->GetState(point,typeTrait);
      };
      ppm::contract::DiffusionContract2D* DiffPtr() const { return diffPtr_; }
      void DiffPtr(ppm::contract::DiffusionContract2D* val) { diffPtr_ = val; }
      ppm::contract::ShearModulusContract2D* SmPtr() const { return smPtr_; }
      void SmPtr(ppm::contract::ShearModulusContract2D* val) { smPtr_ = val; }
      ppm::contract::MacroInputsContract2D* MacrPtr() const { return macrPtr_; }
      void MacrPtr(ppm::contract::MacroInputsContract2D* val) { macrPtr_ = val; }
    protected:
      ppm::contract::DiffusionContract2D* diffPtr_;
      
      ppm::contract::ShearModulusContract2D* smPtr_;
      
      ppm::contract::MacroInputsContract2D* macrPtr_;
      
    };


	} //caseColdRolling
	
}



#endif // caseColdRolling_test_h__
