
/*! \file fineModelsContainer_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "../models/fineModelsContainer.h"
#include "fineModelsContainer_test.h"
#include "./mockMacroModels.h"
#include "../models/description.h"

#include "../utilMesh/mesh.h"
#include "../utilMesh/bintreeMeshSearcher.h"
#include "../utilMesh/meshApprox.h"
#include "../models/namedPoint.h"

#include "../softwareDeformBuiltin/deformHandlerAprox.h"
#include "caseColdRolling_test.h"

//#include "./modelPolycrystalMicrostructure.h"

/*	using ***********************************************************/

namespace mo = am3::model;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;

namespace sdm = am3::software::deform;
namespace tsc = tests::caseColdRolling;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterFineModelsCointainer (){};}}

TEST(FineModelsContainer, TranslationOfModels)
{
  boost::shared_ptr<tests::model::MovementCoordinatesMock<mpr::Coordinates2D> > movementsSource (new tests::model::MovementCoordinatesMock<mpr::Coordinates2D>);
  mo::FineModelSelector<tests::YieldStressMock> selector(movementsSource);
  boost::shared_ptr<tests::YieldStressMock> fmodel1(new tests::YieldStressMock(1e3));
  boost::shared_ptr<tests::YieldStressMock> fmodel2(new tests::YieldStressMock(1e4));

  const mpr::Coordinates2D coord1(0.1,0.2);
  const mpr::Coordinates2D coord2(0.2,0.2);
  mpr::Time time1(0.1);
  mpr::Time time2(0.2);
  boost::shared_ptr<mpt::ComputedPoint<mpr::Coordinates2D> >point1(new mpt::ComputedPoint<mpr::Coordinates2D> (coord1,time1));
  boost::shared_ptr<mpt::ComputedPoint<mpr::Coordinates2D> >point2(new mpt::ComputedPoint<mpr::Coordinates2D> (coord2,time2));

  selector.AddModel(point1,fmodel1);
  selector.AddModel(point2,fmodel2);

  selector.MoveEachModel();
  const mpr::Coordinates2D coord3 = selector.GetClosest(*point1).first->Coordinates();
  ASSERT_DOUBLE_EQ(0.2,coord3.x());
  ASSERT_DOUBLE_EQ(0.4,coord3.y());
}


TEST(FineModelsContainer, AddingNamedPointToApprox)
{
	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
	const mpr::Coordinates2D coord(0.1,0.2);
	mpr::Time time(0.1);
	const unsigned long int pointId = 123;
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point(pointId,coord,time);

	mtm::Mesh<2> testMesh(101);
	int id = testMesh.AddVertex(mtm::Point<2>(point),123);
	ASSERT_TRUE(testMesh.VertexExists(123));
}



TEST(FineModelsContainer, AddingDisplacementsToApprox)
{


  tests::ExternalFemMock mockFEM;
  mtm::Mesh<2> testMesh(1001);

  const mpr::Coordinates2D coord1(0,1);
  const mpr::Coordinates2D coord2(1,2);
  const mpr::Coordinates2D coord3(2,1);
  const mpr::Coordinates2D coord4(1,0);


  mpr::Time time1(0.1);
  mpr::Time time2(0.2);
  boost::shared_ptr<mpt::ComputedPoint<mpr::Coordinates2D> >point1(new mpt::ComputedPoint<mpr::Coordinates2D> (coord1,time1));
  boost::shared_ptr<mpt::ComputedPoint<mpr::Coordinates2D> >point2(new mpt::ComputedPoint<mpr::Coordinates2D> (coord2,time1));
  boost::shared_ptr<mpt::ComputedPoint<mpr::Coordinates2D> >point3(new mpt::ComputedPoint<mpr::Coordinates2D> (coord3,time1));
  boost::shared_ptr<mpt::ComputedPoint<mpr::Coordinates2D> >point4(new mpt::ComputedPoint<mpr::Coordinates2D> (coord4,time1));


  testMesh.AddVertex(mtm::Point<2>(*point1),1);
  testMesh.AddVertex(mtm::Point<2>(*point2),2);
  testMesh.AddVertex(mtm::Point<2>(*point3),3);
  testMesh.AddVertex(mtm::Point<2>(*point4),4);
  testMesh.AddTriangle(1, 2, 4, 101);
  testMesh.AddTriangle(2, 3, 4, 102);


  // Mesh Organizing ========================================
  delete testMesh.get_searcher();  //delete old searcher
  testMesh.set_searcher(new mtm::BintreeMeshSearcher<2>(&testMesh)); //create new searcher
  testMesh.Organize();


  mpr::ElementNumber e1;
  mtm::Point<2> p1(0.5,1);
  mtm::Point<2> p2(1.5, 1);
  mtm::Point<2> p3(0.5, 1.49);
  mtm::Point<2> p4(1.5, 0.01);
  testMesh.FindElementContaining(p1, e1);
  EXPECT_EQ(101,e1());
  testMesh.FindElementContaining(p2, e1);
  EXPECT_EQ(102,e1());

  tests::model::DataProvider provider;
  mtm::BarycentricApprox<tests::model::Displacement2DStore, 2> approx(&provider);
  tests::model::Displacement2DStore result1;
  tests::model::Displacement2DStore result2;

  approx.Approximate(testMesh, p1, result1);
  double approximated1 = (Field<0>(result1)).x();
  approx.Approximate(testMesh, p2, result2);
  double approximated2 = (Field<0>(result2)).x();
  approx.Approximate(testMesh, p3, result1);
  double approximated3 = (Field<0>(result1)).x();
  approx.Approximate(testMesh, p4, result2);
  double approximated4 = (Field<0>(result2)).x();
  EXPECT_DOUBLE_EQ(1,approximated1);
  EXPECT_DOUBLE_EQ(1.5,approximated2);
  EXPECT_DOUBLE_EQ(1,approximated3);
  EXPECT_DOUBLE_EQ(1.5,approximated4);
}

TEST(FineModelsContainer, AlgorithmTest)
{
  boost::shared_ptr<tests::model::MovementCoordinatesMock<mpr::Coordinates2D> > movementsSource (new tests::model::MovementCoordinatesMock<mpr::Coordinates2D>);
  boost::shared_ptr<mo::FineModelSelector<tests::YieldStressMock> > modelSelector(new mo::FineModelSelector<tests::YieldStressMock>(movementsSource));
  boost::shared_ptr<mo::ModelMakerDump<tests::YieldStressMock> > modelMaker(new mo::ModelMakerDump<tests::YieldStressMock>);
  mo::OnlyExactAlgorithm<tests::YieldStressMock,mo::FineModelSelector,mo::ModelMakerDump> modelsProvider(modelMaker,modelSelector);

  const mpr::Coordinates2D coord1(0.1,0.2);
  mpr::Time time1(0.1);
  boost::shared_ptr<mpt::ComputedPoint<mpr::Coordinates2D> >point1(new mpt::ComputedPoint<mpr::Coordinates2D> (coord1,time1));

  boost::shared_ptr<am3::problem::polycrystal::contract::YieldStressContract2D> acquiredModel;
  acquiredModel = modelsProvider.GetModel(point1);
  mpr::YieldStress result;
  result(acquiredModel->GetState(point1.get(),&result));
  EXPECT_DOUBLE_EQ(1e5,result());
}



TEST(FineModelsContainer, DeformImportsMesh)
{
	boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tsc::ConstantYieldStress(0.1,0.2,0.3));
	boost::shared_ptr<mtm::Mesh<2> > mesh(new mtm::Mesh<2>(101));
	boost::shared_ptr<am3::software::deform::DeformHandlerApprox<mpr::Coordinates2D>> deformHandlerPtr
		(new am3::software::deform::DeformHandlerApprox<mpr::Coordinates2D>(ySmodelPtr,mesh));

   const int nodesNumber=6;
   const int elementsNumber=2;
   const int connectivityMatrix[10]={0,0,1,2,3,1,1,4,5,2};
   const double coordinates[12]={0,0,1,0,1,1,0,1,2,0,2,1};
   const double displacements[12]={0,0,0,0,0,0,0,0,0,0,0,0};
   const double time1 = 0;
   const double timeStep = 0.01;
   const int meshId = 0;

  ASSERT_TRUE(deformHandlerPtr->SetMesh(coordinates,displacements,time1,timeStep,elementsNumber,nodesNumber,connectivityMatrix));

	mtm::Point<2> p1(0.5, 0.5);
 	mtm::Point<2> p2(1.5, 0.5);

  mpr::ElementNumber e1;
	bool b1 = mesh->FindElementContaining(p1,e1);
  EXPECT_TRUE(b1);
  EXPECT_TRUE(e1() == 0);
  b1 = mesh->FindElementContaining(p2,e1);
  EXPECT_TRUE(b1);
  EXPECT_TRUE(e1() == 1);
}

TEST(FineModelsContainer, DeformImportsApproximatedValues)
{
  FAIL() << "\nNot Implemented yet!\n";
}
//////////////////////////////////////////////////////////////////////////
// IMPLEMENTATION
//////////////////////////////////////////////////////////////////////////

