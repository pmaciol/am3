//mesh_test.cpp
//

//? #include <iostream>
using namespace std;

#include "../utilMesh/mesh.h"
#include "../utilMesh/bintreeMeshSearcher.h"
#include "../utilMesh/meshApprox.h"
#include "../utilMesh/barycentricApprox.h"
#include "../utilMesh/sphericalBarycentricApprox.h"
#include "../utilMesh/meshList.h"
#include "../storage/cons.h"

namespace am3 { namespace tests { void RegisterMesh(){}; } }

#include "gtest/gtest.h"
#include "../models/namedPoint.h"

namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;
using namespace am3::math::mesh;


TEST(MeshTest, MeshSearching3D)
{
    // Mesh Creation ==========================================
    Mesh<3> mesh(100);

    mesh.AddVertex(Point<3>(0, 0, 0), 500);
    mesh.AddVertex(Point<3>(1, 0, 0), 501);
    mesh.AddVertex(Point<3>(0, 0, 1), 502);
    mesh.AddVertex(Point<3>(0, 1, 0), 503);
    mesh.AddVertex(Point<3>(1, 1, 1), 504);

    mesh.AddTetrahedron(500, 501, 502, 503, 101);
    mesh.AddTetrahedron(501, 502, 503, 504, 102);

    // Mesh Organizing ========================================
    delete mesh.get_searcher();  //delete old searcher
    mesh.set_searcher(new BintreeMeshSearcher<3>(&mesh)); //create new searcher
    mesh.Organize();

    // Mesh Searching =========================================
    Point<3> p1(0.25, 0.49999, 0.25);
    Point<3> p2(0.25, 0.50001, 0.25);
    Point<3> p3(1.0, 1.0, 1.0001);

    mpr::ElementNumber e1;
    bool b1 = mesh.FindElementContaining(p1, e1); //should be 0 (==elem1)
    EXPECT_TRUE(b1);
    EXPECT_TRUE(e1() == 101);

    mpr::ElementNumber e2;
    bool b2 = mesh.FindElementContaining(p2, e2); //should be 1 (==elem2)
    EXPECT_TRUE(b2);
    EXPECT_TRUE(e2() == 102);

    mpr::ElementNumber e3;
    bool b3 = mesh.FindElementContaining(p3, e3); //should be -1
    EXPECT_FALSE(b3);
}

//======================================================================================

TEST(MeshTest, MeshSearching2D)
{
    // Mesh Creation ==========================================
    Mesh<2> mesh(100);

    mesh.AddVertex(Point<2>(0, 1), 500);
    mesh.AddVertex(Point<2>(1, 2), 501);
    mesh.AddVertex(Point<2>(2, 1), 502);
    mesh.AddVertex(Point<2>(1, 0), 503);

    mesh.AddTriangle(500, 501, 503, 101);
    mesh.AddTriangle(501, 502, 503, 102);

    // Mesh Organizing ========================================
    delete mesh.get_searcher();  //delete old searcher
    mesh.set_searcher(new BintreeMeshSearcher<2>(&mesh)); //create new searcher
    mesh.Organize();

    // Mesh Searching =========================================
    Point<2> p1(1.5, 1.49999);
    Point<2> p2(1.5, 1.50001);
    Point<2> p3(0.5, 1.0);

    mpr::ElementNumber e1;
    bool b1 = mesh.FindElementContaining(p1, e1);
    EXPECT_TRUE(b1);
    EXPECT_TRUE(e1() == 102);

    mpr::ElementNumber e2;
    bool b2 = mesh.FindElementContaining(p2, e2);
    EXPECT_FALSE(b2);

    mpr::ElementNumber e3;
    bool b3 = mesh.FindElementContaining(p3, e3);
    EXPECT_TRUE(b3);
    EXPECT_TRUE(e3() == 101);
}


TEST(MeshTest, MeshSearching2DNamedPoints)
{
	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
	const mpr::Coordinates2D coord1(0,1);
	const mpr::Coordinates2D coord2(1,2);
	const mpr::Coordinates2D coord3(2,1);
	const mpr::Coordinates2D coord4(1,0);
	mpr::Time time(0.0);
	
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point1(0,coord1,time);
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point2(1,coord2,time);
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point3(2,coord3,time);
	mpt::NamedPoint<mpt::NamedPointsContainerLastTwoSteps2D> point4(3,coord4,time);

	// Mesh Creation ==========================================
	Mesh<2> mesh(100);

	mesh.AddVertex(point1);
	mesh.AddVertex(point2);
	mesh.AddVertex(point3);
	mesh.AddVertex(point4);

	mesh.AddTriangle(point1.GetId(), point2.GetId(), point4.GetId(), 101);
	mesh.AddTriangle(point2.GetId(), point3.GetId(), point4.GetId(), 102);

	// Mesh Organizing ========================================
	delete mesh.get_searcher();  //delete old searcher
	mesh.set_searcher(new BintreeMeshSearcher<2>(&mesh)); //create new searcher
	mesh.Organize();

	// Mesh Searching =========================================
	Point<2> p11(1.5, 1.49999);
	Point<2> p12(1.5, 1.50001);
	Point<2> p13(0.5, 1.0);

    mpr::ElementNumber e1;
	bool b1 = mesh.FindElementContaining(p11, e1);
    EXPECT_TRUE(b1);
    EXPECT_TRUE(e1() == 102);

    mpr::ElementNumber e2;
	bool b2 = mesh.FindElementContaining(p12, e2);
    EXPECT_FALSE(b2);

    mpr::ElementNumber e3;
	bool b3 = mesh.FindElementContaining(p13, e3);
	EXPECT_TRUE(b3);
	EXPECT_TRUE(e3() == 101);
}


//============================================================================================

//
// Example of a simple "dummy" class that can be used in approximation.
//
class Dummy
{
public:
    Dummy()
    {
    }

    Dummy(float v)
    {
        val_ = v;
    }

    Dummy(const Dummy& d)
    {
        val_ = d.val_;
    }

    float get_val()
    {
        return val_;
    }

    Dummy& operator=(const Dummy& d)
    {
        val_ = d.val_;
        return *this;
    }

    Dummy operator+(const Dummy& d)
    {
        return Dummy(val_+d.val_);
    }

    Dummy operator*(double d)
    {
        return Dummy((float)(val_*d));
    }

private:
    float val_;
};

//
//Typelist that defines sample data struct for approximation.
//
typedef cons<
    double,
    float,
    Dummy
>::type MyTypes;

//
//Data struct for approximation based on Typelist above.
//
typedef MakeData<MyTypes> MyData;

//
//Simple NodeDataProvider for tests.
//Returns proper values for node_id in range [500..507].
//
class TestNodeDataProvider : public NodeDataProvider<MyData>
{
public:
    TestNodeDataProvider()
    {
        Field<0>(data_[0]) = 0;
        Field<0>(data_[1]) = 100;
        Field<0>(data_[2]) = 200;
        Field<0>(data_[3]) = 300;
        Field<0>(data_[4]) = 400;
        Field<0>(data_[5]) = 500;
        Field<0>(data_[6]) = 600;
        Field<0>(data_[7]) = 700;

        Field<1>(data_[0]) = 0.0f;
        Field<1>(data_[1]) = 0.100f;
        Field<1>(data_[2]) = 0.200f;
        Field<1>(data_[3]) = 0.300f;
        Field<1>(data_[4]) = 0.400f;
        Field<1>(data_[5]) = 0.500f;
        Field<1>(data_[6]) = 0.600f;
        Field<1>(data_[7]) = 0.700f;

        Field<2>(data_[0]) = Dummy(0.0f);
        Field<2>(data_[1]) = Dummy(0.100f);
        Field<2>(data_[2]) = Dummy(0.200f);
        Field<2>(data_[3]) = Dummy(0.300f);
        Field<2>(data_[4]) = Dummy(0.400f);
        Field<2>(data_[5]) = Dummy(0.500f);
        Field<2>(data_[6]) = Dummy(0.600f);
        Field<2>(data_[7]) = Dummy(0.700f);
    }

    virtual MyData* GetValue(int node_id)
    {
        return &data_[node_id-500];
    }

private:
    MyData data_[8];
};


TEST(MeshTest, BarycentricApproximation3D)
{
    // Mesh Creation ==========================================
    Mesh<3> mesh(100);

    mesh.AddVertex(Point<3>(0, 0, 0), 500);
    mesh.AddVertex(Point<3>(1, 0, 0), 501);
    mesh.AddVertex(Point<3>(0, 0, 1), 502);
    mesh.AddVertex(Point<3>(0, 1, 0), 503);
    mesh.AddVertex(Point<3>(1, 1, 1), 504);

    mesh.AddTetrahedron(500, 501, 502, 503, 101);
    mesh.AddTetrahedron(501, 502, 503, 504, 102);

    // Mesh Organizing ========================================
    delete mesh.get_searcher();  //delete old searcher
    mesh.set_searcher(new BintreeMeshSearcher<3>(&mesh)); //create new searcher
    mesh.Organize();

    // Approximation ===========================================
    TestNodeDataProvider provider;
    BarycentricApprox<MyData, 3> approx(&provider);
    MyData result;
    approx.Approximate(mesh, Point<3>(0.25, 0.49999, 0.25), result);
    double approximated1 = Field<0>(result);
    approx.Approximate(mesh, Point<3>(0.25, 0.50001, 0.25), result);
    double approximated2 = Field<1>(result);
    double approximated3 = Field<2>(result).get_val();
    EXPECT_NEAR(approximated1, 225, 0.05);
    EXPECT_NEAR(approximated2, 0.225, 0.00005);
    EXPECT_NEAR(approximated3, 0.225, 0.00005);
}

TEST(MeshTest, BarycentricApproximation2D)
{
    // Mesh Creation ==========================================
    Mesh<2> mesh(100);

    mesh.AddVertex(Point<2>(0, 1), 500);
    mesh.AddVertex(Point<2>(1, 2), 501);
    mesh.AddVertex(Point<2>(2, 1), 502);
    mesh.AddVertex(Point<2>(1, 0), 503);

    mesh.AddTriangle(500, 501, 503, 101);
    mesh.AddTriangle(501, 502, 503, 102);

    // Mesh Organizing ========================================
    delete mesh.get_searcher();  //delete old searcher
    mesh.set_searcher(new BintreeMeshSearcher<2>(&mesh)); //create new searcher
    mesh.Organize();

    // Approximation ===========================================
    TestNodeDataProvider provider;
    BarycentricApprox<MyData, 2> approx(&provider);
    MyData result;
    approx.Approximate(mesh, Point<2>(1.5, 1.49999), result);
    double approximated = Field<0>(result);
    EXPECT_NEAR(approximated, 150, 0.05);

    approx.Approximate(mesh, Point<2>(0.5, 1), result);
    double approximated1 = Field<1>(result);

    approx.Approximate(mesh, Point<2>(1.5, 1), result);
    double approximated2 = Field<1>(result);
}



TEST(MeshTest, SphericalBarycentricApproximation3D_Cube)
{
    // Mesh Creation ==========================================
    Mesh<3> mesh(100);

    mesh.AddElement(6, 101);  //cube from <0,0,0> to <1,1,1>

    int a = mesh.AddVertex(Point<3>(0, 0, 0), 500);
    int b = mesh.AddVertex(Point<3>(1, 0, 0), 501);
    int c = mesh.AddVertex(Point<3>(1, 1, 0), 502);
    int d = mesh.AddVertex(Point<3>(0, 1, 0), 503);
    int e = mesh.AddVertex(Point<3>(0, 0, 1), 504);
    int f = mesh.AddVertex(Point<3>(1, 0, 1), 505);
    int g = mesh.AddVertex(Point<3>(1, 1, 1), 506);
    int h = mesh.AddVertex(Point<3>(0, 1, 1), 507);

    mesh.SetElementFace(101, 0, 4);
    mesh.SetElementFaceVertex(101, 0, 0, 500);
    mesh.SetElementFaceVertex(101, 0, 1, 501);
    mesh.SetElementFaceVertex(101, 0, 2, 502);
    mesh.SetElementFaceVertex(101, 0, 3, 503);

    mesh.SetElementFace(101, 1, 4);
    mesh.SetElementFaceVertex(101, 1, 0, 504);
    mesh.SetElementFaceVertex(101, 1, 1, 505);
    mesh.SetElementFaceVertex(101, 1, 2, 501);
    mesh.SetElementFaceVertex(101, 1, 3, 500);

    mesh.SetElementFace(101, 2, 4);
    mesh.SetElementFaceVertex(101, 2, 0, 504);
    mesh.SetElementFaceVertex(101, 2, 1, 500);
    mesh.SetElementFaceVertex(101, 2, 2, 503);
    mesh.SetElementFaceVertex(101, 2, 3, 507);

    mesh.SetElementFace(101, 3, 4);
    mesh.SetElementFaceVertex(101, 3, 0, 503);
    mesh.SetElementFaceVertex(101, 3, 1, 502);
    mesh.SetElementFaceVertex(101, 3, 2, 506);
    mesh.SetElementFaceVertex(101, 3, 3, 507);

    mesh.SetElementFace(101, 4, 4);
    mesh.SetElementFaceVertex(101, 4, 0, 501);
    mesh.SetElementFaceVertex(101, 4, 1, 505);
    mesh.SetElementFaceVertex(101, 4, 2, 506);
    mesh.SetElementFaceVertex(101, 4, 3, 502);

    mesh.SetElementFace(101, 5, 4);
    mesh.SetElementFaceVertex(101, 5, 0, 505);
    mesh.SetElementFaceVertex(101, 5, 1, 504);
    mesh.SetElementFaceVertex(101, 5, 2, 507);
    mesh.SetElementFaceVertex(101, 5, 3, 506);

    // Mesh Organizing ========================================
    delete mesh.get_searcher();  //delete old searcher
    mesh.set_searcher(new BintreeMeshSearcher<3>(&mesh)); //create new searcher
    mesh.Organize();

    // Approximation ===========================================
    TestNodeDataProvider provider;
    SphericalBarycentricApprox<MyData, 3> approx(&provider);
    MyData result;
    approx.Approximate(mesh, Point<3>(0.5, 0.5, 0.5), result);
    double approximated1 = Field<0>(result);
    approx.Approximate(mesh, Point<3>(0.25, 0.50001, 0.25), result);
    double approximated2 = Field<0>(result);
    EXPECT_NEAR(approximated1, 350, 0.00005);
    EXPECT_NEAR(approximated2, 250, 0.05);
}



TEST(MeshTest, SphericalBarycentricApproximation3D_Tetrahedron)
{
    // Mesh Creation ==========================================
    Mesh<3> mesh(100);

    mesh.AddVertex(Point<3>(-0.5, -0.5, -1), 500);
    mesh.AddVertex(Point<3>(1, 0, 0), 501);
    mesh.AddVertex(Point<3>(0, 0, 1), 502);
    mesh.AddVertex(Point<3>(0, 1, 0), 503);

    mesh.AddTetrahedron(500, 501, 502, 503, 101);

    // Mesh Organizing ========================================
    delete mesh.get_searcher();  //delete old searcher
    mesh.set_searcher(new BintreeMeshSearcher<3>(&mesh)); //create new searcher
    mesh.Organize();

    // Approximation ===========================================
    TestNodeDataProvider provider;
    BarycentricApprox<MyData, 3> approx1(&provider);
    SphericalBarycentricApprox<MyData, 3> approx2(&provider);
    MyData result;

    //point inside
    approx1.Approximate(mesh, Point<3>(0.20, 0.15, 0.25), result);
    double approximated1 = Field<0>(result);
    approx2.Approximate(mesh, Point<3>(0.20, 0.15, 0.25), result);
    double approximated2 = Field<0>(result);
    EXPECT_NEAR(approximated1, approximated2, 0.00000005);

    //point on face
    approx1.Approximate(mesh, Point<3>(0.25, 0.25, 0.25), result);
    double approximated3 = Field<0>(result);
    approx2.Approximate(mesh, Point<3>(0.25, 0.25, 0.25), result);
    double approximated4 = Field<0>(result);
    EXPECT_NEAR(approximated3, approximated4, 0.00000005);

    //point on edge
    approx1.Approximate(mesh, Point<3>(0.6, 0.4, 0.0), result);
    double approximated5 = Field<0>(result);
    approx2.Approximate(mesh, Point<3>(0.6, 0.4, 0.0), result);
    double approximated6 = Field<0>(result);
    EXPECT_NEAR(approximated5, approximated6, 0.00000005);

    approx2.Approximate(mesh, Point<3>(0.59999999999999, 0.4, 0.0), result);
    double approximated7 = Field<0>(result);
    EXPECT_NEAR(approximated6, approximated7, 0.0005);
}


TEST(MeshTest, SphericalBarycentricApproximation2D_Quad)
{
    // Mesh Creation ==========================================
    Mesh<2> mesh(100);

    int a = mesh.AddVertex(Point<2>(0, 1), 500);
    int b = mesh.AddVertex(Point<2>(1, 2), 501);
    int c = mesh.AddVertex(Point<2>(2, 1), 502);
    int d = mesh.AddVertex(Point<2>(1, 0), 503);

    mesh.AddTetragon(500, 501, 502, 503, 101);

    // Mesh Organizing ========================================
    delete mesh.get_searcher();  //delete old searcher
    mesh.set_searcher(new BintreeMeshSearcher<2>(&mesh)); //create new searcher
    mesh.Organize();

    // Approximation ===========================================
    TestNodeDataProvider provider;
    SphericalBarycentricApprox<MyData, 2> approx(&provider);
    MyData result;
    approx.Approximate(mesh, Point<2>(1.5, 1.49999), result);
    double approximated = Field<0>(result);
    EXPECT_NEAR(approximated, 150, 0.05);
}



//
//Time & space approximation test
//
TEST(MeshTest, MeshList_Approximation)
{
    // Mesh Creation ==========================================
    Mesh<3> mesh1(100);
    Mesh<3> mesh2(101);

    mesh1.AddVertex(Point<3>(-0.5, -0.5, -1), 500);
    mesh1.AddVertex(Point<3>(1, 0, 0), 501);
    mesh1.AddVertex(Point<3>(0, 0, 1), 502);
    mesh1.AddVertex(Point<3>(0, 1, 0), 503);

    mesh2.AddVertex(Point<3>(-0.5, -0.5, -1), 500);
    mesh2.AddVertex(Point<3>(1, 0, 0), 501);
    mesh2.AddVertex(Point<3>(0, 0, 1), 502);
    mesh2.AddVertex(Point<3>(0, 1, 0), 503);
    mesh2.AddVertex(Point<3>(-0.5, -0.5, -1), 504);
    mesh2.AddVertex(Point<3>(1, 0, 0), 505);
    mesh2.AddVertex(Point<3>(0, 0, 1), 506);
    mesh2.AddVertex(Point<3>(0, 1, 0), 507);

    mesh1.AddTetrahedron(500, 501, 502, 503, 201);
    mesh2.AddTetrahedron(504, 505, 506, 507, 202);

    mesh1.set_time(100.0);
    mesh2.set_time(200.0);

    // Mesh Organizing ========================================
    mesh1.Organize();
    mesh2.Organize();

    // Approximation ===========================================
    TestNodeDataProvider provider;
    BarycentricApprox<MyData, 3> approx(&provider);

    MeshList<MyData, 3> list;
    list.push_back(&mesh1);
    list.push_back(&mesh2);

    MyData result;
    list.Approximate(approx, Point<3>(0.20, 0.15, 0.25), 120.0, result);
    double approximated1 = Field<0>(result);
    EXPECT_NEAR(approximated1, 248.333333333333, 0.00000005);
}



