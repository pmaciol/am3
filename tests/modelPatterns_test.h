
/*! \file modelPatterns_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelPatterns_test_h__
#define modelPatterns_test_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "mockMacroModels.h"
#include "../models/modelsScheme.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace mo  = am3::model;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace tests
{
	namespace model
	{
    mpr::Coordinates2D coord = mpr::Coordinates2D(0.5,0.5);
    mpr::Time t = mpr::Time(0.0);
    pnt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord,t);

// 		struct TemperatureToStress
//     {
//       typedef mpt::Point<typename tests::TemperatureContract::CoordinatesType> Point;
// 
//       boost::shared_ptr<tests::TemperatureContract> tempSource;
//       template<typename VAR>
//       VAR Get(Point* point );
// 
//       template<>
//       mpr::YieldStress Get(Point* point )
//       {
//         mpr::YieldStress stress;
//         mpr::Temperature temp;
//         temp = tempSource->GetState(point,&temp);
//         stress(temp()*2);
//         return stress;
//       }
//     };

    ///
    /// The ModelScheme class does not provide GetState() methods, just Get<>. This class introduces derivation from Contract (and provides GetState)
//     class TestModel
//       : public mo::ModelScheme<
//       mo::patterns::NeverNeeded<tests::TemperatureContract, tests::YieldStressContract2D>,
//       mo::patterns::NeverNeeded<tests::TemperatureContract, tests::YieldStressContract2D>,
//       mo::patterns::NeverNeeded<tests::TemperatureContract, tests::YieldStressContract2D>,
//       mo::patterns::NeverAvailable<tests::TemperatureContract, tests::YieldStressContract2D>,
//       mo::patterns::NeverAvailable<tests::TemperatureContract, tests::YieldStressContract2D>,
//       mo::patterns::TransparentResults<tests::YieldStressContract2D>,
//       tests::TemperatureContract >
//       , public tests::YieldStressContract2D
//     {
//     public:
//       mpr::YieldStress GetState(Point* point, const mpr::YieldStress* const typeTrait)
//       {
//         return Get<mpr::YieldStress>(point);
//       };
//     };

    //////////////////////////////////////////////////////////////////////////


	} //model
} //tests
#endif // modelPatterns_test_h__
