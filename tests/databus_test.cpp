// #include "databus_test.h"
// #include "../databus/databus.h"
// #include "../databus/dataStructures.h"
// #include <vector>
// #include <list>
// #include <deque>
// 
// namespace am3 {namespace tests {void RegisterDatabus(){};}}
// 
// namespace am3
// {
//   namespace databus
//   {
//     TEST_F(DatabusTest,AddToSingleStorage)
//     {
//       double testDouble = 1.34;
//       
//       Databus<DataStorage<double>/*,NullClass*/ > dbusVector;
//       Databus<DataStorage<double,std::list> > dbusList;
//       Databus<DataStorage<double,std::deque> > dbusDeque;
//       
//       Databus<DataStorage<double> >::DataStorage1::iterator vectorStorageIterator;
//       Databus<DataStorage<double,std::list> >::DataStorage1::iterator listStorageIterator;
//       Databus<DataStorage<double,std::deque> >::DataStorage1::iterator dequeStorageIterator;
//       
//       dbusVector.Storage1().push_back(testDouble);
//       vectorStorageIterator=dbusVector.Storage1().begin();
//       EXPECT_EQ(testDouble,(*vectorStorageIterator));
//  
//       dbusList.Storage1().push_back(testDouble);
//       listStorageIterator=dbusList.Storage1().begin();
//       EXPECT_EQ(testDouble,(*listStorageIterator));
// 
//       dbusDeque.Storage1().push_back(testDouble);
//       dequeStorageIterator=dbusDeque.Storage1().begin();
//       EXPECT_EQ(testDouble,(*dequeStorageIterator));
//     }
// 
//     TEST_F(DatabusTest,AddToMultiplyStorage)
//     {
//       double testDouble = 1.27;
//       int testInt = 2;
//       Databus<DataStorage<double> ,DataStorage<int> > dbus;
//       Databus<DataStorage<double> ,DataStorage<int> >::DataStorage1::iterator doubleStorageIterator;
//       Databus<DataStorage<double> ,DataStorage<int> >::DataStorage2::iterator intStorageIterator;
// 
//       dbus.Storage1().push_back(testDouble);
//       doubleStorageIterator=dbus.Storage1().begin();
//       EXPECT_EQ(testDouble,(*doubleStorageIterator));
// 
//       dbus.Storage2().push_back(testInt);
//       intStorageIterator=dbus.Storage2().begin();
//       EXPECT_EQ(testInt,(*intStorageIterator));
//     }
// 
//     TEST_F(DatabusTest,AdinaFStorage)
//     {
//       AdinaFUserMaterialNoMassRatio testAdinaMaterial;
//       testAdinaMaterial.x_velocity=1.0;
//       testAdinaMaterial.y_velocity=2.0;
// 
//       Databus<DataStorage<AdinaFUserMaterialNoMassRatio> > dbusAdinaVector;
//       Databus<DataStorage<AdinaFUserMaterialNoMassRatio,std::list> > dbusAdinaList;
// 
//       Databus<DataStorage<AdinaFUserMaterialNoMassRatio> >::DataStorage1::iterator vectorStorageIterator;
//       Databus<DataStorage<AdinaFUserMaterialNoMassRatio,std::list> >::DataStorage1::iterator listStorageIterator;
// 
// 
//       dbusAdinaVector.Storage1().push_back(testAdinaMaterial);
//       vectorStorageIterator=dbusAdinaVector.Storage1().begin();
//       EXPECT_EQ(testAdinaMaterial.x_velocity,(*vectorStorageIterator).x_velocity);
//        
//       dbusAdinaList.Storage1().push_back(testAdinaMaterial);
//       listStorageIterator=dbusAdinaList.Storage1().begin();
//       EXPECT_EQ(testAdinaMaterial.x_velocity,(*listStorageIterator).x_velocity);
//     }
// 
//     TEST_F(DatabusTest,AdinaFMultiStorage)
//     {
//       AdinaFUserMaterialNoMassRatio testAdinaMaterial;
//       AdinaFUserMaterialReturn testAdinaMaterialReturn;
//       testAdinaMaterial.x_velocity=1.0;
//       testAdinaMaterial.y_velocity=2.0;
//       testAdinaMaterialReturn.density=1000.0;
// 
//       Databus<DataStorage<AdinaFUserMaterialNoMassRatio>, DataStorage<AdinaFUserMaterialReturn> > dbusAdinaVector;
//       Databus<DataStorage<AdinaFUserMaterialNoMassRatio>, DataStorage<AdinaFUserMaterialReturn> >::DataStorage1::iterator vectorStorageIterator;
//       Databus<DataStorage<AdinaFUserMaterialNoMassRatio>, DataStorage<AdinaFUserMaterialReturn> >::DataStorage2::iterator vectorStorageIterator2;
// 
// 
//       dbusAdinaVector.Storage1().push_back(testAdinaMaterial);
//       vectorStorageIterator=dbusAdinaVector.Storage1().begin();
//       EXPECT_EQ(testAdinaMaterial.x_velocity,(*vectorStorageIterator).x_velocity);
// 
//       dbusAdinaVector.Storage2().push_back(testAdinaMaterialReturn);
//       vectorStorageIterator2=dbusAdinaVector.Storage2().begin();
//       EXPECT_EQ(testAdinaMaterialReturn.density,(*vectorStorageIterator2).density);
//     }
// 
//   }//namespace databus
// }