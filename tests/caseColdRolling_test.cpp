
/*! \file caseColdRolling_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "caseColdRolling_test.h"
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../modelPolycrystalMicrostructure/gottstein.h"

#include "../databus/adapter.h"
#include "../databus/databusTypelists.h"
/*	using ***********************************************************/

namespace mpt = am3::model::point;
namespace ppm = am3::problem::polycrystal;
namespace ccr = am3::cases::coldRolling;
namespace ada = am3::adapter;
namespace sdm = am3::software::deform;
namespace tsc = tests::caseColdRolling;
namespace mpm = am3::model::polycrystal;

using tests::caseColdRolling::Consts;
using namespace am3::databus;
using tsc::MacroInputsTestTempl;

/*	extern **********************************************************/

/*extern "C" */ __declspec( dllimport ) int DEFORM_USRE1_SEND(const int * length,const double * values	,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport) int DEFORM_USRE1_GET(const int * length, double * values,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport ) int DEFORM_NODES_SEND(const int * nodesNumber, const double * coordinates,	const double * time,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport) int DEFORM_TEMPERATURE(const double * temperature	,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport) int DEFORM_EFF_STR_RATE(const double * temperature	,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport) int DEFORM_YIELD_STRESS_GET(double * yieldStress,	const int * meshId,	const int * elementId);
/*extern "C" */  __declspec( dllimport) int DEFORM_TIME_STEP_LENGTH(const double * timeStepLength,	const int * meshId,	const int * elementId);

const double tests::caseColdRolling::Consts::yieldStressCheck = 123.0;
const double tests::caseColdRolling::Consts::yieldStressDStrainCheck = 12.30;
const double tests::caseColdRolling::Consts::yieldStressDStrainRateCheck = 1.230;
const int tests::caseColdRolling::Consts::nodesNumber = 4;
const double tests::caseColdRolling::Consts::coordinates[8] = { 0, 0, 1, 0, 1, 1, 0, 1 };
const double tests::caseColdRolling::Consts::time = 0;
const int tests::caseColdRolling::Consts::elementId = 1;
const int tests::caseColdRolling::Consts::meshId = 0;
const double tests::caseColdRolling::Consts::time2 = 0.1;
const double tests::caseColdRolling::Consts::coordinates2[8] = { 1, 0, 1, 1, 0, 1, 0, 0 };
const mpr::Coordinates2D tsc::Consts::coord = mpr::Coordinates2D(0.5, 0.5);
const mpr::Time tsc::Consts::t = mpr::Time(0.0);
pnt::ComputedPoint<mpr::Coordinates2D> tsc::Consts::point = pnt::ComputedPoint<mpr::Coordinates2D>(tsc::Consts::coord, tsc::Consts::t);

/*	classes *********************************************************/
namespace am3 {namespace tests {void RegisterCaseColdRolling (){};}}

typedef TEMPLATELIST_2(mpm::ShearModulusBySherstnevTemp, MacroInputsTestTempl) ModelsTemplatesList;

typedef DatabusTypelists<ModelsTemplatesList> Databus;

TEST(SherstnevModels, ShearModulusGenModel)
{
	Databus db;

	// Add MacroInputsTestTempl instance
	typedef ModelInstanceType<MacroInputsTestTempl, Databus>::Type MacroModel;
	// The line below cannot be included in template, due to undetermined constructors
	boost::shared_ptr<MacroModel> macroModel = boost::shared_ptr<MacroModel>(new MacroModel);
	SetModelInstance<Databus, MacroInputsTestTempl>(&db, macroModel);

	// Add ShearModulusBySherstnevTemp instance
	typedef ModelInstanceType<mpm::ShearModulusBySherstnevTemp, Databus>::Type ShearModulusModel;
	// The line below cannot be included in template, due to undetermined constructors
	boost::shared_ptr<ShearModulusModel> smModel = boost::shared_ptr<ShearModulusModel>(new ShearModulusModel);
	SetModelInstance<Databus, mpm::ShearModulusBySherstnevTemp>(&db, smModel);

	mpr::ShearModulus m;
//	m = db.GetFromDatabus<mpr::ShearModulus>(&tsc::Consts::point);

	EXPECT_NEAR(20.1e9, m.Value(), 0.1e9);
}
// TEST(SherstnevModels, DiffusionModel)
// {
// 	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
// 	boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
// 	boost::shared_ptr<mpm::DiffusionBySherstnev> diffusionModel (new mpm::DiffusionBySherstnev);
// 	
// 	ada::BindModelToBus(testDatabus,macroModel);
// 	ada::BindModelToBus(testDatabus,diffusionModel);
// 
//         DiffusionBySherstnevTemp(): b(0.286E-9), debye(19.3E+12), kb(1.38E-23), qgb(0.5*1.3), qsd(0.98) {};
//         virtual ppm::contract::SelfDiffusion GetState(pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::SelfDiffusion *const )
//         {
//           ppm::contract::SelfDiffusion toRet;
//           mpr::Temperature temperature;
//           temperature = db::GetFromDatabus<mpr::Temperature>(bus_,point);
//           toRet (pow(b,2)*debye*exp(-qsd * 1.6e-19 / (kb * temperature()) ));
//           return toRet;
//         }
//         virtual ppm::contract::GrainBoundaryDiffusion GetState(pnt::Point<mpr::Coordinates2D> * point,const ppm::contract::GrainBoundaryDiffusion *const )
//         {
//           ppm::contract::GrainBoundaryDiffusion toRet;
//           mpr::Temperature temperature;
//           temperature = db::GetFromDatabus<mpr::Temperature>(bus_,point);
//           double grainDif = pow(b,2)*debye*exp(-qgb*1.6E-19/(kb * temperature()));
//           toRet(grainDif);
//           return toRet;
//         }
//       protected:
//         const double b,debye,kb, qsd, qgb;
//       };
// 
// 
// 	selfDiffusionToCheck = selfDiffusionProvider->GetState(&tsc::Consts::point,&selfDiffusionToCheck
// 	grainBndDiffusionToCheck = grainBndDiffusionProvider->GetState(&tsc::Consts::point,&grainBndDiffusionToCheck);
// 
// 	EXPECT_NEAR(6.5e-13,selfDiffusionToCheck.Value(),0.2e-13);			//Based on M. Mantina, Y. Wang, R. Arroyave, L.Q. Chen and Z.K. Liu: First-Principles Calculation of Self-Diffusion Coefficients 
// 	EXPECT_NEAR(9.2e-11,grainBndDiffusionToCheck.Value(),0.2e-11);		//Based on M. Mantina, Y. Wang, R. Arroyave, L.Q. Chen and Z.K. Liu: First-Principles Calculation of Self-Diffusion Coefficients 
// }
// 
// 	  template<typename INPUT>
// 			class ZenerParamBySherstnev
// 				: public ppm::contract::ZenerContract2D
// 				, public mo::InputTypelist<INPUT>//<mpm::NoInput>
// 			{
// 			public:
// 				ZenerParamBySherstnev(): fv_disp(0.018), r_disp(0.6e-6) {};
// 				virtual ppm::contract::ZenerParam GetState(pnt::Point<mpr::Coordinates2D> *,const ppm::contract::ZenerParam *const );
// 			protected:
// 				const double fv_disp;
// 				const double r_disp;
// 			};
// 
// 	ada::BindModelToBus(testDatabus,zenerModel);
// 	
// 	ppm::contract::ZenerParam zenerParamToCheck;
// 	boost::shared_ptr<moc::Provider<ppm::contract::ZenerParam,mpr::Coordinates2D> > zenerParamProvider = testDatabus;
// 	
// 	zenerParamToCheck = zenerParamProvider->GetState(&tsc::Consts::point,&zenerParamToCheck);
// 	EXPECT_NEAR(14600,zenerParamToCheck.Value(),100);
// }
// 
// 
// TEST(SherstnevModels, ShearModulusModel)
// {
// 	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
// 	boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
// 	boost::shared_ptr<mpm::ShearModulusBySherstnev> shearModulusModel (new mpm::ShearModulusBySherstnev);
// 
// 	ada::BindModelToBus(testDatabus,macroModel);
// 	ada::BindModelToBus(testDatabus,shearModulusModel );
// 
// 	ada::BusModelAdapterList<mpm::ShearModulusBySherstnevInput::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::ShearModulusBySherstnev,pnt::Point<mpr::Coordinates2D> > busModelAdapterShearModulus;
// 	busModelAdapterShearModulus.Set(testDatabus,shearModulusModel );
// 
// 	mpr::ShearModulus shearModulusToCheck;
// 	boost::shared_ptr<moc::Provider<mpr::ShearModulus,mpr::Coordinates2D> > shearModulusProvider = testDatabus;
// 	
// 	shearModulusToCheck = shearModulusProvider->GetState(&tsc::Consts::point,&shearModulusToCheck);
// 	EXPECT_NEAR(20.1e9,shearModulusToCheck.Value(),0.1e9);
// }



// TEST(SherstnevModels, ShearModulus3LevelOldModel)
// {
//   boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
//   boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
//   boost::shared_ptr<mpm::ShearModulusBySherstnev> shearModulusModel (new mpm::ShearModulusBySherstnev);
//   boost::shared_ptr<mpm::DiffusionBySherstnev> diffusionModel (new mpm::DiffusionBySherstnev);
// 
//   ada::BindModelToBus(testDatabus,macroModel);
//   ada::BindModelToBus(testDatabus,shearModulusModel );
//   ada::BindModelToBus(testDatabus,diffusionModel );
// 
//   ada::BusModelAdapterList<mpm::ShearModulusBySherstnevInput::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::ShearModulusBySherstnev,pnt::Point<mpr::Coordinates2D> > busModelAdapterShearModulus;
//   busModelAdapterShearModulus.Set(testDatabus,shearModulusModel );
//   ada::BusModelAdapterList<mpm::DiffusionBySherstnevInput::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::DiffusionBySherstnev,pnt::Point<mpr::Coordinates2D> > busModelAdapterDiffusion;
//   busModelAdapterDiffusion.Set(testDatabus,diffusionModel);
// 
//   ppm::contract::SelfDiffusion selfDiffusionToCheck;
//   boost::shared_ptr<moc::Provider<ppm::contract::SelfDiffusion,mpr::Coordinates2D> > selfDiffusionProvider = testDatabus;
//     
//   mpr::ShearModulus shearModulusToCheck;
//   boost::shared_ptr<moc::Provider<mpr::ShearModulus,mpr::Coordinates2D> > shearModulusProvider = testDatabus;
//   
//   mpr::Temperature temperature;
//   boost::shared_ptr<moc::Provider<mpr::Temperature,mpr::Coordinates2D> > temperatureProvider = testDatabus;
// 
// 
//   temperature = temperatureProvider->GetState(&tsc::Consts::point,&temperature);
//   selfDiffusionToCheck = selfDiffusionProvider->GetState(&tsc::Consts::point,&selfDiffusionToCheck);
//   shearModulusToCheck = shearModulusProvider->GetState(&tsc::Consts::point,&shearModulusToCheck);
//   EXPECT_NEAR(20.1e9,shearModulusToCheck.Value(),0.1e9);
// }

TEST(SherstnevModels, ShearModulus3LevelFullVirtualModel)
{
  tsc::TestVirtualDatabus db;
//   boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
  boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
  boost::shared_ptr<tsc::ShearModulusBySherstnevVirt> shearModulusModel(new tsc::ShearModulusBySherstnevVirt);
  boost::shared_ptr<mpm::DiffusionBySherstnevVirt> diffusionModel (new mpm::DiffusionBySherstnevVirt);

  db.DiffPtr(diffusionModel.get());
  db.SmPtr(shearModulusModel.get());
  db.MacrPtr(macroModel.get());

  shearModulusModel->bus = &db;
  diffusionModel->bus = &db;

  mpr::ShearModulus shearModulusToCheck;
  moc::Provider<mpr::ShearModulus,mpr::Coordinates2D>* shearModulusProvider = &db;
  shearModulusToCheck = shearModulusProvider->GetState(&tsc::Consts::point,&shearModulusToCheck);
  EXPECT_NEAR(20.1e9,shearModulusToCheck.Value(),0.1e9);

}

typedef TEMPLATELIST_3( mpm::DiffusionBySherstnevTemp, mpm::ShearModulusBySherstnevTemp , MacroInputsTestTempl) ThreeModelsTemplatesList;
typedef DatabusTypelists<ThreeModelsTemplatesList> ThreeDatabus;

TEST(SherstnevModels, ShearModulus3LevelGenModel)
{
  ThreeDatabus tdb;

  // Add MacroInputsTestTempl instance
  typedef ModelInstanceType<MacroInputsTestTempl,ThreeDatabus>::Type MacroModel;
  // The line below cannot be included in template, due to undetermined constructors
  boost::shared_ptr<MacroModel> macroModel = boost::shared_ptr<MacroModel>(new MacroModel);
  SetModelInstance<ThreeDatabus,MacroInputsTestTempl>(&tdb,macroModel);

  // Add ShearModulusBySherstnevTemp instance
  typedef ModelInstanceType<mpm::ShearModulusBySherstnevTemp,ThreeDatabus>::Type ShearModulusModel;
  // The line below cannot be included in template, due to undetermined constructors
  boost::shared_ptr<ShearModulusModel> smModel = boost::shared_ptr<ShearModulusModel>(new ShearModulusModel);
  SetModelInstance<ThreeDatabus,mpm::ShearModulusBySherstnevTemp>(&tdb,smModel);

  typedef ModelInstanceType<mpm::DiffusionBySherstnevTemp,ThreeDatabus>::Type DiffusionModel;
  boost::shared_ptr<DiffusionModel> difModel = boost::shared_ptr<DiffusionModel>(new DiffusionModel);
  SetModelInstance<ThreeDatabus,mpm::DiffusionBySherstnevTemp>(&tdb,difModel);

  mpr::ShearModulus m;
  mpr::Temperature t;
  mpr::EffectiveStrainRate e;
  ppm::contract::SelfDiffusion sd;

//  ThreeDatabus::MergedMap* tmp;

//   m = GetFromDatabus<mpr::ShearModulus>(&tdb,&tsc::Consts::point);
//   t = GetFromDatabus<mpr::Temperature>(&tdb,&tsc::Consts::point);
//   e = GetFromDatabus<mpr::EffectiveStrainRate>(&tdb,&tsc::Consts::point);
//   sd = GetFromDatabus<ppm::contract::SelfDiffusion>(&tdb,&tsc::Consts::point);
  m = tdb.GetFromDatabus<mpr::ShearModulus>(&tsc::Consts::point);
  t = tdb.GetFromDatabus<mpr::Temperature>(&tsc::Consts::point);
  e = tdb.GetFromDatabus<mpr::EffectiveStrainRate>(&tsc::Consts::point);
  sd = tdb.GetFromDatabus<ppm::contract::SelfDiffusion>(&tsc::Consts::point);

  EXPECT_NEAR(20.1e9,m.Value(),0.1e9);
}

// TEST(DISABLED_SherstnevModels, GrainsModelDeformation)
// {
// 	boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tsc::ConstantYieldStress(Consts::yieldStressCheck,Consts::yieldStressDStrainCheck,Consts::yieldStressDStrainRateCheck));
// 	boost::shared_ptr<ccr::CaseModel> caseMacroImpl (new ccr::CaseModel(ySmodelPtr));
// 	sdm::DeformHandlerUserProc * deformHandlerUserProc;
// 	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
// 
// 	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
// 	deformHandlerUserProc->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);
// 	int userVariablesNumber=8;
// 	double userVariablesValues[8] = {100.0e10,5e-6,20e3,50e-6,2e15,150e10,25e-6,0.5};	
// 	// userVariablesValues(0): dislocation density [m/m3]
// 	// userVariablesValues(1): average subgrain size [m]
// 	// userVariablesValues(2): stored energy [J/m3]
// 	// userVariablesValues(3): grain size [m]
// 	// userVariablesValues(4): number of recrystallized nuclei per unit volume [1/m3] 
// 	// userVariablesValues(5): dislocation density of deformed grains [m/m3] 
// 	// userVariablesValues(6): recrystallized grain size [m]
// 	// userVariablesValues(7): Volume fraction of recrystallized grains [-]
// 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,Consts::meshId,Consts::elementId);
// 
// 	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform);
// 	boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
// 	boost::shared_ptr<tsc::ShearModulusTest> shearModulusModel (new tsc::ShearModulusTest);
// 	boost::shared_ptr<tsc::DislocationTest> dislocationModel (new tsc::DislocationTest);
// 
//   boost::shared_ptr<mpm::GrainsBySherstnevDeformation<ccr::Storage> > grainsModel (new mpm::GrainsBySherstnevDeformation<ccr::Storage>(caseMacroImpl));
// 
// 
// 	ada::BindModelToBus(testDatabus,macroModel);
// 	ada::BindModelToBus(testDatabus,dislocationModel);
// 	ada::BindModelToBus(testDatabus,grainsModel);
// 	ada::BindModelToBus(testDatabus,shearModulusModel);
// 
// 	ada::BusModelAdapterList<mpm::GrainsByShersnevDeformationInput2D::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::GrainsBySherstnevDeformation<ccr::Storage>,pnt::Point<mpr::Coordinates2D> > busModelAdapterGrains;
// 	busModelAdapterGrains.Set(testDatabus,grainsModel );
// 
// 	boost::shared_ptr<moc::Provider<ppm::contract::GrainSize,mpr::Coordinates2D> > grainSizeProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::RecrystalizedGrainSize,mpr::Coordinates2D> > recrystalizedGrainSizeProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::NumberOfRecrystalizedNuclei,mpr::Coordinates2D> > numberOfRecrystalizedNucleiProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::VolumeFractionOfRecrystallizedGrains,mpr::Coordinates2D> > volumeFractionOfRecrystallizedGrainsProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::StoredEnergy,mpr::Coordinates2D> > storedEnergyProvider = testDatabus;
// 	
// 
// 	ppm::contract::GrainSize grainSizeToCheck;
// 	ppm::contract::RecrystalizedGrainSize recrystalizedGrainSizeToCheck;
// 	ppm::contract::NumberOfRecrystalizedNuclei numberOfRecrystalizedNucleiToCheck;
// 	ppm::contract::VolumeFractionOfRecrystallizedGrains  volumeFractionOfRecrystallizedGrainsToCheck;
// 	ppm::contract::StoredEnergy storedEnergyToCheck;
// 
// 	grainSizeToCheck = grainSizeProvider->GetState(&tsc::Consts::point,&grainSizeToCheck);
// 	recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);
// 	numberOfRecrystalizedNucleiToCheck = numberOfRecrystalizedNucleiProvider->GetState(&tsc::Consts::point,&numberOfRecrystalizedNucleiToCheck);
// 	volumeFractionOfRecrystallizedGrainsToCheck = volumeFractionOfRecrystallizedGrainsProvider->GetState(&tsc::Consts::point,&volumeFractionOfRecrystallizedGrainsToCheck);
// 	storedEnergyToCheck = storedEnergyProvider->GetState(&tsc::Consts::point,&storedEnergyToCheck);
// 
// 	EXPECT_NEAR(50e-6,grainSizeToCheck.Value(),1e-6);
// 	EXPECT_NEAR(5e-6,recrystalizedGrainSizeToCheck.Value(),0.5e-6);
// 	EXPECT_DOUBLE_EQ(0.0,numberOfRecrystalizedNucleiToCheck.Value());
// 	EXPECT_DOUBLE_EQ(0.0,volumeFractionOfRecrystallizedGrainsToCheck.Value());
// 	EXPECT_NEAR(20e3,storedEnergyToCheck.Value(),1e-3);	// Fails. Sherstnev equations gives different results than in Sherstnev paper and PhD thesis
// }
// 
// TEST(DISABLED_SherstnevModels, GrainsModelNoDeformation)
// {
// 	boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tsc::ConstantYieldStress(Consts::yieldStressCheck,Consts::yieldStressDStrainCheck,Consts::yieldStressDStrainRateCheck));
// 	boost::shared_ptr<ccr::CaseModel> caseMacroImpl (new ccr::CaseModel(ySmodelPtr));
// 	sdm::DeformHandlerUserProc * deformHandlerUserProc;
// 	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
// 
//   mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
// 	deformHandlerUserProc->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);
// 	int userVariablesNumber=8;
// 	double userVariablesValues[8] = {100.0e10,5e-6,20e3,50e-6,2e15,150e10,25e-6,0.5};	
// 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,Consts::meshId,Consts::elementId);
// 
// 	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
// 	boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
// 	boost::shared_ptr<tsc::ShearModulusTest> shearModulusModel (new tsc::ShearModulusTest);
// 	boost::shared_ptr<tsc::DislocationTest> dislocationModel (new tsc::DislocationTest);
// 	boost::shared_ptr<tsc::DiffusionTest> diffusionModel (new tsc::DiffusionTest);
// 	boost::shared_ptr<tsc::ZenerParamTest> zenerModel (new tsc::ZenerParamTest);
// 
//   
// 	
// 	boost::shared_ptr<mpm::GrainsBySherstnevNoDeformation<ccr::Storage> > grainsModel (new mpm::GrainsBySherstnevNoDeformation<ccr::Storage>(caseMacroImpl));
// 
// 
// 	ada::BindModelToBus(testDatabus,macroModel);
// 	ada::BindModelToBus(testDatabus,dislocationModel);
// 	ada::BindModelToBus(testDatabus,grainsModel);
// 	ada::BindModelToBus(testDatabus,shearModulusModel);
// 	ada::BindModelToBus(testDatabus,zenerModel);
// 	ada::BindModelToBus(testDatabus,diffusionModel);
// 
//   ada::BusModelAdapterList<mpm::GrainsByShersnevNoDeformationInput2D::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::GrainsBySherstnevNoDeformation<ccr::Storage>,pnt::Point<mpr::Coordinates2D> > busModelAdapterGrains;
// 	busModelAdapterGrains.Set(testDatabus,grainsModel );
// 
// 	boost::shared_ptr<moc::Provider<ppm::contract::GrainSize,mpr::Coordinates2D> > grainSizeProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::RecrystalizedGrainSize,mpr::Coordinates2D> > recrystalizedGrainSizeProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::NumberOfRecrystalizedNuclei,mpr::Coordinates2D> > numberOfRecrystalizedNucleiProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::VolumeFractionOfRecrystallizedGrains,mpr::Coordinates2D> > volumeFractionOfRecrystallizedGrainsProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::StoredEnergy,mpr::Coordinates2D> > storedEnergyProvider = testDatabus;
// 
// 
// 	ppm::contract::GrainSize grainSizeToCheck;
// 	ppm::contract::RecrystalizedGrainSize recrystalizedGrainSizeToCheck;
// 	ppm::contract::NumberOfRecrystalizedNuclei numberOfRecrystalizedNucleiToCheck;
// 	ppm::contract::VolumeFractionOfRecrystallizedGrains  volumeFractionOfRecrystallizedGrainsToCheck;
// 	ppm::contract::StoredEnergy storedEnergyToCheck;
// 
// 	grainSizeToCheck = grainSizeProvider->GetState(&tsc::Consts::point,&grainSizeToCheck);
// 	recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);
// 	numberOfRecrystalizedNucleiToCheck = numberOfRecrystalizedNucleiProvider->GetState(&tsc::Consts::point,&numberOfRecrystalizedNucleiToCheck);
// 	volumeFractionOfRecrystallizedGrainsToCheck = volumeFractionOfRecrystallizedGrainsProvider->GetState(&tsc::Consts::point,&volumeFractionOfRecrystallizedGrainsToCheck);
// 	storedEnergyToCheck = storedEnergyProvider->GetState(&tsc::Consts::point,&storedEnergyToCheck);
// 
// 	EXPECT_NEAR((userVariablesValues[3]+userVariablesValues[6])/2,grainSizeToCheck.Value(),1e-6);
// 	EXPECT_GT(recrystalizedGrainSizeToCheck.Value(),userVariablesValues[6]);
// 	EXPECT_GT(numberOfRecrystalizedNucleiToCheck.Value(),userVariablesValues[4]);
// 	EXPECT_GT(volumeFractionOfRecrystallizedGrainsToCheck.Value(),userVariablesValues[7]);
// 	EXPECT_NEAR(20e3,storedEnergyToCheck.Value(),1e-3);	// Fails. Sherstnev equations gives different results than in Sherstnev paper and PhD thesis
// }
// 
// TEST(DISABLED_SherstnevModels, GrainsModelStaticRecovery)
// {
// 	boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tsc::ConstantYieldStress(Consts::yieldStressCheck,Consts::yieldStressDStrainCheck,Consts::yieldStressDStrainRateCheck));
// 	boost::shared_ptr<ccr::CaseModel> caseMacroImpl (new ccr::CaseModel(ySmodelPtr));
// 	sdm::DeformHandlerUserProc * deformHandlerUserProc;
// 	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
// 
//   mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
// 	deformHandlerUserProc->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);
// 	int userVariablesNumber=8;
// 	double userVariablesValues[8] = {100.0e10,5e-6,20e3,50e-6,2e15,150e10,25e-6,0.5};	
// 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,Consts::meshId,Consts::elementId);
// 
// 	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
// 	boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
// 	boost::shared_ptr<tsc::ShearModulusTest> shearModulusModel (new tsc::ShearModulusTest);
// 	boost::shared_ptr<tsc::DislocationTest> dislocationModel (new tsc::DislocationTest);
// 	boost::shared_ptr<tsc::DiffusionTest> diffusionModel (new tsc::DiffusionTest);
// 	boost::shared_ptr<tsc::ZenerParamTest> zenerModel (new tsc::ZenerParamTest);
// 
// 	boost::shared_ptr<mpm::GrainsBySherstnevStaticRecovery<ccr::Storage> > grainsModel (new mpm::GrainsBySherstnevStaticRecovery<ccr::Storage>(caseMacroImpl));
// 
// 
// 	ada::BindModelToBus(testDatabus,macroModel);
// 	ada::BindModelToBus(testDatabus,dislocationModel);
// 	ada::BindModelToBus(testDatabus,grainsModel);
// 	ada::BindModelToBus(testDatabus,shearModulusModel);
// 	ada::BindModelToBus(testDatabus,zenerModel);
// 	ada::BindModelToBus(testDatabus,diffusionModel);
// 
//   ada::BusModelAdapterList<mpm::GrainsByShersnevStaticRecoveryInput2D::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::GrainsBySherstnevStaticRecovery<ccr::Storage>,pnt::Point<mpr::Coordinates2D> > busModelAdapterGrains;
// 	busModelAdapterGrains.Set(testDatabus,grainsModel );
// 
// 	boost::shared_ptr<moc::Provider<ppm::contract::GrainSize,mpr::Coordinates2D> > grainSizeProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::RecrystalizedGrainSize,mpr::Coordinates2D> > recrystalizedGrainSizeProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::NumberOfRecrystalizedNuclei,mpr::Coordinates2D> > numberOfRecrystalizedNucleiProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::VolumeFractionOfRecrystallizedGrains,mpr::Coordinates2D> > volumeFractionOfRecrystallizedGrainsProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::StoredEnergy,mpr::Coordinates2D> > storedEnergyProvider = testDatabus;
// 
// 
// 	ppm::contract::GrainSize grainSizeToCheck;
// 	ppm::contract::RecrystalizedGrainSize recrystalizedGrainSizeToCheck;
// 	ppm::contract::NumberOfRecrystalizedNuclei numberOfRecrystalizedNucleiToCheck;
// 	ppm::contract::VolumeFractionOfRecrystallizedGrains  volumeFractionOfRecrystallizedGrainsToCheck;
// 	ppm::contract::StoredEnergy storedEnergyToCheck;
// 
// 	grainSizeToCheck = grainSizeProvider->GetState(&tsc::Consts::point,&grainSizeToCheck);
// 	recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);
// 	numberOfRecrystalizedNucleiToCheck = numberOfRecrystalizedNucleiProvider->GetState(&tsc::Consts::point,&numberOfRecrystalizedNucleiToCheck);
// 	volumeFractionOfRecrystallizedGrainsToCheck = volumeFractionOfRecrystallizedGrainsProvider->GetState(&tsc::Consts::point,&volumeFractionOfRecrystallizedGrainsToCheck);
// 	storedEnergyToCheck = storedEnergyProvider->GetState(&tsc::Consts::point,&storedEnergyToCheck);
// 
// 	EXPECT_NEAR((userVariablesValues[3]+userVariablesValues[6])/2,grainSizeToCheck.Value(),1e-6);
// 	EXPECT_NEAR(recrystalizedGrainSizeToCheck.Value(),userVariablesValues[6],userVariablesValues[6]/10);
// 	EXPECT_NEAR(numberOfRecrystalizedNucleiToCheck.Value(),userVariablesValues[4],userVariablesValues[4]/10);
// 	EXPECT_NEAR(volumeFractionOfRecrystallizedGrainsToCheck.Value(),userVariablesValues[7],userVariablesValues[7]/10);
// 	EXPECT_NEAR(20e3,storedEnergyToCheck.Value(),1e-3);	// Fails. Sherstnev equations gives different results than in Sherstnev paper and PhD thesis
// }
// 
// TEST(DISABLED_SherstnevModels, DislocationModelDeformation)
// {
// 	boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tsc::ConstantYieldStress(Consts::yieldStressCheck,Consts::yieldStressDStrainCheck,Consts::yieldStressDStrainRateCheck));
// 	boost::shared_ptr<ccr::CaseModel> caseMacroImpl (new ccr::CaseModel(ySmodelPtr));
// 	sdm::DeformHandlerUserProc * deformHandlerUserProc;
// 	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
// 
// 	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
// 	deformHandlerUserProc->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);
// 	int userVariablesNumber=8;
// 	double userVariablesValues[8] = {100.0e10,5e-6,20e3,50e-6,2e15,150e10,25e-6,0.5};	
// 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,Consts::meshId,Consts::elementId);
// 
// 	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
// 	boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
// 	boost::shared_ptr<tsc::DiffusionTest> diffusionModel (new tsc::DiffusionTest);
// 	boost::shared_ptr<tsc::ShearModulusTest> shearModulusModel (new tsc::ShearModulusTest);
// 
// 
// 	boost::shared_ptr<mpm::DislocationsGottsteinDeformation<ccr::Storage> > dislocationModel (new mpm::DislocationsGottsteinDeformation<ccr::Storage>(caseMacroImpl));
// 
// 
// 	ada::BindModelToBus(testDatabus,macroModel);
// 	ada::BindModelToBus(testDatabus,dislocationModel);
// 	ada::BindModelToBus(testDatabus,shearModulusModel);
// 	ada::BindModelToBus(testDatabus,diffusionModel);
// 
//   ada::BusModelAdapterList<mpm::DislocationsGottsteinDeformation2DInputs::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::DislocationsGottsteinDeformation<ccr::Storage>,pnt::Point<mpr::Coordinates2D> > busModelAdapterDislocations;
// 	busModelAdapterDislocations.Set(testDatabus,dislocationModel );
// 
// 	boost::shared_ptr<moc::Provider<ppm::contract::DislDensityMobileDeformed,mpr::Coordinates2D> > dislDensityMobileDeformedProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::DislDensityMobile,mpr::Coordinates2D> > dislDensityMobileProvider = testDatabus;
// 	
// 	ppm::contract::DislDensityMobileDeformed dislDensityMobileDeformedToCheck;
// 	ppm::contract::DislDensityMobile dislDensityMobileToCheck;
// 	
// 	dislDensityMobileDeformedToCheck = dislDensityMobileDeformedProvider->GetState(&tsc::Consts::point,&dislDensityMobileDeformedToCheck);
// 	dislDensityMobileToCheck = dislDensityMobileProvider->GetState(&tsc::Consts::point,&dislDensityMobileToCheck);
// 
// 	EXPECT_DOUBLE_EQ(dislDensityMobileDeformedToCheck.Value(),userVariablesValues[0]);
// 	EXPECT_NEAR(userVariablesValues[0],dislDensityMobileToCheck.Value(),userVariablesValues[0]/10); //Fails, Sherstnyev model needs explanations ...
// }
// 
// TEST(DISABLED_SherstnevModels, DislocationModelNoDeformation)
// {
// 	boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tsc::ConstantYieldStress(Consts::yieldStressCheck,Consts::yieldStressDStrainCheck,Consts::yieldStressDStrainRateCheck));
// 	boost::shared_ptr<ccr::CaseModel> caseMacroImpl (new ccr::CaseModel(ySmodelPtr));
// 	sdm::DeformHandlerUserProc * deformHandlerUserProc;
// 	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
// 
// 	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
// 	deformHandlerUserProc->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);
// 	int userVariablesNumber=8;
// 	double userVariablesValues[8] = {100.0e10,5e-6,20e3,50e-6,2e15,150e10,25e-6,0.5};	
// 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,Consts::meshId,Consts::elementId);
// 
// 	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
// 	boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
// 
// 	boost::shared_ptr<mpm::DislocationsGottsteinNoDeformation<ccr::Storage> > dislocationModel (new mpm::DislocationsGottsteinNoDeformation<ccr::Storage>(caseMacroImpl));
// 
// 
// 	ada::BindModelToBus(testDatabus,macroModel);
// 	ada::BindModelToBus(testDatabus,dislocationModel);
// 
// 	ada::BusModelAdapterList<mpm::DislocationsGottsteinNoDeformation2DInputs::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::DislocationsGottsteinNoDeformation<ccr::Storage>,pnt::Point<mpr::Coordinates2D> > busModelAdapterDislocations;
// 	busModelAdapterDislocations.Set(testDatabus,dislocationModel );
// 
// 	boost::shared_ptr<moc::Provider<ppm::contract::DislDensityMobileDeformed,mpr::Coordinates2D> > dislDensityMobileDeformedProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::DislDensityMobile,mpr::Coordinates2D> > dislDensityMobileProvider = testDatabus;
// 
// 	ppm::contract::DislDensityMobileDeformed dislDensityMobileDeformedToCheck;
// 	ppm::contract::DislDensityMobile dislDensityMobileToCheck;
// 
// 	dislDensityMobileDeformedToCheck = dislDensityMobileDeformedProvider->GetState(&tsc::Consts::point,&dislDensityMobileDeformedToCheck);
// 	dislDensityMobileToCheck = dislDensityMobileProvider->GetState(&tsc::Consts::point,&dislDensityMobileToCheck);
// 
// 	EXPECT_DOUBLE_EQ(dislDensityMobileDeformedToCheck.Value(),userVariablesValues[0]);
// 	EXPECT_NEAR(userVariablesValues[0],dislDensityMobileToCheck.Value(),userVariablesValues[0]/10); //Fails, Sherstnyev model needs explanations ...
// }
// 
// TEST(DISABLED_SherstnevModels, DislocationModelStaticRecovery)
// {
// 	boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tsc::ConstantYieldStress(Consts::yieldStressCheck,Consts::yieldStressDStrainCheck,Consts::yieldStressDStrainRateCheck));
// 	boost::shared_ptr<ccr::CaseModel> caseMacroImpl (new ccr::CaseModel(ySmodelPtr));
// 	sdm::DeformHandlerUserProc * deformHandlerUserProc;
// 	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
// 
// 	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
// 	deformHandlerUserProc->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);
// 	int userVariablesNumber=8;
// 	double userVariablesValues[8] = {100.0e10,5e-6,20e3,50e-6,2e15,150e10,25e-6,0.5};	
// 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,Consts::meshId,Consts::elementId);
// 
// 	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinDeform) ;
// 	boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
// 	boost::shared_ptr<tsc::DiffusionTest> diffusionModel (new tsc::DiffusionTest);
// 	boost::shared_ptr<tsc::ShearModulusTest> shearModulusModel (new tsc::ShearModulusTest);
// 
// 	boost::shared_ptr<mpm::DislocationsGottsteinStaticRecovery<ccr::Storage> > dislocationModel (new mpm::DislocationsGottsteinStaticRecovery<ccr::Storage>(caseMacroImpl));
// 
// 
// 	ada::BindModelToBus(testDatabus,macroModel);
// 	ada::BindModelToBus(testDatabus,dislocationModel);
// 	ada::BindModelToBus(testDatabus,shearModulusModel);
// 	ada::BindModelToBus(testDatabus,diffusionModel);
// 
// 	ada::BusModelAdapterList<mpm::DislocationsGottsteinStaticRecoveryInput2D::VariablesTypes , ccr::DatabusForGottsteinDeform,mpm::DislocationsGottsteinStaticRecovery<ccr::Storage>,pnt::Point<mpr::Coordinates2D> > busModelAdapterDislocations;
// 	busModelAdapterDislocations.Set(testDatabus,dislocationModel );
// 
// 	boost::shared_ptr<moc::Provider<ppm::contract::DislDensityMobileDeformed,mpr::Coordinates2D> > dislDensityMobileDeformedProvider = testDatabus;
// 	boost::shared_ptr<moc::Provider<ppm::contract::DislDensityMobile,mpr::Coordinates2D> > dislDensityMobileProvider = testDatabus;
// 
// 	ppm::contract::DislDensityMobileDeformed dislDensityMobileDeformedToCheck;
// 	ppm::contract::DislDensityMobile dislDensityMobileToCheck;
// 
// 	dislDensityMobileDeformedToCheck = dislDensityMobileDeformedProvider->GetState(&tsc::Consts::point,&dislDensityMobileDeformedToCheck);
// 	dislDensityMobileToCheck = dislDensityMobileProvider->GetState(&tsc::Consts::point,&dislDensityMobileToCheck);
// 
// 	EXPECT_DOUBLE_EQ(dislDensityMobileDeformedToCheck.Value(),userVariablesValues[0]);
// 	EXPECT_NEAR(userVariablesValues[0],dislDensityMobileToCheck.Value(),userVariablesValues[0]/10); //Fails, Sherstnyev model needs explanations ...
// }
// 
// TEST(ColdRolling, SwitcherForGrainsModel)
// {
// 	boost::shared_ptr<sdm::DeformStressContract2D > ySmodelPtr (new tsc::ConstantYieldStress(Consts::yieldStressCheck,Consts::yieldStressDStrainCheck,Consts::yieldStressDStrainRateCheck));
// 	boost::shared_ptr<ccr::CaseModel> caseMacroImpl (new ccr::CaseModel(ySmodelPtr));
// 	sdm::DeformHandlerUserProc * deformHandlerUserProc;
// 	deformHandlerUserProc = caseMacroImpl->GetHandler().get();
// 
//   boost::shared_ptr<sdm::DeformHandler<mpr::Coordinates2D> >testDh (new sdm::DeformHandler<mpr::Coordinates2D>(ySmodelPtr));
// 
// 	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();
// 
// 	deformHandlerUserProc->SetElementalNodes(Consts::nodesNumber,Consts::coordinates,Consts::time, Consts::meshId,Consts::elementId);
// 	int userVariablesNumber=8;
// 	double userVariablesValues[8] = {100.0e10,5e-6,20e3,50e-6,2e15,150e10,25e-6,0.5};	
// 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,Consts::meshId,Consts::elementId);
//   
// 	
// 	boost::shared_ptr<ccr::DatabusForGottsteinDeform> testDatabus (new ccr::DatabusForGottsteinModel<ccr::Storage>);
// 
//   testDatabus->SetPreviousStepsStorage(caseMacroImpl);
// 
// 	boost::shared_ptr<tsc::MacroInputsTest> macroModel (new tsc::MacroInputsTest);
// 	boost::shared_ptr<tsc::ShearModulusTest> shearModulusModel (new tsc::ShearModulusTest);
// 	boost::shared_ptr<tsc::DislocationTest> dislocationModel (new tsc::DislocationTest);
// 	boost::shared_ptr<tsc::DiffusionTest> diffusionModel (new tsc::DiffusionTest);
// 	boost::shared_ptr<tsc::ZenerParamTest> zenerModel (new tsc::ZenerParamTest);
// 
// 
// 
// 	boost::shared_ptr<mpm::GrainsBySherstnevStaticRecovery<ccr::Storage> > grainsModelStaticRecovery (new mpm::GrainsBySherstnevStaticRecovery<ccr::Storage>(caseMacroImpl));
// 	boost::shared_ptr<mpm::GrainsBySherstnevNoDeformation<ccr::Storage> > grainsModelNoDeformation (new mpm::GrainsBySherstnevNoDeformation<ccr::Storage>(caseMacroImpl));
// 	boost::shared_ptr<mpm::GrainsBySherstnevDeformation<ccr::Storage> > grainsModelDeformation (new mpm::GrainsBySherstnevDeformation<ccr::Storage>(caseMacroImpl));
// 
// 	ada::BindModelToBus(testDatabus,macroModel);
// 	ada::BindModelToBus(testDatabus,dislocationModel);
// 	ada::BindModelToBus(testDatabus,shearModulusModel);
// 	ada::BindModelToBus(testDatabus,zenerModel);
// 	ada::BindModelToBus(testDatabus,diffusionModel);
// 
//   ada::BusModelAdapterList<mpm::GrainsByShersnevStaticRecoveryInput2D::VariablesTypes , ccr::DatabusForGottsteinModel<ccr::Storage>,mpm::GrainsBySherstnevStaticRecovery<ccr::Storage>,pnt::Point<mpr::Coordinates2D> > busModelAdapterGrainsStaticRecovery;
// 	busModelAdapterGrainsStaticRecovery.Set(testDatabus,grainsModelStaticRecovery );
// 	ada::BusModelAdapterList<mpm::GrainsByShersnevNoDeformationInput2D::VariablesTypes , ccr::DatabusForGottsteinModel<ccr::Storage>,mpm::GrainsBySherstnevNoDeformation<ccr::Storage>,pnt::Point<mpr::Coordinates2D> > busModelAdapterGrainsNoDeformation;
// 	busModelAdapterGrainsNoDeformation.Set(testDatabus,grainsModelNoDeformation);
// 	ada::BusModelAdapterList<mpm::GrainsByShersnevDeformationInput2D::VariablesTypes , ccr::DatabusForGottsteinModel<ccr::Storage>,mpm::GrainsBySherstnevDeformation<ccr::Storage>,pnt::Point<mpr::Coordinates2D> > busModelAdapterGrainsDeformation;
// 	busModelAdapterGrainsDeformation.Set(testDatabus,grainsModelDeformation);
// 	
// 	boost::shared_ptr<tsc::Kbs> kbs (new tsc::Kbs(testDatabus));
// 	boost::shared_ptr<ada::Switcher<tsc::Kbs, ppm::contract::GrainsContract2D> > grainsSwitcher(new ada::Switcher<tsc::Kbs, ppm::contract::GrainsContract2D>(kbs));
// 	grainsSwitcher->AddModel(grainsModelStaticRecovery);
// 	grainsSwitcher->AddModel(grainsModelNoDeformation);
// 	grainsSwitcher->AddModel(grainsModelDeformation);
// 	ada::BindModelToBus(testDatabus,grainsSwitcher);
// 
// 	boost::shared_ptr<moc::Provider<ppm::contract::RecrystalizedGrainSize,mpr::Coordinates2D> > recrystalizedGrainSizeProvider = testDatabus;
// 	
// 	macroModel->efepse_(0.0);
// 	ppm::contract::RecrystalizedGrainSize recrystalizedGrainSizeToCheck;
// 	recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);
// 	
// 	EXPECT_GT(recrystalizedGrainSizeToCheck.Value(),userVariablesValues[6]);
// 	
// 	userVariablesValues[2] = 10e3;	
// 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,Consts::meshId,Consts::elementId);
// 		
// 	recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);
// 
// 	EXPECT_NEAR(recrystalizedGrainSizeToCheck.Value(),userVariablesValues[6],userVariablesValues[6]/100);
// 
// 	macroModel->efepse_(1.0);
// 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,Consts::meshId,Consts::elementId);
// 	
// 	recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);
// 
// 	EXPECT_DOUBLE_EQ(recrystalizedGrainSizeToCheck.Value(),5e-6);
// }
