namespace am3 {namespace tests {void RegisterKnowledge(){};}}

#include "gtest/gtest.h"
#include <boost/lexical_cast.hpp>
#include <boost/shared_ptr.hpp>
#include "../knowledgeRebit/rebitKnowledgeStructs.h"
#include "../models/dataStructures.h"
#include "../kbsRebit/reasoningData.h"
#include "../common/literals.h"
#include "../models/point.h"

namespace kre=am3::knowledge::rebit;
namespace prop=am3::model::properties;
namespace pnt=am3::model::point;

TEST(RebitConversions_Test, SetVariableDTO_test)
{
  prop::Strain2D str;
  VariableDTO var;
  str.g11(0.1);
  str.g12(0.2);
  str.g21(0.3);
  str.g22(0.4);
  EXPECT_FALSE(kre::SetVariableDTO(var,str));
  *var.Id=am3::kStrain2D;
  EXPECT_TRUE(kre::SetVariableDTO(var,str));
  EXPECT_DOUBLE_EQ(0.1,boost::lexical_cast<double>(var.Value->at(0)));
  EXPECT_DOUBLE_EQ(0.2,boost::lexical_cast<double>(var.Value->at(1)));
  EXPECT_DOUBLE_EQ(0.3,boost::lexical_cast<double>(var.Value->at(2)));
  EXPECT_DOUBLE_EQ(0.4,boost::lexical_cast<double>(var.Value->at(3)));
}

TEST(RebitConversions_Test, TwiceSetVariableDTO_test)
{
  prop::Temperature str;
  VariableDTO var;
  str(100);
  *var.Id=am3::kTemperature;
  EXPECT_TRUE(kre::SetVariableDTO(var,str));
  EXPECT_DOUBLE_EQ(100,boost::lexical_cast<double>(var.Value->at(0)));
  str(200);
  EXPECT_TRUE(kre::SetVariableDTO(var,str));
  EXPECT_DOUBLE_EQ(200,boost::lexical_cast<double>(var.Value->at(0)));
}

TEST(RebitConversions_Test, Point2D_test)
{
  VariableDTO varCoord;
  VariableDTO varTime;

  boost::shared_ptr<pnt::Point<prop::Coordinates2D> > wrongPoint;
  wrongPoint=kre::Point2DFromVariablDTO(varCoord,varTime);
  
  EXPECT_FALSE(wrongPoint);


  *varCoord.Id=am3::kCoordinates2D;
  *varTime.Id=am3::kTime;
  varCoord.Value->push_back("0.1");
  varCoord.Value->push_back("0.2");
  varTime.Value->push_back("0.3");

  boost::shared_ptr<pnt::ComputedPoint<prop::Coordinates2D> > point;
  point=kre::Point2DFromVariablDTO(varCoord,varTime);

  EXPECT_DOUBLE_EQ(0.1,point->Coordinates().x());
  EXPECT_DOUBLE_EQ(0.2,point->Coordinates().y());
  //EXPECT_DOUBLE_EQ(0.3,point->Time().Value());
	EXPECT_DOUBLE_EQ(0.3,point->GetTime());
}

TEST(RebitConversions_Test, Point3D_test)
{
  VariableDTO varCoord;
  VariableDTO varTime;

  boost::shared_ptr<pnt::Point<prop::Coordinates3D> > wrongPoint;
  wrongPoint=kre::Point3DFromVariablDTO(varCoord,varTime);

  EXPECT_FALSE(wrongPoint);


  *varCoord.Id=am3::kCoordinates3D;
  *varTime.Id=am3::kTime;
  varCoord.Value->push_back("0.1");
  varCoord.Value->push_back("0.2");
  varCoord.Value->push_back("0.3");
  varTime.Value->push_back("0.4");

  boost::shared_ptr<pnt::ComputedPoint<prop::Coordinates3D> > point;
  point=kre::Point3DFromVariablDTO(varCoord,varTime);

  EXPECT_DOUBLE_EQ(0.1,point->Coordinates().x());
  EXPECT_DOUBLE_EQ(0.2,point->Coordinates().y());
  EXPECT_DOUBLE_EQ(0.3,point->Coordinates().z());
//  EXPECT_DOUBLE_EQ(0.4,point->Time().Value());
	  EXPECT_DOUBLE_EQ(0.4,point->GetTime());
}

// 
// /*! \file knowledge_test.cpp **************************************************
//  * \author		Piotr Maciol
//  * \copyright 	GNU Public License.
//  * \brief       
//  * \details   	
// *********************************************************************/
// /*	include *********************************************************/
// 
// //#include <boost/mpl/list.hpp>
// #include <vector>
// #include "gtest/gtest.h"
// //#include "../knowledge/knowledge.h"
// #include "../knowledgeRebit/rebitKnowledgeStructs.h"
// #include "knowledge_test.h"
// //#include "../basicModels/basicModels.h"
// //#include "../models/description.h"
// 
// /*	using ***********************************************************/
// 
// namespace kno=am3::knowledge;
// 
// /*	extern **********************************************************/
// 
// //extern kno::KnowledgeSource kno::kKnowledgeSource;
// 
// /*	classes *********************************************************/
// 
// namespace am3 {namespace tests {void RegisterKnowledge(){};}}
// 
// void RegisterKnowledge(){};
// 
// namespace am3
// {
// 	namespace knowledge
// 	{
// // 		TEST(Knowledge,AccessToGlobalKnowledgeSource)
// // 		{
// // 			EXPECT_TRUE(kno::kKnowledgeSource.IsReady());
// // 		}
// 
// 		TEST(Knowledge,GetKnowledgeSubset)
// 		{
// 			kno::KnowledgeSource subset;
// // 			kno::Facts facts;
// // 			subset=kno::kKnowledgeSource.GetKnowledgeSubset(facts);
// 			EXPECT_TRUE(subset.IsReady());
// 		}
// 		TEST(Knowledge,GetKnowledgeSubSubset)
// 		{
// 			FAIL();
// 		}
// 
// 		TEST(Facts,ReadFacts)
// 		{
// 			FAIL();
// 		}
// 
// 		TEST(Facts,CheckFact)
// 		{
// 			FAIL();
// 		}
// 
//     TEST(RebitKnowledgeTests,viscosityFact)
//     {
//       am3::model::ViscosityWithVelocity tm;
//       am3::model::VelocityWithDerivatives pv;
//       std::vector<am3::knowledge::rebit::Fact<double> > vect;
//       am3::knowledge::rebit::ToFact(&vect,tm(pv));
// 
//       EXPECT_STREQ("viscosity", vect[0].Name().c_str());
//       EXPECT_STREQ("double", vect[0].Type_name().c_str());
//       EXPECT_DOUBLE_EQ(123.54, vect[0].Value());
//     }
// 
//     TEST(RebitKnowledgeTests,velocityFact)
//     {
//       am3::model::VelocityWithDerivatives pv;
//       std::vector<am3::knowledge::rebit::Fact<double> > vect;
//       am3::knowledge::rebit::ToFact(&vect,pv);
// 
//       EXPECT_STREQ("velocity_x", vect[0].Name().c_str());
//       EXPECT_STREQ("double", vect[0].Type_name().c_str());
//       EXPECT_DOUBLE_EQ(0.0, vect[0].Value());
// 
//       EXPECT_STREQ("velocity_y", vect[1].Name().c_str());
//       EXPECT_STREQ("double", vect[1].Type_name().c_str());
//       EXPECT_DOUBLE_EQ(0.0, vect[1].Value());
// 
//       EXPECT_STREQ("velocity_z_dz", vect[11].Name().c_str());
//       EXPECT_STREQ("double", vect[11].Type_name().c_str());
//       EXPECT_DOUBLE_EQ(0.0, vect[11].Value());
//     }
// 
// 	} //namespace knowledge
// } //namespace am3
