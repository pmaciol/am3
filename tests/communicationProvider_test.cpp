namespace am3 {namespace tests { void RegisterCommunicationProvider(){}; } }

#include "gtest/gtest.h"
#include "src/tests/communicationProvider_test.h"
#include "src/communicationProvider/predefinedStreams.h"
#include <boost/thread.hpp>

using namespace am3::common::communication;

TEST(communicationProviderTest, KeyboardAndScreenStreamTest)
{
	KeyboardAndScreenStream::initData knsInput;
	knsInput.alwaysAddNewline = true;
	KeyboardAndScreenStream kns(knsInput);

	ASSERT_TRUE( kns.write("KeybordStream: Writing hello world like a class method.") );
	ASSERT_TRUE( (kns << "KeybordStream: Passing arguments like to the standard stream").good() );

}

TEST(DISABLED_communicationProviderTest, FileStreamTest)
{
	FileStream::initData fileData;
	fileData.filename = "exampleFile.txt";
	fileData.openmode = std::fstream::in | std::fstream::out | std::fstream::trunc;
	FileStream am3fstream(fileData);

	ASSERT_TRUE( am3fstream.good() );
	ASSERT_TRUE( am3fstream.write("FileStream: Writing hello world like a class method.") );
	ASSERT_TRUE( (am3fstream << "FileStream: Passing arguments like to the standard stream").good() );
	am3fstream.close();
 	bool b=am3fstream.good();
 	ASSERT_FALSE(b);

	FileStream am3fstream2(fileData);
	std::string from_file;
	ASSERT_TRUE( am3fstream2.read(from_file) );
	std::cout << from_file;
	ASSERT_TRUE( ( am3fstream2 >> from_file).good() );
	std::cout << from_file;
	am3fstream2.close();
	b=am3fstream2.good();
	ASSERT_FALSE( b );
}

void messageRecieved(communicationException & e)
{
	std::cout << "Here callback function: communication succesful";
}

void serverThreadRun()
{
	Winsock2Stream::initData serverSocketData;
	serverSocketData.bufferSize = 1000;
	serverSocketData.hostname = "127.0.0.1";
	serverSocketData.serverPort = "5345";
	serverSocketData.operatingMode = serverSocketData.SERVER_MODE;
	Winsock2Stream serverSocketStream(serverSocketData);
	ASSERT_TRUE( serverSocketStream.good());

	char buffer[1000]={0};
	//serverSocketStream.read(buffer);
	serverSocketStream >> buffer;
	ASSERT_TRUE( serverSocketStream.good());
	std::cout << "  Server recieved a message: " << buffer << "\n" ;

	double tmp;
	//serverSocketStream.read(tmp);
	serverSocketStream >> tmp;
	ASSERT_TRUE( serverSocketStream.good());
	std::cout << "  Server recieved a number (double): " << tmp << "\n" ;

	int tmp2;
	//serverSocketStream.read(tmp2);
	serverSocketStream >> tmp2;
	ASSERT_TRUE( serverSocketStream.good());
	std::cout << "  Server recieved a number (int): " << tmp2 << "\n" ;

	serverSocketStream << 10001202;
	ASSERT_TRUE( serverSocketStream.good());
	std::cout << "  Server sended a number (int): " << 10001202 << "\n" ;

}

TEST(communicationProviderTest, Winsock2StreamTest)
{
	// Run server thread to accept client connection.
	// Since we have to make simultanoulsy connect() and accept()
	// we have to have another thread for server.
	boost::thread serverThread(serverThreadRun);
	
	Winsock2Stream::initData socketData;
	socketData.bufferSize = 1000;
	socketData.hostname = "127.0.0.1";
	socketData.serverPort = "5345";
	socketData.operatingMode = socketData.CLIENT_MODE;
	Winsock2Stream clientSocketStream(socketData);
	ASSERT_TRUE( clientSocketStream.good() );

	const char * message="Messege to send to server";
	//clientSocketStream.write(message);
	clientSocketStream << message;
	ASSERT_TRUE( clientSocketStream.good());
	std::cout << "  Client sended a message: " << message << "\n";

	double tmp=123.2324;
	//clientSocketStream.write(tmp);
	clientSocketStream << tmp;
	ASSERT_TRUE( clientSocketStream.good());
	std::cout << "  Client sended a number (double): " << tmp << "\n";

	int tmp2=12392487;
	//clientSocketStream.write(tmp2);
	clientSocketStream << tmp2;
	ASSERT_TRUE( clientSocketStream.good());
	std::cout << "  Client sended a number (int): " << tmp2 << "\n";

	int result(0);
	clientSocketStream >> result;
	ASSERT_TRUE( clientSocketStream.good());
	std::cout << "  Client recieved a number (int): " << result << "\n";

}


void serverThreadRun_functions()
{
	winsocketTranslator_data serverSocketData2;
	serverSocketData2.bufferSize = 1000;
	serverSocketData2.hostname = "127.0.0.1";
	serverSocketData2.serverPort = "5346";
	serverSocketData2.operatingMode = serverSocketData2.SERVER_MODE;
	int socketID = am3CreateWinsock2Stream(& serverSocketData2);
	std::cout << " Server created a socket with ID: " << socketID << "\n";
	ASSERT_TRUE( am3GoodWinsock2Stream(& socketID)==1 );
	

	char buffer[1000]={0};
	//serverSocketStream.read(buffer);
	//serverSocketStream >> buffer;
	am3Winsock2StreamReadCString(& socketID,buffer,1000);
	ASSERT_TRUE( am3GoodWinsock2Stream(& socketID)==1 );
	std::cout << "  Server recieved a message: " << buffer << "\n" ;

	double tmp=0;
	//serverSocketStream.read(tmp);
	//serverSocketStream >> tmp;
	am3Winsock2StreamReaddouble(& socketID,& tmp);
	ASSERT_TRUE( am3GoodWinsock2Stream(& socketID)==1 );
	std::cout << "  Server recieved a number (double): " << tmp << "\n" ;

	int tmp2=0;
	//serverSocketStream.read(tmp2);
	//serverSocketStream >> tmp2;
	am3Winsock2StreamReadint( & socketID,& tmp2);
	ASSERT_TRUE( am3GoodWinsock2Stream(& socketID)==1 );
	std::cout << "  Server recieved a number (int): " << tmp2 << "\n" ;

	//serverSocketStream << 10001202;
	int tmp3 = 10001202;
	am3Winsock2StreamWriteint(& socketID,& tmp3);
	ASSERT_TRUE( am3GoodWinsock2Stream(& socketID)==1 );
	std::cout << "  Server sended a number (int): " << 10001202 << "\n" ;

	am3DestroyWinsock2Stream(& socketID);

}

// TEST(communicationProviderTest, Winsock2StreamTest_FUNCTIONS)
// {
// 	// Run server thread to accept client connection.
// 	// Since we have to make simultanoulsy connect() and accept()
// 	// we have to have another thread for server.
// 	boost::thread serverThread2(serverThreadRun_functions);
// 	
// 	winsocketTranslator_data socketData2;
// 	socketData2.bufferSize = 1000;
// 	socketData2.hostname = "127.0.0.1";
// 	socketData2.serverPort = "5346";
// 	socketData2.operatingMode = socketData2.CLIENT_MODE;
// 	//Winsock2Stream clientSocketStream(socketData);
// 	int socketID = am3CreateWinsock2Stream(& socketData2);
// 	std::cout << " Client created a socket with ID: " << socketID << "\n";
// 	ASSERT_TRUE( am3GoodWinsock2Stream(&socketID) == 1);
// 
// 	const char * message="Messege to send to server";
// 	//clientSocketStream.write(message);
// 	//clientSocketStream << message;
// 	am3Winsock2StreamWriteCString(& socketID,message, static_cast<int>(strlen(message)) );
// 	ASSERT_TRUE( am3GoodWinsock2Stream(&socketID) == 1);
// 	std::cout << " Client sended a message: " << message << "\n";
// 
// 	double tmp=123.2324;
// 	//clientSocketStream.write(tmp);
// 	//clientSocketStream << tmp;
// 	am3Winsock2StreamWritedouble(& socketID, & tmp);
// 	ASSERT_TRUE( am3GoodWinsock2Stream(&socketID) == 1);
// 	std::cout << "  Client sended a number (double): " << tmp << "\n";
// 
// 	int tmp2=12392487;
// 	//clientSocketStream.write(tmp2);
// 	//clientSocketStream << tmp2;
// 	am3Winsock2StreamWriteint(&socketID,& tmp2);
// 	ASSERT_TRUE( am3GoodWinsock2Stream(&socketID) == 1);
// 	std::cout << "  Client sended a number (int): " << tmp2 << "\n";
// 
// 	int result(0);
// 	//clientSocketStream >> result;
// 	am3Winsock2StreamReadint(&socketID,&result);
// 	ASSERT_TRUE( am3GoodWinsock2Stream(&socketID) == 1);
// 	std::cout << "  Client recieved a number (int): " << result << "\n";
// 
// 	am3DestroyWinsock2Stream(& socketID);
// }