#include "mockMacroModels.h"


void tests::ExternalFemMock::RunStep1Element1()
{

  externalFemHandler_->SetElementalNodes(nodesNumber, coordinates, time, meshId, elementId);
  externalFemHandler_->SetElementalVariables(userVariablesNumber,userVariablesValues,meshId,elementId);
  externalFemHandler_->SetTemperature(&temperature,meshId,elementId);
  externalFemHandler_->SetEffectiveStrainRate(&effStrainRate,meshId,elementId);
  externalFemHandler_->SetStepLength(&timeStepLength,meshId,elementId);
  externalFemHandler_->GetElementalVariables(userVariablesNumber,userVariablesValues,meshId,elementId);
  yieldStress = externalFemHandler_->GetYieldStress(meshId,elementId);
//   yieldStressDStrain = externalFemHandler_->GetYieldStressDStrain(meshId,elementId);
//   yieldStressDStrainRate = externalFemHandler_->GetYieldStressDStrainRate(meshId,elementId);

}

mpr::Coordinates2D tests::testCoord(0.5,0.5);
mpr::Time tests::testTime(0.0);
pnt::ComputedPoint<mpr::Coordinates2D> tests::testPoint (tests::testCoord, tests::testTime);
