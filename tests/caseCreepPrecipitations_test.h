
/*! \file caseCreepPrecipitations_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseCreepPrecipitations_test_h__
#define caseCreepPrecipitations_test_h__
/*	include *********************************************************/

#include <boost/scoped_ptr.hpp>
#include "../modelPrecipitation/contracts.h"
#include "mockMacroModels.h"

/*	using ***********************************************************/

namespace mpc = am3::model::precipitations;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace tests
{
  namespace creepPrecipitations
  {
    class TemperatureInTime
      : public mpc::contract::PrecipitationsBasicInputs
    {
    public:
      void Init(const double& initTime, const double& timeStepLength, const double& temperature) 
      {
        time_.reset(new tests::TimeLinear(initTime,timeStepLength)); 
        temperature_.reset(new tests::TemperatureConstant(temperature));
      };
      virtual mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::Temperature *const temperature){return temperature_->GetState(point, temperature);};
      virtual mpr::Time GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::Time *const time){return time_->GetState(point,time);};
      void NextStep() {time_->NextStep();}
    protected:
      boost::scoped_ptr<tests::TimeLinear> time_;
      boost::scoped_ptr<tests::TemperatureConstant> temperature_;
    };

  }
}
#endif // caseCreepPrecipitations_test_h__
