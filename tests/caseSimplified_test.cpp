/*! \file caseSimplified_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
/*	include *********************************************************/

// Must be included first due to stupid winsock problems!
#include "caseSimplified_test.h"

#include "gtest/gtest.h"
#include <boost/shared_ptr.hpp>
#include "caseColdRolling_test.h"
#include "../modelPolycrystalMicrostructure/gottstein.h"
#include "../caseSimplified/caseSimplified.h"
#include "../caseSimplified/defs.h"
#include "../caseSimplified/macroModel.h"
#include "../softwareDeformBuiltin/deformHandlerOutputsProviders.h"
#include "mockMacroModels.h"



/*	using ***********************************************************/

namespace ccr = am3::cases::coldRolling;
using namespace am3::cases::simpified;
namespace ada = am3::adapter;
namespace tsc = tests::caseColdRolling;
namespace tss = tests::caseSimplified;
namespace ppm = am3::problem::polycrystal;
namespace mpm = am3::model::polycrystal;

/*	extern **********************************************************/



/*	classes *********************************************************/
namespace am3 {namespace tests {void RegisterCaseSimplified (){};}}

const double tests::caseSimplified::Consts::yieldStressCheck = 123.0;
const double tests::caseSimplified::Consts::yieldStressDStrainCheck = 12.30;
const double tests::caseSimplified::Consts::yieldStressDStrainRateCheck = 1.230;
const int tests::caseSimplified::Consts::nodesNumber=4;
const double tests::caseSimplified::Consts::coordinates[8]={0,0,1,0,1,1,0,1};
const double tests::caseSimplified::Consts::time = 0;
const int tests::caseSimplified::Consts::elementId = 1;
const int tests::caseSimplified::Consts::meshId = 0;
const double tests::caseSimplified::Consts::time2 = 0.1;
const double tests::caseSimplified::Consts::coordinates2[8]={1,0,1,1,0,1,0,0};
const mpr::Coordinates2D tests::caseSimplified::Consts::coord = mpr::Coordinates2D(0.5,0.5);
const mpr::Time tests::caseSimplified::Consts::t = mpr::Time(0.0);
pnt::ComputedPoint<mpr::Coordinates2D> tests::caseSimplified::Consts::point = pnt::ComputedPoint<mpr::Coordinates2D>(tsc::Consts::coord,tsc::Consts::t);


TEST(SimplifiedCase, DiffusionModel)
{
	
	DATABUS_INSTANCE(testDatabus,ccr::DatabusForGottsteinDeform);
	MODEL_INSTANCE(macroModel,tsc::MacroInputsTest)
	MODEL_INSTANCE(diffusionModel,mpm::DiffusionBySherstnev)

	DATABUS_ADD_MODELS(testDatabus,macroModel,diffusionModel);

	using mpm::DiffusionBySherstnev;
  MODEL_INPUT_FROM_DATABUS(diffusionModel,testDatabus);

	ppm::contract::SelfDiffusion selfDiffusionToCheck;
	ppm::contract::GrainBoundaryDiffusion grainBndDiffusionToCheck;
	boost::shared_ptr<moc::Provider<ppm::contract::SelfDiffusion,mpr::Coordinates2D> > selfDiffusionProvider = testDatabus;
	boost::shared_ptr<moc::Provider<ppm::contract::GrainBoundaryDiffusion,mpr::Coordinates2D> > grainBndDiffusionProvider = testDatabus;


	selfDiffusionToCheck = selfDiffusionProvider->GetState(&tsc::Consts::point,&selfDiffusionToCheck);
	grainBndDiffusionToCheck = grainBndDiffusionProvider->GetState(&tsc::Consts::point,&grainBndDiffusionToCheck);

	EXPECT_NEAR(6.5e-13,selfDiffusionToCheck.Value(),0.2e-13);			//Based on M. Mantina, Y. Wang, R. Arroyave, L.Q. Chen and Z.K. Liu: First-Principles Calculation of Self-Diffusion Coefficients 
	EXPECT_NEAR(9.2e-11,grainBndDiffusionToCheck.Value(),0.2e-11);		//Based on M. Mantina, Y. Wang, R. Arroyave, L.Q. Chen and Z.K. Liu: First-Principles Calculation of Self-Diffusion Coefficients 
}

TEST(SimplifiedCase, ZenerDragModel)
{
	DATABUS_INSTANCE(testDatabus,ccr::DatabusForGottsteinDeform)
	MODEL_INSTANCE(zenerModel,mpm::ZenerParamBySherstnev)
	DATABUS_ADD_MODELS(testDatabus,zenerModel)


	ppm::contract::ZenerParam zenerParamToCheck;
	boost::shared_ptr<moc::Provider<ppm::contract::ZenerParam,mpr::Coordinates2D> > zenerParamProvider = testDatabus;

	zenerParamToCheck = zenerParamProvider->GetState(&tsc::Consts::point,&zenerParamToCheck);
	EXPECT_NEAR(14600,zenerParamToCheck.Value(),100);
}


TEST(SimplifiedCase, SwitcherForGrainsModel)
{
	MODEL_INSTANCE(strMnull,tss::NullStressModel);
	DATABUS_INSTANCE(databus,CaseSimplifiedDatabus);

	// Using Deform as external FEM software
  DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, databus, databus,strMnull,strMnull);
  CASE_MODEL(CaseModel,caseMacroImpl);

  // 
  sdm::DeformHandlerUserProc * deformHandlerUserProc;
  deformHandlerUserProc = caseMacroImpl->GetHandler().get();
  databus->SetPreviousStepsStorage(caseMacroImpl);

	mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();

	deformHandlerUserProc->SetElementalNodes(tss::Consts::nodesNumber,tss::Consts::coordinates,tss::Consts::time, tss::Consts::meshId,tss::Consts::elementId);
	int userVariablesNumber=8;
	double userVariablesValues[8] = {100.0e10,5e-6,20e3,50e-6,2e15,150e10,25e-6,0.5};	
// 	// userVariablesValues(0): dislocation density [m/m3]
// 	// userVariablesValues(1): average subgrain size [m]
// 	// userVariablesValues(2): stored energy [J/m3]
// 	// userVariablesValues(3): grain size [m]
// 	// userVariablesValues(4): number of recrystallized nuclei per unit volume [1/m3] 
// 	// userVariablesValues(5): dislocation density of deformed grains [m/m3] 
// 	// userVariablesValues(6): recrystallized grain size [m]
// 	// userVariablesValues(7): Volume fraction of recrystallized grains [-]
 	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,tss::Consts::meshId,tss::Consts::elementId);
// 

 	
// 
	MODEL_INSTANCE(macroModel,tsc::MacroInputsTest)
	MODEL_INSTANCE(shearModulusModel,tsc::ShearModulusTest)
	MODEL_INSTANCE(dislocationModel,tsc::DislocationTest)
	MODEL_INSTANCE(diffusionModel,tsc::DiffusionTest)
	MODEL_INSTANCE(zenerModel,tsc::ZenerParamTest)

	typedef mpm::GrainsBySherstnevStaticRecovery<am3::cases::simpified::Storage> GrainsBySherstnevStaticRecovery;
	typedef mpm::GrainsBySherstnevNoDeformation<am3::cases::simpified::Storage> GrainsBySherstnevNoDeformation;
	typedef mpm::GrainsBySherstnevDeformation<am3::cases::simpified::Storage> GrainsBySherstnevDeformation;

	MODEL_WITH_STORAGE_INSTANCE(grainsModelStaticRecovery,GrainsBySherstnevStaticRecovery,caseMacroImpl)
	MODEL_WITH_STORAGE_INSTANCE(grainsModelNoDeformation,GrainsBySherstnevNoDeformation,caseMacroImpl)
	MODEL_WITH_STORAGE_INSTANCE(grainsModelDeformation,GrainsBySherstnevDeformation,caseMacroImpl)


 	MODEL_INPUT_FROM_DATABUS(grainsModelStaticRecovery,databus);
 	MODEL_INPUT_FROM_DATABUS(grainsModelNoDeformation,databus);
 	MODEL_INPUT_FROM_DATABUS(grainsModelDeformation,databus);
// 
  KBS_INSTANCE(kbs,tests::caseSimplified::Kbs,databus)

  SWITCHER_INSTANCE(grainsSwitcher,ppm::contract::GrainsContract2D,kbs)
 	ADD_MODELS_TO_SWITCHER(grainsSwitcher,grainsModelStaticRecovery,grainsModelNoDeformation,grainsModelDeformation);




  DATABUS_ADD_MODELS(databus,macroModel,dislocationModel,shearModulusModel,zenerModel,diffusionModel,grainsSwitcher)

 
	
  	boost::shared_ptr<moc::Provider<ppm::contract::RecrystalizedGrainSize,mpr::Coordinates2D> > recrystalizedGrainSizeProvider;
    recrystalizedGrainSizeProvider = databus;

	macroModel->efepse_(0.0);
	ppm::contract::RecrystalizedGrainSize recrystalizedGrainSizeToCheck;
	recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);

	EXPECT_GT(recrystalizedGrainSizeToCheck.Value(),userVariablesValues[6]);

	userVariablesValues[2] = 10e3;	
	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,tsc::Consts::meshId,tsc::Consts::elementId);

	recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);

	EXPECT_NEAR(recrystalizedGrainSizeToCheck.Value(),userVariablesValues[6],userVariablesValues[6]/100);

	macroModel->efepse_(1.0);
	deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,tsc::Consts::meshId,tsc::Consts::elementId);

	recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);

	EXPECT_DOUBLE_EQ(recrystalizedGrainSizeToCheck.Value(),5e-6);
}

TEST(DISABLED_SimplifiedCase, SwitcherForGrainsModelWithRebit)
{
  MODEL_INSTANCE(strMnull,tss::NullStressModel);
  DATABUS_INSTANCE(databus,CaseSimplifiedDatabus);

  // Using Deform as external FEM software
  DEFORM_ENTRY(sdm::DeformHandlerStressProxy<mpr::Coordinates2D>, databus, databus,strMnull,strMnull);
  CASE_MODEL(CaseModel,caseMacroImpl);

  sdm::DeformHandlerUserProc * deformHandlerUserProc;
  deformHandlerUserProc = caseMacroImpl->GetHandler().get();
  databus->SetPreviousStepsStorage(caseMacroImpl);

  mpt::ClearAllPoints<mpt::NamedPointLastTwoSteps2D>();

  deformHandlerUserProc->SetElementalNodes(tss::Consts::nodesNumber,tss::Consts::coordinates,tss::Consts::time, tss::Consts::meshId,tss::Consts::elementId);
  int userVariablesNumber=8;
  double userVariablesValues[8] = {100.0e10,5e-6,20e3,50e-6,2e15,150e10,25e-6,0.5};	
  deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,tss::Consts::meshId,tss::Consts::elementId);


  MODEL_INSTANCE(macroModel,tsc::MacroInputsTest)
    MODEL_INSTANCE(shearModulusModel,tsc::ShearModulusTest)
    MODEL_INSTANCE(dislocationModel,tsc::DislocationTest)
    MODEL_INSTANCE(diffusionModel,tsc::DiffusionTest)
    MODEL_INSTANCE(zenerModel,tsc::ZenerParamTest)

    typedef mpm::GrainsBySherstnevStaticRecovery<am3::cases::simpified::Storage> GrainsBySherstnevStaticRecovery;
  typedef mpm::GrainsBySherstnevNoDeformation<am3::cases::simpified::Storage> GrainsBySherstnevNoDeformation;
  typedef mpm::GrainsBySherstnevDeformation<am3::cases::simpified::Storage> GrainsBySherstnevDeformation;

  MODEL_WITH_STORAGE_INSTANCE(grainsModelStaticRecovery,GrainsBySherstnevStaticRecovery,caseMacroImpl)
    MODEL_WITH_STORAGE_INSTANCE(grainsModelNoDeformation,GrainsBySherstnevNoDeformation,caseMacroImpl)
    MODEL_WITH_STORAGE_INSTANCE(grainsModelDeformation,GrainsBySherstnevDeformation,caseMacroImpl)


    MODEL_INPUT_FROM_DATABUS(grainsModelStaticRecovery,databus);
  MODEL_INPUT_FROM_DATABUS(grainsModelNoDeformation,databus);
  MODEL_INPUT_FROM_DATABUS(grainsModelDeformation,databus);
  // 
  KBS_INSTANCE(kbs,tests::caseSimplified::KbsWithRebit,databus)

    SWITCHER_INSTANCE(grainsSwitcher,ppm::contract::GrainsContract2D,kbs)
    ADD_MODELS_TO_SWITCHER(grainsSwitcher,grainsModelStaticRecovery,grainsModelNoDeformation,grainsModelDeformation);




  DATABUS_ADD_MODELS(databus,macroModel,dislocationModel,shearModulusModel,zenerModel,diffusionModel,grainsSwitcher)



    boost::shared_ptr<moc::Provider<ppm::contract::RecrystalizedGrainSize,mpr::Coordinates2D> > recrystalizedGrainSizeProvider;
  recrystalizedGrainSizeProvider = databus;

  macroModel->efepse_(0.0);
  ppm::contract::RecrystalizedGrainSize recrystalizedGrainSizeToCheck;
  recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);

  EXPECT_GT(recrystalizedGrainSizeToCheck.Value(),userVariablesValues[6]);

  userVariablesValues[2] = 10e3;	
  deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,tsc::Consts::meshId,tsc::Consts::elementId);

  recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);

  EXPECT_NEAR(recrystalizedGrainSizeToCheck.Value(),userVariablesValues[6],userVariablesValues[6]/100);

  macroModel->efepse_(1.0);
  deformHandlerUserProc->SetElementalVariables(userVariablesNumber,userVariablesValues,tsc::Consts::meshId,tsc::Consts::elementId);

  recrystalizedGrainSizeToCheck = recrystalizedGrainSizeProvider->GetState(&tsc::Consts::point,&recrystalizedGrainSizeToCheck);

  EXPECT_DOUBLE_EQ(recrystalizedGrainSizeToCheck.Value(),5e-6);
}

