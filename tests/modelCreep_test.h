
/*! \file modelCreep_test.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelCreep_test_h__
#define modelCreep_test_h__
/*	include *********************************************************/

#include "../modelCreep/modelLogisticCreepStrain.h"
#include "../modelCreep/contracts.h"
#include "../modelCreep/creepRatePrecipitationAbe.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace mcr = am3::model::creep;
namespace mpr = am3::model::properties;
namespace ppc = am3::problem::creep;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace tests
{
  namespace creep
  {
    struct PublicModelLogisticCreepStrain
      : public mcr::ModelLogisticCreepStrain<mcr::ModelLogisticCreepStrainCoefficientsP91,mcr::TimeToRuptureMansonBrown4P91EN,mpr::Coordinates2D>
    {
      typedef mcr::ModelLogisticCreepStrain<mcr::ModelLogisticCreepStrainCoefficientsP91,mcr::TimeToRuptureMansonBrown4P91EN,mpr::Coordinates2D> BASE_CLASS;

      PublicModelLogisticCreepStrain(boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> ttrModel): BASE_CLASS::ModelLogisticCreepStrain(ttrModel)
      {};
      double CreepRate(const double & temperature, const double & stress , const double & strain) const
      {
        return BASE_CLASS::CreepRate(temperature,stress,strain);
      }
      double TimeToStrain(const double & temperature,const double & stressInPa,const double & strain)
      {
        return BASE_CLASS::TimeToStrain(temperature,stressInPa,strain);
      }
      double X0(const double & temperature, const double & stress) const
      {
        return BASE_CLASS::X0(temperature,stress);
      };

      double P(const double & temperature, const double & stress) const
      {
        return BASE_CLASS::P(temperature,stress);
      }
    };

    struct PublicModelLogisticCreepStrainP22
      : public mcr::ModelLogisticCreepStrain<mcr::ModelLogisticCreepStrainCoefficientsP22,mcr::TimeToRuptureMansonBrown4P22,mpr::Coordinates2D>
    {
      typedef mcr::ModelLogisticCreepStrain<mcr::ModelLogisticCreepStrainCoefficientsP22,mcr::TimeToRuptureMansonBrown4P22,mpr::Coordinates2D> BASE_CLASS;

      PublicModelLogisticCreepStrainP22(boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P22> ttrModel): BASE_CLASS::ModelLogisticCreepStrain(ttrModel)
      {

      }
      double CreepRate(const double & temperature, const double & stress , const double & strain) const
      {
        return BASE_CLASS::CreepRate(temperature,stress,strain);
      }
      double TimeToStrain(const double & temperature,const double & stressInPa,const double & strain)
      {
        return BASE_CLASS::TimeToStrain(temperature,stressInPa,strain);
      }
      double X0(const double & temperature, const double & stress) const
      {
        return BASE_CLASS::X0(temperature,stress);
      };

      double P(const double & temperature, const double & stress) const
      {
        return BASE_CLASS::P(temperature,stress);
      }
    };

    //////////////////////////////////////////////////////////////////////////


    struct TimeToRupturePrecipitationTest
      : public  mcr::TimeToRupturePrecipitationInternalAbeEquation<mcr::TimeToRuptureMansonBrown4P91EN,mcr::TimeToRupturePrecipitationCoefficients>
    {
      TimeToRupturePrecipitationTest(boost::shared_ptr<mcr::TimeToRuptureMansonBrown4P91EN> model) :  mcr::TimeToRupturePrecipitationInternalAbeEquation<mcr::TimeToRuptureMansonBrown4P91EN,mcr::TimeToRupturePrecipitationCoefficients>(model) {};
    };



    typedef mcr::CreepRatePrecipitation<mcr::ModelLogisticCreepStrain<mcr::ModelLogisticCreepStrainCoefficientsP91,TimeToRupturePrecipitationTest,mpr::Coordinates2D>, mcr::CreepRatePrecipitationCoefficients > PublicModelLogisticCreepStrainP91Precipitations;
    
    struct PublicModelLogisticCreepStrainPrecipitation
      : public mcr::ModelLogisticCreepStrain<mcr::ModelLogisticCreepStrainCoefficientsP91,TimeToRupturePrecipitationTest,mpr::Coordinates2D>
    {
      typedef mcr::ModelLogisticCreepStrain<mcr::ModelLogisticCreepStrainCoefficientsP91,TimeToRupturePrecipitationTest,mpr::Coordinates2D> BASE_CLASS;

      PublicModelLogisticCreepStrainPrecipitation(boost::shared_ptr<TimeToRupturePrecipitationTest> ttrModel): BASE_CLASS::ModelLogisticCreepStrain(ttrModel)
      {};
      double CreepRate(const double & temperature, const double & stress , const double & strain) const
      {
        return BASE_CLASS::CreepRate(temperature,stress,strain);
      }
      double TimeToStrain(const double & temperature,const double & stressInPa,const double & strain)
      {
        return BASE_CLASS::TimeToStrain(temperature,stressInPa,strain);
      }
      double X0(const double & temperature, const double & stress) const
      {
        return BASE_CLASS::X0(temperature,stress);
      };

      double P(const double & temperature, const double & stress) const
      {
        return BASE_CLASS::P(temperature,stress);
      }
    };


      struct CreepRatePrecipitationTest
        : public mcr::CreepRatePrecipitation<PublicModelLogisticCreepStrain, mcr::CreepRatePrecipitationCoefficients>
      {};

    };
  };
#endif // modelCreep_test_h__
