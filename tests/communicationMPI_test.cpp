
/*! \file communicationMPI_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include <utility>
#include "../communicationMPI/controller.h"
#include "../communicationMPI/instance.h"
#include "../models/point.h"
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace cco = am3::common::communication;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 { namespace tests { void RegisterCommunicationMPI(){}; } }
