
/*! \file modelPatterns_test.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "modelPatterns_test.h"
#include "../models/modelsPatterns.h"
#include "mockMacroModels.h"

/*	using ***********************************************************/

namespace mo = am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterModelPatterns(){};}}


TEST(ModelPatterns, Basic)
{
  boost::shared_ptr<tests::TemperatureConstant> tConst(new tests::TemperatureConstant(500));

  mo::ModelScheme<
    mo::patterns::NeverNeeded<tests::TemperatureContract, tests::YieldStressContract2D>,
    mo::patterns::NeverNeeded<tests::TemperatureContract, tests::YieldStressContract2D>,
    mo::patterns::NeverNeeded<tests::TemperatureContract, tests::YieldStressContract2D>,
    mo::patterns::NeverAvailable<tests::TemperatureContract, tests::YieldStressContract2D>,
    mo::patterns::NeverAvailable<tests::TemperatureContract, tests::YieldStressContract2D>,
    mo::patterns::TransparentResults<tests::YieldStressContract2D>,
    tests::TemperatureContract> 
    testModel;
  //tests::model::TestModel testModel2;

  boost::shared_ptr<mo::patterns::NeverNeeded<tests::TemperatureContract, tests::YieldStressContract2D>> nnModel(new mo::patterns::NeverNeeded<tests::TemperatureContract, tests::YieldStressContract2D>);
  boost::shared_ptr<mo::patterns::NeverAvailable<tests::TemperatureContract, tests::YieldStressContract2D>> naModel(new mo::patterns::NeverAvailable<tests::TemperatureContract, tests::YieldStressContract2D>);
  boost::shared_ptr<mo::patterns::TransparentResults<tests::YieldStressContract2D> > trResults(new mo::patterns::TransparentResults<tests::YieldStressContract2D>);

  shared_ptr<tests::ConstantStress> stressModel(new tests::ConstantStress(1e2,0.0,0.0));

  trResults->SetModelComputations(stressModel);

  mpr::YieldStress ysTest;
//   testModel2.Setup(tConst,trResults,nnModel,nnModel,nnModel,naModel,naModel);
//   ysTest = testModel2.Get<mpr::YieldStress>(&tests::model::point);

  //   typedef ad::IVStorage<sdm::DeformHandler,pdi::contract::DislocationsStorageContract> Storage;
  //   mdi::DislocationWallEquation<Storage> test(0.1,0.1,0.1);
}