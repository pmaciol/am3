
/*! \file modelPrecipitations.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "gtest/gtest.h"
#include "caseCreepPrecipitations_test.h"
#include "../modelPrecipitation/simpleEquations.h"

/*	using ***********************************************************/

namespace mpc = am3::model::precipitations;

/*	extern **********************************************************/



/*	classes *********************************************************/


namespace am3 {namespace tests {void RegisterModelPrecipitations (){};}}

// TEST(PrecipitationsSimpleEquations, test)
// {
//   mpc::PrecipitationsAmountExponential test(1.2 , 3e-6 , 2.0/3.0);
//   mpr::PrecipitationsAmount precAmount;
//   mpr::Coordinates2D coord(0.5,0.5);
//   mpr::Time t(0.0);
//   pnt::ComputedPoint<mpr::Coordinates2D> point (coord,t);
//   boost::shared_ptr<tests::creepPrecipitations::TemperatureInTime> tempInTime = boost::shared_ptr<tests::creepPrecipitations::TemperatureInTime>(new tests::creepPrecipitations::TemperatureInTime);
//   tempInTime->Init(1e-3,100.0,600.0);
//   test.Set(tempInTime);
// 
//   test.GetState(&point, &precAmount);
// }
// 
// TEST(PrecipitationsSimpleEquations, test2)
// {
//   mpc::PrecipitationsAmountExponentialCoefficientsAbe10Cr30Mn026C coeff;
//   mpc::PrecipitationsAmountExponential test(coeff);
//   mpr::PrecipitationsAmount precAmount;
//   mpr::Coordinates2D coord(0.5,0.5);
//   mpr::Time t(0.0);
//   pnt::ComputedPoint<mpr::Coordinates2D> point (coord,t);
//   boost::shared_ptr<tests::creepPrecipitations::TemperatureInTime> tempInTime = boost::shared_ptr<tests::creepPrecipitations::TemperatureInTime>(new tests::creepPrecipitations::TemperatureInTime);
//   tempInTime->Init(1e-3,100.0,600.0);
//   test.Set(tempInTime);
//   test.GetState(&point, &precAmount);
// }
