#include "caseColdRolling_TestModel.h"

mpr::ShearModulus tests::caseColdRolling::ShearModulusBySherstnevVirt::GetState(pnt::Point<mpr::Coordinates2D> * point, const mpr::ShearModulus *const)
{
	mpr::ShearModulus toRet;
	moc::Provider<mpr::Temperature, mpr::Coordinates2D>* ptr;
	ptr = bus;
	mpr::Temperature temperature = ptr->GetState(point, &temperature);
	//toRet((84.828E+9 - 4.06E+7 * temperature()) / (2 * (1 + pois))); // Sherstnev values
	toRet((32.102 - 0.0155 * temperature())*1e9); // Estimation based on Sherstnev phd, Abbildung 5-8:
	return toRet;
}

