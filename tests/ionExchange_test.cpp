namespace am3 {namespace tests {void RegisterIonExchange(){};}}		

#include "gtest/gtest.h"
#include "../modelIonExchange/databus.h"
#include "../modelIonExchange/adapters.h"
#include "../modelIonExchange/knowledge.h"
#include "../modelIonExchange/knowledgeBuiltIn.h"
#include "../modelIonExchange/softwareAdina.h"
#include "../modelIonExchange/modelsTest.h"

namespace pie=am3::problem::ionExchange;



TEST(IonExchange, overallTest)
{
	typedef pie::knowledge::IonExchangeBuiltInKBS Kbs;
 	typedef pie::software::AdinaF2DIonExchange MacroScaleIonExchange;
	
 	// Knowledge based connection
 	boost::shared_ptr<Kbs> kbs (new Kbs);
 
 	// KBS representations for adapters, initialized with main KBS
 	// Here is only one KBS, can be more
	boost::shared_ptr<pie::knowledge::KbsRepresentationForAllModels> kbsForAll (new pie::knowledge::KbsRepresentationForAllModels(kbs));
	
	// Could be like here:

// 	pie::knowledge::KbsRepSingleFluid kbsRepSingleFluid(kbs);
// 	pie::knowledge::KbsRepTwoFluidSurface kbsRepTwoFluidSurface(kbs);
	// etc ...

	// External model for macroscale problem
	boost::shared_ptr<MacroScaleIonExchange> macroScaleIonExchange (new MacroScaleIonExchange);

	// Adapters, initialized with KbsReprezentation
 	boost::shared_ptr<pie::adapters::SingleFluid> adapterSingleFluid (new pie::adapters::SingleFluid(kbsForAll));
 	boost::shared_ptr<pie::adapters::Dispersion> adapterDispersion (new pie::adapters::Dispersion(kbsForAll));
	//boost::shared_ptr<pie::adapters::TwoPhaseComposition> adapterTwoPhase (new pie::adapters::TwoPhaseComposition(kbsForAll));
	// etc ...
// 
	// Databus, must know all adapters
	// For specific databus, whole set of interfaces must be covered with adapters and one macroscale model 
	boost::shared_ptr<pie::databus::DatabusTurbulentCase> databus (new pie::databus::DatabusTurbulentCase(
		macroScaleIonExchange,
		adapterSingleFluid,
		adapterDispersion,
		macroScaleIonExchange
		// etc ... 
		));
	macroScaleIonExchange->SetDatabus(databus);

	// models, must be initialized with input interfaces, here it is databus for all
	boost::shared_ptr<pie::model::VelocityDependent> sf1 (new pie::model::VelocityDependent(databus));
	boost::shared_ptr<pie::model::TemperatureDependent> sf2 (new pie::model::TemperatureDependent(databus));
 
	boost::shared_ptr<pie::model::ForDispersionShareRate> di1 (new pie::model::ForDispersionShareRate(databus));
	boost::shared_ptr<pie::model::ForDispersionShareRateTemp> di2 (new pie::model::ForDispersionShareRateTemp(databus,databus));

//	boost::shared_ptr<pie::model::ForTwoPhase> tp1 (new pie::model::ForTwoPhase(databus));

	// etc ...
// 
	// add models to adapters, verify kbs's are compatible
	adapterSingleFluid->AddModel(sf1);
 	adapterSingleFluid->AddModel(sf2);
 	adapterSingleFluid->IsKnowledgeProperForAllModels();
// 
	adapterDispersion->AddModel(di1);
	adapterDispersion->AddModel(di2);
 	adapterDispersion->IsKnowledgeProperForAllModels();
	// etc ...

// 	adapterTwoPhase->AddModel(tp1);
// 	adapterTwoPhase->IsKnowledgeProperForAllModels();


// that means "start listening on socket"
//	macroScaleIonExchange->Run();

	FAIL();

}