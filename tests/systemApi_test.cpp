/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/

/*	include *********************************************************/

#include <string>
#include "gtest/gtest.h"
#include "../common/systemApi.h"
#include "../common/systemApiForTests.h"
#include "../common/config.h"
//#include "globalDefs.h"

/*	using ***********************************************************/

using std::string;

/*	extern **********************************************************/
namespace am3
{
  namespace common
  {
    extern Configuration kProgramOptions;
  }
}
/*	classes *********************************************************/

namespace am3 {namespace tests {void RegisterSystemApi(){};}}

namespace am3
{

  namespace systemApi
  {
    TEST(WindowsApiTests,AccessToSystemApi)
    {
      SystemApiHandler sysApiH;
      int test = sysApiH.GetSystemApi()->AsynchronicExecute("","ping","", "");
      EXPECT_GT(test,32);
    }
    TEST(WindowsApiTests,AsynchronicExecute)
    {
       WindowsApi w_api;
       int test = w_api.AsynchronicExecute("","ping","-n 10", "");
       EXPECT_GT(test,32);
    }
    TEST(WindowsApiTests,SynchronicExecute)
    {
      WindowsApi w_api;
      int test = w_api.SynchronicExecute("","ping","-n 10", "");
      std::cout<<"\nSynchronous execute should take longer than asynchronous, check is the time right\n\n";
      EXPECT_GT(test,0);
    }

		TEST(WindowsApiTests,TestFileExists)
		{
			WindowsApi w_api;
			am3::systemApi::SystemApiHandlerForTests apiHandler;
			string confFileName=am3::common::kProgramOptions.GetMainVariableMap()["mainCfg"].as<string>();
			string confFilePath=am3::common::kProgramOptions.GetMainVariableMap()["mainCfgPath"].as<string>();

			int fileExists=apiHandler.GetSystemApiForTests()->FileExists(confFileName,confFilePath);
			EXPECT_EQ(1,fileExists);
		}

    TEST(WindowsApiTests,DeleteFile)
    {
      WindowsApi w_api;
      am3::systemApi::SystemApiHandlerForTests apiHandler;
      string confFileName=am3::common::kProgramOptions.GetMainVariableMap()["mainCfg"].as<string>();
      string confFilePath=am3::common::kProgramOptions.GetMainVariableMap()["mainCfgPath"].as<string>();

			int fileExists=apiHandler.GetSystemApiForTests()->FileExists(confFileName,confFilePath);
			EXPECT_EQ(1,fileExists);
      apiHandler.GetSystemApiForTests()->CpFile(confFilePath+confFileName,confFileName+".tmp");
      int test = w_api.DelFile("",confFileName+".tmp");
      EXPECT_EQ(test,1);
    }
  }
}