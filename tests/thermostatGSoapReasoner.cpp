/*! \file thermostatGSoapReasoner.cpp *********************************
 * \author		Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

#include "thermostatGSoapReasoner.h"

namespace tests
{
	// callback mathod for retrieving (or calcuating) values of variables requiered in inference process
	std::vector<std::string> *ThermostatGSoapReasoner::TemplateGetVariableValue(std::string varId)
	{
		std::vector<std::string> *result = new std::vector<std::string>();
		if (varId == "day")
		{
			// here you can put the code for retrieving the value of the variable "day".
			// this code can be executed only in the case when the value this variable i really required.
			result->push_back("Monday");
		}
		if (varId == "time")
			result->push_back("11");
		if (varId == "month")
			result->push_back("January");

		return result;
	}
}