/*! \file main.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief  Example of am3 realization
 * \details All elements, necessary to design simple multiscale model are described here
*********************************************************************/










// WHEN ADINA WILL WORK WITH REBIT - REMOVE THIS PROJECT!!!!!








/*	include *********************************************************/


// #define _CRTDBG_MAP_ALLOC
// #include <stdlib.h>
// #include <crtdbg.h>


//#include <boost/program_options.hpp>
#include <string>
#include "prototype.h" //Protypes are here; should be replaced with final versions
//#include "adinaTest.h"
//#include "../common/config.h"
#include <boost/thread.hpp> 
#include "../basicModels/mockModel.h"

#include "../softwareAdina/adinaSocketServer.h"
#include "../softwareAdina/adinaFprocessor.h"

//#include "../adapterAdina/adapterAdina.h"

#include "../adapterIonExchange/adapterIonExchange.h"
#include "../softwareAdina/adinaFControls.h"

#include "../basicModels/rebitInterface.h"
#include "../softwareAdina/storageAdina.h"


#include "../modelAdinaSimple/adapter.h"
#include "../modelAdinaSimple/proxy.h"
#include "../modelAdinaSimple/problemDependentDataStructures.h"
#include "../modelAdinaSimple/kbs.h"

#include "../models/point.h"

/*	using ***********************************************************/

using std::string;
namespace adi=am3::model::adina;
namespace sto=am3::storage;

/*	definitions *********************************************************/

//@{ \name globals_for_program_options Globals
//! Used by boost::program_options implementation of program options
extern int*  global_argc;
extern char ***global_argv;
//@}

/*	classes *********************************************************/

int main(int argc, char **argv)
{

//  adi::AdinaFCallsProcessor am3;

  //Passing argv to progam_options
  global_argc=&argc;
  global_argv=&argv;

  //Initialization od program_options, reading config files (check am3.conf in main binary directory)
  kProgramOptions.Init();

  //Protope \todo main am3 Simulation should be used 
//  Simulation sim;
  //Protope \todo main am3 Manager should be used
//  Manager man;

  //  AdinaAdapter adAdapter;

  //Adapter knows, what adapts. Set of models are connected with Adapter
//  am3::model::mock::MockAdapter adAdapter;
  am3::knowledge::rebit::MockRebitKBS kbs;

//   am3::model::mock::MockAdapterProxy adProxy(&adAdapter,&kbs) ;
// 
// 
//   man.SetMacroAdapter(&adAdapter);
//   sim.SetManager(&man);
//   sim.Validate();
//  sim.Run();

// Adina test
   Simulation simAdina;
   Manager manAdina;


   am3::knowledge::example1::Example1KBS ex1Kbs;
    am3::adapter::adinaSimple::SimpleAdinaAdapter adinaSimpleAdapter;


    // With example
    am3::adapter::adinaSimple::SimpleAdinaAdapterProxy<kno::example1::Example1KBS, kno::example1::Example1GeneralRebit<am3::adapter::adinaSimple::SimpleAdinaAdapter> > 
      adinaSimpleProxy(&adinaSimpleAdapter,&ex1Kbs);
    // With Rebit
//     am3::adapter::adinaSimple::SimpleAdinaAdapterProxy<am3::knowledge::rebit::MockRebitKBS, kno::example1::Example1GeneralRebit<am3::adapter::adinaSimple::SimpleAdinaAdapter> > 
//     adinaSimpleProxy(&adinaSimpleAdapter,&kbs);




//     am3::model::common::Point p;
//     am3::model::example1::AgglomerationFactor af;
 //   adinaSimpleAdapter.GetState(&p,&af);

   am3::model::ModelAdina ma;

   sto::AdinaFStorage storage;

   adinaSimpleAdapter.Init(&storage);
  // am3.Init(&storage);

//    am3::adapter::IonExchangeAdapter adapter;
//    am3::adapter::IonExchangeAdapterProxy ieProxy(&adapter,&kbs) ;



   //ma.Init(&adapter,&storage);

   ma.Init(&adinaSimpleAdapter,&storage);
   ma.Run();

}