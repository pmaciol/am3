/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/

/*	include *********************************************************/

#include "adapter.h"
#include "../logger/logger.h"

/*	using ***********************************************************/



/*	classes *********************************************************/


//////////////////////////////////////////////////////////////////////////
void Adapter::Validate()
{
  am3::logger::NotImplemented(__FUNCTION__);
}

void Adapter::Run()
{
//   am3::logger::Starting("adapter running");
//     macro_model_->Init(); //Initialization, making files, preparing workspace etc.
// //    macro_model_->IC(); //Numerical models' Initial Conditions
//     NewThread(macro_model_->Run()); //Run simulation as a new thread
//   am3::logger::Stopping("adapter running");
 }

void Adapter::AddFineModels( KnowledgeSource& kSource )
{
    am3::logger::Starting("adding fine models");
//    KnowledgeValues modelDescription;
//    modelDescription=modelExternalDescripton.GetModelDescripton(); // ticket 32

/*!
* \todo How find micro models?
* GetProblemDescription()  //from KnowledgeSource (external)
* GetMacroModelDescription() //from macro_model
*   // Possible fine models list have to be coded by adapter creator/programmer
* ExternalKnowledgeSource -> WhichModelsArePossible( problemDescription , macroModelDescription , listOfFineModelsDescriptions )
* 
* ///////////////////////////////////////////////////////////////////////
* // What should be in Knowledge, what hardly coded?
* ///////////////////////////////////////////////////////////////////////
*/

// OK, this is important!!
//    available_fine_models_.AddModels(this->macro_model_->ModelContract(),implementedModelsContainer,kSource);


  am3::logger::Stopping("adding fine models");
}

// double Adapter::DoubleData( const int dataPositionInVector,const State* state )
// {
//   am3::logger::NotImplemented(__FUNCTION__);
//   return -1;
// }

Adapter::Adapter()
{
  //macro_model_=new am3::model::ModelAdina();
}

Adapter::~Adapter()
{
  //delete macro_model_;
}
//////////////////////////////////////////////////////////////////////////

int NewThread( pthread_t ID )
{
  am3::logger::NotImplemented(__FUNCTION__);
  return -1;
}
//////////////////////////////////////////////////////////////////////////
void ModelsContainer::AddModels( const Contract& modelContract, ModelsContainer& implementedModelsContainer,KnowledgeSource& kSource )
{
  am3::logger::NotImplemented(__FUNCTION__);
}
//////////////////////////////////////////////////////////////////////////
KnowledgeValues KnowledgeValues::GetModelDescripton()
{
    am3::logger::NotImplemented(__FUNCTION__);
    return KnowledgeValues();
}
//////////////////////////////////////////////////////////////////////////

void KnowledgeSource::GetMyKnowledge()
{
  am3::logger::NotImplemented(__FUNCTION__);
}
//////////////////////////////////////////////////////////////////////////


