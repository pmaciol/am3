/*!
 * \file prototype.h
 *
 * \author Piotr Maciol
 * \date 02.07.2012
 *
 * 
 */
#ifndef prototype_h__
#define prototype_h__
/*	include *********************************************************/

#include <iostream>
#include <vector>
#include "adapter.h"
#include "../adapter/adapter.h"
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/thread.hpp>


/*	using ***********************************************************/

using std::vector;

/*	extern ***********************************************************/

//extern KnowledgeSource designTimeKnowledge;

/*	classes *********************************************************/

class KBSThreadHandler
{
public:
  void operator()()
  {
    boost::posix_time::seconds workTime(5);  

    std::cout << "Worker: running" << std::endl;  

    // Pretend to do something useful...  
    boost::this_thread::sleep(workTime);  
    std::cout << "Worker: running" << std::endl;
    boost::this_thread::sleep(workTime);  
    std::cout << "Worker: finished" << std::endl;  
  }
};


class Manager
{
protected:
  // Simpler model - only one macro model. More complicated problem needs more macro models - the same domain or with boundaries
  am3::adapter::Adapter* macro_adapter_;
  // vector<KnowledgeSource> knowledge_sources_; //Should be stored here? Or in external module, like a implementedModelsContainer and modelExternalDescripton?
  KnowledgeSource* design_time_knowledge_;
  bool is_adapter_set_;

public:
  Manager();
  virtual void SetMacroAdapter( am3::adapter::Adapter* adapter);

  void SetAdapter( am3::adapter::Adapter* adapter );

  virtual void Validate();
  virtual void Run();
  bool Is_adapter_set() const { return is_adapter_set_; }
  //   macro_adapter_->Run();
  
};

//////////////////////////////////////////////////////////////////////////

class Simulation
{
protected:
  Manager* manager_;
  bool is_manager_set_;

public:
  Simulation();
  bool Is_manager_set() const { return is_manager_set_; }
  virtual void SetManager(Manager* manager);
  virtual void Validate();
  virtual void Run();
  virtual void PostProcess(){};
};
#endif // prototype_h__
