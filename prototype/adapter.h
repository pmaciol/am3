/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/
#ifndef adapter_h__
#define adapter_h__
/*	include *********************************************************/

#include <string>
#include <vector>
#include "../databus/dataStructures.h"
#include "../softwareAdina/adinaFControls.h"
#include "../models/point.h"
//#include "../softwareAdina/adinaDataInterface.h"

/*	using ***********************************************************/


/*	definitions *********************************************************/

typedef int pthread_t ;
int NewThread(pthread_t ID);

/*	classes *********************************************************/

//////////////////////////////////////////////////////////////////////////
//"Contract" defines needs and possibilities of model. Contracts could be verified for its compatibility
class Contract
{
public:
  bool CheckInput(const Contract& input);
  bool CheckOutput(const Contract& output);

};
//////////////////////////////////////////////////////////////////////////

// Rules
class KnowledgeSource
{
public:
  //Just for mark at this moment. I have not idea how it should work yet
  void GetMyKnowledge();
};
//////////////////////////////////////////////////////////////////////////
// Variables values - specific configurations of actual state
class KnowledgeValues
{
public:
  KnowledgeValues GetModelDescripton();
};
//////////////////////////////////////////////////////////////////////////
//Container with models. Active models in adapter, maybe in other places?
class ModelsContainer
{
public:
//  Gets model which are:
//    - implemented
//    - compatible
//    - suitable due to knowledge
//  In theory, it should be split to two methods: ChoosePossible(modelContract, implementedModelsContainer) and ChooseSuitable(kSource), but one method could be better optimized.
  void AddModels(const Contract& modelContract, ModelsContainer& implementedModelsContainer,KnowledgeSource& kSource);
};

//////////////////////////////////////////////////////////////////////////
//Container with implemented and available models. Linked from other, alternative modules
// extern ModelsContainer implementedModelsContainer;
// extern KnowledgeValues modelExternalDescripton;
//////////////////////////////////////////////////////////////////////////

class Adapter
{
protected:
  ModelsContainer available_fine_models_;
  am3::model::ModelAdina *macro_model_;
  //am3::model::ModelAdina macro_model_;
  KnowledgeSource runtime_knowledge_;
public:
  //Macroscale model have to be initialized here. Adapter type is fixed with one macroscale model's type, cannot be used with other.
  Adapter();
  virtual ~Adapter();
  // Design time
  // Basing on given KnowledgeSource, choses available models
  virtual void AddFineModels(KnowledgeSource& kSource);
  virtual void Validate();
  virtual void Run();
  virtual am3::model::common::Point ActualPoint(){return am3::model::common::Point();}; 
  //Runtime
  //Responding to the request for material data
 // virtual double DoubleData(const int dataPositionInVector,const State* state); 
};


#endif // adapter_h__
