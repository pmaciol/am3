
include_directories (../)
add_executable(am3prototype prototype.cpp prototype.h adapter.h adapter.cpp main.cpp)

target_link_libraries(am3prototype am3adapterIonExchange am3models am3logger am3softwareAdina am3adinaServer am3common am3adapter am3basicModels am3knowledgeRebit am3adinaServer  am3modelAdinaSimple)

SET(CMAKE_EXE_LINKER_FLAGS_DEBUG /NODEFAULTLIB:\"LIBCMTD.lib;libcpmtd.lib\")
SET(CMAKE_EXE_LINKER_FLAGS_RELEASE /NODEFAULTLIB:\"libcpmt.lib\")