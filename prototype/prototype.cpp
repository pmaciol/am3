/********************************************************************
	author:		Piotr Maciol
	
	purpose:	
*********************************************************************/

/*	include *********************************************************/

#include <assert.h>
#include "prototype.h"
#include "../logger/logger.h"

/*	using ***********************************************************/



/*	classes *********************************************************/

void Simulation::SetManager(Manager* manager)
{
  am3::logger::Starting("simulation design");
    if (manager->Is_adapter_set())
    {
      manager_=manager; //Simulation type is chosen here
      is_manager_set_=true;
    }
    else
    {
      am3::logger::log<am3::logger::SEV_CRITICAL>(am3::logger::LOG_MANAGER,"Manager is not initialized");  
    }
  am3::logger::Stopping("simulation design");
}

void Simulation::Validate()
{
  am3::logger::Starting("simulation validation");
    manager_->Validate();
  am3::logger::Stopping("validation finished");
}

void Simulation::Run()
{
  am3::logger::Starting("simulation run");
    manager_->Run();
  am3::logger::Stopping("simulation run");
}

Simulation::Simulation()
{
  is_manager_set_=0;
}

//////////////////////////////////////////////////////////////////////////
void Manager::SetMacroAdapter(  am3::adapter::Adapter* adapter)
{
  am3::logger::Starting("manager macro model settings");
    SetAdapter(adapter);
//    design_time_knowledge_->GetMyKnowledge(); //ticket 29
//    macro_adapter_->AddFineModels(*design_time_knowledge_); //ticket 31 // Sends knowledge sources collection to init "First Level Below Macro", M+1; Todo - were KS's come from?
  am3::logger::Stopping("manager macro model settings");
}

void Manager::Validate()
{
 am3::logger::Starting("manager validation");
//   macro_adapter_->Validate();
 am3::logger::Stopping("manager validation");
}

void Manager::Run()
{
  am3::logger::Starting("manager run");
    macro_adapter_->Run();
  am3::logger::Stopping("manager run");
}

Manager::Manager()
{
  am3::logger::TestingMsg("Copying global designTimeKnowledge to manager");
//    design_time_knowledge_=&designTimeKnowledge; 
    is_adapter_set_=0;
}

void Manager::SetAdapter( am3::adapter::Adapter* adapter )
{
	if (adapter!=nullptr)
	{
		is_adapter_set_=true;
		macro_adapter_=adapter;
	} 
	else
	{
		am3::logger::log<am3::logger::SEV_CRITICAL>(am3::logger::LOG_MANAGER,"Adapter is not initialized");
	}
}



