//*****************************************************
//
// Dieses file enthaelt alle Definitionen, die
// von MatCalc 2.0 und der mc_core.dll verwendet
// werden.
//
// (C) Ernst Kozeschnik
//
//*****************************************************

//*****************************************************
// Uebergabestrukturen
//*****************************************************

#ifndef mc_defines_h
#define mc_defines_h

const double const_pi=3.1415926535897932384626433832795028841971693993751;
const double const_NA=6.02214199e23;
const double const_kB=1.3806503e-23;
const double const_Rg=const_NA*const_kB;
#define MC_FALSE false
#define MC_HANDLE int
#define MC_BOOL bool


class TDMGRElementData 
{
public:
	char name[32];
	char def_ref_state[32];
	double mass;
	double enthalpie;
	double entropie;
	double x0;                    //Gesamtgehalt
	double x0p;                   //Gesamtgehalt in wt%
	int IsSelected;
	int OldSelected;  //fuer Ueberpruefung->Aenderung der Auswahl
public:
	TDMGRElementData()
	{
		name[0] = 0;
		def_ref_state[0] = 0;
		mass = 0.0;
		enthalpie = 0.0;
		entropie = 0.0;
		x0 = 0.0;
		x0p = 0.0;
		IsSelected = MC_FALSE;
		OldSelected = MC_FALSE;
	}
};

class TDMGRPhaseData 
{
public:
	int NameIndex;			//Index auf PhasenName
	int ConstIndex;			//Index auf Constituents in Constituentsarray
	char name[32];			//Phasenname
	char constituents[400];	//Constituents
	char datatype[32];  //Dataype aus TDB-File
	int NumSL;										//Anzahl Sublattices
	double Sites[8];			 		 			//Anzahl Plaetze auf Sublattices
	double p;         //Magnetic Parameter
	int IsSelected;
	int OldSelected;
	int IsPossible;
	int disordered_index;	//gibt's disordered part?
	int disorder_type;	//welches order-disorder model?
	char phase_description[400];	//short description of phase
	int recommended_phase_relevance;	//0 .. seldom used, 1 .. sometimes used, 2 .. often used

public:
	TDMGRPhaseData()
	{
		NameIndex = -1;
		ConstIndex = -1;
		name[0] = 0;
		constituents[0] = 0;
		datatype[0] = 0;
		NumSL = 0;
		p=0.0;
		IsSelected = MC_FALSE;
		OldSelected = MC_FALSE;
		IsPossible = MC_FALSE;
		disordered_index = -1;
		disorder_type = 0;	//DT_REGULAR_Y
		phase_description[0] = 0;
		recommended_phase_relevance = 0;	//lowest level
	}
};

class TDMGRCompSet 
{
public:
	char PhaseName[32];
	char CompSetString[200];
	int CompSetIndex;
public:
	TDMGRCompSet()
	{
		PhaseName[0] = 0;
		CompSetString[0] = 0;
		CompSetIndex = -1;
	}
};

class CMPS_ParamData
{
public:
	int type_code;
	int step_type;
	double start;
	double stop;
	int intervals;
	int table_handle;
public:
	CMPS_ParamData()
	{
		type_code = -1;
		step_type = -1;
		start = 0.0;
		stop = 0.0;
		intervals = 0;
		table_handle = -1;
	}
};


//*****************************************************
// licensing

#define LT_FREE					0
#define LT_FULL					10


#define LT2_UNLIMITED			1
#define LT2_SINGLE_UNBOUND		10
#define LT2_HARDWARE_BOUND		50
#define LT2_FLOATING			100


#define LC_LICENSE_OK					1
#define LC_INVALID_HARDWARE_ID			10
#define LC_LICENSE_HAS_EXPIRED			20

//info
#define LIT_LICENCE_FILE_PATH				1
#define LIT_LICENCE_TYPE_1					10
#define LIT_LICENCE_TYPE_2					11
#define LIT_LICENSE_ID						50
#define LIT_USER_NAME						100
#define LIT_USER_ORGANIZATION				101
#define LIT_VALID_UNTIL						110
#define LIT_UPGRADE_UNTIL					115
#define LIT_MAINTENANCE_UNTIL				116
#define LIT_ENABLED_MODULES					200
#define LIT_ENABLED_MODULES_UNFORMATTED		201

//module definitions ...
#define MOD_CORE						0x80000000	//should be always true
#define MOD_MULTI_COMP_THERMODYNAMICS	0x40000000	//demo version gives zero, only full version
#define MOD_PRECIPITATION_KINETICS		0x00000001	//precipitation kinetics
#define MOD_CAFE_SIMULATION				0x00000010	//discrete microstructure simulation
#define MOD_MONTE_CARLO					0x00000011	//monte carlo simulation


//modules (is index in array !!!)
#define MC_MODULE_CORE		0
#define MC_MODULE_CAFE		1
#define MC_MODULE_MONTE		2

//Autosave
#define AST_NONE			0
#define AST_BINARY			1
#define AST_WORKSPACE		5

//*****************************************************
//kinetic store

#define KST_NONE		0
#define KST_LIN			1
#define KST_LOG			10
#define KST_COUNT		20
#define KST_AUTO		100
			

//*****************************************************
// Windows Messages, die an den Anwendungsrahmen
// geschickt werden.
//*****************************************************

//non-windows (e.g. Qt)
#ifdef MY_QT
#define WM_USER_MC QEvent::User
#else
#define WM_USER_MC 1024
#endif

#define MCCM_READY						WM_USER_MC + 10	//MCCORE ist fertig/bereit
#define MCCM_STATUSCHANGED				WM_USER_MC + 11	//irgendetwas ist anders
#define MCCM_SENDINGINFO				WM_USER_MC + 20	//wird gesendet, wenn auf die Informationsanfrage geantwortet wird
#define MCCM_MESSAGE_BEEP				WM_USER_MC + 21	//beep Signal

#define MCCM_CHECKCOMPOSITION			WM_USER_MC + 50	//Ueberpruefung beim Linken -> Zusammensetzung nicht eingegeben
#define MCCM_BUFFERDATACHANGED			WM_USER_MC + 51	//wird gesendet, wenn sich der Inhalt eines Buffers geaendert hat (MC_core)
#define MCCM_BUFFER_ADDED_REMOVED		WM_USER_MC + 52	//wird gesendet, wenn sich der Inhalt eines Buffers geaendert hat (MC_core)
#define MCCM_UPDATE_BUFFER_STATE_SEL	WM_USER_MC + 53	//wird gesendet, wenn Buffer/State selection geaendert (MC_core)
#define MCCM_SIMBUFFERDATACHANGED		WM_USER_MC + 60	//wird gesendet, wenn sich der Inhalt eines Buffers geaendert hat (MC_real)
#define MCCM_STATIONARYDATACHANGED		WM_USER_MC + 61	//wird gesendet, wenn sich der Inhalt nach einer stationary geaendert hat (MC_real)
#define MCCM_BUFFERDATAAPPEND			WM_USER_MC + 62	//wird gesendet, wenn sich der Inhalt eines Buffers geaendert hat (MC_core)
#define MCCM_SIMBUFFERDATAAPPEND		WM_USER_MC + 63	//wird gesendet, wenn sich der Inhalt eines Buffers geaendert hat (MC_real)
#define MCCM_CAFD_1D_DATACHANGED		WM_USER_MC + 65	//wird gesendet, wenn sich der Inhalt fuer 1-D CAFD Fenster geaendert hat (MC_fdtr)
#define MCCM_CAFD_2D_DATACHANGED		WM_USER_MC + 66	//wird gesendet, wenn sich der Inhalt fuer 2-D CAFD Fenster geaendert hat (MC_fdtr)
#define MCCM_SIMULATIONFINISHED			WM_USER_MC + 70	//wird gesendet, wenn sich der Inhalt eines Buffers geaendert hat (MC_real)
#define MCCM_STATIONARYCALCFINISHED		WM_USER_MC + 71	//wird gesendet, wenn sich der Inhalt eines Buffers geaendert hat (MC_real)
#define MCCM_CAFD_SIMULATIONFINISHED	WM_USER_MC + 72	//wird gesendet, wenn sich der Inhalt eines Buffers geaendert hat (MC_fdtr)
#define MCCM_TABLEDATACHANGED			WM_USER_MC + 80	//wird gesendet, wenn sich table geaendert hat (MC_core)

#define MCCM_WRITEOUT					WM_USER_MC + 100	//in Outputfenster schreiben
#define MCCM_WRITESTATUS				WM_USER_MC + 101	//in Statuszeile schreiben
#define MCCM_WRITEOUTCONSOLE			WM_USER_MC + 102	//in Konsolen-fenster schreiben
#define MCCM_READFROMCONSOLE			WM_USER_MC + 103	//von Konsolen-fenster lesen
#define MCCM_WRITEOUTPROJECT			WM_USER_MC + 105	//in Project-Outputfenster schreiben
#define MCCM_UPDATEPROGRESS				WM_USER_MC + 110	//Progress-Window update
#define MCCM_UPDATEMEMORYUSAGE			WM_USER_MC + 111	//Memory-Usage update
#define MCCM_RESETPROGRESS				WM_USER_MC + 112	//Progress-Window reset
#define MCCM_UPDATEPROGRESSINDICATOR	WM_USER_MC + 113	//Progress-Indicator update
#define MCCM_SETPROGRESS				WM_USER_MC + 114	//Progress-Window set
#define MCCM_SET_HELP_TEXT				WM_USER_MC + 115	//help-text zu variable ausgeben
#define MCCM_UPDATEQACTIONSTATES		WM_USER_MC + 130	//alle QAction aktualisieren
#define MCCM_UPDATEOPTIONSWINDOW		WM_USER_MC + 131	//Options-Window aktualisieren
#define MCCM_ECHOERROR					WM_USER_MC + 150	//Error aufgetreten ...
#define MCCM_WAITINGONEVENT				WM_USER_MC + 151	//SysCalcDebug ...
#define MCCM_ASKUSER					WM_USER_MC + 152	//User interaction ...

#define MCCM_WORKINGDIRCHANGED			WM_USER_MC + 160	//current directory has changed

#define MCCM_CONFIRMUSERBREAK			WM_USER_MC + 200	//soll der Userbreak wirklich durchgefuehrt werden?
#define MCCM_CONFIRMERRORBREAK			WM_USER_MC + 201	//Fehler bei Step: soll abgebrochen werden?
#define MCCM_ASKCHANGESTEPDIR			WM_USER_MC + 205	//in andere Richtung weiter-steppen?
#define MCCM_EDITNEXTITEM				WM_USER_MC + 210	//fahre mit edit fort bei naechstem item (GUI)
#define MCCM_PROCESS_COMMAND			WM_USER_MC + 215	//process command for script Verarbeitung aus Threads (GUI)
#define MCCM_RUN_SCRIPT_FINISHED		WM_USER_MC + 217	//script finished for script Verarbeitung aus Threads (GUI)

#define MCCM_OPENDATABASEFINISHED		WM_USER_MC + 1000	//DoOpenDatabase() Thread finished
#define MCCM_OPENDIFFDATAFINISHED		WM_USER_MC + 1001	//DoOpenDatabase() Thread finished
#define MCCM_OPENPHYSDATAFINISHED		WM_USER_MC + 1002	//DoOpenDatabase() Thread finished
#define MCCM_READDATABASEFINISHED		WM_USER_MC + 1003	//DoReadDatabase() Thread finished
#define MCCM_READDIFFDATAFINISHED		WM_USER_MC + 1004	//DoReadDatabase() Thread finished
#define MCCM_READPHYSDATAFINISHED		WM_USER_MC + 1005	//DoReadDatabase() Thread finished
#define MCCM_FILEREADFINISHED			WM_USER_MC + 1006	//DoReadFile() Thread finished
#define MCCM_CALCEQUILIBFINISHED		WM_USER_MC + 1020	//CalcEquilibrium() Thread finished
#define MCCM_STEPEQUILIBFINISHED		WM_USER_MC + 1021	//CalcEquilibriumStep() Thread finished
#define MCCM_CALCSOLTEMPFINISHED		WM_USER_MC + 1022	//CalcSolubilityTemperature() Thread finished
#define MCCM_CALCT0TEMPFINISHED			WM_USER_MC + 1023	//CalcT0Temperature() Thread finished
#define MCCM_CALCTIELINESFINISHED		WM_USER_MC + 1024	//CalcTieLines() Thread finished
#define MCCM_SCHEILCALCFINISHED			WM_USER_MC + 1025	//CalcScheilReaction() Thread finished
#define MCCM_CALCMULTIPARAMSTEPFINISHED	WM_USER_MC + 1026	//CalcMultiParamStep() Thread finished
#define MCCM_CALCKINETICSTEPFINISHED	WM_USER_MC + 1030	//CalcKineticTimeStep() Thread finished
#define MCCM_CALCKINETICTTPDIAGRAMFINISHED	WM_USER_MC + 1031	//CalcKineticTTPDiagram() Thread finished
#define MCCM_GETBUFFERVARSFINISHED		WM_USER_MC + 1050	//GetBufferVariables() Thread finished
#define MCCM_GETSIMBUFFERVARSFINISHED	WM_USER_MC + 1051	//GetBufferVariables() Thread finished

#define MCCM_REMOVE_PLOT_WITH_ID        WM_USER_MC + 1100   //remove plot (from RMB menu of plot)

//grid simulation ...
#define MCCM_STARTSIMULATIONFINISHED	WM_USER_MC + 2000	//StartSimulation() Thread finished

//monte carlo ...
#define MCCM_UPDATEMONTE				WM_USER_MC + 3000	//update monte windows

//*****************************************************
// Topics fuer Info-Anfragen
//*****************************************************

#define TOPIC_PHASES_GENERAL_INFO			10
#define TOPIC_PHASES_SUMMARY				11
#define TOPIC_PHASE_DATABASE_INFO			12
#define TOPIC_SYMBOL_DATABASE_INFO			13
#define TOPIC_CHEM_POTS_GENERAL				14
#define TOPIC_PHASE_NNDATABASE_INFO			15
#define TOPIC_VARIABLES						20
#define TOPIC_BUFFER_RESULTS				21
#define TOPIC_DIFFUSION_DATA				22
#define TOPIC_PARTICLE_DISTRIBUTION			30
#define TOPIC_NUCLEUS_DISTRIBUTION			31
#define TOPIC_SFK_TRAP_PART_TABLE			40
#define TOPIC_DEBUG_DX_ARRAYS				100
#define TOPIC_DEBUG_JACO_MATRIX				101
#define TOPIC_DEBUG_GE_DERIVATIVES			102
#define TOPIC_DEBUG_DFM_DERIVATIVES			103
#define TOPIC_DEBUG_MU_DERIVATIVES			104
#define TOPIC_DEBUG_FE_DERIVATIVES			105
#define TOPIC_DEBUG_KINETIC_MATRICES		110


//*****************************************************
// Globale Variablen
//*****************************************************

#define MCGV_SMALL_VALUE						10			//double
#define MCGV_JACO_ANALYTICAL					20			//MC_BOOL
#define MCGV_LSM_METHODE						21			//int
#define MCGV_REDUCE_PB							22			//MC_BOOL
#define MCGV_REDUCE_SFB							23			//MC_BOOL
#define MCGV_REDUCE_SFESCALE					24			//MC_BOOL
#define MCGV_USE_EQUI_SYSTEM_FOR_CP				25			//MC_BOOL
#define MCGV_USE_THERMOCALC_COMPATIBILITY		26			//MC_BOOL
#define MCGV_COMPOSITION_TYPE					100			//int
#define MCGV_STEPGLOBALELEMENTCONTENT			101			//MC_BOOL
#define MCGV_STEPPARAEQUILIBCONSTRAINT			102			//MC_BOOL
#define MCGV_CURRENT_STEPSTART					200			//double
#define MCGV_CURRENT_STEPSTOP					201			//double
#define MCGV_CURRENT_STEPWIDTH					202			//double
#define MCGV_CURRENT_ELEMENT_IN_STEP			203			//int
#define MCGV_CURRENT_STEP_TYPE					204			//int
#define MCGV_ELEMENT_STEP_TEMPERATURE			205			//double
#define MCGV_CURRENT_SOLPHASE_IN_STEP			206			//int
#define MCGV_CURRENT_DEPT0PHASE_IN_STEP			207			//int
#define MCGV_CURRENT_PARENTT0PHASE_IN_STEP		208			//int
#define MCGV_CURRENT_STEPT0OFFSET_IN_STEP		209			//double
#define MCGV_MAX_DT_IN_SOLTEMP_STEP				210			//double
#define MCGV_CURRENT_STEP_FLAGS					215			//int
#define MCGV_SCHEIL_STEPSTART					300			//double
#define MCGV_SCHEIL_STEPSTOP					301			//double
#define MCGV_SCHEIL_STEPWIDTH					302			//double
#define MCGV_SCHEIL_DEPENDENT_PHASE				305			//handle
#define MCGV_SCHEIL_MIN_LIQUID_FRACTION			306			//double
#define MCGV_SCHEIL_BACK_DIFFUSION				310			//handle
#define MCGV_CURRENT_SEARCH_BOUNDARY_TYPE		400			//int
#define MCGV_CURRENT_SEARCH_TARGET_PHASE		405			//int
#define MCGV_CURRENT_SEARCH_PARENT_PHASE		406			//int
#define MCGV_CURRENT_SEARCH_ELEMENT				410			//int
#define MCGV_CURRENT_SEARCH_TEMPERATURE		411			//double
#define MCGV_CURRENT_SEARCH_T_IN_C				412			//int
#define MCGV_CURRENT_SEARCH_DFM_OFFSET			420			//double
#define MCGV_CURRENT_SEARCH_FORCE_COMP			421			//int

//Kinetik
#define MCGV_KIN_SIM_END_TIME					1000			//double
#define MCGV_KIN_SIM_END_TEMP					1001			//double
#define MCGV_KIN_MANUAL_TIME_STEP_WIDTH		1005			//double
#define MCGV_KIN_TIME_STEP_CONTROL_TYPE		1006			//int
#define MCGV_KIN_INITIAL_TIME_STEP				1007			//double
#define MCGV_KIN_MAXIMUM_TIME_STEP				1008			//double
#define MCGV_KIN_T_CONTROL_TYPE					1010			//int
#define MCGV_KIN_ISOTHERMAL_T					1015			//double
#define MCGV_KIN_T_HEAT_TREATMENT_INDEX		1017			//int
#define MCGV_KIN_GLOBAL_EPS_DOT					1018			//handle
#define MCGV_KIN_UPDATE_EVERY					1020			//int
#define MCGV_KIN_STORE_EVERY					1021			//int
#define MCGV_KIN_LIN_STORE_EVERY				1022			//double
#define MCGV_KIN_LOG_STORE_EVERY				1023			//double
#define MCGV_KIN_STORE_TYPE						1024			//int
#define MCGV_KIN_T_IN_C							1025			//int
#define MCGV_KINETIC_FLAGS						1030			//int
#define MCGV_KIN_LOAD_STATE						1040			//int
#define MCGV_KIN_MANUAL_TIME_STEP_INCREASE		1041			//double
#define MCGV_KIN_CONTI_T_DOT					1042			//double
#define MCGV_KIN_MAX_T_STEP_WIDTH				1045			//double
//#define MCGV_KIN_THERMODYNAMIC_MODEL			1050			//int
#define MCGV_NUCLEATION_ENHANCEMENT				1060			//double
//#define MCGV_KIN_TIME_INTEGR_MODEL				1065			//int

#define MCGV_KIN_MASS_BALANCE_TOLERANCE		1070			//double
#define MCGV_KIN_STOP_NUCLEATION_FRACTION		1071			//double

#define MCGV_FIRST_MAX_NUCL_DENSITY				1100			//double
#define MCGV_REL_MATRIX_CHANGE_NUCL				1101			//double
#define MCGV_NUCL_DENSITY_INC_FACT				1102			//double
#define MCGV_REL_RADIUS_CHANGE_GROWTH			1110			//double
#define MCGV_REL_RADIUS_CHANGE_SHRINK			1111			//double
#define MCGV_REL_VOL_CHANGE_GROWTH				1115			//double
#define MCGV_REL_VOL_CHANGE_SHRINK				1116			//double
#define MCGV_REL_VA_SITE_FRACTION_CHANGE		1117			//double
#define MCGV_REL_DISLOCATION_DENSITY_CHANGE		1118			//double
#define MCGV_REL_SUBGRAIN_DIAMETER_CHANGE		1119			//double
#define MCGV_REL_GRAIN_DIAMETER_CHANGE			1120			//double
#define MCGV_REL_COMP_CHANGE_IN_PREC			1125			//double
#define MCGV_REL_COMP_CHANGE_IN_MATRIX			1126			//double
#define MCGV_REL_COMP_CHANGE_BY_COMP_CONTROL	1127			//double
//#define MCGV_REL_HEUN_GRADIENT_CHANGE			1130			//double
#define MCGV_REL_DFM_CHANGE						1135			//double
//#define MCGV_MIN_REDUCED_DRIVING_FORCE			1136			//double
//#define MCGV_USE_INDEPENDENT_VARIABLES			1140			//int
#define MCGV_KINETIC_PRECISION					1141			//double
//#define MCGV_ADVANCED_VARIABLE_INTEGRATION		1145			//int
#define MCGV_DFM_COMPENSATION_RATE				1143			//double
#define MCGV_WEIGHTED_IPDCF						1144			//int
#define MCGV_KIN_ACCELERATOR					1145			//int
#define MCGV_PREDICTED_PREC_PARAMETERS			1148			//int
#define MCGV_USE_OSCILLATION_DAMPING			1149			//int

#define MCGV_DEBUG_KINETIC_STEPSIZE_2DERIVS		1150			//double
#define MCGV_DEBUG_KINETIC_STEPSIZE_MAT_CHANGE	1151			//double
#define MCGV_DEBUG_KINETIC_ARRAY_INDEX			1152			//int
#define MCGV_DEBUG_KINETIC_MATRIX_FLAGS			1153			//int

#define MCGV_TTP_UPPER_TEMPERATURE				1200			//double
#define MCGV_TTP_LOWER_TEMPERATURE				1201			//double
#define MCGV_TTP_MAX_TIME						1202			//double
#define MCGV_TTP_T_CONTROL_TYPE					1205			//int
#define MCGV_TTP_DELTA_TEMPERATURE				1210			//double
#define MCGV_TTP_MIN_TEMPERATURE_GRAD			1220			//double
#define MCGV_TTP_MAX_TEMPERATURE_GRAD			1221			//double
#define MCGV_TTP_DELTA_TEMPERATURE_GRAD_FACT	1222			//double
#define MCGV_TTP_STOP_IF_N_DECR_FACT			1225			//double

// ********** Composition Types ******************

#define CT_MOLE_FRACTION			1
#define CT_WEIGHT_FRACTION			2
#define CT_WEIGHT_PERCENT			3
#define CT_U_FRACTION				4

//*****************************************************
// Flags fuer Step
//*****************************************************

#define CES_TYPEEXTERNAL				0
#define CES_TYPETEMPERATURE				1
#define CES_TYPEELEMENTCONTENT			2
//#define CES_TYPEPRESSURE				3
#define CES_TYPEDEFECTDENSITY			3
#define CES_TYPESOLUBILITYTEMP			4
#define CES_TYPETEMPWITHOUTEQUILIB		5
#define CES_TYPET0TEMPERATURE			6
#define CES_TYPESCHEILCALC				100
#define CES_TYPEKINETICCALC				200

#define SPB_TYPE_TEMPERATURE			10
#define SPB_TYPE_ELEMENT				11
#define SPB_TYPE_T0_TEMPERATURE			20

#define KINETIC_ORTHO_EQUILIB_STATE		0
#define KINETIC_PARA_EQUILIB_STATE		1
#define CES_STATE_BEFORE_STEP_COMMAND	0
#define CES_FIRST_VALID_STEP_EQILIB		1
#define CES_STATE_AFTER_LAST_STEP		2
#define CES_STATE_BEFORE_LAST_STEP		3
#define CES_STATE_AFTER_LAST_SUBSTEP	4
#define CES_STATE_BEFORE_LAST_SUBSTEP	5

#define CES_APPENDTOEXISTINGDATA		0x00000001		//Werte werden an existierenden Buffer angehaengt
#define CES_ELEMENTSINWEIGHTPERCENT		0x00000002		//Die Elementgehalte fuer Step in Gewichtsprozent
#define CES_TEMPERATUREINC				0x00000004		//Temperature als C interpretieren
#define CES_APPENDWITHOUTLOAD			0x00000008		//Werte werden an existierenden Buffer angehaengt, aber letzter Zustand nicht geladen
#define CES_VARYELEMENTINPARAEQUILIB	0x00000020		//Elementgehalt in paraequilib constraints aendern
#define CES_FORCETOCOMPFROMPARENT		0x00000040		//Force composition fuer T0 step
#define CES_APPENDINVERSEORDER			0x00000100		//Werte in umgekehrter Reihenfolge speichern
#define CES_INCLUDEHEADERINFILE			0x00000200		//Soll header in file inkludiert werden?
#define CES_SCHEILCALCSOLTEMP			0x00000400		//fuer Scheil calc
#define CES_RANGE_TYPE_LOG				0x00000800		//logarithmic increments?


//*****************************************************
// Flags fuer Equilib calc output
//*****************************************************

//QuietStatusFlags bis 0x0000000F
#define EO_QUIET						0x00010000		//keine Meldungen
#define EO_STATUS_ONLY					0x00020000		//nur Status
#define EO_NORMAL						0x00040000		//normal lang
#define EO_SHORT						0x00080000		//kurze ausgabe

#define EO_NO_AUTO_RESET_ON_MAXITER		0x00000100		//...
#define EO_SAVE_CHECK_COMPOSITION		0x00001000		//bei SetCompositionFromCompArray
#define EO_ECHO_RESET_COMPOSITION		0x00002000		//bei SetSFFromChemPot

//*****************************************************
// Flags fuer PhasesGeneral output
//*****************************************************

#define PG_ELEMENTS_IN_WEIGHT_SCALE		0x00000001
#define PG_ELEMENTS_IN_PERCENT			0x00000002
#define PG_ELEMENTS_IN_U_FRACTION		0x00000004
#define PG_ELEMENTS_IN_UC_FRACTION		0x00000008

#define PG_PAINT_SHOW_CELL_BOUNDARY		0x00000010

#define PG_PHASES_IN_WEIGHT_SCALE		0x00000100
#define PG_PHASES_IN_VOLUME_SCALE		0x00000200
#define PG_PHASES_IN_PERCENT			0x00000400

#define PG_PHASES_IN_U_MOLES			0x00001000
#define PG_COMPOSITION_ONLY				0x00002000
#define PG_MASS_BALANCE_DIFF			0x00004000

#define PG_DISPLAY_RELATIVE_CHANGE		0x00100000
#define PG_DISPLAY_FIRST_DERIVATIVE		0x01000000
#define PG_DISPLAY_DERIVE_REL_CHANGE	0x02000000
#define PG_DISPLAY_SECOND_DERIVATIVE	0x04000000

#define PG_SHOW_NUMERICAL_DERIVS		0x08000000


//show displacement types
#define PV_SHOW_INITIAL_COORDS				0
#define PV_SHOW_CURRENT_COORDS				10
#define PV_SHOW_BOTH_COORDS					20


//*****************************************************
// Flags
//*****************************************************

//DataMgr (unteren 2 Byte)
#define DBMGR_PROJECTDEFINED	0x00000001
#define DBMGR_EQUIDATABASEOPEN	0x00000002
#define DBMGR_EQUIDATABASEREAD	0x00000004
#define DBMGR_DIFFDATABASEOPEN	0x00000008
#define DBMGR_DIFFDATABASEREAD	0x00000010
#define DBMGR_PHYSDATABASEOPEN	0x00000020
#define DBMGR_PHYSDATABASEREAD	0x00000040
#define DBMGR_PHYSDATADEFINED	0x00000080
#define DBMGR_ELEMENTSDEFINED	0x00000100
#define DBMGR_PHASESDEFINED		0x00000200

//Gibbs (oberen 2 Byte)
#define GIBBS_SYSTEMLINKED				0x00010000
#define GIBBS_SYSTEMUPDATED				0x00020000   //wenn gerade gelinkt und noch nicht gerechnet
#define GIBBS_KINETIKCONTINUE			0x00040000   //gesetzt, wenn Continue gedrueckt werden darf
#define GIBBS_SORTRESARRAY				0x00100000   //Soll Progress sortiert werden?
#define GIBBS_STORERESULTLINE           0x00200000   //Soll AddResultLine wirklich durchgefuehrt werden?
#define GIBBS_SYSCALCDEBUG				0x00400000	 //Unterbrechung beim Rechnen

//Phases
#define FLAG_PHASEISSUSPENDED			0x00000001
#define FLAG_PHASEISSTOCHIOMETRIC		0x00000002
#define FLAG_PHASEISSELECTED			0x00000004
#define FLAG_USERSUSPENDED				0x00000008
#define FLAG_PHASEWITHMAGNETIC			0x00000010   //Phase mit magnetischem Anteil
#define FLAG_PHASEISFIXED 				0x00000020   //Phasenstatus FIXED
#define FLAG_PHASEISDORMANT				0x00000040	//Phasenstatus DORMANT
#define FLAG_PHASEISMATRIXPHASE			0x00000080	//Phase ist MatrixPhase
#define FLAG_KINMATRIX					0x00000100	//Ausscheidungstyp: Matrix
#define FLAG_KINPRECIPITATE				0x00000200	//Ausscheidungstyp: Praezipitat
#define FLAG_PHASEISNEWCOMPOSITIONSET	0x00000400   //Phase ist als neues CompositionSet erzeugt worden
#define FLAG_PHASEISACTIVE				0x00000800   //Phase ist aktiv (ENTERED)
#define FLAG_PHASEFORSOLCALC			0x00001000   //fuer diese Phase wird Loeslichkeitstemp bestimmt
#define FLAG_PHASEISDISPERSED			0x00002000   //diese Phase is disperse Phase in Diffusion calc
#define FLAG_PHASEISFIXED_U				0x00004000   //Phasenstatus FIXED u-Phase Fraction
#define FLAG_PHASEISTEMPSUSPENDED		0x00008000   //Phasenstatus temporarily suspended
#define FLAG_PHASEFIXEDKEEPDFMZERO		0x00010000   //Bei fixed status wird dgm auf NULL gehalten
#define FLAG_PHASEISKINETIC				0x00020000   //Ausscheidungsphase
#define FLAG_PHASEISORDERED				0x00040000   //Geordnete Phase, d.h. besitzt disordered part (obsolete)
#define FLAG_GBBWITHOUTMAGNETIC			0x00080000   //GBB variables ohne magnetischem Anteil rechnen
#define FLAG_ENFORCE_MAJOR_CONST		0x00100000   //make sure major constituents are maintained in simulations


//PhasenStatus inaktiv
#define PHSTAT_OK							0
#define PHSTAT_CPDIFFER	  					1
#define PHSTAT_POSITIVEDFM					2

//IsFixedSolidPhase
#define FSP_SOLID			1
#define FSP_TRANS			2
#define FSP_TRANS_SOLID		3
#define FSP_KINETIC			10
#define FSP_INDEPENDENT		100		// for use in independent Gibbs node
//#define FSP_DEFECT			20

//for list phases (types)
#define SF_ALL					0x00000001
#define SF_MATRIX_PHASES		0x00000002
#define SF_PRECIPITATES			0x00000004

//chemical potentials type...
//quality for calculation in ascending order !!!
//everything better than CPT_DEFAULT (=0) delivers perfect chemPot
#define CPT_UNKNOWN				-1		//can certainly not calc chemicalc potential
#define CPT_DEFAULT				0		//use default (old) method. Delivers compound potential, but usually not correct
#define CPT_EQU_SYSTEM			2		//ok, but must be evaluated from solution of equation system
#define CPT_CAN_COMPLETE		5		//ok, but must be evaluated based on other potential (must calc first!), must be one more than CPT_DEFAULT
#define CPT_SINGLE_SL			7		//ok
#define CPT_VA_ON_SL			10		//ok
#define CPT_MULTI_VA_ON_SL		11		//ok
#define CPT_EL_ON_ALL_SL		50		//ok
#define CPT_IS_VACANCY			1000	//ok, element itself is vacancy. Per definitionen: mu(Va) = 0.0



//**************** Constituent *****************

#define CON_NOMAJORCONSTMARKED			0x0001
#define CON_JUSTMAJORCONSTITUENTS		0x0002
#define CON_WITHCOLONS						0x0004

#define CONSET_ONLYWITHPERCENTSIGN		0x0001

#define FET_NO_CONDITION				0
#define FET_FIXED_MF					1	//fixed mole fraction
#define FET_FIXED_WP					2	//fixed weight percent
#define FET_FIXED_UF					3	//fixed u-fraction

//**************** Richtung bei UpdateComposition *****************

#define UCDIR_FROMWTP			1
#define UCDIR_FROMAF			2

//***** Anpassung der Elementgehalte bei SetAmountOfElement **********

#define SAE_ADJUST_NOTHING			0	//nichts anpassen
#define SAE_ADJUST_REF_ELEMENT		1	//nur Referenz-Element anpassen
#define SAE_ADJUST_ALL_ELEMENTS		2	//danach alle Elemente relativ anpassen
#define SAE_ADJUST_ALL_BUT_ME		3	//alle andere Elemente relativ anpassen
#define SAE_ADJUST_INTERST_SUBST	5	//interstitial/substitutional eigen behandeln (Diffusionsrechnung)


//**************** Flags beim T0 Calc *****************

#define T0CALC_FIRSTCALCCOMP			0x00000001

//**************** Solid Transformation Types *****************

#define STT_CONSTR_EQUIL			1
#define STT_FULL_EQUIL				2
#define STT_MANUAL_RATIO			3
#define STT_MANUAL_AVRAMI			4
#define STT_MANUAL_KOIST_MAR		5

//*****************************************************
// FunctionManager Codes
//*****************************************************

#define DB_SYMBOLS							1
#define DB_DIFFUSION_SYMBOLS				2
#define DB_PHYSICAL_SYMBOLS					3
#define DB_PHASE_PARAMETERS					10
#define DB_PHASE_DIFFUSION_PARAMETERS		11
#define DB_PHASE_PHYSICAL_PARAMETERS		12

#define TFKT_CONSTITUTION			0x00000001	//Haette gerne die Funktion: G(BCC_A2, FE,MN:VA)
#define TFKT_POLYNOMDATA			0x00000002	//Die Polynomdaten
#define TFKT_ENABLED				0x00000004	//Enabled-flag der Funktion
#define TFKT_POLYNOMVALUES			0x00000008	//aktuelle Werte der Polynomdaten

//*****************************************************
// Database Manager Types
//*****************************************************

#define DBOC_EQUILIBRIUM		1
#define DBOC_DIFFUSION			2
#define DBOC_PHYSICAL			3

//Parser Codes

#define DBLT_DBDESCRIPTION		1
#define DBLT_DBELEMENT			2
#define DBLT_DBPHASE			3

//Parser Typen

#define TYPE_G					0
#define TYPE_L					1
#define TYPE_IS					3	//type internal stress
#define TYPE_OFFSET				5
#define TYPE_TC                 10
#define TYPE_BMAGN				20	// 
#define TYPE_HMVA				30	// enhalpy of vacancy formation
#define TYPE_SMVA				31	// entropy of vacancy formation
//#define TYPE_DEFECT_ENERGY		30
#define TYPE_MQ					50
#define TYPE_MF					51
#define TYPE_DQ					52
#define TYPE_DF					53
#define TYPE_VS					54
#define TYPE_CR					60	//vacancy wind factor

#define TYPE_SYMBOL				100
#define TYPE_DIFFSYMBOL			101
#define TYPE_PHYSSYMBOL			102

#define TYPE_FIRSTPHYSPARAM		10000		//erster physikalischer Parameter Index

//TermParser
#define TDTP_ERROR				0
#define TDTP_SYMBOL				1
#define TDTP_T0					2
#define TDTP_T1					3
#define TDTP_TLNT				4
#define TDTP_TN					5
#define TDTP_RTLN				6
#define TDTP_LN					7
#define TDTP_LOG				8
#define TDTP_EXP				9
#define TDTP_P1					10
#define TDTP_PT					11
#define TDTP_PT2				12
#define TDTP_RT					13
#define TDTP_LAST				100

// ThermFkt defines
#define DT_REGULAR_Y			0		// no disorder / order model
#define DT_DISORDER_ORD_DIS		10		// regular order-disorder contribution (prototypes: BCC_C2, FCC_L12, etc)
#define DT_DISORDER_NO_S		20		// disorder contribution without entropy in X (prototype: SIGMA)
#define DT_PARENT_PHASE			30		// used, for instance, for Cottrell atmospheres


//Fehlercodes
#define DBP_ERRGETLINE			100
#define DBP_ERRKEYWORD			101
#define DBP_ERRTOOLONGWORD		102
#define DBP_ERRPARSINGLINE		103
#define DBP_ERRTYPEDEFSEQ    	104
#define DBP_ERRTYPEDEFGES    	105
#define DBP_ERRPHASENOTFOUND	106


//MultiParameterStep
#define MPS_TEMPERATURE_C		100
#define MPS_TEMPERATURE_K		101

#define MPS_SYSTEM_COMP_X_FIRST	1000
#define MPS_PARA_COMP_X_FIRST	2000


#define MPST_LINEAR				1
#define MPST_LOGARITHMIC		2
#define MPST_TABLE_INDEX_FIRST	100

//Neural Network ...
#define NNP_ACC			1
#define NNP_GM			5
#define NNP_DC			10
#define NNP_DS			11

//transfer functions
#define AFT_TANSIG		1
#define AFT_LOGSIG		2
#define AFT_PURELIN		10


//****************************************
//KINETICS ...
//****************************************

// Ashby-Orowan Selection
#define AOT_MINIMUM				1
#define AOT_WEAK				5
#define AOT_STRONG				6


//Model (gibt's nicht mehr)
#define KM_CLASSICAL_NUCL_GROWTH		0
#define KM_SVOBODA_MODEL				10

//thermodynamic data from ...
#define TM_CALPHAD						1
#define TM_CALPHAD_MCA					2
#define TM_NEURAL_NETWORK				10

//Time Integration Modell
#define TIM_EULER_FORWARD				1
#define TIM_EULER_FORWARD_2				2
#define TIM_EULER_FORWARD_EEPFD			3	//estimated equilibrium phase fraction damping
#define TIM_HEUN_1_2					5
#define TIM_HEUN_PC						6
#define TIM_IMPLICIT					100
#define TIM_BULIRSCH_STOER				101

//Status
#define KS_NOT_INITIALIZED				0	//nicht initialisiert
#define KS_INITIALIZED					1	//initialisiert
#define KS_READY						2	//fertig zum Rechnen


//Surface tension, nucleation constant etc.
#define STT_NONE						0
#define STT_FROM_PHYSICAL_PARAMETER		1	//
#define STT_FROM_GLOBAL_TABLE			2	//
#define STT_FIXED_VALUE					10	//

//Temperature control
#define STT_ISOTHERMAL					0
#define STT_CONTINUOUS					2
#define STT_FROM_HEAT_TREATMENT			10

//Timestep control
#define TSC_MANUAL						0
#define TSC_AUTOMATIC					1

//heat treatment definition types (2 out of 3: T_end, T_dot and delta_t)
#define HTDT_INVALID_CODE				0
#define HTDT_TEND_TDOT					1
#define HTDT_TDOT_DELTAT				2
#define HTDT_TEND_DELTAT				3
#define HTDT_TDOT_ACCEPS				4

//heat treatment property codes
//#define HTP_SEGMENT_NAME				1
#define HTP_TEMP_START					1
#define HTP_INHERIT_TEMP_START			2
#define HTP_SEGMENT_TEMP_CODE			6
#define HTP_TEMP_END					10
#define HTP_DELTA_TIME					11
#define HTP_TEMP_DOT					12
#define HTP_SEGMENT_START_TIME			15
#define HTP_STORE_IS_LOG				17
#define HTP_NUM_STORE_INTERVALS			18
#define HTP_PRECIPITATION_DOMAIN		20
#define HTP_EPS_DOT						30
#define HTP_ACC_EPS						31
#define HTP_PRE_SEGMENT_SCRIPT			40
#define HTP_POST_SEGMENT_SCRIPT			41
#define HTP_SEGMENT_COMMENT				50

// dislocation shearing types
#define DST_SINGLE						0
#define DST_PAIR						1

//Kinetic properties ...
#define KP_KINETIC_STATUS				1
#define KP_KINETIC_ALIAS_NAME			2
#define KP_SOLID_PHASE_INDEX			3
#define KP_SPECIAL_OPTIONS				10
#define KP_NUCLEATION_SITE				15
#define KP_NUCL_USE_SITE_SATURATION		16
#define KP_SATURATION_INACTIVE_RADIUS	20
#define KP_USE_SHAPE_FACTOR				25
#define KP_SHAPE_FACTOR					26
#define KP_NUMBER_PREC_SIZE_CLASSES		30
#define KP_NUMBER_VALID_PREC_CLASSES	31
#define KP_PREC_PARENT_PHASE_HANDLE		35
#define KP_MOLE_FRAC_FROM_PART_ARRAY	50
#define KP_INTERFACE_ENERGY				60
#define KP_AUTO_INTERFACE_ENERGY		64
#define KP_AUTO_IE_SIZE_CORR			65
#define KP_AUTO_IE_DIFFUSE_INTF_CORR	66
#define KP_MINIMUM_NUCLEATION_RADIUS	77
#define KP_NUCL_CONST					80
#define KP_INCUBATION_TIME_MODEL		83
#define KP_INCUBTIME_CONST				85
#define KP_NUCLEUS_COMP_MODEL			90
#define KP_NUCLEATION_MODEL				100
#define KP_ATOMIC_ATTACHMENT_CONTROL	110
#define KP_INTF_MOBILITY				111
#define KP_OTHER_PART_NUCL_TYPE			120
#define KP_EQUIV_INTERF_ENERGY			125
#define KP_NUCL_TRANSFORM_RADIUS_MIN	130
#define KP_NUCL_TRANSFORM_RADIUS_MAX	131
#define KP_OTHER_PART_NUCL_INHER_COMP	140
#define KP_VOLUMETRIC_MISFIT			150
#define KP_AUTO_VOLUMETRIC_MISFIT		151
#define KP_RESTRICT_NUCL_TO_PD			160
#define KP_RESTRICT_NUCL_VALID_MAJ_CON	170
#define KP_RESTRICT_NUCL_PREC_DOMAIN	180

#define KP_USE_NUCL_MISFIT_STRESS		200
#define KP_USE_NUCL_GB_ENERGY   		201
#define KP_IGNORE_MISFIT_DURING_DEF		205
#define KP_USE_NUCL_EXCESS_VA			210
#define KP_USE_NUCL_SHAPE_FACTOR		220
//#define KP_AUTO_COHERENCY_RADIUS		230
#define KP_COHERENCY_RADIUS				231
//#define KP_AUTO_SHEARABLE_RADIUS		235
//#define KP_SHEARABLE_RADIUS				236
#define KP_AUTO_BURGERS_VECTOR			240
#define KP_BURGERS_VECTOR				241
#define KP_LINE_TENSION_FROM_SIMPLE		250
#define KP_LINE_TENSION_THETA_SHEAR		255
#define KP_LINE_TENSION_OUTER_CUT_OFF	256
#define KP_LINE_TENSION_INNER_CUT_OFF	257
#define KP_STRENGTH_NON_SHEAR_ANGLE		260
#define KP_STRENGTH_COUPLING_TOTAL		270
#define KP_STRENGTH_COUPLING_SHEAR		271
#define KP_STRENGTH_COUPLING_NON_SHEAR	272
#define KP_IGNORE_FOR_STRENGTH			275
#define KP_KINETIC_MATRIX_PHASE_HANDLE	280		//for backwards compatibility only
#define KP_PRECIPITATION_DOMAIN_HANDLE	281
#define KP_PD_AUTO_BURGERS_VECTOR		290
#define KP_PD_BURGERS_VECTOR			291
//#define KP_PREC_DOMAIN_DOMAIN_TYPE		290
//#define KP_PREC_DOMAIN_COMP_TYPE		291
//#define KP_PREC_DOMAIN_DIMENSION		295
//#define KP_PREC_DOMAIN_FRACTION			300
#define KP_DIFFUSION_COEFF_FROM_MATRIX	310
#define KP_DIFFUSION_COEFF_FACTOR		311
#define KP_INTERST_DIFF_FROM_MATRIX		320
#define KP_INTERST_DIFF_COEFF_FACTOR	321
#define KP_GBDIFF_COEFF_FROM_MATRIX		330
#define KP_GBDIFF_COEFF_FACTOR			331
#define KP_INTERST_GBDIFF_FROM_MATRIX	340
#define KP_INTERST_GBDIFF_COEFF_FACTOR	341
#define KP_DISLDIFF_COEFF_FROM_MATRIX	350
#define KP_DISLDIFF_COEFF_FACTOR		351
#define KP_INTERST_DISLDIFF_FROM_MATRIX	360
#define KP_INTERST_DISLDIFF_COEFF_FACTOR	361
#define KP_OTHER_PART_NUCL_SITES		370
#define KP_MATRIX_DIFF_ENH_FACTOR_SUBS	380
#define KP_MATRIX_DIFF_ENH_FACTOR_INTS	381
#define KP_EXCESS_VA_ANNIHILATION_FACT	390
#define KP_EXCESS_VA_TRAPPING_FACT		400
#define KP_PREC_COALESCENCE_FACT		410
#define KP_PREC_COALESCENCE_TYPE		411
#define KP_BINDER_STAUFER_EXP			420

#define KP_TOTAL_DISL_DENSITY			500
#define KP_EQUILIB_DISL_DENSITY			510
#define KP_EXCESS_DISL_DENSITY_DEF		511
#define KP_EXCESS_DISL_DENSITY_REXX		512
#define KP_GRAIN_SIZE					520
#define KP_GRAIN_SIZE_ELONG_FACT		521
#define KP_SUB_GRAIN_SIZE				530
#define KP_SUB_GRAIN_SIZE_ELONG_FACT	531
#define KP_DISL_GENERATION_COEFF_A		540
#define KP_DISL_GENERATION_COEFF_B		541
#define KP_DISL_GENERATION_COEFF_C		542
#define KP_EVOLVE_GD_DYNAMICALLY		550
#define KP_EVOLVE_SGD_DYNAMICALLY		551
#define KP_EVOLVE_DISL_DENS_DYNAMICALLY	552
#define KP_SGD_EVOLUTION_COEFF_A		560
#define KP_SGD_EVOLUTION_COEFF_B		561
#define KP_SGD_EVOLUTION_MOB_M0			562
#define KP_SGD_EVOLUTION_MOB_Q			563
#define KP_GD_EVOLUTION_COEFF_K_D		570
#define KP_GD_EVOLUTION_COEFF_K_R		571
#define KP_GD_EVOLUTION_MOB_M0			572
#define KP_GD_EVOLUTION_MOB_Q			573
#define KP_GD_ALLOW_RECRYSTALLIZATION	580
#define KP_GD_REXX_COEFF_CGB			581

#define KP_IE_DISLOCATION				600
#define KP_IE_GRAIN_BOUNDARY			601
#define KP_IE_SUBGRAIN_BOUNDARY			602
#define KP_MP_USE_SAME_AS_MATRIX		610
#define KP_MP_YOUNGS_MODULUS			620
#define KP_MP_POISSON_RATIO				621
#define KP_MP_APB_ENERGY				630
#define KP_APB_STRENGTH_DISL_REPULSION	631
#define KP_STRENGTH_USE_LINEAR_MISFIT	635
#define KP_STRENGTH_LINEAR_MISFIT		636
#define KP_COH_STRENGTH_AVG_FORCE		640
#define KP_STRENGTH_GG_PINNING_FACTOR	645
#define KP_STRENGTH_GG_PINNING_EXPONENT	646

#define KP_SV_USE_IN_DIFFUSION			650
#define KP_SV_VA_DIFFUSION_CORRECTION	660
#define KP_SV_AUTO_MEAN_VA_DIFF_DIST	670
#define KP_SV_MEAN_DIFF_DISTANCE		671
#define KP_SV_EXCESS_VA_EFFICIENCY		680
#define KP_SV_GENERATION_COEFFICIENT	690
#define KP_SV_DIFF_DIST_ACCELERATION	700
#define KP_SV_VA_EVOLUTION_MODEL		710
#define KP_SV_VA_COND_NUCL_CONST		720
#define KP_SV_VA_COND_NUCL_GAMMA		721
#define KP_FRANK_LOOP_MEAN_RADIUS		730
#define KP_FRANK_LOOP_DENSITY			731
#define KP_JOG_DENSITY_DISLOCATION		740
#define KP_JOG_DENSITY_FRANK_LOOP		741
#define KP_STRENGTH_TAYLOR_FACTOR		755
#define KP_STRENGTH_BASIC_STRENGTH		760
#define KP_STRENGTH_HALL_PETCH			765
#define KP_STRENGTH_HALL_PETCH_SGB		766
#define KP_STRENGTH_DISL_STRENGTH_COEFF	770

#define KP_USE_SOLUTE_TRAPPING			800
#define KP_SOLUTE_TRAP_PHASE_HANDLE		805
#define KP_SOL_TRAP_ONLY_INTERSTITIAL	806
#define KP_TRAP_COMP_RESET_PHASE_CHANGE	807
#define KP_TRAP_PHASE_FRACTION_TYPE		820
#define KP_PHASE_FRACTION_FUNCTION		821
#define KP_TRAP_PHASE_INIT_COMP_TYPE	830
#define KP_TRAP_PHASE_EVOLVE_COMP_TYPE	831
#define KP_PREC_DOMAIN_MDEF_SUBST		840
#define KP_PREC_DOMAIN_MDEF_INTERST		841


#define KP_TTP_USER_FLAG				900
#define KP_TTP_USER_VALUE_1				905
#define KP_TTP_USER_VALUE_2				906
#define KP_TTP_USER_VALUE_3				907


//precipitation domain type
/*
#define PDT_NONE						0
#define PDT_CUBICAL						10
#define PDT_SHELL						20
#define PDT_SPHERE						30
*/
#define VDT_STATIC						0
#define VDT_DYNAMIC						1

#define STPFT_AUTOMATIC					0
#define STPFT_MANUAL					1

#define ISTP_FROM_PARENT_COMP			0
#define ISTP_FROM_EQUILIBRIUM			1

#define ESTP_FROM_EVOLUTION_EQUS		0
#define ESTP_FROM_EQUILIBRIUM			1

#define PCT_NONE						0	//precipitate coalescence type
#define PCT_PHENOMENOLOGIC				1

//Precipitation Domain Solute Trap property codes
#define PD_ST_ACTIVE					0
#define PD_ST_TRAPPED_ELEMENT			1
#define PD_ST_TRAP_ELEMENT				2		//0-num_elements: trapping at solute atoms, -1: disl, -2: gb
#define PD_ST_ENTHALPY_TRAP				3
#define PD_ST_ENTHALPY_TRAP_FUNCTION	4
#define PD_ST_TRAP_COORDINATION_NUMBER  5
#define PD_ST_PREC_NAME					10

//Nucleation Sites
#define KNS_NONE						0
#define KNS_BULK						0x00000001
#define KNS_DISLOCATION					0x00000002
#define KNS_SUBGRAIN_BOUNDARY			0x00000010
#define KNS_GRAIN_BOUNDARY				0x00000020
#define KNS_GRAIN_BOUNDARY_EDGE			0x00000040
#define KNS_GRAIN_BOUNDARY_CORNER		0x00000080
#define KNS_OTHER_PARTICLES				0x00000100
#define KNS_SGB_EDGE					0x00001000
#define KNS_SGB_CORNER					0x00002000

//special kinetic options
//#define KPO_FREEZE_RADIUS				0x00000001
//#define KPO_INTERSTITIALS_AS_EQUIL		0x00000002
//#define KPO_FORCE_EQUILIB_COMPOSITION	0x00000004


//flags
#define KF_APPEND_TO_BUFFER				0x00000001
#define KF_AUTO_SAVE					0x00000002
#define KF_RESET_PRECIPITATES			0x00000004
#define KF_LOAD_FROM_STATE				0x00000008
#define KF_TTP_STOP_IF_N_DECR			0x00000010
#define KF_TTP_APPEND_TO_BUFFER			0x00000020
#define KF_TTP_NO_FINAL_UPDATE			0x00000040

//Init calc
#define IP_FOR_EQILIBRIUM_STEP		1
#define IP_FOR_KINETIK_STEP			2
#define IP_SVOBODA_CONFIG			3

//Composition of Nucleus
#define NC_ORTHO_EQUILIBRIUM		0	//Zusammensetzung von GGW
#define NC_PARA_EQUILIBRIUM			1	//Zusammensetzung von Para-GGW
#define NC_MAX_NUCL_RATE			10	//Zusammensetzung mit max nucleation rate
#define NC_MIN_G_STAR				15	//Zusammensetzung mit minimalem G*
//#define NC_SVO_KOZE					5	//aus unserem Modell: SVO/KOZ/FISCH/FRA
//#define NC_ORTHO_EQUILIBRIUM_AVG	10	//Zusammensetzung von GGW, spaeter average
//#define NC_PARA_EQUILIBRIUM_AVG		11	//Zusammensetzung von Para-GGW, spaeter average
#define NC_FIXED_COMP				100	//fixe Zusammensetzung, muss gesetzt werden

//Nucleation model
#define NM_VOLMER_WEBER				0
#define NM_BECKER_DOERING			1
#define NM_BD_TIME_DEP				2
#define NM_SVOKO_STOCHASTIC			5
#define NM_DIRECT_TRANSFORMATION	10

//atomic attachment rate during nucleation
#define AAR_MAXIMUM_TIME			0
#define AAR_LOWER_BOUND				1
#define AAR_UPPER_BOUND				2
#define AAR_SFFK					10

//incubation time
#define ITM_MIXED_CONTROL			0
#define ITM_INTERFACE_CONTROL		1
#define ITM_RELAXATION_CONTROL		2

//other particle nucleation
#define OPNT_NUCLEATE_AT_SURFACE		0
#define OPNT_TRANSFORMATION_RADIUS		1
#define OPNT_EQUIVALENT_INTF_ENERGY		2

//va evolution
#define VCM_NO_EVOLUTION				0
#define VCM_MEAN_DIFFUSION_DISTANCE		1
#define VCM_FISCHER_SVO_KOZE			2


//flags fuer TTP-diagram
/*
#define TTP_SHOW_NUCL			0x00000001
#define TTP_SHOW_01				0x00000002
#define TTP_SHOW_1				0x00000004
#define TTP_SHOW_5				0x00000008
#define TTP_SHOW_10				0x00000010
#define TTP_SHOW_25				0x00000020
#define TTP_SHOW_50				0x00000040
#define TTP_SHOW_75				0x00000080
#define TTP_SHOW_90				0x00000100
#define TTP_SHOW_95				0x00000200
#define TTP_SHOW_99				0x00000400
#define TTP_SHOW_USER_1			0x00001000
#define TTP_SHOW_USER_2			0x00002000
#define TTP_SHOW_USER_3			0x00004000
#define TTP_SHOW_M95			0x00100000
#define TTP_SHOW_M90			0x00200000
#define TTP_SHOW_M75			0x00400000
#define TTP_SHOW_M50			0x00800000
#define TTP_SHOW_M25			0x01000000
#define TTP_SHOW_M10			0x02000000
#define TTP_SHOW_M5				0x04000000
*/
#define TTPT_ABSOLUTE_FRACTION			0
#define TTPT_RELATIVE_FRACTION			1
#define TTPT_RELATIVE_MAX_F				2

//histogram options
#define HT_NUMBER			0	
#define HT_VOLUME			1

//*****************************************************
//some defines for Monte module functionality
//*****************************************************

#define CDE_GIBBS_ENERGY			10
#define CDE_ENTHALPY				11
#define CDE_ENTROPY					12
#define CDE_CONFIG_ENTROPY			15
#define CDE_GE_MINUS_CONF_ENTROPY	100

#define CDE_OK							0
#define CDE_ERROR_INVALID_TYPE			10
#define CDE_ERROR_TRANSFER_COMP_ARRAY	20


//*****************************************************
// View-Types
//*****************************************************

	//globale Fenstertypen
#define VT_OUTPUT					10
#define VT_PHASEDETAILS				11
#define VT_TABULATERESULTS			20
#define VT_LIST_GIBBS_VARIABLES		21
#define VT_LIST_FE_VARIABLES		22
#define VT_TABULATERESULTS_F1		25		//mit Formula one
#define VT_TABULATENUCLDIST			30		//tabulation of non-classical nucleus distribution

#define VT_PHASESUMMARY				100
#define VT_PHASEDATABASEINFO		110
#define VT_PHASENNDATABASEINFO		111
#define VT_PHASEDATASYMBOLINFO		120
#define VT_CHEMPOTSGENERAL			133
#define VT_PHASEDIFFUSIONDATA		134
#define VT_SFKTRAPPARTTABLE			135		//SFK-trap partitioning table
	//Specimen
#define VT_PLOTSPECGEOMETRY			250		//Geometrie zeichnen
#define VT_FE_CURSORPOS_INFO		251		//Infos zur Cursorposition
#define VT_FE_INTERFACE_INFO		252		//Infos zu den Interfaces

	//spezielle Typen
#define VT_DEBUG_DX_ARRAYS			300
#define VT_DEBUG_JACO_MATRIX		301
#define VT_DEBUG_GE_DERIVATIVES		302
#define VT_DEBUG_DFM_DERIVATIVES	303
#define VT_DEBUG_MU_DERIVATIVES		304
#define VT_DEBUG_FE_DERIVATIVES		305

#define VT_DEBUG_KINETIC_MATRICES	310

#define VT_DEBUG_DX_ARRAYS_F1		330		//mit Formula one
#define VT_DEBUG_JACO_MATRIX_F1		331		//mit Formula one

#define VT_DEBUG_FE_MATRIX_F1		350		//mit Formula one
#define VT_DEBUG_FE_DX_ARRAYS_F1	351		//mit Formula one
#define VT_DEBUG_FE_JACO_MATRIX_F1	352		//mit Formula one

//CAFD
#define VT_CAFE_SPECIMEN			400		//CAFE-Specimen
#define VT_CAFE_CURSORPOS_INFO		410		//Infos zur Cursorposition

#define VT_DEBUG_CAFE_THERM_MATRIX	500		//CAFE-calculation matrices
#define VT_DEBUG_CAFE_DIFF_MATRIX	501		//CAFE-calculation matrices

//monte
#define VT_MONTE_NEAREST_NEIGHBOR		600		//monte carlo nearest neighbor list
#define VT_MONTE_NEIGHBOR_SHELL			601		//monte carlo nearest neighbor shell list
#define VT_MONTE_PAIR_DISTRIBUTION		602		//monte carlo pair distribution
#define VT_MONTE_MEAN_COMP_ENERGIES		603		//monte carlo mean composition energy general
#define VT_MONTE_MEAN_COMP_ENERGY_CURVE	604		//monte carlo mean composition energy specific
#define VT_MONTE_CLUSTER_ANALYSIS		605		//monte carlo cluster analysis window
#define VT_MONTE_CA_PROXIGRAM			606		//proxigram for cluster analysis
#define VT_MONTE_LCE_ENERGY				607		//local chemical environment

#define FIRSTPLOTWINDOW				1000
#define VT_PLOTPROGRESS				FIRSTPLOTWINDOW + 10
#define VT_PLOT_GRID_PROFILE		FIRSTPLOTWINDOW + 15	//one-dimensional profile
#define VT_PLOT_GRID_SURFACE		FIRSTPLOTWINDOW + 16	//2-dim surface plot
#define VT_PLOT_CELL_HISTORY		FIRSTPLOTWINDOW + 17	//cell property evolution
#define VT_PLOT_CONTOURPLOT			FIRSTPLOTWINDOW + 18
#define VT_TTPDIAGRAM				FIRSTPLOTWINDOW + 20
#define VT_DIAGRAMPARTICLEDIST		FIRSTPLOTWINDOW + 21
#define VT_TABULATEPARTICLEDIST		FIRSTPLOTWINDOW + 22
#define VT_IM_VELOCITY_FUNCTION		FIRSTPLOTWINDOW + 23
#define VT_DIAGRAMPARTICLE_SCATTER	FIRSTPLOTWINDOW + 25
#define VT_PAINT_CELLS_2D			FIRSTPLOTWINDOW + 30	//self drawn
#define VT_PAINT_MC_3D				FIRSTPLOTWINDOW + 40	//3-dim monte carlo

#define VT_SPECIMENNODALRESULT		FIRSTPLOTWINDOW + 100
#define VT_SIMULATIONRESULT			FIRSTPLOTWINDOW + 101
#define VT_SPECIMENELEMENTRESULT	FIRSTPLOTWINDOW + 102

#define VT_CAFE_SPEC_PROPERTIES		FIRSTPLOTWINDOW + 200

#define LASTPLOTWINDOW				FIRSTPLOTWINDOW + 1000


#define PT_PARTICLE_DIST			100
#define PT_PARTICLE_SCATTER			110
#define PT_PARTICLE_TTP				120
#define PT_XY						200
#define PT_1D_PROFILE				1000
#define PT_CELL_HISTORY				1010
#define PT_2D_SURFACE				1100
#define PT_CONTOURPLOT				1200



//*****************************************************
// Hint-Typen fuer UpdateAllViews()
//*****************************************************

#define VT_HINT_NORMAL				0
#define VT_HINT_FE_CURSOR			10		//gilt nur fuer FE-cursorpos infos


//*****************************************************
// View-Flags
//*****************************************************

#define VF_WINDOWISFROZEN			0x00000001		//fixiert?

#define VF_WINDOWISFIXEDTONODE		0x00010000		//an einen bestimmten Knoten gebunden?

//*****************************************************
// Flags fuer paint output FE
//*****************************************************

#define PO_SHOW_GRIDPOINTS				0x00000001
#define PO_SHOW_ELEMENTS				0x00000002
#define PO_SHOW_CONTOURS				0x00000004
#define PO_SHOW_CONTOUR_FILL			0x00000008
#define PO_AUTO_CONTOURS				0x00000010
#define PO_SHOW_HEAT_SOURCES			0x00000020
#define PO_SHOW_INTERFACE_GPS			0x00000040
#define PO_SHOW_INTERFACES				0x00000080
#define PO_SHOW_ELEMENT_SUBSTRUCT		0x00000100
#define PO_SHOW_2D_PROPLINES			0x00000200
#define PO_SHOW_GP_INDEX				0x00000400
#define PO_SHOW_GP_ID					0x00000800
#define PO_SHOW_SPECIMEN				0x00001000

//*****************************************************
// Flags fuer paint output CAFD
//*****************************************************

#define PO_SHOW_CAFD_ELEMENT_BOUNDS			0x00000001		//Zellen Grenzen
#define PO_SHOW_CAFD_BLOCK_BOUNDS			0x00000002		//Block-Grenzen
#define PO_SHOW_CAFD_GRIDPOINTS				0x00000010		//CA-Zentrum als Kreis
#define PO_SHOW_CAFD_INTERFACE_CELLS		0x00000020		//Interface elements als X

//#define PO_DISPL_NOTHING					0x00000000		//nichts darstellen
#define PO_DISPL_SELECTED_CAS				0x00010000		//ausgewaehlte ca's
#define PO_DISPL_PHASE_FIELD				0x00020000		//alle phasen
#define PO_DISPL_CA_VARIABLE				0x00040000		//eine ausgewaehlte variable

#define PO_DISPL_RANGE_AUTOMATIC			0x01000000		//Auto-range bei variable


//*****************************************************
// Variablentypen
//*****************************************************

#define SVT_GIBBS					1	//Variablen aus MC_core
#define SVT_SPECIMENNODAL			2	//Variablen aus MC_real nodal solution
#define SVT_SPECIMENELEMENT			3	//Variablen aus MC_real elemental solution
#define SVT_PARTICLE_DISTRIBUTION	4	//Variablen aus MC_core Phase: Particle distribution
#define SVT_IM_VELOCITY_FUNCTION	5	//Variablen aus MC_real Phase: IM_Velocity_Functions
#define SVT_CAFE_SPEC_PROPS			6	//Variablen aus MC_fdtr: 1D-diagram

//Table types
#define TT_GIBBS					1
#define TT_SPECIMEN					2

//element types in phases
#define PET_NOT_IN_PHASE			0
#define PET_SUBSTITUTIONAL			1
#define PET_INTERSTITIAL			2
#define PET_SUBSTITUTIONAL_REF		10	//nur in CAFE verwendet

#define SFK_TRAP_DISL				1000
#define SFK_TRAP_GB					1001
#define SFK_TRAP_TETRAGONALITY		1005
#define SFK_TRAP_FIRST_PREC			1100

//*****************************************************
// Variables ...
//*****************************************************

//physical units
#define VU_DIMENSION_LESS				0
#define VU_STEPVALUE					1
#define VU_TEMPERATURE					10
#define VU_LENGTH						20
#define VU_AREA							21
#define VU_VOLUME						22
#define VU_TIME							25
#define VU_PRESSURE						30
#define VU_NUMBER						40
#define VU_MOLE							41
#define VU_MASS							50

#define VU_PER_LENGTH					100
#define VU_PER_AREA						101
#define VU_PER_VOLUME					102
#define VU_PER_TEMPERATURE				103
#define VU_PER_TIME						105

#define VU_MASS_PER_VOLUME				150
#define VU_MOLES_PER_VOLUME				151
#define VU_MOLES_PER_TIME				152
#define VU_AREA_PER_TIME				160
#define VU_NUMBER_PER_TIME				161
#define VU_NUMBER_PER_VOLUME			170
#define VU_NUMBER_PER_VOLUME_TIME		171
#define VU_NUMBER_PER_AREA_TIME			172
#define VU_TEMPERATURE_PER_TIME			180

#define VU_ENERGY						200
#define VU_ENERGY_PER_MOLE				210
#define VU_ENERGY_PER_ATOM				211
#define VU_ENERGY_PER_MOLE_MOLE			215
#define VU_ENERGY_PER_MOLE_TEMP			216

#define VU_ENERGY_PER_NUMBER			220
#define VU_ENERGY_PER_NUMBER_NUMBER		221
#define VU_ENERGY_PER_NUMBER_TEMP		225

#define VU_ENERGY_PER_LENGTH			230
#define VU_ENERGY_PER_AREA				231
#define VU_ENERGY_PER_VOLUME			232

#define VU_LENGTH_PER_TIME				300
#define VU_LENGHT_4_PER_ENERGY_TIME		310

//preferred units
#define PVU

//variable categories
#define MV_CAT_ALL						0
#define MV_CAT_GENERAL					1
#define MV_CAT_PHASE_FRACTION			10
#define MV_CAT_COMPOSITION				11
#define MV_CAT_SITE_FRACTION			12
#define MV_CAT_MASS_VOLUME_DENSITY		20
#define MV_CAT_PHYSICAL_PARAMS			30
#define MV_CAT_STATE_VARIABLES			35
#define MV_CAT_STATE_VARS_GBB			36
#define MV_CAT_CHEMICAL_POTENTIALS		40
#define MV_CAT_ACTIVITIES				41
#define MV_CAT_DIFFUSION				50
#define MV_CAT_DRIVING_FORCES			55
#define MV_CAT_KINETIC_GENERAL			60
#define MV_CAT_PREC_DOMAIN_STRUCT		65
#define MV_CAT_PREC_DOMAIN_STRENGTH		66
#define MV_CAT_PREC_DOMAIN_SPECIAL		67
#define MV_CAT_PRECIPITATES				70
#define MV_CAT_NUCLEATION				80
#define MV_CAT_PREC_STRENGTH			85
//#define MV_CAT_NEURAL_NETWORKING		99
#define MV_CAT_CONSTANTS				100
#define MV_CAT_VARIABLES				105
#define MV_CAT_STRING_VARIABLES			106
#define MV_CAT_FUNCTIONS				107
#define MV_CAT_PRECIPITATE_DIST			150
#define MV_CAT_OBSOLETE					200
#define MV_CAT_DEBUG					900

#define MV_CAT_LAST_CORE_CAT			999	//no more category above this number. then come mc_cafe category

//variable seperators
#define VS_NONE				0
#define VS_NUMBER			1
#define VS_PHASE			10		//all phases
#define VS_KIN_PHASE		11		//all precipitates
#define VS_KIN_MATRIX		12		//all kinetic matrix phases
#define VS_PREC_DOMAIN		15		//all precipitation domains
#define VS_ELEMENT			20		//all elements
#define VS_ELEMENT_WO_VA	21		//all elements without vacancies
#define VS_ELEMENT_W_SV 	22		//all elements without interstitial vacancies and with substitutional vacancies
#define VS_ELEMENT_W_DEF	24		//all elements with defects (dislocations, grain boundaries, ...)
#define VS_ELEMENT_TRAP 	25		//all elements on trapped sublattices (dislocations, grain boundaries, ...)
#define VS_SUBLATTICE		30		//all sublattices
#define VS_ELEMENT_KIN_X	31		//all site fractions for precipitate calc


//********************************************************************
//
// Variablencodes
//
//********************************************************************

#define VC_Z							0	//Z-Variable
#define VC_X							1	//X-Variable (Achse)
#define VC_Y							2	//Y-Variable (Achse)

#define VC_UNDEFINED					0
#define VC_PHASEFRACTION				1
#define VC_ELEMENTAMOUNT				2
#define VC_ELEMENTCONCENTRATION			3
#define VC_SITAMOUNT					4
#define VC_U_FRACTION					10
#define VC_UC_FRACTION					11
#define VC_ELEMENTAMOUNT_SYSTEM			15
#define VC_ELEMENTAMOUNT_PREC_DOMAIN	16
#define VC_ELEMENTAMOUNT_PRECIPITATE	17
#define VC_U_FRACTION_SYSTEM			20
#define VC_U_FRACTION_PREC_DOMAIN		21
#define VC_U_FRACTION_NOMINAL			22
#define VC_NOMINAL_COMPOSITION			25
#define VC_SITAMOUNT_IN_MF				27
#define VC_MOLAR_GIBBSENERGY			50
#define VC_MOLAR_ENTHALPY				51
#define VC_MOLAR_ENTROPY				52
#define VC_MOLAR_CONF_ENTROPY			53
#define VC_MOLAR_HEAT_CAPACITY			55
#define VC_MOLAR_MAGNETIC_ENTHALPY		56
#define VC_CURIE_TEMPERATURE			60
#define VC_INTERNAL_STRESS_FACTOR		61
#define VC_MOLAR_PHASE_GIBBSENERGY		70
#define VC_MOLAR_PHASE_ENTHALPY			71
#define VC_MOLAR_PHASE_ENTROPY			72
#define VC_MOLAR_PHASE_CONF_ENTROPY		73
#define VC_MOLAR_PHASE_HEAT_CAP			74
#define VC_MOLAR_PHASE_HM_VA_FORMATION	80
#define VC_MOLAR_PHASE_SM_VA_FORMATION	81
#define VC_DRIVING_FORCE				90
#define VC_CHEM_DRIVING_FORCE			91
#define VC_MECH_DRIVING_FORCE			92
#define VC_CHEMICAL_POT_GLOBAL			95
#define VC_CHEMICAL_POTENTIAL			96
#define VC_CHEMICAL_POTENTIAL_0			97		//anderer Ref-State
#define VC_ACTIVITY_GLOBAL_298			100
#define VC_ACTIVITY						101
#define VC_ACTIVITY_298					102		//anderer Ref-State
#define VC_ACTIVITY_COEFFICIENT			105
#define VC_ACTIVITY_COEFFICIENT_298		106		//anderer Ref-State
#define VC_THERMODYNAMIC_FACTOR_RED		110
#define VC_THERMODYNAMIC_FACTOR_MEAN	111
#define VC_TOTAL_DISLOCATION_DENSITY	120
#define VC_EQUIL_DISLOCATION_DENSITY	121
#define VC_EXCESS_DISL_DENSITY_MEAN		122
#define VC_EXCESS_DISL_DENSITY_DEF		123
#define VC_EXCESS_DISL_DENSITY_REXX		124
#define VC_COND_DISLOCATION_DENSITY		127
#define VC_GRAIN_DIAMETER				130
#define VC_SUB_GRAIN_DIAMETER			135
#define VC_PD_YOUNGS_MODULUS			140
#define VC_PD_SHEAR_MODULUS				141
#define VC_PD_POISSONS_RATIO			142
#define VC_PD_BURGERS_VECTOR			150
//#define VC_CALC_TOTAL_DISL_DENSITY		160
#define VC_DISL_DENSITY_STEADY_STATE	161
#define VC_CALC_GRAIN_DIAMETER			170
#define VC_CALC_GRAIN_DIAMETER_GROWTH   171
#define VC_CALC_GRAIN_DIAMETER_REXX     172
#define VC_GD_RECRYSTALLIZED_FRACTION   173
#define VC_GRAIN_DIAMETER_STEADY_STATE	185
#define VC_CALC_SUB_GRAIN_DIAMETER		190
#define VC_SG_DIAMETER_STEADY_STATE		191
#define VC_KINETIC_TIME					200
#define VC_KINETIC_DELTA_TIME			205
#define VC_KINETIC_HT_SEG_TIME			210
#define VC_KINETIC_HT_SEG_START_TIME	211
#define VC_KINETIC_HT_SEG_END_TIME		212
#define VC_KINETIC_ACC_CALC_TIME		230
#define VC_KINETIC_ACC_EPS				231
#define VC_KINETIC_EPS_DOT				240
#define VC_EST_MATRIX_EQUIL_COMP		250
#define VC_DIFFUSION_MQ					300
#define VC_DIFFUSION_MF					301
#define VC_TRACER_DIFFUSIVITY			310
#define VC_INTRINSIC_DIFFUSIVITY		311
#define VC_CHEMICAL_DIFFUSIVITY			312
#define VC_TRACER_DIFFUSIVITY_DISL		315
#define VC_TRACER_DIFFUSIVITY_GB		316
#define VC_MANNING_CORRECTION_R			319
#define VC_CHEM_DIFF_DT_PHI				320
#define VC_MOBILITY						330
#define VC_STEPVALUE					350
#define VC_DERIVE_CHEM_POT				360
#define VC_DIS_SITAMOUNT				361
#define VC_SYSTEMMASS					400
#define VC_PHASEMASS					401
#define VC_MOLAR_SYSTEMMASS				405
#define VC_MOLAR_PHASEMASS				406
#define VC_ELEMPHASEMASS				410
#define VC_DEFECT_SITE_FRACTION			415
#define VC_SYSTEMVOLUME					420
#define VC_PHASEVOLUME					421
#define VC_MOLAR_SYSTEMVOLUME			430
#define VC_MOLAR_PHASEVOLUME			431
#define VC_SUBS_ATOMIC_VOLUME			440
#define VC_ATOMIC_VOLUME				441
#define VC_SYSTEMDENSITY				450
#define VC_PHASEDENSITY					451
#define VC_SYSTEMMC_TRUETHERMEXP			460
#define VC_PHASEMC_TRUETHERMEXP			461
#define VC_SYSTEMLINTHERMEXP			465
#define VC_PHASELINTHERMEXP				466
#define VC_SYSTEMSTRAIN					470
#define VC_PHASESTRAIN					471
#define VC_SYSTEMLENGHT					480
#define VC_PHASELENGHT					481
#define VC_LATTICE_CONSTANT				490
#define VC_SUBST_LATTICE_CONSTANT		491

//#define VC_INTERFACE_ENERGY				500
#define VC_CRITICAL_ENERGY				510
#define VC_CRITICAL_RADIUS				511
#define VC_CRITICAL_RADIUS_GB			512
#define VC_CRITICAL_NUMBER_ATOMS		520
#define VC_CALC_INTERFACE_ENERGY		530
#define VC_EFFECTIVE_INTERFACE_ENERGY	540
#define VC_KR_LSW_PRECIPITATE			550
#define VC_NUCLEATION_RATE				560
//#define VC_NUCLEATION_RATE_BD			561
#define VC_DELTA_V_NUCLEATION			562
//#define VC_NUCLEATION_RATE_BDTD			463
#define VC_DELTA_V_GROWTH				580
#define VC_DELTA_V_GROWTH_IN_PHASE		581
#define VC_X_NUCLEUS					590
#define VC_YX_NUCLEUS					591
#define VC_EST_INTERSTITIAL_YVA			595
#define VC_MEAN_INTERFACIAL_PRESSURE	596
#define VC_TOT_NUMBER_OF_PARTICLES		600
#define VC_NUMBER_OF_PARTICLES			601
#define VC_TOT_FRACTION_OF_PARTICLES	605
#define VC_MEAN_RADIUS					610
#define VC_MEAN_RADIUS_VOL				611
#define VC_MEAN_EQU_RADIUS_H			620
#define VC_MEAN_EQU_RADIUS_D			621
#define VC_MIN_RADIUS					630
#define VC_MAX_RADIUS					631
#define VC_MEAN_VA_DIFFUSION_DIST		640

#define VC_CRIT_ASHBY_OROWAN_STRESS		650		//Critical Orowan Stress 
#define VC_CHEMICAL_HARDENING			655		//
#define VC_COHERENCY_STRAIN_HARDENING	656		//
#define VC_COHERENCY_STRAIN_HARDENING_W	657		//
#define VC_COHERENCY_STRAIN_HARDENING_S	658		//
#define VC_MODULUS_MISMATCH_HARDENING	660		//
#define VC_MODULUS_MISMATCH_HARDENING_W	661		//
#define VC_MODULUS_MISMATCH_HARDENING_S	662		//
#define VC_APB_ENERGY_HARDENING			665		//
#define VC_APB_ENERGY_HARDENING_W		666		//
#define VC_APB_ENERGY_HARDENING_S		667		//
#define VC_TOT_SHEAR_STRESS_SHEARING	670		//
//#define VC_TOT_SHEAR_STRESS_NON_SHEAR	671		//
#define VC_TOT_SHEAR_STRESS_SUM			672		//
//#define VC_TOT_YIELD_STRESS_SUM			673		//
#define VC_MEAN_DISTANCE_2D				675
#define VC_MEAN_DISTANCE_3D				676
#define VC_MEAN_PDISTANCE_3D			677
#define VC_BURGERS_VECTOR_PREC			678
#define VC_TOT_MEAN_DIST_2D				680
#define VC_TOT_MEAN_DIST_3D				681
#define VC_TOT_MEAN_PDIST_3D			682
#define VC_TOT_MEAN_RADIUS				683
#define VC_DIST_DIFF_CORR				690
#define VC_RO_DIFF_CORR					691
#define VC_TRAP_DIFF_CORR				692

#define VC_DISL_LINE_TENSION_SIMPLE		700
#define VC_DISL_LINE_TENSION_WEAK		701
#define VC_DISL_LINE_TENSION_STRONG		702

#define VC_PD_SHEAR_STRESS_SHEARING		710		//
#define VC_PD_SHEAR_STRESS_NON_SHEAR	711		//
#define VC_PD_SHEAR_STRESS_SUM			712		//
#define VC_PD_YIELD_STRESS_SUM			713		//
#define VC_BASIC_YIELD_STRENGTH			720
#define VC_TOT_SOLID_SOLUTION_STRENGTH	725
#define VC_TOT_DISLOCATION_STRENGTH		730
#define VC_TOT_FINE_GRAIN_STRENGTH		731
#define VC_TOT_YIELD_STRENGTH			735

#define VC_TOT_NUM_ATOMS_BULK			740
#define VC_TOT_NUM_ATOMS_DISL			741
#define VC_TOT_NUM_ATOMS_GB				742
#define VC_TOT_NUM_ATOMS_GBE			743
#define VC_TOT_NUM_ATOMS_GBC			744
#define VC_TOT_NUM_ATOMS_SGB			745
#define VC_TOT_NUM_ATOMS_SGBE			746
#define VC_TOT_NUM_ATOMS_SGBC			747

#define VC_MEAN_RADIUS_ST_DEV			750
#define VC_MEAN_DIAMETER				755
#define VC_MEAN_DIAMETER_VOL			756
#define VC_MEAN_POROD					760
#define VC_MEAN_GUINIER					761
#define VC_NUCLEATION_DRIVING_FORCE		780
#define VC_NUCLEATION_CHEM_DFM			785
#define VC_NUCLEATION_MECH_DFM			786
#define VC_NUCLEATION_VA_DFM1			787
#define VC_NUCLEATION_VA_DFM2			788
#define VC_D_EFFECTIVE_SVO				800
#define VC_NUCL_INCUBATION_TIME			805
#define VC_NUCLEATION_INTERF_ENERGY		810
#define VC_NUCLEATION_INTERF_S_CORR		815
#define VC_ATOMIC_ATTACHMENT_RATE		820
#define VC_ATOMIC_ATTACHMENT_RATE_MT	821
#define VC_ATOMIC_ATTACHMENT_RATE_LB	822
#define VC_ATOMIC_ATTACHMENT_RATE_UB	823
#define VC_ATOMIC_ATTACHMENT_RATE_MD	824
#define VC_ZELDOVICH_FACTOR				890
#define VC_EQUIL_CLUSTER_DISTR_FRAC		900
#define VC_NUCL_INC_TIME_INTF_CONTR		910
#define VC_NUCL_INC_TIME_RELAX_CONTR	911
#define VC_R_DISTR_MOMENT				920
#define VC_R_DISTR_MOMENT_SCALED		921

#define VC_DOM_X_VA_EQUIL				950
#define VC_DOM_X_VA_CURR				951
#define VC_DOM_X_VA_STEADY_STATE		952
#define VC_XVA_DIFF_CORR				960
#define VC_DOM_DX_VA_GB					970
#define VC_DOM_DX_VA_DISL				971
#define VC_DOM_DX_VA_FL					972
#define VC_DOM_DX_VA_DEF				973
#define VC_DOM_SFK_YT_L					980
#define VC_DOM_SFK_YT_TK				981
#define VC_DOM_SFK_YXT_L				982
#define VC_DOM_SFK_YXT_TK				983
#define VC_DOM_Y_L                      985
#define VC_DOM_Y_TK                     986
#define VC_DOM_YX_L                     987
#define VC_DOM_YX_TK                    988
#define VC_DOM_BETA_TRAP                990
#define VC_DOM_ALPHA_DISL               995

#define VC_PHASE_FRACTION_OF_PART		1000
#define VC_PD_RADIUS					1010
#define VC_PD_NUMBER_PARTICLES			1015
#define VC_EQU_RADIUS_H					1020
#define VC_EQU_RADIUS_D					1021
#define VC_PD_COMPOSITION				1030
#define VC_PD_VOLUME					1035
#define VC_PD_ATOMS_PER_PREC			1040
#define VC_PD_TOTAL_DFM					1050
#define VC_PD_PARTIAL_DFM				1051
#define VC_PD_MECHANICAL_DFM			1055
#define VC_PD_SMOOTH_MECHANICAL_DFM		1056
#define VC_PD_D_RADIUS_DT				1060
#define VC_PD_D_NUMBER_PARTICLES_DT		1065
#define VC_PD_D_COMPOSITION_DT			1070
#define VC_PD_D_RADIUS_DT_0				1075
#define VC_PD_D_COMPOSITION_DT_0		1080
#define VC_PD_D_COMPOSITION_DT_1		1081
#define VC_PD_EST_INTERSTITIAL_YVA		1090
#define VC_PD_CUR_INTERSTITIAL_YVA		1091
#define VC_PD_OSC_DAMPING_FACTOR		1100
#define VC_PD_OSC_DAMPING_FACTOR_VA		1101
#define VC_PD_OSC_DAMPING_FACTOR_WK		1102
#define VC_PD_SIZE_CORRECTION_FACT		1105
#define VC_PD_DIFF_INTF_CORRECTION_FACT	1110

#define VC_DHM_DF_PRECIPITATE			1200
#define VC_DGM_DF_PRECIPITATE			1201
#define VC_DHM_DF_DX_PRECIPITATE		1205
#define VC_L_IJ_ATOMIC_INTERACTION		1210
#define VC_REGULAR_DESOL				1220
#define VC_REGULAR_TSOL					1221
#define VC_TOTAL_NUM_NUCL_SITES			1230
#define VC_OCCUP_NUM_NUCL_SITES			1235
#define VC_EFFECT_NUM_NUCL_SITES		1240

#define VC_PD_MECHANICAL_WK				1300
#define VC_PD_MECHANICAL_WK_DOT			1301
#define VC_PD_MECH_DIFF_RETARD_FACT		1310

#define VC_LINEAR_MISFIT_STRAIN			1320
#define VC_VOLUME_MISFIT_STRAIN			1321
#define VC_PRECIPITATE_VOLUME			1325
#define VC_PRECIPITATE_AREA				1326

//#define VC_CALC_COHERENCY_RADIUS		1350
//#define VC_CALC_SHEARABLE_RADIUS		1351
//#define VC_NUM_PREC_SMALLER_THAN_CRIT	1360
//#define VC_MEAN_RAD_SMALLER_THAN_CRIT	1361
//#define VC_F_SMALLER_THAN_CRIT			1365
//#define VC_NUM_PREC_LARGER_THAN_CRIT	1370
//#define VC_MEAN_RAD_LARGER_THAN_CRIT	1371
//#define VC_F_LARGER_THAN_CRIT			1375

#define VC_NN_CHEMICAL_POTENTIAL		1390	//obsolete
#define VC_NN_ACTIVITY					1391	//obsolete
#define VC_NN_ACTIVITY_COEFFICIENT		1392	//obsolete
#define VC_NN_MOD_ACTIVITY_COEFF		1393	//obsolete
#define VC_NN_MOD_GIBBS_ENERGY			1394	//obsolete
#define VC_NN_GIBBS_ENERGY				1395	//obsolete

#define VC_VACANCY_CONC_EQUIL			1400		//equilibrium vacancy concentration
#define VC_VACANCY_CONC_CURRENT			1401		//current vacancy concentration
#define VC_SUBST_DIFF_CORR_FACTOR		1410		//diffusion correction factor according to excess vacancies
#define VC_FRANK_LOOP_MEAN_RADIUS		1415		//Frank loops
#define VC_FRANK_LOOP_DENSITY			1416		//Frank loops
#define VC_DISLOCATION_LINE_ENERGY		1420		//dislocation line energy (e.g. for Frank loops)

#define VC_DISLOCATION_JOG_FRACTION		1430		//equilibrium dislocation jog fraction

#define VC_TEST_VARIABLE				1500

#define VC_VARIABLE						2000

#define VC_TEMPERATURE					2100
#define VC_PRESSURE						2101
#define VC_GAS_CONSTANT					2010
#define VC_PI							2011
#define VC_AVOGADRO_CONSTANT			2012
#define VC_BOLTZMANN_CONSTANT			2013

#define VC_GRID_SIM_VARIABLE			8000

#define VC_MONTE_SIM_VARIABLE			9000

#define VC_USER_FUNCTION				TYPE_FIRSTPHYSPARAM - 1

#define VC_FIRSTPHYSICAL				TYPE_FIRSTPHYSPARAM // = 10000


//Values for Spectrogram plot
#define SP_MEANINTERPOL					1
#define SP_SHEPARDINTERPOL				2

#endif
