
/*! \file matcalcCore_impl.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef matcalcCore_impl_h__
#define matcalcCore_impl_h__
/*	include *********************************************************/


/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace matcalc
    {
//       template <typename SETUP, typename PRECOMPUTATIONS, typename STEP, typename RESULTS_TRANSFORMER>
//             MatcalcCore<SETUP, PRECOMPUTATIONS, STEP, RESULTS_TRANSFORMER>::MatcalcCore(boost::shared_ptr<SETUP> setup, boost::shared_ptr<PRECOMPUTATIONS> precomputations, boost::shared_ptr<STEP> step, boost::shared_ptr<RESULTS_TRANSFORMER> resultsTransformer)
//               : setup_(setup)
//               , precomputations_(precomputations)
//               , step_(step)
//               , resultsTransformer_(resultsTransformer)
//             {      };
//             template <typename SETUP, typename PRECOMPUTATIONS, typename STEP, typename RESULTS_TRANSFORMER , typename INPUT_PROVIDER>
//             template <typename VARIABLE>
//             VARIABLE am3::software::matcalc::MatcalcCore<SETUP, PRECOMPUTATIONS, STEP, RESULTS_TRANSFORMER, INPUT_PROVIDER>::Get( typename RESULTS_TRANSFORMER::Point * point )
//             {
//               if ( ! initialized_)
//               {
//                 mpr::Temperature temperature = inputsProvider_->Get<mpr::Temperature,mpr::Coordinates2D>::InPoint(point);
//                 precomputations_->Precomputations(temperature);
//                 initialized_ = true;
//                 lastTemperature_(temperature);
//               }
//               if (lastTime_ < point->GetTime())
//               {
//                 mpr::Temperature temperature = inputsProvider_->Get<mpr::Temperature,mpr::Coordinates2D>::InPoint(point);
//                 double newTime = point->GetTime();
//                 mpr::StepLength stepLength;
//                 stepLength(newTime - lastTime_);
//                 lastTime_ = newTime;
//                 step_->CalculateStep(stepLength,lastTemperature_, temperature);
//                 lastTemperature_(temperature);
//               }
//               return this->resultsTransformer_->Get<VARIABLE>(point);
//             }
//       
//             template <typename SETUP, typename PRECOMPUTATIONS, typename STEP, typename RESULTS_TRANSFORMER, typename INPUT_PROVIDER>
//             bool MatcalcCore<SETUP, PRECOMPUTATIONS, STEP, RESULTS_TRANSFORMER, INPUT_PROVIDER>::Setup( const std::string workSpacePath, const std::string licensePath, const std::string initScript )
//             {
//               return setup_->Setup( workSpacePath, licensePath, initScript );
//             }
//       
//       
//             template <typename SETUP, typename PRECOMPUTATIONS, typename STEP, typename RESULTS_TRANSFORMER, typename INPUT_PROVIDER>
//             void MatcalcCore<SETUP, PRECOMPUTATIONS, STEP, RESULTS_TRANSFORMER, INPUT_PROVIDER>::CalculateStep( mpr::StepLength timeStepLength, mpr::Temperature finalTemperature )
//             {
//               step_->CalculateStep(timeStepLength, finalTemperature );
//             }
//       
//             template <typename SETUP, typename PRECOMPUTATIONS, typename STEP, typename RESULTS_TRANSFORMER, typename INPUT_PROVIDER>
//             void MatcalcCore<SETUP, PRECOMPUTATIONS, STEP, RESULTS_TRANSFORMER, INPUT_PROVIDER>::Precomputations( mpr::Temperature initialTemperature )
//             {
//               step->Precomputations(initialTemperature );
//             }
//       
//       
//       
//             template <typename SETUP, typename PRECOMPUTATIONS, typename STEP, typename RESULTS_TRANSFORMER, typename INPUT_PROVIDER>
//             void MatcalcCore<SETUP, PRECOMPUTATIONS, STEP, RESULTS_TRANSFORMER, INPUT_PROVIDER>
//               ::Set( boost::shared_ptr<SETUP> setup, boost::shared_ptr<PRECOMPUTATIONS> precomputations, boost::shared_ptr<STEP> step, boost::shared_ptr<RESULTS_TRANSFORMER> resultsTransformer , boost::shared_ptr<INPUT_PROVIDER> inputsProvider )
//             {
//               setup_ = setup;
//               precomputations_ = precomputations;
//               step_ = step;
//               resultsTransformer_ = resultsTransformer;
//               resultsTransformer_->Set(step_);
//               inputsProvider_ = inputsProvider;
//             }


    }
  }
}


#endif // matcalcCore_impl_h__
