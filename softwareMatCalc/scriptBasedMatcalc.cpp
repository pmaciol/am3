
/*! \file scriptBasedMatcalc.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include "scriptBasedMatcalc.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace matcalc
		{
			

      void ScriptBasedPrecomputations::Precomputations()
      {
        char * precomputationsScriptChar = new char[precomputationsScript_.size() + 1];
        std::copy(precomputationsScript_.begin(), precomputationsScript_.end(), precomputationsScriptChar);
        precomputationsScriptChar [precomputationsScript_.size()] = '\0';
        MCCOL_RunScriptFileChar(precomputationsScriptChar);
      }


      void ScriptBased::CalculateStep( mpr::StepLength timeStepLength, mpr::Temperature lastTemperature, mpr::Temperature finalTemperature )
      {
      //         MCC_SetHTSegmentProperty(hts_handle, HTP_SEGMENT_TEMP_CODE, HTDT_TEND_TDOT, buff, NULL);
         MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_START, NULL, lastTemperature(), NULL);
         //MCC_SetHTSegmentProperty(hts_handle, HTP_INHERIT_TEMP_START, NULL, NULL, NULL); //???
         MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_END, NULL, finalTemperature(), NULL);
         MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_DOT, NULL,  (lastTemperature() - finalTemperature()) / timeStepLength() , NULL);
         //MCC_SetHTSegmentProperty(hts_handle, HTP_DELTA_TIME, NULL,  timeStepLength() , NULL); //?????

//         int lastTreatment = MCC_GetNumberOfHeatTreatments();
//         MC_HANDLE treatment = MCC_GetHTHandleByIndex(lastTreatment - 1);
// 
//         int lastSegment = MCC_GetNumberOfHTSegments(treatment);
//         MC_HANDLE segement = MCC_GetHTSegmentHandleFromIndex(treatment,lastSegment - 1);
//         log = MCC_RemoveHTSegment(treatment, segement);
//         lastSegment = MCC_GetNumberOfHTSegments(treatment);
//         log = MCC_AppendNewHTSegment(treatment);
//         lastSegment = MCC_GetNumberOfHTSegments(treatment);
//         MC_HANDLE hts_handle = MCC_GetHTSegmentHandleFromIndex(treatment, 0);
//         char* expr ="martensite";
//         MCC_SetHTSegmentProperty(hts_handle, HTP_PRECIPITATION_DOMAIN, false, 0, (void*)expr);
//         double buff = 0;
//         MCC_SetHTSegmentProperty(hts_handle, HTP_SEGMENT_TEMP_CODE, HTDT_TEND_TDOT, buff, NULL);
//         MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_START, NULL, lastTemperature(), NULL);
//         //MCC_SetHTSegmentProperty(hts_handle, HTP_INHERIT_TEMP_START, NULL, NULL, NULL); //???
//         MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_END, NULL, finalTemperature(), NULL);
//         MCC_SetHTSegmentProperty(hts_handle, HTP_TEMP_DOT, NULL,  (lastTemperature() - finalTemperature()) / timeStepLength() , NULL);
//         //MCC_SetHTSegmentProperty(hts_handle, HTP_DELTA_TIME, NULL,  timeStepLength() , NULL); //?????
// 
//         int kinetic_flag;
//         MCC_GetGlobalParameter(MCGV_KINETIC_FLAGS,&kinetic_flag);
// 
//         kinetic_flag &= ~(KF_LOAD_FROM_STATE|KF_RESET_PRECIPITATES);
//         kinetic_flag |= KF_APPEND_TO_BUFFER;
//         MCC_SetGlobalParameter(MCGV_KINETIC_FLAGS, kinetic_flag);
// 
//         MCC_CalcKineticStep(false,EO_NORMAL);
      }

    } //matcalc
	} //software
} //am3
