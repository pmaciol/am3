
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef softwareMatCalcContracts_h__
#define softwareMatCalcContracts_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/macrosSimplifiedModels.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace moc = am3::model::contract;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace matcalc
    {
      CONTRACT(   MatCalcGenericInputForElementContract         , 3, mpr::Coordinates2D, mpr::Temperature, mpr::Time,mpr::ElementNumber);
      CONTRACT(   MatCalcGenericInputContract         , 2, mpr::Coordinates2D, mpr::Temperature, mpr::Time);

      //******************************************************************************************************************************************************************

      SIMPLE_PROP(Alcrfemnsi_a_p0_Radius, double);
      SIMPLE_PROP(Alcrfemnsi_a_p0_VolumeFraction, double);
      SIMPLE_PROP(TestValue, double);
    }
  }
}
#endif // softwareMatCalcContracts_h__
