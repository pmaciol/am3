
// basic types used in MatCalc
#ifndef MC_HANDLE
#ifdef WIN32
#define MC_HANDLE	int
#else
#define MC_HANDLE	void*
#endif
#endif

//platform specific ...
#ifndef WIN32

#ifndef ASSERT
//	#define ASSERT assert
// do NOT assert on UNIX systems!!!
	#define ASSERT 
#endif
#ifndef ASSERT_VALID
//	#define ASSERT_VALID assert
// do NOT assert on UNIX systems!!!
	#define ASSERT_VALID 
#endif


#ifndef MC_BOOL
	#define MC_BOOL unsigned int
#ifndef MC_TRUE
	#define MC_TRUE 1
#endif
#ifndef MC_FALSE
	#define MC_FALSE 0
#endif
	#define UINT unsigned int
	#define BYTE unsigned char
	#define WORD short int
	#define DWORD unsigned int
	#define POSITION int
	#define LPCSTR unsigned char *
	#define HWND int
	#define HANDLE int
	#define LPVOID void *
#endif

#ifdef DLL_EXPORT
#define DECL_DLL_EXPORT extern "C" __declspec( dllexport )
#else
#define DECL_DLL_EXPORT extern "C"
#endif

#endif
