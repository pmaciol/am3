
#include <string>
#include <vector>

//#include "mc_types.h"
#include "mc_defines.h"

#include "externs.h"
#include "matcalcController.h"
#include "../models/dataStructures.h"
//#include "structure.h"

namespace mpr = am3::model::properties;

int main(int argc, char **argv)
{
// 	bool test;
//   MCC_SetApplicationDirectoryChar("d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\");
//   MCC_SetLicenseFilePathChar("d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\license.dat");
//   MCC_ReadLicenseInformation();
//   char * licText = MCC_GetLicenseInformation(false);
//   // Log!
//   MCC_ReadDefaultSettings();
//   MCC_SetGlobalParameter(MCGV_COMPOSITION_TYPE, CT_WEIGHT_PERCENT);
// 
// 	test=MCC_InitializeKernel();
//   char* script_file_string = "Sanicro25.mcs";
//  MCCOL_RunScriptFileChar(script_file_string);
  am3::software::matcalc::MatcalcController mc("d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\", "d:\\pmaciol\\src\\am3\\tmp\\matcalc32\\matcalc\\license.dat", "d:\\pmaciol\\src\\am3\\tmp\\matcalcSimpleScript\\test_on_heat_treatment_9Cr_steel.mcs" );
  mpr::Time t(200);
  mpr::Temperature temp1, temp2;
  temp1(1150);
  temp2(1140);

  mc.NextStep(t,temp2,temp1);

  do 
  {
    temp1(temp2);
    temp2(temp2() - 10);
    mc.NextStep(t,temp2,temp1);
  } while (temp2() > 900);
//	MCC_GetNumVariableCategories();
}