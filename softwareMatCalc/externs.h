#ifndef externs_h__
#define externs_h__


#define MC_FALSE false
#define MC_TRUE true
#define MC_HANDLE int
#define MC_BOOL bool

#include "mc_defines.h"

extern "C" {
	int MCCOL_RunScriptFileChar(char * script_file_string);

	MC_BOOL MCC_InitializeKernel();
	int MCC_ReadLicenseInformation();
	char * MCC_GetLicenseInformation(MC_BOOL html_format);
	MC_BOOL MCC_SetApplicationDirectoryChar(char * appl_dir);
	MC_BOOL MCC_SetLicenseFilePathChar(char * appl_dir);
	//MC_BOOL MCC_SetApplicationDirectoryChar(char * appl_dir);

	int MCC_ReadDefaultSettings();
	MC_BOOL MCC_IsFlagSet(int flag);
	int MCC_SetVariableValue(char * var_name, double value);
	int MCC_GetVariableValue(char * var_name, double & value);
  int MCC_SetVariableString(char * var_name, char * value);
	MC_BOOL MCC_OpenDatabaseFileChar(char * FileName,
		int Type,
		MC_BOOL StartAsThread);
	MC_BOOL MCC_ReadDatabaseFile(int Type,
		MC_BOOL CreateCompSetPhases,
		MC_BOOL StartAsThread);
	MC_BOOL MCC_SelectDBElement(int Index, MC_BOOL Select);
	MC_BOOL MCC_SelectDBPhase(int Index, MC_BOOL Select);
	TDMGRElementData * MCC_GetDBElement(int Index);
	TDMGRPhaseData * MCC_GetDBPhase(int Index);
	int MCC_GetDBPhaseIndex(char * Phase);
	int MCC_GetDBElementIndex(char * Element);
	int MCC_GetElementIndex(char * element_name);
	TDMGRElementData * MCC_GetElement(int Index);
	MC_HANDLE MCC_GetPhaseHandleByName(char * PhaseName);

	MC_BOOL MCC_SetGlobalParameter(int Which, int Value);
	MC_BOOL MCC_SetGlobalDoubleParameter(int Which, double Value);

	MC_BOOL MCC_SetTotalAmountOfElement(int GBSIndex,
		double NewVal,
		MC_BOOL InWeightPercent,
		int AdjustType);		// check SAE_ADJUST_* in mc_defines

	int MCC_SetPhaseFlags(MC_HANDLE PhaseHandle,
		int flags,
		MC_BOOL Set);

	int MCC_GetPhysicalParameterIndex(char * param_code);
	int MCC_GetNumPhysicalParameters();
	char * MCC_GetPhysicalParameterCode(int ParameterIndex);
	MC_HANDLE MCC_GetThermFktMgrHandle(int Type,
		MC_HANDLE PhaseHandle,
		int ElementIndex);
	int MCC_GetTFM_NumFunctions(MC_HANDLE ThermFktHandle);
	// DECL_DLL_EXPORT int MCC_SetGlobalCompositionFromArray(double * composition);		//must provide array (mole fraction) with GBS_Elements.GetSize() elements

	char * MCC_GetTFM_FunctionData(MC_HANDLE ThermFktHandle,
		int FuncIndex,
		int get_flags);
	bool  MCC_SetTFM_FunctionData(MC_HANDLE ThermFktHandle,
		int FuncIndex,
		char * new_data,
		int set_flags);

	MC_HANDLE MCC_CreateNewPhaseFrom(MC_HANDLE PhaseHandle,
		int FixedSolidStatus,
		int ExtraIndex = 0);

	MC_HANDLE MCC_CreateNewCompositionSetFrom(MC_HANDLE PhaseHandle,
		char * constituents,
		char * new_name);
	//////////////////////////////////////////////////////////////////////////
	MC_HANDLE MCC_CreatePrecipitationDomain(char * name);
	MC_HANDLE MCC_GetPrecipitationDomainHandleFromName(char * name);
	int MCC_SetPrecDomainMatrixPhase(MC_HANDLE domain_handle, MC_HANDLE matrix_handle);
	int MCC_SetKineticPropertyFromHandle(MC_HANDLE ph_handle,
		int type_code,
		int int_var,
		double double_var,
		void* extra_ptr);
	int MCC_GetKineticPropertyFromHandle(MC_HANDLE ph_handle,
		int type_code,
		int & int_var,
		double & double_var,
		void*& extra_ptr);
	MC_HANDLE MCC_GetPrecDomainMatrixPhase(MC_HANDLE domain_handle);

	MC_HANDLE MCC_CreateNewPrecipitateFrom(MC_HANDLE PhaseHandle,
		int & result,
		char * alias_name = NULL);

	int MCC_InitializeKineticCalc(MC_HANDLE ph_handle,
		int num_size_classes);
	//////////////////////////////////////////////////////////////////////////
	MC_HANDLE MCC_CreateNewHeatTreatment(char * name);
	int MCC_AppendNewHTSegment(MC_HANDLE ht_handle);
	int MCC_SetHTSegmentProperty(MC_HANDLE seg_handle, 
		int prop_code, 
		int int_buff, 
		double double_buff, 
		void * void_ptr);
	MC_HANDLE MCC_GetHTSegmentHandleFromIndex(MC_HANDLE ht_handle,
		int index);
	MC_HANDLE MCC_GetHTHandleByName(char * name);
	//////////////////////////////////////////////////////////////////////////
	double MCC_SetTemperature(double NewTemp, bool ReCalcGibbsEnergy);
	bool MCC_SetAllStartValues(int flags);
	int MCC_CalcEquilibrium(bool SupressStartAsThread,int flags);

	char * MCC_WriteOutKineticInfoForPhase(MC_HANDLE phase_handle);
	int MCC_CalcKineticStep(bool SupressStartAsThread,
		int output_flags);


	MC_HANDLE MCC_CreateNewBuffer(char * B_name);
	MC_HANDLE MCC_CreateNewState(char * CL_name);
	int MCC_SelectBuffer(MC_HANDLE BufferHandle);
	bool MCC_SetGlobalHandleParameter(int Which, MC_HANDLE Value);

	char * MCC_WriteOutPrecDistributionForPhase(MC_HANDLE phase_handle);
	int MCC_GetPrecArrayEntry(MC_HANDLE ph_handle,
		int row,
		int col,
		double * destination);

  int MCC_GetNumberOfHTSegments(MC_HANDLE ht_handle);
  int MCC_StoreState(MC_HANDLE LineHandle);
  int MCC_LoadState(MC_HANDLE LineHandle);

  int MCC_GetNumberOfHeatTreatments();
  MC_HANDLE MCC_GetHTHandleByIndex(int index);
  int MCC_RemoveHTSegment(MC_HANDLE ht_handle, MC_HANDLE seg_handle);
  MC_BOOL MCC_GetGlobalParameter(int Which, int * Buff);
  MC_BOOL MCC_GetCalcVariable(char * VariableDesc,
    double * destination,
    MC_BOOL AllowOnlyOneVariable, 
    MC_BOOL NoErrorWarning);
  char * MCC_GetPhaseName(MC_HANDLE PhaseHandle);
  MC_HANDLE MCC_GetNextPhaseHandle(MC_HANDLE PreviousHandle);
  char * MCC_GetPhaseName(MC_HANDLE PhaseHandle);
  int MCC_GetPhaseFlags(MC_HANDLE PhaseHandle);
  int MCC_SetPhaseFlags(MC_HANDLE PhaseHandle,
    int flags,
    MC_BOOL Set);
}

#endif // externs_h__