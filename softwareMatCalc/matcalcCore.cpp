
/*! \file matcalcCore.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
/*	include *********************************************************/

#include <cmath> 
#include "matcalcCore.h"
#include "mc_defines.h"
#include "externs.h"
#include "externsOverloaded.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace matcalc
    {

      void MatCalcCore::RunStep(mpr::Temperature afterStepTemperature, mpr::Time afterStepTime, const std::string& nextStepScript)
      {
        assert(!isnan(presentTemperature_()));
        assert(!isnan(presentTime_()));
        //MCC_SetVariableValue("initTemperature", presentTemperature_());
        MCC_SetVariableValue("afterStepTemperature", afterStepTemperature());
//        MCC_SetVariableValue("initTime", presentTime_());
        MCC_SetVariableValue("afterStepTime", afterStepTime() - presentTime_());
  //      MCC_SetVariableString("nextStateName", afterStepTime());
        MCC_SetVariableString("outputFile", (processCodeName_ + std::to_string(afterStepTime())));
        MCCOL_RunScriptFileChar(nextStepScript);
        presentTime_ = afterStepTime;
        presentTemperature_ = afterStepTemperature;
      }

      void MatCalcCore::Init(const std::string& programPath, const std::string& licensePath, const std::string& initScript, const std::string& processCodeName)
      {
        processCodeName_ = processCodeName;
        MCC_SetApplicationDirectoryChar(programPath);
        MCC_SetLicenseFilePathChar(licensePath);
        MCC_SetLicenseFilePathChar(licensePath);
         MCC_ReadLicenseInformation();
         char * licText = MCC_GetLicenseInformation(false);
         MCC_ReadDefaultSettings();
         MCC_InitializeKernel();
         MCCOL_RunScriptFileChar(initScript);
         MCC_SetVariableString("processCodeName", processCodeName);
         std::cout << "Setting process name " << processCodeName << std::endl;
      }

      void MatCalcCore::InitModel(const std::string& initModelScript, mpr::Temperature initTemperature = 0 , mpr::Time initTime = 0 )
      {
        MCC_SetVariableValue("initTime", initTime());
        MCC_SetVariableValue("initTemperature", initTemperature());
        MCCOL_RunScriptFileChar(initModelScript);
        presentTemperature_ = initTemperature;
        presentTime_ = initTime;
      }

      void MatCalcCore::SetProcessId(const std::string& processId)
      {
        processCodeName_ = processId;
      }

      std::vector<double> MatCalcCore::GetResults()
      {
        double numberOfOutputs;
        int errorIndicator = 0;
        lo::debugLog("reading numberOfOutputs");
        errorIndicator = MCC_GetVariableValue("numberOfOutputs", numberOfOutputs);
        lo::debugLog("numberOfOutputs = " + std::to_string(numberOfOutputs) + ", errorIndicator = " + std::to_string(errorIndicator));
        if (errorIndicator != 1) throw std::runtime_error("MatCalc script does not have variable 'numberOfOutputs'");
        std::vector<double>::size_type length = std::lround(numberOfOutputs);
        std::vector<double> toRet;
        double resultTmp;
        std::string resutlTempName;
        for (std::vector<double>::size_type i = 0; i < length; i++)
        {
          resutlTempName = "variable" + std::to_string(i);
          lo::debugLog("reading " + resutlTempName);
          errorIndicator = MCC_GetVariableValueStr(resutlTempName, &resultTmp);
          lo::debugLog(resutlTempName + " = " + std::to_string(resultTmp));
          if (errorIndicator != 1) throw std::runtime_error("MatCalc did not returned proper variable value");
          toRet.push_back(resultTmp);
        }
        std::string vectorString;
        for (const auto& i : toRet)
          vectorString += std::to_string(i);
        lo::debugLog("Variables read from MatCalc: " + vectorString + " ");
        return toRet;
      }

      void MatCalcCore::SetVariables(std::vector<double> variables)
      {
        std::string resutlTempName;
        int i = 0;
        for (double &var : variables)
        {
          resutlTempName = "input_variable" + std::to_string(i++);
          int errorIndicator = 0;
          std::cout << "Setting var " << resutlTempName << " " << var<<"\n";
          errorIndicator = MCC_SetVariableValueStr(resutlTempName, &var);
        }
      }

    }
  }
}