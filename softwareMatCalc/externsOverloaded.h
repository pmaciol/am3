
/*! \file externsOverloaded.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3softwareMatCalc_externsOverloaded_h__
#define am3softwareMatCalc_externsOverloaded_h__
/*	include *********************************************************/

#include "externs.h"
#include <string>

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

template<typename T>
inline T WithString(T(*f)(char * data), const std::string& stringValue)
{
  char * charPtr = new char[stringValue.size() + 1];
  std::copy(stringValue.begin(), stringValue.end(), charPtr);
  charPtr[stringValue.size()] = '\0';
  T toRet = (*f)(charPtr);
  delete[] charPtr;
  return toRet;
}

inline int MCC_SetVariableString(char * var_name, const std::string& stringValue)
{
  char * charPtr = new char[stringValue.size() + 1];
  std::copy(stringValue.begin(), stringValue.end(), charPtr);
  charPtr[stringValue.size()] = '\0';
  int toRet = MCC_SetVariableString(var_name, charPtr);
  delete[] charPtr;
  return toRet;
}

inline int MCC_GetVariableValueStr(const std::string& var_name, double* value)
{
  char * charPtr = new char[var_name.size() + 1];
  std::copy(var_name.begin(), var_name.end(), charPtr);
  charPtr[var_name.size()] = '\0';
  int toRet = MCC_GetVariableValue(charPtr, *value);
  delete[] charPtr;
  return toRet;
}

inline int MCC_SetVariableValueStr(const std::string& var_name, double* value)
{
  char * charPtr = new char[var_name.size() + 1];
  std::copy(var_name.begin(), var_name.end(), charPtr);
  charPtr[var_name.size()] = '\0';
  int toRet = MCC_SetVariableValue(charPtr, *value);
  delete[] charPtr;
  return toRet;
}

inline int MCC_SetVariableString(char * var_name, const double& var_value)
{
  return MCC_SetVariableString(var_name, std::to_string((long double)var_value));
}

inline int MCCOL_RunScriptFileChar(const std::string& script_file_string)
{
  return WithString(&MCCOL_RunScriptFileChar, script_file_string);
//   char * nextStepScriptChar = new char[script_file_string.size() + 1];
//   std::copy(script_file_string.begin(), script_file_string.end(), nextStepScriptChar);
//   nextStepScriptChar[script_file_string.size()] = '\0'; 
//   int toRet = MCCOL_RunScriptFileChar(nextStepScriptChar);
//   delete[] nextStepScriptChar;
//   return toRet;
}

inline bool MCC_SetApplicationDirectoryChar(const std::string& appl_dir)
{
  return WithString(&MCC_SetApplicationDirectoryChar, appl_dir);
//   char * charPtr = new char[appl_dir.size() + 1];
//   std::copy(appl_dir.begin(), appl_dir.end(), charPtr);
//   charPtr[appl_dir.size()] = '\0';
//   int toRet = MCC_SetApplicationDirectoryChar(charPtr);
//   delete[] charPtr;
//   return toRet;
}

inline bool MCC_SetLicenseFilePathChar(const std::string& appl_dir)
{
  return WithString(&MCC_SetLicenseFilePathChar, appl_dir);
}
#endif // am3softwareMatCalc_externsOverloaded_h__
