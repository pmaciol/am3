#ifndef INCLUDE_DLL_IMPORT_H
#define INCLUDE_DLL_IMPORT_H

//#include "mc_types.h"

#ifdef MY_QT
DECL_DLL_EXPORT MC_BOOL MCC_SendDebugInfo(int Topic);
DECL_DLL_EXPORT int MCC_GetCalcMatrixPointers(int	&num_matrix_lines,
											  int &num_matrix_cols,
											  double ** &matrix1,
											  double *  &x_values,
											  double *  &dx_values,
											  double *  &fv_values,
											  myCStringArray* VarBuff);
#endif

//******************************************************************************
// Coline ...
//******************************************************************************

DECL_DLL_EXPORT int MCCOL_ProcessCommandLineInput(char * command_string);
DECL_DLL_EXPORT int MCCOL_ProcessCommandLineInputConst(const char * command_string);
#ifdef MY_QT
DECL_DLL_EXPORT int MCCOL_RunScriptFile(QString& script_file_string);
#endif
DECL_DLL_EXPORT int MCCOL_RunScriptFileChar(char * script_file_string);
DECL_DLL_EXPORT int MCCOL_RedirectInputAndOutput(int (*console_out_func)(char * out_string),
														int (*console_in_func)(char * buffer, char * message_text));
DECL_DLL_EXPORT int MCCOL_SetAllowThreads(int Allow);
DECL_DLL_EXPORT int MCCOL_GetMCCalcVariable(char * var_desc, double * destination);

//******************************************************************************
// Variables ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumVariableCategories();
DECL_DLL_EXPORT int MCC_GetVariableCategory(int index, int & code, char*& text);
DECL_DLL_EXPORT int MCC_GetCategoryVariables(int category_code, int & num_vars, char **& cat_vars);
DECL_DLL_EXPORT int MCC_ExpandVariableWildcards(char * var, int wildcard_index, int & num_vars, char **& exp_vars);

//******************************************************************************
// Licensing ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_ReadLicenseInformation();
DECL_DLL_EXPORT char * MCC_GetLicenseInformationDetail(int info_type);
DECL_DLL_EXPORT char * MCC_GetLicenseInformation(bool html_format);

//bool MCC_IsFullVersion();
DECL_DLL_EXPORT bool MCC_LicenseValid(bool echo_expired);
DECL_DLL_EXPORT bool MCC_IsModuleEnabled(int module_flags);
DECL_DLL_EXPORT bool MCC_CheckLicenseAndMaxSelectedElements(int echo_error);


//******************************************************************************
// speichern und laden ...
//******************************************************************************

#ifdef MY_QT
DECL_DLL_EXPORT int MCC_ReadWorkspaceFile(QDataStream & ds);	//read from existing
DECL_DLL_EXPORT int MCC_WriteWorkspaceFile(QDataStream & ds);	//append to existing

DECL_DLL_EXPORT int MCC_SetAutoSaveOptions(int AutoSaveType, QString & AutoSaveFileName, int AutoSaveIntervalSeconds);
DECL_DLL_EXPORT int MCC_RunAutoSaveProcedure();		//auto save of binary files

DECL_DLL_EXPORT int MCC_ReadDefaultSettings();		//read settings from preset/defaults.txt
#endif

//******************************************************************************
// Binary file operation ...
//******************************************************************************

#ifdef MY_QT
DECL_DLL_EXPORT int MCC_WriteBinaryFile(QString & filename);
DECL_DLL_EXPORT int MCC_ReadBinaryFile(QString & filename);
#endif
DECL_DLL_EXPORT int MCC_WriteBinaryFileChar(char * filename);
DECL_DLL_EXPORT int MCC_ReadBinaryFileChar(char * filename);

//******************************************************************************
// Testing file operation ...
//******************************************************************************

DECL_DLL_EXPORT MC_BOOL MCC_TestExistFile(QString & FileName,
											   int Type);
DECL_DLL_EXPORT MC_BOOL MCC_TestExistFileChar(char * FileName,
												int Type);


//******************************************************************************
// Initialisierung ...
//******************************************************************************

DECL_DLL_EXPORT MC_BOOL MCC_InitializeKernel();
DECL_DLL_EXPORT MC_BOOL MCC_InitializeKernelPathConst(const char * app_file_path);

#ifdef MY_QT
DECL_DLL_EXPORT MC_BOOL MCC_InitializeKernelPath(QString & application_file_path);
DECL_DLL_EXPORT MC_BOOL MCC_SetApplicationDirectory(QString & appl_dir);
DECL_DLL_EXPORT MC_BOOL MCC_SetWorkingDirectory(QString & work_dir);
DECL_DLL_EXPORT const QString & MCC_GetApplicationDirectory();
DECL_DLL_EXPORT const QString & MCC_GetWorkingDirectory();
#endif
DECL_DLL_EXPORT MC_BOOL MCC_InitializeKernelPathChar(char * application_file_path);
DECL_DLL_EXPORT MC_BOOL MCC_SetApplicationDirectoryChar(char * appl_dir);
DECL_DLL_EXPORT MC_BOOL MCC_SetWorkingDirectoryChar(char * work_dir);
DECL_DLL_EXPORT char * MCC_GetApplicationDirectoryChar();
DECL_DLL_EXPORT char * MCC_GetWorkingDirectoryChar();

DECL_DLL_EXPORT MC_BOOL MCC_InitializeExternalChar(char * app_file_path);
DECL_DLL_EXPORT MC_BOOL MCC_InitializeExternalConstChar(const char * app_file_path);
#ifdef MY_QT
DECL_DLL_EXPORT MC_BOOL MCC_InitializeExternal(QString & app_file_path);
#endif


DECL_DLL_EXPORT MC_BOOL MCC_SetEcho(int new_IsEcho);
DECL_DLL_EXPORT MC_BOOL MCC_IsFlagSet(int flag);
DECL_DLL_EXPORT int  MCC_SetFlag(int flag, MC_BOOL Set);
DECL_DLL_EXPORT MC_BOOL MCC_IsBusy();
DECL_DLL_EXPORT MC_BOOL MCC_StopCurrentActivity();
DECL_DLL_EXPORT MC_BOOL MCC_IsStopActivity();
DECL_DLL_EXPORT MC_BOOL MCC_ClearStopActivity();
DECL_DLL_EXPORT MC_BOOL MCC_IsPaused();
DECL_DLL_EXPORT int  MCC_PauseThreads();
DECL_DLL_EXPORT int  MCC_ResumeThreads();

DECL_DLL_EXPORT int  MCC_SetRemoteControlMode(MC_BOOL IsRemoteControl);


//********************************************************************************
// Knotenmanagment
//********************************************************************************

DECL_DLL_EXPORT int MCC_GetNumberOfNodeGroups();
DECL_DLL_EXPORT int MCC_GetNumberOfNodesInGroup(int NodeGrpIndex);
DECL_DLL_EXPORT MC_HANDLE MCC_GetNodeHandle(int NodeGrpIndex,
					  int NodeIndex);
DECL_DLL_EXPORT int MCC_GetNodeGroupIndexFromHandle(MC_HANDLE NodeHandle);
DECL_DLL_EXPORT int MCC_SetSelectedNode(MC_HANDLE NodeHandle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetSelectedNodeHandle();
DECL_DLL_EXPORT int MCC_GetNodeID(MC_HANDLE NodeHandle);
DECL_DLL_EXPORT char * MCC_GetNodeName(MC_HANDLE NodeHandle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetNodeByName(int NodeGroupIndex, char * NodeName);
DECL_DLL_EXPORT MC_BOOL MCC_IsValidNodeHandle(MC_HANDLE NodeHandle);

DECL_DLL_EXPORT int MCC_CreateNewNodeGroup(char * group_name);
DECL_DLL_EXPORT char * MCC_GetNodeGroupName(int GroupIndex);
DECL_DLL_EXPORT int MCC_GetNodeGroupIndex(char * group_name);
DECL_DLL_EXPORT int MCC_ProtectNodeGroup(int GroupIndex, MC_BOOL Protect);
DECL_DLL_EXPORT int MCC_GetNodeGroupProtect(int GroupIndex);
DECL_DLL_EXPORT MC_HANDLE MCC_CreateNewNode(int GroupIndex, char * name);
DECL_DLL_EXPORT int MCC_DuplicateGibbsProps(MC_HANDLE TargetNodeHandle,
											MC_HANDLE SourceNodeHandle);
DECL_DLL_EXPORT int MCC_DuplicatePrecDomainSettings(MC_HANDLE TargetNodeHandle,
													MC_HANDLE SourceNodeHandle);		//for simulation module


DECL_DLL_EXPORT int MCC_RemoveNodeGroup(int GroupIndex);
DECL_DLL_EXPORT int MCC_RemoveNode(MC_HANDLE NodeHandle);

//******************************************************************************
// Knoteneigenschaften ...
//******************************************************************************

//ein integer wert frei definierbar. wird nicht gespeichert !!!
DECL_DLL_EXPORT int MCC_SetNodeUserData(MC_HANDLE NodeHandle,
										int data);
DECL_DLL_EXPORT int MCC_GetNodeUserData(MC_HANDLE NodeHandle);
DECL_DLL_EXPORT int MCC_SetNodeGroupUserData(int GroupIndex,
											 int data);
DECL_DLL_EXPORT int MCC_GetNodeGroupUserData(int GroupIndex);

//Knotendaten, die gespeichert werden ...
DECL_DLL_EXPORT void * MCC_GetNodeExtraDataPtr(MC_HANDLE NodeHandle);



//******************************************************************************
// Datenbankfunktionen ...
//******************************************************************************

#ifdef MY_QT
DECL_DLL_EXPORT MC_BOOL MCC_OpenDatabaseFile(QString & FileName,
										  int Type,
										  MC_BOOL StartAsThread);
DECL_DLL_EXPORT const QString & MCC_GetOpenDatabaseFile(int Type);
#endif

DECL_DLL_EXPORT MC_BOOL MCC_OpenDatabaseFileChar(char * FileName,
											  int Type,
											  MC_BOOL StartAsThread);
DECL_DLL_EXPORT char * MCC_GetOpenDatabaseFileChar(int Type);

DECL_DLL_EXPORT MC_BOOL MCC_SelectDBElement(int Index, MC_BOOL Select);
DECL_DLL_EXPORT MC_BOOL MCC_SelectDBPhase(int Index, MC_BOOL Select);
DECL_DLL_EXPORT MC_BOOL MCC_ReadDatabaseFile(int Type,
										  MC_BOOL CreateCompSetPhases,
										  MC_BOOL StartAsThread);

DECL_DLL_EXPORT MC_BOOL MCC_RemoveDiffusionData();
DECL_DLL_EXPORT MC_BOOL MCC_RemovePhysicalData();


//******************************************************************************
// Neural Networking functions ...
//******************************************************************************

DECL_DLL_EXPORT MC_BOOL MCC_ReadNNDatabaseFile(char * FileName);
DECL_DLL_EXPORT MC_BOOL MCC_RemoveNNData();
DECL_DLL_EXPORT MC_BOOL MCC_HasNNData();

DECL_DLL_EXPORT int MCC_GetNumberOfNNAliases();					//from list
DECL_DLL_EXPORT char * MCC_GetNNAliasPhase(int alias_index);	//from list
DECL_DLL_EXPORT char * MCC_GetNNAliasParent(int alias_index);	//from list

DECL_DLL_EXPORT int MCC_GetNNAliasIndexFromPhaseHandle(MC_HANDLE phase_handle);	//from phase
DECL_DLL_EXPORT int MCC_SetNNAliasIndexFromPhaseHandle(MC_HANDLE phase_handle,
													   int alias_index);	//from phase


//******************************************************************************
// Gibbs-funktionen bearbeiten ...
//******************************************************************************

DECL_DLL_EXPORT MC_HANDLE MCC_GetThermFktMgrHandle(int Type,
												   MC_HANDLE PhaseHandle,
												   int ElementIndex);
DECL_DLL_EXPORT int MCC_GetTFM_NumFunctions(MC_HANDLE ThermFktHandle);
DECL_DLL_EXPORT char * MCC_GetTFM_FunctionData(MC_HANDLE ThermFktHandle,
											   int FuncIndex,
											   int get_flags);
DECL_DLL_EXPORT MC_BOOL   MCC_SetTFM_FunctionData(MC_HANDLE ThermFktHandle,
											   int FuncIndex,
											   char * new_data,
											   int set_flags);
DECL_DLL_EXPORT int MCC_LookupTFM_Function(MC_HANDLE ThermFktHandle, 
										   char * name,
										   char * constituents,
										   int element,
										   int digits);

DECL_DLL_EXPORT int MCC_AppendTFM_Parameter(char * param_string, 
											int database_type,
											MC_HANDLE & ThermFktHandle);

//******************************************************************************
// Physikalische daten bearbeiten ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumPhysicalParameters();
DECL_DLL_EXPORT int MCC_GetPhysicalParameterIndex(char * param_code);
DECL_DLL_EXPORT char * MCC_GetPhysicalParameterCode(int ParameterIndex);


//******************************************************************************
// Datenbankinformationen ...
//******************************************************************************

DECL_DLL_EXPORT MC_BOOL MCC_CheckDBReload();
DECL_DLL_EXPORT int MCC_GetNumDBElements(MC_BOOL SelectedOnly);
DECL_DLL_EXPORT int MCC_GetNumDBPhases(MC_BOOL SelectedOnly);
DECL_DLL_EXPORT TDMGRElementData * MCC_GetDBElement(int Index);
DECL_DLL_EXPORT TDMGRPhaseData * MCC_GetDBPhase(int Index);
DECL_DLL_EXPORT int MCC_GetDBElementIndex(char * Element);
DECL_DLL_EXPORT int MCC_GetDBPhaseIndex(char * Phase);

//******************************************************************************
// Globale Informationen / Variablen ...
//******************************************************************************

DECL_DLL_EXPORT void MCC_WriteOutString(char * text, MC_BOOL AppendCR=MC_FALSE, MC_BOOL SendSignal=MC_FALSE);
DECL_DLL_EXPORT MC_BOOL MCC_SendInfoOn(MC_HANDLE hWndSendTo,
									int Topic,
									void * str_vars,
									void * str_vars1,
									int num_vars,
									MC_HANDLE var1);
DECL_DLL_EXPORT MC_BOOL MCC_SetGlobalParameter(int Which, int Value);
DECL_DLL_EXPORT MC_BOOL MCC_SetGlobalDoubleParameter(int Which, double Value);
DECL_DLL_EXPORT MC_BOOL MCC_SetGlobalHandleParameter(int Which, MC_HANDLE Value);
DECL_DLL_EXPORT MC_BOOL MCC_GetGlobalParameter(int Which, int * Buff);
DECL_DLL_EXPORT MC_BOOL MCC_GetGlobalDoubleParameter(int Which, double * Buff);
DECL_DLL_EXPORT MC_BOOL MCC_GetGlobalHandleParameter(int Which, MC_HANDLE * Buff);
DECL_DLL_EXPORT int MCC_GetElementIndex(char * element_name);
DECL_DLL_EXPORT MC_BOOL MCC_SetTotalAmountOfElement(int GBSIndex,
												 double NewVal,
												 MC_BOOL InWeightPercent,
												 int AdjustType);
DECL_DLL_EXPORT MC_BOOL MCC_GetTotalAmountOfElement(int GBSIndex,
												 double * buff,
												 MC_BOOL InWeightPercent);
/*
MC_BOOL MCC_SetMatrixAmountOfElement(int GBSIndex,
								  double NewVal,
								  MC_BOOL InWeightPercent,
								  int AdjustType);
MC_BOOL MCC_GetMatrixAmountOfElement(int GBSIndex,
								  double * buff,
								  MC_BOOL InWeightPercent);
*/
DECL_DLL_EXPORT MC_BOOL MCC_SetTotalUFractionOfElement(int GBSIndex,
													double NewVal,
													int AdjustType);
DECL_DLL_EXPORT MC_BOOL MCC_GetTotalUFractionOfElement(int GBSIndex,
													double * buff);
/*
MC_BOOL MCC_SetMatrixUFractionOfElement(int GBSIndex,
									 double NewVal,
									 int AdjustType);
MC_BOOL MCC_GetMatrixUFractionOfElement(int GBSIndex,
									 double * buff);
*/
DECL_DLL_EXPORT double MCC_GetSubstitutionalElementFraction();
DECL_DLL_EXPORT MC_HANDLE MCC_GetTemporaryParsedVarHandle(char * VariableDesc);

DECL_DLL_EXPORT int MCC_GetElementCompositionControl(int GBSIndex,
													 char *& func_expresssion);
DECL_DLL_EXPORT int MCC_SetElementCompositionControl(int GBSIndex,
													 char * func_expression);

DECL_DLL_EXPORT MC_BOOL MCC_GetCalcVariable(char * VariableDesc,
										 double * destination,
										 MC_BOOL AllowOnlyOneVariable, 
										 MC_BOOL NoErrorWarning);
DECL_DLL_EXPORT double MCC_GetMCVariable(char * VariableDesc);
DECL_DLL_EXPORT MC_BOOL MCC_GetCalcVariableHelpText(char * VariableDesc,
												 char*& destination,
												 char*& unit_qual_dest);
DECL_DLL_EXPORT MC_BOOL MCC_GetCalcVariableFromNodeHandle(MC_HANDLE NodeHandle,
													   char * VariableDesc,
													   double * destination);
DECL_DLL_EXPORT MC_BOOL MCC_GetParsedCalcVarFromNodeHandle(MC_HANDLE NodeHandle,
														MC_HANDLE PVar_Handle,
														double * destination);
DECL_DLL_EXPORT MC_BOOL MCC_GetBufferVariables(char ** VariableDesc,
											double ** destination,
											int NumVars,
											MC_HANDLE BufferHandle,
											HWND hWndMsg);
DECL_DLL_EXPORT MC_BOOL MCC_GetParticleArrayVariables(char ** VariableDesc,
												   double ** destination,
												   int NumVars,
												   HWND hWndMsg);

DECL_DLL_EXPORT MC_BOOL MCC_GetParticleDistribution(MC_HANDLE ph_handle,
												 double ** ptrDoubleData,
												 int NumSizeClasses,
												 double FixedLowerValue,
												 double FixedUpperValue,
												 MC_BOOL ScaledRadius,
												 MC_BOOL ScaleFrequency,
												 int histogram_type);


//
DECL_DLL_EXPORT double MCC_GetTemperature();
DECL_DLL_EXPORT double MCC_SetTemperature(double NewTemp,
										  MC_BOOL ReCalcGibbsEnergy);
DECL_DLL_EXPORT double MCC_GetPressure();
DECL_DLL_EXPORT double MCC_SetPressure(double NewTemp,
									   MC_BOOL ReCalcGibbsEnergy);
DECL_DLL_EXPORT double MCC_GetMolarGibbsEnergy();

DECL_DLL_EXPORT double MCC_GetPhaseAmount(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT double MCC_GetUPhaseAmount(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT double MCC_GetFixedUPhaseAmount(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT MC_BOOL   MCC_SetPhaseAmount(MC_HANDLE PhaseHandle,
										  double new_amount);
DECL_DLL_EXPORT MC_BOOL   MCC_SetUPhaseAmount(MC_HANDLE PhaseHandle,
										   double new_amount);
DECL_DLL_EXPORT MC_BOOL   MCC_SetFixedUPhaseAmount(MC_HANDLE PhaseHandle,
												double new_amount);
DECL_DLL_EXPORT double MCC_GetAmountOfElementInPhase(MC_HANDLE PhaseHandle,
													 int ElementIndex,
													 MC_BOOL InWeightPercent);
DECL_DLL_EXPORT double MCC_GetUFractionOfElementInPhase(MC_HANDLE PhaseHandle,
														int ElementIndex);

DECL_DLL_EXPORT double MCC_GetStructuralDefectFraction(MC_HANDLE PhaseHandle, char *& func_expression);
DECL_DLL_EXPORT int    MCC_SetStructuralDefectFraction(MC_HANDLE PhaseHandle, char * new_expression);

DECL_DLL_EXPORT double MCC_GetInternalStressFactor(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT int    MCC_SetInternalStressFactor(MC_HANDLE PhaseHandle, double value);
DECL_DLL_EXPORT double MCC_GetInternalStressExponent(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT int    MCC_SetInternalStressExponent(MC_HANDLE PhaseHandle, double value);
DECL_DLL_EXPORT double MCC_GetInternalStressMinFactor(MC_HANDLE PhaseHandle, char *& func_expression);
DECL_DLL_EXPORT int    MCC_SetInternalStressMinFactor(MC_HANDLE PhaseHandle, char * new_expression);
DECL_DLL_EXPORT double MCC_GetInternalStressRelaxationConstant(MC_HANDLE PhaseHandle, char *& func_expression);
DECL_DLL_EXPORT int    MCC_SetInternalStressRelaxationConstant(MC_HANDLE PhaseHandle, char * new_expression);

DECL_DLL_EXPORT double MCC_GetMolarMass();
DECL_DLL_EXPORT double MCC_GetMolarMassOfPhase(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT double MCC_GetMolarVolume();
DECL_DLL_EXPORT double MCC_GetMolarVolumeOfPhase(MC_HANDLE PhaseHandle);

DECL_DLL_EXPORT double MCC_GetEquilibriumVacancyConcentration(MC_HANDLE PhaseHandle);

//warning: this function must be used with care, since it is not yet
//working correctly for complex phases!!!
//Function takes yx-values !!!
DECL_DLL_EXPORT int MCC_TransferCompositionIntoPhase(MC_HANDLE PhaseHandle, 
													 double * yx_composition_array,
													 int num_elements_in_array);

/*
double MCC_GetTracerDiffusionCoefficient(MC_HANDLE GibbsNodeHandle,
										 int ElementIndex);
double MCC_GetChemicalDiffusionCoefficient(MC_HANDLE GibbsNodeHandle,
										   int LineIndex,
										   int RowIndex);
double MCC_GetL1kjCoefficient(MC_HANDLE GibbsNodeHandle,
							  int LineIndex,
							  int RowIndex);
double MCC_GetChemicalPotential(MC_HANDLE GibbsNodeHandle,
								int ElementIndex);
double MCC_GetExtrapolatedChemPot(MC_HANDLE GibbsNodeHandle,
								  int ElementIndex,
								  double new_X_value);
double MCC_GetExtrapolatedChemPotInPhase(MC_HANDLE PhaseHandle,
										 int ElementIndex,
										 double new_X_value);
double MCC_GetExtrapolatedX(MC_HANDLE GibbsNodeHandle,
							int ElementIndex,
							double new_Potential);
double MCC_GetReducedChemicalPotential(MC_HANDLE GibbsNodeHandle,
									   int ElementIndex);
double MCC_GetExtrapolatedReducedChemPot(MC_HANDLE GibbsNodeHandle,
										 int ElementIndex,
										 double new_X_value);
double MCC_GetExtrapolatedReducedChemPotInPhase(MC_HANDLE PhaseHandle,
												int ElementIndex,
												double new_X_value);
double MCC_GetExtrapolatedXReduced(MC_HANDLE GibbsNodeHandle,
								   int ElementIndex,
								   double new_Potential);
double MCC_GetDeriveChemPot(MC_HANDLE GibbsNodeHandle,
							int LineIndex,
							int RowIndex);
double MCC_GetDeriveChemPot_2(MC_HANDLE GibbsNodeHandle,
							  int RootIndex,
							  int DeriveIndex);
double MCC_GetReducedDeriveChemPot(MC_HANDLE GibbsNodeHandle,
								   int LineIndex,
								   int RowIndex);
double MCC_GetIntrinsicDiffusionCoefficient(MC_HANDLE GibbsNodeHandle,
											int LineIndex,
											int RowIndex);
*/
DECL_DLL_EXPORT double MCC_GetDiffusionMobility(MC_HANDLE PhaseHandle, 
												int ElementIndex);
DECL_DLL_EXPORT double MCC_GetDiffusionCoefficient(MC_HANDLE PhaseHandle, 
												  int ElementIndex1,
												  int ElementIndex2);
DECL_DLL_EXPORT double MCC_GetTracerDiffusionCoefficient(int ElementIndex);
DECL_DLL_EXPORT double MCC_GetChemicalDiffusionCoefficient(int LineIndex,
														   int RowIndex);
DECL_DLL_EXPORT double MCC_GetIntrinsicDiffusionCoefficient(int LineIndex,
															int RowIndex);
DECL_DLL_EXPORT double MCC_GetL1kjCoefficient(int LineIndex,
											  int RowIndex);


DECL_DLL_EXPORT double MCC_GetGlobalChemicalPotential(int ElementIndex);
DECL_DLL_EXPORT double MCC_GetChemicalPotentialInPhase(MC_HANDLE PhaseHandle,
													   int ElementIndex);
DECL_DLL_EXPORT double MCC_GetGlobalReducedChemicalPotential(int ElementIndex);
DECL_DLL_EXPORT double MCC_GetExtrapolatedChemPot(int ElementIndex,
												  double new_X_value);
DECL_DLL_EXPORT double MCC_GetExtrapolatedChemPotInPhase(MC_HANDLE PhaseHandle,
														 int ElementIndex,
														 double new_X_value);
DECL_DLL_EXPORT double MCC_GetExtrapolatedX(int ElementIndex,
											double new_Potential);
DECL_DLL_EXPORT double MCC_GetExtrapolatedReducedChemPot(int ElementIndex,
														 double new_X_value);
DECL_DLL_EXPORT double MCC_GetExtrapolatedReducedChemPotInPhase(MC_HANDLE PhaseHandle,
																int ElementIndex,
																double new_X_value);
DECL_DLL_EXPORT double MCC_GetExtrapolatedXReduced(int ElementIndex,
												   double new_Potential);
DECL_DLL_EXPORT double MCC_GetDeriveChemPot(int LineIndex,
											int RowIndex);
DECL_DLL_EXPORT double MCC_GetDeriveChemPot_2(int RootIndex,
											  int DeriveIndex);
DECL_DLL_EXPORT double MCC_GetReducedDeriveChemPot(int LineIndex,
												   int RowIndex);
//int MCC_GetNumberOfDiffElementsInMatrix();


//******************************************************************************
//some helpers with functionality for CAFE module

DECL_DLL_EXPORT double MCC_GetCalculatedPlanarSharpInterfaceEnergy(MC_HANDLE MatrixHandle,
																   MC_HANDLE PrecHandle,
																   int force_recalc);

//******************************************************************************
// some helpers with functionality for Monte module
/*
int MCC_GetCompositionDependentEnergyOfPhase(MC_HANDLE PhaseHandle,
											 double * energy,
											 double * yx_composition_array,
											 int num_elements_in_array,
											 int type);
*/

DECL_DLL_EXPORT int MCC_SetPhaseComposition(MC_HANDLE PhaseHandle,
											double * yx_composition_array,
											int num_elements_in_array);

DECL_DLL_EXPORT double MCC_GetAtomicInteractionEnergy(MC_HANDLE PhaseHandle,
													  /*double * interaction_energy,*/
													  int element_index1,
													  int element_index2,
													  int force_recalc);



//******************************************************************************
// Berechnungen ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_CalcEquilibrium(MC_BOOL SupressStartAsThread,
										int flags);
DECL_DLL_EXPORT int MCC_CalcEquilibriumStep(MC_BOOL SupressStartAsThread);
DECL_DLL_EXPORT int MCC_CalcSolubilityTemperature(MC_BOOL SupressStartAsThread);
DECL_DLL_EXPORT int MCC_CalcT0Temperature(MC_BOOL SupressStartAsThread);
DECL_DLL_EXPORT int MCC_CalcSolubilityElementContent(MC_BOOL SupressStartAsThread);
DECL_DLL_EXPORT int MCC_CalcScheilReaction(MC_BOOL SupressStartAsThread);

DECL_DLL_EXPORT int MCC_GetCalcStatus();

#ifdef WIN32
DECL_DLL_EXPORT int MCC_CalcEquilibriumStep(int Type, 
											int Element, 
											MC_HANDLE sol_phase, 
											MC_HANDLE T0_step_phase, 
											MC_HANDLE T0_parent_phase, 
											double T0_dfm_offset, 
											double ScheilMinLiqFraction, 
											double Start, 
											double Stop, 
											double StepWidth, 
											int * IsSolidTransformation, 
											int * IsBackDiffusion, 
											int flags, 
											MC_BOOL SuppressStartAsThread);
DECL_DLL_EXPORT int MCC_CalcSolubilityTemperature(MC_HANDLE PhaseHandle,
												  MC_BOOL SupressStartAsThread);
DECL_DLL_EXPORT int MCC_CalcEquilibTieLine(MC_HANDLE SolPhaseHandle1,
										   MC_HANDLE SolPhaseHandle2,
										   int ElementIndex,
										   MC_BOOL SupressStartAsThread,
										   int output_flags);
DECL_DLL_EXPORT int MCC_CalcT0Temperature(MC_HANDLE ParentPhaseHandle,
										  MC_HANDLE ChildPhaseHandle,
										  double Energy_Difference,
										  int flags, 
										  MC_BOOL SupressStartAsThread);
DECL_DLL_EXPORT int MCC_CalcScheilReaction(MC_HANDLE DependentPhaseHandle,
										   double StartTemp,
										   double StopTemp,
										   double StepTemp,
										   double MinLiquidFraction, 
										   int * IsBackDiffusion,
										   int * IsSolidTransformation,
										   int CalcOptions,
										   int calc_flags,
										   int output_flags);

DECL_DLL_EXPORT int MCC_CalcMultiParamStep(int _params,
										   CMPS_ParamData * param_array,
										   int flags,
										   MC_BOOL SupressStartAsThread);

#endif	//WIN32

//special
DECL_DLL_EXPORT int MCC_SetSiteFractionsByChemPot(MC_BOOL SupressStartAsThread);
DECL_DLL_EXPORT char * MCC_WriteOutStepInfo();


//******************************************************************************
// diverse Hilfsfunktionen ...
//******************************************************************************

DECL_DLL_EXPORT MC_BOOL MCC_UpdateComposition(int Direction);
//missing unicode support here
DECL_DLL_EXPORT MC_BOOL MCC_SaveComposition(char * FileName);
DECL_DLL_EXPORT MC_BOOL MCC_ReadComposition(char * FileName);
DECL_DLL_EXPORT MC_BOOL MCC_ViewCompositionFile(char * FileName);

DECL_DLL_EXPORT MC_BOOL MCC_IsPhasePossible(char  * PhaseName,
										 char  * ConstituentString);

DECL_DLL_EXPORT int MCC_SetDefaultReferenceElement(char* element);
DECL_DLL_EXPORT int MCC_GetNumElements();
DECL_DLL_EXPORT TDMGRElementData * MCC_GetElement(int Index);
DECL_DLL_EXPORT int MCC_GetReferenceElementIndex();
DECL_DLL_EXPORT void MCC_SetReferenceElement(int Index);
DECL_DLL_EXPORT int MCC_GetVacancyIndex();
DECL_DLL_EXPORT MC_BOOL MCC_SetAllStartValues(int flags);
DECL_DLL_EXPORT void MCC_NormalizePhaseAmounts();
DECL_DLL_EXPORT void MCC_AdjustAmountOfRefPhase();
DECL_DLL_EXPORT int MCC_SetCompositionFromParent(MC_HANDLE ParentPhaseHandle,
												 MC_HANDLE ChildPhaseHandle,
												 int flags);
DECL_DLL_EXPORT int MCC_SetGlobalCompositionFromArray(double * composition);		//must provide array (mole fraction) with GBS_Elements.GetSize() elements
DECL_DLL_EXPORT int MCC_GetGlobalCompositionIntoArray(double * composition);		//must provide array (mole fraction) with GBS_Elements.GetSize() elements

DECL_DLL_EXPORT MC_BOOL MCC_SaveDefaultComposition();

//******************************************************************************
// variables ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumberOfVariables();
DECL_DLL_EXPORT char * MCC_GetVariableName(int index);
DECL_DLL_EXPORT int MCC_GetVariableIndex(char * name);
DECL_DLL_EXPORT int MCC_RenameVariable(char * OldName, char * NewName);
DECL_DLL_EXPORT int MCC_GetVariableValue(char * var_name, double & value);
DECL_DLL_EXPORT int MCC_SetVariableValue(char * var_name, double value);
DECL_DLL_EXPORT int MCC_RemoveVariable(char * var_name);

DECL_DLL_EXPORT int MCC_GetNumberOfStringVariables();
DECL_DLL_EXPORT char * MCC_GetStringVariableName(int index);
DECL_DLL_EXPORT int MCC_GetStringVariableIndex(char * name);
DECL_DLL_EXPORT int MCC_RenameStringVariable(char * OldName, char * NewName);
DECL_DLL_EXPORT int MCC_GetVariableString(char * var_name, char *& value);
DECL_DLL_EXPORT int MCC_SetVariableString(char * var_name, char * value);
DECL_DLL_EXPORT int MCC_GetNumVarArgs(char * format_string, int & num_args);
DECL_DLL_EXPORT int MCC_FormatVariableString(char * format_string, char ** var_ptr_array, char * var_name);
DECL_DLL_EXPORT int MCC_RemoveStringVariable(char * var_name);
DECL_DLL_EXPORT int MCC_IsValidStringVariableName(char * var_name);
DECL_DLL_EXPORT char * MCC_StringVariableRegExp();


//******************************************************************************
// functions ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumberOfFunctions();
DECL_DLL_EXPORT char * MCC_GetFunctionName(int index);
DECL_DLL_EXPORT int MCC_GetFunctionIndex(char * name);

DECL_DLL_EXPORT int MCC_RenameFunction(char * OldName, char * NewName);

DECL_DLL_EXPORT int MCC_GetFunctionDataIndex(int index,
											 char *& name,
											 char *& function_data);

DECL_DLL_EXPORT int MCC_GetFunctionData(char * name,
										char *& function_data,
										MC_BOOL EchoError);

DECL_DLL_EXPORT int MCC_GetFunctionValue(int index, double & value);

DECL_DLL_EXPORT int MCC_SetFunctionData(char * name,
										char * function_data);

DECL_DLL_EXPORT int MCC_CreateNewFunction(char * name,
										  char * function_data);

DECL_DLL_EXPORT int MCC_RemoveFunction(char * name);


//******************************************************************************
// Alle moeglichen Aktionen fuer Phasen im Gibbs-Modul...
//******************************************************************************

DECL_DLL_EXPORT int MCC_IsValidPhaseHandle(MC_HANDLE td_phase_handle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetNextPhaseHandle(MC_HANDLE PreviousHandle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetParentPhaseHandle(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetPhaseHandleByName(char * PhaseName);

DECL_DLL_EXPORT MC_BOOL MCC_SetPhaseAsReferencePhase(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetReferencePhaseHandle();
DECL_DLL_EXPORT char * MCC_GetPhaseName(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT int MCC_GetPhaseFlags(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT int MCC_SetPhaseFlags(MC_HANDLE PhaseHandle,
									  int flags,
									  MC_BOOL Set);

DECL_DLL_EXPORT MC_HANDLE MCC_CreateNewPhaseFrom(MC_HANDLE PhaseHandle,
												 int FixedSolidStatus,
												 int ExtraIndex = 0);
DECL_DLL_EXPORT MC_HANDLE MCC_CreateNewCompositionSetFrom(MC_HANDLE PhaseHandle,
														   char * constituents,
														   char * new_name);
DECL_DLL_EXPORT MC_HANDLE MCC_CreateNewPrecipitateFrom(MC_HANDLE PhaseHandle,
													   int & result,
													   char * alias_name = NULL);
DECL_DLL_EXPORT int MCC_RemovePhase(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT char * MCC_GetPhaseConstituents(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT char * MCC_GetPhaseMajorConstituents(MC_HANDLE PhaseHandle,
													 int type);
DECL_DLL_EXPORT MC_BOOL MCC_SetPhaseMajorConstituents(MC_HANDLE PhaseHandle,
												   char * MajConstString);
DECL_DLL_EXPORT MC_BOOL MCC_SetPhaseStartValues(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT int MCC_GetNumOfSiteFractionVars(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT int MCC_GetNumOfSublattices(MC_HANDLE PhaseHandle);
DECL_DLL_EXPORT int MCC_GetNumOfElementsOnSublattice(MC_HANDLE PhaseHandle,
													 int SL);
DECL_DLL_EXPORT int MCC_GetSublatticeAndPosFromSFVar(MC_HANDLE PhaseHandle,
													 int var_pos,
													 int& SL,
													 int& pos);
DECL_DLL_EXPORT int MCC_GetElementIndexOnSiteFractionPos(MC_HANDLE PhaseHandle, 
														 int SL,
														 int pos);
DECL_DLL_EXPORT int MCC_GetElementIndexOfSiteFractionVar(MC_HANDLE PhaseHandle, 
														 int var_pos);
DECL_DLL_EXPORT int MCC_ExistsElementOnSublattice(MC_HANDLE PhaseHandle,
												  char * Element,
												  int SL);
DECL_DLL_EXPORT int MCC_ExistsElementInPhase(MC_HANDLE PhaseHandle,
											 char * Element);

DECL_DLL_EXPORT int MCC_GetElementStatusInPhase(MC_HANDLE PhaseHandle,
												int ElementIndex,
												int& condition_type,
												double& condition_value);
DECL_DLL_EXPORT int MCC_SetElementStatusInPhase(MC_HANDLE PhaseHandle,
												int ElementIndex,
												int condition_type,
												double condition_value);

DECL_DLL_EXPORT int MCC_GetElementSublatticeTypeInPhase(MC_HANDLE PhaseHandle,
														int ElementIndex,
														int & type);

//******************************************************************************
// Resultate, Condition-lines, buffer ...
//******************************************************************************

DECL_DLL_EXPORT MC_HANDLE MCC_CreateNewState(char * CL_name);
DECL_DLL_EXPORT int MCC_RemoveState(MC_HANDLE LineHandle);
DECL_DLL_EXPORT int MCC_SelectState(MC_HANDLE LineHandle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetActiveStateHandle();
DECL_DLL_EXPORT MC_HANDLE MCC_GetStateHandle(char * CL_name);
DECL_DLL_EXPORT char * MCC_GetStateName(MC_HANDLE LineHandle);
DECL_DLL_EXPORT int MCC_SetStateName(MC_HANDLE LineHandle,
									 char * new_CL_name);
DECL_DLL_EXPORT int MCC_GetNumStates();
DECL_DLL_EXPORT int MCC_StoreState(MC_HANDLE LineHandle);
DECL_DLL_EXPORT int MCC_LoadState(MC_HANDLE LineHandle);
DECL_DLL_EXPORT int MCC_LoadStateFromNode(MC_HANDLE DestNodeHandle,
										  MC_HANDLE SourceNodeHandle,
										  MC_HANDLE LineHandle);

DECL_DLL_EXPORT MC_HANDLE MCC_CreateNewBuffer(char * B_name);
DECL_DLL_EXPORT int MCC_RemoveBuffer(MC_HANDLE BufferHandle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetBufferHandle(char * B_name);
DECL_DLL_EXPORT char * MCC_GetBufferName(MC_HANDLE BufferHandle);
DECL_DLL_EXPORT int MCC_SetBufferName(MC_HANDLE BufferHandle,
									  char * new_B_name);
DECL_DLL_EXPORT int MCC_GetNumBuffers();
DECL_DLL_EXPORT int MCC_GetNumLinesInBuffer(MC_HANDLE BufferHandle);
DECL_DLL_EXPORT int MCC_ClearBufferContent(MC_HANDLE BufferHandle);
DECL_DLL_EXPORT int MCC_ClearBufferRecords(MC_HANDLE BufferHandle,
										   double FromValaue,
										   double ToValue);
DECL_DLL_EXPORT int MCC_ClearBufferRecordsIndex(MC_HANDLE BufferHandle,
												int FromIndex,
												int ToIndex);
DECL_DLL_EXPORT int MCC_RemoveBufferRecord(MC_HANDLE BufferHandle,
										   MC_HANDLE LineHandle);
DECL_DLL_EXPORT int MCC_ReduceBufferRecords(MC_HANDLE BufferHandle, 
											double FromStepVal,
											double ToStepVal);
DECL_DLL_EXPORT MC_HANDLE MCC_GetActiveBufferHandle();
DECL_DLL_EXPORT int MCC_SelectBuffer(MC_HANDLE BufferHandle);

DECL_DLL_EXPORT int MCC_GetBufferType(MC_HANDLE BufferHandle);
DECL_DLL_EXPORT double MCC_GetBufferLineStepVal(MC_HANDLE BufferHandle,
												MC_HANDLE LineHandle);
DECL_DLL_EXPORT double MCC_GetBufferLineTemperature(MC_HANDLE BufferHandle,
													MC_HANDLE LineHandle);
DECL_DLL_EXPORT char * MCC_GetBufferLineComment(MC_HANDLE BufferHandle,
												MC_HANDLE LineHandle);
DECL_DLL_EXPORT double MCC_GetLastBufferLineStepValue(MC_HANDLE BufferHandle);
DECL_DLL_EXPORT int MCC_LoadStateFromBufferLine(MC_HANDLE BufferHandle,
												MC_HANDLE LineHandle);
DECL_DLL_EXPORT int MCC_StoreStateInBufferLine(MC_HANDLE BufferHandle,
											   double AktStepVal,
											   char * comment,
											   MC_BOOL Append);

//******************************************************************************
// Export ...
//******************************************************************************


#ifdef MY_QT
DECL_DLL_EXPORT int MCC_OpenExportFile(QString & filename);
DECL_DLL_EXPORT const QString & MCC_GetExportFileName();
DECL_DLL_EXPORT const QString & MCC_GetExportFilePath();
#endif
DECL_DLL_EXPORT int MCC_OpenExportFileChar(char * filename);
DECL_DLL_EXPORT char * MCC_GetExportFileNameChar();
DECL_DLL_EXPORT char * MCC_GetExportFilePathChar();
DECL_DLL_EXPORT int MCC_CloseExportFile();
//int MCC_FileExportData(char * data);
DECL_DLL_EXPORT int MCC_FileExportVariables(char * format_string, char ** var_ptr_array);
DECL_DLL_EXPORT int MCC_FileExportBufferVariables(char * format_string, char ** var_ptr_array);

DECL_DLL_EXPORT int MCC_ImportPrecipitateDistribution(MC_HANDLE precipitate_handle, char * filename);
DECL_DLL_EXPORT int MCC_ExportPrecipitateDistribution(MC_HANDLE precipitate_handle, char * filename);

//******************************************************************************
// Transformation ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumberOfTransformations();

DECL_DLL_EXPORT MC_HANDLE MCC_GetTransformationHandle(int index);
DECL_DLL_EXPORT MC_HANDLE MCC_GetTransformationHandleFromName(char * name);

DECL_DLL_EXPORT char * MCC_GetTransformationName(MC_HANDLE sp_handle);
DECL_DLL_EXPORT char * MCC_GetTransformationOptionDescription(MC_HANDLE sp_handle);

DECL_DLL_EXPORT int MCC_IsTransformationActive(MC_HANDLE sp_handle);
DECL_DLL_EXPORT int MCC_SetTransformationActive(MC_HANDLE sp_handle, MC_BOOL active);

DECL_DLL_EXPORT int MCC_SetTransformationName(MC_HANDLE sp_handle,
											  char * new_desc);

DECL_DLL_EXPORT MC_HANDLE MCC_CreateNewTransformationRecord(char * name);

DECL_DLL_EXPORT int MCC_DeleteTransformationRecord(MC_HANDLE sp_handle);

DECL_DLL_EXPORT int MCC_SetTransformationParameters(MC_HANDLE sp_handle, 
													int Type, 
													int IsActive,
													char * FromPhaseName, 
													char * ToPhaseName, 
													char * EquiPhaseName, 
													double upper_T, 
													double lower_T, 
													int TempInC, 
													double Avrami_k, 
													double Avrami_n, 
													double Koistinen_Marburger_n,
													double max_phase_fraction, 
													int table_index);

DECL_DLL_EXPORT int MCC_GetTransformationParameters(MC_HANDLE sp_handle, 
													int & Type, 
													int & IsActive,
													char *& FromPhaseName, 
													char *& ToPhaseName, 
													char *& EquiPhaseName, 
													double & upper_T, 
													double & lower_T, 
													int & TempInC, 
													double & Avrami_k, 
													double & Avrami_n, 
													double & Koistinen_Marburger_n,
													double & max_phase_fraction, 
													int & table_index);
DECL_DLL_EXPORT int MCC_GetNumberOfHeatTreatments();
									/*
int MCC_SetTransformationParameters(MC_HANDLE sp_handle,
									int Type,
									char * FromPhaseName,
									char * ToPhaseName,
									char * EquiPhaseName,
									int num_entries,
									double * T_array,
									double * ratio_array);

int MCC_GetNumberOfTransformationRecordEntries(MC_HANDLE sp_handle);

int MCC_GetTransformationParameters(MC_HANDLE sp_handle,
									int & Type,
									char * FromPhaseName,
									char * ToPhaseName,
									char * EquiPhaseName,
									int & max_entries,
									double * T_array,
									double * ratio_array);

*/

//******************************************************************************
// Tables ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumberOfTables();
DECL_DLL_EXPORT MC_HANDLE MCC_GetTableHandle(int index);
DECL_DLL_EXPORT int MCC_CreateNewTable(char * table_name);
DECL_DLL_EXPORT int MCC_RemoveTable(MC_HANDLE handle);

DECL_DLL_EXPORT int MCC_GetTableIndex(char * table_name);
DECL_DLL_EXPORT char * MCC_GetTableName(MC_HANDLE table_handle);
DECL_DLL_EXPORT int MCC_SetTableName(MC_HANDLE table_handle,
									 char * new_name);

DECL_DLL_EXPORT int MCC_RemoveTableContents(MC_HANDLE table_handle);
DECL_DLL_EXPORT int MCC_GetNumberOfTableRecords(MC_HANDLE table_handle);
DECL_DLL_EXPORT int MCC_GetTableRecord(MC_HANDLE table_handle,
									   int index,
									   double &x_value,
									   double &y_value);
DECL_DLL_EXPORT int MCC_AddTableRecord(MC_HANDLE table_handle,
									   double x_value,
									   double y_value,
									   bool over_write_same);
DECL_DLL_EXPORT double MCC_GetTableValueAtX(MC_HANDLE table_handle,		//gets table value from x-value
											double x_value);
DECL_DLL_EXPORT double MCC_GetTableValueAt(MC_HANDLE table_handle);		//gets user defined variable as x-value: e.g.: table(time)
DECL_DLL_EXPORT double MCC_GetTableDXToNextPinPoint(MC_HANDLE table_handle,
													double x_value);

DECL_DLL_EXPORT int MCC_ImportGlobalTable(MC_HANDLE table_handle, char * filename);
DECL_DLL_EXPORT int MCC_ExportGlobalTable(MC_HANDLE table_handle, char * filename);

//******************************************************************************
// Arrays ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumberOfGlobalArrays();
DECL_DLL_EXPORT MC_HANDLE MCC_GetGlobalArrayHandle(int index);
DECL_DLL_EXPORT char * MCC_GetGlobalArrayName(MC_HANDLE array_handle);
DECL_DLL_EXPORT int MCC_GetGlobalArrayIndex(char * array_name);
DECL_DLL_EXPORT int MCC_GetGlobalArrayCount();
DECL_DLL_EXPORT int MCC_CreateNewGlobalArray(char * table_name);
DECL_DLL_EXPORT int MCC_RemoveGlobalArray(MC_HANDLE handle);
DECL_DLL_EXPORT int MCC_GetNumberOfGlobalArrayRows(MC_HANDLE handle);
DECL_DLL_EXPORT int MCC_GetNumberOfGlobalArrayCols(MC_HANDLE handle);
DECL_DLL_EXPORT double MCC_GetGlobalArrayValue(MC_HANDLE array_handle,
											   int row,
											   int col);
DECL_DLL_EXPORT int MCC_SetGlobalArrayName(MC_HANDLE array_handle, char * new_name);

/*DECL_DLL_EXPORT int MCC_SetTableName(MC_HANDLE table_handle,
									 char * new_name);

DECL_DLL_EXPORT int MCC_RemoveTableContents(MC_HANDLE table_handle);
DECL_DLL_EXPORT int MCC_GetNumberOfTableRecords(MC_HANDLE table_handle);
DECL_DLL_EXPORT int MCC_GetTableRecord(MC_HANDLE table_handle,
									   int index,
									   double &x_value,
									   double &y_value);
DECL_DLL_EXPORT int MCC_AddTableRecord(MC_HANDLE table_handle,
									   double x_value,
									   double y_value,
									   bool over_write_same);
DECL_DLL_EXPORT double MCC_GetTableValueAtX(MC_HANDLE table_handle,		//gets table value from x-value
											double x_value);
DECL_DLL_EXPORT double MCC_GetTableValueAt(MC_HANDLE table_handle);		//gets user defined variable as x-value: e.g.: table(time)
*/
DECL_DLL_EXPORT int MCC_SetGlobalArraySize(MC_HANDLE array_handle,
									  int num_rows,
									  int num_cols);
DECL_DLL_EXPORT int MCC_SetGlobalArrayValue(MC_HANDLE array_handle,
									  int row,
									  int col,
									  double value);

DECL_DLL_EXPORT int MCC_ImportGlobalArray(MC_HANDLE array_handle, char * filename);
DECL_DLL_EXPORT int MCC_ExportGlobalArray(MC_HANDLE array_handle, char * filename);

//******************************************************************************
// precipitation domains ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumberOfPrecipitationDomains();
DECL_DLL_EXPORT int MCC_IsValidPrecipitationDomainHandle(MC_HANDLE pd_handle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetPrecipitationDomainHandleFromName(char * name);
DECL_DLL_EXPORT MC_HANDLE MCC_CreatePrecipitationDomain(char * name);
DECL_DLL_EXPORT MC_HANDLE MCC_GetPrecipitationDomainHandleFromIndex(int index);
DECL_DLL_EXPORT int MCC_RemovePrecipitationDomain(MC_HANDLE domain_handle);
DECL_DLL_EXPORT char * MCC_GetPrecipitationDomainName(MC_HANDLE domain_handle);
DECL_DLL_EXPORT int MCC_SetPrecipitationDomainName(MC_HANDLE domain_handle, char * new_name);
//int MCC_GetPrecipitationDomainType(MC_HANDLE domain_handle);

DECL_DLL_EXPORT int MCC_SetPrecDomainMatrixPhase(MC_HANDLE domain_handle, MC_HANDLE matrix_handle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetPrecDomainMatrixPhase(MC_HANDLE domain_handle);

DECL_DLL_EXPORT int MCC_GetNumberOfPrecipitatesInPrecDomain(MC_HANDLE domain_handle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetPrecipitateInPrecDomainFromIndex(MC_HANDLE domain_handle, int index);
//DECL_DLL_EXPORT int MCC_AttachPrecipitateToPrecipitationDomain(MC_HANDLE domain_handle, MC_HANDLE phase_handle);		//obsolete
//DECL_DLL_EXPORT int MCC_DetachPrecipitateFromPrecipitationDomain(MC_HANDLE domain_handle, MC_HANDLE phase_handle);	//obsolete

DECL_DLL_EXPORT int MCC_ResetSoluteTrappingPhaseComposition(MC_HANDLE domain_handle);

DECL_DLL_EXPORT char * MCC_WriteOutPrecipitationDomainInfo(MC_HANDLE domain_handle);

DECL_DLL_EXPORT MC_BOOL MCC_SetPrecipitationDomainAmountOfElement(MC_HANDLE domain_handle,
															   int GBSIndex,
															   double NewVal,
															   MC_BOOL InWeightPercent,
															   int AdjustType);
DECL_DLL_EXPORT MC_BOOL MCC_GetPrecipitationDomainAmountOfElement(MC_HANDLE domain_handle,
															   int GBSIndex,
															   double * buff,
															   MC_BOOL InWeightPercent);
DECL_DLL_EXPORT MC_BOOL MCC_SetPrecipitationDomainUFractionOfElement(MC_HANDLE domain_handle,
																  int GBSIndex,
																  double NewVal,
																  int AdjustType);
DECL_DLL_EXPORT MC_BOOL MCC_GetPrecipitationDomainUFractionOfElement(MC_HANDLE domain_handle,
																  int GBSIndex,
																  double * buff);


DECL_DLL_EXPORT MC_BOOL MCC_SetPrecipitationDomainGlobalComposition(MC_HANDLE domain_handle,
																 double * global_composition);			//format: GBS_Elements.x0[]
DECL_DLL_EXPORT MC_BOOL MCC_GetPrecipitationDomainGlobalComposition(MC_HANDLE domain_handle,
																 double * global_composition);			//format: GBS_Elements.x0[]
DECL_DLL_EXPORT MC_BOOL MCC_GetPrecipitationDomainMatrixComposition(MC_HANDLE domain_handle,
																 double * matrix_composition);			//format: GBS_Elements.x0[]


//******************************************************************************
// precipitation domain interactions ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumberOfPrecDomainInteractions();
DECL_DLL_EXPORT int MCC_AddPrecDomainInteraction(MC_HANDLE domain_handle_alpha, MC_HANDLE domain_handle_beta);
DECL_DLL_EXPORT int MCC_RemovePrecDomainInteraction(int index);
DECL_DLL_EXPORT int MCC_GetPrecDomainInteraction(int index, MC_HANDLE & domain_handle_alpha, MC_HANDLE & domain_handle_beta);


//******************************************************************************
// precipitation domain solute traps ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumberOfPrecDomainSoluteTraps(MC_HANDLE domain_handle);
DECL_DLL_EXPORT int MCC_AddPrecDomainSoluteTrap(MC_HANDLE domain_handle);
DECL_DLL_EXPORT int MCC_RemovePrecDomainSoluteTrap(MC_HANDLE domain_handle, MC_HANDLE trap_handle);
DECL_DLL_EXPORT int MCC_IsValidPrecDomainSoluteTrap(MC_HANDLE domain_handle, MC_HANDLE trap_handle);
DECL_DLL_EXPORT int MCC_GetPrecDomainSoluteTrapProperty(MC_HANDLE domain_handle,
											 MC_HANDLE trap_handle,
											 int prop_code,
											 int & int_buff,
											 double & double_buff,
											 void *& void_ptr);
DECL_DLL_EXPORT int MCC_SetPrecDomainSoluteTrapProperty(MC_HANDLE domain_handle,
											 MC_HANDLE trap_handle, 
											 int prop_code, 
											 int int_buff, 
											 double double_buff, 
											 void * void_ptr);

//******************************************************************************
// heat treatments ...
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumberOfHeatTreatments();
DECL_DLL_EXPORT MC_HANDLE MCC_GetHTHandleByIndex(int index);
DECL_DLL_EXPORT MC_HANDLE MCC_GetHTHandleByName(char * name);

DECL_DLL_EXPORT MC_HANDLE MCC_CreateNewHeatTreatment(char * name);
DECL_DLL_EXPORT int MCC_SetHeatTreatmentName(MC_HANDLE handle, char * name);
DECL_DLL_EXPORT int MCC_RemoveHeatTreatment(MC_HANDLE ht_handle);

DECL_DLL_EXPORT char * MCC_GetHeatTreatmentName(MC_HANDLE ht_handle);
DECL_DLL_EXPORT int MCC_GetNumberOfHTSegments(MC_HANDLE ht_handle);
DECL_DLL_EXPORT MC_HANDLE MCC_GetHTSegmentHandleFromIndex(MC_HANDLE ht_handle,
														  int index);
DECL_DLL_EXPORT int MCC_AppendNewHTSegment(MC_HANDLE ht_handle);
DECL_DLL_EXPORT int MCC_InsertHTSegment(MC_HANDLE ht_handle, int index);
DECL_DLL_EXPORT int MCC_RemoveHTSegment(MC_HANDLE ht_handle, MC_HANDLE seg_handle);

DECL_DLL_EXPORT int MCC_GetHTSegmentProperty(MC_HANDLE seg_handle,
											 int prop_code,
											 int & int_buff,
											 double & double_buff,
											 void *& void_ptr);
DECL_DLL_EXPORT int MCC_SetHTSegmentProperty(MC_HANDLE seg_handle, 
											 int prop_code, 
											 int int_buff, 
											 double double_buff, 
											 void * void_ptr);
DECL_DLL_EXPORT int MCC_ImportHeatTreatment(MC_HANDLE ht_handle, char * filename);
DECL_DLL_EXPORT int MCC_ExportHeatTreatment(MC_HANDLE ht_handle, char * filename);


//******************************************************************************
// Precipitate calculation ...
//******************************************************************************

DECL_DLL_EXPORT double MCC_GetCurrentSimulationTime();

DECL_DLL_EXPORT int MCC_CalcKineticStepEndTime(double end_time,
											   MC_BOOL SupressStartAsThread,
											   int output_flags);

DECL_DLL_EXPORT int MCC_CalcKineticTTPDiagram(MC_BOOL SupressStartAsThread,
											  int output_flags);

DECL_DLL_EXPORT int MCC_CalcKineticStep(MC_BOOL SupressStartAsThread,
										int output_flags);

//DECL_DLL_EXPORT int MCC_CalcNucleusComposition(MC_HANDLE phase_handle, int DoInit, int output_flags);
DECL_DLL_EXPORT int MCC_CalcNucleusCompositions(MC_HANDLE phase_handle, int DoInit, int output_flags);
DECL_DLL_EXPORT int MCC_SetNucleusComposition(MC_HANDLE ph_handle,
											  int num_vars,
											  char ** new_values);
DECL_DLL_EXPORT int MCC_GetNucleusComposition(MC_HANDLE ph_handle,
											  int var_index,
											  char *& func_string);

DECL_DLL_EXPORT int MCC_SetRelaxedPrecipitateState(MC_HANDLE ph_handle);

DECL_DLL_EXPORT int MCC_GetKineticPropertyFromHandle(MC_HANDLE ph_handle,
													 int type_code,
													 int & int_var,
													 double & double_var,
													 void*& extra_ptr);
DECL_DLL_EXPORT int MCC_SetKineticPropertyFromHandle(MC_HANDLE ph_handle,
													 int type_code,
													 int int_var,
													 double double_var,
													 void* extra_ptr);

DECL_DLL_EXPORT int MCC_InitializeKineticCalc(MC_HANDLE ph_handle,
											  int num_size_classes);
DECL_DLL_EXPORT int MCC_GetPrecArrayEntry(MC_HANDLE ph_handle,
										  int row,
										  int col,
										  double * destination);
DECL_DLL_EXPORT int MCC_SetPrecArrayEntry(MC_HANDLE ph_handle,
										  int row,
										  int col,
										  double new_value);

DECL_DLL_EXPORT int MCC_SetCompositionFromPrecArray(MC_HANDLE ph_handle);

DECL_DLL_EXPORT int MCC_IsPhaseKineticPrecipitate(MC_HANDLE phase_handle);
DECL_DLL_EXPORT int MCC_IsPhaseKineticParent(MC_HANDLE phase_handle);
DECL_DLL_EXPORT int MCC_IsPhaseKineticMatrix(MC_HANDLE phase_handle);
DECL_DLL_EXPORT int MCC_IsPhaseKineticTrapping(MC_HANDLE phase_handle);

DECL_DLL_EXPORT char * MCC_WriteOutKineticInfoForPhase(MC_HANDLE phase_handle);
DECL_DLL_EXPORT char * MCC_WriteOutPrecDistributionForPhase(MC_HANDLE phase_handle);
DECL_DLL_EXPORT char * MCC_WriteOutKineticInfoForSimulation();
DECL_DLL_EXPORT char * MCC_WriteOutKineticInfoForTTPSimulation();

//******************************************************************************
// TTP-Buffer manipulation
//******************************************************************************

DECL_DLL_EXPORT int MCC_GetNumRecordsInTTPBuffer();
DECL_DLL_EXPORT int MCC_GetTTPRecordData(int rec_index,
										 int& num_entries, 
										 int& T_type,
										 double& step_value);
DECL_DLL_EXPORT int MCC_ClearTTPBuffer();
DECL_DLL_EXPORT int MCC_RemoveTTPRecord(int rec_index);
DECL_DLL_EXPORT int MCC_AppendBufferToTTPBuffer(MC_HANDLE BufferHandle,
												int T_type,
												double step_value);

DECL_DLL_EXPORT int MCC_GetKineticTTPLine(char * phase_names, 
										  double line_fract,
										  int ttp_line_type, 
										  double * x_data,
										  double * y_data);
DECL_DLL_EXPORT int MCC_GetKineticTTPTemperatureLine(double step_value,
													 int & num_points,
													 double *& x_data,
													 double *& y_data);

DECL_DLL_EXPORT char * MCC_WriteOutTTPBufferContents();


#endif



