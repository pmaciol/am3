
/*! \file matcalcCore.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef matcalcCore_h__
#define matcalcCore_h__

/*	include *********************************************************/

#include <limits>
#include <boost/shared_ptr.hpp>
#include "../softwareMatCalcGeneric/contracts.h"
#include "../models/dataStructures.h"
#include "../modelPrecipitation/contracts.h"
#include "../models/models.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpc = am3::model::precipitations;
namespace mo = am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace matcalc
    {
      class matCalc
      {
      public:
        void InitSoftware();

      };

      class MatCalcCore
      {
      public:
        MatCalcCore() : presentTemperature_(std::numeric_limits<double>::quiet_NaN()), presentTime_(std::numeric_limits<double>::quiet_NaN()){};
        void RunStep(mpr::Temperature afterStepTemperature, mpr::Time afterStepTime, const std::string& nextStepScript);
        void Init(const std::string& programPath, const std::string& licensePath, const std::string& initScript, const std::string& processCodeName);
        void InitModel(const std::string& initModelScript, mpr::Temperature initTemperature, mpr::Time initTime);
        void SetProcessId(const std::string& processId);
        std::vector<double> GetResults();
        void SetVariables(std::vector<double> variables);



      protected:
        mpr::Temperature presentTemperature_;
        mpr::Time presentTime_;
        std::string processCodeName_;
      };

    } //matcalc
  } // software
} //am3

#include "matcalcCore_impl.h"
#endif // matcalcCore_h__
