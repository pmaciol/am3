
/*! \file matcalcController.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef matcalcController_h__
#define matcalcController_h__
/*	include *********************************************************/

#include <string>
#include <boost/shared_ptr.hpp>
#include "../models/dataStructures.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace matcalc
		{
      struct PrecipitationStructure
      {
        mpr::PrecipitationsMeanRadius precipitationsMeanRadius;
      };

			class MatcalcController
      {
      public:
        MatcalcController(const std::string workSpacePath, const std::string licensePath, const std::string initScript);
        boost::shared_ptr<PrecipitationStructure> FirstStep( mpr::Time time, mpr::Temperature previousTemperature,mpr::Temperature temperature);
        boost::shared_ptr<PrecipitationStructure> NextStep(mpr::Time time, mpr::Temperature temperature,mpr::Temperature previousTemperature);
        boost::shared_ptr<PrecipitationStructure> Init(mpr::Temperature temperature);

        boost::shared_ptr<PrecipitationStructure> CalculateOutput();

      };
		} //matcalc
	} //software
} //am3
#endif // matcalcController_h__
