
/*! \file ThermostatClient.h **************************************************
 * \author		Piotr Maciol, Stanislaw Jedrusik
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef ThermostatClient_h__
#define ThermostatClient_h__
/*	include *********************************************************/

#include "stdafx.h"
#include "../knowledgeRebitProxy/soapBasicHttpBinding_USCOREIReasoningServiceProxy.h"
#include "../knowledgeRebitProxy/BasicHttpBinding_USCOREIReasoningService.nsmap"
#include "../knowledgeRebitProxy/stdsoap2.h" 
#include "../knowledgeRebitProxy/knowledgeBaseReader.h"
#include "../knowledgeRebitProxy/reasoner.h"
#include "../knowledgeRebitProxy/reasoningServiceProxyWrapper.h"


#include "../knowledgeRebit/rebitKnowledgeStructs.h"

#include "../knowledge/kbsInterfaces.h"

/*	using ***********************************************************/

//namespace kn=am3::knowledge;

/*	extern **********************************************************/
/*	classes *********************************************************/

class What{};

class ThermostatClient: public am3::knowledge::rebit::RebitClient
{
protected:
  Logger *logger;
  ReasoningServiceProxyWrapper *rsw;
  std::string kb;
  int ruleSetId;
  ReasoningDataProvider *provider ;
  ResponseDTO *resp;
  char* serviceEndPoint;
  char*  thermostatPath;

  boost::shared_ptr<am3::knowledge::I4KBS<VariableDTO> > i4KBS_;
public:
	void Init(boost::shared_ptr<am3::knowledge::I4KBS<VariableDTO> > i4KBS)
	{
		i4KBS_=i4KBS;
	}
	ThermostatClient(string filePath, string serviceAdress)
	{
  thermostatPath = new char[filePath.size() + 1];
  std::copy(filePath.begin(), filePath.end(), thermostatPath);
  thermostatPath[filePath.size()] = '\0'; // don't forget the terminating 0

  serviceEndPoint = new char[serviceAdress.size() + 1];
  std::copy(serviceAdress.begin(), serviceAdress.end(), serviceEndPoint);
  serviceEndPoint[serviceAdress.size()] = '\0'; // don't forget the terminating 0


  // wyniki b�d� wy�wietlane na ekranie
  this->logger = new ScreenLogger(); // alernatywnie NullLoger() - jesli nie chcemy wynik�w, lub dowolny w�asny (np. logger zapisujacy do pliku)

  rsw = new ReasoningServiceProxyWrapper(serviceEndPoint, logger);
  rsw->Hello();

  // odczytaj baz� wiedzy "theromstat"
  kb = KnowledgeBaseReader::getXmlFromFile(thermostatPath); 
  ruleSetId = 1;	// numer ruleset-a w bazie wiedzy (numeracja zaczyna si� od 1)


  // dla bazy wiedzy "thermostat" utw�rz obiekt dostarczaj�cy przyk�adow� hipotez�, warto�ci zmiennych �r�d�owych i warto�ci zmiennych na ��danie (w trakcie interakcji)
  // !!!UWAGA!!!
  // Dla ka�dej bazy wiedzy trzeba przygotowa� oddzieln� klas�. Musi ona dziedziczy� po ReasoningDataProvider
  // Jej zadanie polega na "dostarczaniu" danych startowych, danych �adanych przez silnik wnioskuj�cy oraz hipotez
  //provider = new TermostatDataProvider();
  resp = NULL;
	};
//   //! Implementation of am3::knowledge::rebit::RebitClient
	virtual int SetFact (const VariableDTO&  prop){return -1;};
   virtual int SetSource(am3::knowledge::rebit::VariableProvider* varProvider){return -1;};
   virtual int GetVariableValue(VariableDTO & prop)
   {
  std::string result;
  // 1. Wnioskowanie pe�ne
  MixReasoner *mixReasoner = new MixReasoner();
  ResponseDTO *resp = mixReasoner->Run(serviceEndPoint, &kb, &ruleSetId, provider, logger);

  VariableDTO *varDTO = &(*resp->InteractionVariables)[0];

  prop.Id = varDTO->Id;
  prop.Value = varDTO->Value;

  return -1;
   }
	   ;
};
#endif // ThermostatClient_h__
