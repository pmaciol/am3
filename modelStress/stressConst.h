
/*! \file stressConst.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3modelStress_stressConst_h__
#define am3modelStress_stressConst_h__
/*	include *********************************************************/

#include "../modelTemplates/modelsTemplates.h"
#include "contracts.h"

/*	using ***********************************************************/

namespace pst = am3::problem::stress;
namespace mot = am3::model::templates;

/*	extern **********************************************************/



/*	classes *********************************************************/


namespace am3
{
	namespace model
	{
		namespace stress
		{
//			CONTRACT(StressThermalAthermalInput, 2, mpr::Coordinates2D, mpr::StressAthermal, mpr::StressThermal);

			template<typename INPUT>
			class StressConstant
				: public pst::contract::StressContract
				, public mo::InputTypelist < INPUT >//<StressThermalAthermalInput>
			{
			public:
				typedef mot::EmptyContract2D Inputs;

				StressConstant(const double& stress)
				{
					constValue_(stress);
				};

				mpr::Stress GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::Stress *const)
				{
					mpr::Stress toRet; toRet(0.1);
					return toRet;
				}
			protected:
				mpr::Stress constValue_;
			};
// 
			template<typename INPUT>
			class StressConstantWithDerivatives
				: public pst::contract::StressStressDStrainStressDStrainRateContract
				, public mo::InputTypelist < INPUT >//<StressThermalAthermalInput>
			{
			public:
				typedef mot::EmptyContract2D Inputs;

				StressConstantWithDerivatives(const double& stress)
				{
					constValue_(stress);
					stDstr_(0);
					stDstrRate_(0);
				};

				mpr::Stress GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::Stress  *const)
				{
					return constValue_;
				}
				mpr::StressDStrain GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrain *const)
				{
					return stDstr_;
				}
				mpr::StressDStrainRate GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrainRate *const)
				{
					return stDstrRate_;
				}
			protected:
				mpr::Stress  constValue_;
				mpr::StressDStrain stDstr_;
				mpr::StressDStrainRate stDstrRate_;
			};


		}
	}
}
#endif // am3modelStress_stressConst_h__
