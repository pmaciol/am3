
/*! \file stressAthermalDislocationsEq.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef stressAthermalDislocationsEq_h__
#define stressAthermalDislocationsEq_h__
/*	include *********************************************************/

#include "./contracts.h"
#include "../modelDislocations/contracts.h"
#include "../models/models.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

/*	using ***********************************************************/

namespace pst = am3::problem::stress;
namespace pdi = am3::problem::dislocations;
namespace mo  = am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace stress
    {
      CONTRACT(		StressAthermalDislocationsEqInput			        	,3,mpr::Coordinates2D, pdi::DislocationsDensityRandom, pdi::DislocationsDensityWall, mpr::ShearModulus);
      CONTRACT(		StressAthermalDislocationsPrecipitationsEqInput	,4,mpr::Coordinates2D, pdi::DislocationsDensityRandom, pdi::DislocationsDensityWall, mpr::PrecipitationsMeanRadius, mpr::ShearModulus);

      struct StressAthermalDislocationsEq_Param
      {
        StressAthermalDislocationsEq_Param(const double& alpha1, const double& alpha2, const double& burgersVector) : alpha1_(alpha1), alpha2_(alpha2), burgersVector_(mo::castToType<pdi::BurgersVector>(burgersVector)){};
        const double alpha1_, alpha2_;
        pdi::BurgersVector  burgersVector_;
      };

	  template<typename INPUT>
      class StressAthermalDislocationsEq
        : public pst::contract::AthermalStressContract
        , public mo::InputTypelist<INPUT>//s<StressAthermalDislocationsEqInput>
      {
      public:
        typedef StressAthermalDislocationsEqInput Inputs;

        StressAthermalDislocationsEq(const StressAthermalDislocationsEq_Param& param) : alpha1_(param.alpha1_), alpha2_(param.alpha2_), burgersVector_(param.burgersVector_){};
        StressAthermalDislocationsEq(const double& alpha1, const double& alpha2, const double& burgersVector): alpha1_(alpha1), alpha2_(alpha2), burgersVector_(mo::castToType<pdi::BurgersVector>(burgersVector)){}; // suggested: 0.3,3.0,0.286e-9
        mpr::StressAthermal GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressAthermal *const )
        {
          pdi::DislocationsDensityRandom rhoR = bus_->GetFromDatabus<pdi::DislocationsDensityRandom>(point);
          pdi::DislocationsDensityWall rhoW = bus_->GetFromDatabus<pdi::DislocationsDensityWall>(point);
//          pdi::BurgersVector rhoW  = Get<pdi::DislocationsDensityWall,BaseCoordinates>::InPoint(point);
          mpr::ShearModulus sm = bus_->GetFromDatabus<mpr::ShearModulus>(point);

          double tmp = alpha1_ * sqrt(rhoR());
          tmp += alpha2_ * sqrt(rhoW());
          tmp *= sm() * burgersVector_() ;
          mpr::StressAthermal toRet; toRet(tmp);
          return toRet;
        }
      protected:
        const double alpha1_, alpha2_;
        pdi::BurgersVector  burgersVector_;
      };

//       template<typename INPUT>
//       class StressAthermalDislocationsEqTempl
//         : StressAthermalDislocationsEq
//         , public mo::InputTypelist<INPUT>
//       {
//       public:
//         typedef typename StressAthermalDislocationsEqInput Inputs;
//         typedef typename pst::contract::AthermalStressContract Contract;
// 
//         StressAthermalDislocationsEqTempl(const double& alpha1, const double& alpha2, const double& burgersVector):StressAthermalDislocationsEq(alpha1, alpha2,burgersVector){};
//       };

//       template<>
//       class StressAthermalDislocationsEqTempl<am3::databus::MockDatabus>
//         : StressAthermalDislocationsEq
//         , public mo::InputTypelist<am3::databus::MockDatabus>
//       {
//       public:
//         typedef StressAthermalDislocationsEqInput Inputs;
//         typedef pst::contract::AthermalStressContract Contract;
//       };
      //////////////////////////////////////////////////////////////////////////

	  template<typename INPUT>
      class StressAthermalDislocationsPrecipitationsEq
        : public pst::contract::AthermalStressContract
        , public mo::InputTypelist<INPUT>//s<StressAthermalDislocationsPrecipitationsEqInput>
      {
      public:
        StressAthermalDislocationsPrecipitationsEq(const double& alpha1, const double& alpha2, const double& burgersVector): alpha1_(alpha1), alpha2_(alpha2), burgersVector_(mo::castToType<pdi::BurgersVector>(burgersVector)){}; // suggested: 0.3,3.0,0.286e-9
        mpr::StressAthermal GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressAthermal *const )
        {
          pdi::DislocationsDensityRandom rhoR = Get<pdi::DislocationsDensityRandom,BaseCoordinates>::InPoint(point);
          pdi::DislocationsDensityWall rhoW  = Get<pdi::DislocationsDensityWall,BaseCoordinates>::InPoint(point);
          mpr::PrecipitationsMeanRadius prAm = Get<mpr::PrecipitationsMeanRadius,BaseCoordinates>::InPoint(point);
          mpr::ShearModulus sm = Get<mpr::ShearModulus,BaseCoordinates>::InPoint(point);

          double tmp = alpha1_ * sqrt(rhoR());
          tmp += alpha2_ * sqrt(rhoW());
          tmp *= sm() * burgersVector_() ;
          mpr::StressAthermal toRet; toRet(tmp);
          return toRet;
        }
      protected:
        const double alpha1_, alpha2_;
        pdi::BurgersVector  burgersVector_;
      };
    }
  }
}
#endif // stressAthermalDislocationsEq_h__
