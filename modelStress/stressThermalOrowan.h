
/*! \file stressThermalOrowan.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef stressThermalOrowan_h__
#define stressThermalOrowan_h__
/*	include *********************************************************/

#include "./contracts.h"
#include "../modelDislocations/contracts.h"
#include "../models/models.h"
#include "../models/inputTypelists.h"
#include <math.h>

/*	using ***********************************************************/

namespace pst = am3::problem::stress;
namespace pdi = am3::problem::dislocations;
namespace mo  = am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace stress
    {
      CONTRACT(		StressThermalOrowanInput			        	,7,mpr::Coordinates2D, pdi::ActivationVolume, pdi::ActivationEnergy, pdi::InterObstacleLimit, pdi::FrequencyDominantObstacle, pdi::DislocationDensityMobile, mpr::EffectiveStrainRate, mpr::Temperature, pdi::BurgersVector);
      CONTRACT(		StressThermalOrowanReducedInput       	,3,mpr::Coordinates2D, mpr::EffectiveStrainRate, mpr::Temperature);

	  template<typename INPUT>
      class StressThermalOrowan
        : public pst::contract::ThermalStressContract
        , public mo::InputTypelist<INPUT>//s<StressThermalOrowanInput>
      {
        mpr::StressThermal GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressThermal *const )
        {
          pdi::ActivationVolume aV = Get<pdi::ActivationVolume,BaseCoordinates>::InPoint(point);
          pdi::ActivationEnergy qObst  = Get<pdi::ActivationEnergy,BaseCoordinates>::InPoint(point);
          pdi::InterObstacleLimit d = Get<pdi::InterObstacleLimit,BaseCoordinates>::InPoint(point);
          pdi::FrequencyDominantObstacle f = Get<pdi::FrequencyDominantObstacle,BaseCoordinates>::InPoint(point);
          pdi::DislocationDensityMobile rhom = Get<pdi::DislocationDensityMobile,BaseCoordinates>::InPoint(point);
          mpr::EffectiveStrainRate sr = Get<mpr::EffectiveStrainRate,BaseCoordinates>::InPoint(point);
          pdi::BurgersVector burgers_ = Get<pdi::BurgersVector,BaseCoordinates>::InPoint(point);
          mpr::Temperature temperature = Get<mpr::Temperature,BaseCoordinates>::InPoint(point);


          temperature(temperature()+293.15);

          double tmp = log(sr() / (d() * f() * rhom() * burgers_()));
          tmp *= mpr::kBoltzmann * temperature();
          tmp += qObst();
          tmp /= aV();
          mpr::StressThermal toRet; toRet(tmp);
          return toRet;
        }
      };

      struct StressThermalOrowanReduced_Params
      {
        StressThermalOrowanReduced_Params(const double &  activationVolume, const double & activationEnergy, const double & interObstacleLimit, const double & frequencyDominantObstacle, const double & dislocationDensityMobile, const double& burgers)
          : aV_(mo::castToType<pdi::ActivationVolume>(activationVolume))
          , qObst_(mo::castToType<pdi::ActivationEnergy>(activationEnergy))
          , d_(mo::castToType<pdi::InterObstacleLimit>(interObstacleLimit))
          , f_(mo::castToType<pdi::FrequencyDominantObstacle>(frequencyDominantObstacle))
          , rhom_(mo::castToType<pdi::DislocationDensityMobile>(dislocationDensityMobile))
          , burgers_(mo::castToType<pdi::BurgersVector>(burgers))
        {};
        pdi::ActivationVolume aV_;
        pdi::ActivationEnergy qObst_;
        pdi::InterObstacleLimit d_;
        pdi::FrequencyDominantObstacle f_;
        pdi::DislocationDensityMobile rhom_;
        pdi::BurgersVector burgers_;
      };
      
      template<typename INPUT>
        class StressThermalOrowanReduced
          : public pst::contract::ThermalStressContract
          , public mo::InputTypelist<INPUT>//s<StressThermalOrowanReducedInput>
        {
        public:
          typedef StressThermalOrowanReducedInput Inputs;

          StressThermalOrowanReduced(const double &  activationVolume, const double & activationEnergy, const double & interObstacleLimit, const double & frequencyDominantObstacle, const double & dislocationDensityMobile, const double& burgers)
            : aV_(mo::castToType<pdi::ActivationVolume>(activationVolume))
            , qObst_(mo::castToType<pdi::ActivationEnergy>(activationEnergy))
            , d_(mo::castToType<pdi::InterObstacleLimit>(interObstacleLimit))
            , f_(mo::castToType<pdi::FrequencyDominantObstacle>(frequencyDominantObstacle))
            , rhom_(mo::castToType<pdi::DislocationDensityMobile>(dislocationDensityMobile))
            , burgers_(mo::castToType<pdi::BurgersVector>(burgers))
          {};

          StressThermalOrowanReduced(const StressThermalOrowanReduced_Params &  params)
            : aV_(params.aV_)
            , qObst_(params.qObst_)
            , d_(params.d_)
            , f_(params.f_)
            , rhom_(params.rhom_)
            , burgers_(params.burgers_)
          {};

          mpr::StressThermal GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::StressThermal *const )
          {
            
            mpr::EffectiveStrainRate sr = bus_->GetFromDatabus<mpr::EffectiveStrainRate>(point);
            mpr::Temperature temperature = bus_->GetFromDatabus<mpr::Temperature>(point);

            double tmp = log(sr() / (d_() * f_() * rhom_() * burgers_()));
            tmp *= mpr::kBoltzmann * temperature();
            tmp += qObst_();
            tmp /= aV_();
            mpr::StressThermal toRet; toRet(tmp);
            return toRet;
          }
        protected:
          pdi::ActivationVolume aV_;
          pdi::ActivationEnergy qObst_;
          pdi::InterObstacleLimit d_;
          pdi::FrequencyDominantObstacle f_;
          pdi::DislocationDensityMobile rhom_;
          pdi::BurgersVector burgers_;
      };

//         template<typename INPUT>
//         class StressThermalOrowanReducedTempl
//           : public StressThermalOrowanReduced
// 		  , public mo::InputTypelist<INPUT>
//         {
// 
//         public:
//           typedef typename StressThermalOrowanInput Inputs;
//           typedef typename pst::contract::ThermalStressContract Contract;
// 
//           StressThermalOrowanReducedTempl(const double &  activationVolume, const double & activationEnergy, const double & interObstacleLimit, const double & frequencyDominantObstacle, const double & dislocationDensityMobile, const double& burgers)
//             : StressThermalOrowanReduced(activationVolume,activationEnergy, interObstacleLimit, frequencyDominantObstacle, dislocationDensityMobile, burgers) {};
//         };
// 
//         template<>
//         class StressThermalOrowanReducedTempl<am3::databus::MockDatabus>
//           : public StressThermalOrowanReduced
//           , public mo::InputTypelist<am3::databus::MockDatabus>
//         {
//         public:
//           typedef StressThermalOrowanReducedInput Inputs;
//           typedef pst::contract::ThermalStressContract Contract;
//         };
    }
  }
}
#endif // stressThermalOrowan_h__
