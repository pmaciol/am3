
/*! \file stressProxy.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3modelStress_stressProxy_h__
#define am3modelStress_stressProxy_h__
/*	include *********************************************************/

#include "../modelTemplates/innerSideInterfacesMacro.h"
#include "../modelTemplates/modelsTemplates.h"
#include "../models/inputTypelists.h"
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "./contracts.h"

/*	using ***********************************************************/


/*	extern **********************************************************/

namespace mot = am3::model::templates;
namespace mo  = am3::model;
namespace pst = am3::problem::stress;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;

/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace stress
    {
      // Gets stresses from databus and provide them to FEM software
      template<typename INPUT>
      class  StressProxy
        : public mot::OneStepOneElementMacroStressWithDerivatives
        , public mo::InputTypelist < INPUT >
      {
      public:
        typedef pst::contract::StressStressDStrainStressDStrainRateContract Inputs;
        typedef typename mot::EmptyContract2D Contract;
        typedef mpr::Coordinates2D T_COORDINATE;

        StressProxy();

        void SetMockPoint(mpt::Point<T_COORDINATE>*  point);

        mpr::YieldStress GetState(mpt::Point<T_COORDINATE>* point, const mpr::YieldStress* const typeTrait);
        mpr::StressDStrain GetState(mpt::Point<T_COORDINATE>* point, const mpr::StressDStrain* const typeTrait);
        mpr::StressDStrainRate GetState(mpt::Point<T_COORDINATE>* point, const mpr::StressDStrainRate* const typeTrait);

        virtual mpr::YieldStress GetStress() const;
        virtual mpr::StressDStrain GetStressDStrain() const;
        virtual mpr::StressDStrainRate GetStressDStrainRate() const;

      protected:
        mpt::Point<T_COORDINATE>*  mockPoint_;
      };

      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////

      template<typename INPUT>
      void am3::model::stress::StressProxy<INPUT>::SetMockPoint(mpt::Point<T_COORDINATE>* point)
      {
        mockPoint_ = point;
      }

      template<typename INPUT>
      mpr::StressDStrainRate am3::model::stress::StressProxy<INPUT>::GetStressDStrainRate() const
      {
        return bus_->GetFromDatabus<mpr::StressDStrainRate>(mockPoint_);
      }

      template<typename INPUT>
      mpr::StressDStrain am3::model::stress::StressProxy<INPUT>::GetStressDStrain() const
      {
        return bus_->GetFromDatabus<mpr::StressDStrain>(mockPoint_);
      }

      template<typename INPUT>
      mpr::YieldStress am3::model::stress::StressProxy<INPUT>::GetStress() const
      {
        mpr::YieldStress toRet;
        toRet(bus_->GetFromDatabus<mpr::Stress>(mockPoint_).Value());
        return toRet;
      }

      template<typename INPUT>
      mpr::StressDStrain am3::model::stress::StressProxy<INPUT>::GetState(mpt::Point<T_COORDINATE>* point, const mpr::StressDStrain* const typeTrait)
      {
				return GetStressDStrainRate();
      }

      template<typename INPUT>
      mpr::StressDStrainRate am3::model::stress::StressProxy<INPUT>::GetState(mpt::Point<T_COORDINATE>* point, const mpr::StressDStrainRate* const typeTrait)
      {
				return GetStressDStrainRate();
      }

      template<typename INPUT>
      mpr::YieldStress am3::model::stress::StressProxy<INPUT>::GetState(mpt::Point<T_COORDINATE>* point, const mpr::YieldStress* const typeTrait)
      {
				return GetStress();
      }

      template<typename INPUT>
      am3::model::stress::StressProxy<INPUT>::StressProxy()
      {
        const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5, 0.5);
        const mpr::Time t = mpr::Time(0.0);
        mockPoint_ = new mpt::ComputedPoint<mpr::Coordinates2D>(coord, t);
      }
    }
  }
}

#endif // am3modelStress_stressProxy_h__
