
/*! \file shearModulus.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef shearModulus_h__
#define shearModulus_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
//#include "../models/modelsTemplates.h"
#include "../modelTemplates/modelsTemplates.h"
#include "./contracts.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mo  = am3::model;
namespace mot = am3::model::templates;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace stress
    {
//      CONTRACT(		StressThermalAthermalInput			        	,2,mpr::Coordinates2D, mpr::StressAthermal, mpr::StressThermal);

      //typedef mo::templates::ConstantValueModel<mpr::ShearModulus, am3::problem::stress::contract::ShearModulusContract> ShearModulusConst;

      template <typename INPUT>
      struct ShearModulusConst
        : public mot::ConstantValueModel < mpr::ShearModulus, am3::problem::stress::contract::ShearModulusContract >
        , public mo::InputTypelist < INPUT >
      {
        typedef mot::EmptyContract2D Inputs;
        ShearModulusConst(const mpr::ShearModulus& sMod) : mot::ConstantValueModel < mpr::ShearModulus, am3::problem::stress::contract::ShearModulusContract >(sMod){};
        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> mpr::ShearModulus Get<mpr::ShearModulus>(mpt::Point<mpr::Coordinates2D> * point);
      };
    }
  }
}
#endif // shearModulus_h__
