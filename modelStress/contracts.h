
/*! \file contracts.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef stressContracts_h__
#define stressContracts_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../models/contractMacros.h"
#include "../models/macrosSimplifiedModels.h"

/*	using ***********************************************************/

namespace moc=am3::model::contract;
namespace mpr=am3::model::properties;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace problem
  {
    namespace stress
    {


       namespace contract
       {
         CONTRACT(		ThermalStressContract			        	,1,mpr::Coordinates2D, mpr::StressThermal);
         CONTRACT(		AthermalStressContract			        ,1,mpr::Coordinates2D, mpr::StressAthermal);
         CONTRACT(		StressContract			                ,1,mpr::Coordinates2D, mpr::Stress);
         CONTRACT(		ShearModulusContract			          ,1,mpr::Coordinates2D, mpr::ShearModulus);
         CONTRACT( StressStressDStrainStressDStrainRateContract, 3, mpr::Coordinates2D, mpr::Stress, mpr::StressDStrain, mpr::StressDStrainRate);

//         CONTRACT(		RandomDislocationsContract			        ,1,mpr::Coordinates2D, DislocationsDensityRandom);
//         CONTRACT(		ShearRateTemperatureInput    	  ,2,mpr::Coordinates2D, mpr::ShearRate,mpr::Temperature);
//         CONTRACT(		ShearRateInput		  		,1,mpr::Coordinates2D, mpr::ShearRate);
//         CONTRACT(		ShearRateTemperatureInput    	  		  	,2,mpr::Coordinates2D, mpr::ShearRate,mpr::Temperature);
      }
    }
  }
}
#endif // stressContracts_h__
