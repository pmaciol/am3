
/*! \file stressSum.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef stressSum_h__
#define stressSum_h__
/*	include *********************************************************/

#include "../models/inputTypelists.h"
#include "./contracts.h"

/*	using ***********************************************************/

namespace pst = am3::problem::stress;
namespace mpr = am3::model::properties;
namespace mo  = am3::model;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace stress
    {
      CONTRACT(		StressThermalAthermalInput			        	,2,mpr::Coordinates2D, mpr::StressAthermal, mpr::StressThermal);

	  template<typename INPUT>
      class StressThermalAthermalTaylor
        : public pst::contract::StressContract
        , public mo::InputTypelist<INPUT>//<StressThermalAthermalInput>
      {
      public:
        typedef typename StressThermalAthermalInput Inputs;


        StressThermalAthermalTaylor(const double& taylorFactor): taylorFactor_(taylorFactor){};
        mpr::Stress GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::Stress *const )
        {
          //mpr::StressThermal tStress = Get<mpr::StressThermal,BaseCoordinates>::InPoint(point);
          mpr::StressThermal tStress = bus_->GetFromDatabus<mpr::StressThermal>(point);
          mpr::StressAthermal aStress = bus_->GetFromDatabus<mpr::StressAthermal>(point);


          double tmp = tStress() + aStress()/**1e-9*/;
          tmp *= 1e-6;
          tmp *= taylorFactor_;

//          tmp*= 1e-10;
          mpr::Stress toRet; toRet(tmp);
//          mpr::Stress toRet; toRet(tStress());
          return toRet;
        }
      protected:
        const double taylorFactor_;

      };

//       template <typename INPUTS>
//       class StressThermalAthermalTaylorTempl
// //        : public pst::contract::StressContract
//         : public mo::InputTypelist<INPUTS>
//         , public StressThermalAthermalTaylor
//       {
//       public:
//         typedef typename StressThermalAthermalInput Inputs;
//         typedef typename pst::contract::StressContract Contract;
// 
//         StressThermalAthermalTaylorTempl(const double& taylorFactor): StressThermalAthermalTaylor(taylorFactor){};
// //         StressThermalAthermalTaylor(const double& taylorFactor): taylorFactor_(taylorFactor){};
// //         mpr::Stress GetState(mpt::Point<mpr::Coordinates2D> * point,const mpr::Stress *const )
// //         {
// //           mpr::StressThermal tStress = Get<mpr::StressThermal,BaseCoordinates>::InPoint(point);
// //           mpr::StressAthermal aStress  = Get<mpr::StressAthermal,BaseCoordinates>::InPoint(point);
// // 
// //           double tmp = tStress() + aStress();
// //           tmp *= taylorFactor_;
// //           mpr::Stress toRet; toRet(tmp);
// //           //          mpr::Stress toRet; toRet(tStress());
// //           return toRet;
// //         }
// //       protected:
// //         const double taylorFactor_;
// 
//       };
// 
//       template<>
//       class StressThermalAthermalTaylorTempl<am3::databus::MockDatabus>
//         : public pst::contract::StressContract
//         , public mo::InputTypelist<StressThermalAthermalInput>
//       {
//       public:
//         typedef StressThermalAthermalInput Inputs;
//         typedef pst::contract::StressContract Contract;
//       };
    }
  }
}
#endif // stressSum_h__
