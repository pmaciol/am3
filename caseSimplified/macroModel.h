
/*! \file macroModel.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef macroModel_h__
#define macroModel_h__
/*	include *********************************************************/

#include "../models/models.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
//     namespace adapter
//     {
//       template <>
//       struct VariableID<ppm::contract::DislDensityMobile>
//       {
//         static const unsigned int id = 0;
//       };
// 
//       template <>
//       struct VariableID<ppm::contract::SubgrainSize>
//       {
//         static const unsigned int id = 1;
//       };
// 
//       template <>
//       struct VariableID<ppm::contract::StoredEnergy>
//       {
//         static const unsigned int id = 2;
//       };
// 
//       template <>
//       struct VariableID<ppm::contract::GrainSize>
//       {
//         static const unsigned int id = 3;
//       };
//       template <>
//       struct VariableID<ppm::contract::NumberOfRecrystalizedNuclei>
//       {
//         static const unsigned int id = 4;
//       };
//       template <>
//       struct VariableID<ppm::contract::DislDensityMobileDeformed>
//       {
//         static const unsigned int id = 5;
//       };
//       template <>
//       struct VariableID<ppm::contract::RecrystalizedGrainSize>
//       {
//         static const unsigned int id = 6;
//       };
//       template <>
//       struct VariableID<ppm::contract::VolumeFractionOfRecrystallizedGrains>
//       {
//         static const unsigned int id = 7;
//       };
//     };
	namespace cases
	{
		namespace simpified
		{

      template<typename HANDLER, typename CONTRACT>
      class MacroModel : public am3::model::MacroModel<HANDLER, CONTRACT>
      {
      public:
        mpr::EffectiveStrainRate GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::EffectiveStrainRate *const )
        {
          return pointerToMacroHandler_->GetEffectiveStrainRateIn(*point);
        }
        mpr::Temperature GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::Temperature *const )
        {
          return pointerToMacroHandler_->GetTemperatureIn(*point);
        }
        mpr::StepLength GetState(am3::model::common::Point<typename CONTRACT::CoordinatesType> * point,const mpr::StepLength *const )
        {
          return pointerToMacroHandler_->GetStepLength();
        }
      };


		} //simplified
	} //cases
} //am3
#endif // macroModel_h__
