
/*! \file caseColdRolling.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef caseSimplified_h__
#define caseSimplified_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../databus/databus.h"
#include "../caseColdRolling/databus.h"
#include "../softwareDeformBuiltin/deformHandler.h"
#include "../adapter/ivstorage.h"
#include "../caseSimplified/macroModel.h"
#include "defs.h"
#include "../caseColdRolling/caseColdRolling.h"



#include "../models/models.h"



namespace mpr = am3::model::properties;
namespace db = am3::databus;



namespace mo = am3::model;
namespace ad = am3::adapter;

namespace am3
{
	namespace adapter
	{
    template <template<class, class> class HANDLER,typename IV_CONTRACT>
      std::vector<double> am3::adapter::IVStorage<HANDLER, IV_CONTRACT>::GetInternalVariables( mpt::Point<mpr::Coordinates2D> *point )
       {
      std::vector<double> toRet;
      return toRet;

    }
	} //adapter
} //am3
    

  

namespace am3
{
	namespace cases
	{
		namespace simpified
		{
			using namespace mpr;

      typedef ad::IVStorage<sdm::DeformHandler,ppm::DeformInput2D> Storage;
      typedef am3::cases::simpified::MacroModel<sdm::DeformHandler<mpr::Coordinates2D, am3::cases::simpified::Storage>,ppm::contract::MacroInputsContract2D> MacroModelDeform;
      typedef mo::CaseMacroImplementation<MacroModelDeform,Storage> CaseModel;

			// Properties ("additional")
// 			SIMPLE_PROP(SubgrainSize,double);
// 			SIMPLE_PROP(GrainSize,double);
// 			SIMPLE_PROP(RecrystalizedGrainSize,double);
// 			SIMPLE_PROP(VolumeFractionOfRecrystallizedGrains,double);
// 			SIMPLE_PROP(NumberOfRecrystalizedNuclei,double);
// 			SIMPLE_PROP(DislDensitySum,double);
// 			SIMPLE_PROP(DislDensityMobile,double);
// 			SIMPLE_PROP(DislDensityImmobileInteriors,double);
// 			SIMPLE_PROP(DislDensityImmobileWalls,double);
// 			SIMPLE_PROP(DislDensityMobileDeformed,double);
// 			SIMPLE_PROP(DislDensityImmobileInteriorsDeformed,double);
// 			SIMPLE_PROP(DislDensityImmobileWallsDeformed,double);
//      SIMPLE_PROP(SelfDiffusion,double); 
// 			SIMPLE_PROP(GrainBoundaryDiffusion,double);
// 			SIMPLE_PROP(ZenerParam,double);
// 			SIMPLE_PROP(StoredEnergy,double);
// 			SIMPLE_PROP(PrecipitateSize,double);


// Properties from polycrystal used to keep possibility of use models from polycrystal solution
			typedef am3::problem::polycrystal::contract::SubgrainSize SubgrainSize;
			typedef am3::problem::polycrystal::contract::GrainSize GrainSize;
			typedef am3::problem::polycrystal::contract::RecrystalizedGrainSize RecrystalizedGrainSize;
			typedef am3::problem::polycrystal::contract::VolumeFractionOfRecrystallizedGrains VolumeFractionOfRecrystallizedGrains;
			typedef am3::problem::polycrystal::contract::NumberOfRecrystalizedNuclei NumberOfRecrystalizedNuclei;
			typedef am3::problem::polycrystal::contract::DislDensitySum DislDensitySum; 
			typedef am3::problem::polycrystal::contract::DislDensityMobile DislDensityMobile;
			typedef am3::problem::polycrystal::contract::DislDensityImmobileInteriors DislDensityImmobileInteriors;
			typedef am3::problem::polycrystal::contract::DislDensityImmobileWalls DislDensityImmobileWalls;
			typedef am3::problem::polycrystal::contract::DislDensityMobileDeformed DislDensityMobileDeformed;
			typedef am3::problem::polycrystal::contract::DislDensityImmobileInteriorsDeformed DislDensityImmobileInteriorsDeformed;
			typedef am3::problem::polycrystal::contract::DislDensityImmobileWallsDeformed DislDensityImmobileWallsDeformed;
      typedef am3::problem::polycrystal::contract::SelfDiffusion SelfDiffusion;  
			typedef am3::problem::polycrystal::contract::GrainBoundaryDiffusion GrainBoundaryDiffusion;
			typedef am3::problem::polycrystal::contract::ZenerParam ZenerParam;
			typedef am3::problem::polycrystal::contract::StoredEnergy StoredEnergy;
			typedef am3::problem::polycrystal::contract::PrecipitateSize PrecipitateSize;

      
			#define MacroContractLength 3
			#define StressContractLength 1
			#define GrainsContractLength 5
			#define DislocationContractLength 7
			#define DiffusionContractLength 2
			#define ShearModulusContractLength 1
			#define ZenerDragContractLength 1

			CONTRACT(		DeformStressContract				,3,Coordinates2D,YieldStress,StressDStrain,StressDStrainRate);
			CONTRACT(		StressContract							,StressContractLength,Coordinates2D,YieldStress);
			CONTRACT(		MacroContract								,MacroContractLength,Coordinates2D, Temperature , EffectiveStrainRate , StepLength);
      // ppm contract used to keep possibility of using of ppm switcher for grains model
			//CONTRACT(		GrainsContract							,GrainsContractLength,GrainSize, RecrystalizedGrainSize, NumberOfRecrystalizedNuclei, VolumeFractionOfRecrystallizedGrains, StoredEnergy );
      typedef am3::problem::polycrystal::contract::GrainsContract2D GrainsContract;
			CONTRACT(		DislocationContract					,DislocationContractLength,Coordinates2D,DislDensityMobile, DislDensityImmobileInteriors, DislDensityImmobileWalls, DislDensityMobileDeformed, DislDensityImmobileInteriorsDeformed, DislDensityImmobileWallsDeformed, SubgrainSize );
			CONTRACT(		DiffusionContract						,DiffusionContractLength,Coordinates2D,SelfDiffusion , GrainBoundaryDiffusion );
			CONTRACT(		ShearModulusContract				,ShearModulusContractLength ,Coordinates2D,ShearModulus);
			CONTRACT(		ZenerDragContract						,ZenerDragContractLength,Coordinates2D,ZenerParam);



			// Databus
			DATABUS(CaseSimplifiedDatabus,Storage,
				MacroContract,
				GrainsContract, 
				ShearModulusContract, 
				DislocationContract, 
				DiffusionContract, 
				StressContract,
				ZenerDragContract);

//       template <typename T,typename DATABUS> std::vector<std::string> GetStringFromDatabus(boost::shared_ptr<DATABUS> databus, pnt::Point<mpr::Coordinates2D>* point)
//       {
//         std::vector<std::string> toRet;
//         boost::shared_ptr< moc::Provider<typename T,mpr::Coordinates2D> > provider;
//         provider = databus;
//         const typename T* typeTrait = nullptr;
//         toRet = provider->GetState(point,typeTrait).ToStringsVector();
//         return toRet;
//       }
// 
//       template <typename T,typename DATABUS> std::vector<std::string> GetStringFromStorage(boost::shared_ptr<DATABUS> databus)
//       {
//         std::vector<std::string> toRet;
//         T storedVal = databus->PreviousData()->GetPreviousState<T>();
//         toRet = storedVal.ToStringsVector();
//         return toRet;
//       }


      class CaseSimplifiedDatabusStringInterface
      {
      public:
        CaseSimplifiedDatabusStringInterface(boost::shared_ptr<CaseSimplifiedDatabus> variablesProvider): variablesProvider_(variablesProvider){};
        ~CaseSimplifiedDatabusStringInterface()
        {
          delete point;
        }
        std::vector<std::string> Value(std::string variable)
        {
          const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5,0.5);
          const mpr::Time t = mpr::Time(0.0);
          point = new pnt::ComputedPoint<mpr::Coordinates2D>(coord,t);

          
          if (variable=="EffectiveStrainRate")
          {
            toRet = db::GetStringFromDatabus<EffectiveStrainRate,CaseSimplifiedDatabus,mpr::Coordinates2D>(variablesProvider_,point);
          } 
          else if(variable=="ZenerParam")
          {
            toRet = db::GetStringFromDatabus<ZenerParam,CaseSimplifiedDatabus,mpr::Coordinates2D>(variablesProvider_,point);
          }
          else if(variable=="StoredEnergy")
          {
            toRet = db::GetStringFromStorage<StoredEnergy>(variablesProvider_);
          }
          else
          {
            toRet.push_back("WRONG VARIABLE");
          }
          return toRet;
        }

        

      protected:
        boost::shared_ptr<CaseSimplifiedDatabus> variablesProvider_;
        std::vector<std::string> toRet;
        pnt::ComputedPoint<mpr::Coordinates2D>* point;
//        template <typename T> void ToRet();
      };

			// Inputs (Input values for specific models)
			
			CONTRACT(StressInput,6,Coordinates2D,Temperature,ShearModulus,DislDensityImmobileInteriors,DislDensityImmobileWalls,DislDensityMobile,SubgrainSize)
			CONTRACT(GrainsByShersnevNoDeformationInput,10,Coordinates2D,
			  mpr::Temperature,
				mpr::ShearModulus,
				mpr::EffectiveStrainRate,
				mpr::StepLength,
				ppm::contract::DislDensityImmobileInteriors,
				ppm::contract::DislDensityImmobileWalls,
				ppm::contract::DislDensityMobile,
				ppm::contract::SubgrainSize,
				ppm::contract::SelfDiffusion,
				ppm::contract::ZenerParam
				)

			CONTRACT(GrainsByShersnevDeformationInput,7,Coordinates2D,
				ppm::contract::DislDensityImmobileInteriors,
				ppm::contract::DislDensityImmobileWalls,
				ppm::contract::DislDensityMobile,
				ppm::contract::SubgrainSize,
				mpr::EffectiveStrainRate,
				mpr::StepLength,
				mpr::ShearModulus
				)

			CONTRACT(GrainsByShersnevStaticRecoveryInput,9,Coordinates2D,
				mpr::Temperature,
				mpr::ShearModulus,
				ppm::contract::DislDensityImmobileInteriors,
				ppm::contract::DislDensityImmobileWalls,
				ppm::contract::DislDensityMobile,
				mpr::StepLength,
				ppm::contract::SelfDiffusion,
				ppm::contract::ZenerParam,
				ppm::contract::SubgrainSize
				)

			CONTRACT(DislocationsGottsteinDeformationInput,5,Coordinates2D,
				mpr::Temperature,
				mpr::StepLength,
				mpr::EffectiveStrainRate,
				mpr::ShearModulus,
				ppm::contract::SelfDiffusion
				)

			CONTRACT(DislocationsGottsteinStaticRecoveryInput,4,Coordinates2D,
				mpr::Temperature,
				mpr::StepLength,
				ppm::contract::SelfDiffusion,
				ppm::contract::ZenerParam
				)

			CONTRACT(DiffusionBySherstnevInput,1,Coordinates2D,mpr::Temperature )
			CONTRACT(ShearModulusBySherstnevInput,1,Coordinates2D,mpr::Temperature )
 			CONTRACT(NullInput,1,Coordinates2D,Temperature)

			// Models

			MODEL(StressBySherstnyev,StressContract,StressContractLength,StressInput)
			MODEL(ShearModulusBySherstnev,ShearModulusContract,ShearModulusContractLength,ShearModulusBySherstnevInput);
//			MODEL(DislocationsGottsteinDeformation<ActualDeformHandler>,DislocationContract,DislocationContractLength,DislocationsGottsteinDeformationInput);
//			boost::shared_ptr<mpm::GrainsBySherstnevStaticRecovery<ActualDeformHandler> > grainsModelStaticRecovery (new mpm::GrainsBySherstnevStaticRecovery<ActualDeformHandler>(deformHandlerPtr));
			MODEL(DiffusionBySherstnev,DiffusionContract,DiffusionContractLength,DiffusionBySherstnevInput);
			MODEL(ZenerParamBySherstnev,ZenerDragContract,ZenerDragContractLength,NullInput);
 			MODEL(NullStressModel,DeformStressContract,3,NullInput)


//			DATABUS_OUTPUTS_FOR_MODELS(CaseSimplifiedDatabus,StressBySherstnyev,ShearModulusBySherstnev,DiffusionBySherstnev,ZenerParamBySherstnev)

//      typedef ad::IVStorage<sdm::DeformHandler,ppm::DeformInput2D> Storage;
//       typedef tests::caseColdRolling::MacroModel<sdm::DeformHandler<mpr::Coordinates2D, Storage>,ppm::contract::MacroInputsContract2D> MacroModel;
//       typedef tests::caseColdRolling::CaseMacroImplementation<MacroModel,Storage> 

			// This case is used as a library. Constructor of Main class works like a "main" procedure in single executable solution
			class Main
			{
			public:
				Main();
			};    
		} //simpified
	} //cases  
} //am3



#endif // caseSimplified_h__caseSimplified_h__
