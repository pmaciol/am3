
/*! \file ivstorage.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef ivstorage_h__
#define ivstorage_h__
/*	include *********************************************************/

#include "../adapter/ivstorage.h"
#include "../problemPolycrystalMicrostructure/contracts.h"
#include "../models/dataStructures.h"
#include "../models/point.h"

/*	using ***********************************************************/

namespace ppm = am3::problem::polycrystal;
namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace adapter
	{
//     template <>
//     struct VariableID<ppm::contract::DislDensityMobile>
//     {
//       static unsigned int id = 0;
//     }
// 
//     template <>
//     struct VariableID<ppm::contract::SubgrainSize>
//     {
//       static unsigned int id = 0;
//     }
// 
//     template <>
//     struct VariableID<ppm::contract::StoredEnergy>
//     {
//       static unsigned int id = 0;
//     }
// 
//     template <>
//     struct VariableID<ppm::contract::GrainSize>
//     {
//       static unsigned int id = 0;
//     }
//     template <>
//     struct VariableID<ppm::contract::NumberOfRecrystalizedNuclei>
//     {
//       static unsigned int id = 0;
//     }
//     template <>
//     struct VariableID<ppm::contract::DislDensityMobileDeformed>
//     {
//       static unsigned int id = 0;
//     }
//     template <>
//     struct VariableID<ppm::contract::RecrystalizedGrainSize>
//     {
//       static unsigned int id = 0;
//     }
//     template <>
//     struct VariableID<ppm::contract::VolumeFractionOfRecrystallizedGrains>
//     {
//       static unsigned int id = 0;
//     }

    template <template<class, class> class HANDLER,typename IV_CONTRACT>
    std::vector<double> am3::adapter::IVStorage<HANDLER, IV_CONTRACT>::GetInternalVariables( mpt::Point<mpr::Coordinates2D> *point )
    {
      std::vector<double> toRet; 
      toRet.push_back(Get<ppm::contract::DislDensityMobile,mpr::Coordinates2D>::InPoint(point).Value());
      toRet.push_back(Get<ppm::contract::SubgrainSize,mpr::Coordinates2D>::InPoint(point).Value());
      toRet.push_back(Get<ppm::contract::StoredEnergy,mpr::Coordinates2D>::InPoint(point).Value());
      toRet.push_back(Get<ppm::contract::GrainSize,mpr::Coordinates2D>::InPoint(point).Value());
      toRet.push_back(Get<ppm::contract::NumberOfRecrystalizedNuclei,mpr::Coordinates2D>::InPoint(point).Value());
      toRet.push_back(Get<ppm::contract::DislDensityMobileDeformed,mpr::Coordinates2D>::InPoint(point).Value());
      toRet.push_back(Get<ppm::contract::RecrystalizedGrainSize,mpr::Coordinates2D>::InPoint(point).Value());
      toRet.push_back(Get<ppm::contract::VolumeFractionOfRecrystallizedGrains,mpr::Coordinates2D>::InPoint(point).Value());
      return toRet;
    }
	} //adapter
} //am3
#endif // ivstorage_h__
