
/*! \file adapter.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef adapter_h__
#define adapter_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/point.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
  namespace databus
  {
    template < 
      template <class G> class T_Databus, 
      template <class G> class T_Model>
    struct Adapter 
    {
      typedef Adapter< T_Databus, T_Model>       MyType;
      typedef T_Databus<Adapter>                 Databus;
      typedef T_Model<Adapter>                   Model;
    };

    struct EmptyDatabus
    {
      typedef EmptyType Databus;
    };
    template<typename T>
    struct NoInputs
    {
      typedef EmptyDatabus Databus;
    };

    
    struct MockDatabus
    {
      typedef mpr::Coordinates2D CoordinatesType;
      template<typename T>
      T Get(const mpt::Point<mpr::Coordinates2D>* pt){ T* toRet = nullptr; return *toRet; }
      typedef EmptyDatabus Databus;
      typedef PairTemplate<EmptyType,EmptyType> MergedMap;
      template <typename VAR>
      VAR GetFromDatabus(mpt::Point<mpr::Coordinates2D> * point)
      {
        VAR tmp;
        return tmp;
      }
    };

  } //databus
} //am3
#endif // adapter_h__
