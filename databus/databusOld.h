
/********************************************************************
	author:		Piotr Maciol
	
	purpose:*/
/**
 * \defgroup Databus Databus
 *
 * Classes from namespace \ref am3::databus is used to communicate between different modules. 
 *
 * am3::databus::DataStorage is used to storage a homogeneous sequence of objects. 
 *
 * am3::databus::Databus is a container of heterogeneous DataStorages. At this moment, up to 4 storages can be added to one Databus. If more is needed, read am3::databus::NullClass comments
 *
 * @{
 */

#ifndef databus_h__
#define databus_h__
/*	include *********************************************************/

#include <vector>

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/



namespace am3
{
  namespace databus
  {
     template <typename T, //Holy shit!
       template <typename ELEM,
       typename ALLOC = std::allocator<ELEM> >
       class T_container = std::vector> 
     class DataStorage
     {

     protected:
        typedef T_container<T> SPECIALIZED_CONTAINER;
        typename SPECIALIZED_CONTAINER storage;
     public:
       typedef typename SPECIALIZED_CONTAINER::iterator iterator;
       typedef T StoredElement;
       DataStorage(){};
       void push_back(typename const StoredElement & element){storage.push_back(element);};
       iterator begin(){return storage.begin();};
     };

//////////////////////////////////////////////////////////////////////////
/// Very bad design below. 
/// How to simplify it, keeping compiling on VS2010?
//////////////////////////////////////////////////////////////////////////
/// If you want to add more DataStorages, you have to:
/// - redefine DATABUS_TEMPLATE_N - add one ",NullClas"
/// - move Databus definition (look below) with highest N on the end, remove "=NullClass" from template, add specialization in "class" row
/// - write new Databus definition with N = N + 1, MUST BE before other Databus definition - VC2010.
//////////////////////////////////////////////////////////////////////////
class NullClass{};

#define DATABUS_TEMPLATE_1 ,NullClass,NullClass,NullClass
#define DATABUS_TEMPLATE_2 ,NullClass,NullClass
#define DATABUS_TEMPLATE_3 ,NullClass



template<typename T_Data_Storage1,typename T_Data_Storage2=NullClass,typename T_Data_Storage3=NullClass,typename T_Data_Storage4=NullClass>
class Databus : public Databus<T_Data_Storage1, T_Data_Storage2,T_Data_Storage3>
  {
  public:
    typedef T_Data_Storage4 DataStorage4;
    DataStorage4& Storage4(){return storage4;};
  protected:
    DataStorage4 storage4;
  };
     
template<typename T_Data_Storage>
class Databus<T_Data_Storage DATABUS_TEMPLATE_1>
{
public:
  Databus(){};
  typedef T_Data_Storage DataStorage1;
  DataStorage1& Storage1(){return storage1;};
protected:
  DataStorage1 storage1;
};
     
template <typename T_Data_Storage1,  typename T_Data_Storage2>
class Databus<T_Data_Storage1,T_Data_Storage2 DATABUS_TEMPLATE_2> : public Databus<T_Data_Storage1>
{
public:
  typedef T_Data_Storage2 DataStorage2;
  DataStorage2& Storage2(){return storage2;};
protected:
  DataStorage2 storage2;
};

  template<typename T_Data_Storage1,typename T_Data_Storage2,typename T_Data_Storage3>
  class Databus<T_Data_Storage1, T_Data_Storage2, T_Data_Storage3 DATABUS_TEMPLATE_3>: public Databus<T_Data_Storage1,T_Data_Storage2>
       
  {
public:
  typedef T_Data_Storage3 DataStorage3;
  DataStorage3& Storage3(){return storage3;};
protected:
  DataStorage3 storage3;
  };
//////////////////////////////////////////////////////////////////////////
// The end of bad design!
//////////////////////////////////////////////////////////////////////////
   }
}

/**@}*/

#endif // databus_h__

