
/*! \file databus.h **************************************************
* \author		Piotr Maciol
* \copyright 	GNU Public License.
* \brief       
* \details   	
*********************************************************************/
#ifndef databus_databus_h__
#define databus_databus_h__
/*	include *********************************************************/

#include <iostream>
#include <typeinfo>
#include <string>
#include "../logger/logger.h"
#include "../common/typelists.h"
#include "../models/contracts.h"
#include "../models/point.h"



/*	using ***********************************************************/

namespace mpt=am3::model::point;
namespace moc=am3::model::contract;
namespace lo=am3::logger;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace databus
  {
		template <typename PREVIOUS_DATA_STORAGE>
		class PreviousStepStorage
		{
		public:
		  typedef PREVIOUS_DATA_STORAGE PreviousDataStorage;
			void SetPreviousStepsStorage(boost::shared_ptr<PREVIOUS_DATA_STORAGE> previousData) {previousData_ = previousData;}
			boost::shared_ptr<PREVIOUS_DATA_STORAGE> PreviousData() {return previousData_;}
		protected:
			PreviousStepStorage(){};
			virtual ~PreviousStepStorage(){};
			boost::shared_ptr<PREVIOUS_DATA_STORAGE> previousData_;
		};

		template<typename LIST, typename CONTRACT>
		struct HandlerInternal;

		template<typename T1, typename T2,typename CONTRACT>
		struct HandlerInternal<Typelist<T1, T2>, CONTRACT>
			: public HandlerInternal<T2, CONTRACT>
			,  public moc::Provider<T1,typename CONTRACT::CoordinatesType>
		{
			//! NullProvider is used, when a databus have not been initialized with proper inputs provider before first use
			struct NullProvider: public moc::Provider<T1,typename CONTRACT::CoordinatesType>
			{
				T1 GetState(mpt::Point<typename CONTRACT::CoordinatesType> * point,const T1 * const typeTrait) 
				{
					T1 test;
					std::string str = " Use of nullProvider of  ";
					str += typeid(test).name();
					lo::log<lo::SEV_ERROR>(lo::LOG_MANAGER,str.c_str());
					return test;
				}
			};
			//////////////////////////////////////////////////////////////////////////
			// ! Constructor sets safe, but dump NullProvider
			HandlerInternal(){value_= boost::shared_ptr<moc::Provider<T1,typename CONTRACT::CoordinatesType> >(new NullProvider);};

			boost::shared_ptr<moc::Provider<T1,typename CONTRACT::CoordinatesType> > value_;

			T1 GetState(mpt::Point<typename CONTRACT::CoordinatesType> * point,const T1 * const typeTrait) 
			{
				T1 *test=nullptr;
// 				std::string str = " Getting value of ";
// 				str += typeid(*test).name();
// 				lo::log<lo::SEV_DEBUG>(lo::LOG_MANAGER,str.c_str());
				return this->value_->GetState(point,typeTrait);
			}
			void Set(boost::shared_ptr<CONTRACT> contract)
			{
				T1 *test=nullptr;
// 				std::string str = " Setting provider for ";
// 				str += typeid(*test).name();
// 				lo::log<lo::SEV_DEBUG>(lo::LOG_MANAGER,str.c_str());
				value_= (boost::shared_ptr<moc::Provider<T1,typename CONTRACT::CoordinatesType> >) contract;
				HandlerInternal<T2, CONTRACT>::Set(contract);
			};
		};


		template<typename T1, typename T2,typename T3,typename CONTRACT>
		struct HandlerInternal< TypelistTuple<Typelist<T1, T2> , T3>, CONTRACT>
			: public HandlerInternal<T2, CONTRACT>
			,  public moc::Provider<T1,typename CONTRACT::CoordinatesType>
		{
			//! NullProvider is used, when a databus have not been initialized with proper inputs provider before first use
			struct NullProvider: public moc::Provider<T1,typename CONTRACT::CoordinatesType>
			{
				T1 GetState(mpt::Point<typename CONTRACT::CoordinatesType> * point,const T1 * const typeTrait) 
				{
					T1 test;
					std::string str = " Use of nullProvider of  ";
					str += typeid(test).name();
					lo::log<lo::SEV_ERROR>(lo::LOG_MANAGER,str.c_str());
					return test;
				}

			};
			//////////////////////////////////////////////////////////////////////////

			// ! Constructor sets safe, but dump NullProvider
			HandlerInternal(){value_= boost::shared_ptr<moc::Provider<T1,typename CONTRACT::CoordinatesType> >(new NullProvider);};

			boost::shared_ptr<moc::Provider<T1,typename CONTRACT::CoordinatesType> > value_;
      template<typename T>
      T GetState(mpt::Point<typename CONTRACT::CoordinatesType> * point)//;
//      template<>
//      T1 GetState<T1>(mpt::Point<typename CONTRACT::CoordinatesType> * point)
      {
	static_assert(std::is_same<T1,T>::value,"Handler does not provide this Property");
        T1 *test=nullptr;
        std::string str = " Getting value of ";
        str += typeid(*test).name();
        lo::log<lo::SEV_DEBUG>(lo::LOG_MANAGER,str.c_str());
        return this->value_->GetState(point,test);
      }

			T1 GetState(mpt::Point<typename CONTRACT::CoordinatesType> * point,const T1 * const typeTrait) 
			{
				T1 *test=nullptr;
//				std::string str = " Getting value of ";
//				str += typeid(*test).name();
//				lo::log<lo::SEV_DEBUG>(lo::LOG_MANAGER,str.c_str());
				return this->value_->GetState(point,typeTrait);
			}
			void Set(boost::shared_ptr<CONTRACT> contract)
			{
				T1 *test=nullptr;
//				std::string str = " Setting provider for ";
//				str += typeid(*test).name();
//				lo::log<lo::SEV_DEBUG>(lo::LOG_MANAGER,str.c_str());
				value_= (boost::shared_ptr<moc::Provider<T1,typename CONTRACT::CoordinatesType> >) contract;
				HandlerInternal<T2, CONTRACT>::Set(contract);
			};
		};


		template<typename CONTRACT>
		struct HandlerInternal<EmptyType, CONTRACT>
		{
      EmptyType GetState(mpt::Point<typename CONTRACT::CoordinatesType> * point,const EmptyType * const typeTrait);
			void Set(boost::shared_ptr<CONTRACT> contract){};
		};


		template<typename CONTRACT>
		struct Handler : public HandlerInternal<typename CONTRACT::VariablesTypes,CONTRACT>{};

    template <typename T,typename DATABUS, typename COORDINATES> std::vector<std::string> GetStringFromDatabus(boost::shared_ptr<DATABUS> databus, mpt::Point<COORDINATES>* point)
    {
      std::vector<std::string> toRet;
      boost::shared_ptr< moc::Provider<T,mpr::Coordinates2D> > provider;
      provider = databus;
      const T* typeTrait = nullptr;
      toRet = provider->GetState(point,typeTrait).ToStringsVector();
      return toRet;
    }

    template <typename T,typename DATABUS> std::vector<std::string> GetStringFromStorage(boost::shared_ptr<DATABUS> databus)
    {
      std::vector<std::string> toRet;
//      boost::shared_ptr<typename DATABUS::PreviousStepStorage> ptr;
//      ptr = databus->PreviousData();
      T storedVal = databus->PreviousData()->GetPreviousState();
      toRet = storedVal.ToStringsVector();
      return toRet;
    }

    //////////////////////////////////////////////////////////////////////////

    template<typename T1, typename T2, typename MODEL>
    struct DatabusInternal;

    template<typename T1, typename T2, typename T3, typename T4, typename MODEL>
    struct DatabusInternal<TypelistTuple<Typelist<T1,T2>,T3 >, T4,MODEL>
      : public DatabusInternal<Typelist<T1,T2>,T4 ,MODEL>
    {
    
    };


    template<typename T1, typename T2, typename T3, typename MODEL>
    struct DatabusInternal<Typelist<T1, T2>, T3 , MODEL>
      : public DatabusInternal<T2, T3, MODEL>
    {
      using DatabusInternal<T2, T3, MODEL>::var_;
      void Getf(T1* t){};

      template <typename T>
      void Get()
      {
	const T* trait = nullptr;
        GetMatching(trait);
//         T test;
//         std::string str = typeid(test).name();
//         std::cout<<str<<" not resolved"<<std::endl;
//         DatabusInternal<T2, T3,MODEL>::Get<T>();
      };

    private:
      void GetMatching(const T1* trait)
      {
        T1 test;
        std::string str = typeid(test).name();
        std::cout<<str<<" resolved"<<std::endl;
        const mpr::Coordinates2D coord = mpr::Coordinates2D(0.5,0.5);
        const mpr::Time t = mpr::Time(0.0);
        mpt::ComputedPoint<mpr::Coordinates2D> point = mpt::ComputedPoint<mpr::Coordinates2D>(coord,t);
        test = var_.GetState(point,&test);
      };
    };



    template<typename T1, typename T2, typename MODEL>
    struct DatabusInternal<EmptyType, Typelist<T1, T2> , MODEL> 
      : public DatabusInternal<typename T1::ProvidedContract::VariablesTypes,T2, T1>
    {
      MODEL var_;
    };

    template<typename MODEL>
    struct DatabusInternal<EmptyType,EmptyType, MODEL>
    {
      MODEL var_;
    };

    template<typename T2, typename T3, typename MODEL>
    struct DatabusInternal<Typelist<EmptyType, T2>, T3 , MODEL>
    {

    };

//     template<typename MODELS_LIST>
//     class DatabusGeneric;
// 
//     template<typename T1, typename T2>
//     class DatabusGeneric<Typelist<T1,T2> >
//       : public DatabusInternal<typename T1::ProvidedContract::VariablesTypes,T2, T1>
//     {
//     
//     };


    template <class Adapter>
    struct DatabusTest {
      typedef typename Adapter::Model Model;
      Model* node;
    };
  } //databus
} //am3


#endif // databus_databus_h__