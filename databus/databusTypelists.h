
/*! \file databusTypelists.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef databusTypelists_h__
#define databusTypelists_h__
/*	include *********************************************************/

#include <assert.h>
#include <type_traits>
#include "../common/typelists.h"
#include "../models/point.h"
#include "adapter.h"
//#include <boost/>
#include <boost/static_assert.hpp>

/*	using ***********************************************************/

namespace mpt = am3::model::point;

/*	extern **********************************************************/

/*	classes *********************************************************/

namespace am3
{
	namespace databus
	{

    //////////////////////////////////////////////////////////////////////////
    template <
      template <typename G> class H, 
      typename T,
      typename LIST_ALL_INPUTS>
    struct InputsListInternal;

    template<typename TL>
    struct InputsList;

    template <
      template <typename G> class H, 
      typename T,
      typename LIST_ALL_OUTPUTS>
    struct OutputsListInternal;

    template<
      template <typename G> class H,
      typename T>
    struct DatabusTypelistsInternal;

    template <typename VARIABLELIST, typename MODEL>
    struct DatabusTypelistsContractInternal;

    template<typename TYPELIST>
    struct DatabusTypelists;

//////////////////////////////////////////////////////////////////////////

    template <
      template <typename G> class H, 
      template <typename G> class H2, 
      typename T2,
      typename LIST_ALL_INPUTS
    >
    struct InputsListInternal<H,Templatelist<H2,T2> , LIST_ALL_INPUTS>
      : public InputsListInternal<H2,T2, typename  MergeTypelists<LIST_ALL_INPUTS, typename H<MockDatabus>::Inputs::VariablesTypes>::Merged  >
      // Error here means there is no 'Input' typedef in submodel class
    {    };

    template<template <typename G> class H, typename LIST_ALL_INPUTS>
    struct InputsListInternal<H,EmptyType, LIST_ALL_INPUTS>
    {
      typedef typename MergeTypelists<LIST_ALL_INPUTS, typename H<EmptyType>::Inputs::VariablesTypes>::Merged Inputs;
    };

    template <
      template <typename G> class H, 
    class T
    >
    struct InputsList<Templatelist<H,T>>
      : public InputsListInternal<H,T, EmptyType>
    {  };

//////////////////////////////////////////////////////////////////////////

    template <
      template <typename G> class H, 
      template <typename G> class H2, 
      typename T2,
      typename LIST_ALL_OUTPUTS
    >
    struct OutputsListInternal<H,Templatelist<H2,T2> , LIST_ALL_OUTPUTS>
      : public OutputsListInternal<H2,T2, typename  MergeTypelists<LIST_ALL_OUTPUTS, typename H<MockDatabus>::Contract::VariablesTypes>::Merged  >
    {  };

    template<template <typename G> class H, typename LIST_ALL_OUTPUTS>
    struct OutputsListInternal<H,EmptyType, LIST_ALL_OUTPUTS>
    {
      typedef typename MergeTypelists<LIST_ALL_OUTPUTS, typename H<EmptyType>::Contract::VariablesTypes>::Merged Contract;
    };

    template<typename TL>
    struct OutputsList;

    template <
      template <typename G> class H, 
    class T>
    struct OutputsList<Templatelist<H,T>>
      : public OutputsListInternal<H,T, EmptyType>
    { };

//////////////////////////////////////////////////////////////////////////


    template<typename TYPE, typename MODEL, typename FLAG>
    struct Convert;

    template<typename TYPE, typename MODEL>
    struct Convert<TYPE,MODEL,std::true_type>
    {
      TYPE operator()(boost::shared_ptr<MODEL> model,mpt::Point<mpr::Coordinates2D> * point, const TYPE* var)
      {
        return boost::static_pointer_cast<moc::Provider<TYPE, mpr::Coordinates2D> >(model)->GetState(point, var);
      }
    };

    template<typename TYPE, typename MODEL>
    struct Convert < TYPE, MODEL, std::false_type >
    {
      TYPE operator()(boost::shared_ptr<MODEL> model, mpt::Point<mpr::Coordinates2D> * point, const TYPE* var)
      {
        return (model)->Get<TYPE>(point);
      }
    };



    template <typename H, typename T, typename MODEL>
    struct DatabusTypelistsContractInternal<Typelist<H,T>, MODEL >
      : public DatabusTypelistsContractInternal<T, MODEL>
    {
      typedef Typelist<H,T> Contract;

      template <typename TI>
      TI Get(mpt::Point<mpr::Coordinates2D> * point)
      {
        return DatabusTypelistsContractInternal<T, MODEL>::Get<TI>(point);
      }

      template <>
      H Get<H>(mpt::Point<mpr::Coordinates2D> * point)
      {
        H* var = nullptr;
        Convert<H, MODEL, std::is_convertible<MODEL, moc::Provider<H, mpr::Coordinates2D> >::type> toRet;
				assert(model_ != nullptr);
        return toRet(model_,point, var);
      }
    };

    template <typename H, typename T,typename  OTHER, typename MODEL>
    struct DatabusTypelistsContractInternal<TypelistTuple<Typelist<H,T>,OTHER>, MODEL>
      : public DatabusTypelistsContractInternal<Typelist<H,T> , MODEL>
    { };

    template <typename MODEL>
    struct DatabusTypelistsContractInternal<EmptyType, MODEL>
    {
    protected:
      boost::shared_ptr<MODEL> model_;
    };

// / / / / / / / / / / / / / / / / / / / / / / / / / / /  / / / / / / / / / / / / / / / /

    template<typename DB>
    struct DatabusTypelistsContract
      : public DatabusTypelistsContractInternal<typename DB::Contract::VariablesTypes,typename DB>
    {
      typedef typename DB::Contract::VariablesTypes Contract;
    };

//////////////////////////////////////////////////////////////////////////

    template <typename H, typename T, typename CAST_TO>
    struct ModelMapTemplateInternal;

    template <typename H, typename H1, typename T1, typename CAST_TO>
    struct ModelMapTemplateInternal<H, Typelist<H1, T1>, CAST_TO>
      : ModelMapTemplateInternal<H1, T1, CAST_TO>
    {
      typedef PairTemplate<H,CAST_TO> Pair;
      typedef Typelist<Pair,typename ModelMapTemplateInternal<H1, T1, CAST_TO>::List > List;
    };

    template <typename H, typename CAST_TO>
    struct ModelMapTemplateInternal<H, EmptyType, CAST_TO>
    {
      typedef PairTemplate<H,CAST_TO> Pair;
      typedef Typelist<Pair,EmptyType> List;
    };

// / / / / / / / / / / / / / / / / / / / / / / / / / / /  / / / / / / / / / / / / / / / /

    template<typename LIST, typename CAST_TO>
    struct ModelMapTemplate;

    template<typename H, typename T, typename CAST_TO>
    struct ModelMapTemplate<Typelist<H,T>, CAST_TO>
      : public ModelMapTemplateInternal<H ,T, CAST_TO>
    { };


//////////////////////////////////////////////////////////////////////////
    template <
      template <typename G> class H, 
      template <typename G> class H2, 
      typename T2
    >
    struct DatabusTypelistsInternal<H, Templatelist<H2,T2> >
      : public DatabusTypelistsContract< H <DatabusTypelistsInternal<H2,T2> > > //this inheritance must be first (at least in MSVC2010)
      , public DatabusTypelistsInternal<H2,T2>
     
    {
      typedef H<DatabusTypelistsInternal<H2,T2> >  Model;
      typedef typename Model::Contract::VariablesTypes Contract;
      typedef ModelMapTemplate<typename ToTypelist<Contract>::PureTypelist,DatabusTypelistsInternal<H, Templatelist<H2,T2> > > ModelMap;
      typedef typename MergeTypelists<typename ModelMap::List, typename DatabusTypelistsInternal<H2,T2>::MergedMap >::Merged MergedMap;

      void SetModel(boost::shared_ptr<Model> model)
      {
        // Sets model constructed outside
        model_ = model;
        // Sets this databus as input from model; all variables from the tail of model templates lists available for model
        SetModelInputs();
        // Sets model in 'Contract' hierarchy with model from the tail;
        SetContractModel();
      }

      void SetInput(boost::shared_ptr<Model> model)
      {
        // Sets this databus as input from model; all variables from the tail of model templates lists available for model
        SetModelInputs();
      }

      void SetSwitcher(boost::shared_ptr<Model> model)
      {
        // Sets model constructed outside
        model_ = model;
        // Sets model in 'Contract' hierarchy with model from the tail;
        SetContractModel();
      }

	  template <typename VAR>
	  VAR GetFromDatabus(mpt::Point<mpr::Coordinates2D> * point)
	  {
			lo::debugLog("Databus asked" );
      return static_cast<GetFromMap<VAR, typename public DatabusTypelistsInternal<H, Templatelist<H2, T2> >::MergedMap>::Second *>(this)->Get<VAR>(point);  //Error here usually means that there is no requested variable in the Databus; add new model or change the sequence of the models
	  }
      protected:
        boost::shared_ptr<Model> model_;
    protected:
      void SetModelInputs()
      {
        model_->SetModelInput(this);
      }
      
      void SetContractModel()
      {
        DatabusTypelistsContract< H <DatabusTypelistsInternal<H2,T2> > >::model_ = model_;
      }    
    };

    template<template <typename G> class H>
    struct DatabusTypelistsInternal<H, EmptyType >
      : public DatabusTypelistsContract< H <DatabusTypelistsInternal<H, EmptyType > > > //this inheritance must be first (at least in MSVC2010)
    {
      typedef H<DatabusTypelistsInternal<H, EmptyType > >  Model;
      typedef typename Model::CoordinatesType CoordinatesType;  //Error here means probably that there is no such class in databusList definition

      void SetModel(boost::shared_ptr<Model> model)
      {
        model_=model;
        DatabusTypelistsContract< H <DatabusTypelistsInternal<H, EmptyType > > > ::model_ = model_;
      }

      void SetSwitcher(boost::shared_ptr<Model> model)
      {
        model_ = model;
      }

      typedef typename Model::Contract::VariablesTypes Contract;
      typedef ModelMapTemplate<typename ToTypelist<Contract>::PureTypelist,DatabusTypelistsInternal<H, EmptyType > > ModelMap;
      typedef typename ModelMap::List MergedMap;

	  template <typename VAR>
	  VAR GetFromDatabus(mpt::Point<mpr::Coordinates2D> * point)
	  {
		  return static_cast<GetFromMap<VAR, typename DatabusTypelistsInternal<H, EmptyType >::MergedMap>::Second *>(this)->Get<VAR>(point);  //Error here usually means that there is no requested variable in the Databus; add new model or change the sequence of the models
	  }
    protected:
      boost::shared_ptr<Model> model_;
    };


//////////////////////////////////////////////////////////////////////////

    template<
      template <typename G> class H,
      typename T >
    struct DatabusTypelists<Templatelist<H,T> >
      : public DatabusTypelistsInternal<H,T>
    {
      typedef typename InputsList<Templatelist<H,T> >::Inputs AllInputs;
      typedef typename OutputsList<Templatelist<H,T> >::Contract AllOutputs;
    };

//////////////////////////////////////////////////////////////////////////

    template<template <typename G> class H, typename T>
    struct GetWithTemplateTailFromDatabusInternal;

    template<template <typename G> class TYPE, template <typename G> class H, typename T>
    struct GetWithTemplateTailFromDatabusInternal<TYPE, Templatelist<H,T> >
      : public GetWithTemplateTailFromDatabusInternal<TYPE, T>
    { };

    template<template <typename G> class TYPE, typename T>
    struct GetWithTemplateTailFromDatabusInternal<TYPE, Templatelist<TYPE,T> >
    {
      typedef T Tail;
    };

    template<template <typename G> class TYPE>
    struct GetWithTemplateTailFromDatabusInternal<TYPE, EmptyType >
    { 
      typedef EmptyType Tail;
    };

    template<template <typename G> class H, typename T>
    struct GetWithTemplateTailFromDatabus;

    template<template <typename G> class H, typename MODELS_LIST>
    struct GetWithTemplateTailFromDatabus<H,am3::databus::DatabusTypelists<MODELS_LIST> >
      :public GetWithTemplateTailFromDatabusInternal<H,MODELS_LIST>
    { };

//////////////////////////////////////////////////////////////////////////

    template<template <typename G> class T, typename DATABUS>
    struct ModelInstanceType
    {
      typedef typename DatabusTypelistsInternal <
        T, 
        typename GetWithTemplateTailFromDatabus<T,DATABUS>::Tail
      >::Model Type;

      typedef typename GetWithTemplateTailFromDatabus<T, DATABUS>::Tail VariableProvidersTypelist;

      typedef DATABUS Databus;
    };

    template<typename DATABUS, template <typename G> class MODELTMPL, typename MODEL>
    void SetModelInstance(DATABUS* db, boost::shared_ptr<MODEL> model)
    {
      static_cast<
        DatabusTypelistsInternal<
          MODELTMPL, 
          typename GetWithTemplateTailFromDatabus<MODELTMPL,DATABUS>::Tail
          > *
        > (db)->SetModel(model);
      // Error here means that you tried to link with databus the submodel which is not in DatabusList
    }

    template<typename DATABUS, template <typename G> class MODELTMPL, typename MODEL>
    void SetSwitcherInstance(DATABUS* db, boost::shared_ptr<MODEL> model)
    {
      static_cast<
        DatabusTypelistsInternal<
        MODELTMPL,
        typename GetWithTemplateTailFromDatabus<MODELTMPL, DATABUS>::Tail
        > *
      > (db)->SetSwitcher(model);
    }
  } //databus
} //am3
#endif // databusTypelists_h__


