
/*! \file deformHandlerOutputsProviders.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef deformHandlerOutputsProviders_h__
#define deformHandlerOutputsProviders_h__
/*	include *********************************************************/

#include <boost/scoped_ptr.hpp>
#include "deformOutputs.h"
#include "../modelCreep/contracts.h"
#include "../modelStress/contracts.h"
#include "../modelTemplates/modelsTemplates.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace ppc = am3::problem::creep;
namespace pst = am3::problem::stress;
namespace mot = am3::model::templates;
using boost::shared_ptr;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace deform
    {
      
      template<typename T_COORDINATE>
      class  DeformHandlerStressProxy;

      template<>
      class  DeformHandlerStressProxy<mpr::Coordinates2D>
       : public DeformStressContract2D
      {
      public:
        typedef mpr::Coordinates2D T_COORDINATE;
        DeformHandlerStressProxy(boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel,
          boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressStrainModel,
          boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressStrainRateModel)
          : yieldStressModel_(yieldStressModel),yieldStressStrainModel_(yieldStressStrainModel), yieldStressStrainRateModel_(yieldStressStrainRateModel) { };

        mpr::YieldStress GetState( mpt::Point<T_COORDINATE>* point,const mpr::YieldStress* const typeTrait ) {return yieldStressModel_->GetState(point,typeTrait);}
        mpr::StressDStrain GetState(mpt::Point<T_COORDINATE>* point,const mpr::StressDStrain* const typeTrait ) {return yieldStressStrainModel_->GetState(point,typeTrait);}
        mpr::StressDStrainRate GetState( mpt::Point<T_COORDINATE>* point,const mpr::StressDStrainRate* const typeTrait ) {return yieldStressStrainRateModel_->GetState(point,typeTrait);}

      protected:
        boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel_;
        boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressStrainModel_;
        boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressStrainRateModel_;
      };

      //////////////////////////////////////////////////////////////////////////

      //////////////////////////////////////////////////////////////////////////

      template<typename T_COORDINATE>
      class  DeformHandlerCreepProxy;

      template<>
      class  DeformHandlerCreepProxy<mpr::Coordinates2D>
      : public DeformCreepContract2D
      {
      public:
        typedef mpr::Coordinates2D T_COORDINATE;
        virtual ppc::CreepRate GetState( am3::model::common::Point<T_COORDINATE>* point,const ppc::CreepRate* const typeTrait ) {return creepRateModel_->GetState(point,typeTrait);}
        virtual ppc::CreepRateDStrain GetState( am3::model::common::Point<T_COORDINATE>* point,const ppc::CreepRateDStrain* const typeTrait )  {return creepRateDStrain_->GetState(point,typeTrait);}
      protected:
        boost::shared_ptr<moc::Provider<ppc::CreepRate,T_COORDINATE> > creepRateModel_;
        boost::shared_ptr<moc::Provider<ppc::CreepRateDStrain,T_COORDINATE> > creepRateDStrain_;
      };
    }
  }
}
#endif // deformHandlerOutputsProviders_h__
