
/*! \file deformHandlerBackwardCompatibility.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef deformHandlerBackwardCompatibility_h__
#define deformHandlerBackwardCompatibility_h__
/*	include *********************************************************/
/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace deform
    {
      // Obsolete, kept due to backward compatibility. Should be removed when not necessary in DeformHandler definition
      struct NullIV
      {
        std::vector<double> GetInternalVariables(mpt::Point<mpr::Coordinates2D> *point) {std::vector<double> dumb; return dumb;}
      };

      ////////////////////////////////////////////////////////////////////////// //////////////////////////////////////////////////////////////////////////
      // Compilations of components for Deform. Some are added for backward compatibility

      // Backward compatibility
      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL = NullIV>
      class DeformHandler
        : public DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE> >
//        , public DeformHandlerStressComponent<T_COORDINATE, DeformHandlerElement<T_COORDINATE> >
        , public DeformHandlerUserProc
//        , public DeformHandlerTimeComponent<T_COORDINATE>
        , public DeformHandlerElement<T_COORDINATE>
        , public DeformHandlerStressTimeComponent<T_COORDINATE, DeformHandlerElement<T_COORDINATE> >
      {
                NullIV* dumbIVModel;
      public:
        //      typedef mpt::NamedPoint<mpt::NamedPointsContainer<mpt::NamedPointsStorageLastTwo<mpr::Coordinates2D> > > NamedPoint;

        DeformHandler(boost::shared_ptr<ExternalSoftwareInputs> yieldStressModel);
        DeformHandler(boost::shared_ptr<ExternalSoftwareInputs> yieldStressModel,T_INTERNAL_VARIABLE_MODEL* ivModel);
        DeformHandler(boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel,
          boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressStrainModel,
          boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressStrainRateModel,
          T_INTERNAL_VARIABLE_MODEL* ivModel);

        // Just for backward compatibility, due to deriving from DeformHandlerUserProc
        virtual int CallSetElementalVariables( const int length,const double* values, const int meshId, const int elementId )
        {
          return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::CallSetElementalVariables(length,values,meshId,elementId);
        }

        virtual int SetElementalVariables( const int length,const double* values, const int meshId, const int elementId )
        {
          return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::SetElementalVariables(length,values,meshId,elementId);
        }

        virtual int CallGetElementalVariables( const int length,double* values, const int meshId, const int elementId )
        {
          return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::CallGetElementalVariables( length,values, meshId, elementId );
        }

        virtual double CallGetElementalVariables( const unsigned int whichVariable ) const
        {
          return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::CallGetElementalVariables( whichVariable );
        }

        virtual int CallSetGlobalVariables( const int length,const double* values, const int meshId, const int elementId )
        {
          return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::CallSetGlobalVariables( length,values, meshId, elementId );
        }

        virtual int CallSetStepLength( const double* timeStepLength, const int meshId, const int elementId )
        {
          return DeformHandlerTimeComponent<T_COORDINATE>::CallSetStepLength( timeStepLength, meshId, elementId );
        }

        virtual int SetStepLength( const double* timeStepLength, const int meshId, const int elementId )
        {
          return DeformHandlerTimeComponent<T_COORDINATE>::SetStepLength( timeStepLength, meshId, elementId );
        }

        virtual int CallSetTime( const double* time, const int meshId, const int elementId )
        {
          return DeformHandlerTimeComponent<T_COORDINATE>::CallSetTime( time, meshId, elementId );
        }

        virtual int SetTime( const double* time, const int meshId, const int elementId )
        {
          return DeformHandlerTimeComponent<T_COORDINATE>::SetTime( time, meshId, elementId );
        }

        virtual int CallSetElementalNodes( const int length,const double* values, const double time, const int meshId, const int elementId )
        {
          return DeformHandlerElement<T_COORDINATE>::CallSetElementalNodes(length,values,time,meshId,elementId);
        }

        virtual int CallSetTemperature( const double* temperature, const int meshId, const int elementId )
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallSetTemperature( temperature, meshId, elementId );
        }

        virtual int CallSetEffectiveStrainRate( const double* effStrainRate, const int meshId, const int elementId )
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallSetEffectiveStrainRate( effStrainRate, meshId, elementId );
        }

        virtual double CallGetYieldStress( const int meshId, const int elementId ) const
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallGetYieldStress( meshId, elementId );
        }

        virtual double CallGetYieldStressDStrain( const int meshId, const int elementId ) const
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallGetYieldStressDStrain( meshId, elementId );
        }

        virtual double CallGetYieldStressDStrainRate( const int meshId, const int elementId ) const
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallGetYieldStressDStrainRate( meshId, elementId );
        }

        int SetTemperature(const double* temperature, const int meshId, const int elementId)
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::SetTemperature(temperature, meshId,elementId);
        };

        int SetElementalNodes( const int length,const double* values, const double time, const int meshId, const int elementId ) 
        {
          return DeformHandlerElement<T_COORDINATE>::SetElementalNodes(length,values,time, meshId,elementId);
        };

        double GetYieldStress(const int meshId, const int elementId)
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::GetYieldStress(meshId,elementId);
        };

        virtual int GetElementalVariables(const int length,double* values, const int meshId, const int elementId) 
        {return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::CallGetElementalVariables(length,values,meshId,elementId);};

        virtual int SetEffectiveStrainRate(const double* effStrainRate, const int meshId, const int elementId)
        {return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallSetEffectiveStrainRate(effStrainRate, meshId,elementId);};
      };



      //////////////////////////////////////////////////////////////////////////
      // Backward compatibility
      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL = NullIV>
      class DeformHandlerMesh
        : public DeformHandlerMeshComponent<T_COORDINATE>
        , public DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE> >
        , public DeformHandlerStressComponent<T_COORDINATE, DeformHandlerElement<T_COORDINATE> >
        , public DeformHandlerTimeComponent<T_COORDINATE>
        , public DeformHandlerUserProcMesh
      {
      private:
        NullIV* dumbIVModel;
      public:
        typedef mpt::NamedPoint<mpt::NamedPointsContainer<mpt::NamedPointsStorageLastTwo<mpr::Coordinates2D> > > NamedPoint;

        DeformHandlerMesh(boost::shared_ptr<ExternalSoftwareInputs> yieldStressModel);
        DeformHandlerMesh(boost::shared_ptr<ExternalSoftwareInputs> yieldStressModel,T_INTERNAL_VARIABLE_MODEL* ivModel);
        DeformHandlerMesh(boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel,
          boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressStrainModel,
          boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressStrainRateModel,
          T_INTERNAL_VARIABLE_MODEL* ivModel);

        // Just for backward compatibility, due to deriving from DeformHandlerUserProc
        virtual int CallSetElementalVariables( const int length,const double* values, const int meshId, const int elementId )
        {
          return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::CallSetElementalVariables(length,values,meshId,elementId);
        }

        virtual int CallGetElementalVariables( const int length,double* values, const int meshId, const int elementId )
        {
          return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::CallGetElementalVariables( length,values, meshId, elementId );
        }

        virtual double CallGetElementalVariables( const unsigned int whichVariable ) const
        {
          return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::CallGetElementalVariables( whichVariable );
        }

        virtual int CallSetGlobalVariables( const int length,const double* values, const int meshId, const int elementId )
        {
          return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::CallSetGlobalVariables( length,values, meshId, elementId );
        }

        virtual int CallSetStepLength( const double* timeStepLength, const int meshId, const int elementId )
        {
          return DeformHandlerTimeComponent<T_COORDINATE>::CallSetStepLength( timeStepLength, meshId, elementId );
        }

        virtual int CallSetElementalNodes( const int length,const double* values, const double time, const int meshId, const int elementId )
        {
          return DeformHandlerElement<T_COORDINATE>::CallSetElementalNodes(length,values,time,meshId,elementId);
        }

        virtual int CallSetTemperature( const double* temperature, const int meshId, const int elementId )
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallSetTemperature( temperature, meshId, elementId );
        }

        virtual int CallSetEffectiveStrainRate( const double* effStrainRate, const int meshId, const int elementId )
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallSetEffectiveStrainRate( effStrainRate, meshId, elementId );
        }

        virtual double CallGetYieldStress( const int meshId, const int elementId ) const
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallGetYieldStress( meshId, elementId );
        }

        virtual double CallGetYieldStressDStrain( const int meshId, const int elementId ) const
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallGetYieldStressDStrain( meshId, elementId );
        }

        virtual double CallGetYieldStressDStrainRate( const int meshId, const int elementId ) const
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallGetYieldStressDStrainRate( meshId, elementId );
        }

        int SetTemperature(const double* temperature, const int meshId, const int elementId)
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::SetTemperature(temperature, meshId,elementId);
        };

        int SetElementalNodes( const int length,const double* values, const double time, const int meshId, const int elementId ) 
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::SetElementalNodes(length,values,time, meshId,elementId);
        };

        double GetYieldStress(const int meshId, const int elementId)
        {
          return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::GetYieldStress(meshId,elementId);
        };

        virtual int CallSetMesh( const double * nodalCoordinates, const double * nodalDisplacements, const double currentTime, const double currentTimeStep, const unsigned int elementsNumber, const unsigned int nodesNumber, const int * connectivityMatrix )
        {
          return DeformHandlerMeshComponent<T_COORDINATE>::CallSetMesh( nodalCoordinates, nodalDisplacements, currentTime, currentTimeStep, elementsNumber, nodesNumber, connectivityMatrix );
        }

        virtual int CallSetElement( const double time, const int meshId, const int elementId )
        {
          return DeformHandlerMeshComponent<T_COORDINATE>::CallSetElement( time, meshId, elementId );
        }

        virtual int SetMesh(const double * nodalCoordinates, const double * nodalDisplacements, const double currentTime, const double currentTimeStep, const unsigned int elementsNumber, const unsigned int nodesNumber, const int * connectivityMatrix)
        {
          return DeformHandlerMeshComponent<T_COORDINATE>::SetMesh(nodalCoordinates, nodalDisplacements, currentTime, currentTimeStep, elementsNumber, nodesNumber, connectivityMatrix);
        }

        virtual int GetElementalVariables(const int length,double* values, const int meshId, const int elementId) 
        {return DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::CallGetElementalVariables(length,values,meshId,elementId);};

        virtual int SetEffectiveStrainRate(const double* effStrainRate, const int meshId, const int elementId)
        {return DeformHandlerStressComponent<T_COORDINATE,DeformHandlerElement<T_COORDINATE> >::CallSetEffectiveStrainRate(effStrainRate, meshId,elementId);};

        virtual int CallSetTime( const double* time, const int meshId, const int elementId )
        {
          return DeformHandlerTimeComponent<T_COORDINATE>::CallSetTime(time, meshId,elementId);
        }

      };

      //////////////////////////////////////////////////////////////////////////

      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL /*= NullIV*/>
      am3::software::deform::DeformHandlerMesh<T_COORDINATE, T_INTERNAL_VARIABLE_MODEL>::DeformHandlerMesh( boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel, boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressStrainModel, boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressStrainRateModel, T_INTERNAL_VARIABLE_MODEL* ivModel )
        : DeformHandlerStressComponent<T_COORDINATE, DeformHandlerElement<T_COORDINATE> >::DeformHandlerStressComponent(yieldStressModel,yieldStressStrainModel,yieldStressStrainRateModel,this)
        , DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::DeformHandlerIVComponent(ivModel,this)
      {

      }

      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL /*= NullIV*/>
      am3::software::deform::DeformHandlerMesh<T_COORDINATE, T_INTERNAL_VARIABLE_MODEL>::DeformHandlerMesh( boost::shared_ptr<ExternalSoftwareInputs> yieldStressModel,T_INTERNAL_VARIABLE_MODEL* ivModel )
        : DeformHandlerStressComponent<T_COORDINATE, DeformHandlerElement<T_COORDINATE> >::DeformHandlerStressComponent(yieldStressModel,this)
        , DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::DeformHandlerIVComponent(ivModel,this)
      {

      }



      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL /*= NullIV*/>
      am3::software::deform::DeformHandlerMesh<T_COORDINATE, T_INTERNAL_VARIABLE_MODEL>::DeformHandlerMesh( boost::shared_ptr<ExternalSoftwareInputs> yieldStressModel )
        : DeformHandlerStressComponent<T_COORDINATE, DeformHandlerElement<T_COORDINATE> >::DeformHandlerStressComponent(yieldStressModel,this)
        , DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::DeformHandlerIVComponent(dumbIVModel,this)
      {

      }

      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL /*= NullIV*/>
      am3::software::deform::DeformHandler<T_COORDINATE, T_INTERNAL_VARIABLE_MODEL>::DeformHandler( boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel, boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressStrainModel, boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressStrainRateModel, T_INTERNAL_VARIABLE_MODEL* ivModel )
        : DeformHandlerStressTimeComponent<T_COORDINATE, DeformHandlerElement<T_COORDINATE> >::DeformHandlerStressTimeComponent(yieldStressModel,yieldStressStrainModel,yieldStressStrainRateModel,this)
        , DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::DeformHandlerIVComponent(ivModel,this)
      {

      }

      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL /*= NullIV*/>
      am3::software::deform::DeformHandler<T_COORDINATE, T_INTERNAL_VARIABLE_MODEL>::DeformHandler(boost::shared_ptr<ExternalSoftwareInputs> yieldStressModel,T_INTERNAL_VARIABLE_MODEL* ivModel)
        : DeformHandlerStressTimeComponent<T_COORDINATE, DeformHandlerElement<T_COORDINATE> >::DeformHandlerStressTimeComponent(yieldStressModel,this)
        , DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::DeformHandlerIVComponent(ivModel,this)
      {
      }

      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL /*= NullIV*/>
      am3::software::deform::DeformHandler<T_COORDINATE, T_INTERNAL_VARIABLE_MODEL>::DeformHandler(boost::shared_ptr<ExternalSoftwareInputs> yieldStressModel)
        : DeformHandlerStressTimeComponent<T_COORDINATE, DeformHandlerElement<T_COORDINATE> >::DeformHandlerStressTimeComponent(yieldStressModel,this)
        , DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, DeformHandlerElement<T_COORDINATE>  >::DeformHandlerIVComponent(dumbIVModel,this)
      {
      }


    } //deform
  } //software
} //am3
#endif // deformHandlerBackwardCompatibility_h__
