@echo off
REM -- batch build DEF_SIM with user routine of 2d 
REM -- $Id: build_def_sim_usr_Absoftv110.bat,v 1.1.2.1 2011-03-09 20:16:40 pwei Exp $
REM -- Author : Michael Li
REM -- Date   : 03/25/05

set ABSOFTVERSION=Absoft11.0
set ABSOFTDRIVE=C:

set ABSOFT=%ABSOFTDRIVE%\%ABSOFTVERSION%
set path=%ABSOFTDRIVE%\%ABSOFTVERSION%\Bin;%path%
set ABSOFT_LIB=%ABSOFTDRIVE%\%ABSOFTVERSION%\LIB
set LIB=%ABSOFT_LIB%;%LIB%

set THE_MAKEFILE=DEF_SIM_USR_Absoftv110.amake

echo ****************************************
echo ***    Build DEF_SIM.EXE file        ***
echo ****************************************

set EXE_FILE=DEF_SIM.EXE

if not exist  ".\Objs" mkdir ".\Objs"
if not exist %THE_MAKEFILE% goto error1

AMAKE /f %THE_MAKEFILE% clean

AMAKE /f %THE_MAKEFILE% /e

if not exist %EXE_FILE% goto error

goto ok


:error1
echo !Can not find the make file of this program, please regenerate or reinstall DEFORM-3D !
goto error

:error
echo *********************************************
echo *** ERROR BUILDING DEF_SIM                ***
echo *** Please Check your source code         ***
echo *********************************************
goto done

:ok
echo ************************************
echo *** FINISH BUILDING EXECUTABLE   ***
echo ************************************
echo !! Now you can copy DEF_SIM.exe to your running directory. !!
goto done

:done
