
extern int DEFORM_USRE1_SEND(const int * length,const double * values	,	const int * meshId,	const int * elementId);
extern int DEFORM_USRE1_GET(const int * length,const double * values	,	const int * meshId,	const int * elementId);
extern int DEFORM_TEMPERATURE(const double * temperature	,	const int * meshId,	const int * elementId);
extern int DEFORM_YIELD_STRESS_GET(double * yieldStress,	const int * meshId,	const int * elementId);

//extern "C++" void DeleteDeformHandler();