 #include "deformHandler.h"
#include "../logger/logger.h"

namespace lo = am3::logger;
// #include "../caseEsaform2015/caseEsaform2015.h"
// 
// am3::software::deform::DeformHandlerUserProc* deformHandlerProc;
// am3::software::deform::DeformHandlerUserProcMesh* deformHandlerProcMesh;
// boost::shared_ptr<am3::software::deform::DeformHandlerUserProc> deformHandlerProcBoost;
// 
// am3::cases::esaform2015::Main*  mainInstance;
// am3::model::InitializableMainModel *m = mainInstance;
// 
// 
// 

am3::software::deform::DeformHandlerUserProc* deformHandlerProc;
am3::software::deform::DeformHandlerUserProcMesh* deformHandlerProcMesh;

bool SecureDeformHandlerExistance()
{
  ////DebugBreak();
 	if (deformHandlerProc!=nullptr)	return true;
 	else
  {
    lo::log<lo::SEV_CRITICAL>(lo::LOG_MODEL,"Main model not initialized!");
//     mainInstance = new am3::cases::esaform2015::Main;
//     mainInstance->Init();
    return false;
  }
    //return false;
}
