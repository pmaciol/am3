
/*! \file deformHandler.h **************************************************
* \author		Piotr Maciol
* \copyright 	GNU Public License.
* \brief       
* \details   	
*********************************************************************/
#ifndef deformHandler_h__
#define deformHandler_h__
/*	include *********************************************************/

#include <vector>

#include <boost/shared_ptr.hpp>

#include "../common/typelists.h"
#include "../models/conversions.h"
#include "../models/dataStructures.h"
#include "../models/contracts.h"
#include "../models/namedPoint.h"
#include "../models/point.h"
#include "../models/models.h"
#include "../adapter/ivstorage.h"

#include "deformOutputs.h"
#include "deformHandlerUserProc.h"
#include "deformHandlerMesh.h"

#include <iostream>
#include <fstream>
#include <limits>

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace moc = am3::model::contract;
namespace mo  = am3::model;
namespace ad = am3::adapter;
/*	extern **********************************************************/

//////////////////////////////////////////////////////////////////////////
// IMPORTANT!
// You have to chose coordinates type for Deform here!
//////////////////////////////////////////////////////////////////////////
// namespace am3{	namespace software	{		namespace deform		{
//   template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL> class DeformHandler;		}}}

//////////////////////////////////////////////////////////////////////////

/*	classes *********************************************************/
#undef max
const unsigned int kMaxNodesFromDeform = std::numeric_limits<int>::max() / 200;

bool SecureDeformHandlerExistance();
extern am3::software::deform::DeformHandlerUserProc* deformHandlerProc;
extern am3::software::deform::DeformHandlerUserProcMesh* deformHandlerProcMesh;


namespace am3
{
  namespace software
  {
    namespace deform
    {

    //////////////////////////////////////////////////////////////////////////
    // Time variables
    template<typename T_COORDINATE>
    class DeformHandlerTimeComponent : public DeformHandlerFacadeTime
    {
    public:
      mpr::StepLength GetStepLength() const;
      mpr::Time GetTime() const;
    protected:
      virtual int CallSetStepLength(const double* timeStepLength, const int meshId, const int elementId);
      virtual int CallSetTime(const double* time, const int meshId, const int elementId);
      mpr::StepLength timeStepLength_;
      mpr::Time time_;
    };

    //////////////////////////////////////////////////////////////////////////
    // Stress/Strain component (with temperature, probably should be moved to new class)
    template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
    class DeformHandlerStressComponent
      : public DeformHandlerFacadeMtr
    {
    public:
      typedef DeformStressContract2D ExternalSoftwareInputs;

      DeformHandlerStressComponent(boost::shared_ptr<ExternalSoftwareInputs> yieldStressModel, const T_MESH_REPRESENTATION* mesh);
      DeformHandlerStressComponent(boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel,
        boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressStrainModel,
        boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressStrainRateModel
        , const T_MESH_REPRESENTATION*  mesh);

      mpr::Temperature GetTemperatureIn(const mpt::Point<T_COORDINATE>& point) const;
      mpr::EffectiveStrainRate GetEffectiveStrainRateIn(const mpt::Point<T_COORDINATE>& point) const;


    protected:
      virtual int CallSetTemperature( const double* temperature, const int meshId, const int elementId );
      virtual int CallSetEffectiveStrainRate( const double* effStrainRate, const int meshId, const int elementId );
      virtual double CallGetYieldStress( const int meshId, const int elementId ) const;
      virtual double CallGetYieldStressDStrain( const int meshId, const int elementId ) const;
      virtual double CallGetYieldStressDStrainRate( const int meshId, const int elementId ) const;

      boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel_;
      boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressDStrainModel_;
      boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressDStrainRateModel_;
      mpr::Temperature temperature_;
      mpr::EffectiveStrainRate effStrainRate_;

    private:
      const T_MESH_REPRESENTATION* mesh_;
    };

    //////////////////////////////////////////////////////////////////////////
    // Internal variable (User variables in Deform) class
    template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL, typename T_MESH_REPRESENTATION>
    class DeformHandlerIVComponent
      : public DeformHandlerFacadeUpd
      , public ad::IvInterface<T_INTERNAL_VARIABLE_MODEL>
    {
    public:
      DeformHandlerIVComponent(){};
      DeformHandlerIVComponent(T_INTERNAL_VARIABLE_MODEL* internalVariableModel,  const T_MESH_REPRESENTATION* mesh): internalVariableModel_(internalVariableModel),mesh_(mesh){};
      virtual int CallGetElementalVariables(const int length,double* values, const int meshId, const int elementId);
      virtual double CallGetElementalVariables(const unsigned int whichVariable) const;
      virtual int CallGetElementalVariablesNumber() const ;
      virtual double CallGetGlobalVariables(const unsigned int whichVariable) const;
      virtual int CallSetElementalVariables( const int length,const double* values, const int meshId, const int elementId );
      virtual int CallSetGlobalVariables( const int length,const double* values, const int meshId, const int elementId );
      std::vector<double> GetElementalVariablesVector()const {return elementalVariables_;};
      virtual double GetElementalVariable(const unsigned int whichVariable) const
      {
       try
       {
         if (whichVariable > elementalVariables_.size()) throw "Too high number of IV";
        return elementalVariables_[whichVariable];
      }
      catch (const char* msg)
      {
        lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, msg);
        return 0.0;
      }
        
      };
      virtual double GetElementalVariables(const unsigned int whichVariable) const
      {
        try
        {
          if (whichVariable > elementalVariables_.size()) throw "Too high number of IV";
          return elementalVariables_[whichVariable];
        }
        catch (const char* msg)
        {
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, msg);
        }

      };

      virtual void SetIvProvider(T_INTERNAL_VARIABLE_MODEL* ivProvider)
      {
        internalVariableModel_ = ivProvider;
      }

    protected:
      T_INTERNAL_VARIABLE_MODEL* internalVariableModel_;
      unsigned lengthElemental_;
      std::vector<double> elementalVariables_;
      std::vector<double> globalVariables_;
      unsigned lengthGlobal_;

    private:
      const T_MESH_REPRESENTATION* mesh_;
    };


    //******************************************************************************************************************************************************************

    template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
    class DeformHandlerStressTimeComponent
      : public DeformHandlerStressComponent<T_COORDINATE, T_MESH_REPRESENTATION>
      , public DeformHandlerTimeComponent<T_COORDINATE>
    {
    public:
      DeformHandlerStressTimeComponent(boost::shared_ptr<ExternalSoftwareInputs> yieldStressModel, const T_MESH_REPRESENTATION* mesh) : DeformHandlerStressComponent(yieldStressModel, mesh){};

    };
    } //deform
  } //software
} //am3

// Implementation files with template based classes bodies
#include "deformHandlerBackwardCompatibility.h"
#include "deformHandlerImpl.h"

extern am3::software::deform::DeformHandlerUserProc* deformHandlerProc;

#endif // deformHandler_h__


