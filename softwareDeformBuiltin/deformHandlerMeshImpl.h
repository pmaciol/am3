
/*! \file deformHandlerMeshImpl.h **************************************************
* \author		Piotr Maciol
* \copyright 	GNU Public License.
* \brief       
* \details   	
*********************************************************************/
#ifndef deformHandlerMeshImpl_h__
#define deformHandlerMeshImpl_h__
/*	include *********************************************************/



/*	using ***********************************************************/



/*	extern **********************************************************/

extern am3::model::InitializableMainModel* m;

/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace deform
    {

      template<typename T_COORDINATE>
      boost::shared_ptr<typename am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::NamedPoint> am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::GetMidPoint() const
      {
        return midPoint_;
      };

      template<typename T_COORDINATE>
      typename am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::NamedPoint am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::GetNodalPoint( const unsigned int whichPoint ) const
      {
        if (whichPoint<nodalPoints_.size())
        {
          return *nodalPoints_[whichPoint];
        } 
        else
        {
          return *nodalPoints_[0];
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Asked for node without a range");
        }
      };

      template<typename T_COORDINATE>
      boost::shared_ptr<typename am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::NamedPoint> am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::CreateMidPoint( const int meshId, const int elementId, const double time )
      {
        const unsigned long id = GenerateMidPointId(meshId,elementId);
        T_COORDINATE coord = GetMidPointCoordinates();
        return RegisterPoint(id,coord,time);
      }

      template<typename T_COORDINATE>
      const unsigned int am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::GetMeshId() const
      {
        return meshId_;
      }


      template<typename T_COORDINATE>
      boost::shared_ptr<typename am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::NamedPoint> am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::RegisterPoint( const unsigned long id, T_COORDINATE coord, const double time )
      {
        return boost::shared_ptr<NamedPoint>(new NamedPoint(id,coord,time));
      }

      template<typename T_COORDINATE>
      const unsigned long am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::GenerateMidPointId( const int meshId, const int elementId )
      {
        return (1 + meshId) + (kMaxNodesFromDeform + 1 + elementId) * 100;
      }

      template<typename T_COORDINATE>
      T_COORDINATE am3::software::deform::DeformHandlerInternalMesh<T_COORDINATE>::GetMidPointCoordinates()
      {
        std::vector<boost::shared_ptr<mpt::Point<T_COORDINATE> > > points(nodalPoints_.size());
        std::copy(nodalPoints_.begin(),nodalPoints_.end(),points.begin());
        return mo::conversions::MeanPoint4Nodes(points);
      }

      //////////////////////////////////////////////////////////////////////////

      template<typename T_COORDINATE>
      int am3::software::deform::DeformHandlerElement<T_COORDINATE>::CallSetElementalNodes( const int length,const double* values, const double time, const int meshId, const int elementId )
      {
        bool isSuccess=false;
        meshId_ = meshId;
        if (NewElement(time, meshId, elementId))
        {
          elementId_ = elementId;
          //SendNewDataFlag();
          int dimensions=T_COORDINATE::variables;
          nodalPoints_.clear();
          for (int i = 0;i<length;i++)
          {
            nodalPoints_.push_back(CreateNodalPoint(values+(i*dimensions), time, meshId, elementId,i));
          }
          midPoint_=CreateMidPoint(meshId, elementId, time);
        }
//        SetTime(time);
        isSuccess=true;
        return isSuccess;
      }

      template<typename T_COORDINATE>
      boost::shared_ptr<typename am3::software::deform::DeformHandlerElement<T_COORDINATE>::NamedPoint> am3::software::deform::DeformHandlerElement<T_COORDINATE>::CreateNodalPoint( const double* values, const double time, const int meshId, const int elementId, const int nodeId )
      {
        T_COORDINATE coord(values);
        const unsigned long id = GenerateNodeId(meshId,elementId,nodeId);
        return RegisterPoint(id,coord,time);
      }


      template<typename T_COORDINATE>
      const unsigned long am3::software::deform::DeformHandlerElement<T_COORDINATE>::GetElementId( const unsigned long& codedId ) const
      {
        return elementId_;
      }

      template<typename T_COORDINATE>
      const bool am3::software::deform::DeformHandlerElement<T_COORDINATE>::NewElement( const double time, const int meshId, const int elementId ) const
      {
        if (PointsInitialized())
        {
          boost::shared_ptr<NamedPoint> midPoint = GetMidPoint();
          const unsigned int lastMeshId = GetMeshId();
          const unsigned long lastElementId = GetElementId(midPoint->GetId());
          const double lastTime = midPoint->GetTime();
          if ((lastTime == time )&&(meshId == lastMeshId)&&(elementId==lastElementId))
          {
            return false;
          } 
          else
          {
            return true;
          }
        } 
        else
        {
          return true;
        }
      }


      template<typename T_COORDINATE>
      const bool am3::software::deform::DeformHandlerElement<T_COORDINATE>::PointsInitialized() const
      {
        if (midPoint_) return true;
        else return false;
      }


      template<typename T_COORDINATE>
      const unsigned long am3::software::deform::DeformHandlerElement<T_COORDINATE>::GenerateNodeId( const int meshId, const int elementId, int i )
      {
        return meshId + (i + 1) * 10 + elementId * 100;
      }

      //////////////////////////////////////////////////////////////////////////


      template<typename T_COORDINATE>
      int am3::software::deform::DeformHandlerMeshComponent<T_COORDINATE>::CallSetMesh( const double * nodalCoordinates, const double * nodalDisplacements, const double currentTime, const double currentTimeStep, const unsigned int elementsNumber, const unsigned int nodesNumber, const int * connectivityMatrix )
      {
        if (m->IsInitialized()==false) m->Init();
        int toRet = 1;
        const int dimensions=T_COORDINATE::variables;
        double presentCoord[dimensions];
        if (nodesNumber <= kMaxNodesFromDeform)
        {
          nodesNumber_ = nodesNumber;
          conMat_.clear();

          for (unsigned int i = 0; i < nodesNumber; i++)
          {
            for (unsigned int j = 0; j < dimensions; j++)
            {
              presentCoord[j] = nodalCoordinates[i*dimensions+j];
            }
            T_COORDINATE coord(presentCoord);
            RegisterPoint(i,coord,currentTime);
          }

          unsigned int nodesPerElement;
          if (dimensions==2) {nodesPerElement = 4;}
          else {nodesPerElement = 8;}

          unsigned int elementNumberPositionInConnectivityMatrix = 0;
          const int elementNumberInConnectivityMatrix = 1;
          for (unsigned int i = 0; i < elementsNumber; i++)
          {
            std::vector<unsigned int> nodesInElement;
            std::pair<unsigned int,std::vector<unsigned int> > p(i,nodesInElement);


            const unsigned int connectivityMatrixLength = elementsNumber * (nodesPerElement);
            unsigned int nodePositionInConnectivityMatrix = 0;
            for (unsigned int j = 0; j < nodesPerElement; j++)
            {
              ++elementNumberPositionInConnectivityMatrix;
              p.second.push_back(connectivityMatrix[elementNumberPositionInConnectivityMatrix]);
            }
            conMat_.insert(p);
            ++elementNumberPositionInConnectivityMatrix;
          }
        } 
        else
        {
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Too much nodes in Deform Mesh; nodes number higher than kMaxNodesFromDeform limit");
          toRet = 0;
        }
        return toRet;
      }

      template<typename T_COORDINATE>
      int am3::software::deform::DeformHandlerMeshComponent<T_COORDINATE>::CallSetElement( const double time, const int meshId, const int elementId )
      {
        unsigned int toRet;
        time_(time);
        if (conMat_.size()>0)
        {
          nodalPoints_.clear();
          std::vector<unsigned int>::iterator it = conMat_.find(elementId)->second.begin();
          elementId_ = elementId;
          while (it < conMat_.find(elementId)->second.end())
          {
            boost::shared_ptr<NamedPoint> node(new NamedPoint(*it));
            nodalPoints_.push_back(node);
            it++;
          }
          const unsigned long midId = GenerateMidPointId( meshId, elementId );
          toRet = midId;
          boost::shared_ptr<NamedPoint> pnt(new NamedPoint(midId, this->GetMidPointCoordinates(),time));
          midPoint_ = pnt;
        } 
        else
        {
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Connectivity matrix not initialized; wrong element creation method");
          toRet = 0;
        }
        toRet = !!toRet;
        return toRet;
      }



    }
  }
}

#endif // deformHandlerMeshImpl_h__
