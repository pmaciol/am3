To link this solution with Deform:
* build project as dll, debug or release
* run VS command prompt and change to this folder
* run runMeDebug.bat or runMeRelease.bat
* copy am3softwareDeform.lib to Deform User Routine Folder (lib subfolder)
* copy usr_mtr.f, usr_upd.f and build_def_sim_usr_Absoftv110.bat to Deform User Routine Folder
* copy am3softwareDeformBuiltin.dll to Deform executable folder
* run Absoft console and change to Deform User Routine Folder
* build_def_sim_usr_Absoftv110.bat 
* follow Deform script instructions

To test:
* build project as dll, debug
* run VS command prompt and change to this folder
* run runMeDebug.bat
* in VS, set am3softwareDeformBuiltin properties / Debugging / Command as $(ProjectDir)$(Configuration)\test.exe
* start debugging