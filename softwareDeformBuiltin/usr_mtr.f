C********************************************************************
C Yield stress computations delegated to C++ lib
C Author: Piotr Maciol
C Date: 12.07.2013
C Provided procedure: UFLOW1


      SUBROUTINE USRMTR(NPTRTN,YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C     THIS SUBROUTINE CALCULATES THE FLOW STRESS OF PLASTIC MATERIAL
C
C     INPUT :
C
C      NPTRTN    =   FLOW STRESS NUMBER
C      TEPS      =   EFFECTIVE STRAIN
C      EFEPS     =   EFFECTIVE STRAIN RATE
C      TEMP      =   TEMPERATURE
C
C     OUTPUT :
C
C      YS        =   FLOW STRESS
C      YPS       =   DERIVATIVE OF FLOW STRESS W.R.T TEPS
C      FIP       =   DERIVATIVE OF FLOW STRESS W.R.T. EFEPS
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C  USER SUPPLIED SUBROUTINES
C
C
C ****    USER DEFINED VARIABLES ****
C
      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C     
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     Version 5.1
C
C     COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,

C    +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),USRE1(2),USRE2(2),
C    +                NODEE(4),KELE
C
C     Version 6.0
C
C     COMMON /ELMCOM/
C
C         RZE     : Four corner coordinates
C         URZE    : Velocity
C         STSE    : Stress
C         EPSE    : Strain rate
C         EFEPSE  : effective strain rate
C         EFSTSE  : Effective stress
C         TEPSE   : Total effective strain
C         RDTYE   : Density
C         TEMPE   : Temperature
C         DTMPE   : Temperature rate
C         DAMAGE  : Damage value
C         USRE1   : Element user state variable 1
C         USRE2   : Element user state variable 2
C         USRNE   : Nodal user state variables 1,2 at 4 nodes
C         NODEE   : Connectivity
C         KELE    : Global element number
C         KELEL   : Local element number
C         KGROUP  : Material group number
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP

C
C     COMMON /NODCOM/
C
C        RZN      : Nodal point coordinates
C        URZN     : Nodal point velocities
C        DRZN     : Nodal point displacement
C        TEMPN    : Nodal point temperature
C        USRN1    : User defined state variables (Input : At the beginning of Step N)
C        USRN2    : User defined state variables (Output: At the end of Step N)
C        KNODE    : Node number
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE

C
C     CURTIM: CURRENT TIME           
C      
      COMMON /CLOK/ CURTIM
C
C     DTMAXC: CURRENT TIME STEP SIZE
C
      COMMON /SSTU/ DTMAXC
C
C
C     COMMON /USRCTL/
C
C        KOBJ      : Object number
C        KSTEP     : Step number (N)
C        ISTATUS   : 0 - the beginning of the step
C                    1 - the end of the step
C        KSSTEP    : negative step indication -1 for negative step 1 for else
C
C     WHEN (ISTATUS.EQ. 1)  --> USRE2/USRN2 should be updated here
C     KELE  > 0             --> Element data is active
C     INODE > 0             --> Node Data is active
C
      COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP
C
C
C     Branching to proper flow stress routine based on the
C     number specified in the pre-processor
C     
C
      GO TO (510,520,530,540,550,560,570,580,590,600),NPTRTN 
C     
  510 CALL UFLOW1(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  520 CALL UFLOW2(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  530 CALL UFLOW3(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  540 CALL UFLOW4(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  550 CALL UFLOW5(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  560 CALL UFLOW6(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  570 CALL UFLOW7(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  580 CALL UFLOW8(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  590 CALL UFLOW9(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
  600 CALL UFLOW10(YS,YPS,FIP,TEPS,EFEPS,TEMP)
      RETURN
C
C  TO BE CONTINUED BY USER
C
      END
C********************************************************************

      SUBROUTINE UFLOW2(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C
C********************************************************************
      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N) 
C
C ****    USER DEFINED VARIABLES ****
C
      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C     
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     DESCRIPTION
C
C       THIS ROUTINE IS USED TO DEMONSTRATE THE IMPLEMENTATION OF
C       MATERIAL ROUTINE.  ALL THE REAL VARIABLES SHOULD BE DOUBLE 
C       PRECISION. THE DEFINITION OF ARGUMENTS ARE DESCRIBED AS FOLLOWS:
C
C    INPUT :
C
C      TEPS     =   EFFECTIVE STRAIN
C      EFEPS    =   EFFECTIVE STRAIN RATE
C      TEMP     =   TEMPERATURE
C
C    OUTPUT :
C
C      YS       =   FLOW STRESS
C      YPS      =   DERIVATIVE OF FLOW STRESS W.R.T. TEPS
C      FIP      =   DERIVATIVE OF FLOW STRESS W.R.T. EFEPS
C
C
      COMMON /ELMCOM/ RZE(2,4),URZE(2,4),STSE(4),EPSE(4),EFEPSE,EFSTSE,
     +                TEPSE,RDTYE,TEMPE(4),DTMPE(4),DAMAGE,
     +                USRE1(1500),USRE2(1500),
     +                USRNE(1500,4),NODEE(4),KELE,KELEL,KGROUP
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     DOES IT WORKS?
C
      COMMON /NODCOM/ RZN(2),URZN(2),DRZN(2),TEMPN,DTMPN,USRN1(1500),
     +                USRN2(1500),KNODE
	  COMMON /USRCTL/ KOBJ,ISTATUS,KSTEP,KSSTEP

C
C     CURTIM: CURRENT TIME           
C      
      COMMON /CLOK/ CURTIM
C
C     DTMAXC: CURRENT TIME STEP SIZE
C
      COMMON /SSTU/ DTMAXC
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      DOUBLE PRECISION yieldStress
      DOUBLE PRECISION yieldStressDstrainRate
      DOUBLE PRECISION yieldStressDstrain
      DOUBLE PRECISION Temp
	  DOUBLE PRECISION COORD(8)
	  
      integer*4 DEFORMMTRCRESULT
      integer*4 DEFORM_YS      
	  integer*4 DEFORM_USRE1_SEND
c      IF(CURTIM.GT.0.0) THEN
c	  open (unit = 2, file = "usr.txt", ACCESS = 'APPEND')
c	  write(2, *)CURTIM, KOBJ, ISTATUS, KSTEP, KELE, KELEL, KNODE, USRE1(1)
c     +, USRE1(2)
c	  close(2)	 
c	  ENDIF
      Temp=(TEMPE(1)+TEMPE(2)+TEMPE(3)+TEMPE(4))/4      
      
C     Check it - Curtim added by me, I am not sure it works!
C     Set data
      DEFORMMTRCRESULT=CALL_DEFORM_TIME(CURTIM,KGROUP,KELEL)
	  do 10 i = 1, 8 ,2
	  COORD(i)=RZE(1,i)
	  COORD(i+1)=RZE(2,i)
  10  continue
c     The line below should be used when mesh facade is used
c      DEFORMMTRCRESULT=CALL_DEFORM_ELEM_SET(CURTIM,KGROUP,KELEL);
      DEFORMMTRCRESULT=CALL_DEFORM_NODES_SEND(
     +4,RZE,CURTIM,KGROUP,KELE)
	 
      DEFORMUPDCRESULT=CALL_DEFORM_USRE1_SEND(4,USRE1,
     +KGROUP,KELE)
	 
      DEFORMMTRCRESULT=CALL_DEFORM_TEMPERATURE(Temp,KGROUP,KELE)
	  
	  DEFORMMTRCRESULT=CALL_DEFORM_TIME_STEP_LENGTH(DTMAXC,KGROUP,KELE)
      
c      open (unit = 2, file = "strRate.txt", ACCESS = 'APPEND')
c      write(2, *)EFEPSE, KGROUP, KELE
c      close(2)	
	  
      DEFORMMTRCRESULT=CALL_DEFORM_EFF_STR_RATE(
     +EFEPSE,KGROUP,KELE)

C     Now get results
      DEFORMMTRCRESULT=CALL_DEFORM_YIELD_STRESS_GET(
     +yieldStress,KGROUP,KELE)

      DEFORMMTRCRESULT=CALL_DEFORM_YIELD_STRESSDSTRAIN_GET(
     +yieldStressDstrain,KGROUP,KELE)

      DEFORMMTRCRESULT=CALL_DEFORM_YIELD_STRESSDSTRAINRATE_GET(
     +yieldStressDstrainRate,KGROUP,KELE)

C     And set return values      
      YS = yieldStress
      YPS = yieldStressDstrain
      FIP = yieldStressDstrainRate

      
c	  YS = 2e10
c      YPS = 0
c      FIP = 0
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW1(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION 
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW3(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW4(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW5(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW6(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW7(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW8(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW9(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************

      SUBROUTINE UFLOW10(YS,YPS,FIP,TEPS,EFEPS,TEMP)

C********************************************************************
C
C
C
C********************************************************************

      IMPLICIT REAL*8 (A-H,O-Z), INTEGER*4 (I-N)
C
C ****    USER DEFINED VARIABLES ****

      CHARACTER*80 IUSRVL
      COMMON /IUSR/ IUSRVL(10)
C
C     TO READ DATA (10 RESERVED LINES)
C        READ(IUSRVL(LINE NUMBER),*) DATA1,DATA2,DATA3...
C
C     TO WRITE DATA (10 RESERVED LINES)
C        WRITE(IUSRVL(LINE NUMBER),*) NEWDATA1, NEWDATA2, NEWDATA3 ...
C
C ****     END    ****
C
C     SEE UFLOW1 FOR DETAILED DESCRIPTION
C
      RETURN
      END
C********************************************************************
