
/*! \file deformHandlerMesh.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef deformHandlerMesh_h__
#define deformHandlerMesh_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../models/dataStructures.h"
#include "../models/namedPoint.h"
#include "deformOutputs.h"

/*	using ***********************************************************/

namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace deform
    {
      // The class containing operations on Deform mesh items common for all implementations
      template<typename T_COORDINATE>
      class DeformHandlerInternalMesh
      {
      public:
        typedef mpt::NamedPoint<mpt::NamedPointsContainer<mpt::NamedPointsStorageLastTwo<mpr::Coordinates2D> > > NamedPoint;

        boost::shared_ptr<NamedPoint> GetMidPoint() const;
        NamedPoint GetNodalPoint(const unsigned int whichPoint) const;
        const unsigned int GetMeshId() const;
        void SetTime(const double & time){time_(time);};

      protected:
        boost::shared_ptr<NamedPoint> RegisterPoint( const unsigned long id, T_COORDINATE coord, const double time );
        const unsigned long GenerateMidPointId( const int meshId, const int elementId );
        T_COORDINATE GetMidPointCoordinates();
        boost::shared_ptr<NamedPoint> CreateMidPoint( const int meshId, const int elementId, const double time );
        

        boost::shared_ptr<NamedPoint> midPoint_;
        std::vector<boost::shared_ptr<NamedPoint> > nodalPoints_;
        unsigned long elementId_;
        unsigned long meshId_;
        unsigned long nodesNumber_;
        mpr::Time time_;
      };

      //////////////////////////////////////////////////////////////////////////
      // The class supporting operations on elemenets. Can be used separatelly or as a base class, enhanced with mesh operations
      template<typename T_COORDINATE>
      class DeformHandlerElement 
        : public DeformHandlerInternalMesh<T_COORDINATE>
        , public DeformHandlerFacadeElm
      {
      public:
        virtual int CallSetElementalNodes( const int length,const double* values, const double time, const int meshId, const int elementId );
        const unsigned long GetElementId(const unsigned long& codedId) const;
        mpr::ElementNodesNumber GetNodesNumber() const;

      protected:
        const bool PointsInitialized() const;
        const bool NewElement( const double time, const int meshId, const int elementId ) const;
        boost::shared_ptr<NamedPoint> CreateNodalPoint( const double* values, const double time, const int meshId, const int elementId, const int nodeId);
        const unsigned long GenerateNodeId( const int meshId, const int elementId, int i );
      };

      //////////////////////////////////////////////////////////////////////////
      // The class to be used when full Deform mesh is used
      template<typename T_COORDINATE>
      class DeformHandlerMeshComponent
        : public DeformHandlerFacadeMsh
        , public DeformHandlerElement<T_COORDINATE>
      {
      public:
        virtual int CallSetMesh(const double * nodalCoordinates, const double * nodalDisplacements, const double currentTime, const double currentTimeStep, const unsigned int elementsNumber, const unsigned int nodesNumber, const int * connectivityMatrix);
        virtual int CallSetElement( const double time, const int meshId, const int elementId );

      protected:
        std::map<unsigned int,std::vector<unsigned int> > conMat_;
      };
    }
  }
}

#include "deformHandlerMeshImpl.h"

#endif // deformHandlerMesh_h__
