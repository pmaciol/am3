
/*! \file usr_msh_lib.cpp **************************************************
 * \author		Piotr Maciol
 * \brief     
 * \details   	
*********************************************************************/
#include "deformHandler.h"
#include "logger/logger.h"
#include <fstream>

extern am3::software::deform::DeformHandlerUserProcMesh* deformHandlerProcMesh;
//extern am3::software::deform::DeformHandlerFacadeMsh* deformHandlerProcMesh;
extern void CreateDeformHandler();

using namespace std;
namespace lo = am3::logger;

__declspec( dllexport ) int DEFORM_MESH_SET(const double * nodalCoordinates,
  const double * nodalDisplacements,
  const double * currentTime,
  const double * currentTimeStep,
  const int * elementsNumber,
  const int * nodesNumber,
  const int * connectivityMatrix)
{
	if (SecureDeformHandlerExistance())
	{
//    //DebugBreak();
    std::ostringstream str;
    str << "calling SetMesh with params: time: " << currentTime << " currentTimeStep: " << currentTimeStep << "elements number " << elementsNumber << "nodes number "<< nodesNumber;
//     std::string s = str.str();
//     const char* p = s.c_str();

    lo::log<lo::SEV_INFORMATIONAL>(lo::LOG_MODEL,str.str().c_str());
		return deformHandlerProcMesh->SetMesh(nodalCoordinates, nodalDisplacements,*currentTime,*currentTimeStep,*elementsNumber,*nodesNumber,connectivityMatrix);
	}	
	else return 0;
}


__declspec( dllexport ) int DEFORM_ELEM_SET(double const * currentTime,
const int * meshId,
const int * elementId)
{
//  //DebugBreak();
  if (SecureDeformHandlerExistance())
  {
    return deformHandlerProcMesh->SetElement(*currentTime,*meshId,*elementId);
  }	
  else return 0;
}
