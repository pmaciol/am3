
/*! \file usr_mtr_lib.cpp **************************************************
 * \author		Piotr Maciol
 * \brief     Provides DEFORM_YS method for deform usr_mtr procedures
 * \details   	
*********************************************************************/
#include "deformHandler.h"
#include <fstream>

extern am3::software::deform::DeformHandlerUserProc* deformHandlerProc;
extern void CreateDeformHandler();

using namespace std;

__declspec( dllexport ) int DEFORM_YIELD_STRESS_GET(double * yieldStress,
	const int * meshId,
	const int * elementId)
// extern "C" int DEFORM_YIELD_STRESS_GET(double * yieldStress,
// 	const int * meshId,
// 	const int * elementId)
{
	if (SecureDeformHandlerExistance())
	{
		*yieldStress = deformHandlerProc->GetYieldStress(*meshId, *elementId);
		return 0;
	}	
	else return -1;
}

//  extern "C"   __declspec( dllexport ) int DEFORM_YIELD_STRESS_GET(double * yieldStress,
// 	const int * meshId,
// 	const int * elementId)
// 	// extern "C" int DEFORM_YIELD_STRESS_GET(double * yieldStress,
// 	// 	const int * meshId,
// 	// 	const int * elementId)
// {
// 	if (SecureDeformHandlerExistance())
// 	{
// 		*yieldStress = deformHandler->GetYieldStress(*meshId, *elementId);
// 		return 0;
// 	}	
// 	else return -1;
// }

__declspec( dllexport )  int DEFORM_YIELD_STRESSDSTRAIN_GET(double * yieldStressDstrain,
	const int * meshId,
	const int * elementId)
{
	if (SecureDeformHandlerExistance())
	{
		*yieldStressDstrain = deformHandlerProc->GetYieldStressDStrain(*meshId, *elementId);
		return 0;
	}
	else return -1;
}

__declspec( dllexport )  int DEFORM_YIELD_STRESSDSTRAINRATE_GET(double * yieldStressDstrainRate,
	const int * meshId,
	const int * elementId)
{
	if (SecureDeformHandlerExistance()) 
		{
			*yieldStressDstrainRate = deformHandlerProc->GetYieldStressDStrainRate(*meshId, *elementId);
			return 0;
	}
	else return -1;
}

__declspec( dllexport )  int DEFORM_TEMPERATURE(const double * temperature,
	const int * meshId,
	const int * elementId)
{
//  //DebugBreak();
	if (SecureDeformHandlerExistance()) return deformHandlerProc->SetTemperature(temperature,*meshId, *elementId);
	else return -1;
}

__declspec( dllexport )  int DEFORM_EFF_STR_RATE(const double * effStrainRate,
	const int * meshId,
	const int * elementId)
{
//  //DebugBreak();
	if (SecureDeformHandlerExistance()) return deformHandlerProc->SetEffectiveStrainRate(effStrainRate,*meshId, *elementId);
	else return -1;
}

__declspec( dllexport ) int DEFORM_TIME(
  const double * time,
	const int * meshId,
	const int * elementId)
{
	if (SecureDeformHandlerExistance()) return deformHandlerProc->SetTime(time,*meshId, *elementId);
	else return -1;
}

//extern "C" int DEFORM_NODES_SEND(
__declspec( dllexport ) int DEFORM_NODES_SEND(
	const int * nodesNumber,
	const double * coordinates,
	const double * time,
	const int * meshId,
	const int * elementId
	)
{
  ////DebugBreak();
	if (SecureDeformHandlerExistance()) return deformHandlerProc->SetElementalNodes(*nodesNumber, coordinates, *time, *meshId, *elementId);
	else return -1;
}