include_directories (../)

SET(ReadMe readMe.txt runMeRelease.bat runMeDebug.bat)

SOURCE_GROUP("Header Impl" REGULAR_EXPRESSION .*Impl.*)
SOURCE_GROUP("Deform side" FILES build_def_sim_usr_Absoftv110.bat REGULAR_EXPRESSION .*f$)
SOURCE_GROUP("Read Me" FILES ${ReadMe})

add_library(am3softwareDeformBuiltin SHARED
${ReadMe}
# Files below cannot be added to VS project due to linking problem. They are used only in testing of this library
# test.h test.cpp
usr_mtr_lib.cpp
usr_upd_lib.cpp
usr_msh_lib.cpp
usr_crp_lib.cpp
usr_mtr.f
usr_upd.f
usr_msh.f
usr_crp.f
deformHandler.cpp deformHandler.h deformHandlerImpl.h
main.c
build_def_sim_usr_Absoftv110.bat
deformHandlerUserProc.h 
deformHandlerAprox.h
deformOutputs.h
deformHandlerOutputsProviders.h
deformHandlerMesh.h deformHandlerMeshImpl.h
deformHandlerBackwardCompatibility.h
)

target_link_libraries(am3softwareDeformBuiltin
am3models
am3logger 

#am3caseCreep
#am3caseEsaform2015
)
 
SET_TARGET_PROPERTIES(am3softwareDeformBuiltin  PROPERTIES LINKER_LANGUAGE CXX)

SET(CMAKE_SHARED_LINKER_FLAGS /NODEFAULTLIB:\"LIBCMTD.lib;libcpmtd.lib;LIBCMT.lib;libcpmt.lib;\")

add_custom_command(TARGET am3softwareDeformBuiltin POST_BUILD        # Adds a post-build event to MyTest
    COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
        "${CMAKE_BINARY_DIR}/softwareDeformBuiltin/Debug/am3softwareDeformBuiltin.dll"      # <--this is in-file
        "${CMAKE_BINARY_DIR}/tests/Debug/am3softwareDeformBuiltin.dll")                 # <--this is out-file path

add_custom_command(TARGET am3softwareDeformBuiltin POST_BUILD        # Adds a post-build event to MyTest
    COMMAND ${CMAKE_COMMAND} -E copy_if_different  # which executes "cmake - E copy_if_different..."
        "${CMAKE_BINARY_DIR}/softwareDeformBuiltin/Release/am3softwareDeformBuiltin.dll"      # <--this is in-file
        "${CMAKE_BINARY_DIR}/tests/Release/am3softwareDeformBuiltin.dll")                 # <--this is out-file path
