__declspec( dllimport ) int DEFORM_TIME_STEP_LENGTH(const double * timeStepLength,
	const int * meshId,
	const int * elementId);

__declspec( dllimport ) int DEFORM_USRE1_GET(
	const int * length,
	double * values	,
	const int * meshId,
	const int * elementId);

__declspec( dllimport ) int DEFORM_YIELD_STRESS_GET(double * yieldStress,
	const int * meshId,
	const int * elementId);

__declspec( dllimport ) int DEFORM_NODES_SEND(
	const int * nodesNumber,
	const double * coordinates,
	const double * time,
	const int * meshId,
	const int * elementId
	);

__declspec( dllexport ) int DEFORM_USRE1_SEND(
	const int * length,
	const double * values,
	const int * meshId,
	const int * elementId
	);

__declspec( dllexport ) int DEFORM_YIELD_STRESSDSTRAIN_GET(double * yieldStressDstrain,
	const int * meshId,
	const int * elementId);

__declspec( dllexport )  int DEFORM_YIELD_STRESSDSTRAINRATE_GET(double * yieldStressDstrainRate,
	const int * meshId,
	const int * elementId);

__declspec( dllexport )  int DEFORM_TEMPERATURE(const double * temperature,
	const int * meshId,
	const int * elementId);

__declspec( dllexport )  int DEFORM_EFF_STR_RATE(const double * effStrainRate,
	const int * meshId,
	const int * elementId);

__declspec( dllexport )   int DEFORM_EFF_STR_RATE(const double * effStrainRate,
	const int * meshId,
	const int * elementId);

__declspec( dllexport ) int DEFORM_MESH_SET(const double * nodalCoordinates,
  const double * nodalDisplacements,
  const double * currentTime,
  const double * currentTimeStep,
  const int * elementsNumber,
  const int * nodesNumber,
  const int * connectivityMatrix);

__declspec( dllexport ) int DEFORM_ELEM_SET(double const * currentTime,
  const int * meshId,
  const int * elementId);