
/*! \file usr_upd_lib.cpp **************************************************
 * \author		Piotr Maciol
 * \brief     Provides methods for deform usr_upd procedures
 * \details   	
*********************************************************************/

#include "deformHandler.h"

extern am3::software::deform::DeformHandlerUserProc* deformHandlerProc;
extern void CreateDeformHandler();

__declspec( dllexport )  int DEFORM_TIME_STEP_LENGTH(const double * timeStepLength,
	const int * meshId,
	const int * elementId)
{
	if (SecureDeformHandlerExistance()) return deformHandlerProc->SetStepLength(timeStepLength,*meshId, *elementId);
	else return -1;
}

// Setting user variables
extern "C" int DEFORM_IUSRVL_SEND(
	const int * length,
	const double * values,
	const int * meshId,
	const int * elementId
)
{
	if (SecureDeformHandlerExistance()) return deformHandlerProc->SetGlobalVariables(*length, values,*meshId, *elementId);
	else return -1;
}

// Setting user variables for element
__declspec( dllexport ) int DEFORM_USRE1_SEND(
//extern "C" int DEFORM_USRE1_SEND(
	const int * length,
	const double * values,
	const int * meshId,
	const int * elementId
)
{
	if (SecureDeformHandlerExistance()) return deformHandlerProc->SetElementalVariables(*length, values,*meshId, *elementId);
	else return -1;
}


__declspec( dllexport )  int DEFORM_USRE1_GET(
	const int * length,
	double * values	,
	const int * meshId,
	const int * elementId)
{
//  //DebugBreak();
//   if (*meshId > 1)
//   {
//     //DebugBreak();
//   }
	if (SecureDeformHandlerExistance()) return deformHandlerProc->GetElementalVariables(*length, values,*meshId, *elementId);
	else return -1;


// 	Zener(thisStepData.ZenerParam);
// 	ShearModulus(thisStepData.temperature,thisStepData.shearModulus);
// 	Diffusion(thisStepData.temperature,thisStepData.SelfDiffusion,thisStepData.GrainBoundaryDiffusion);
// 	Dislocation(thisStepData.temperature,thisStepData.effStrainRate,thisStepData.shearModulus,thisStepData.SelfDiffusion,thisStepData.DislDensityMobile,thisStepData.DislDensityImmobileInteriors,thisStepData.DislDensityImmobileWalls);
//  	Grains(thisStepData.temperature,thisStepData.ZenerParam,thisStepData.DislDensityImmobileInteriors,thisStepData.DislDensityImmobileWalls,thisStepData.DislDensityMobile,thisStepData.GrainElongation,thisStepData.RecrystalizedGrainSize,thisStepData.SubgrainSize,thisStepData.NumberOfRecrystalizedNuclei,thisStepData.VolumeFractionOfRecrystallizedGrains);
}