
/*! \file deformHandlerAprox.h **************************************************
* \author		Piotr Maciol
* \copyright 	GNU Public License.
* \brief       
* \details   	
*********************************************************************/
#ifndef deformHandlerAprox_h__
#define deformHandlerAprox_h__
/*	include *********************************************************/

#include "./deformHandler.h"
#include "../utilMesh/mesh.h"
#include "../models/description.h"

/*	using ***********************************************************/

namespace mtm = am3::math::mesh;
namespace mpr = am3::model::properties;

/*	extern **********************************************************/


/*	classes *********************************************************/
namespace am3
{
	namespace software
	{
		namespace deform
		{
			template<typename COORD>
			struct Dim;

			template<>
			struct Dim<mpr::Coordinates2D>
			{
				static const int dim = 2;
			};

			template<>
			struct Dim<mpr::Coordinates3D>
			{
				static const int dim = 3;
			};

			template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL = NullIV>
			class DeformHandlerApprox: public DeformHandlerMesh<T_COORDINATE, T_INTERNAL_VARIABLE_MODEL>
			{
			public:
				typedef DeformHandlerMesh<T_COORDINATE, T_INTERNAL_VARIABLE_MODEL> DeformHdr;
        typedef boost::shared_ptr<DeformStressContract2D> OutputPtr;
				typedef boost::shared_ptr<mtm::Mesh<Dim<T_COORDINATE>::dim> > AppMeshPtr;
				DeformHandlerApprox(OutputPtr yieldStressModel,AppMeshPtr approxMesh) : approxMesh_(approxMesh), DeformHdr::DeformHandlerMesh(yieldStressModel){};
				DeformHandlerApprox(OutputPtr yieldStressModel,T_INTERNAL_VARIABLE_MODEL* ivModel,AppMeshPtr approxMesh) : approxMesh_(approxMesh), DeformHdr::DeformHandlerMesh(yieldStressModel,ivModel){};
				DeformHandlerApprox(boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel,
					boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressStrainModel,
					boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressStrainRateModel,
					AppMeshPtr approxMesh) : approxMesh_(approxMesh), DeformHdr::DeformHandlerMesh(yieldStressModel,yieldStressStrainModel,yieldStressStrainRateModel){};
				DeformHandlerApprox(boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel,
					boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressStrainModel,
					boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressStrainRateModel,
					T_INTERNAL_VARIABLE_MODEL* ivModel,
					AppMeshPtr approxMesh) : approxMesh_(approxMesh), DeformHdr::DeformHandlerMesh(yieldStressModel,yieldStressStrainModel,yieldStressStrainRateModel,ivModel){};
		
				virtual int CallSetElementalNodes(const int length,const double* values, const double time, const int meshId, const int elementId)
				{
					int toRet = DeformHandlerMesh<T_COORDINATE, T_INTERNAL_VARIABLE_MODEL>::CallSetElementalNodes(length,values,time,meshId,elementId);
          unsigned int i;
					BOOST_FOREACH(boost::shared_ptr<NamedPoint> vtx,nodalPoints_)
					{
            bool exist = false;
            for (i=0;i<approxMesh_->vertices_.size();i++)
            {
              if (approxMesh_->GetVertexId(i) == vtx->GetId() ) exist = true;
            }

            if (!exist)
            {
              i = approxMesh_->AddVertex(*vtx);
              nodesNumbers_.insert(std::pair<unsigned int, unsigned int>(vtx->GetId(),i));
            }
					}
					AddElement<T_COORDINATE>(elementId);
					return toRet;
				}

        virtual int CallSetMesh(const double * nodalCoordinates, const double * nodalDisplacements, const double currentTime, const double currentTimeStep, const unsigned int elementsNumber, const unsigned int nodesNumber, const int * connectivityMatrix)
        {
          int toRet;
          toRet = DeformHandlerMesh<T_COORDINATE, T_INTERNAL_VARIABLE_MODEL>::CallSetMesh(nodalCoordinates, nodalDisplacements, currentTime, currentTimeStep, elementsNumber, nodesNumber, connectivityMatrix);
          this->AddPointsToApproximation();
          return toRet;
        }
			protected:
        void AddPointsToApproximation()
        {
          
          std::map<unsigned int,std::vector<unsigned int> >::iterator conMatIt = conMat_.begin();

          for (unsigned long i=0;i< nodesNumber_;i++)
          {
            approxMesh_->AddVertex(NamedPoint(i));
          }
          while (conMatIt != conMat_.end())
          {
            approxMesh_->AddTetragon(conMatIt->second.at(0), conMatIt->second.at(1), conMatIt->second.at(2), conMatIt->second.at(3), conMatIt->first);
            conMatIt++;
          }
          delete approxMesh_->get_searcher();  //delete old searcher
          approxMesh_->set_searcher(new mtm::BintreeMeshSearcher<2>(approxMesh_.get())); //create new searcher
          approxMesh_->Organize();
        }
        std::map<unsigned int,unsigned int> nodesNumbers_;
				boost::shared_ptr<mtm::Mesh<Dim<T_COORDINATE>::dim> > approxMesh_;
				template<typename COORD>
				int AddElement(const int elId);

				template<>
				int AddElement<mpr::Coordinates2D>(const int elId)
        {
          int id1 = nodesNumbers_.find(nodalPoints_[0]->GetId())->second;
          int id2 = nodesNumbers_.find(nodalPoints_[1]->GetId())->second;
          int id3 = nodesNumbers_.find(nodalPoints_[2]->GetId())->second;
          int id4 = nodesNumbers_.find(nodalPoints_[3]->GetId())->second;
					return approxMesh_->AddTetragon(id1,id2,id3,id4,elId);
				};
			};
		}
	}
}

#endif // deformHandlerAprox_h__


