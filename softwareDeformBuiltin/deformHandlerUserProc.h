/*! \file deformHandlerUserProc.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef deformHandlerUserProc_h__
#define deformHandlerUserProc_h__
/*	include *********************************************************/
/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace deform
    {
      // Theses classes are used to separate Deform's user supplied procedures from all am3

      class DeformHandlerFacadeTime
      {
      public:
        int SetStepLength(const double* timeStepLength, const int meshId, const int elementId){return CallSetStepLength(timeStepLength,meshId,elementId);};
        int SetTime(const double* time, const int meshId, const int elementId){return CallSetTime(time,meshId,elementId);};
      protected:
        virtual int CallSetStepLength(const double* timeStepLength, const int meshId, const int elementId) = 0;
        virtual int CallSetTime(const double* time, const int meshId, const int elementId) = 0;
      };

      class DeformHandlerFacadeElm
      {
      public:
        int SetElementalNodes( const int length,const double* values, const double time, const int meshId, const int elementId ) {return CallSetElementalNodes(length,values,time, meshId,elementId);};
      protected:
        virtual int CallSetElementalNodes( const int length,const double* values, const double time, const int meshId, const int elementId ) = 0;
      };


      class DeformHandlerFacadeMtr
      {
      public:
        double GetYieldStress(const int meshId, const int elementId) const{return CallGetYieldStress(meshId,elementId);};
        double GetYieldStressDStrain(const int meshId, const int elementId) const {return CallGetYieldStressDStrain(meshId,elementId);};
        double GetYieldStressDStrainRate(const int meshId, const int elementId) const{return CallGetYieldStressDStrainRate(meshId,elementId);};
//        int SetElementalNodes( const int length,const double* values, const double time, const int meshId, const int elementId ) {return CallSetElementalNodes(length,values,time, meshId,elementId);};
        int SetTemperature(const double* temperature, const int meshId, const int elementId){return CallSetTemperature(temperature, meshId,elementId);};
        int SetEffectiveStrainRate(const double* effStrainRate, const int meshId, const int elementId){return CallSetEffectiveStrainRate(effStrainRate, meshId,elementId);};
      protected:
//        virtual int CallSetElementalNodes( const int length,const double* values, const double time, const int meshId, const int elementId ) = 0;
        virtual int CallSetTemperature(const double* temperature, const int meshId, const int elementId) = 0;
        virtual int CallSetEffectiveStrainRate(const double* effStrainRate, const int meshId, const int elementId) = 0;
        virtual double CallGetYieldStress(const int meshId, const int elementId) const = 0;
        virtual double CallGetYieldStressDStrain(const int meshId, const int elementId) const = 0;
        virtual double CallGetYieldStressDStrainRate(const int meshId, const int elementId) const = 0;
      };

      class DeformHandlerFacadeUpd
      {
      public:
        virtual int SetElementalVariables(const int length, const double* values, const int meshId, const int elementId){ return CallSetElementalVariables(length, values, meshId, elementId); };
        virtual int GetElementalVariables(const int length, double* values, const int meshId, const int elementId) { return CallGetElementalVariables(length, values, meshId, elementId); };
        virtual double GetElementalVariables(const  int whichVariable) const{return CallGetElementalVariables(whichVariable);};
        virtual int SetGlobalVariables(const int length, const double* values, const int meshId, const int elementId){ return CallSetGlobalVariables(length, values, meshId, elementId); };
//        int SetStepLength(const double* timeStepLength, const int meshId, const int elementId){return CallSetStepLength(timeStepLength,meshId,elementId);};
      protected:
        virtual int CallSetElementalVariables(const int length,const double* values, const int meshId, const int elementId) = 0;
        virtual int CallGetElementalVariables(const int length,double* values, const int meshId, const int elementId)  = 0;
        virtual int CallSetGlobalVariables(const int length,const double* values, const int meshId, const int elementId) = 0;
//        virtual int CallSetStepLength(const double* timeStepLength, const int meshId, const int elementId) = 0;
        virtual double CallGetElementalVariables(const unsigned int whichVariable) const = 0;
      };

      class DeformHandlerFacadeMsh
      {
      public:
        int SetMesh(const double * nodalCoordinates, const double * nodalDisplacements, const double currentTime, const double currentTimeStep, const unsigned int elementsNumber, const unsigned int nodesNumber, const int * connectivityMatrix) {return CallSetMesh(nodalCoordinates, nodalDisplacements, currentTime, currentTimeStep, elementsNumber, nodesNumber, connectivityMatrix);}
        int SetElement( const double time, const int meshId, const int elementId )        {return CallSetElement(time, meshId, elementId );}
      protected:
        virtual int CallSetMesh(const double * nodalCoordinates, const double * nodalDisplacements, const double currentTime, const double currentTimeStep, const unsigned  int elementsNumber, const unsigned int nodesNumber, const int * connectivityMatrix) = 0;
        virtual int CallSetElement( const double time, const int meshId, const int elementId ) = 0;
      };

      class DeformHandlerFacadeCrp
      {
      public:
        double GetCreepRate( const double effectiveStress, const double temperature, const double effectiveStrain, const int meshId, const int elementId )  {return CallGetGetCreepRate(effectiveStress, temperature, effectiveStrain, meshId, elementId );};
        double GetCreepRateDStrain( const double effectiveStress, const double temperature, const double effectiveStrain, const int meshId, const int elementId ) {return CallGetGetCreepRateDStrain(effectiveStress, temperature, effectiveStrain, meshId, elementId );};
      protected:
        virtual double CallGetGetCreepRate( const double effectiveStress, const double temperature, const double effectiveStrain, const int meshId, const int elementId ) const = 0;
        virtual double CallGetGetCreepRateDStrain( const double effectiveStress, const double temperature, const double effectiveStrain, const int meshId, const int elementId ) const = 0;
      };
      
      // "Superclass" with interfaces for Deform; obsolete, classes dedicated for each user file in Deform should be used instead
      class DeformHandlerUserProc
        : public DeformHandlerFacadeMtr
        , public DeformHandlerFacadeElm
        , public DeformHandlerFacadeUpd
        , public DeformHandlerFacadeTime
      {};

      class DeformHandlerUserProcMesh
        : public DeformHandlerFacadeMtr
        , public DeformHandlerFacadeMsh
        , public DeformHandlerFacadeUpd
        , public DeformHandlerFacadeTime
      {};
    }
  }
}
#endif // deformHandlerUserProc_h__