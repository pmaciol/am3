
/*! \file observer.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef observer_h__
#define observer_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>


/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace software
	{
		namespace deform
		{

			template<typename HANDLER>
			class HandlerObserver
			{
			public:
				HandlerObserver():newData_(false){};
				void NewData(){newData_=true;};
				void DataStored(){newData_=false;};
				bool HaveNewData(){return newData_;};
				void SetHandler(HANDLER* handler){handler_=handler;};
			protected:
				/*HANDLER* handler_;*/
				HANDLER* StoreDataFrom()
				{
					DataStored();
					return handler_;
				}
			private:
				HANDLER* handler_;
				bool newData_;
			};
//////////////////////////////////////////////////////////////////////////
// Check efficiency of virtual call 
			template<typename MODEL>
			class ObservableHandler 
			{
			public:
				void RegisterObserver(boost::shared_ptr<HandlerObserver<MODEL> > observer);
				MODEL* model;
			protected:
				void SendNewDataFlag();
				std::vector<boost::shared_ptr< HandlerObserver<MODEL> > > observers_;
				//void SetHandlerToObserver(boost::shared_ptr<DeformHandlerObserver<T_COORDINATE> > observer);
			};

			template<typename MODEL>
			void ObservableHandler<MODEL>::SendNewDataFlag()
			{
				BOOST_FOREACH( boost::shared_ptr<HandlerObserver<MODEL> > observer, observers_ )
				{
					observer->NewData();				  
				}
			}


			template<typename MODEL>
			void ObservableHandler<MODEL>::RegisterObserver( boost::shared_ptr<HandlerObserver<MODEL> > observer )
			{
				observers_.push_back(observer);
				model->SetHandlerToObserver(observer);
				//this->SetHandlerToObserver(observer);
			}

		}
	}
}
#endif // observer_h__


