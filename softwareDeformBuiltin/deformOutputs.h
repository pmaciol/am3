
/*! \file deformOutputs.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef deformOutputs_h__
#define deformOutputs_h__
/*	include *********************************************************/

#include "../models/macrosSimplifiedModels.h"
#include "../modelCreep/contracts.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace ppc = am3::problem::creep;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace software
  {
    namespace deform
    {
      // Defines data to be computed by Deform users supplied procedures
      CONTRACT(		DeformStressContract2D				,3,mpr::Coordinates2D, mpr::YieldStress, mpr::StressDStrain, mpr::StressDStrainRate);
      CONTRACT(		DeformStressDerivativesContract2D				,2,mpr::Coordinates2D, mpr::StressDStrain, mpr::StressDStrainRate);
      CONTRACT(		DeformCreepContract2D				  ,2,mpr::Coordinates2D, ppc::CreepRate, ppc::CreepRateDStrain);
    }
  }
}

#endif // deformOutputs_h__
