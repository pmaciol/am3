
/*! \file deformHandlerImpl.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef deformHandlerImpl_h__
#define deformHandlerImpl_h__
/*	include *********************************************************/
/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/



namespace am3
{
  namespace software
  {
    namespace deform
    {
      template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
      mpr::EffectiveStrainRate am3::software::deform::DeformHandlerStressComponent<T_COORDINATE, T_MESH_REPRESENTATION>::GetEffectiveStrainRateIn( const mpt::Point<T_COORDINATE>& point ) const
      {
        return effStrainRate_;
      }

      template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
      mpr::Temperature am3::software::deform::DeformHandlerStressComponent<T_COORDINATE, T_MESH_REPRESENTATION>::GetTemperatureIn( const mpt::Point<T_COORDINATE>& point ) const
      {
        return temperature_;
      }

      template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
      am3::software::deform::DeformHandlerStressComponent<T_COORDINATE, T_MESH_REPRESENTATION>::DeformHandlerStressComponent( boost::shared_ptr<moc::Provider<mpr::YieldStress,T_COORDINATE> > yieldStressModel, boost::shared_ptr<moc::Provider<mpr::StressDStrain,T_COORDINATE> > yieldStressStrainModel, boost::shared_ptr<moc::Provider<mpr::StressDStrainRate,T_COORDINATE> > yieldStressStrainRateModel , const T_MESH_REPRESENTATION*  mesh) 
        :yieldStressModel_(yieldStressModel), yieldStressDStrainModel_(yieldStressStrainModel), yieldStressDStrainRateModel_(yieldStressStrainRateModel), mesh_(mesh)
      {     }

      template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
      am3::software::deform::DeformHandlerStressComponent<T_COORDINATE, T_MESH_REPRESENTATION>::DeformHandlerStressComponent( boost::shared_ptr<ExternalSoftwareInputs> allVariablesModel , const T_MESH_REPRESENTATION* mesh)
        :yieldStressModel_(allVariablesModel), yieldStressDStrainModel_(allVariablesModel), yieldStressDStrainRateModel_(allVariablesModel), mesh_(mesh)
      {
      }


      template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
      double am3::software::deform::DeformHandlerStressComponent<T_COORDINATE, T_MESH_REPRESENTATION>::CallGetYieldStressDStrainRate( const int meshId, const int elementId ) const
      {
        const unsigned long meshIdToCheck=mesh_->GetMeshId();
        const unsigned long elementIdToCheck=mesh_->GetElementId(mesh_->GetMidPoint()->GetId());
        if (meshIdToCheck==meshId && elementIdToCheck==elementId)
        {

          return (yieldStressDStrainRateModel_->GetState(mesh_->GetMidPoint().get())).Value();
        } 
        else
        {
          return std::numeric_limits<double>::quiet_NaN();
        }
      }

      template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
      double am3::software::deform::DeformHandlerStressComponent<T_COORDINATE, T_MESH_REPRESENTATION>::CallGetYieldStressDStrain( const int meshId, const int elementId ) const
      {
        const unsigned long meshIdToCheck=mesh_->GetMeshId();
        const unsigned long elementIdToCheck=mesh_->GetElementId(mesh_->GetMidPoint()->GetId());
        if (meshIdToCheck==meshId && elementIdToCheck==elementId)
        {

          return (yieldStressDStrainModel_->GetState(mesh_->GetMidPoint().get())).Value();
        } 
        else
        {
          return std::numeric_limits<double>::quiet_NaN();
        }
      }

      template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
      double am3::software::deform::DeformHandlerStressComponent<T_COORDINATE, T_MESH_REPRESENTATION>::CallGetYieldStress( const int meshId, const int elementId ) const
      {
        const unsigned long meshIdToCheck=mesh_->GetMeshId();
        const unsigned long elementIdToCheck=mesh_->GetElementId(mesh_->GetMidPoint()->GetId());
        if (meshIdToCheck==meshId && elementIdToCheck==elementId)
        {

          return (yieldStressModel_->GetState(mesh_->GetMidPoint().get())).Value();
        } 
        else
        {
          return std::numeric_limits<double>::quiet_NaN();
        }
      }

      template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
      int am3::software::deform::DeformHandlerStressComponent<T_COORDINATE, T_MESH_REPRESENTATION>::CallSetEffectiveStrainRate( const double* effStrainRate, const int meshId, const int elementId )
      {
        effStrainRate_(*effStrainRate);
        return 0;
      }

      template<typename T_COORDINATE, typename T_MESH_REPRESENTATION>
      int am3::software::deform::DeformHandlerStressComponent<T_COORDINATE, T_MESH_REPRESENTATION>::CallSetTemperature( const double* temperature, const int meshId, const int elementId )
      {
        temperature_(*temperature);
        return 0;
      }

      template<typename T_COORDINATE>
      mpr::StepLength am3::software::deform::DeformHandlerTimeComponent<T_COORDINATE>::GetStepLength() const
      {
        return timeStepLength_;
      }

      template<typename T_COORDINATE>
      mpr::Time am3::software::deform::DeformHandlerTimeComponent<T_COORDINATE>::GetTime() const
      {
        return time_;
      }

      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL, typename T_MESH_REPRESENTATION>
      double am3::software::deform::DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, T_MESH_REPRESENTATION>::CallGetGlobalVariables( const unsigned int whichVariable ) const
      {
        if (whichVariable<lengthGlobal_)
        {
          return globalVariables_[whichVariable];
        } 
        else
        {
          return std::numeric_limits<double>::quiet_NaN();
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Asked for variable without a range");
        }
      }
      template<typename T_COORDINATE>
      mpr::ElementNodesNumber DeformHandlerElement<T_COORDINATE>::GetNodesNumber() const
      {
        mpr::ElementNodesNumber nodes=this->nodalPoints_.size();
        return nodes;
      }
      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL, typename T_MESH_REPRESENTATION>
      int am3::software::deform::DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, T_MESH_REPRESENTATION>::CallGetElementalVariablesNumber() const
      {
        return lengthElemental_;
      }
      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL, typename T_MESH_REPRESENTATION>
      int am3::software::deform::DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, T_MESH_REPRESENTATION>::CallSetGlobalVariables( const int length,const double* values , const int meshId, const int elementId )
      {
        bool isSuccess=false;
        if (lengthGlobal_==length)
        {
          globalVariables_.clear();
          globalVariables_.insert(globalVariables_.end(), values, values+length);
          isSuccess=true;
        }
        else 
        {
          //log ERR
        }
        return isSuccess;
      }
// 
      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL, typename T_MESH_REPRESENTATION>
      double am3::software::deform::DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, T_MESH_REPRESENTATION>::CallGetElementalVariables( const unsigned int whichVariable ) const
      {
        if (whichVariable<lengthElemental_)
        {
          return elementalVariables_[whichVariable];
        } 
        else
        {
          return std::numeric_limits<double>::quiet_NaN();
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"Asked for variable without a range");
        }
      }

      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL, typename T_MESH_REPRESENTATION>
      int am3::software::deform::DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, T_MESH_REPRESENTATION>::CallGetElementalVariables(const int length,double* values, const int meshId, const int elementId)
      {
        //std::copy(internalVariableModel_->GetInternalVariables().begin(),internalVariableModel_->GetInternalVariables().end(),elementalVariables_.begin());
        elementalVariables_ = internalVariableModel_->GetInternalVariables(mesh_->GetMidPoint().get());
        if (length==elementalVariables_.size())
        {
          std::copy( elementalVariables_.begin(), elementalVariables_.end(), values );
          return true;
        }
        else
        {
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL,"User variables vector length is wrong");
          return false;
        }
      }

      template<typename T_COORDINATE>
      int am3::software::deform::DeformHandlerTimeComponent<T_COORDINATE>::CallSetStepLength( const double* timeStepLength, const int meshId, const int elementId )
      {
        //SendNewDataFlag();
        timeStepLength_(*timeStepLength);
        return 0;
      }

      template<typename T_COORDINATE>
      int am3::software::deform::DeformHandlerTimeComponent<T_COORDINATE>::CallSetTime( const double* time, const int meshId, const int elementId )
      {
        //SendNewDataFlag();
        time_(*time);
        return 0;
      }

      template<typename T_COORDINATE, typename T_INTERNAL_VARIABLE_MODEL, typename T_MESH_REPRESENTATION>
      int am3::software::deform::DeformHandlerIVComponent<T_COORDINATE,T_INTERNAL_VARIABLE_MODEL, T_MESH_REPRESENTATION>::CallSetElementalVariables( const int length,const double* values, const int meshId, const int elementId )
      {
        bool isSuccess=false;
        lengthElemental_ = length;
        //SendNewDataFlag();
        elementalVariables_.clear();
        elementalVariables_.insert(elementalVariables_.end(), values, values+length);
        isSuccess=true;

        return isSuccess;
      }
    } //deform
  } //software
} //am3


#endif // deformHandlerImpl_h__
