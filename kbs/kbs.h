
/*! \file kbs.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef kbs_h__
#define kbs_h__
/*	include *********************************************************/

#include <typeinfo.h>
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "../common/typelists.h"
#include "../models/conversions.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace mco = am3::model::conversions;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace kbs
  {
    struct VariableValue // Compare to VariableDTO!
    {
      std::string name;
      std::string value;
    };

    template <typename VARIABLES_SOURCE>
    class Kbs
    {
    public:
		Kbs(boost::shared_ptr<VARIABLES_SOURCE> databus) : databus_(databus){};
		template <typename VARIABLE> mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point);
    protected:
		boost::shared_ptr<VARIABLES_SOURCE> databus_;
    };

	template <typename VARIABLES_SOURCE>
    class KbsString
		: public Kbs<VARIABLES_SOURCE>
    {
    public:
      KbsString(boost::shared_ptr<VARIABLES_SOURCE> databus) :Kbs<VARIABLES_SOURCE>(databus){};
//      virtual mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, std::string variableName){}
      template <typename VARIABLE> mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point);
      template <> mpr::ModelID GetModelId<mpr::ShearModulus>(mpt::Point<mpr::Coordinates2D> *point);
    protected:
      VariableValue Get(std::string variableName, mpt::Point<mpr::Coordinates2D> *point)
      {
        std::string toRetVal = databus_->GetVal(variableName, point);
        VariableValue toRet;
        toRet.name = variableName;
        toRet.value = toRetVal;
        return toRet;
      };
    };

    template <typename VARIABLES_SOURCE>
    class KbsStringByHand
      : public KbsString < VARIABLES_SOURCE >
    {
    public:
      KbsStringByHand(boost::shared_ptr<VARIABLES_SOURCE> databus) :KbsString<VARIABLES_SOURCE>(databus){};
      template <typename VARIABLE> mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point)
      {
        std::string variableName = mco::VariableName<VARIABLE>();
        std::cout << "KBS query: GetModelId for variable " << variableName << ";  point " << *point << std::endl;
        bool goNext = false;
        while (!goNext)
        {
          std::cout << "Do you need any additional variable values? [y/n] :" ;
          char read;
          std::cin >> read;
          if ('y' == read)
          {
            std::cout << "Which variable you need:";
            std::string variableName;
            std::cin >> variableName;
            VariableValue value;
            value = Get(variableName, point);
            std::cout << value.value<<std::endl;
          } 
          else if (read == 'n')
          {
            goNext = true;
          } 
        }
        std::cout << "Set ModelID (starting with 0) for " << variableName  << ": ";
        unsigned int modelId;
        std::cin >> modelId;
        mpr::ModelID toRet;
        toRet(modelId);
        return toRet;
      }


    };

    struct KbsStringInterface
    {
      virtual std::string GetVal(const std::string variableName, mpt::Point<mpr::Coordinates2D> * point) = 0;
    };

    template <typename List, typename DATABUS>
    struct DatabusRequesterInternal;

    template <template <typename G> class H, typename T, typename DATABUS>
    struct DatabusRequesterInternal<Typelist<H, T>, DATABUS >
      :public DatabusRequesterInternal<T,DATABUS>
    {
      void SetDatabus(boost::shared_ptr<DATABUS> databus)
      { 
        DatabusRequesterInternal<T, DATABUS>::SetDatabus(databus); 
      }
      virtual std::string GetVal(const std::string variableName, mpt::Point<mpr::Coordinates2D> * point)
      {
        std::string providedName = mco::VariableName<H>();
        if (variableName == providedName)
        {
          return mco::ToString(databus_->GetFromDatabus<H>(point));
          //return mco::ToString(databus_->GetFromDatabus<mpr::Temperature>(point));
        } 
        else
        {
          return DatabusRequesterInternal < T,DATABUS >::GetVal(variableName,point) ;
        }
      };
    };

    template <template <typename G> class H, typename DATABUS>
    struct DatabusRequesterInternal<Typelist<H, EmptyType> ,DATABUS>
      : public KbsStringInterface
    {
      void SetDatabus(boost::shared_ptr<DATABUS> databus)
      { 
        databus_ = databus; 
      }
      virtual std::string GetVal(const std::string variableName, mpt::Point<mpr::Coordinates2D> * point)
      {
        std::string providedName = mco::VariableName<H>();
        if (variableName == providedName)
        {
          return mco::ToString(databus_->GetFromDatabus<H>(point));
          //return mco::ToString(databus_->GetFromDatabus<mpr::Temperature>(point));
        }
        else
        {
          return "No this variable in Databus";
        }
      };
    protected:
      boost::shared_ptr<DATABUS> databus_;
    };



    template <typename DATABUS>
    class DatabusRequester
      : public DatabusRequesterInternal<typename DATABUS::AllOutputs, typename DATABUS>
    {
    public:
      void SetDatabus(boost::shared_ptr<DATABUS> databus){ DatabusRequesterInternal<typename DATABUS::AllOutputs, typename DATABUS>::SetDatabus(databus); }
    protected:
      //boost::shared_ptr<DATABUS> databus_;
    };

////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////

//     template <typename DATABUS>
//     void am3::kbs::Kbs::SetDatabus(boost::shared_ptr<DATABUS> databus)
//     {
//       databus_ = databus;
//     }

//     template <typename VARIABLE>
//     VariableValue am3::kbs::Kbs::GetValue(mpt::Point<mpr::Coordinates2D> *point, const VARIABLE* trait)
//     {
//       return ToString(databus_->GetState<VARIABLE>(point));
//     }

  } //kbs

} //am3
#endif // kbs_h__
