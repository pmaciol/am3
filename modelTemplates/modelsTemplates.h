
/*! \file modelsTemplates.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef modelsTemplates_h__
#define modelsTemplates_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/contractMacros.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace moc = am3::model::contract;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace templates
		{
      typedef moc::Contract<TYPELIST_TUPLE_1(EmptyType), mpr::Coordinates2D> EmptyContract2D;

			template<typename VARIABLE, typename CONTRACT>
      class ConstantValueModel
        : public CONTRACT
      {
      public:
        typedef VARIABLE Variable;
        typedef CONTRACT Contract;

        ConstantValueModel(const VARIABLE& valueToSet): variable_(valueToSet){};
        Variable GetState(mpt::Point<mpr::Coordinates2D> * point,const Variable *const )
        {
          return variable_;
        };
      protected:
        const Variable variable_;
      };
		} //templates
	} //model
} //am3
#endif // modelsTemplates_h__
