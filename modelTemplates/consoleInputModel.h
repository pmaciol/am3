
/*! \file byHandModel.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef byHandModel_h__
#define byHandModel_h__
/*	include *********************************************************/

#include <iostream>
#include <typeinfo>
#include "../common/typelists.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../models/conversions.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;
namespace mco = am3::model::conversions;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace trivial
		{

      template<typename H, typename T>
      struct ByHandModelContract;

     
      template<typename H, typename H2, typename T2>
      struct ByHandModelContract < H, Typelist<H2, T2> >
        :public ByHandModelContract< H2, T2>
      {
        void aaa(){};
        template<typename VAR> 
        VAR Get(mpt::Point<mpr::Coordinates2D> * point)
        {
          return ByHandModelContract< H2, T2>::Get<VAR>(point);
        }

        template<> 
        H Get<H>(mpt::Point<mpr::Coordinates2D> * point)
        {
          std::cout << "Value of " << typeid(H).name()<<": ";
          std::string value;
          std::cin >> value;
          H toRet; 
          toRet = mco::convertScalar<H>(value);
          return toRet; 
        };

      };

      template<typename H>
      class ByHandModelContract < H, EmptyType>
      {
      public:
        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point)
        {
          static_assert(false, "Getting here means there is no expected variable in the typelist");
          VAR toRet;
          return toRet;
        }

        template<>
        H Get<H>(mpt::Point<mpr::Coordinates2D> * point)
        {
          std::cout << "Give me variable " << typeid(H).name() << std::endl;
          std::string value;
          std::cin >> value;
          H toRet;
          toRet = mco::convertScalar<H>(value);
          return toRet;
        };
      };



      template </* class INPUTS_LIST, */class OUTPUTS_LIST>
      class ConsoleInputModel
        : public ByHandModelContract<typename OUTPUTS_LIST::List::head, typename OUTPUTS_LIST::List::tail>
      {};

		} //trivial
	} //model
} //am3
#endif // byHandModel_h__
