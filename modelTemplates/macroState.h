
/*! \file macroState.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3modelTemplates_macroState_h__
#define am3modelTemplates_macroState_h__
/*	include *********************************************************/

#include "../models/macrosSimplifiedModels.h"
#include "../models/dataStructures.h"
#include "../models/inputTypelists.h"
#include "../models/point.h"
#include "../modelTemplates/modelsTemplates.h"
#include "../models/models.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mo = am3::model;
namespace mpt = am3::model::point;
namespace mot = am3::model::templates;

/*	extern **********************************************************/
/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace templates
    {
      CONTRACT(MacroContractElNumber, 5, mpr::Coordinates2D, mpr::Temperature, mpr::EffectiveStrainRate, mpr::StepLength, mpr::Time, mpr::ElementNumber);
      CONTRACT(MacroContract, 4, mpr::Coordinates2D, mpr::Temperature, mpr::EffectiveStrainRate, mpr::StepLength, mpr::Time);
      CONTRACT(MacroTemperatureContract, 4, mpr::Coordinates2D, mpr::Temperature, mpr::StepLength, mpr::Time, mpr::ElementNumber);


      template<typename HANDLER, typename INPUT>
      class MacroModelState
        : public am3::model::MacroModel<HANDLER, MacroContractElNumber>
        , public mo::InputTypelist < INPUT >
      {
      protected:
        MacroModelState(boost::shared_ptr<HANDLER> handler) : am3::model::MacroModel<HANDLER, MacroContractElNumber>(handler){};
      public:
        typedef mot::EmptyContract2D Inputs;
        typedef typename MacroContractElNumber Contract;
        typedef mpr::Coordinates2D CoordinatesType;

        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> mpr::EffectiveStrainRate Get<mpr::EffectiveStrainRate>(mpt::Point<mpr::Coordinates2D> * point) 
        {
          if (pointerToMacroHandler_)
          {
            return pointerToMacroHandler_->GetEffectiveStrainRate();
          }
          else
          {
            lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Wrong pointerToMacroHandler_");
            assert(!"Wrong pointerToMacroHandler_");
            mpr::EffectiveStrainRate toRet;
            toRet(std::numeric_limits<double>::signaling_NaN());
            return toRet;
          }
       };
        template<> mpr::Temperature Get<mpr::Temperature>(mpt::Point<mpr::Coordinates2D> * point){ return pointerToMacroHandler_->GetTemperature(); };
        template<> mpr::StepLength Get<mpr::StepLength>(mpt::Point<mpr::Coordinates2D> * point){ return pointerToMacroHandler_->GetStepLength(); };
        template<> mpr::Time Get<mpr::Time>(mpt::Point<mpr::Coordinates2D> * point){ return pointerToMacroHandler_->GetTime(); };
        template<> mpr::ElementNumber Get<mpr::ElementNumber>(mpt::Point<mpr::Coordinates2D> * point){return pointerToMacroHandler_->GetElementId(); };

        mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::Temperature *const){ return Get<mpr::Temperature>(point); }
        mpr::EffectiveStrainRate GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::EffectiveStrainRate *const){ return Get<mpr::EffectiveStrainRate>(point); }
        mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StepLength *const){ return Get<mpr::StepLength>(point); }
        mpr::Time GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::Time *const){ return Get<mpr::Time>(point); }
        mpr::ElementNumber GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::ElementNumber *const){ return Get<mpr::ElementNumber>(point); }
      };

      //******************************************************************************************************************************************************************

      template<typename HANDLER, typename INPUT>
      class MacroModelTemperature
        : public am3::model::MacroModel<HANDLER, MacroTemperatureContract>
        , public mo::InputTypelist < INPUT >
      {
      protected:
        MacroModelTemperature(boost::shared_ptr<HANDLER> handler) : am3::model::MacroModel<HANDLER, MacroTemperatureContract>(handler){};
      public:
        typedef mot::EmptyContract2D Inputs;
        typedef typename MacroTemperatureContract Contract;
        typedef mpr::Coordinates2D CoordinatesType;

        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point);
        template<> mpr::Temperature Get<mpr::Temperature>(mpt::Point<mpr::Coordinates2D> * point)
        {
          if (pointerToMacroHandler_)
          {
            return pointerToMacroHandler_->GetTemperature();
          }
          else
          {
            lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Wrong pointerToMacroHandler_");
            assert(!"Wrong pointerToMacroHandler_");
            mpr::Temperature toRet;
            toRet(std::numeric_limits<double>::signaling_NaN());
            return toRet;
          }
        };
        template<> mpr::StepLength Get<mpr::StepLength>(mpt::Point<mpr::Coordinates2D> * point){ return pointerToMacroHandler_->GetStepLength(); };
        template<> mpr::Time Get<mpr::Time>(mpt::Point<mpr::Coordinates2D> * point){ return pointerToMacroHandler_->GetTime(); };
        template<> mpr::ElementNumber Get<mpr::ElementNumber>(mpt::Point<mpr::Coordinates2D> * point){ return pointerToMacroHandler_->GetElementNumber(); };

        mpr::Temperature GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::Temperature *const){ return Get<mpr::Temperature>(point); }
        mpr::StepLength GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::StepLength *const){ return Get<mpr::StepLength>(point); }
        mpr::Time GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::Time *const){ return Get<mpr::Time>(point); }
        mpr::ElementNumber GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::ElementNumber *const){ return Get<mpr::ElementNumber>(point); };
      };
    } // templates
  }// model
}//am3

#endif // am3modelTemplates_macroState_h__
