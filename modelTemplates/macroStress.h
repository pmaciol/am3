
/*! \file macroStress.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3modelTemplates_macroStress_h__
#define am3modelTemplates_macroStress_h__
/*	include *********************************************************/

#include "../models/macrosSimplifiedModels.h"
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "../models/inputTypelists.h"
#include "../models/predefinedContracts.h"
#include "innerSideInterfacesMacro.h"
#include "modelsTemplates.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mo = am3::model;
namespace mot = am3::model::templates;
namespace mpt = am3::model::point;
namespace mcd = am3::model::contract::defined;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace model
  {
    namespace templates
    {

      template<typename INPUT>
      class MacroStressFromDatabus_GivenPoint2D
        : public mot::OneStepOneElementMacroStressWithDerivatives
        , public mo::InputTypelist < INPUT >
      {
      public:
	using mo::InputTypelist < INPUT >::bus_;
        MacroStressFromDatabus_GivenPoint2D(mpt::Point<mpr::Coordinates2D>* point) : mockPoint_(point){};
        void ChangePoint(const mpt::Point<mpr::Coordinates2D>* point) { mockPoint_ = point; };

        virtual mpr::YieldStress GetStress() const                      {return bus_->template GetFromDatabus<mpr::YieldStress>(mockPoint_);}
        virtual mpr::StressDStrain GetStressDStrain() const             {return bus_->template GetFromDatabus<mpr::StressDStrain>(mockPoint_);}
        virtual mpr::StressDStrainRate GetStressDStrainRate() const     {return bus_->template GetFromDatabus<mpr::StressDStrainRate>(mockPoint_);}
      protected:
        mpt::Point<mpr::Coordinates2D>* mockPoint_;
      };

      //******************************************************************************************************************************************************************

      template<typename INPUT>
      class MacroStress
        : public mo::InputTypelist < INPUT >
        , public mcd::MacroStressContract2D
     {
     public:
       typedef mcd::MacroStressMockDstrainDstrainrateContract2D Inputs;
        typedef mcd::MacroStressContract2D Contract;
        typedef mpr::Coordinates2D CoordinatesType;

        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point)
	{
	  const VAR* trait = nullptr;
	  return Get(point,trait);
	}
	
     private:
        mpr::YieldStress Get(mpt::Point<mpr::Coordinates2D> * point, const mpr::YieldStress*) { return mo::InputTypelist < INPUT >::bus_->template GetFromDatabus<mpr::YieldStress>(point); };
        mpr::StressDStrain Get(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrain*) { return mo::InputTypelist < INPUT >::bus_->template GetFromDatabus<mpr::StressDStrain>(point); };
        mpr::StressDStrainRate Get(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrainRate*) { return mo::InputTypelist < INPUT >::bus_->template GetFromDatabus<mpr::StressDStrainRate>(point); };
      };

      template<typename INPUT>
      class MockStressDerivatives
        : public mo::InputTypelist < INPUT >
        //, public mcd::MacroStressMockDstrainDstrainrateContract2D
      {
      public:
        typedef EmptyContract2D Inputs;
        typedef mcd::MacroStressMockDstrainDstrainrateContract2D Contract;
        typedef mpr::Coordinates2D CoordinatesType;

        template<typename VAR>
        VAR Get(mpt::Point<mpr::Coordinates2D> * point)
	{
	  const VAR* trait = nullptr;
	  return Get(point,trait);
	}
	private:
                mpr::StressDStrain Get(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrain*) { mpr::StressDStrain toRet;  toRet(0);  return toRet; };
        mpr::StressDStrainRate Get(mpt::Point<mpr::Coordinates2D> * point, const mpr::StressDStrainRate*) { mpr::StressDStrainRate toRet;  toRet(0);  return toRet; };
	
        
      };
    } // templates
  }// model
}//am3
#endif // am3modelTemplates_macroStress_h__
