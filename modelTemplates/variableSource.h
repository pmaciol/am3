
/*! \file variableSource.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3modelTemplates_variableSource_h__
#define am3modelTemplates_variableSource_h__
/*	include *********************************************************/

#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../logger/logger.h"

/*	using ***********************************************************/

namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;
namespace lo = am3::logger;
/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace varSource
		{

			//The class is used to push some values to 'source' before starting dependent model. Usuallly AM3 models 'drags' input variables, but eg. in MPI implementations of external clients data are transfered BEFORE computations
      template<typename VAR_TYPE>
      class PushableSingleValueSource
      {
      public:
        VAR_TYPE GetVariable(const mpt::Point<mpr::Coordinates2D>& point) const;
        void SetVariable(const mpt::Point<mpr::Coordinates2D>& point, const VAR_TYPE& variableValue);
        bool AnyAdditionalVariables() const;
        std::vector<double> GetAdditionalVariables(const mpt::Point<mpr::Coordinates2D>& point);
        void SetAdditionalVariables(const mpt::Point<mpr::Coordinates2D>& point, const std::vector<double>& variables);
        protected:
          boost::shared_ptr<mpt::Point<mpr::Coordinates2D> > point_;
          VAR_TYPE variableValue_;

      };

      class PushableTempPlusVectorValueSource
        : public PushableSingleValueSource<mpr::Temperature>
      {
      public:
        bool AnyAdditionalVariables() const;
        std::vector<double> GetAdditionalVariables(const mpt::Point<mpr::Coordinates2D>& point);
        void SetAdditionalVariables(const mpt::Point<mpr::Coordinates2D>& point, const std::vector<double>& variables);
      protected:
        std::vector<double> actualVariables_;
      };

      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////



      template<typename VAR_TYPE>
      void am3::model::varSource::PushableSingleValueSource<VAR_TYPE>::SetAdditionalVariables(const mpt::Point<mpr::Coordinates2D>& point, const std::vector<double>& variables)
      {
        std::cout << "aaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        throw std::logic_error("This version of PushableSingleValueSource does not contain any additional variables");
      }

      template<typename VAR_TYPE>
      std::vector<double> am3::model::varSource::PushableSingleValueSource<VAR_TYPE>::GetAdditionalVariables(const mpt::Point<mpr::Coordinates2D>& point)
      {
        std::cout << "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb";
        throw std::logic_error("This version of PushableSingleValueSource does not contain any additional variables");
        std::vector<double> temp;
        return temp;
      }

      template<typename VAR_TYPE>
      bool am3::model::varSource::PushableSingleValueSource<VAR_TYPE>::AnyAdditionalVariables() const
      {
        return false;
      }

      template<typename VAR_TYPE>
      void am3::model::varSource::PushableSingleValueSource<VAR_TYPE>::SetVariable(const mpt::Point<mpr::Coordinates2D>& point, const VAR_TYPE& variableValue)
      {
        point_ = point.Snapshot();
        variableValue_ = variableValue;
      }

      template<typename VAR_TYPE>
      VAR_TYPE PushableSingleValueSource<VAR_TYPE>::GetVariable(const mpt::Point<mpr::Coordinates2D>& point) const
      {
        if (*point_ == point)
        {
          return variableValue_;
        }
        else
        {
          VAR_TYPE toRet;
          lo::log<lo::SEV_ERROR>(lo::LOG_MODEL, "Variable source asked for unknown point.");
          return toRet;
        }
      }


      ////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////


		} //varSource
	} //model
} //am3
#endif // am3modelTemplates_variableSource_h__
