
/*! \file innerSideInterfacesMacro.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief Inner side interfaces are abstract classes defining behaviour of "plugins" to external software
 * \details   	
*********************************************************************/
#ifndef am3models_innerSideInterfacesMacro_h__
#define am3models_innerSideInterfacesMacro_h__
/*	include *********************************************************/

#include "../models/dataStructures.h"
#include "../models/point.h"

/*	using ***********************************************************/

namespace mpr = am3::model::properties;
namespace mpt = am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace model
	{
		namespace templates
		{
      class OneStepOneElement
      {
      public:
        // 'Organizational' variables - which element and what time
        virtual mpr::ElementNumber GetElementNumber() const = 0;
      };


      class OneStepOneElement2D
        : public OneStepOneElement
      {
      public:
        virtual std::vector<mpt::Point<mpr::Coordinates2D>*> GetNodes() const = 0;
      };


      class OneStepOneElement3D
        : public OneStepOneElement
      {
      public:
        virtual std::vector<mpt::Point<mpr::Coordinates3D>*> GetNodes() const = 0;
      };


      class OneStepOneElementMacroState
      {
      public:
        virtual mpr::StepLength GetStepLength() const = 0;
        virtual mpr::Time GetTime() const = 0;

        // Material state independent of dimensions
        virtual mpr::Temperature GetTemperature() const = 0;
        virtual mpr::TotalEffectiveStrain GetEffectiveStrain() const = 0;
        virtual mpr::EffectiveStrainRate GetEffectiveStrainRate() const = 0;
      };


      class OneStepOneElementMacroState2D
        : public OneStepOneElementMacroState
      {
      public:
        virtual mpr::Strain2D GetStrain() const = 0;
        virtual mpr::StrainRate2D GetStrainRate() const = 0;
      };
			
      //******************************************************************************************************************************************************************

      class OneStepOneElementMacroStressWithDerivatives
      {
      public:
        virtual mpr::YieldStress GetStress() const = 0;
        virtual mpr::StressDStrain GetStressDStrain() const = 0;
        virtual mpr::StressDStrainRate GetStressDStrainRate() const = 0;
      };

      //******************************************************************************************************************************************************************

      class OneStepOneElementIvPresent
      {
      public:
        virtual std::vector<double> GetInternalVariables() const = 0;
        virtual double GetInternalVariable(std::size_t index) const = 0;
        virtual mpr::Temperature GetTemperature() const = 0;
        virtual mpr::Time GetTime() const = 0;
        virtual mpr::StepLength GetStepLength() const = 0;
        virtual mpt::Point<mpr::Coordinates2D>* GetCentralElementPoint() = 0;
        virtual mpr::ElementNumber GetElementNumber() = 0;
      };

      class OneStepOneElementIvNew
      {
      public:
        virtual std::vector<double> GetInternalVariablesNew(mpt::Point<mpr::Coordinates2D> * point) = 0;
      };

      //******************************************************************************************************************************************************************

      class OneStepOneMeshIvPresent
      {
      public:
        virtual ~OneStepOneMeshIvPresent(){};

        virtual double GetInternalVariable(const unsigned elemId, std::size_t index) const = 0;
        virtual unsigned NumberOfElements() const = 0;
        virtual unsigned NumberOfIvs() const = 0;
      
      protected:
        OneStepOneMeshIvPresent(){};
      };

      class OneStepOneMeshTemperatureElem
      {
      public:
        virtual ~OneStepOneMeshTemperatureElem(){}

        virtual mpr::Temperature GetOldTemperature(const unsigned elementId) const = 0;
        virtual mpr::Temperature GetNewTemperature(const unsigned elementId) const = 0;
      
      protected:
        OneStepOneMeshTemperatureElem(){}
      };

      class OneStepOneMeshTemperatureInNodes
      {
      public:
        virtual ~OneStepOneMeshTemperatureInNodes(){}

        virtual mpr::Temperature GetOldTemperature(const unsigned nodeId) const = 0;
        virtual mpr::Temperature GetNewTemperature(const unsigned nodeId) const = 0;
        virtual unsigned GetNodeStart() const = 0;
        virtual unsigned GetNodeLast() const = 0;
        virtual unsigned GetNodesNumber() const = 0;

      protected:
        OneStepOneMeshTemperatureInNodes(){}
      };

      class OneStepOneMeshTime
      {
      public:
        virtual ~OneStepOneMeshTime(){};

        virtual mpr::StepLength GetStepLength() const = 0;
        virtual mpr::Time GetTime() const = 0;
        virtual mpr::Time GetNewTime() const = 0;

      protected:
        OneStepOneMeshTime(){};
      };

		} //contract
	} //model
} //am3
#endif // am3models_innerSideInterfacesMacro_h__
