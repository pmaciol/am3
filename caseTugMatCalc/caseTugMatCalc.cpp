
/*! \file caseTugMatCalc.cpp **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/

/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../softwareDeform/setDeformHandler.h"
#include "caseTugMatCalc.h"
#include "modelsAdapters.h"
#include "main.h"

/*	using ***********************************************************/

namespace sdm = am3::software::deform;
namespace ctm = am3::cases::tugmc;

/*	extern **********************************************************/

extern am3::cases::tugmc::Main*  mainInstance;

sdm::DeformHandlerFacadeMshTemperature* am3::cases::tugmc::GetDeformHandlerFacadeMshTemperatureTime()
{
  if (mainInstance == nullptr) mainInstance = new am3::cases::tugmc::Main;
  return mainInstance->GetDeformCasePrecomp();
}

/*	classes *********************************************************/


sdm::DeformHandlerFacadeUpd* am3::cases::tugmc::GetDeformHandlerFacadeUpdate()
{
  if (mainInstance == nullptr) mainInstance = new am3::cases::tugmc::Main;
  return mainInstance->GetDeformCaseIV();
}

void am3::cases::tugmc::DeformCaseStepPrecomputations::SetTemperaturesInNodes(const int materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const int nodeStart, const int nodeEnd)
{
  sdm::OneStepOneMeshTemperatureTime::SetTemperaturesInNodes(materialId, time, timeStep, temperatures, deltaTemperatures, nodeStart, nodeEnd);

  unsigned presentElement = 1;
  mpr::Time timeProperty; timeProperty(time);
  for (const auto element : connectivityMatrix_)
  {
		std::map<int, int>::iterator it = predefinedModels_.find(presentElement);
		if ((it != predefinedModels_.end()) && (it->second == 1))
		{
			std::cout << "Element " << presentElement << ": MatCalc model ran" << std::endl;
			mpt::ComputedPoint<mpr::Coordinates2D> localPoint(&(cont.MakePoint(presentElement, timeProperty)));
			double elementMeanTemperature = 0;
			for (unsigned tempIndex = 0; tempIndex < 4; tempIndex++)
			{
				std::cout << "NodalTemperature " << connectivityMatrix_.at(presentElement - 1).at(tempIndex) << " " << temperatures[(connectivityMatrix_.at(presentElement - 1).at(tempIndex)) - 1] << std::endl;
				elementMeanTemperature += temperatures[(connectivityMatrix_.at(presentElement - 1).at(tempIndex)) - 1];
			}
			elementMeanTemperature /= 4;

			mpr::Temperature temperature = elementMeanTemperature;
			mda::Coord2dTimeTempVect val(localPoint.Coordinates(), localPoint.GetTime(), temperature, userVariablesVector_.at(presentElement - 1));
			std::pair<mpt::ComputedPoint<mpr::Coordinates2D>, mda::Coord2dTimeTempVect  > pair(localPoint, val);
			std::cout << "11th IV is " << std::get<3>(pair.second).at(11) << std::endl;
			cont.Run(pair);
		}
		else
		{
			std::cout << "Element " << presentElement << ": no precomputations"<< std::endl;
		}
    ++presentElement;
  }

}

void am3::cases::tugmc::DeformCaseStepPrecomputations::SetConnectivityMatrix(const int meshId, const double time, const int maxElemement, const int * connectivityMatrix)
{
  const int* conMatrixPointer = connectivityMatrix;
  connectivityMatrix_.clear();
  for (int i = 0; i < maxElemement; i++)
  {
    nodeStruct next = { { *conMatrixPointer, *(conMatrixPointer + 1), *(conMatrixPointer + 2), *(conMatrixPointer + 3) } };
    connectivityMatrix_.push_back(next);
    conMatrixPointer += 4;
  }
//   std::cout << "ConMat" << std::endl;
//   for (auto i = connectivityMatrix_.begin(); i != connectivityMatrix_.end(); ++i)
//   {
//     for (const auto s : *i)
//       std::cout << s << ' ';
//   }
//   std::cout << std::endl;

}

void am3::cases::tugmc::DeformCaseStepPrecomputations::SetUsrVar(const int meshId, const double time, const int uvNumber, const int maxElemement, const double * userVariables)
{
  userVariablesVector_.clear();
  const double * ptr = userVariables;
  std::cout << "Internal variables:\n";
  for (int i = 0; i < maxElemement;i++)
  {
    std::cout << "element " << i + 1 << std::endl;
    std::vector<double> tempVect;
    for (int j = 0; j < uvNumber;j++)
    {
      tempVect.push_back(*ptr);
      std::cout << *ptr << " ";
      ptr++;
    }
    std::cout << std::endl;
    userVariablesVector_.push_back(tempVect);

  }
}

am3::cases::tugmc::DeformCaseStepPrecomputations::DeformCaseStepPrecomputations()
{
	std::ifstream myfile("model.dat");
	if (myfile.is_open())
	{
		predefinedModels_.clear();
		int readElement, readModel;
		while ((myfile >> readElement) && (myfile >> readModel))
		{
			predefinedModels_[readElement] = readModel;
		}
		myfile.close();
	}
}

std::map<am3::cases::tugmc::Controller::InputData, am3::cases::tugmc::Controller::OutputData> am3::cases::tugmc::DeformCaseStepPrecomputations::GetAllPrecomputedValues(const mpr::Time& time)
{

	std::map<Controller::InputData, Controller::OutputData> toRet;

	std::vector<Controller::Point> points = cont.GetPoints();


	for (auto point : points)
	{
		Controller::OutputData outputData = cont.Get(point).second;
		Controller::InputData inputData = cont.GetInputs(point).second;
		toRet.emplace(inputData, outputData);

	}

	return toRet;
}
