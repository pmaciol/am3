
/*! \file matCalc.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_matCalc_h__
#define am3caseTugMatCalc_matCalc_h__
/*	include *********************************************************/

// #include "../communicationMPI/controller.h"
// #include "../communicationMPI/instance.h"
// #include "../models/predefinedContracts.h"
// #include "../models/point.h"
// #include "../models/dataStructures.h"
// #include "../models/inputTypelists.h"
// #include "../modelTemplates/modelsTemplates.h"
// #include "../modelTemplates/macroState.h"
#include "caseTugMatCalc.h"

/*	using ***********************************************************/



/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
  namespace cases
  {
    namespace tugmc
    {

      template <typename INPUT>
      class SecondPhasePrecipitationsMatCalc
        : public PrecipitationsContract
        , public mo::InputTypelist < INPUT >
      {
      protected:
//        Controller controller_;
      public:
        typedef mot::MacroTemperatureContract Inputs;

        boost::shared_ptr<DeformCaseStepPrecomputations> precomp_;

//        void Init();

        mpr::PrecipitationsMeanRadius GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::PrecipitationsMeanRadius *const) override { return precomp_->Get<mpr::PrecipitationsMeanRadius>(point); };
				mpr::PrecipitationsVolumeFraction GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::PrecipitationsVolumeFraction *const) override { return precomp_->Get<mpr::PrecipitationsVolumeFraction>(point); };

      };

    } //tugmc
  } //cases
} //am3

#endif // am3caseTugMatCalc_matCalc_h__
