
/*! \file dllExport.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseMatCalcMPI_dllExport_h__
#define am3caseMatCalcMPI_dllExport_h__
/*	include *********************************************************/
/*	using ***********************************************************/
/*	extern **********************************************************/
/*	classes *********************************************************/

#ifdef BUILD_STANDALONE_CASE
  #define EXPORT __declspec(dllexport) 
#else
  #define EXPORT 
#endif

EXPORT int DEFORM_TEMPERATURES_SET(int materialId, double  time, double  timeStep, double  * temperatures, double  * deltaTemperatures, int nodeStart, int nodeEnd);
EXPORT int DEFORM_ALL_STRESS_GET(double * yieldStress, double * yieldStressDstrain, double * yieldStressDstrainRate, const int * meshId, const int * elementId);
EXPORT int DEFORM_TEMPERATURE(const double * temperature, const int * meshId, const int * elementId);
EXPORT int DEFORM_EFF_STR_RATE(const double * effStrainRate, const int * meshId, const int * elementId);
EXPORT int DEFORM_EFF_STR(const double * effStrain, const int * meshId, const int * elementId);
EXPORT int DEFORM_USRE1_GET(const int * length, double * values, const int * meshId, const int * elementId);
EXPORT int DEFORM_USRE1_SEND(const int * length, const double * values, const double * temperature, const double * time, const double * timeStep, const int * meshId, const int * elementId);
EXPORT int DEFORM_TIME(const double * time, const double * timeStep, const int * meshId, const int * elementId);
EXPORT int DEFORM_MESH_SET(const int * meshId, const double * time, const int * maxElemement, const int * ConnectivityMatrix);
EXPORT int DEFORM_MESH_ELUSRVAL(const int * meshId, const double * time, const int* uvNumber, const int * maxElemement, const double* userVariables);

#endif // am3caseMatCalcMPI_dllExport_h__
