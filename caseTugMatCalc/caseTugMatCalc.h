
/*! \file caseTugMatCalc.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_caseTugMatCalc_h__
#define am3caseTugMatCalc_caseTugMatCalc_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include "../softwareDeform/policyOneStepOneMeshTemperatureTime.h"
#include "../softwareDeform/policyOneStepOneElement.h"
#include "../communicationMPI/instance.h"
#include "../communicationMPI/controller.h"
#include "../models/modelsManager.h"


#include "../communicationMPI/controller.h"
#include "../communicationMPI/instance.h"
#include "../models/predefinedContracts.h"
#include "../models/point.h"
#include "../models/dataStructures.h"
#include "../models/inputTypelists.h"
#include "../modelTemplates/modelsTemplates.h"
#include "../modelTemplates/macroState.h"
#include "../utilMath/linearInterpolation1D.h"



//#include "matcalc.h"
#include "databus.h"

/*	using ***********************************************************/

namespace sdm = am3::software::deform;
namespace cco = am3::common::communication;

namespace mom = am3::model::manager;


//namespace cco = am3::common::communication;
namespace mda = am3::model::datapacks;
namespace mpt = am3::model::point;
namespace mpr = am3::model::properties;
namespace mo = am3::model;
namespace mot = am3::model::templates;
namespace apr = am3::math::approximation;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
      sdm::DeformHandlerFacadeMshTemperature* GetDeformHandlerFacadeMshTemperatureTime();
      sdm::DeformHandlerFacadeUpd* GetDeformHandlerFacadeUpdate();

      typedef sdm::OneStepOneElementNoElementCheckUserValues DeformCaseIV;
      typedef std::tuple<mpr::PrecipitationsMeanRadius, mpr::PrecipitationsVolumeFraction> PrecipitationOutput;



      //typedef cco::MpiInstance<mda::Coord2dTimeTemp, PrecipitationOutput, mpt::ComputedPoint<mpr::Coordinates2D> > Instance;
      typedef cco::MpiInstance<mda::Coord2dTimeTempVect, PrecipitationOutput, mpt::ComputedPoint<mpr::Coordinates2D> > Instance;
			typedef cco::Controller<mda::Coord2dTimeTempVect, PrecipitationOutput, Instance, mpt::ComputedPoint<mpr::Coordinates2D>, mpr::Coordinates2D > Controller;

      struct MicroModelData
      {
        unsigned id;
        double time;
        double temperature;
        double variable;
      };



      class DeformCaseStepPrecomputations
        : public sdm::OneStepOneMeshTemperatureTime
      {
      public:
        DeformCaseStepPrecomputations();

				typedef Controller::InputData InputData;
				typedef Controller::OutputData OutputData;

        template<typename VAR> VAR Get(mpt::Point<mpr::Coordinates2D> * point);

        virtual void SetTemperaturesInNodes(const int materialId, double const time, double const timeStep, double const * temperatures, double const * deltaTemperatures, const int nodeStart, const int nodeEnd) override;
        virtual void SetConnectivityMatrix(const int meshId, const double time, const int maxElemement, const int * ConnectivityMatrix) override;
        void Init(){ cont.Init(); };
				std::map<Controller::InputData, Controller::OutputData> GetAllPrecomputedValues(const mpr::Time& time);

      protected:
				std::map<int, int> predefinedModels_;
        cco::Controller<mda::Coord2dTimeTempVect, PrecipitationOutput, Instance, mpt::ComputedPoint<mpr::Coordinates2D>, mpr::Coordinates2D > cont;
        bool isInitialized_;

        void PushData(const int i, const double temperature, const double time, std::vector<MicroModelData> & dataToRun);
        bool IsInitialization(const double time) const;
        void Initialized();

        virtual void SetUsrVar(const int meshId, const double time, const int uvNumber, const int maxElemement, const double * userVariables) override;

        typedef std::array<unsigned, 4> nodeStruct;
        std::vector<nodeStruct> connectivityMatrix_;
        std::vector<std::vector< double>> userVariablesVector_;

			public:
				template<> mpr::PrecipitationsMeanRadius Get<mpr::PrecipitationsMeanRadius>(mpt::Point<mpr::Coordinates2D> * point)
				{
					mpt::ComputedPoint<mpr::Coordinates2D> localPoint(point);
					bool isPointInController = cont.IsPointAvailable(localPoint);
					Controller::PointWithOutputDataPair outputPair = cont.Get(localPoint);
					mpr::PrecipitationsMeanRadius toRet;
					if (isPointInController)
					{
						Controller::OutputData od = outputPair.second;
						toRet(std::get<0>(od));
						lo::debugLog("returning PrecipitationsMeanRadius from direct model" + std::to_string(toRet()));
					}
					else
					{
						throw std::out_of_range("No computations for this model");
					}

					return toRet;
				}

				template<> mpr::PrecipitationsVolumeFraction Get<mpr::PrecipitationsVolumeFraction>(mpt::Point<mpr::Coordinates2D> * point)
				{
					mpt::ComputedPoint<mpr::Coordinates2D> localPoint(point);
					bool isPointInController = cont.IsPointAvailable(localPoint);
					Controller::PointWithOutputDataPair outputPair = cont.Get(localPoint);
					mpr::PrecipitationsVolumeFraction toRet;
					if (isPointInController)
					{
						Controller::OutputData od = outputPair.second;
						toRet(std::get<1>(od));
						lo::debugLog("returning PrecipitationsMeanRadius from direct model" + std::to_string(toRet()));
					}
					else
					{
						throw std::out_of_range("No computations for this model");
					}
					return toRet;
				}
      };


		} //tugmc

	} //cases
} //am3

inline void VectorToOutput(std::vector<double>& input, am3::cases::tugmc::PrecipitationOutput& output)
{
  lo::debugLog("Converting received values to internal output, only precipitation on 11 and 12");
  std::get<0>(output)(input[10]);
  std::cout << input[10];
  std::get<1>(output)(input[11]);
  std::cout << input[11];
}
#endif // am3caseTugMatCalc_caseTugMatCalc_h__
