
/*! \file kbs.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_kbs_h__
#define am3caseTugMatCalc_kbs_h__
/*	include *********************************************************/

#include <boost/shared_ptr.hpp>
#include <fstream>
#include "../models/dataStructures.h"
#include "../models/point.h"
#include "../modelTemplates/macroState.h"


/*	using ***********************************************************/

namespace ctm = am3::cases::tugmc;
namespace mpr = am3::model::properties;
namespace pdi = am3::problem::dislocations;
namespace mot = am3::model::templates;
namespace mpt = am3::model::point;

/*	extern **********************************************************/



/*	classes *********************************************************/

namespace am3
{
	namespace cases
	{
    namespace tugmc
    {
      // UserGuide: This is ugly and simple "Knowledge Based System" for caseColdRolling.
      template<typename INPUT>
      class Kbs
        : public mo::InputTypelist < INPUT >
      {
      public:
        typedef mot::MacroContract Inputs;
        typedef mot::EmptyContract2D Contract;

        typedef mpr::Coordinates2D CoordinatesType;

				bool Check()
				{
					lo::debugLog("KBS exists");
					return true;
				}


        Kbs(boost::shared_ptr<INPUT> databus)
        {
          temperature(0);
          SetModelInput(databus.get());
          ReadModelsFromFile();
        }

        unsigned int instances;
        mpr::Temperature temperature;

        template <typename VARIABLE>
        mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point)
        {
//					lo::debugLog("Entering GetModelId");
          if (bus_)
          {
            const VARIABLE* trait = nullptr;
            return GetModelId(point, trait);
          }
          else
          {
            mpr::ModelID mId;
            mId(std::numeric_limits<int>::signaling_NaN());
            lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE, "Databus not initialized pointer not set");
            return mId;
          }
        }

      protected:

        std::map<int, int> predefinedModels_;
        void ReadModelsFromFile()
        {
          std::ifstream myfile("model.dat");
          if (myfile.is_open())
          {
            predefinedModels_.clear();
            int readElement, readModel;
            while ((myfile >> readElement)&&(myfile >> readModel))
            {
              predefinedModels_[readElement] = readModel;
            }
            myfile.close();
          }

          else lo::log<lo::SEV_ERROR>(lo::LOG_INFERENCE, "Unable to open file");
          
          for (auto& element : predefinedModels_) {
            lo::debugLog("Element " + std::to_string(element.first) + " Model " + std::to_string(element.second));
          }

        }

        // UserGuide:  Here is "knowledge"
        mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const mpr::StressAthermal* const)
        {
					lo::debugLog("KBS asked for StressAthermal Model");
          mpr::ModelID mId;
          mId(0);
          return mId;
        }
        // 				// UserGuide: GetModelId must be provided for each variable in Contract of the Switcher, which use this KBS. If more than one Switcher is used, GetModelId must be provided for all variables from all Contracts
        mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const mpr::PrecipitationsMeanRadius* const)
        {
					lo::debugLog("KBS asked for PrecipitationsMeanRadius Model");
          mpr::ModelID mId;
          mpr::Temperature localTemperature;
          localTemperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          mpr::ElementNumber elNr;
          elNr = bus_->GetFromDatabus<mpr::ElementNumber>(point);
          //std::cout << "ELEMENT NUMBER FOR THIS POINT IS = " << elNr() << std::endl;

          std::map<int, int>::iterator it = predefinedModels_.find(elNr());
          if (it != predefinedModels_.end())
            mId(it->second);
          else
          {
            if ((temperature() - localTemperature()) > 5)
            {
              mId(1);
            }
            else
            {
              mId(0);
            }
         }
          
          lo::debugLog("mId chosen = " + std::to_string(mId()));
          return mId;
        }
        mpr::ModelID GetModelId(mpt::Point<mpr::Coordinates2D> *point, const mpr::PrecipitationsVolumeFraction* const)
        {
					lo::debugLog("KBS asked for PrecipitationsVolumeFraction Model");
          mpr::ModelID mId;
          mpr::Temperature localTemperature;
          localTemperature = bus_->GetFromDatabus<mpr::Temperature>(point);
          mpr::ElementNumber elNr;
          elNr = bus_->GetFromDatabus<mpr::ElementNumber>(point);
          //std::cout << "ELEMENT NUMBER FOR THIS POINT IS = " << elNr() << std::endl;

          std::map<int, int>::iterator it = predefinedModels_.find(elNr());
          if (it != predefinedModels_.end())
            mId(it->second);
          else
          {
            if ((temperature() - localTemperature()) > 5)
            {
              mId(1);
            }
            else
            {
              mId(0);
            }
          }
          lo::debugLog("mId chosen = " + std::to_string(mId()));
          return mId;
        };
      };
    } //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalc_kbs_h__
