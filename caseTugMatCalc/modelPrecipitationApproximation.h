
/*! \file modelPrecipitationApproximation.h **************************************************
 * \author		Piotr Maciol
 * \copyright 	GNU Public License.
 * \brief       
 * \details   	
*********************************************************************/
#ifndef am3caseTugMatCalc_modelPrecipitationApproximation_h__
#define am3caseTugMatCalc_modelPrecipitationApproximation_h__
/*	include *********************************************************/

#include "contract.h"
#include "../models/inputTypelists.h"
#include "../modelTemplates/modelsTemplates.h"
#include "../utilMath/approximation.h"

/*	using ***********************************************************/

namespace mo = am3::model;
namespace mot = am3::model::templates;
namespace apr = am3::math::approximation;

/*	extern **********************************************************/



/*	classes *********************************************************/


namespace am3
{
	namespace cases
	{
		namespace tugmc
		{
			template <typename INPUT>
			class SecondPhasePrecipitationsApproximated
				: public PrecipitationsContract
				, public mo::InputTypelist < INPUT >
			{
			public:
				typedef mot::MacroTemperatureContract Inputs;

				SecondPhasePrecipitationsApproximated() : lastTime_(0){};

				 mpr::PrecipitationsMeanRadius GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::PrecipitationsMeanRadius *const) override;
				 mpr::PrecipitationsVolumeFraction GetState(mpt::Point<mpr::Coordinates2D> *, const mpr::PrecipitationsVolumeFraction *const) override;
				template<typename VAR> VAR Get(mpt::Point<mpr::Coordinates2D> * point);
				template<> mpr::PrecipitationsMeanRadius Get<mpr::PrecipitationsMeanRadius>(mpt::Point<mpr::Coordinates2D> * point) { mpr::PrecipitationsMeanRadius* trait;  return GetState(point, trait); };
				template<> mpr::PrecipitationsVolumeFraction Get<mpr::PrecipitationsVolumeFraction>(mpt::Point<mpr::Coordinates2D> * point) { mpr::PrecipitationsVolumeFraction* trait;  return GetState(point, trait); };
				void SetApproximationSource(boost::shared_ptr<DeformCaseStepPrecomputations> approximationSource);

			protected:
				typedef apr::LinearInterpolation1D<mpr::PrecipitationsVolumeFraction, mpr::Temperature> InterpolationVolumeFract;
				typedef apr::LinearInterpolation1D<mpr::PrecipitationsMeanRadius, mpr::Temperature> InterpolationMeanRadius;

				void InitilizeIfNeeded(const mpt::Point<mpr::Coordinates2D>& point);

				void Initilize(const mpr::Time &actualTime);


			private:
				boost::shared_ptr<DeformCaseStepPrecomputations> approximationSource_;
				mpr::Time lastTime_;
				InterpolationVolumeFract interpVolFrac_;
				InterpolationMeanRadius interpMeanRad_;

			};
			

			////////////////////////////////////////////////////////////////////////// IMPLEMENTATION //////////////////////////////////////////////////////////////////////////

			template <typename INPUT>
			void am3::cases::tugmc::SecondPhasePrecipitationsApproximated<INPUT>::Initilize(const mpr::Time &actualTime)
			{
				std::map<DeformCaseStepPrecomputations::InputData, DeformCaseStepPrecomputations::OutputData> precomputedValues = approximationSource_->GetAllPrecomputedValues(actualTime);
				interpMeanRad_.ClearAll();
				interpVolFrac_.ClearAll();
				for (auto pair : precomputedValues)
				{
					interpMeanRad_.AddNode(std::get<2>(pair.first), std::get<0>(pair.second));
					interpVolFrac_.AddNode(std::get<2>(pair.first), std::get<1>(pair.second));
				}
				lastTime_ = actualTime;
			}


			template <typename INPUT>
			mpr::PrecipitationsVolumeFraction am3::cases::tugmc::SecondPhasePrecipitationsApproximated<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point , const mpr::PrecipitationsVolumeFraction *const)
			{
				mpr::PrecipitationsVolumeFraction toRet;
				mpr::Temperature localTemperature = bus_->GetFromDatabus<mpr::Temperature>(point);
				InitilizeIfNeeded(*point);
				toRet = interpVolFrac_.GetApproximation(localTemperature);
				return toRet;
			}

			template <typename INPUT>
			mpr::PrecipitationsMeanRadius am3::cases::tugmc::SecondPhasePrecipitationsApproximated<INPUT>::GetState(mpt::Point<mpr::Coordinates2D> * point, const mpr::PrecipitationsMeanRadius *const)
			{
				mpr::PrecipitationsMeanRadius toRet;
				mpr::Temperature localTemperature = bus_->GetFromDatabus<mpr::Temperature>(point);
				InitilizeIfNeeded(*point);
				toRet = interpMeanRad_.GetApproximation(localTemperature);
				return toRet;
			}

			template <typename INPUT>
			void am3::cases::tugmc::SecondPhasePrecipitationsApproximated<INPUT>::SetApproximationSource(boost::shared_ptr<DeformCaseStepPrecomputations> approximationSource)
			{
				approximationSource_ = approximationSource;
			}


			template <typename INPUT>
			void am3::cases::tugmc::SecondPhasePrecipitationsApproximated<INPUT>::InitilizeIfNeeded(const mpt::Point<mpr::Coordinates2D>& point)
			{
				mpr::Time actualTime = point.GetTimeProperty();
				if (actualTime > lastTime_)
				{
					Initilize(actualTime);
				}
			}


		} //tugmc
	} //cases
} //am3
#endif // am3caseTugMatCalc_modelPrecipitationApproximation_h__
